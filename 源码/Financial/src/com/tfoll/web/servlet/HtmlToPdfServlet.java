package com.tfoll.web.servlet;

import com.lowagie.text.pdf.BaseFont;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class HtmlToPdfServlet extends HttpServlet {

	private static final long serialVersionUID = -5648766590580602023L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content_str = request.getParameter("content");
		String css = request.getParameter("css");
		if (css == null || "".equals(css)) {
			css = "";
		}
		String content = content_str.replaceAll("<br>", "<br/>");
		String pdfName = "contract";
		response.setHeader("Content-disposition", "attachment;filename=" + pdfName);
		response.setContentType("application/pdf");
		response.setCharacterEncoding("UTF-8");
		OutputStream os = response.getOutputStream();
		try {
			ITextRenderer renderer = new ITextRenderer();
			ITextFontResolver fontResolver = renderer.getFontResolver();
			fontResolver.addFont("C:/Windows/fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
			StringBuffer html = new StringBuffer();
			// DOCTYPE 必需写否则类似于 这样的字符解析会出现错误
			html.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
			html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">").append("<head>").append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />").append("<style type=\"text/css\" mce_bogus=\"1\">body {font-family: SimSun;}</style>").append("<style type=\"text/css\">")
			// .append(""+ css +"")
					.append("</style>").append("</head>").append("<body>");
			html.append("" + content + "");
			html.append("</body></html>");
			renderer.setDocumentFromString(html.toString());
			// 解决图片的相对路径问题
			// renderer.getSharedContext().setBaseURL("file:/F:/teste/html/");
			renderer.layout();
			renderer.createPDF(os);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			os.close();
		}

	}

}

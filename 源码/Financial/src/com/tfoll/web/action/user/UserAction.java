package com.tfoll.web.action.user;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;

/**
 * @author xiangxuan
 * 
 */
@ActionKey("/user/user")
public class UserAction extends Controller {

	/**
	 * 下面的方法都还没有套页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "修改密码", last_update_author = "向旋")
	public void updata_password() {
		String old_password = getParameter("old_password");
		if (!Utils.isNotNullAndNotEmptyString(old_password)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_password = user.getString("password");
		if (!old_password.equals(session_password)) {
			renderText("2");
			return;
		}
		String new_password = getParameter("new_password");
		if (!Utils.isNotNullAndNotEmptyString(new_password)) {
			renderText("3");
			return;
		}
		if (new_password.length() < 6 || new_password.length() > 35) {
			renderText("4");
			return;
		}
		boolean update_ok = user.set("password", new_password).update();
		if (!update_ok) {
			renderText("5");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "修改提现密码", last_update_author = "向旋")
	public void update_withdraw_password() {
		String old_password = getParameter("old_password");
		if (Utils.isNotNullAndNotEmptyString(old_password)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_password = user.getString("password");
		if (!old_password.equals(session_password)) {
			renderText("2");
			return;
		}
		String new_password = getParameter("new_password");
		if (!Utils.isNotNullAndNotEmptyString(new_password)) {
			renderText("3");
			return;
		}
		if (new_password.length() < 6 || new_password.length() > 35) {
			renderText("4");
			return;
		}
		boolean update_ok = user.set("money_password", new_password).update();
		if (!update_ok) {
			renderText("5");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "绑定手机号码", last_update_author = "向旋")
	public void binding_phone() {
		String phone = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("1");
			return;
		}
		if (phone.length() != 11) {
			renderText("2");
		}
		String page_code = getParameter("code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("3");
			return;
		}
		String session_code = getSessionAttribute("binding_phone");
		if (!page_code.equals(session_code)) {
			renderText("4");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		boolean binding_isok = user.set("phone", phone).update();
		if (!binding_isok) {
			renderText("5");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "先使用旧手机验证", last_update_author = "向旋")
	public void update_phone() {
		String old_phone = getParameter("old_phone");
		if (!Utils.isNotNullAndNotEmptyString(old_phone)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_phone = user.getString("phone");
		if (!old_phone.equals(session_phone)) {
			renderText("2");
			return;
		}
		String page_code = getParameter("page_code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("3");
			return;
		}
		String session_code = getSessionAttribute("update_phone_code");
		if (!page_code.equals(session_code)) {
			renderText("4");
			return;
		}
		renderText("5");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "通过身份证修改手机号码", last_update_author = "向旋")
	public void update_phone_for_idenitty() {
		String user_identity = getParameter("user_identity");
		if (!Utils.isNotNullAndNotEmptyString(user_identity)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_identity = user.getString("user_identity");
		if (!Utils.isNotNullAndNotEmptyString(session_identity)) {
			renderText("2");
			return;
		}
		if (!user_identity.equals(session_identity)) {
			renderText("3");
			return;
		}
		String money_password = getParameter("money_password");
		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("4");
			return;
		}
		String session_money_password = user.getString("monety_password");
		if (!Utils.isNotNullAndNotEmptyString(session_money_password)) {
			renderText("5");
			return;
		}
		if (!money_password.equals(session_money_password)) {
			renderText("6");
			return;
		}
		renderText("7");
		return;
	}
	/**
	 *上面的方法都还没有套页面
	 **/
}
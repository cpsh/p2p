package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.upload.CosUpload;
import com.tfoll.trade.upload.UploadFile;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.common.code.CheckCodeServlet;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.SystemNotificationM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserEmailRequestWaitingM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMessageRequestWaitingM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserSuggestM;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.GetCode;
import com.tfoll.web.util.GetTypeByHead;
import com.tfoll.web.util.GetUserIdentityInfo;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.RiskParameterFilter;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebApp;
import com.tfoll.web.util.GetTypeByHead.Image;
import com.tfoll.web.util.GetUserIdentityInfo.UserIdentityInfo;
import com.tfoll.web.util.page.PageDiv;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

@ActionKey("/user/usercenter")
public class UserCenterAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "进入用户中心", last_update_author = "hx")
	public void into_user_center() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money(user_id);

		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
		user_now_money.set("cny_can_used", cny_can_used);
		BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
		cny_freeze = cny_freeze.setScale(2, BigDecimal.ROUND_DOWN);
		user_now_money.set("cny_freeze", cny_freeze);
		setAttribute("user", user);
		setAttribute("user_now_money", user_now_money);

		// 左部导航点击个人信息
		removeSessionAttribute("curr_id");
		removeSessionAttribute("pare_id");
		// 安全等级精度条
		String phone = user.getString("phone");// 手机号
		String real_name = user.getString("real_name");// 真实姓名
		String email = user.getString("email");// 邮箱
		String money_password = user.getString("money_password");// 提现密码

		int security_progress = 0;
		// 判断电话号码是否绑定
		if (phone != null && !"".equals(phone)) {
			security_progress += 25;
			setAttribute("is_bind_phone", 1);
		} else {
			setAttribute("is_bind_phone", 0);
		}
		// 判断是否实名认证
		if (real_name != null && !"".equals(real_name)) {
			security_progress += 25;
			setAttribute("is_real_name_confirmed", 1);
		} else {
			setAttribute("is_real_name_confirmed", 0);
		}
		// 判断是否绑定邮箱
		if (email != null && !"".equals(email)) {
			security_progress += 25;
			setAttribute("is_bind_email", 1);
		} else {
			setAttribute("is_bind_email", 0);
		}
		// 判断是否设置资金密码
		if (money_password != null && !"".equals(money_password)) {
			security_progress += 25;
			setAttribute("is_set_money_password", 1);
		} else {
			setAttribute("is_set_money_password", 0);
		}
		setAttribute("security_progress", security_progress);

		int borrow_type = user.getInt("borrow_type");
		if (borrow_type == 1 || borrow_type == 2 || borrow_type == 3) {
			/**
			 * 显示借款状态的四个阶段 申请 借贷信息 凑集 理财
			 */
			// UserM.LoanProcessInformation loan_process_information =
			// UserM.get_loan_process_information(user_id, user);
			UserM.LoanProcessInformation loan_process_information = UserM.get_loan_process_information(user_id, user);
			setAttribute("loan_process_information", loan_process_information);
		}

		// 获取借款账户的信息
		Map<String, Object> borrow_info = CommonRepayMethod.get_borrow_info(user_id);
		setAttribute("borrow_info", borrow_info);

		// 获取债权投资账户资产，即带回收本金
		Map<String, Object> financial_account_asset = CommonRepayMethod.get_elite_bid_account(user_id);
		setAttribute("financial_account_asset", financial_account_asset);

		// 获取联富宝①账户资产，②已赚金额，③加权平均收益，④持有数量
		Map<String, Object> get_lfoll_fix_bid = CommonRepayMethod.get_lfoll_fix_bid_account_info(user_id);
		BigDecimal to_collect_pricipal = (BigDecimal) financial_account_asset.get("to_collect_pricipal");// 债权投资待还本金
		BigDecimal total_bid_money = (BigDecimal) get_lfoll_fix_bid.get("total_bid_money");// 联富宝投资待回收金额
		BigDecimal financial_total = to_collect_pricipal.add(total_bid_money).setScale(2, BigDecimal.ROUND_DOWN);// 理财总资金
		setAttribute("get_lfoll_fix_bid", get_lfoll_fix_bid);
		setAttribute("financial_total", financial_total);

		// 总资产负债
		BigDecimal weihuan_total = (BigDecimal) borrow_info.get("weihuan_total");
		// 投资金额 + 账户余额 - 资产负债 = 账户净资产
		// 净资产
		BigDecimal net_worth_total = cny_can_used.add(cny_freeze).add(financial_total).subtract(weihuan_total).setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("net_worth_total", net_worth_total);
		// 已赚总金额 = 债权投资已赚金额 + 联富宝已赚金额
		BigDecimal total_encome = new BigDecimal((String) financial_account_asset.get("eared_money")).add((BigDecimal) get_lfoll_fix_bid.get("total_earned_incom")).setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("total_encome", total_encome);
		renderJsp("/user/usercenter/account_info.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(value = { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "跳转到借到相关页面-页面跳转经可能用JAVA代码进行跳转-这样可以进行代码化的定制-但是查询数据会增加服务器的性能损耗-只有特殊的状态不确定的时候才进行这个Action的跳转", last_update_author = "czh")
	public void into_user_borrow_page() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		if (user == null) {
			throw new RuntimeException("user is null");
		}
		int borrow_type = user.getInt("borrow_type");
		// 不用查询后面信息
		String __ROOT_PATH__ = WebApp.getWebRootPath(getRequest());
		if (borrow_type == 0) {
			// <%=__ROOT_PATH__%>/user/usercenter/into_safety_information.html
			//
			renderHttp(__ROOT_PATH__ + "/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=1");// 默认消费贷-通常使用的
			return;
		}

		/**
		 * 查询最近的借款申请单
		 */
		BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findFirst("SELECT * from borrower_bulk_standard_apply_order WHERE user_id=?  order by id desc LIMIT 1", new Object[] { user_id });
		if (borrower_bulk_standard_apply_order == null) {
			logger.error("查询申请单失败-请联系客服 user_id=" + user_id);
			renderText("查询申请单失败-请联系客服");
			return;
		} else {
			// 均支持该申请单ID
			setSessionAttribute(SystemConstantKey.Applying_Order_Id_In_Session, borrower_bulk_standard_apply_order.getLong("id") + "");
			/**
			 * <pre>
			 * `state`'单子状态 
			 * 1.创建订单完成-*
			 * 2填写认证信息完成-但是由于非必填的资料没有上传-那么可以在三天内仍然可以修改---同时这个状态所做的事和系统撤回请求修改的信息一致-*
			 * 3已经提交申请
			 * 4被驳回修改认证信息-*
			 * 5系统审核失败
			 * 6系统审核成功
			 * </pre>
			 */
			long apply_order_id = borrower_bulk_standard_apply_order.getLong("id");
			int state = borrower_bulk_standard_apply_order.getInt("state");
			// 单子刚刚创建
			if (state == 1) {// * 123
				renderHttp(__ROOT_PATH__ + "/user/authentication/persional_info/persional_index.html");
				return;
			} else if (state == 2) {// *
				renderHttp(__ROOT_PATH__ + "/user/authentication/submit_application/auditing_index.html");
				return;

			} else if (state == 3) {
				renderHttp(__ROOT_PATH__ + "/user/authentication/submit_application/auditing_index.html");
				return;
			} else if (state == 4) {// *
				renderHttp(__ROOT_PATH__ + "/user/authentication/submit_application/auditing_index.html");
				return;

			} else if (state == 5) {
				renderHttp(__ROOT_PATH__ + "/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=" + borrow_type);// 默认消费贷-通常使用的
				return;
			} else if (state == 6) {
				// 需要讨论凑集状态
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT * from borrower_bulk_standard_gather_money_order WHERE  apply_order_id=?", new Object[] { apply_order_id });
				if (borrower_bulk_standard_gather_money_order == null) {
					logger.error("查询不到凑集单信息,请联系客服 user_id=" + user_id);
					renderHttp("查询不到凑集单信息,请联系客服");
					return;
				} else {
					/**
					 * <pre>
					 *  `gather_state` 
					 *   '凑集状态
					 *    1筹集中
					 *    2在规定的时间内凑集失败-这个凑集过程的监控需要定时器一直监控-需要采用临时表#-这个字段在申请表里面没有
					 *    3筹集成功',
					 * </pre>
					 */
					int gather_state = borrower_bulk_standard_gather_money_order.getInt("gather_state");
					if (gather_state == 1) {
						renderHttp(__ROOT_PATH__ + "/user/financial/borrow/to_gather_money_page.html?borrow_type=" + borrow_type);// borrow_type
						return;
					} else if (gather_state == 2) {
						renderHttp(__ROOT_PATH__ + "/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=" + borrow_type);// 默认消费贷-通常使用的
						return;
					} else if (gather_state == 3) {
						/**
						 * <pre>
						 * `payment_state` 
						 *  '还款状态 
						 *  1.还款之前
						 *  2.正在还款 3.还款完成',
						 * </pre>
						 */
						int payment_state = borrower_bulk_standard_gather_money_order.getInt("payment_state");
						if (payment_state == 1) {
							// 错误的状态
							renderHttp("错误的状态");
							return;

						} else if (payment_state == 2) {// 跳转到还款页面
							renderHttp(__ROOT_PATH__ + "/user/usercenter/loan_manage/to_my_loan_page.html");
							return;

						} else if (payment_state == 3) {
							renderHttp(__ROOT_PATH__ + "/user/financial/borrow/loan_to_write_borrow_apply.html?borrow_type=" + borrow_type);// 默认消费贷-通常使用的
							return;
						} else {
							renderText("不存在该状态");
							return;
						}
					} else {

						renderText("不存在该状态");
						return;
					}
				}

			} else {

				renderText("不存在该状态");
				return;

			}

		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "需要登录", function_description = "跳转到个人基本信息", last_update_author = "向旋")
	public void to_personal_information() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		String nickname = user.getString("nickname");
		setAttribute("nickname2", nickname);
		String real_name = user.getString("real_name");
		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			setAttribute("name", null);
		} else {
			int len = real_name.length();
			String real_name2 = null;
			if (len == 2) {
				real_name2 = real_name.substring(0, 1);
				real_name2 = real_name2 + "*";
			}
			if (len > 2) {
				String real_name4 = real_name.substring(len - 1);
				String num = "";
				for (int i = 0; i < len - 1; i++) {
					num += "*";
				}
				real_name2 = num + real_name4;
			}
			setAttribute("name1", real_name2 == null ? real_name : real_name2);
		}
		String user_identity = user.getString("user_identity");
		if (!Utils.isNotNullAndNotEmptyString(user_identity)) {
			setAttribute("user_identity1", null);
		} else {
			String user_identity2 = user_identity.substring(0, 2) + "**** **** **** ****";
			int a = user_identity.charAt(16);
			String birthday = user_identity.substring(6, 14);
			String year = birthday.substring(0, 4);
			String month = birthday.substring(4, 6);
			String day = birthday.substring(6, 8);
			String birthday2 = year + "-" + month + "-" + day;
			setAttribute("a", a);
			setAttribute("birthday", birthday2);
			setAttribute("user_identity2", user_identity2);
		}

		String phone = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			setAttribute("phone1", null);
		} else {
			String phone1 = phone.substring(0, 3);
			String phone3 = phone.substring(7, 11);
			String display_phone = phone1 + "****" + phone3;
			setSessionAttribute("phone2", display_phone);
		}
		String email = user.getString("email");
		if (Utils.isNotNullAndNotEmptyString(email)) {
			int temp = email.indexOf('@');
			String display_email = email.substring(0, 2) + "****" + email.substring(temp, email.length());
			setSessionAttribute("email", display_email);
		}
		String url = user.getString("url");
		if (Utils.isNotNullAndNotEmptyString(url)) {
			setAttribute("url", url);
		}

		renderJsp("/user/usercenter/account_basic_information.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "进入安全信息页面", last_update_author = "向旋")
	public void into_safety_information() {
		UserM user = getSessionAttribute(SystemConstantKey.User);

		String email = user.getString("email");
		if (Utils.isNotNullAndNotEmptyString(email)) {
			int temp = email.indexOf('@');
			String display_email = email.substring(0, 2) + "****" + email.substring(temp, email.length());
			setAttribute("email", display_email);
		}
		String nickname = user.getString("nickname");
		setAttribute("nickname2", nickname);

		String real_name = user.getString("real_name");
		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			setAttribute("name", real_name);
		} else {
			int len = real_name.length();
			String real_name2 = null;
			if (len == 2) {
				real_name2 = real_name.substring(1, 2);
				real_name2 = "*" + real_name2;
			}
			if (len > 2) {
				String real_name4 = real_name.substring(len - 1);
				String num = "";
				for (int i = 0; i < len - 1; i++) {
					num += "*";
				}
				real_name2 = num + real_name4;
			}
			setAttribute("name1", real_name2 == null ? real_name : real_name2);
		}
		String user_identity = user.getString("user_identity");

		if (!Utils.isNotNullAndNotEmptyString(user_identity)) {
			setAttribute("user_identity1", user_identity);
		} else {
			String user_identity2 = user_identity.substring(0, 2) + "**** **** **** ****";
			int a = user_identity.charAt(16);
			String birthday = user_identity.substring(6, 14);
			String year = birthday.substring(0, 4);
			String monty = birthday.substring(4, 6);
			String day = birthday.substring(6, 8);
			String birthday2 = year + "-" + monty + "-" + day;
			setAttribute("a", a);
			setAttribute("birthday", birthday2);
			setAttribute("user_identity2", user_identity2);
		}

		String phone = user.getString("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			setAttribute("phone1", phone);
		} else {
			String phone1 = phone.substring(0, 3);
			String phone3 = phone.substring(7, 11);
			String display_phone = phone1 + "****" + phone3;
			setAttribute("phone2", display_phone);
		}
		renderJsp("/user/usercenter/account_safety_information.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "实名认证", last_update_author = "zjb")
	public void real_name() {
		final String identity = getParameter("identity");
		final String real_name = getParameter("real_name");

		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			renderText("0"); // 请输入姓名
			return;
		}

		String regex = "[\u4E00-\u9FA5]{2,5}(?:·[\u4E00-\u9FA5]{2,5})*"; // 表示a或F
		Pattern pat = Pattern.compile(regex);
		Matcher mat = pat.matcher(real_name);
		boolean rs = mat.find();
		if (!rs) {
			renderText("1"); // 姓名有误
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(identity)) {
			renderText("2"); // 请输入身份证号码
			return;
		}
		final UserM user = UserM.dao.findFirst("select * from user_info where user_identity = ?", identity);
		if (user != null) {
			renderText("3");
			return;
		}
		UserIdentityInfo user_identity_info = GetUserIdentityInfo.getUserIdentityInfo(identity);
		if (user_identity_info == null) {
			renderText("4");
			return;
		}
		boolean ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserM user2 = getSessionAttribute(SystemConstantKey.User);
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String d = sdf.format(date);
				boolean save_identity = user2.set("user_identity", identity).set("real_name", real_name).update();
				boolean send_system_message = SystemNotificationM.send_system_notification(0, user2.getInt("id"), "实名认证通知", "您好，您于" + d + "在联富金融提交的身份证绑定信息已经成功通过审核。");
				setSessionAttribute(SystemConstantKey.User, user2);
				return save_identity && send_system_message;
			}
		});
		if (!ok) {
			renderText("5");
			return;
		}
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "修改登录密码", last_update_author = "向旋")
	public void update_login_password() {
		String old_password = getParameter("old_password");
		if (!Utils.isNotNullAndNotEmptyString(old_password)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_password = user.getString("password");
		if (!MD5.md5(old_password).equals(session_password)) {
			renderText("2");
			return;
		}
		String new_password = getParameter("new_password");
		if (!Utils.isNotNullAndNotEmptyString(new_password)) {
			renderText("3");
			return;
		}
		if (new_password.length() > 35 || new_password.length() < 6) {
			renderText("4");
			return;
		}
		String new_password2 = getParameter("new_password2");
		if (!Utils.isNotNullAndNotEmptyString(new_password2)) {
			renderText("5");
			return;
		}
		if (!new_password2.equals(new_password)) {
			renderText("6");
			return;
		}
		boolean update_password_ok = user.set("password", MD5.md5(new_password)).update();
		if (!update_password_ok) {
			renderText("7");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("8");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "设置提现密码", last_update_author = "向旋")
	public void binding_withdraw_password() {
		String withdraw_password = getParameter("withdraw_password");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password)) {
			renderText("1");
			return;
		}
		if (withdraw_password.length() < 6 || withdraw_password.length() > 35) {
			renderText("2");
			return;
		}
		String withdraw_password2 = getParameter("withdraw_password2");
		if (!Utils.isNotNullAndNotEmptyString(withdraw_password2)) {
			renderText("3");
			return;
		}
		if (!withdraw_password.equals(withdraw_password2)) {
			renderText("4");
			return;
		}
		String page_code = getParameter("page_code");
		String session_code = getSessionAttribute("binding_withdraw_code");
		String session_voice = getSessionAttribute("binding_withdraw_voice");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("5");
			return;
		}
		if (!page_code.equals(session_code) && !page_code.equals(session_voice)) {
			renderText("6");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		boolean binding_withdraw_password_ok = user.set("money_password", MD5.md5(withdraw_password)).update();
		if (!binding_withdraw_password_ok) {
			renderText("7");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("8");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "修改提现密码", last_update_author = "向旋")
	public void update_withdraw_password() {
		String old_password = getParameter("old_password");
		if (!Utils.isNotNullAndNotEmptyString(old_password)) {
			renderText("1");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		String session_password = user.getString("money_password");
		if (!MD5.md5(old_password).equals(session_password)) {
			renderText("2");
			return;
		}
		String new_password = getParameter("new_password");
		if (!Utils.isNotNullAndNotEmptyString(new_password)) {
			renderText("3");
			return;
		}
		if (new_password.length() > 35 || new_password.length() < 6) {
			renderText("4");
			return;
		}
		String new_password2 = getParameter("new_password2");
		if (!Utils.isNotNullAndNotEmptyString(new_password2)) {
			renderText("5");
			return;
		}
		if (!new_password2.equals(new_password)) {
			renderText("6");
			return;
		}
		boolean update_password_ok = user.set("money_password", MD5.md5(new_password)).update();
		if (!update_password_ok) {
			renderText("7");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("8");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "绑定手机", last_update_author = "向旋")
	public void binding_phone() {
		final String tel = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("1");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", new Object[] { tel });
		if (user != null) {
			renderText("2");
			return;
		}
		String code = getParameter("page_code");
		if (!Utils.isNotNullAndNotEmptyString(code)) {
			renderText("3");
			return;
		}
		String session_code = getSessionAttribute("binding_phone");
		String session_voice = getSessionAttribute("binding_phone_voice");
		if (!code.equals(session_code) && !code.equals(session_voice)) {
			renderText("4");
			return;
		}
		final UserM user1 = getSessionAttribute(SystemConstantKey.User);
		boolean ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				boolean binding_ok = user1.set("phone", tel).update();
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String d = sdf.format(date);
				boolean send_system_message = SystemNotificationM.send_system_notification(0, user1.getInt("id"), "手机号绑定通知", "您好，您于" + d + "在联富金融提交的手机绑定信息已经成功通过审核。");
				return binding_ok && send_system_message;
			}
		});

		if (!ok) {
			renderText("5");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user1);
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "修改手机号码", last_update_author = "向旋")
	public void update_phone() {
		String old_tel = getParameter("old_tel");
		if (!Utils.isNotNullAndNotEmptyString(old_tel)) {
			renderText("1");// 原手机号为空
			return;
		}
		UserM user2 = getSessionAttribute(SystemConstantKey.User);
		String phone = user2.getString("phone");
		if (!old_tel.equals(phone)) {
			renderText("2");// 原手机号填写错误
			return;
		}
		String tel = getParameter("tel");
		if (!Utils.isNotNullAndNotEmptyString(tel)) {
			renderText("3");// 手机号为空
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", tel);
		if (user != null) {
			renderText("4");// 该手机号已经被使用
			return;
		}
		String page_code = getParameter("update_phone_code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("5");
			return;
		}
		String session_code = getSessionAttribute("update_phone_code");
		String session_voice = getSessionAttribute("update_phone_voice");
		if (!page_code.equals(session_code) && !page_code.equals(session_voice)) {
			renderText("6");
			return;
		}
		boolean update_ok = user2.set("phone", tel).update();
		if (!update_ok) {
			renderText("7");
			return;
		}
		setSessionAttribute(SystemConstantKey.User, user2);
		renderText("8");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "找回提现密码的验证码是否正确", last_update_author = "向旋")
	public void back_whthdraw_code_isok() {
		String page_code = getParameter("page_code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("1");
			return;
		}
		String session_code = getSessionAttribute("back_withdraw_code");
		String session_voice = getSessionAttribute("back_withdraw_voice");
		if (!page_code.equals(session_code) && !page_code.equals(session_voice)) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "找回提现密码", last_update_author = "向旋")
	public void back_whthdraw_password() {
		final String new_withdraw_password = getParameter("new_withdraw_password");
		if (!Utils.isNotNullAndNotEmptyString(new_withdraw_password)) {
			renderText("1");
			return;
		}
		if (new_withdraw_password.length() < 6 || new_withdraw_password.length() > 35) {
			renderText("2");
			return;
		}
		String new_withdraw_password2 = getParameter("new_withdraw_password2");
		if (!Utils.isNotNullAndNotEmptyString(new_withdraw_password2)) {
			renderText("3");
			return;
		}
		if (!new_withdraw_password.equals(new_withdraw_password2)) {
			renderText("4");
			return;
		}
		final UserM user = getSessionAttribute(SystemConstantKey.User);
		if (user == null) {
			renderText("5");
			return;
		}
		Date date = new Date();
		String code = Model.Date.format(date);
		String[] a = new String[] { code };
		boolean send_ok = UserMessageRequestWaitingM.back_withdraw_message2(user.getString("phone"), a, user.getInt("id"));
		if (!send_ok) {
			renderText("6");
			return;
		}
		boolean back_ok = user.set("money_password", MD5.md5(new_withdraw_password)).update();
		if (back_ok) {
			setSessionAttribute(SystemConstantKey.User, user);
			renderText("7");
			return;
		} else {
			renderText("6");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "邮箱绑定发送邮箱", last_update_author = "向旋")
	public void bingding_email_send_email() {
		String email = getParameter("email");
		if (!Utils.isNotNullAndNotEmptyString(email)) {
			renderText("1");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where email=?", new Object[] { email });
		if (user != null) {
			renderText("2");
			return;
		}
		String code = GetCode.get_code();
		setSessionAttribute("code", code);
		UserM user2 = getSessionAttribute(SystemConstantKey.User);
		int id = user2.getInt("id");

		String nickname = user2.getString("nickname");
		boolean send_eamil = UserEmailRequestWaitingM.add_user_bingding_email(id, email, code, nickname);
		if (send_eamil == false) {
			renderText("3");
			return;
		}
		renderText("4");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "邮箱绑定", last_update_author = "向旋")
	public void bingding_email() {
		String email = getParameter("email");
		if (!Utils.isNotNullAndNotEmptyString(email)) {
			renderText("1");
			return;
		}
		String page_code = getParameter("code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("2");
			return;
		}
		String session_code = getSessionAttribute("code");
		if (!page_code.equals(session_code)) {
			renderText("3");
			return;
		}
		UserM user = getSessionAttribute(SystemConstantKey.User);
		boolean update_ok = user.set("email", email).update();
		setSessionAttribute(SystemConstantKey.User, user);
		if (update_ok == false) {
			renderText("4");
			return;
		}
		renderText("5");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "跳转到站内信", last_update_author = "向旋")
	public void to_station_information() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		Record system_notification = Db.findFirst("select u.add_time u_add_time,s.* from sys_notification s,user_info u where s.user_id = u.id and user_id = ?", new Object[] { user.getInt("id") });
		if (system_notification == null) {
			setAttribute("tips", "暂时没有数据");
			renderJsp("/user/usercenter/station_information.jsp");
			return;
		}
		String content = system_notification.getString("content");
		content = content.substring(0, 15) + "...";
		system_notification.add("content", content);
		setAttribute("system_notification", system_notification);
		renderJsp("/user/usercenter/station_information.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "跳转到站内信", last_update_author = "向旋")
	public void station_information_details() {
		int pn = getParameterToInt("pn", 1);
		UserM user = getSessionAttribute(SystemConstantKey.User);
		Long totalRow = Db.queryLong("select count(1) from sys_notification s,user_info u where s.user_id = u.id and user_id = ?", new Object[] { user.getInt("id") });
		List<SystemNotificationM> system_station = SystemNotificationM.dao.find("select * from sys_notification where user_id = ? and system_status = ? ", new Object[] { user.getInt("id"), 0 });
		for (SystemNotificationM systemNotificationM : system_station) {
			systemNotificationM.set("system_status", 1).update();
		}
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn, 5);
			String sql = "select s.*,u.add_time u_add_time from sys_notification s,user_info u where s.user_id = u.id and user_id = ? order by s.id desc";
			List<Record> station_list = Db.find(page.creatLimitSql(sql), new Object[] { user.getInt("id") });
			setAttribute("station_list", station_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/usercenter/station_information_details");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/usercenter/station_information_details.jsp");
		return;
	}

	/**
	 * 保存左边导航选中的ID和父级ID
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAjax.class, Ajax.class })
	@Function(for_people = "登陆后的所有用户", function_description = "保存左边导航选中的ID和父级ID", last_update_author = "hx")
	public void save_or_update_dump_path() {
		String currentObjectId = getParameter("currentObjectId");
		String parentObjectId = getParameter("parentObjectId");
		// System.out.println("------currentObjectId--------" +
		// currentObjectId);
		// System.out.println("------parentObjectId--------" + parentObjectId);
		if (currentObjectId != null && parentObjectId != null && !"".equals(currentObjectId) && !"".equals(parentObjectId)) {
			setSessionAttribute("curr_id", currentObjectId);
			setSessionAttribute("pare_id", parentObjectId);
			renderText("1");
		} else {
			renderText("2");
		}
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登录", function_description = "跳转到意见反馈", last_update_author = "向旋")
	public void to_user_suggest() {
		renderJsp("/user/usercenter/account_suggest.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "登录", function_description = "用户提交意见反馈", last_update_author = "向旋")
	public void save_suggest() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		if (user == null) {
			renderText("7");
			return;
		}
		int type = getParameterToInt("type", 0);
		if (type == 0) {
			renderText("1");
			return;
		}
		String content = getParameter("content");
		if (!Utils.isNotNullAndNotEmptyString(content) || content.length() < 10 || content.length() > 500) {
			renderText("2");
			return;
		}
		if (RiskParameterFilter.validateXss(content)) {
			renderText("8");
			return;
		}
		String verification_code = getParameter("verification_code");
		if (!Utils.isNotNullAndNotEmptyString(verification_code)) {
			renderText("3");
			return;
		}
		String session_verification = getSessionAttribute(CheckCodeServlet.Code);
		if (!verification_code.equals(session_verification)) {
			renderText("4");
			return;
		}
		UserSuggestM user_suggest = new UserSuggestM();
		boolean save_suggest = user_suggest.set("user_id", user.getInt("id")).set("is_read", 1).set("type", type).set("content", content).set("add_time", new Date()).save();
		if (!save_suggest) {
			renderText("5");
			return;
		}
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAjax.class, Ajax.class })
	@Function(for_people = "登陆后用户可见", function_description = "资金面板中用户可用资金", last_update_author = "hx")
	public void get_user_acount_can_used_cny() {
		UserM userM = getSessionAttribute("user");
		Integer user_id = userM.getInt("id");
		String user_can_used_sql = "select t.cny_can_used as cny_can_used,t.cny_freeze as cny_freeze from user_now_money t where t.user_id = ?";
		UserNowMoneyM user_now_money = UserNowMoneyM.dao.findFirst(user_can_used_sql, new Object[] { user_id });
		// BigDecimal cny_can_used = Db.queryBigDecimal(user_can_used_sql, new
		// Object[]{user_id});
		if (user_now_money != null) {
			BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
			BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

			BigDecimal acount_balance = cny_can_used.add(cny_freeze).setScale(2, BigDecimal.ROUND_DOWN);
			Gson gson = new Gson();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("acount_balance", acount_balance);
			renderText(gson.toJson(map));
			return;
		} else {
			renderText("");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAjax.class, Ajax.class })
	@Function(for_people = "登陆后用户可见", function_description = "上传头像", last_update_author = "zjb")
	public void upload_portrait() throws Exception {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		CosUpload cosupload = new CosUpload(getRequest(), null, new String[] { ".jpg", ".jpeg", ".bmp", ".png" }, new String[] {});
		List<UploadFile> upload_file_list = cosupload.getFiles();

		if (!Utils.isHasData(upload_file_list)) {
			cosupload.deleteFiles();
			renderText("1");// 上传不成功
			return;
		}

		UploadFile upload_file = upload_file_list.get(0);
		File file = upload_file.getFile();

		if (file.length() > 1572864) {
			cosupload.deleteFiles();
			renderText("0");// 图片超出限制大小
			return;

		}

		String path = WebApp.Portrait_Files_Stored_Palce;

		File f = new File(path);
		if (!f.exists()) {
			f.mkdirs();
		}

		String file_type = GetTypeByHead.getFileType(file);

		if (Image.Image_Type_Set.contains(file_type)) {
			String image_name = GetTypeByHead.Image.getImageFormatName(file);
			if (image_name == null) {
				cosupload.deleteFiles();
				renderText("1");// 不是图片
				return;
			}
		} else {
			cosupload.deleteFiles();
			renderText("2");// 上传失败
			return;
		}

		File fol = new File(f, "portrait-" + user_id + '.' + file_type);
		String portrait_url = user.getString("url");
		if (!Utils.isNotNullAndNotEmptyString(portrait_url)) {
			fol.createNewFile();
		} else {
			File old_file = new File(portrait_url);// 删除原来文件
			old_file.delete();
		}

		FileUtils.copyFile(file, fol);

		String url_save = fol.getName();

		boolean user_info_ok = user.set("url", "/" + url_save).update();
		cosupload.deleteFiles();

		if (user_info_ok) {
			setSessionAttribute("user", null);
			setSessionAttribute("user", user);
			renderText("3");// 上传成功
			return;
		} else {
			renderText("2");// 上传失败
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAjax.class, Ajax.class })
	@Function(for_people = "登陆后用户可见", function_description = "保存用户基本信息", last_update_author = "zjb")
	public void save_basic_info() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String education = getParameter("education");
		String school = getParameter("school");
		String marriage = getParameter("marriage");
		String address = getParameter("address");
		String company_trades = getParameter("company_trades");
		String company_size = getParameter("company_size");
		String position = getParameter("position");
		String monthly_income = getParameter("monthly_income");

		String[] arr = { education, school, marriage, address, company_trades, company_size, position, monthly_income };
		for (int i = 0; i < arr.length; i++) {
			if (Utils.isNotNullAndNotEmptyString(arr[i])) {
				renderText(i + "");
				return;
			}
		}
		UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findById(user_id);
		UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findById(user_id);
		UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findById(user_id);

		boolean persional_ok = user_authenticate_persional_info.set("education", education).set("school", school).set("address", address).update();
		boolean family_ok = user_authenticate_family_info.set("marriage", marriage).update();
		boolean work_ok = user_authenticate_work_info.set("company_trades", company_trades).set("company_size", company_size).set("position", position).set("monthly_income", monthly_income).update();

		if (persional_ok && family_ok && work_ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}
	}

}

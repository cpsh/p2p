package com.tfoll.web.action.user.financial;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.model.BorrowerBulkStandardOrderUserInfoM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldLogM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightHoldValueChangePushM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferInM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutWaitingM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.DateUtil;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.AjaxAsyncPageDiv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@ActionKey("/user/financial/transfer")
public class DebtTransferAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "确定债权转让", last_update_author = "lh")
	public void confirm_debt_transfer() {
		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id"); // 债权转出人id
		final long creditor_right_hold_id = getParameterToLong("creditor_right_hold_id");// 债权持有表的id
		final long gather_money_order_id = getParameterToLong("gather_money_order_id");// 债权id（筹款单id）
		final long gather_money_order_id_of_final = gather_money_order_id;// 债权id（筹款单id）
		/**
		 * 查询凑集单凑集满的时间
		 */
		Date finish_time = Db.queryTimestamp("SELECT  finish_time from borrower_bulk_standard_gather_money_order WHERE id=?", new Object[] { gather_money_order_id_of_final });
		Date date_fater_finish_time_3_month = DateUtil.getDateAfterN(finish_time, 3);
		if ((new Date()).before(date_fater_finish_time_3_month)) {
			renderText("非法操作");// 判断当前转出的操作是否大于了三个月
			return;
		}

		final BigDecimal creditor_right_value = new BigDecimal(getParameterToDouble("creditor_right_value")); // 债权价值
		final BigDecimal transfer_factor = new BigDecimal(getParameterToDouble("transfer_factor"));// 转让系数
		final BigDecimal transfer_price = new BigDecimal(getParameterToDouble("transfer_price"));// 转让价格(每份)
		final int transfer_share = getParameterToInt("transfer_share"); // 转让份数
		// 转让总价格
		final BigDecimal transfer_total_value = transfer_price.multiply(new BigDecimal(transfer_share + ""));
		final BigDecimal transfer_manage_fee = new BigDecimal(getParameterToDouble("transfer_manage_fee"));

		final BigDecimal transfer_money = new BigDecimal(50).multiply(new BigDecimal(transfer_share + ""));// ？？
		final AtomicReference<String> error_code = new AtomicReference<String>();

		String money_password_page = MD5.md5(getParameter("money_password"));
		String money_password = user.getString("money_password");
		if (money_password == "" || money_password == null) {
			renderText("7");// 请先设置资金密码
			return;
		}
		if (!money_password.equals(money_password_page)) {
			renderText("8");// 资金密码不正确
			return;
		}

		/**
		 * 根据gather_money_order_id去数据库查询
		 */

		// 往债权转出表中添加记录

		boolean is_Ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				// 债权持有表，减少持有金额、持有分数 增加转出份额（冻结）（持有金额这个概念不存在了吧）
				LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold = LenderBulkStandardCreditorRightHoldM.dao.findById(creditor_right_hold_id);
				if (lender_bulk_standard_creditor_right_hold == null) {
					return false;
				}
				long gather_money_order_id = lender_bulk_standard_creditor_right_hold.getLong("gather_money_order_id");
				if (gather_money_order_id != gather_money_order_id_of_final) {
					error_code.set("非法操作"); // 非法操作
					return false;
				}

				@SuppressWarnings("unused")
				BigDecimal hold_money_before = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
				int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 持有份数
				if (hold_share <= 0) {
					error_code.set("没有债权可以转让了"); // 没有债权可以转让了
					return false;
				}
				int transfer_share_before = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 原来的冻结份数
				int can_transfer_share = hold_share - transfer_share_before; // 可转让份数

				if (transfer_share > can_transfer_share) { // 如果转让份数超过可转让份数
					error_code.set("2"); // 转让份数过大
					return false;
				}

				int transfer_share_later = transfer_share_before + transfer_share;
				BigDecimal transfer_money_later = new BigDecimal(transfer_share_later + "").multiply(new BigDecimal(50 + ""));

				// int hold_share_later = hold_share - transfer_share ;

				// lender_bulk_standard_creditor_right_hold.set("hold_money",hold_money_later);
				// lender_bulk_standard_creditor_right_hold.set("hold_share",hold_share_later);
				lender_bulk_standard_creditor_right_hold.set("transfer_money", transfer_money_later);// 转出金额是什么
				lender_bulk_standard_creditor_right_hold.set("transfer_share", transfer_share_later);

				boolean ok_lender_bulk_standard_creditor_right_hold_update = lender_bulk_standard_creditor_right_hold.update();

				// 在债权转出表中添加记录
				LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out = new LenderBulkStandardOrderCreditorRightTransferOutM();

				lender_bulk_standard_order_creditor_right_transfer_out.set("gather_money_order_id", gather_money_order_id);
				lender_bulk_standard_order_creditor_right_transfer_out.set("creditor_right_hold_id", creditor_right_hold_id);
				lender_bulk_standard_order_creditor_right_transfer_out.set("creditor_right_hold_user_id", user_id);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_share", transfer_share);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_share_has", 0);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_share_remainder", transfer_share);
				lender_bulk_standard_order_creditor_right_transfer_out.set("creditor_right_value", creditor_right_value); // 债权价值
				// lender_bulk_standard_order_creditor_right_transfer_out.set("current_creditor_right_value",
				// creditor_right_value); //
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_factor", transfer_factor);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_price", transfer_price);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_total_value", transfer_total_value);
				// lender_bulk_standard_order_creditor_right_transfer_out.set("current_transfer_total_value",
				// transfer_total_value);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_manage_fee", transfer_manage_fee);
				lender_bulk_standard_order_creditor_right_transfer_out.set("state", 1);
				lender_bulk_standard_order_creditor_right_transfer_out.set("add_time", new Date());

				boolean ok_lender_bulk_standard_order_creditor_right_transfer_out_save = lender_bulk_standard_order_creditor_right_transfer_out.save();

				LenderBulkStandardOrderCreditorRightTransferOutWaitingM lender_bulk_standard_order_creditor_right_transfer_out_waiting = new LenderBulkStandardOrderCreditorRightTransferOutWaitingM();
				lender_bulk_standard_order_creditor_right_transfer_out_waiting.set("creditor_right_transfer_out_id", lender_bulk_standard_order_creditor_right_transfer_out.getLong("id"));
				lender_bulk_standard_order_creditor_right_transfer_out_waiting.set("deal_level", 1);

				boolean ok_lender_bulk_standard_order_creditor_right_transfer_out_waiting_save = lender_bulk_standard_order_creditor_right_transfer_out_waiting.save();

				return ok_lender_bulk_standard_creditor_right_hold_update && ok_lender_bulk_standard_order_creditor_right_transfer_out_save && ok_lender_bulk_standard_order_creditor_right_transfer_out_waiting_save;
			}
		});

		if (is_Ok) {
			renderText("1");// 挂出转让单成功
			return;
		} else {
			// 挂出转出单失败
			renderText(error_code.get());
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "获取债权转让列表", last_update_author = "lh")
	public void get_debt_transfer_list() {

		int pn = getParameterToInt("pn", 1);

		StringBuilder sql_count = new StringBuilder("select count(1) from lender_bulk_standard_order_creditor_right_transfer_out where state != 3");
		StringBuilder sql_list = new StringBuilder("select * from lender_bulk_standard_order_creditor_right_transfer_out where state != 3 ");

		List<Object> params = new ArrayList<Object>();
		sql_list.append(" order by state,add_time limit ?,? ");

		Long total_row = Db.queryLong(sql_count.toString(), params.toArray());
		Page page = new Page(total_row, pn, 5);
		params.add(page.getBeginIndex());
		params.add(page.getEndIndex());

		List<Record> record_list = Db.find(sql_list.toString(), params.toArray());
		List<Map<String, Object>> creditor_right_transfer_out_list = new ArrayList<Map<String, Object>>();
		/*
		 * 借款标题 信用等级 年利率 剩余期限 每份价值 剩余份额 转让系数 转出价格
		 */

		for (Record record : record_list) {
			long gather_money_order_id = record.getLong("gather_money_order_id");
			String sql_gather_order = "select id as gather_order_id, borrow_title, credit_rating, annulized_rate_int, borrow_type from borrower_bulk_standard_gather_money_order where id = ? ";
			Record gather_order = Db.findFirst(sql_gather_order, new Object[] { gather_money_order_id });
			long gather_order_id = gather_order.getLong("gather_order_id");
			String borrow_title = gather_order.getString("borrow_title");
			String credit_rating = gather_order.getString("credit_rating");
			int annulized_rate_int = gather_order.getInt("annulized_rate_int");
			int borrow_type = gather_order.getInt("borrow_type");

			record.add("gather_order_id", gather_order_id);
			record.add("borrow_title", borrow_title);
			record.add("credit_rating", credit_rating);
			record.add("annulized_rate_int", annulized_rate_int);
			record.add("borrow_type", borrow_type);

			String sql_repayment_plan = "select current_period, total_periods from borrower_bulk_standard_repayment_plan where gather_money_order_id = ? and ? < repay_end_time and is_repay = 0 order by current_period asc limit 1 ";
			Record repayment_plan = Db.findFirst(sql_repayment_plan, new Object[] { gather_money_order_id, new Date() });
			int current_period = repayment_plan.getInt("current_period");
			int total_periods = repayment_plan.getInt("total_periods");
			int remain_period = total_periods - current_period + 1;

			record.add("remain_period", remain_period);

			creditor_right_transfer_out_list.add(record.getR());

		}

		Gson gson = new Gson();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("creditor_right_transfer_out_list", creditor_right_transfer_out_list);
		String asyncPageDiv = AjaxAsyncPageDiv.getDiv(page.getTotalPageNum(), page.getPageSize(), page.getCurrentPageNum());
		map.put("asyncPageDiv", asyncPageDiv);

		String str = gson.toJson(map);
		renderText(str);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入债权转让详情", last_update_author = "lh")
	public void to_debt_transger_detail() {

		/*
		 * 转让单价 （元/份） 8.54 转让系数 100.0% 剩余份数 是否转让完成
		 * 
		 * 年利率 11.00% 原标的ID 244816 借款标题
		 * 
		 * 待收本息 8.62元/份
		 * 
		 * 剩余期限 （月） 1/6 下一还款日 2015-02-11 逾期情况 未发生过逾期
		 * 
		 * 提前还款费率 1.00% 还款方式 按月还款/等额本息
		 */
		UserM user = getSessionAttribute("user");
		BigDecimal cny_can_used = new BigDecimal("0.00");
		if (user != null) {
			int user_id = user.getInt("id");
			UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
			cny_can_used = user_now_money.getBigDecimal("cny_can_used");
		}
		cny_can_used = cny_can_used.setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("cny_can_used", cny_can_used);

		int transfer_out_order_id = getParameterToInt("transfer_out_order_id", 0);
		if (transfer_out_order_id == 0) {
			renderText("");
			return;
		}

		String sql = "select * from lender_bulk_standard_order_creditor_right_transfer_out where id = ? ";
		Record debt_detail_record = Db.findFirst(sql, new Object[] { transfer_out_order_id });
		int state = debt_detail_record.getInt("state");
		BigDecimal transfer_price = debt_detail_record.getBigDecimal("transfer_price").setScale(2, BigDecimal.ROUND_DOWN);// 转让价格
		int transfer_share_remainder = debt_detail_record.getInt("transfer_share_remainder");// 剩余份数

		BigDecimal remain_money = (new BigDecimal(transfer_share_remainder + "").multiply(transfer_price)).setScale(2, BigDecimal.ROUND_DOWN); // 剩余价钱
		debt_detail_record.add("remain_money", remain_money);

		// 算出最多可购买几份？
		int can_buy_share = cny_can_used.divide(transfer_price, 10, BigDecimal.ROUND_DOWN).intValue();
		can_buy_share = can_buy_share > transfer_share_remainder ? transfer_share_remainder : can_buy_share;
		setAttribute("can_buy_share", can_buy_share);

		long gather_order_id = debt_detail_record.getLong("gather_money_order_id");

		// 算出 每份的 待收本息
		BigDecimal weihuan_benxi_total_per_share = (BigDecimal) CommonRepayMethod.get_repayment_info(gather_order_id).get("weihuan_benxi_total_per_share");
		debt_detail_record.add("weihuan_benxi_total_per_share", weihuan_benxi_total_per_share);

		String sql_gather_order = "select annulized_rate_int,borrow_title,borrow_type from borrower_bulk_standard_gather_money_order where id = ?";
		Record gather_order_record = Db.findFirst(sql_gather_order, new Object[] { gather_order_id });
		int annulized_rate_int = gather_order_record.getInt("annulized_rate_int");
		String borrow_title = gather_order_record.getString("borrow_title");
		int borrow_type = gather_order_record.getInt("borrow_type");

		debt_detail_record.add("gather_order_id", gather_order_id);
		debt_detail_record.add("annulized_rate_int", annulized_rate_int);
		debt_detail_record.add("borrow_title", borrow_title);
		debt_detail_record.add("borrow_type", borrow_type);

		String sql_repayment_plan = "select current_period, total_periods, automatic_repayment_date from borrower_bulk_standard_repayment_plan where gather_money_order_id = ? and ? < repay_end_time_long order by current_period asc limit 1 ";
		Record repayment_plan = Db.findFirst(sql_repayment_plan, new Object[] { gather_order_id, System.currentTimeMillis() });
		int current_period = repayment_plan.getInt("current_period");//		
		int total_periods = repayment_plan.getInt("total_periods");
		int remain_period = total_periods - current_period + 1;
		String automatic_repayment_date = repayment_plan.getString("automatic_repayment_date");

		debt_detail_record.add("current_period", current_period);
		debt_detail_record.add("total_periods", total_periods);
		debt_detail_record.add("remain_period", remain_period);
		debt_detail_record.add("automatic_repayment_date", automatic_repayment_date);

		// 借款人的用户id
		int user_id = Db.queryInt("select user_id from borrower_bulk_standard_gather_money_order where id = ( select gather_money_order_id from lender_bulk_standard_order_creditor_right_transfer_out where id = ? )", new Object[] { transfer_out_order_id });

		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
		BorrowerBulkStandardOrderUserInfoM borrower_bulk_standard_order_user_info = BorrowerBulkStandardOrderUserInfoM.get_last_order_user_info(user_id);

		setAttribute("user_credit_files", user_credit_files);
		setAttribute("borrower_bulk_standard_order_user_info", borrower_bulk_standard_order_user_info);

		if (state == 1) {// 正在转让中
			setAttribute("debt_detail_record", debt_detail_record);
			renderJsp("/user/financial/debt_transfer_detial_doing.jsp");
			return;
		} else if (state == 2) {// 转让完成

			Date add_time = debt_detail_record.getTimestamp("add_time");
			Date finish_time = debt_detail_record.getTimestamp("finish_time");

			long spend_time = finish_time.getTime() - add_time.getTime();
			long day = spend_time / Day;
			long hour = (spend_time % Day) / Hour;
			long minute = (spend_time % Hour) / Minute;
			long second = (spend_time % Minute) / Second;

			String spend_time_str;
			if (day != 0) {
				spend_time_str = day + "天" + hour + "时" + minute + "分" + second + "秒";
			} else if (day == 0 && hour != 0) {
				spend_time_str = hour + "时" + minute + "分" + second + "秒";
			} else if (day == 0 && hour == 0 && minute != 0) {
				spend_time_str = minute + "分" + second + "秒";
			} else {
				spend_time_str = second + "秒";
			}
			debt_detail_record.add("spend_time_str", spend_time_str);
			setAttribute("debt_detail_record", debt_detail_record);
			renderJsp("/user/financial/debt_transfer_detial_done.jsp");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "所有人", function_description = "确定买入债权", last_update_author = "lh")
	public void confirm_buy_debt() {

		/*
		 * 要操作3张业务表 用户资金表 还有财务记录表 用户信息标记为理财
		 */
		final UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id"); // 债权买入人的id

		final long transfer_out_order_id = getParameterToLong("transfer_out_order_id"); // 转出单表的id
		/**
		 * 判断现在是否正在进行汇款后价值改变
		 */
		List<LenderBulkStandardOrderCreditorRightHoldValueChangePushM> lender_bulk_standard_order_creditor_right_hold_value_change_push_list = LenderBulkStandardOrderCreditorRightHoldValueChangePushM.dao.find("SELECT failure_time from lender_bulk_standard_order_creditor_right_hold_value_change_push WHERE creditor_right_transfer_out_id=?", new Object[] { transfer_out_order_id });
		if (Utils.isHasData(lender_bulk_standard_order_creditor_right_hold_value_change_push_list)) {
			for (LenderBulkStandardOrderCreditorRightHoldValueChangePushM lender_bulk_standard_order_creditor_right_hold_value_change_push : lender_bulk_standard_order_creditor_right_hold_value_change_push_list) {
				Date failure_time = lender_bulk_standard_order_creditor_right_hold_value_change_push.getDate("failure_time");
				if ((new Date()).before(new Date(failure_time.getTime() + Model.Second * 5))) {
					renderText("系统正在对该用户的理财进行回款，债权价值改变中");
					return;
				}
			}
		}

		double transfer_price_double = getParameterToDouble("transfer_price");// 转让价格
		final BigDecimal transfer_price = new BigDecimal(transfer_price_double + "");
		final int buy_share = getParameterToInt("buy_share"); // 要购买的份数
		double invest_money_double = getParameterToDouble("invest_money"); // 花费的钱
		// double转成BigDecimal
		BigDecimal invest_money = new BigDecimal(invest_money_double + ""); // 

		UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
		BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");

		if (invest_money.compareTo(cny_can_used) > 0) { // 如果要花的钱大于可用的钱
			renderText("1"); // 你的余额不足
			return;
		}
		final AtomicReference<String> error_code = new AtomicReference<String>();
		boolean is_Ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				/* 处理转出表 增加已转出份数 减少剩余份数 （要锁表） */
				LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out = LenderBulkStandardOrderCreditorRightTransferOutM.get_lender_bulk_standard_order_creditor_right_transfer_out_short_wait(transfer_out_order_id);
				int transfer_out_user_id = lender_bulk_standard_order_creditor_right_transfer_out.getInt("creditor_right_hold_user_id"); // 债权转出人id
				long creditor_right_hold_id = lender_bulk_standard_order_creditor_right_transfer_out.getLong("creditor_right_hold_id");// 债权持有表id
				long gather_order_id = lender_bulk_standard_order_creditor_right_transfer_out.getLong("gather_money_order_id"); // 筹款单id

				int borrower_user_id = Db.queryInt("select user_id from borrower_bulk_standard_gather_money_order where id = ? ", new Object[] { gather_order_id });

				if (user_id == transfer_out_user_id) {
					/**
					 * 需要判断第一理财人和第二理财人是不是同一个人
					 */
					error_code.set("不能买自己转出的债权"); // 不能购买自己转出的债权-外面提示程序出错
					return false;
				}

				if (user_id == borrower_user_id) {
					error_code.set("不能买自己申请借款的债权"); // 债权的借款人不能购买
					return false;
				}

				int transfer_share_has_before = lender_bulk_standard_order_creditor_right_transfer_out.getInt("transfer_share_has");// 已转出份数
				int transfer_share_remainder_before = lender_bulk_standard_order_creditor_right_transfer_out.getInt("transfer_share_remainder");// 剩余的份数
				if (transfer_share_remainder_before <= 0) {
					error_code.set("剩余债权份数为0");
					return false;
				}
				int transfer_share_has_later = transfer_share_has_before + buy_share;// 已转出份数增加
				int transfer_share_remainder_later = transfer_share_remainder_before - buy_share; // 剩余份数
				// 减少
				/**
				 * 判读用户转入的是否足够
				 */
				if (transfer_share_remainder_later < 0) {
					error_code.set("2");// 改债权的剩余份数不足，请减少购买份数或者购买其它债权
					return false;
				}

				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_share_has", transfer_share_has_later);
				lender_bulk_standard_order_creditor_right_transfer_out.set("transfer_share_remainder", transfer_share_remainder_later);
				/**
				 * 判断是否全部已经转让完毕了
				 */
				if (transfer_share_remainder_later == 0) {
					lender_bulk_standard_order_creditor_right_transfer_out.set("state", 2);// 转让完成
					lender_bulk_standard_order_creditor_right_transfer_out.set("finish_time", new Date());
				}

				boolean ok_lender_bulk_standard_order_creditor_right_transfer_out_update = lender_bulk_standard_order_creditor_right_transfer_out.update();
				if (!ok_lender_bulk_standard_order_creditor_right_transfer_out_update) {
					return false;
				}
				/*
				 * 处理债权持有表 （只能操作对应的债权id对应的记录）
				 * 
				 * 1.转出人的持有份数和冻结份数都减少 2.添加或更新转入人的记录
				 */
				// 处理转出人的债权持有表记录
				LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold_out = LenderBulkStandardCreditorRightHoldM.dao.findById(creditor_right_hold_id);
				int hold_share_before_out = lender_bulk_standard_creditor_right_hold_out.getInt("hold_share");// 原来的持有份数
				int transfer_share_before_out = lender_bulk_standard_creditor_right_hold_out.getInt("transfer_share");

				int hold_share_later = hold_share_before_out - buy_share;
				int transfer_share_later = transfer_share_before_out - buy_share;
				if (transfer_share_later < 0 || hold_share_later < 0) {
					error_code.set("3"); // 程序出错
					return false;
				}
				BigDecimal hold_money_later = new BigDecimal(hold_share_later).multiply(new BigDecimal(50));// useless
				BigDecimal transfer_money_later = new BigDecimal(transfer_share_later).multiply(new BigDecimal(50));// useless

				lender_bulk_standard_creditor_right_hold_out.set("hold_share", hold_share_later);
				lender_bulk_standard_creditor_right_hold_out.set("transfer_share", transfer_share_later);
				lender_bulk_standard_creditor_right_hold_out.set("hold_money", hold_money_later);
				lender_bulk_standard_creditor_right_hold_out.set("transfer_money", transfer_money_later);
				/**
				 * 该字段不再使用-删除掉-如果使用这个字段业务会出现错误的
				 * 
				 * <pre>
				 * if (hold_share_later == 0) {
				 * 	lender_bulk_standard_creditor_right_hold_out.set(&quot;is_transfer_all&quot;, 1);
				 * }
				 * </pre>
				 */
				boolean ok_lender_bulk_standard_creditor_right_hold_out_update = lender_bulk_standard_creditor_right_hold_out.update();
				if (!ok_lender_bulk_standard_creditor_right_hold_out_update) {
					return false;
				}

				/* 处理转入人债权持有表 */
				String sql = "select * from lender_bulk_standard_creditor_right_hold where user_id = ? and gather_money_order_id = ? ";
				LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold_in = LenderBulkStandardCreditorRightHoldM.dao.findFirst(sql, new Object[] { user_id, gather_order_id });
				boolean ok_lender_bulk_standard_creditor_right_hold_in_save_or_update;
				// 定义lender_bulk_standard_creditor_right_hold_log_in中持有人hold_share和hold_money
				int hold_share_for_hold_log_in = 0;
				BigDecimal hold_money_for_hold_log_in = new BigDecimal("0");
				if (lender_bulk_standard_creditor_right_hold_in == null) { // 如果之前没有持有过该债权id对应的债权，则添加一条记录
					lender_bulk_standard_creditor_right_hold_in = new LenderBulkStandardCreditorRightHoldM();
					lender_bulk_standard_creditor_right_hold_in.set("user_id", user_id);
					lender_bulk_standard_creditor_right_hold_in.set("gather_money_order_id", gather_order_id);
					lender_bulk_standard_creditor_right_hold_in.set("gather_money_order_user_id_borrower", borrower_user_id);// 借款用户id
					//
					lender_bulk_standard_creditor_right_hold_in.set("hold_money", buy_share * 50);// useless
					lender_bulk_standard_creditor_right_hold_in.set("hold_share", buy_share);
					{
						hold_share_for_hold_log_in = buy_share;
						hold_money_for_hold_log_in = new BigDecimal(buy_share * 50);
					}
					lender_bulk_standard_creditor_right_hold_in.set("transfer_money", 0);// useless
					lender_bulk_standard_creditor_right_hold_in.set("transfer_share", 0);
					// lender_bulk_standard_creditor_right_hold_in.set("is_transfer_all",
					// 0);
					lender_bulk_standard_creditor_right_hold_in.set("add_time", new Date());
					lender_bulk_standard_creditor_right_hold_in.set("add_time_long", System.currentTimeMillis());
					lender_bulk_standard_creditor_right_hold_in.set("is_need_handle", 1);

					ok_lender_bulk_standard_creditor_right_hold_in_save_or_update = lender_bulk_standard_creditor_right_hold_in.save();
				} else {
					int hold_share_before_in = lender_bulk_standard_creditor_right_hold_in.getInt("hold_share");
					int hold_share_later_in = hold_share_before_in + buy_share;

					lender_bulk_standard_creditor_right_hold_in.set("hold_share", hold_share_later_in);
					lender_bulk_standard_creditor_right_hold_in.set("hold_money", hold_share_later_in * 50);
					{

						hold_share_for_hold_log_in = hold_share_later_in;
						hold_money_for_hold_log_in = new BigDecimal(hold_share_later_in * 50);
					}

					ok_lender_bulk_standard_creditor_right_hold_in_save_or_update = lender_bulk_standard_creditor_right_hold_in.update();
				}
				if (!ok_lender_bulk_standard_creditor_right_hold_in_save_or_update) {
					return false;
				}
				// 持有双方建立持有变化记录
				// 转出方
				LenderBulkStandardCreditorRightHoldLogM lender_bulk_standard_creditor_right_hold_log_out = new LenderBulkStandardCreditorRightHoldLogM();
				/**
				 * 从转出方的持有表里面查询出变化之前的持有信息,加上更改的那部分,得到更改后的持有信息(-)
				 */

				/**
				 * <pre>
				 * CREATE TABLE `lender_bulk_standard_creditor_right_hold_log` (
				 * `gather_money_order_id` bigint(20) DEFAULT NULL COMMENT '凑集单ID-该ID用来进行还款',
				 * `gather_money_order_user_id_borrower` int(11) DEFAULT NULL COMMENT 'gather_money_order_id对应的借款用户ID',
				 * `id` bigint(20) NOT NULL AUTO_INCREMENT,
				 * `user_id` int(20) DEFAULT NULL COMMENT '持有者id',
				 * `creditor_right_way` int(11) DEFAULT '0' COMMENT '债权来源方式 0.购买精英标获得 1.转入债权,2转出债权',
				 * `order_id` bigint(20) DEFAULT '0' COMMENT '理财人订单表-with凑集单表',
				 * `gather_money_order_user_id_lender` int(20) DEFAULT '0' COMMENT '这个字段不再使用-以后会删除',
				 * `creditor_right_hold_from_id` bigint(20) DEFAULT '0' COMMENT '债权来源id（本表的id）,如果是直接从散标理财得来的默认是0,通过债权转让则不是0',
				 * `creditor_right_hold_from_user_id` int(11) DEFAULT '0' COMMENT '上一个债权持有用户ID-如果通过理财投标来的那个叫借贷用户的ID0，如果是债权转让那么就是上一个债权转让者的ID不为0',
				 * `hold_money` decimal(10,0) DEFAULT '0' COMMENT '持有金额',
				 * `hold_share` int(11) DEFAULT NULL COMMENT '持有份额',
				 * `hold_start_time` timestamp NULL DEFAULT NULL COMMENT '开始持有债权时间',
				 * `hold_start_time_long` bigint(20) DEFAULT NULL,
				 * `transfer_money` decimal(10,0) DEFAULT '0' COMMENT '转入转出金额 正数是转入负数是转出.通过散标投标的不涉及转入转出，通过债权转让的涉及转入转入转出',
				 * `transfer_share` int(11) DEFAULT NULL COMMENT '转入转出份额 正数是转入,负数是转出.通过散标投标的不涉及转入转出，通过债权转让的涉及转入+转出-',
				 * `is_transfer_all` int(255) DEFAULT '0' COMMENT '这个字段不使用-以后会删掉的',
				 * PRIMARY KEY (`id`)
				 * ) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COMMENT='精英标债权持有变化表';
				 * </pre>
				 */
				boolean save_lender_bulk_standard_creditor_right_hold_log_out_ok = lender_bulk_standard_creditor_right_hold_log_out.//
						set("gather_money_order_id", lender_bulk_standard_creditor_right_hold_out.getLong("gather_money_order_id")).//
						set("gather_money_order_user_id_borrower", lender_bulk_standard_creditor_right_hold_out.getInt("gather_money_order_user_id_borrower")).//
						// id
						set("user_id", lender_bulk_standard_creditor_right_hold_out.getInt("user_id")).//
						set("creditor_right_way", 2).// 转出
						set("order_id", 0).//
						// set("gather_money_order_user_id_lender",
						// "gather_money_order_user_id_lender").'这个字段不使用-以后会删掉的'
						set("creditor_right_hold_from_id", 0).//
						set("creditor_right_hold_from_user_id", 0).// 表示转出$的持有没有从其他的地方获得持有
						set("hold_money", hold_money_later).//
						set("hold_share", hold_share_later).//
						set("hold_start_time", new Date()).//
						set("hold_start_time_long", System.currentTimeMillis()).//
						set("transfer_money", new BigDecimal(buy_share * 50 * -1)).//
						set("transfer_share", buy_share * -1).//
						// set("is_transfer_all` int(255) DEFAULT '0' COMMENT
						// '这个字段不使用-以后会删掉的',
						save();
				if (!save_lender_bulk_standard_creditor_right_hold_log_out_ok) {
					return false;
				}

				// 转入方
				LenderBulkStandardCreditorRightHoldLogM lender_bulk_standard_creditor_right_hold_log_in = new LenderBulkStandardCreditorRightHoldLogM();
				boolean save_lender_bulk_standard_creditor_right_hold_log_in_ok = lender_bulk_standard_creditor_right_hold_log_in.//
						set("gather_money_order_id", lender_bulk_standard_creditor_right_hold_out.getLong("gather_money_order_id")).//
						set("gather_money_order_user_id_borrower", lender_bulk_standard_creditor_right_hold_out.getInt("gather_money_order_user_id_borrower")).//
						// id
						set("user_id", user_id).//
						set("creditor_right_way", 1).// 转入
						set("order_id", 0).//
						// set("gather_money_order_user_id_lender",
						// "gather_money_order_user_id_lender").'这个字段不使用-以后会删掉的'
						set("creditor_right_hold_from_id", lender_bulk_standard_creditor_right_hold_out.getLong("id")).//
						set("creditor_right_hold_from_user_id", lender_bulk_standard_creditor_right_hold_out.getInt("user_id")).// 表示转出
						set("hold_money", hold_money_for_hold_log_in).// ###见前面定义
						set("hold_share", hold_share_for_hold_log_in).// ###见前面定义
						set("hold_start_time", new Date()).//
						set("hold_start_time_long", System.currentTimeMillis()).//
						set("transfer_money", new BigDecimal(buy_share * 50)).//
						set("transfer_share", buy_share).//
						// set("is_transfer_all` int(255) DEFAULT '0' COMMENT
						// '这个字段不使用-以后会删掉的',
						save();
				if (!save_lender_bulk_standard_creditor_right_hold_log_in_ok) {
					return false;
				}

				/* 向买入债权表添加一条记录 */
				LenderBulkStandardOrderCreditorRightTransferInM lender_bulk_standard_order_creditor_right_transfer_in = new LenderBulkStandardOrderCreditorRightTransferInM();
				// 转入时的债权价值（元/份）
				BigDecimal transfer_debt_value = CalculationFormula.get_debt_value_per_share(gather_order_id);
				// 交易金额 就是用转入价格份数 * 转入份数 吗 (转入人应该支付的金额)
				BigDecimal trade_money = transfer_price.multiply(new BigDecimal(buy_share));
				// 转让费用 = 交易金额 * 0.5% (0.5%的转让费率)（系统的收入）
				BigDecimal transefer_fee = trade_money.multiply(new BigDecimal("0.005"));

				lender_bulk_standard_order_creditor_right_transfer_in.set("gather_money_order_id", gather_order_id); // 筹集单id
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_out_order_id", transfer_out_order_id);// 转出订单表id
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_out_user_id", transfer_out_user_id);// 债权转让人id
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_in_hold_id", lender_bulk_standard_creditor_right_hold_in.getLong("id"));// 债权持有表id
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_in_user_id", user_id); // 转入人id
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_debt_value", transfer_debt_value); // 转让时的债权价值
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_price", transfer_price); // 转入价格（元/份）
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_share", buy_share); // 转入份额

				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_all_money", trade_money); // 转出后收到的资金
				lender_bulk_standard_order_creditor_right_transfer_in.set("transfer_fee", transefer_fee); // 转出手续费

				BigDecimal should_count_interest = CalculationFormula.get_should_count_interest(gather_order_id);
				lender_bulk_standard_order_creditor_right_transfer_in.set("should_count_interest", should_count_interest);// 应计利息
				lender_bulk_standard_order_creditor_right_transfer_in.set("add_time", new Date());
				boolean is_ok_add_lender_bulk_standard_order_creditor_right_transfer_in = lender_bulk_standard_order_creditor_right_transfer_in.save();
				if (!is_ok_add_lender_bulk_standard_order_creditor_right_transfer_in) {
					return false;
				}

				long creditor_right_transfer_in_id = lender_bulk_standard_order_creditor_right_transfer_in.getLong("id");
				/**
				 * 转出人
				 */
				/* 转出人的资金表 */
				// 转出人应该收入
				BigDecimal transfer_out_user_recieve = trade_money.subtract(transefer_fee);

				UserNowMoneyM user_now_money_out = UserNowMoneyM.get_user_current_money_for_update(transfer_out_user_id);
				BigDecimal cny_can_used_out = user_now_money_out.getBigDecimal("cny_can_used");// 转出人原来的剩余可用资金
				BigDecimal cny_freeze_out = user_now_money_out.getBigDecimal("cny_freeze");// 转出人原来的冻结资金

				BigDecimal cny_can_used_out_later = cny_can_used_out.add(transfer_out_user_recieve); // 得到收入后的资金
				boolean ok_user_now_money_out_update = user_now_money_out.set("cny_can_used", cny_can_used_out_later).update();
				if (!ok_user_now_money_out_update) {
					return false;
				}
				boolean ok_add_sys_income_records_save = SysIncomeRecordsM.add_sys_income_records(user_id, UserMoneyChangeRecordsM.Type_73, transefer_fee, "债权转让手续费creditor_right_transfer_in_id:" + creditor_right_transfer_in_id);
				if (!ok_add_sys_income_records_save) {
					return false;
				}
				/* 转出人资金变化 */
				boolean ok_user_money_change_records_out_save = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(transfer_out_user_id, UserMoneyChangeRecordsM.Type_72, cny_can_used_out.add(cny_freeze_out), new BigDecimal(0 + ""), trade_money, cny_can_used_out.add(cny_freeze_out).add(trade_money), "债权转让所得资金#债权转入id" + creditor_right_transfer_in_id);
				if (!ok_user_money_change_records_out_save) {
					return false;
				}
				ok_user_money_change_records_out_save = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(transfer_out_user_id, UserMoneyChangeRecordsM.Type_73, cny_can_used_out.add(cny_freeze_out), transefer_fee, new BigDecimal(0 + ""), cny_can_used_out.add(cny_freeze_out).add(trade_money).subtract(transefer_fee), "债权转让手续费#债权转入id" + creditor_right_transfer_in_id);
				if (!ok_user_money_change_records_out_save) {
					return false;
				}

				/* 转出人用户交易记录 */
				boolean ok_user_transaction_records_out_save = UserTransactionRecordsM.add_user_transaction_records(transfer_out_user_id, UserTransactionRecordsM.Type_53, cny_can_used_out.add(cny_freeze_out), new BigDecimal(0 + ""), trade_money, cny_can_used_out.add(cny_freeze_out).add(trade_money), "债权转让所得资金");
				if (!ok_user_transaction_records_out_save) {
					return false;
				}
				ok_user_transaction_records_out_save = UserTransactionRecordsM.add_user_transaction_records(transfer_out_user_id, UserTransactionRecordsM.Type_51, cny_can_used_out.add(cny_freeze_out), transefer_fee, new BigDecimal(0 + ""), cny_can_used_out.add(cny_freeze_out).add(trade_money).subtract(transefer_fee), "债权转让手续费");
				if (!ok_user_transaction_records_out_save) {
					return false;
				}
				/**
				 * 转入人
				 */
				/* 转入人的资金表 */
				UserNowMoneyM user_now_money_in = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used_in = user_now_money_in.getBigDecimal("cny_can_used");// 转入人原来的剩余资金
				BigDecimal cny_freeze_in = user_now_money_in.getBigDecimal("cny_freeze");// 转入人原来的冻结资金

				boolean ok_user_now_money_in_update = user_now_money_in.set("cny_can_used", cny_can_used_in.subtract(trade_money)).update();
				if (!ok_user_now_money_in_update) {
					return false;
				}
				/* 转入人资金变化 */
				boolean ok_user_money_change_records_in_save = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_77, cny_can_used_in.add(cny_freeze_in), trade_money, new BigDecimal(0 + ""), cny_can_used_in.add(cny_freeze_in).subtract(trade_money), "购买债权支付金额#债权转入id" + creditor_right_transfer_in_id);
				if (!ok_user_money_change_records_in_save) {
					return false;
				}
				/* 转入人交易记录 */
				boolean ok_user_transaction_records_in_save = UserTransactionRecordsM.add_user_transaction_records(user_id, UserTransactionRecordsM.Type_52, cny_can_used_in.add(cny_freeze_in), trade_money, new BigDecimal(0 + ""), cny_can_used_in.add(cny_freeze_in).subtract(trade_money), "购买债权支付金额");
				if (!ok_user_transaction_records_in_save) {
					return false;
				}

				return true;
			}
		});
		if (is_Ok) {
			renderText("0");
			return;
		} else {
			System.out.println(error_code.get());
			renderText(error_code.get());
			return;
		}
	}
}

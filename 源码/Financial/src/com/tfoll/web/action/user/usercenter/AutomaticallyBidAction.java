package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.model.UserAutomaticallyBidM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.WebLogRecordsUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ActionKey("/user/usercenter/automatically_bid")
public class AutomaticallyBidAction extends Controller {
	/**
	 * <pre>
	 * CREATE TABLE `user_automatically_bid` (
	 * 			  `user_id` int(11) NOT NULL COMMENT '用户id',
	 * 			  `bid_amount` decimal(11,2) DEFAULT '0.00' COMMENT '每次投标金额-多了不让投',
	 * 			  `annulized_rate_min` decimal(10,2) DEFAULT '0.00' COMMENT '年化利率(最小)',
	 * 			  `annulized_rate_max` decimal(10,2) DEFAULT '0.00' COMMENT '年化利率(最大)',
	 * 			  `borrow_duration_min` int(2) DEFAULT '0' COMMENT '最小借款期限(月)',
	 * 			  `borrow_duration_max` int(2) DEFAULT '0' COMMENT '最大借款期限(月)',
	 * 			  `borrow_duration_day_min` int(2) DEFAULT '0' COMMENT '最小借款期限(天)',
	 * 			  `borrow_duration_day_max` int(2) DEFAULT '0' COMMENT '最大借款期限(天)',
	 * 			  `credit_rate_min` varchar(3) DEFAULT NULL COMMENT '信用等级范围（最小）',
	 * 			  `credit_rate_min_int` int(1) DEFAULT '0' COMMENT 'credit_rating A+=1,A=2,B+=3,B=4,C+=5,C=6',
	 * 			  `credit_rate_max` varchar(3) DEFAULT NULL COMMENT '信用等级范围（最大）',
	 * 			  `credit_rate_max_int` int(1) DEFAULT '0' COMMENT 'credit_rating A+=1,A=2,B+=3,B=4,C+=5,C=6',
	 * 			  `reserved_amount` decimal(11,2) DEFAULT NULL COMMENT '保留金额',
	 * 			  `disjunctor` int(1) DEFAULT '0' COMMENT '是否开启自动投标  0关闭，1 开启',
	 * 			  PRIMARY KEY (`user_id`)
	 * 			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='精英散表自动投标';
	 * </pre>
	 */

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "登录", function_description = "跳转到自动投标设置页面", last_update_author = "zjb")
	public void to_automatically_bid() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		UserAutomaticallyBidM user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);
		if (user_automatically_bid == null) {
			user_automatically_bid = new UserAutomaticallyBidM();
			/**
			 * 默认开启的设置
			 * 
			 * <pre>
			 * bid_amount = 200;
			 * annulized_rate_min = &quot;10&quot;;
			 * annulized_rate_max = &quot;24&quot;;
			 * borrow_duration_min = &quot;5天&quot;;
			 * borrow_duration_max = &quot;36月&quot;;
			 * credit_rate_min = &quot;C&quot;;
			 * credit_rate_max = &quot;A+&quot;;
			 * reserved_amount = 200;
			 * 
			 * </pre>
			 */
			boolean save_ok = user_automatically_bid.//
					set("user_id", user_id).//
					set("bid_amount", 200).//
					//
					set("annulized_rate_min", new BigDecimal("0.10")).//
					set("annulized_rate_max", new BigDecimal("0.24")).//

					set("borrow_duration_min", 0).//
					set("borrow_duration_max", 36).//
					set("borrow_duration_day_min", 5).//
					set("borrow_duration_day_max", 0).//
					//
					set("credit_rate_min", "C").//
					set("credit_rate_min_int", 6).//
					set("credit_rate_max", "A+").//
					set("credit_rate_max_int", 1).//
					//
					set("reserved_amount", new BigDecimal("200")).//
					set("disjunctor", 0).save();//
			user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);

			if (!save_ok) {
				renderText("进入页面系统自动创建自动投标失败");
				return;
			}
		}
		setAttribute("user_automatically_bid", user_automatically_bid);

		int disjunctor = user_automatically_bid.getInt("disjunctor");

		if (disjunctor == 0) {
			setAttribute("disjunctor", 0);// 关闭 状态
		} else if (disjunctor == 1) {
			setAttribute("disjunctor", 1);// 已 开启 状态
		} else {
			setAttribute("disjunctor", 2);// 帐号异常，请联系客服
		}

		BigDecimal cny_can_used = Db.queryBigDecimal("select cny_can_used from user_now_money where user_id = ?", user_id).setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("cny_can_used", cny_can_used);

		renderJsp("/user/usercenter/financial_manage/automatically_bid.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "登录", function_description = "开启/更新  自动投标设置", last_update_author = "zjb")
	public void open_automatically_bid() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		int borrow_type = user.getInt("borrow_type");
		if (!(borrow_type == 0 || borrow_type == 1 || borrow_type == 2 || borrow_type == 3)) {
			renderText("帐号异常，请联系客服");
			return;
		}
		if (borrow_type == 1 && borrow_type == 2) {
			renderText("您申请了借款，无法开启自动投标");
			return;
		}
		if (!(borrow_type == 0 || borrow_type == 3)) {
			renderText("只有理财用户和净值贷用户才能开启自动投标");
			return;
		}
		UserAutomaticallyBidM user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);
		if (user_automatically_bid == null) {
			renderText("非法操作!!!系统没有查询到你的自动投标记录");
			return;
		}
		int disjunctor = user_automatically_bid.getInt("disjunctor");
		if (!(disjunctor == 0 || disjunctor == 1)) {
			renderText("帐号异常，请联系客服");
			return;
		}

		int bid_amount_int = getParameterToInt("bid_amount", 0);// 投标金额
		//
		String annulized_rate_min_string = getParameter("annulized_rate_min");// 年化利率(最小)
		String annulized_rate_max_string = getParameter("annulized_rate_max");// 年化利率(最大)

		//
		// 需转为int
		String borrow_duration_min_string = getParameter("borrow_duration_min");// 借款期限范围(最小)
		// 需转为int
		String borrow_duration_max_string = getParameter("borrow_duration_max");// 借款期限范围(最大)
		//
		String credit_rate_min = getParameter("credit_rate_min");// 信用等级范围(最小)
		String credit_rate_max = getParameter("credit_rate_max");// 信用等级范围(最大)
		int reserved_amount_int = getParameterToInt("reserved_amount", 0);// 保留金额

		//
		int borrow_duration_day_min = 0;// 最小借款期限（天）
		int borrow_duration_day_max = 0;// 最大借款期限（天）
		int borrow_duration_min = 0;// 最小借款期限（月）
		int borrow_duration_max = 0;// 最大借款期限（月）
		// 投标金额
		if (bid_amount_int == 0) {
			// int max 2147483647 数量过大是指超过2147483647
			renderText("投标金额数量过大或者格式错误");
			return;
		}
		if (bid_amount_int < 200) {
			renderText("投标金额不能小于200");
			return;
		}
		if (bid_amount_int % 50 != 0) {
			renderText("投标金额必须为50倍数");
			return;
		}
		/**
		 * 单笔不能超过当次借款金额的20% 如果每次起投1万 那么*5则为5万
		 */
		if (bid_amount_int > 100000) {
			renderText("单笔投标金额不能超过十万");
			return;
		}
		BigDecimal bid_amount = new BigDecimal(bid_amount_int + "");

		// 年化利率
		Set<String> annulized_rate_set = new HashSet<String>();
		annulized_rate_set.add("10");
		annulized_rate_set.add("11");
		annulized_rate_set.add("12");
		annulized_rate_set.add("13");
		annulized_rate_set.add("14");
		annulized_rate_set.add("15");
		annulized_rate_set.add("16");
		annulized_rate_set.add("17");
		annulized_rate_set.add("18");
		annulized_rate_set.add("19");
		annulized_rate_set.add("20");
		annulized_rate_set.add("21");
		annulized_rate_set.add("22");
		annulized_rate_set.add("23");
		annulized_rate_set.add("24");

		if (!annulized_rate_set.contains(annulized_rate_min_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("10%-24%为有效利率范围");// 非法操作
			return;
		}
		if (!annulized_rate_set.contains(annulized_rate_max_string)) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("10%-24%为有效利率范围");// 非法操作
			return;
		}
		BigDecimal annulized_rate_min = new BigDecimal(annulized_rate_min_string).multiply(new BigDecimal("0.01"));// 最小年化利率
		BigDecimal annulized_rate_max = new BigDecimal(annulized_rate_max_string).multiply(new BigDecimal("0.01"));// 最大年化利率

		if (annulized_rate_min.compareTo(annulized_rate_max) > 0) {
			renderText("年化利率设置错误");
			return;
		}

		// 信用等级
		List<String> credit_rate_list = new ArrayList<String>();
		credit_rate_list.add("A+");
		credit_rate_list.add("A");
		credit_rate_list.add("B+");
		credit_rate_list.add("B");
		credit_rate_list.add("C+");
		credit_rate_list.add("C");
		if (!(credit_rate_list.contains(credit_rate_min))) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (!(credit_rate_list.contains(credit_rate_max))) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}

		// 最小信用等级 不可大于 最大信用等级
		int credit_rate_min_int = credit_rate_list.indexOf(credit_rate_min);
		// 越小的在list后面
		int credit_rate_max_int = credit_rate_list.indexOf(credit_rate_max);
		if (credit_rate_min_int < credit_rate_max_int) {
			renderText("最小信用等级 不可大于  最大信用等级");
			return;
		}
		// 期限
		ArrayList<String> duration_list = new ArrayList<String>();
		duration_list.add("5天");
		duration_list.add("10天");
		duration_list.add("15天");
		duration_list.add("20天");
		duration_list.add("25天");

		duration_list.add("1月");
		duration_list.add("3月");
		duration_list.add("6月");
		duration_list.add("9月");
		duration_list.add("12月");

		duration_list.add("15月");
		duration_list.add("18月");
		duration_list.add("24月");
		duration_list.add("36月");

		if (!(duration_list.contains(borrow_duration_min_string))) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}
		if (!(duration_list.contains(borrow_duration_max_string))) {
			logger.error("有黑客在尝试...ip" + WebLogRecordsUtil.getIpAddr(getRequest()));
			renderText("");// 非法操作
			return;
		}

		if (borrow_duration_min_string.contains("月")) {
			borrow_duration_min = Integer.valueOf((borrow_duration_min_string.replace("月", "")));
		} else if (borrow_duration_min_string.contains("天")) {
			borrow_duration_day_min = Integer.valueOf((borrow_duration_min_string.replace("天", "")));
		}

		if (borrow_duration_max_string.contains("月")) {
			borrow_duration_max = Integer.valueOf((borrow_duration_max_string.replace("月", "")));
		} else if (borrow_duration_max_string.contains("天")) {
			borrow_duration_day_max = Integer.valueOf((borrow_duration_max_string.replace("天", "")));
		}

		/**
		 * 最大和最小借款期限(月)等于0 且 最小 借款期限(天)大于最大借款期限(天) return
		 * 最大和最小借款期限(天)等于0且最小借款期限(月)大于最大借款期限(月) return 最小期限(天)等于0 且
		 * 最大期限(月)等于0(即最小期限为月，最大期限为天) return
		 */
		if ((borrow_duration_min == 0 && borrow_duration_max == 0 && borrow_duration_day_min > borrow_duration_day_max) // 当两个都是选择天数的时候，如果出现了前面月大于后面月则不行
				|| //
				(borrow_duration_day_min == 0 && borrow_duration_day_max == 0 && borrow_duration_min > borrow_duration_max) // 当两个都是选择月数的时候，如果出现了前面天大于后面天则不行
				|| //
				(borrow_duration_day_min == 0 && borrow_duration_max == 0)) {
			renderText("期限设置错误");
			return;
		}
		/**
		 * czh的白名单-不要删 和上面的ZJB的黑名单结果一致
		 * 
		 * <pre>
		 * boolean is_ok = false;
		 * if ((borrow_duration_day_min != 0)) {// 小的天不为0
		 * 	if (borrow_duration_day_max != 0) {// 大的天不为0
		 * 		if (borrow_duration_day_min &lt;= borrow_duration_day_max) {
		 * 			is_ok = true;
		 * 		} else {
		 * 			is_ok = false;
		 * 		}
		 * 
		 * 	} else {// borrow_duration_max!=0 // 大的月不为0
		 * 		is_ok = true;
		 * 	}
		 * 
		 * } else {// borrow_duration_min != 0 小的月不为0
		 * 	if (borrow_duration_day_max != 0) {// 大的天不为0
		 * 		is_ok = false;
		 * 	} else {// borrow_duration_max!=0 // 大的月不为0
		 * 		if (borrow_duration_min &lt;= borrow_duration_max) {
		 * 			is_ok = true;
		 * 		} else {
		 * 			is_ok = false;
		 * 		}
		 * 	}
		 * 
		 * }
		 * if (!is_ok) {
		 * 	renderText(&quot;期限设置错误&quot;);
		 * 	return;
		 * }
		 * </pre>
		 */

		BigDecimal cny_can_used = Db.queryBigDecimal("select cny_can_used from user_now_money where user_id = ?", user_id);
		if ((cny_can_used.compareTo(new BigDecimal("1000")) < 0)) {
			renderText("启用自动投标，账户余额不能小于1000!");
			return;
		}
		if (reserved_amount_int < 100) {
			renderText("保留金额不能小于100");
			return;
		}
		BigDecimal reserved_amount = new BigDecimal(reserved_amount_int + "");// 保留金额-只要大于等于0即可
		/**
		 * <pre>
		 * if ((cny_can_used.compareTo(reserved_amount) == -1)) {
		 * 	renderText(&quot;保留金额大于账户可用金额！&quot;);
		 * 	return;
		 * }
		 * 
		 * // 现有金额 - 投标金额 - 保留金额 必须大于 0
		 * if (!(cny_can_used.subtract(bid_amount).subtract(reserved_amount).compareTo($0) &gt; 0)) {
		 * 	renderText(&quot;投标金额填写错误，请重新填写&quot;);
		 * 	return;
		 * }
		 * </pre>
		 */

		boolean save_ok = user_automatically_bid.//
				set("user_id", user_id).//
				set("bid_amount", bid_amount).//
				//
				set("annulized_rate_min", annulized_rate_min).//
				set("annulized_rate_max", annulized_rate_max).//
				//

				set("borrow_duration_min", borrow_duration_min).//
				set("borrow_duration_max", borrow_duration_max).//
				set("borrow_duration_day_min", borrow_duration_day_min).//
				set("borrow_duration_day_max", borrow_duration_day_max).//
				//
				set("credit_rate_min", credit_rate_min).//
				set("credit_rate_min_int", credit_rate_min_int + 1).//
				set("credit_rate_max", credit_rate_max).//
				set("credit_rate_max_int", credit_rate_max_int + 1).//
				set("reserved_amount", reserved_amount).//
				set("disjunctor", 1).update();//
		if (save_ok) {
			renderText("ok");
			return;
		} else {
			renderText("提交失败");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "关闭自动投标设置", last_update_author = "zjb")
	public void shut_automatically_bid() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		UserAutomaticallyBidM user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);
		int disjunctor = user_automatically_bid.getInt("disjunctor");
		if (disjunctor != 0 && disjunctor != 1) {
			renderText("帐号异常，请联系客服。");
			return;
		} else if (disjunctor == 0) {
			renderText("自动投标工具已关闭");
			return;
		} else {
			boolean is_ok = user_automatically_bid.set("disjunctor", 0).update();
			if (is_ok) {
				renderText("关闭成功！");
				return;
			} else {
				renderText("关闭失败，请联系客服。");
				return;
			}
		}

	}
}

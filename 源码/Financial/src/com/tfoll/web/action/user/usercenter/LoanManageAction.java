package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.domain.FinancialRecipientInformation;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardOverdueRepaymentRecordM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentBySystemM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldLogM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldRepaymentRecordM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutWaitingM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.SendBackTheSuccessfulInformation;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@ActionKey("/user/usercenter/loan_manage")
public class LoanManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--申请查询", last_update_author = "lh")
	public void to_my_loan_apply() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql = "select * from borrower_bulk_standard_apply_order where user_id = ?";
		List<BorrowerBulkStandardApplyOrderM> borrower_bulk_standard_apply_order_list = BorrowerBulkStandardApplyOrderM.dao.find(sql, new Object[] { user_id });
		setSessionAttribute("borrower_bulk_standard_apply_order_list", borrower_bulk_standard_apply_order_list);

		renderJsp("/user/usercenter/loan_manage/my_loan_apply.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--还款", last_update_author = "lh or czh")
	public void to_my_loan_page() {

		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		String sql_gather_order = "select * from borrower_bulk_standard_gather_money_order where payment_state = 2 and user_id = ? ";
		Record gather_order_record = Db.findFirst(sql_gather_order, new Object[] { user_id });

		/**
		 * 如果下一次存在则获得所有的借贷金额
		 */
		BigDecimal borrow_all_money = new BigDecimal("0.00");
		if (gather_order_record != null) {
			long gather_order_id = gather_order_record.getLong("id");
			borrow_all_money = gather_order_record.getBigDecimal("borrow_all_money");

			String sql_repayment_plan = " select current_period, total_periods, automatic_repayment_date, should_repayment_total, repay_end_time from borrower_bulk_standard_repayment_plan where gather_money_order_id = ? and is_repay = 0  ";
			Record repayment_plan = Db.findFirst(sql_repayment_plan, new Object[] { gather_order_id });
			gather_order_record.addColumnMap(repayment_plan.getColumnMap());
		}
		borrow_all_money = borrow_all_money.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("loan_info", gather_order_record);
		setSessionAttribute("borrow_all_money", borrow_all_money);

		//
		/**
		 * 查询正在还款的所有还款记录
		 */
		String sql_repayment_plan_all_pay = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find(sql_repayment_plan_all_pay, new Object[] { user_id });
		List<Map<String, Object>> borrower_bulk_standard_repayment_plan_map_list = new ArrayList<Map<String, Object>>();

		BigDecimal has_pay = new BigDecimal("0"); // 已还的（可能未逾期,可能逾期）
		BigDecimal not_has_pay = new BigDecimal("0"); // 未还的（可能未逾期,可能逾期）
		BigDecimal not_has_pay_of_overdueverdue = new BigDecimal("0"); // 逾期未还的

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			Map<String, Object> borrower_bulk_standard_repayment_plan_map = borrower_bulk_standard_repayment_plan.getM();// getMap映射-可以扩容-Keys-但是不能更新数据
			borrower_bulk_standard_repayment_plan_map_list.add(borrower_bulk_standard_repayment_plan_map);
			// 还款日期
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");

			BigDecimal normal_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("normal_manage_fee");

			BigDecimal over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
			BigDecimal over_punish_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");
			//

			/**
			 * 是否支付0/1
			 */
			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");// 是否支付-决定逾期管理费和逾期罚息是否需要从数据库计算还是从数据库记录中取值
			/**
			 * <pre>
			 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
			 * </pre>
			 */
			int repayment_period = borrower_bulk_standard_repayment_plan.getInt("repayment_period");

			if (is_repay == 0) {
				// 正常管理费
				normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
				// 管理费-该公式里面已经计算了逾期管理费
				over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
				// 罚息-该公式里面已经计算了逾期罚息
				over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
				// actual_repayment==0
				repayment_period = CalculationFormula.get_repayment_period(repay_end_time);// 仅仅供外面查询-当还款成功的时候-该值会写入数据库

			} else if (is_repay == 1) {
				// 这里就不需要进行处理了
			} else {
				renderText("该状态不存在");
				return;
			}
			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_total_fee = over_manage_fee.add(over_punish_interest);
			// 应还总额=月还本息+管理费 +逾期费用
			BigDecimal total_total = should_repayment_total.add(normal_manage_fee).add(over_total_fee);// 理财那边的还款和借款这边的还款是不一样的

			borrower_bulk_standard_repayment_plan_map.put("manage_fee", normal_manage_fee);// 正常的管理费用
			borrower_bulk_standard_repayment_plan_map.put("over_total_fee", over_total_fee);// 包含逾期的管理费用
			borrower_bulk_standard_repayment_plan_map.put("total_total", total_total);// 该月应该还的所有

			if (is_repay == 0) {// 没有支付
				if (repayment_period == 1 || repayment_period == 2) {
					not_has_pay = not_has_pay.add(should_repayment_total).add(normal_manage_fee);
				} else if (repayment_period == 3 || repayment_period == 4) { // 所处时期为普通逾期,或者严重逾期
					not_has_pay = not_has_pay.add(should_repayment_total).add(normal_manage_fee).add(over_total_fee);
					not_has_pay_of_overdueverdue = not_has_pay_of_overdueverdue.add(should_repayment_total).add(normal_manage_fee).add(over_total_fee);
				} else {
					renderText("该状态不存在");
					return;
				}
			} else if (is_repay == 1) {// 支付
				has_pay = has_pay.add(total_total);// 支付了的所有总的累加
			} else {
				renderText("该状态不存在");
				return;
			}
		}
		not_has_pay = not_has_pay.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("weihuan", not_has_pay); // 未还的
		not_has_pay_of_overdueverdue = not_has_pay_of_overdueverdue.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("over_weihuan", not_has_pay_of_overdueverdue); // 逾期未还的

		renderJsp("/user/usercenter/loan_manage/loan_query.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--借款统计", last_update_author = "lh")
	public void to_loan_statistics() {

		renderJsp("/user/usercenter/loan_manage/loan_statistics.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class })
	@Function(for_people = "登录", function_description = "获取借款人的还款计划", last_update_author = "lh")
	public void get_repayment_plan() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql_money = "select cny_can_used from user_now_money where user_id = ?";
		BigDecimal cny_can_used = Db.queryBigDecimal(sql_money, new Object[] { user_id });

		String sql = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plans = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { user_id });
		// 查找对应的借款单的借款总金额
		String sql_all_money = "SELECT b.borrow_all_money FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";
		BigDecimal borrow_all_money = Db.queryBigDecimal(sql_all_money, new Object[] { user_id });

		// 查询包含当期(包含)之后的连续三期有多少期没有还款

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan = new BigDecimal("0");
		BigDecimal weihuan = new BigDecimal("0");

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plans) {
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			/**
			 * 还款了才显示还款的处理的状态
			 */
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			// 正常管理费
			BigDecimal manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));

			// 逾期管理费
			BigDecimal over_manage_fee = new BigDecimal(0 + "");
			// 逾期罚息
			BigDecimal over_faxi = new BigDecimal(0 + "");

			if (is_repay == 0) {
				over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
				over_faxi = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
			} else if (is_repay == 1) {
				over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
				over_faxi = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");
			}

			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_total_fee = over_faxi.add(over_manage_fee);

			// 应还总额=月还本息+管理费 +逾期费用
			total_total = total_total.add(should_repayment_total).add(manage_fee).add(over_total_fee);

			repayment_plan.put("manage_fee", manage_fee);

			repayment_plan.put("over_manage_fee", over_manage_fee);
			repayment_plan.put("over_faxi", over_faxi);

			repayment_plan.put("over_total_fee", over_total_fee);
			repayment_plan.put("total_total", total_total);

			if (is_repay == 0) {
				weihuan = weihuan.add(should_repayment_total).add(manage_fee).add(over_total_fee);
			} else if (is_repay == 1) {
				yihuan = yihuan.add(should_repayment_total).add(manage_fee).add(over_total_fee);
			}

		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("repayment_plans", repayment_plans);
		map.put("yihuan", yihuan);
		map.put("weihuan", weihuan);
		map.put("cny_can_used", cny_can_used);

		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class })
	@Function(for_people = "登录", function_description = "获取借款人的还款计划（一次性还款）", last_update_author = "zjb")
	public void get_pre_repayment_plan() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql_money = "select cny_can_used from user_now_money where user_id = ?";
		BigDecimal cny_can_used = Db.queryBigDecimal(sql_money, new Object[] { user_id });

		String sql = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plans = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { user_id });

		String str = "";
		long weihuan_count = 0;

		if (Utils.isHasData(borrower_bulk_standard_repayment_plans)) {

			// 对应的筹款单id
			long gather_order_id = borrower_bulk_standard_repayment_plans.get(0).getLong("gather_money_order_id");

			// 查询包含当期(包含)之后的连续三期有多少期没有还款
			String sql_weihuan_count = "SELECT count(1) FROM borrower_bulk_standard_repayment_plan WHERE gather_money_order_id = ? AND ? < repay_end_time AND is_repay = 0 ORDER BY repay_end_time ASC LIMIT 3";
			weihuan_count = Db.queryLong(sql_weihuan_count, new Object[] { new Date(), gather_order_id });
		}

		// 查找对应的借款单的借款总金额
		String sql_all_money = "SELECT b.borrow_all_money FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";
		BigDecimal borrow_all_money = Db.queryBigDecimal(sql_all_money, new Object[] { user_id });

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan = new BigDecimal("0");
		BigDecimal weihuan = new BigDecimal("0");

		long now_long = System.currentTimeMillis();
		/**
		 * 先计算出是否含有当期
		 */
		boolean is_in_the_normal_repayment_plan = false;
		long repayment_plan_id_of_this_month = 0;
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plans) {

			long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if ((repay_start_time_long <= now_long && now_long <= repay_end_time_long)) {
				is_in_the_normal_repayment_plan = true;
				repayment_plan_id_of_this_month = repayment_plan_id;
				break;
			}
		}

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plans) {
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			/**
			 * 还款了才显示还款的处理的状态
			 */
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
			BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");

			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			// 正常管理费
			BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));

			BigDecimal over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
			BigDecimal over_punish_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");

			if (is_repay == 0) {

				// 管理费-该公式里面已经计算了逾期管理费
				over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
				// 罚息-该公式里面已经计算了逾期罚息
				over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
				int repayment_period = CalculationFormula.get_repayment_period(repay_end_time);// 仅仅供外面查询-当还款成功的时候-该值会写入数据库

				/**
				 * 针对不同日期进行还款细节计算
				 */
				if (is_in_the_normal_repayment_plan) {

					long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
					if (repayment_plan_id_of_this_month > repayment_plan_id) {
						if (!(repayment_period == 3 || repayment_period == 4)) {
							renderText("当月还款计划ID之前的还款周期计算错误");
							return;
						}
						// 有利息+两个管理费+罚息
						// should_repayment_principle
						// should_repayment_interest
						// normal_manage_fee
						// over_manage_fee
						// over_punish_interest

					} else if (repayment_plan_id_of_this_month == repayment_plan_id) {
						if (!(repayment_period == 1 || repayment_period == 2)) {
							renderText("当月还款计划的还款周期计算错误");
							return;
						}
						/**
						 * 没有逾期管理费和罚息
						 */
						over_manage_fee = new BigDecimal("0");
						over_punish_interest = new BigDecimal("0");

					} else {// repayment_plan_id_of_this_month<repayment_plan_id
						if (!(repayment_period <= 2)) {
							renderText("当月还款计划之后的还款周期计算错误");
							return;
						}
						/**
						 * 没有利息和正常管理费和逾期管理费和罚息
						 */
						should_repayment_interest = new BigDecimal("0");
						normal_manage_fee = new BigDecimal("0");
						over_manage_fee = new BigDecimal("0");
						over_punish_interest = new BigDecimal("0");

					}

				} else {// 算的上是逾期的
					if (!(repayment_period == 3 || repayment_period == 4)) {
						renderText("当月还款计划ID之前的还款周期计算错误");
						return;
					}
				}

				// 应还总额=月还本息[利息可能为0]+管理费 +逾期费用
				total_total = total_total.add(should_repayment_principle).add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
				// 求和
				weihuan = weihuan.add(should_repayment_principle).add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);

			} else if (is_repay == 1) {
				yihuan = yihuan.add(should_repayment_total).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
			}
			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_total_fee = over_manage_fee.add(over_punish_interest);

			repayment_plan.put("manage_fee", normal_manage_fee);
			repayment_plan.put("over_manage_fee", over_manage_fee);
			repayment_plan.put("over_faxi", over_punish_interest);

			repayment_plan.put("over_total_fee", over_total_fee);
			repayment_plan.put("total_total", total_total);

		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("repayment_plans", repayment_plans);
		map.put("yihuan", yihuan);
		map.put("weihuan", weihuan);
		map.put("cny_can_used", cny_can_used);
		map.put("weihuan_count", weihuan_count);
		str = gson.toJson(map);

		renderText(str);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "异步获取易还清借款", last_update_author = "lh")
	public void get_pay_off_loan() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String sql = "select * from borrower_bulk_standard_gather_money_order where payment_state = 3 and user_id = ? ";
		List<BorrowerBulkStandardGatherMoneyOrderM> borrower_bulk_standard_gather_money_order_list = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql, new Object[] { user_id });
		List<Map<String, Object>> pay_off_loan_list = new ArrayList<Map<String, Object>>();
		for (BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order : borrower_bulk_standard_gather_money_order_list) {
			Map<String, Object> pay_off_loan = borrower_bulk_standard_gather_money_order.getM();
			pay_off_loan_list.add(pay_off_loan);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pay_off_loan_list", pay_off_loan_list);
		Gson gson = new Gson();
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	/**
	 * <还款规则> 还款是针对成功还款一次则记录一次的原则-同时需要保证整个还款的过程中数据是否一致
	 * 
	 * <pre>
	 * 系统还款由N部分构成:还款截止日之前手动还款,系统还款日自动还款,普通逾期,严重逾期
	 * 
	 * 提前还掉所有的款项[还款截止日之前手动还款,系统还款日自动还款,普通逾期,严重逾期]-必须一次性还清所有的单子-那么这个是很复杂的-需要一个事务-这个时候需要把用户的债权转让计划给撤掉,
	 * 
	 * 单月
	 * <tbody>
	 * 手动还当期[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统],
	 * 单独的系统自动还款[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统][严重逾期处理和自动还款处理冲突解决办法-逾期处理位于自动还款日最大的天数31天之后即32天],
	 * 手动还普通逾期[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统],
	 * 手动还严重逾期[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统],
	 * 
	 * 手动还当月后一个月的还款[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统],
	 * 手动还当月后第二个月的还款[如果系统出现严重逾期或者凑集单被标记为严重逾期,那么这笔钱则自动划归系统],
	 * 
	 * 根据上面单个的还款标准-同时需要加上一个还款的约束的条件这个就是还款必须是如果还款还后几个月的话如果不是完全还掉所有,那么最多只能还当月后两个月,且只能连续还款
	 * 
	 * 针对上述每个单子我们需要分别对待-每个还款计划处于那个阶段-执行那种还款指标. 说明一点：还款是不能跳序还款 
	 * </tbody>
	 * 
	 * 处理思路:
	 * 1检查还款计划ID数组含有元素且合法有序
	 * 2检查还款计划ID数组中每项都没有进行还款
	 * 3如果存在当月，对当月，当月后第一个月，当月后第二个月进行连续性测试
	 * 
	 * 4核心需要关注几个点：还款项 还款期间  借款人 理财人 系统逾期还款记录
	 * </pre>
	 * 
	 * 
	 * <pre>
	 * 用户手动还款功能和系统自动还款功能和系统自动逾期处理功能，这三个功能间存在一个处理难题。
	 * 系统中每个还款计划之间前后两个之间间隔为28,29,30,31天,我们需要需要确保系统只能在 同一时刻点只对三个功能中的任何一个功能的处理。
	 * 解决办法是1 对凑集单和还款计划加锁,2时间分离,严重逾期处理必须是自动还款日32天才能进行逾期处理,即当时期自动还款日最大值之后[31天]。
	 * ... 28 29 30 31[28 29 30 31间隔之差-自动还款日] 32[严重逾期处理-之后pay_for_object才能为1]
	 * </pre>
	 * 
	 * 对净值标和其他的进行还款修改
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "确定还款", last_update_author = "lh")
	public void confirm_repay_money() {
		UserM user = getSessionAttribute("user");
		final int user_id_of_borrower = user.getInt("id");
		final String borrower_nickname = user.getString("nickname");
		// 传递过来的参数格式"1,2,3,"
		String repayment_plan_id_string = getParameter("pass_id_str");
		if (!Utils.isNotNullAndNotEmptyString(repayment_plan_id_string)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("请先勾选");
			return;
		}
		String[] repayment_plan_ids_string = repayment_plan_id_string.split(",");// 还款计划id数组字符串
		final int repayment_plan_id_length = repayment_plan_ids_string.length;
		if (repayment_plan_id_length == 0) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("请先勾选");
			return;
		}
		final long[] repayment_plan_ids = new long[repayment_plan_id_length];
		int id_length = repayment_plan_ids_string.length;
		for (int i = 0; i < id_length; i++) {
			try {
				repayment_plan_ids[i] = Long.parseLong(repayment_plan_ids_string[i]);
			} catch (Exception e) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("传入的字符非数字");// 可以不用判断
				return;
			}
		}
		/**
		 * 检查传入的ID是否有序-保证我们还款是有序的,同时必须保证还款的数组所对应的还款计划没有还且不会乱序
		 */
		if (!Utils.is_asc_order(repayment_plan_ids)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("传入的还款计划的ID无序");// 可以不用判断
			return;
		}

		final AtomicReference<String> error_code = new AtomicReference<String>("出问题了...就是不知道在哪出的,需要检查一下那个地方没有返回值");
		/**
		 * 根据传入ID数组的第一个去数据库查询凑集单
		 */
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT gmo.* from borrower_bulk_standard_gather_money_order gmo,borrower_bulk_standard_repayment_plan  rp  WHERE rp.id=? and rp.borrower_user_id=? and rp.gather_money_order_id=gmo.id ", new Object[] { repayment_plan_ids[0], user_id_of_borrower });// 至少含有几个还款计划
		if (borrower_bulk_standard_gather_money_order == null) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统查询不到您的凑集借款单信息,请联系我公司客服,我们将尽快解决");
			return;
		}
		final long gather_money_order_id = borrower_bulk_standard_gather_money_order.getLong("id");
		final String borrow_title = borrower_bulk_standard_gather_money_order.getString("borrow_title");
		final int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");
		final int pay_for_object = borrower_bulk_standard_gather_money_order.getInt("pay_for_object");
		if (!(pay_for_object == 0 || pay_for_object == 1)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统中您的凑集借款单信息中还款状态错误,请联系我公司客服,我们将尽快解决");
			return;
		}
		/**
		 * 根据凑集单ID查询所有的还款计划,一次性查询出来提高数据库的性能:max=36条
		 */
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT * from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id= ?  ORDER BY id asc ", new Object[] { gather_money_order_id });
		if (!Utils.isHasData(borrower_bulk_standard_repayment_plan_list)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统中您的凑集借款单还款计划查询失败,请联系我公司客服,我们将尽快解决");
			return;

		}
		Map<String, BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_map = new HashMap<String, BorrowerBulkStandardRepaymentPlanM>();
		/**
		 * 需要通过current_period来确定下一个或者下下一个月的还款计划 <br/>
		 * current_period还款的顺序为key, BorrowerBulkStandardRepaymentPlanM为value <br/>
		 */
		Map<String, BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_map_of_order_by_current_period = new HashMap<String, BorrowerBulkStandardRepaymentPlanM>();

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			borrower_bulk_standard_repayment_plan_map.put(borrower_bulk_standard_repayment_plan.getLong("id") + "", borrower_bulk_standard_repayment_plan);
			borrower_bulk_standard_repayment_plan_map_of_order_by_current_period.put(borrower_bulk_standard_repayment_plan.getInt("current_period") + "", borrower_bulk_standard_repayment_plan);
		}

		/**
		 * 传入还款计划的ID数组每个元素需要在map中存在,且没有还款
		 */
		int repayment_plan_ids_index = 0;
		for (long repayment_plan_id : repayment_plan_ids) {
			repayment_plan_ids_index++;
			if (!borrower_bulk_standard_repayment_plan_map.containsKey(repayment_plan_id + "")) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("根据传入的还款计划ID在系统中查询不到对应的还款计划");// 用户的非法操作才能导致这个问题
				return;
			}
			/**
			 * 如果还了则不用再还了-Js控制-这可能会出现一个问题了,还款的时候出现错误那么后面的还款则不能继续-以免导致数据错误
			 */
			int is_repay = borrower_bulk_standard_repayment_plan_map.get(repayment_plan_id + "").getInt("is_repay");
			if (is_repay == 1) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("你勾选的第" + (repayment_plan_ids_index) + "项已经还款,不能再进行还款");/* 核心问题 */
				return;
			}
		}

		Date now = new Date();
		long now_long = now.getTime();
		// 当月有还款计划?
		// true表示处于第一个还款计划开始时间和最后一个还款计划的最后还款截至时间&&false表示已经超过最后一个还款计划的最后还款截至时间
		boolean is_in_the_normal_repayment_plan = false;
		long repayment_plan_id_of_this_month = 0;

		/**
		 * 最低的还款的时间保证-当前的还款时间如果小于还款计划的第一个还款计划的时间则肯定不行
		 */
		{
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan_list.get(0);
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			if (now_long < repay_start_time_long) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("当前时间小于第一个还款计划的还款开始时间");
				return;
			}
		}
		/**
		 * 寻找当月还款计划的ID-可能不存在
		 */
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if (repay_start_time_long <= now_long && now_long <= repay_end_time_long) {
				is_in_the_normal_repayment_plan = true;
				repayment_plan_id_of_this_month = repayment_plan_id;
				// 如果找不到则则表示逾期了,和之前的小于第一个月的还款计划开始时间进行判断做首尾呼应
				break;
			}
		}
		/**
		 * 获得该凑集单还款计划最后一个的当期期数，以及总的期数,如果最后一期都还了那么已经存在的债权转让需要失效，
		 * 已经存在的持有将失去其理财债权的效果
		 */

		/**
		 * 如果不是处于正常的还款期间进行还款,要是逾期还款,要么是非法操作
		 */
		if (!is_in_the_normal_repayment_plan) {
			if (pay_for_object == 0) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("尊敬的用户你好,按照正常的流程,你在签订的还款的合约最后一期还款截止时间进行还款,应该是已经逾期了,但是系统记录的是正常还款,请立即和我们客服联系,我们将尽快进行处理");
				return;
			} else {// 理论上就该逾期处理了
				// 逾期...
			}
			//
			/**
			 * 当月需要还款,但用户可以选择需要还款以及我们需要判断执行该业务操作的时候是否还款
			 */
		} else {
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan_of_this_month = borrower_bulk_standard_repayment_plan_map.get(repayment_plan_id_of_this_month + "");
			int current_period_of_this_month = borrower_bulk_standard_repayment_plan_of_this_month.getInt("current_period");
			int current_period_of_next_month = current_period_of_this_month + 1;
			int current_period_of_next_next_month = current_period_of_this_month + 2;

			// 当月后的第一个月
			long repayment_plan_id_of_next_month = 0;
			// 当月后的第二个月
			long repayment_plan_id_of_next_next_month = 0;

			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan_of_next_month = borrower_bulk_standard_repayment_plan_map_of_order_by_current_period.get(current_period_of_next_month + "");
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan_of_next_next_month = borrower_bulk_standard_repayment_plan_map_of_order_by_current_period.get(current_period_of_next_next_month + "");

			int is_repay_of_this_month = borrower_bulk_standard_repayment_plan_of_this_month.getInt("is_repay");
			if (!(is_repay_of_this_month == 0 || is_repay_of_this_month == 1)) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("尊敬的用户你好,您系统中某项还款计划还款状态错误,可能系统出现了严重的错误,请尽快联系客服,确保你的资金不受损失");
				return;
			}
			/**
			 * 根据还款计划是否为空判断该还款计划是否存在
			 */
			boolean is_exist_borrower_bulk_standard_repayment_plan_of_next_month = (borrower_bulk_standard_repayment_plan_of_next_month != null);
			boolean is_exist_borrower_bulk_standard_repayment_plan_of_next_next_month = (borrower_bulk_standard_repayment_plan_of_next_next_month != null);
			/**
			 * 根据还款计划是否存在获取is_repay值
			 */
			int is_repay_of_next_month = 0;
			if (is_exist_borrower_bulk_standard_repayment_plan_of_next_month) {
				repayment_plan_id_of_next_month = borrower_bulk_standard_repayment_plan_of_next_month.getLong("id");
				is_repay_of_next_month = borrower_bulk_standard_repayment_plan_of_next_month.getInt("is_repay");
				if (!(is_repay_of_next_month == 0 || is_repay_of_next_month == 1)) {
					logger.error("hack user_id:" + user_id_of_borrower);
					renderText("尊敬的用户你好,您系统中当月后第一个月的还款计划还款状态错误,请联系客服");
					return;
				}
			}
			int is_repay_of_next_next_month = 0;
			if (is_exist_borrower_bulk_standard_repayment_plan_of_next_next_month) {
				repayment_plan_id_of_next_next_month = borrower_bulk_standard_repayment_plan_of_next_next_month.getLong("id");
				is_repay_of_next_next_month = borrower_bulk_standard_repayment_plan_of_next_next_month.getInt("is_repay");
				if (!(is_repay_of_next_next_month == 0 || is_repay_of_next_next_month == 1)) {
					logger.error("hack user_id:" + user_id_of_borrower);
					renderText("尊敬的用户你好,您系统中当月后第二个月还款计划还款状态错误,请联系客服");
					return;
				}
			}
			/**
			 * 判断还款计划数组里面是否存在指定还款计划的ID
			 */
			boolean is_contains_this_month = Arrays.binarySearch(repayment_plan_ids, repayment_plan_id_of_this_month) >= 0;
			boolean is_contains_next_month = Arrays.binarySearch(repayment_plan_ids, repayment_plan_id_of_next_month) >= 0;
			boolean is_contains_next_next_month = Arrays.binarySearch(repayment_plan_ids, repayment_plan_id_of_next_next_month) >= 0;
			/**
			 * <还款规则>[还款首先要存在,还款状态0/1,还款计划数组是否包含,还款的顺序必须挨着还-最多只能还当月后的2个月
			 * 规则][判断步数少的优先于步数多的]
			 */

			/**
			 * 新的还款的思路
			 */
			/**
			 * 如果要还当月
			 */
			if (is_contains_this_month) {
				// 则必须保证当月存在，且没有还款
				if (!(borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 0)) {
					renderText("请正确你所需要勾选的还款的计划!");// 区分号!
					return;
				}
			}
			/**
			 * 如果要还当月后第一个月
			 */
			if (is_contains_next_month) {

				// 则必须保证当月后第一个月存在，且没有还款
				if (!(//
				(borrower_bulk_standard_repayment_plan_of_next_month != null && is_repay_of_next_month == 0) && // //
				// 当月后的第一个月
				(//
				(borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 0 && is_contains_this_month)// 当月
				|| //
				(borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 1 && !is_contains_this_month)// 当月
				)

				//		
				)) {
					renderText("请正确你所需要勾选的还款的计划!!");// 区分号!
					return;
				}

			}

			/**
			 * 如果要还当月后第二个月
			 */
			if (is_contains_next_next_month) {

				// 则必须保证当月后第二个月存在，且没有还款
				if (!(//
				(borrower_bulk_standard_repayment_plan_of_next_next_month != null && is_repay_of_next_next_month == 0) && // 当月后的第二个月
				(//
				((borrower_bulk_standard_repayment_plan_of_next_month != null && is_repay_of_next_month == 0 && is_contains_next_month) && ((borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 1 && !is_contains_this_month) || (borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 0 && is_contains_this_month)))// 当月后第一个月之前没有还款
				|| //
				((borrower_bulk_standard_repayment_plan_of_next_month != null && is_repay_of_next_month == 1 && !is_contains_next_month) && (borrower_bulk_standard_repayment_plan_of_this_month != null && is_repay_of_this_month == 1 && !is_contains_this_month))// 当月后第一个月之前还款,那么第一个月必须是还款了的
				)
				//
				)) {
					renderText("请正确你所需要勾选的还款的计划!!!");// 区分号!
					return;
				}

			}

			/**
			 * 只要出现有还款计划数组中存在超过当月后第二个月的,一律不允许
			 */
			for (long repayment_plan_id : repayment_plan_ids) {
				if (repayment_plan_id > repayment_plan_id_of_next_next_month) {
					renderText("您最多可以选择连续还当月后的两个月,如果要还掉所有款项请提前一次性还清!!!!");// 区分号!
					return;
				}
			}

		}// end 所有情况的讨论

		final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
		final int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");// 需要关注份额之比
		/**
		 * 判读还的资金够不
		 */
		BigDecimal all_actual_repayment = new BigDecimal(0);
		for (int i = 0; i < id_length; i++) {
			final long repayment_plan_id = repayment_plan_ids[i];
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan_map.get(repayment_plan_id + "");
			// 还款日期
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			// 应该要还的本金和利息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			//
			// 正常管理费
			BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
			// 管理费-该公式里面已经计算了逾期管理费
			BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
			// 罚息-该公式里面已经计算了逾期罚息
			BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

			BigDecimal actual_repayment = should_repayment_total.add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
			all_actual_repayment = all_actual_repayment.add(actual_repayment);
		}

		UserNowMoneyM user_now_money_borrower = UserNowMoneyM.get_user_current_money(user_id_of_borrower);
		BigDecimal cny_can_used_borrower = user_now_money_borrower.getBigDecimal("cny_can_used");
		if (cny_can_used_borrower.compareTo(all_actual_repayment) < 0) {
			renderText("资金不够");
			return;
		}
		/**
		 * 需要记录那些成功还款的还款计划-因为我们需要发送成功的还款通知消息-这个是不能写在核心的代码里面-怕影响核心流程的正常的执行
		 */
		/**
		 * 系统是2014-12-23 17:08:20开始逾期
		 */
		final List<Long> has_pay_repayment_plan_id_list = new ArrayList<Long>();
		final Map<Long, List<FinancialRecipientInformation>> financial_recipient_information_list_map = new HashMap<Long, List<FinancialRecipientInformation>>();

		// 需要记录还款了的
		/**
		 * 分开还钱
		 */
		for (int i = 0; i < id_length; i++) {
			final long repayment_plan_id = repayment_plan_ids[i];
			final int repayment_plan_ids_index_in_transaction = i + 1;// 第几个勾选

			boolean is_ok = Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {

					/**
					 * <pre>
					 * CREATE TABLE `borrower_bulk_standard_repayment_plan` (
					 * `repay_actual_time` timestamp NULL DEFAULT NULL COMMENT '实际还款时间',
					 * `repay_actual_time_long` bigint(20) DEFAULT NULL COMMENT '实际还款时间毫秒数',
					 * 
					 * `should_repayment_total` decimal(10,2) DEFAULT '0.00' COMMENT '应该要还的本金和利息',
					 * `should_repayment_principle` decimal(10,2) DEFAULT '0.00' COMMENT '要还的本金',
					 * `should_repayment_interest` decimal(10,2) DEFAULT '0.00' COMMENT '要还的利息（逾期后该数值每天都会改变，要写存储过程？）',
					 * 
					 * 
					 * `normal_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正常管理费',
					 * `over_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '逾期管理费',
					 * `over_punish_interest` decimal(10,2) DEFAULT '0.00' COMMENT '逾期罚息',
					 * `actual_repayment` decimal(10,2) DEFAULT '0.00' COMMENT '实际还款金额',
					 * 
					 * `is_repay` int(1) DEFAULT '0' COMMENT '是否已经还款 0.没有还款 1.已经还款',
					 * 
					 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
					 * PRIMARY KEY (`id`),
					 * KEY `automatic_repayment_date` (`automatic_repayment_date`)
					 * ) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COMMENT='借款人的还款计划表';
					 * </pre>
					 */

					BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = BorrowerBulkStandardRepaymentPlanM.dao.findById(repayment_plan_id);
					//
					/**
					 * 是否支付0/1
					 */
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");// 是否支付-决定逾期管理费和逾期罚息是否需要从数据库计算还是从数据库记录中取值
					if (is_repay != 0) {
						return true;// 不需要计算后面
					}
					// total_periods, current_period
					int total_periods = borrower_bulk_standard_repayment_plan.getInt("total_periods");
					int current_period = borrower_bulk_standard_repayment_plan.getInt("current_period");

					// 实际还款时间
					Date repay_actual_time = new Date();
					long repay_actual_time_long = repay_actual_time.getTime();
					borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
					borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

					// 还款日期
					Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
					String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
					/**
					 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
					 * 系统收取管理费用
					 */
					// 要还的本息
					// 应该要还的本金和利息
					BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
					// 要还的本金
					BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
					// 要还的利息
					BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
					// 正常管理费
					BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
					// 管理费[包含了逾期管理费]
					BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
					// 罚息
					BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
					/**
					 * 上面是单项的数据
					 */
					// 应还总额=月还本息+管理费 +逾期费用
					BigDecimal actual_repayment = should_repayment_total.add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);// 理财那边的还款和借款这边的还款是不一样的

					/**
					 * 针对各种借款还款阶段做出响应的操作:判断正常还款,自动还款,是否严重逾期,逾期 <br/>
					 * 
					 * <pre>
					 * 正常还款和自动还款是没有罚息的
					 * 逾期是含有罚息的,那么这部分的资金是打给理财用户的
					 * 严重逾期那么这部分的资金是还给系统的-而不是理财的用户-理财用户是我们系统指定的一个帐号id= 4id=[1-4]都被系统征用
					 * </pre>
					 */

					/**
					 * <pre>
					 * CREATE TABLE `borrower_bulk_standard_repayment_plan` (
					 * `repay_actual_time` timestamp NULL DEFAULT NULL COMMENT '实际还款时间',
					 * `repay_actual_time_long` bigint(20) DEFAULT NULL COMMENT '实际还款时间毫秒数',
					 * `should_repayment_total` decimal(10,2) DEFAULT '0.00' COMMENT '应该要还的本金和利息',
					 * `should_repayment_principle` decimal(10,2) DEFAULT '0.00' COMMENT '要还的本金',
					 * `should_repayment_interest` decimal(10,2) DEFAULT '0.00' COMMENT '要还的利息（逾期后该数值每天都会改变，要写存储过程？）',
					 * `normal_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正常管理费',
					 * `over_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '逾期管理费',
					 * `over_punish_interest` decimal(10,2) DEFAULT '0.00' COMMENT '逾期罚息',
					 * `actual_repayment` decimal(10,2) DEFAULT '0.00' COMMENT '实际还款金额',
					 * `is_repay` int(1) DEFAULT '0' COMMENT '是否已经还款 0.没有还款 1.已经还款',
					 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
					 * PRIMARY KEY (`id`),
					 * KEY `automatic_repayment_date` (`automatic_repayment_date`)
					 * ) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COMMENT='借款人的还款计划表';
					 * </pre>
					 */
					// 记录上面的费用
					borrower_bulk_standard_repayment_plan.set("normal_manage_fee", normal_manage_fee);// 正常管理费-属于系统的收入
					borrower_bulk_standard_repayment_plan.set("over_manage_fee", over_manage_fee);// 逾期管理费-属于系统的收入
					borrower_bulk_standard_repayment_plan.set("over_punish_interest", over_punish_interest);// 罚息利息-如果是严重逾期则给系统，如果是一般逾期则给理财的用户,还款日之前-包括还款日不需要给理财用户
					borrower_bulk_standard_repayment_plan.set("actual_repayment", actual_repayment);
					//
					borrower_bulk_standard_repayment_plan.set("is_repay", 1);
					/**
					 * <pre>
					 * 记录什么时候还钱的-还款期间:
					 * -----------------没有逾期的三个阶段------------------
					 * 0未还款
					 * 1处于还款日期之前[手动还款]
					 * 2还款日当天系统自动还款
					 * -----------------没有逾期的二个阶段------------------
					 * 3普通逾期
					 * 4严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}]
					 * </pre>
					 */
					// repayment_period是不能返回为0的
					int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);// 得到当前的还款期间
					if (!(repayment_period == 1 || repayment_period == 2 || repayment_period == 3 || repayment_period == 4)) {
						error_code.set("还款期间计算出错");// 还款期间计算得出的不是01234!
						return false;
					}

					borrower_bulk_standard_repayment_plan.set("repayment_period", repayment_period);
					//
					boolean is_ok_update_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.update();
					if (!is_ok_update_borrower_bulk_standard_repayment_plan) {
						error_code.set("系统更新还款信息失败");
						return false;
					}

					/**
					 * 1修改借款人的资金信息
					 */
					UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_of_borrower);
					BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used"); // 还款之前的金额
					BigDecimal cny_freeze_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_freeze");

					boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower.subtract(actual_repayment)).update();
					if (!is_ok_update_user_now_money_of_borrower) {
						error_code.set("系统更新借款人资金失败");
						return false;
					}
					/**
					 * 判断是否含有逾期管理费
					 */
					boolean is_has_over_manage_fee = over_manage_fee.compareTo(new BigDecimal("0")) > 0;
					/**
					 * 判断是否含有逾期罚息
					 */
					boolean is_has_over_punish_interest = over_punish_interest.compareTo(new BigDecimal("0")) > 0;
					/**
					 * 需要区分那些是打款给理财人或者系统的
					 * 
					 * <pre>
					 * sys_expenses_records no
					 * sys_income_records yes
					 * 
					 * user_money_change_records yes 
					 * user_transaction_records yes
					 * </pre>
					 */
					// 系统收入的部分
					// 正常管理费+逾期管理费+罚息-需要根据'pay_for_object和还款的周期'进行决定[Type_37,Type_38]
					// ###
					// 正常管理费
					boolean is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, normal_manage_fee, "正常管理费");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[正常管理费]失败");
						return false;
					}
					// 逾期管理费
					if (repayment_period == 1 || repayment_period == 2) {// 没有逾期管理费
						if (is_has_over_manage_fee) {
							error_code.set("添加财务收入部分[逾期管理费]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出逾期管理费大于0");
							return false;
						}

					} else {
						if (!is_has_over_manage_fee) {
							error_code.set("添加财务收入部分[逾期管理费]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "逾期,但是系统却计算出逾期管理费小于等于0");
							return false;
						} else {// 大于0
							is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, over_manage_fee, "逾期管理费");
							if (!is_ok_add_sys_income_records) {
								error_code.set("添加财务收入部分[逾期管理费]失败");
								return false;
							}
						}
					}
					// 罚息
					if (pay_for_object == 0) {// 严重逾期处理是32天-所以会出现即使逾期用户逾期了31天但是pay_for_object
						// == 0
						if (repayment_period == 1 || repayment_period == 2) {// 没有罚息
							if (is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出罚息大于0");
								return false;
							}
						} else if (repayment_period == 3) {// 普通罚息-这部分的资金是需要打给理财人的
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[普通逾期罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "逾期,但是系统却计算出普通逾期罚息小于等于0");
								return false;
							} else {
								// 普通罚息-这部分的资金是需要打给理财人的
							}
						} else if (repayment_period == 4) {// 严重逾期的情况这部分的资金需要给系统的
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[严重逾期罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "逾期,但是系统却计算出严重逾期罚息小于等于0");
								return false;
							}
							is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_38, over_punish_interest, "严重逾期罚息");
							if (!is_ok_add_sys_income_records) {
								error_code.set("添加财务收入部分[严重逾期罚息]失败");
								return false;
							}
						} else {
							error_code.set("不可能执行到这");
							return false;
						}
						/**
						 * 如果处于逾期状态则需要把这部分的资金给系统收入
						 */
					} else {

						if ((repayment_period == 1 || repayment_period == 2)) {// 没有罚息
							if (is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出罚息大于0");
								return false;
							}
						} else if (repayment_period == 3) {// 普通逾期
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[普通逾期罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出普通逾期罚息小于等于0");
								return false;
							}
							is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_37, over_punish_interest, "一般逾期罚息");
							if (!is_ok_add_sys_income_records) {
								error_code.set("添加财务收入部分[普通逾期罚息]失败");
								return false;
							}
						} else if (repayment_period == 4) {// 严重逾期
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[严重逾期罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出严重逾期罚息小于等于0");
								return false;
							}
							is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_38, over_punish_interest, "严重逾期罚息");
							if (!is_ok_add_sys_income_records) {
								error_code.set("添加财务收入部分[严重逾期罚息]失败");
								return false;
							}
						}

					}

					/**
					 * 
					 * <pre>
					 * 向借款人打钱由两部分组成
					 * public static final int Type_31 = 31;// 打入借入者借款总额-收入
					 * @SysIncome
					 * public static final int Type_32 = 32;// 收取借入者借款服务费-支出
					 * // 向借款人收取本息由...部分组成.这个是正常还款的记录
					 * ------------------------------------------------------------------------
					 * public static final int Type_33 = 33;// 偿还本息-支出
					 * @SysIncome
					 * public static final int Type_34 = 34;// 返还服务费..特别收取-支出
					 * @SysIncome
					 * public static final int Type_35 = 35;// 借款管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_36 = 36;// 借款超期管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_37 = 37;// 普通罚息-1-30天内-支出[理财人]
					 * @SysIncome
					 * public static final int Type_38 = 38;// 严重超期罚息-大于30天-支出-[系统]
					 * 可能还有其他key的需要添加系统的收入
					 * </pre>
					 */
					// 用户资金变化
					boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息#还款计划id"
							+ repayment_plan_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee),
							"偿还一般管理费#还款计划id" + repayment_plan_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[一般管理费]记录失败");
						return false;
					}
					if (is_has_over_manage_fee) {
						is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(
								over_manage_fee), "偿还逾期管理费#还款计划id" + repayment_plan_id);
						if (!is_ok_add_user_money_change_records_by_type) {
							error_code.set("添加资金变化[逾期管理费]记录失败");
							return false;
						}
					}
					/**
					 * 一般罚息和严重罚息
					 */
					{
						if ((repayment_period == 1 || repayment_period == 2)) {// 没有罚息
							if (is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出罚息大于0");
								return false;
							}
							/**
							 * 按照自己的实际进行交清罚息
							 */
						} else if (repayment_period == 3) {// 一般逾期的情况这部分的资金需要给理财人的
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[普通罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出普通罚息大于0");
								return false;
							}
							is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_37, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_punish_interest, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee)
									.subtract(over_manage_fee).add(over_punish_interest), "偿还普通罚息#还款计划id" + repayment_plan_id);
							if (!is_ok_add_user_money_change_records_by_type) {
								error_code.set("添加资金变化[普通罚息]记录失败");
								return false;
							}
						} else if (repayment_period == 4) {// 严重逾期的情况这部分的资金需要给系统的
							if (!is_has_over_punish_interest) {
								error_code.set("添加财务收入部分[严重罚息]失败[bug]:你选择的第" + repayment_plan_ids_index_in_transaction + "没有逾期,但是系统却计算出严重罚息大于0");
								return false;
							}
							is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_38, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_punish_interest, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee)
									.subtract(over_manage_fee).add(over_punish_interest), "偿还严重罚息#还款计划id" + repayment_plan_id);
							if (!is_ok_add_user_money_change_records_by_type) {
								error_code.set("添加资金变化[严重罚息]记录失败");
								return false;
							}
						}

					}

					// 用户交易记录
					/**
					 * <pre>
					 * public static final int Type_31 = 31;// 借款服务费
					 * public static final int Type_32 = 32;// 偿还本息 
					 * public static final int Type_33 = 33;// 借款管理费
					 * public static final int Type_34 = 34;// 返还服务费 
					 * public static final int Type_35 = 35;//回收本息 
					 * public static final int Type_36 = 36;// 提前回收 
					 * public static final int Type_37 = 37;// 提前还款 
					 * public static final int Type_38 = 38;// 逾期管理费
					 * public static final int Type_39 = 39;// 逾期罚息
					 * </pre>
					 */
					boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_32, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee), "借款管理费");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[借款管理费]记录失败");
						return false;
					}
					if (is_has_over_manage_fee) {
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_38, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(over_manage_fee),
								"逾期管理费");
						if (!is_ok_add_user_transaction_records) {
							error_code.set("添加交易记录[逾期管理费]记录失败");
							return false;
						}
					}
					if (is_has_over_punish_interest) {
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_punish_interest, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(
								over_manage_fee).add(over_punish_interest), "逾期罚息");
						if (!is_ok_add_user_transaction_records) {
							error_code.set("添加交易记录[逾期罚息]记录失败");
							return false;
						}
					}
					/**
					 * [手动还款是有序的]--这个和[自动还款和普通逾期还款]不一样--[严重逾期处理和一次性还款]又不一样
					 * 
					 * 判断当前还的最后一期是不是最后的一期-如果是则需要把整个系统设置为还款成功-
					 * 如果有持有则把所有的持有都变为失效-不在系统的还款和债权转让的生命周期
					 */
					// 判断是不是最后的一期
					final boolean is_last_period = total_periods == current_period;
					if (is_last_period) {
						BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT id,payment_state,payment_finish_time from borrower_bulk_standard_gather_money_order WHERE id=? ", new Object[] { gather_money_order_id });// 至少含有几个还款计划
						boolean is_ok_update_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("payment_state", 3).set("payment_finish_time", new Date()).update();
						if (!is_ok_update_borrower_bulk_standard_gather_money_order) {
							error_code.set("还款最后一期,更新凑集单还款成功状态失败");
							return false;
						}
						// is_not_pay还款完成的时候需要修改is_not_pay =0,borrow_type=0
						boolean is_update_user_info_is_not_pay_ok = Db.update("UPDATE user_info SET is_not_pay =0,borrow_type=0 WHERE id=?", new Object[] { user_id_of_borrower }) >= 0;
						if (!is_update_user_info_is_not_pay_ok) {
							return false;
						}
						// 如果当前没有处于严重逾期状态,则需要把该凑集单的所有的持有和正在转让的单子撤掉
						// 见下面...非逾期状态-债权持有处理之后
					}

					/**
					 * 根绝pay_for_object是否还给系统来决定是否还给理财用户或者系统
					 */
					if (pay_for_object == 1) {
						// 还给系统-如果系统进行对这个资金进行处理了-那么这部分的钱应该入账了且可以被公司使用
						boolean is_ok_add_borrower_bulk_standard_overdue_repayment_record = BorrowerBulkStandardOverdueRepaymentRecordM.add_borrower_bulk_standard_overdue_repayment_record(//
								gather_money_order_id,//
								repayment_plan_id, //
								total_periods, //
								current_period,//
								should_repayment_total, //
								should_repayment_principle, //
								should_repayment_interest,//
								normal_manage_fee,//
								over_manage_fee, //
								over_punish_interest,//
								actual_repayment);//
						if (!is_ok_add_borrower_bulk_standard_overdue_repayment_record) {
							error_code.set("逾期还款过程中添加逾期记录失败");
							return false;
						} else {
							return true;
						}

					} else {

						List<FinancialRecipientInformation> financial_recipient_information_list = new ArrayList<FinancialRecipientInformation>();
						/**
						 * 本息是所有的理财用户都需要进行平均分配的
						 */
						BigDecimal each_principal_interest = should_repayment_total.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本息
						/**
						 * 
						 * 定义当前需要还的每份的本金
						 */
						BigDecimal each_principal = should_repayment_principle.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本金
						/**
						 * 定义当前需要还的每份的利息
						 */
						BigDecimal each_interest = should_repayment_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的利息
						/**
						 * 定义借款人给理财人的还款罚息
						 */

						BigDecimal each_punish_interest = new BigDecimal("0"); // 平均每份所对应的借款罚息
						if (repayment_period == 1 || repayment_period == 2) {
						} else if (repayment_period == 3) {
							/**
							 * <pre>
							 *  普通超期罚息归理财用户-需要被理财用户平分[(这个状态&&pay_for_object == 0)才能进行罚息的支付]
							 * </pre>
							 */
							each_punish_interest = over_punish_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);
						} else if (repayment_period == 4) {
						} else {
							error_code.set("不可能执行到这的");
							return false;
						}

						// 到债权持有表中查找每个理财人对应的持有份数, 来增加他们资金表中的可用余额
						String sql_find_holders = "select id,user_id,hold_money,hold_share,transfer_share,is_transfer_all from lender_bulk_standard_creditor_right_hold where gather_money_order_id =? and hold_share!=0 ";
						List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find(sql_find_holders, new Object[] { gather_money_order_id });
						if (!Utils.isHasData(lender_bulk_standard_creditor_right_hold_list)) {
							error_code.set("根据凑集单的ID查询不到理财持有信息");
							return false;
						}

						// 还给理财人
						/**
						 * 还款的时候债权的价值以及对应的价格和手续费会相应的按照折价的系数修改-
						 * 同时系统需要一种机制告诉系统所有需要通过债权转让获取的债权持有的理财人暂时不能操作
						 */
						for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {
							long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");// id
							int financial_user_id = lender_bulk_standard_creditor_right_hold.getInt("user_id"); // 理财人id
							BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
							int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 原始购买的份额
							int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 转出的份额
							/**
							 * 针对每一个单期 是需要改变债权的价值即可,针对一次还清或者严重逾期处理则是让债权失效和债权转让撤单
							 */
							if (transfer_share != 0) {// 正在转让过程中需要对外面的债权转让实现冻结

								/**
								 * 在还款的时候债权的价值需要被改变
								 */
								List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list = LenderBulkStandardOrderCreditorRightTransferOutM.dao.find("SELECT id from lender_bulk_standard_order_creditor_right_transfer_out WHERE creditor_right_hold_id=? and state=1", new Object[] { creditor_right_hold_id });
								if (Utils.isHasData(lender_bulk_standard_order_creditor_right_transfer_out_list)) {
									for (LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out : lender_bulk_standard_order_creditor_right_transfer_out_list) {
										LenderBulkStandardOrderCreditorRightTransferOutWaitingM lender_bulk_standard_order_creditor_right_transfer_out_waiting = new LenderBulkStandardOrderCreditorRightTransferOutWaitingM();
										boolean is_ok_add_lender_bulk_standard_order_creditor_right_transfer_out_waiting = lender_bulk_standard_order_creditor_right_transfer_out_waiting.//
												set("creditor_right_transfer_out_id", lender_bulk_standard_order_creditor_right_transfer_out.getLong("id")).//
												set("deal_level", 2).// 要求系统马上进行债权转让程序进行处理
												save();
										if (!is_ok_add_lender_bulk_standard_order_creditor_right_transfer_out_waiting) {
											return false;
										}

									}

								}

							}
							// 应该收到的钱由两部分构成
							BigDecimal principal = each_principal.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本金
							BigDecimal interest = each_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的利息
							BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本息
							//						
							BigDecimal punish_interest = each_punish_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的罚息
							// 应该回收的金额:principal_interest+punish_interest
							BigDecimal recieve_money = principal_interest.add(punish_interest);

							UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(financial_user_id);
							BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
							BigDecimal cny_freeze_of_lender = user_now_money_of_lender.getBigDecimal("cny_freeze");

							/**
							 * 理财人回款
							 */
							boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender.add(recieve_money)).update();
							if (!is_ok_update_user_now_money_of_lender) {
								error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人资金信息失败,为了避免重复还款请刷新页面");
								return false;
							}
							/**
							 * 需要记录资金信息
							 * 
							 * <pre>
							 * sys_expenses_records no
							 * sys_income_records no
							 * 
							 * user_money_change_records yes 
							 * user_transaction_records yes
							 * </pre>
							 */
							// user_money_change_records
							is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_74, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息#还款计划ID" + repayment_plan_id);
							if (!is_ok_add_user_money_change_records_by_type) {
								error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人资金信息[回收本息]失败,为了避免重复还款请刷新页面");
								return false;
							}
							if (is_has_over_punish_interest) {
								is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_76, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息#还款计划ID"
										+ repayment_plan_id);
								if (!is_ok_add_user_money_change_records_by_type) {
									error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人资金信息[逾期产生的罚息]失败,为了避免重复还款请刷新页面");
									return false;
								}
							}
							// user_transaction_records
							is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(financial_user_id, UserTransactionRecordsM.Type_32, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息");
							if (!is_ok_add_user_transaction_records) {
								error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人资金信息[回收本息]失败,为了避免重复还款请刷新页面");
								return false;
							}
							is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(financial_user_id, UserTransactionRecordsM.Type_39, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息");
							if (!is_ok_add_user_transaction_records) {
								error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人资金信息[逾期产生的罚息]失败,为了避免重复还款请刷新页面");
								return false;
							}

							/**
							 * 添加回款的财务记录
							 */
							boolean is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record = LenderBulkStandardCreditorRightHoldRepaymentRecordM.add_lender_bulk_standard_creditor_right_hold_repayment_record(//
									financial_user_id,//
									user_id_of_borrower,//
									gather_money_order_id, //
									borrow_type, //
									borrower_nickname,//
									repayment_plan_id, //
									total_periods, //
									current_period,//
									creditor_right_hold_id,//
									hold_money, //
									hold_share,//
									principal,//
									interest,//
									principal_interest,//
									punish_interest, //
									recieve_money,//
									new Date());//
							if (!is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record) {
								error_code.set("你选择的第" + repayment_plan_ids_index_in_transaction + "项,更新理财人回款记录失败,请你刷新页面重新还款");
								return false;
							}
							// 记录进financial_recipient_information_list
							FinancialRecipientInformation financial_recipient_information = new FinancialRecipientInformation(//
									financial_user_id,//
									gather_money_order_id,//
									user_id_of_borrower,//
									repayment_plan_id, //
									total_periods, //
									current_period, //
									creditor_right_hold_id, //
									hold_money,//
									hold_share, //
									principal_interest, //
									punish_interest,//
									new Date(), //
									new Date().getTime(),//
									borrow_title);//
							financial_recipient_information_list.add(financial_recipient_information);
						}// for
						if (is_last_period) {
							// 如果当前没有处于严重逾期状态,则需要把该[凑集单-最后一期了]的所有的持有和正在转让的单子撤掉
							boolean is_ok_update_lender_bulk_standard_creditor_right_hold = Db.update("UPDATE lender_bulk_standard_creditor_right_hold SET transfer_money=0,transfer_share=0,is_need_handle=0 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
							if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
								error_code.set("最后一期所有理财人持有失效操作更新失败");
								return false;
							}
							boolean is_ok_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
							if (!is_ok_lender_bulk_standard_order_creditor_right_transfer_out) {
								error_code.set("最后一期所有理财人债权转让撤销操作更新失败");
								return false;
							}
						}

						financial_recipient_information_list_map.put(repayment_plan_id, financial_recipient_information_list);
						has_pay_repayment_plan_id_list.add(repayment_plan_id);// 保证有序性
						return true;

					}

				}
			});

			/**
			 * 统一在这里发送回款成功短信
			 */

			if (!is_ok) {
				if (!Utils.isHasData(has_pay_repayment_plan_id_list)) {
					System.out.println(repayment_plan_id);
					renderText("还款失败");
					return;
				} else {// 只要还款成功一个则发送消息
					SendBackTheSuccessfulInformation.send_back_the_successful_information(has_pay_repayment_plan_id_list, financial_recipient_information_list_map);
					String msg = error_code.get();
					renderText(msg);
					return;
				}

			}

		}// for i
		/**
		 * 统一在这里发送回款成功短信
		 */
		if (Utils.isHasData(has_pay_repayment_plan_id_list)) {
			SendBackTheSuccessfulInformation.send_back_the_successful_information(has_pay_repayment_plan_id_list, financial_recipient_information_list_map);
		}
		renderText("还款成功");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "登录", function_description = "提前还款", last_update_author = "lh")
	public void pre_repay_money() {

		UserM user = getSessionAttribute("user");
		final int user_id_of_borrower = user.getInt("id");
		final String borrower_nickname = user.getString("nickname");

		final long gather_money_order_id = getParameterToLong("gather_money_order_id", 0);
		if (gather_money_order_id == 0) {
			renderText("0");
			return;
		}
		final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT gmo.* FROM borrower_bulk_standard_gather_money_order gmo WHERE gmo.id = ? AND gmo.user_id = ?", new Object[] { gather_money_order_id, user_id_of_borrower });// 至少含有几个还款计划
		if (borrower_bulk_standard_gather_money_order == null) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统查询不到您的凑集借款单信息,请联系我公司客服,我们将尽快解决");
			return;
		}
		final String borrow_title = borrower_bulk_standard_gather_money_order.getString("borrow_title");
		final int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");

		if (!(borrow_type == 1 || borrow_type == 2 || borrow_type == 3)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统中您的凑集借款单信息中借贷类型错误,请联系我公司客服,我们将尽快解决");
			return;
		}

		int payment_state = borrower_bulk_standard_gather_money_order.getInt("payment_state");
		final int pay_for_object = borrower_bulk_standard_gather_money_order.getInt("pay_for_object");
		if (!(payment_state == 1 || payment_state == 2 || payment_state == 3)) {
			renderText("还款状态错误!");
			return;
		}
		if (payment_state == 1) {
			renderText("还款状态错误!!");
			return;
		}
		if (payment_state == 3) {
			renderText("还款成功");
			return;
		}
		if (!(pay_for_object == 0 || pay_for_object == 1)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统中您的凑集借款单信息中还款状态错误,请联系我公司客服,我们将尽快解决");
			return;
		}

		/**
		 * 根据凑集单ID查询所有的还款计划,一次性查询出来提高数据库的性能:max=36条
		 */
		final List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT * from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id= ?  ORDER BY id asc ", new Object[] { gather_money_order_id });
		if (!Utils.isHasData(borrower_bulk_standard_repayment_plan_list)) {
			logger.error("hack user_id:" + user_id_of_borrower);
			renderText("尊敬的用户你好,系统中您的凑集借款单还款计划查询失败,请联系我公司客服,我们将尽快解决");
			return;
		}
		Date now = new Date();
		long now_long = now.getTime();
		/**
		 * 最低的还款的时间保证-当前的还款时间如果小于还款计划的第一个还款计划的时间则肯定不行
		 */
		{
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan_list.get(0);
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			if (now_long < repay_start_time_long) {
				logger.error("hack user_id:" + user_id_of_borrower);
				renderText("当前时间小于第一个还款计划的还款开始时间");
				return;
			}
		}
		/**
		 * 检查该借款人是否已经都还款了
		 */
		final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
		final int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");// 需要关注份额之比
		final int total_periods = borrower_bulk_standard_repayment_plan_list.get(0).getInt("total_periods");
		final AtomicReference<String> error_code = new AtomicReference<String>("出问题了...就是不知道在哪出的,需要检查一下那个地方没有返回值");
		/**
		 * <pre>
		 * 优先关注逾期处理-系统已经进行严重逾期处理了-那么这部分的资金需要还给系统，
		 * 没有严重逾期处理的情况下面如果遇到处于严重逾期的单子我们需要提示系统该单子严重逾期了。
		 * </pre>
		 * 
		 * <pre>
		 * 具有严重逾期的标识,那么这部分还给系统
		 * 
		 * 当月存在还款计划
		 * ----------对于已经逾期的还款计划除了正常的本息-还有正常的管理费和逾期的管理费-还有罚息
		 * ----------对于当期的还款计划除了正常的本息-还有正常的管理费
		 * ----------对于之后的还款计划只需要本金即可
		 * 当月不存在还款计划,表示都逾期了
		 * ----------对于已经逾期的还款计划除了正常的本息-还有正常的管理费和逾期的管理费-还有罚息
		 * </pre>
		 * 
		 * 
		 * <pre>
		 * 没有严重逾期的标识,这这部分的本金和罚息是给理财人的，管理费是给系统的。-这个地方需要和向旋一样在数据库里面插入一条总结性的数据
		 * pay_by_borrow_or_system=2
		 * 
		 * 当月存在还款计划
		 * ----------对于已经逾期的还款计划除了正常的本息-还有正常的管理费和逾期的管理费-还有罚息
		 * ----------对于当期的还款计划除了正常的本息-还有正常的管理费
		 * ----------对于之后的还款计划只需要本金即可
		 * 当月不存在还款计划,表示都逾期了
		 * ----------对于已经逾期的还款计划除了正常的本息-还有正常的管理费和逾期的管理费-还有罚息
		 * </pre>
		 */
		/**
		 * 先计算出是否含有当期
		 */
		boolean is_in_the_normal_repayment_plan = false;
		long repayment_plan_id_of_this_month = 0;
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {

			long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if ((repay_start_time_long <= now_long && now_long <= repay_end_time_long)) {
				is_in_the_normal_repayment_plan = true;
				repayment_plan_id_of_this_month = repayment_plan_id;
				break;
			}
		}
		if (pay_for_object == 1) {
			logger.info("已经进行严重逾期处理");
			int count = 0;// 计算还有需要进行还款的?
			for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
				int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
				if (!(is_repay == 0 || is_repay == 1)) {
					logger.error("hack user_id:" + user_id_of_borrower);
					renderText("还款状态错误");
					return;
				}
				if (is_repay == 1) {
					continue;
				} else {
					count++;
				}

			}
			if (count == 0) {
				renderText("已经还款成功,请不要重复还款,但是系统记录该借款的还款状态为未成功还完，所以用户您需要联系我们的客服进行处理");
				return;
			}
			//
			BigDecimal all_repayment = new BigDecimal("0");
			// 全部本息
			BigDecimal all_principle_interest = new BigDecimal("0");
			// 全部本金
			BigDecimal all_principle = new BigDecimal("0");
			// 全部利息
			BigDecimal all_interest = new BigDecimal("0");
			// 全部正常的管理费
			BigDecimal all_normal_manage_fee = new BigDecimal("0");
			// 全部逾期的管理费
			BigDecimal all_over_manage_fee = new BigDecimal("0");
			// 全部罚息
			BigDecimal all_over_punish_interest = new BigDecimal("0");
			/**
			 * 根据上面判断当月是否在还款计划里面-做出不同的处理
			 */
			if (is_in_the_normal_repayment_plan) {// 不一定所有的还款都是逾期了的
				logger.info("含有当月");
				for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
					if (is_repay == 1) {
						continue;
					} else {
						// 还款日期
						Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
						String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
						int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);// 得到当前的还款期间
						if (!(repayment_period == 1 || repayment_period == 2 || repayment_period == 3 || repayment_period == 4)) {
							renderText("还款期间计算出错");
							return;
						}
						/**
						 * 需要关注正常的本息-正常的管理费-逾期的管理费-逾期的罚息三个部分
						 */
						// 应该要还的本金和利息
						BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
						BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
						BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");

						//
						// 正常管理费
						BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
						// 管理费-该公式里面已经计算了逾期管理费
						BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
						// 罚息-该公式里面已经计算了逾期罚息
						BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

						long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
						if (repayment_plan_id_of_this_month > repayment_plan_id) {
							if (!(repayment_period == 3 || repayment_period == 4)) {
								renderText("当月还款计划ID之前的还款周期计算错误");
								return;
							}
						} else if (repayment_plan_id_of_this_month == repayment_plan_id) {
							if (!(repayment_period == 1 || repayment_period == 2)) {
								renderText("当月还款计划的还款周期计算错误");
								return;
							}
							/**
							 *没有逾期管理费和罚息
							 */
							over_manage_fee = new BigDecimal("0");
							over_punish_interest = new BigDecimal("0");

						} else {// repayment_plan_id_of_this_month<repayment_plan_id
							if (!(repayment_period <= 2)) {
								renderText("当月还款计划之后的还款周期计算错误");
								return;
							}
							/**
							 * 没有利息和正常管理费和逾期管理费和罚息
							 */
							should_repayment_interest = new BigDecimal("0");
							normal_manage_fee = new BigDecimal("0");
							over_manage_fee = new BigDecimal("0");
							over_punish_interest = new BigDecimal("0");

						}
						// all
						{
							//
							all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
							all_principle = all_principle.add(should_repayment_principle);
							all_interest = all_interest.add(should_repayment_interest);
							all_normal_manage_fee = all_normal_manage_fee.add(normal_manage_fee);
							all_over_manage_fee = all_over_manage_fee.add(over_manage_fee);
							all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);

							BigDecimal actual_repayment = should_repayment_principle.add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
							all_repayment = all_repayment.add(actual_repayment);
						}

						if (Constants.devMode) {
							System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + borrower_bulk_standard_repayment_plan.getLong("id") + "   repayment_period:" + repayment_period);
							System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
							System.out.println("要还的本金" + should_repayment_principle);
							System.out.println("要还的利息" + should_repayment_interest);
							// System.out.println("正常管理费" + normal_manage_fee);
							// System.out.println("管理费[包含了逾期管理费]" +
							// over_manage_fee);
							System.out.println("31天内的罚息" + over_punish_interest);//
						}
					}

				}

			} else {// 都是逾期了的
				logger.info("不含有当月");
				for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
					if (is_repay == 1) {
						continue;
					} else {
						// 还款日期
						Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
						String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
						int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);// 得到当前的还款期间
						if (!(repayment_period == 1 || repayment_period == 2 || repayment_period == 3 || repayment_period == 4)) {
							renderText("还款期间计算出错");
							return;
						}
						if (!(repayment_period == 3 || repayment_period == 4)) {
							renderText("已经逾期了但是还是计算出来的不是逾期的状态");
							return;
						}

						/**
						 * 需要关注正常的本息-正常的管理费-逾期的管理费-逾期的罚息三个部分
						 */
						// 应该要还的本金和利息
						BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
						BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
						BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");

						//
						// 正常管理费
						BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
						// 管理费-该公式里面已经计算了逾期管理费
						BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
						// 罚息-该公式里面已经计算了逾期罚息
						BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

						//
						// all
						{
							//
							all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
							all_principle = all_principle.add(should_repayment_principle);
							all_interest = all_interest.add(should_repayment_interest);
							all_normal_manage_fee = all_normal_manage_fee.add(normal_manage_fee);
							all_over_manage_fee = all_over_manage_fee.add(over_manage_fee);
							all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);

							BigDecimal actual_repayment = should_repayment_principle.add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
							all_repayment = all_repayment.add(actual_repayment);
						}
						if (Constants.devMode) {
							System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + borrower_bulk_standard_repayment_plan.getLong("id") + "   repayment_period:" + repayment_period);
							System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
							System.out.println("要还的本金" + should_repayment_principle);
							System.out.println("要还的利息" + should_repayment_interest);
							// System.out.println("正常管理费" + normal_manage_fee);
							// System.out.println("管理费[包含了逾期管理费]" +
							// over_manage_fee);
							System.out.println("31天内的罚息" + over_punish_interest);//
						}

					}
				}

			}
			UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money(user_id_of_borrower);
			BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used");
			// BigDecimal cny_freeze_of_borrower =
			// user_now_money_of_borrower.getBigDecimal("cny_freeze");
			if (cny_can_used_of_borrower.compareTo(all_repayment) < 0) {
				renderText("资金不够");
				return;
			}

			// 全部本息
			final BigDecimal all_principle_interest_of_final = all_principle_interest;
			// 全部本金
			final BigDecimal all_principle_of_final = all_principle;
			// 全部利息
			final BigDecimal all_interest_of_final = all_interest;
			// 全部正常的管理费
			final BigDecimal all_normal_manage_fee_of_final = all_normal_manage_fee;
			// 全部逾期的管理费
			final BigDecimal all_over_manage_fee_of_final = all_over_manage_fee;
			// 全部罚息
			final BigDecimal all_over_punish_interest_of_final = all_over_punish_interest;
			final BigDecimal all_repayment_of_final = all_repayment;
			if (Constants.devMode) {
				System.out.println("全部本息" + all_principle_interest_of_final);
				System.out.println("全部本金" + all_principle_of_final);
				System.out.println("全部利息" + all_interest_of_final);
				System.out.println("全部罚息" + all_over_punish_interest_of_final);
				System.out.println(" all" + all_repayment_of_final);
			}
			boolean is_ok = Db.tx(new IAtomic() {
				public boolean transactionProcessing() throws Exception {

					/**
					 * <pre>
					 * 1借款人资金修改-如果是净值贷则需要把净值贷相关的标记去掉
					 * 2凑集单的状态需要修改为还款成功和借贷状态为0
					 * 3还款计划需要变成已经还款成功的状态
					 * 4需要把该借款人的借款状态变为0
					 * 6需要把严重逾期的状态对应的系统记录标记为已经由借款人还款成功了...需要建立表.....
					 * 7建立借款人的资金变化的记录....4种
					 * </pre>
					 */
					UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_of_borrower);
					BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used");
					BigDecimal cny_freeze_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_freeze");
					if (borrow_type == 3) {
						/**
						 * 清除净值贷的对应的提现的时候的标记
						 */
						user_now_money_of_borrower.set("net_amount", 0).set("total_net_cash", 0);
					}
					boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower.subtract(all_repayment_of_final)).update();
					if (!is_ok_update_user_now_money_of_borrower) {
						error_code.set("更新借款人资金失败");
						return false;
					}

					{
						/**
						 * 更新借款单还款状态和借贷状态
						 */
						boolean is_ok_update_of_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("payment_state", 3).set("payment_finish_time", new Date()).update();
						if (!is_ok_update_of_borrower_bulk_standard_gather_money_order) {
							error_code.set("更新借款单还款状态失败");
							return false;
						}
						// is_not_pay还款完成的时候需要修改is_not_pay =0,borrow_type=0
						boolean is_update_user_info_is_not_pay_ok = Db.update("UPDATE user_info SET is_not_pay =0,borrow_type=0 WHERE id=?", new Object[] { user_id_of_borrower }) >= 0;
						if (!is_update_user_info_is_not_pay_ok) {
							return false;
						}
					}
					/**
					 * 把所有的还款计划都变为已经还清
					 */
					for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
						int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
						if (is_repay == 1) {
							continue;
						} else {
							boolean is_ok_update_of_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.set("is_repay", 1).update();
							if (!is_ok_update_of_borrower_bulk_standard_repayment_plan) {
								error_code.set("更新还款计划状态失败");
								return false;
							}

						}
					}

					BorrowerBulkStandardRepaymentBySystemM borrower_bulk_standard_repayment_by_system = BorrowerBulkStandardRepaymentBySystemM.dao.findFirst("SELECT id,overdue_repayment_by_self_or_risk_money from borrower_bulk_standard_repayment_by_system WHERE gather_money_order_id=?", new Object[] { gather_money_order_id });
					if (borrower_bulk_standard_repayment_by_system == null) {
						error_code.set("没有查询到借款严重逾期处理记录");
						return false;
					} else {
						/**
						 * <pre>
						 * 0未处理，1借款人来支付付这部分的钱2风险资金来还这部分的钱,3风险资金先来支付这部分的钱，然后借款人同时也还款了
						 * </pre>
						 */
						int overdue_repayment_by_self_or_risk_money = borrower_bulk_standard_repayment_by_system.getInt("overdue_repayment_by_self_or_risk_money");

						if (overdue_repayment_by_self_or_risk_money == 0) {
							boolean is_ok_update_borrower_bulk_standard_repayment_by_system = borrower_bulk_standard_repayment_by_system.set("overdue_repayment_by_self_or_risk_money", 1).update();
							if (!is_ok_update_borrower_bulk_standard_repayment_by_system) {
								error_code.set("更新借款人严重逾期-资金被支付状态失败!");
								return false;
							}
						} else if (overdue_repayment_by_self_or_risk_money == 1) {
							error_code.set("更新借款人严重逾期-资金被支付状态失败!!-已经由借款人还了");
							return false;

						} else if (overdue_repayment_by_self_or_risk_money == 2) {
							boolean is_ok_update_borrower_bulk_standard_repayment_by_system = borrower_bulk_standard_repayment_by_system.set("overdue_repayment_by_self_or_risk_money", 3).update();
							if (!is_ok_update_borrower_bulk_standard_repayment_by_system) {
								error_code.set("更新借款人严重逾期-资金被支付状态失败!!!");
								return false;
							}

						} else if (overdue_repayment_by_self_or_risk_money == 3) {
							error_code.set("更新借款人严重逾期-资金被支付状态失败!!-已经由借款人和风险资金还了");
							return false;
						} else {
							error_code.set("更新借款人严重逾期-资金被支付状态失败!!-ELSE");
							return false;
						}
					}
					/**
					 * 资金变化
					 */
					/**
					 * 需要区分那些是打款给理财人或者系统的
					 * 
					 * <pre>
					 * sys_expenses_records no
					 * sys_income_records yes
					 * 
					 * user_money_change_records yes 
					 * user_transaction_records yes
					 * </pre>
					 */
					boolean is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, all_normal_manage_fee_of_final, "一次性还清正常管理费");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[正常管理费]失败");
						return false;
					}
					// 逾期管理费
					is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, all_over_manage_fee_of_final, "一次性还清逾期管理费");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[逾期管理费]失败");
						return false;
					}
					// 罚息
					is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_38, all_over_punish_interest_of_final, "一次性还清严重逾期罚息");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[严重逾期罚息]失败");
						return false;
					}

					/**
					 * 
					 * <pre>
					 * 向借款人打钱由两部分组成
					 * public static final int Type_31 = 31;// 打入借入者借款总额-收入
					 * @SysIncome
					 * public static final int Type_32 = 32;// 收取借入者借款服务费-支出
					 * // 向借款人收取本息由...部分组成.这个是正常还款的记录
					 * ------------------------------------------------------------------------
					 * public static final int Type_33 = 33;// 偿还本息-支出
					 * @SysIncome
					 * public static final int Type_34 = 34;// 返还服务费..特别收取-支出
					 * @SysIncome
					 * public static final int Type_35 = 35;// 借款管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_36 = 36;// 借款超期管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_37 = 37;// 普通罚息-1-30天内-支出[理财人]
					 * @SysIncome
					 * public static final int Type_38 = 38;// 严重超期罚息-大于30天-支出-[系统]
					 * 可能还有其他key的需要添加系统的收入
					 * </pre>
					 */
					// 用户资金变化
					boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_principle_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final),
							"一次性偿还本息#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_normal_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final), "一次性还清一般管理费#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[一般管理费]记录失败");
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final), "一次性还清逾期管理费#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[逾期管理费]记录失败");
						return false;
					}
					/**
					 * 一般罚息和严重罚息
					 */
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_37, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_punish_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final).subtract(all_over_punish_interest_of_final), "一次性还清普通罚息#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[普通罚息]记录失败");
						return false;
					}

					// 用户交易记录
					/**
					 * <pre>
					 * public static final int Type_31 = 31;// 借款服务费
					 * public static final int Type_32 = 32;// 偿还本息 
					 * public static final int Type_33 = 33;// 借款管理费
					 * public static final int Type_34 = 34;// 返还服务费 
					 * public static final int Type_35 = 35;//回收本息 
					 * public static final int Type_36 = 36;// 提前回收 
					 * public static final int Type_37 = 37;// 提前还款 
					 * public static final int Type_38 = 38;// 逾期管理费
					 * public static final int Type_39 = 39;// 逾期罚息
					 * </pre>
					 */
					boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_32, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_principle_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final), "一次性还清本息");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_normal_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final), "一次性还清借款管理费");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[借款管理费]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_38, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final), "一次性还清逾期管理费");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[逾期管理费]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_punish_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final).subtract(all_over_punish_interest_of_final), "一次性还清逾期罚息");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[逾期罚息]记录失败");
						return false;
					}

					boolean is_ok_add_borrower_bulk_standard_overdue_repayment_record = BorrowerBulkStandardOverdueRepaymentRecordM.add_borrower_bulk_standard_overdue_repayment_record_of_one_off(//
							gather_money_order_id,//
							0, // repayment_plan_id
							total_periods, // total_periods
							0,// current_period
							all_principle_interest_of_final, //
							all_principle_of_final, //
							all_interest_of_final,//
							all_normal_manage_fee_of_final,//
							all_over_manage_fee_of_final, //
							all_over_punish_interest_of_final,//
							all_repayment_of_final);//
					if (!is_ok_add_borrower_bulk_standard_overdue_repayment_record) {
						error_code.set("逾期还款过程中添加逾期记录失败");
						return false;
					}
					return true;

				}
			});

			if (!is_ok) {
				/**
				 * 不需要通知理财用户回款成功了
				 */
				renderText(error_code.get());
				return;
			} else {
				renderText("还款成功");
				return;
			}

		} else {
			logger.info("未已经进行严重逾期处理");
			/**
			 * 系统未进行严重逾期处理-按照公式进行提前还款处理-还给理财人的-如果出现了严重逾期则提示需要系统进行逾期的还款处理-
			 * 保证整个系统正常运转
			 */
			// 计算还有几个未还的还款计划
			/**
			 * 提前还款的公式-前提是没有严重逾期
			 * 
			 * <pre>
			 * 提前还款应还金额 = 剩余本金 + 当期本息及账户管理费 + 提前还款违约金[剩余本金的1 % 作为违约金]
			 * </pre>
			 * 
			 * 由于借款后还款按照正常的情况是没有严重逾期的，但是由于程序可能在某个时候严重逾期-
			 * 但是我们严重逾期的程序没有进行处理则需要先进行严重逾期处理。
			 * 如果存在当月含有还款计划,则需要对之前的还款计划进行逾期的处理，后面的按照上面的公式进行处理.
			 * 如果不存在当月的还款计划,则表示该借款人已经
			 */

			/**
			 * <pre>
			 * [1 2 3 4 5 6 7 8 9 ... 手动还款期间][28 29 30 31可能的几个自动还款日][32严重逾期]
			 * </pre>
			 */
			/**
			 * 检测是否含有严重逾期的单子存在-如果存在则提示用户联系客服-还款标识[pay_for_object]有问题
			 */
			for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {

				// 还款截至日期
				Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
				String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
				/**
				 * 检测是否有严重逾期的单子-但是凑集单的状态pay_for_object==0的情况需要通知管理员
				 */
				int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);
				if (borrower_bulk_standard_repayment_plan.getInt("is_repay") == 0 && repayment_period == 4) {// 距离还款的截至时间是大于31天的时间间隔,需要把pay_for_object变为1
					renderText("尊敬的用户你有一个还款计划已经处于严重逾期的状态，但是系统还是记录你是正常还款的状态，请通知我们，我们将尽快解决");
					return;
				}
			}
			int count = 0;// 计算还有需要进行还款的?
			for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
				int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
				if (!(is_repay == 0 || is_repay == 1)) {
					logger.error("hack user_id:" + user_id_of_borrower);
					renderText("还款状态错误");
					return;
				}
				if (is_repay == 1) {
					continue;
				} else {
					count++;
				}

			}
			if (count == 0) {
				renderText("已经还款成功,请不要重复还款,但是系统记录该借款的还款状态为未成功还完，所以用户您需要联系我们的客服进行处理");
				return;
			}
			BigDecimal all_repayment = new BigDecimal("0");
			// 全部本息
			BigDecimal all_principle_interest = new BigDecimal("0");
			// 全部本金
			BigDecimal all_principle = new BigDecimal("0");
			// 全部利息
			BigDecimal all_interest = new BigDecimal("0");
			// 全部正常的管理费
			BigDecimal all_normal_manage_fee = new BigDecimal("0");
			// 全部逾期的管理费
			BigDecimal all_over_manage_fee = new BigDecimal("0");
			// 全部罚息
			BigDecimal all_over_punish_interest = new BigDecimal("0");
			/**
			 * 给理财人的罚息
			 */
			// BigDecimal all_punish_interest_for_financial = new
			// BigDecimal("0");
			/**
			 * 需要考虑是否含有当月的情况
			 */
			if (is_in_the_normal_repayment_plan) {
				logger.info("含有当月");
				/**
				 * <pre>
				 * 如果含有当月-当月以前的还款计划如果没有还还则需要把响应的｛本息-正常的管理费-逾期的管理费-逾期的罚息｝{最多一期}还了。
				 * 当期
				 * </pre>
				 */
				for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
					if (is_repay == 1) {
						continue;
					} else {
						Date repay_actual_time = new Date();
						long repay_actual_time_long = repay_actual_time.getTime();
						borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
						borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

						// 还款日期
						Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
						String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
						// repayment_period==3普通逾期 repayment_period==4严重逾期
						int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);
						if (!(repayment_period == 1 || repayment_period == 2 || repayment_period == 3 || repayment_period == 4)) {
							renderText("还款期间计算错误");
							return;
						}
						/**
						 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
						 * 系统收取管理费用
						 */
						// 要还的本息
						// 应该要还的本金和利息
						BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
						// 要还的本金
						BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
						// 要还的利息
						BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
						// 正常管理费
						BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
						// 管理费[包含了逾期管理费]
						BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
						// 罚息
						BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

						long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
						if (repayment_plan_id < repayment_plan_id_of_this_month) {
							// repayment_period==3普通逾期 repayment_period==4严重逾期
							if (repayment_period != 3) {
								logger.error("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
								throw new RuntimeException("还款计划周期计算");
							} else if (repayment_period == 3) {

							} else {
								// return false;
								renderText("还款期间-else");
								return;
							}

						} else if (repayment_plan_id == repayment_plan_id_of_this_month) {
							/**
							 *没有逾期管理费和罚息
							 */
							over_manage_fee = new BigDecimal("0");
							over_punish_interest = new BigDecimal("0");
						} else {
							/**
							 * 没有利息和正常管理费和逾期管理费和罚息
							 */
							should_repayment_interest = new BigDecimal("0");
							normal_manage_fee = new BigDecimal("0");
							over_manage_fee = new BigDecimal("0");
							over_punish_interest = new BigDecimal("0");
						}

						// all
						{
							//
							all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
							all_principle = all_principle.add(should_repayment_principle);
							all_interest = all_interest.add(should_repayment_interest);
							all_normal_manage_fee = all_normal_manage_fee.add(normal_manage_fee);
							all_over_manage_fee = all_over_manage_fee.add(over_manage_fee);
							all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);

							BigDecimal actual_repayment = should_repayment_principle.add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
							all_repayment = all_repayment.add(actual_repayment);
						}
						if (Constants.devMode) {
							System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + borrower_bulk_standard_repayment_plan.getLong("id") + "   repayment_period:" + repayment_period);
							System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
							System.out.println("要还的本金" + should_repayment_principle);
							System.out.println("要还的利息" + should_repayment_interest);
							// System.out.println("正常管理费" + normal_manage_fee);
							// System.out.println("管理费[包含了逾期管理费]" +
							// over_manage_fee);
							System.out.println("31天内的罚息" + over_punish_interest);//
						}
					}

				}

			} else {
				logger.info("不含有当月");
				/**
				 * 表示之前的所有还款计划是存在逾期了-但是不是严重逾期
				 */
				/**
				 * 如果不含有当期，则可能是借款日是28号，刚刚现在还款计划的最后的一期，同时当前是某个月的3号。[即最后一期还款截止时间之后
				 * ] 那么则所有的还款计划如果没有还则都需要安装本金和罚息来算.
				 */

				for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
					if (is_repay == 1) {
						continue;
					} else {
						Date repay_actual_time = new Date();
						long repay_actual_time_long = repay_actual_time.getTime();
						borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
						borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

						// 还款日期
						Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
						String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
						// repayment_period==3普通逾期 repayment_period==4严重逾期
						int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);
						/**
						 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
						 * 系统收取管理费用
						 */
						// 要还的本息
						// 应该要还的本金和利息
						BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
						// 要还的本金
						BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
						// 要还的利息
						BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
						// 正常管理费
						BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
						// 管理费[包含了逾期管理费]
						BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
						// 罚息
						BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

						// repayment_period==3普通逾期 repayment_period==4严重逾期
						if (repayment_period != 3) {
							logger.error("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
							throw new RuntimeException("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
						} else if (repayment_period == 3) {

							// all
							{
								//
								all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
								all_principle = all_principle.add(should_repayment_principle);
								all_interest = all_interest.add(should_repayment_interest);
								all_normal_manage_fee = all_normal_manage_fee.add(normal_manage_fee);
								all_over_manage_fee = all_over_manage_fee.add(over_manage_fee);
								all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);

								BigDecimal actual_repayment = should_repayment_principle.add(should_repayment_interest).add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
								all_repayment = all_repayment.add(actual_repayment);
							}

						} else {
							// return false;
							renderText("还款期间-else");
							return;
						}
						if (Constants.devMode) {
							System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + borrower_bulk_standard_repayment_plan.getLong("id") + "   repayment_period:" + repayment_period);
							System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
							System.out.println("要还的本金" + should_repayment_principle);
							System.out.println("要还的利息" + should_repayment_interest);
							// System.out.println("正常管理费" + normal_manage_fee);
							// System.out.println("管理费[包含了逾期管理费]" +
							// over_manage_fee);
							System.out.println("31天内的罚息" + over_punish_interest);//
						}
					}

				}

			}

			UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money(user_id_of_borrower);
			BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used");
			if (cny_can_used_of_borrower.compareTo(all_repayment) < 0) {
				renderText("资金不够");
				return;
			}
			final BigDecimal all_repayment_of_final = all_repayment;
			// 全部本息
			final BigDecimal all_principle_interest_of_final = all_principle_interest;
			// 全部本金
			final BigDecimal all_principle_of_final = all_principle;
			// 全部利息
			final BigDecimal all_interest_of_final = all_interest;
			// 全部正常的管理费
			final BigDecimal all_normal_manage_fee_of_final = all_normal_manage_fee;
			// 全部逾期的管理费
			final BigDecimal all_over_manage_fee_of_final = all_over_manage_fee;
			// 全部罚息
			final BigDecimal all_over_punish_interest_of_final = all_over_punish_interest;// 给理财人的罚息

			final List<FinancialRecipientInformation> financial_recipient_information_list = new ArrayList<FinancialRecipientInformation>();
			boolean is_ok = Db.tx(new IAtomic() {
				/**
				 * 需要处理两部分的需求-理财人和借款人
				 */
				public boolean transactionProcessing() throws Exception {

					/**
					 * <pre>
					 * 1借款人资金修改-如果是净值贷则需要把净值贷相关的标记去掉
					 * 2凑集单的状态需要修改为还款成功和借贷状态为0
					 * 3还款计划需要变成已经还款成功的状态
					 * 4需要把该借款人的借款状态变为0
					 * 6需要把严重逾期的状态对应的系统记录若有则报错
					 * 7建立借款人的资金变化的记录....4种
					 * </pre>
					 */
					UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_of_borrower);
					BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used");
					BigDecimal cny_freeze_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_freeze");
					if (borrow_type == 3) {
						/**
						 * 清除净值贷的标记
						 */
						user_now_money_of_borrower.set("net_amount", 0).set("total_net_cash", 0);
					}
					boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower.subtract(all_repayment_of_final)).update();
					if (!is_ok_update_user_now_money_of_borrower) {
						error_code.set("更新借款人资金失败");
						return false;
					}
					{
						/**
						 * 更新借款单还款状态
						 */
						boolean is_ok_update_of_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("payment_state", 3).set("finish_time", new Date()).update();
						if (!is_ok_update_of_borrower_bulk_standard_gather_money_order) {
							error_code.set("更新借款单还款状态失败");
							return false;
						}
						// is_not_pay还款完成的时候需要修改is_not_pay =0,borrow_type=0
						boolean is_update_user_info_is_not_pay_ok = Db.update("UPDATE user_info SET is_not_pay =0,borrow_type=0 WHERE id=?", new Object[] { user_id_of_borrower }) >= 0;
						if (!is_update_user_info_is_not_pay_ok) {
							return false;
						}
					}
					/**
					 * 把所有的还款计划都变为已经还清
					 */
					for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
						int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
						if (is_repay == 1) {
							continue;
						} else {
							boolean is_ok_update_of_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.set("is_repay", 1).update();
							if (!is_ok_update_of_borrower_bulk_standard_repayment_plan) {
								error_code.set("更新还款计划状态失败");
								return false;
							}

						}
					}
					BorrowerBulkStandardRepaymentBySystemM borrower_bulk_standard_repayment_by_system = BorrowerBulkStandardRepaymentBySystemM.dao.findFirst("SELECT id,overdue_repayment_by_self_or_risk_money from borrower_bulk_standard_repayment_by_system WHERE gather_money_order_id=?", new Object[] { gather_money_order_id });
					if (borrower_bulk_standard_repayment_by_system != null) {
						error_code.set("在还款严重逾期表里面查询出了一条数据");
						return false;
					}
					/**
					 * 资金变化
					 */
					/**
					 * 需要区分那些是打款给理财人或者系统的
					 * 
					 * <pre>
					 * sys_expenses_records no
					 * sys_income_records yes
					 * 
					 * user_money_change_records yes 
					 * user_transaction_records yes
					 * </pre>
					 */
					boolean is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, all_normal_manage_fee_of_final, "一次性还清正常管理费");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[正常管理费]失败");
						return false;
					}
					// 逾期管理费
					is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, all_over_manage_fee_of_final, "一次性还清逾期管理费");
					if (!is_ok_add_sys_income_records) {
						error_code.set("添加财务收入部分[逾期管理费]失败");
						return false;
					}
					// 罚息-给理财人

					/**
					 * 
					 * <pre>
					 * 向借款人打钱由两部分组成
					 * public static final int Type_31 = 31;// 打入借入者借款总额-收入
					 * @SysIncome
					 * public static final int Type_32 = 32;// 收取借入者借款服务费-支出
					 * // 向借款人收取本息由...部分组成.这个是正常还款的记录
					 * ------------------------------------------------------------------------
					 * public static final int Type_33 = 33;// 偿还本息-支出
					 * @SysIncome
					 * public static final int Type_34 = 34;// 返还服务费..特别收取-支出
					 * @SysIncome
					 * public static final int Type_35 = 35;// 借款管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_36 = 36;// 借款超期管理费-支出-[系统]
					 * @SysIncome
					 * public static final int Type_37 = 37;// 普通罚息-1-30天内-支出[理财人]
					 * @SysIncome
					 * public static final int Type_38 = 38;// 严重超期罚息-大于30天-支出-[系统]
					 * 可能还有其他key的需要添加系统的收入
					 * </pre>
					 */
					// 用户资金变化
					boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_principle_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final),
							"一次性还清本息#凑集单id" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_normal_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final), "一次性还清一般管理费#凑集单id" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[一般管理费]记录失败");
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final), "一次性还清逾期管理费#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[逾期管理费]记录失败");
						return false;
					}
					/**
					 * 一般罚息和严重罚息
					 */
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_37, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_punish_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final).subtract(all_over_punish_interest_of_final), "一次性还清普通罚息#凑集单ID" + gather_money_order_id);
					if (!is_ok_add_user_money_change_records_by_type) {
						error_code.set("添加资金变化[普通罚息]记录失败");
						return false;
					}

					// 用户交易记录
					/**
					 * <pre>
					 * public static final int Type_31 = 31;// 借款服务费
					 * public static final int Type_32 = 32;// 偿还本息 
					 * public static final int Type_33 = 33;// 借款管理费
					 * public static final int Type_34 = 34;// 返还服务费 
					 * public static final int Type_35 = 35;//回收本息 
					 * public static final int Type_36 = 36;// 提前回收 
					 * public static final int Type_37 = 37;// 提前还款 
					 * public static final int Type_38 = 38;// 逾期管理费
					 * public static final int Type_39 = 39;// 逾期罚息
					 * </pre>
					 */
					boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_32, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_principle_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final), "一次性还清本息");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[偿还本息]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_normal_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final), "一次性还清借款管理费");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[借款管理费]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_38, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_manage_fee_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final), "一次性还清逾期管理费");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[逾期管理费]记录失败");
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), all_over_punish_interest_of_final, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(all_principle_interest_of_final).subtract(
							all_normal_manage_fee_of_final).subtract(all_over_manage_fee_of_final).subtract(all_over_punish_interest_of_final), "一次性还清一般逾期罚息");
					if (!is_ok_add_user_transaction_records) {
						error_code.set("添加交易记录[逾期罚息]记录失败");
						return false;
					}
					/**
					 * 本息和普通罚息给理财人
					 */
					BigDecimal each_principal_interest = all_principle_interest_of_final.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);
					BigDecimal each_principal = all_principle_of_final.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);
					BigDecimal each_interest = all_interest_of_final.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);
					BigDecimal each_punish_interest = all_over_punish_interest_of_final.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);

					// 到债权持有表中查找每个理财人对应的持有份数, 来增加他们资金表中的可用余额
					String sql_find_holders = "select id,user_id,hold_money,hold_share,transfer_share,is_transfer_all from lender_bulk_standard_creditor_right_hold where gather_money_order_id =? and hold_share!=0 ";
					List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find(sql_find_holders, new Object[] { gather_money_order_id });
					if (!Utils.isHasData(lender_bulk_standard_creditor_right_hold_list)) {
						error_code.set("根据凑集单的ID查询不到理财持有信息");
						return false;
					}
					// 还给理财人
					/**
					 * 还款的时候债权的价值以及对应的价格和手续费会相应的按照折价的系数修改-
					 * 同时系统需要一种机制告诉系统所有需要通过债权转让获取的债权持有的理财人暂时不能操作
					 */
					for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {
						long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");// id
						int financial_user_id = lender_bulk_standard_creditor_right_hold.getInt("user_id"); // 理财人id
						BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
						int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 原始购买的份额
						int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 转出的份额
						if (transfer_share != 0) {// 正在转让过程中需要对外面的债权转让实现冻结
							/**
							 *将该人的所有的正在债权转让的所有的单子都取消掉
							 */
							boolean is_ok_update_lender_bulk_standard_creditor_right_hold = lender_bulk_standard_creditor_right_hold.set("transfer_money", 0).set("transfer_share", 0).update();
							if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
								error_code.set("撤销债权转让单失败");
								return false;
							}
							boolean is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE creditor_right_hold_id=? and state=1", new Object[] { creditor_right_hold_id }) >= 0;
							if (!is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out) {
								error_code.set("撤销债权转让单失败");
								return false;
							}

						}
						// 应该收到的钱由两部分构成
						BigDecimal principal = each_principal.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本金
						BigDecimal interest = each_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的利息
						BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本息
						//						
						BigDecimal punish_interest = each_punish_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的罚息
						// 应该回收的金额:principal_interest+punish_interest
						BigDecimal recieve_money = principal_interest.add(punish_interest);

						UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(financial_user_id);
						BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
						BigDecimal cny_freeze_of_lender = user_now_money_of_lender.getBigDecimal("cny_freeze");
						/**
						 * 理财人回款
						 */
						boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender.add(recieve_money)).update();
						if (!is_ok_update_user_now_money_of_lender) {
							error_code.set("更新理财人资金信息失败");
							return false;
						}
						/**
						 * 需要记录资金信息
						 * 
						 * <pre>
						 * sys_expenses_records no
						 * sys_income_records no
						 * 
						 * user_money_change_records yes 
						 * user_transaction_records yes
						 * </pre>
						 */
						// user_money_change_records
						is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_74, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "一次性回收本息#凑集单ID" + gather_money_order_id);
						if (!is_ok_add_user_money_change_records_by_type) {
							error_code.set("更新理财人资金信息[回收本息]失败,为了避免重复还款请刷新页面");
							return false;
						}
						is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_76, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息#凑集单ID"
								+ gather_money_order_id);
						if (!is_ok_add_user_money_change_records_by_type) {
							error_code.set("更新理财人资金信息[逾期产生的罚息]失败,为了避免重复还款请刷新页面");
							return false;
						}
						// user_transaction_records
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(financial_user_id, UserTransactionRecordsM.Type_32, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "一次性回收本息");
						if (!is_ok_add_user_transaction_records) {
							error_code.set("更新理财人资金信息[回收本息]失败,为了避免重复还款请刷新页面");
							return false;
						}
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records(financial_user_id, UserTransactionRecordsM.Type_39, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "一次性均摊借款人逾期的罚息");
						if (!is_ok_add_user_transaction_records) {
							error_code.set("更新理财人资金信息[逾期产生的罚息]失败,为了避免重复还款请刷新页面");
							return false;
						}

						/**
						 * 添加回款的财务记录
						 */
						boolean is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record = LenderBulkStandardCreditorRightHoldRepaymentRecordM.add_lender_bulk_standard_creditor_right_hold_repayment_record(//
								financial_user_id,//
								user_id_of_borrower,//
								gather_money_order_id, //
								borrow_type, //
								borrower_nickname,//
								0, // repayment_plan_id
								total_periods, //
								0,// current_period
								creditor_right_hold_id,//
								hold_money, //
								hold_share,//
								principal,//
								interest,//
								principal_interest,//
								punish_interest, //
								recieve_money,//
								new Date());//
						if (!is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record) {
							error_code.set("更新理财人回款记录失败,请你刷新页面重新还款");
							return false;
						}
						// 记录进financial_recipient_information_list
						FinancialRecipientInformation financial_recipient_information = new FinancialRecipientInformation(//
								financial_user_id,//
								gather_money_order_id,//
								user_id_of_borrower,//
								0, //
								total_periods, //
								0, //
								creditor_right_hold_id, //
								hold_money,//
								hold_share, //
								principal_interest, //
								punish_interest,//
								new Date(), //
								new Date().getTime(),//
								borrow_title);//
						financial_recipient_information_list.add(financial_recipient_information);
					}// for
					/**
					 * 让所有理财人的持有失效 不再系统的处理流程中进行业务流转
					 */
					boolean is_ok_set_all_is_not_need_handle = LenderBulkStandardCreditorRightHoldLogM.set_all_is_not_need_handle(gather_money_order_id);
					if (!is_ok_set_all_is_not_need_handle) {
						return false;
					}
					return true;

				}
			});
			if (is_ok) {
				if (Utils.isHasData(financial_recipient_information_list)) {
					SendBackTheSuccessfulInformation.send_back_the_successful_information_of_all_with_overdue(financial_recipient_information_list);
				}
				renderText("还款成功");
				return;
			} else {
				renderText(error_code.get());
				return;
			}

		}

	}
}

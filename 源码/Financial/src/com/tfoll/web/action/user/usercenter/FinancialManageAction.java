package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldRepaymentRecordM;
import com.tfoll.web.model.LenderBulkStandardNewerBidGatherM;
import com.tfoll.web.model.LenderBulkStandardNewerBidRecordM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.CommonRepayMethod;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ActionKey("/user/usercenter/financial_manage")
// 个人信息-->理财模块的Action
public class FinancialManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  我的债权页面", last_update_author = "lh")
	public void to_my_debt() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		Map<String, Object> financial_account_asset = CommonRepayMethod.get_elite_bid_account(user_id);
		setAttribute("financial_account_asset", financial_account_asset);

		renderJsp("/user/usercenter/financial_manage/account_my_debt.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  债权转让页面", last_update_author = "lh")
	public void to_debt_transfer() {

		UserM user = getSessionAttribute("user");
		final int user_id = user.getInt("id");
		String select_transfering_debt_sql = "select * from lender_bulk_standard_order_creditor_right_transfer_out t where t.creditor_right_hold_user_id = ? and t.state = 1 order by t.id desc";
		List<Record> record_list = Db.find(select_transfering_debt_sql, new Object[] { user_id });
		List<Record> list = new ArrayList<Record>();
		if (Utils.isHasData(record_list)) {
			for (Record record : record_list) {
				Long id = record.getLong("id");// 转让ID
				Long gather_money_order_id = record.getLong("gather_money_order_id");// 借款方凑集单ID(债权ID)
				// 查询当前剩余期数
				String select_repayment_plan_sql = "SELECT t.total_periods AS total_periods, t.current_period AS current_period, t.automatic_repayment_date AS automatic_repayment_date FROM borrower_bulk_standard_repayment_plan t WHERE t.gather_money_order_id = ? AND t.is_repay = 0 LIMIT 1";
				Record standard_repayment_plan_record = Db.findFirst(select_repayment_plan_sql, new Object[] { gather_money_order_id });//
				int current_period = standard_repayment_plan_record.getInt("current_period");
				int total_periods = standard_repayment_plan_record.getInt("total_periods");
				int remain_periods = (total_periods - current_period) + 1;// 剩余期数
				// 查询年利率
				String rate_sql = "select t.annulized_rate_int as annulized_rate_int,t.borrow_type as borrow_type from borrower_bulk_standard_gather_money_order t where t.id = ?";
				Record rate_record = Db.findFirst(rate_sql, new Object[] { gather_money_order_id });
				// 年化利率
				int annulized_rate_int = rate_record.getInt("annulized_rate_int");
				// 借款类型 1消费贷，2 生意贷，3 净值贷
				int borrow_type = rate_record.getInt("borrow_type");
				// 当前债权价值
				BigDecimal current_creditor_right_value = record.getBigDecimal("creditor_right_value").setScale(2, BigDecimal.ROUND_DOWN);
				// 转让价格
				BigDecimal transfer_price = record.getBigDecimal("transfer_price").setScale(2, BigDecimal.ROUND_DOWN);// 转让价格（元/份）
				// 转让系数
				BigDecimal transfer_factor = record.getBigDecimal("transfer_factor").multiply(new BigDecimal("100")).setScale(2, BigDecimal.ROUND_DOWN);
				// 要转让的份数
				int transfer_share = record.getInt("transfer_share");
				// 剩下没有转让的份数
				int transfer_share_remainder = record.getInt("transfer_share_remainder");

				//
				Record red = new Record();
				red.add("id", id);// 转让ID
				red.add("borrow_type", borrow_type);// 借款类型
				red.add("gather_money_order_id", gather_money_order_id);// 债权ID(筹集单ID)
				red.add("remain_periods", remain_periods + "/" + total_periods);// 剩余期数
				red.add("annulized_rate_int", annulized_rate_int + "%");// 年化利率
				red.add("current_creditor_right_value", current_creditor_right_value);// 当前债权价值
				red.add("transfer_price", transfer_price);// 转让价格
				red.add("transfer_factor", transfer_factor + "%");// 转让系数
				red.add("remain_share", transfer_share_remainder + "/" + transfer_share);// 剩余份数
				list.add(red);
			}
			setAttribute("list", list);
		} else {
			setAttribute("list", list);
		}
		/**
		 * 查询成功转入总金额
		 */

		String transfer_in_all_money_sql = "SELECT IFNULL( SUM(t.transfer_all_money), 0 ) AS total_transfer_in_all_money, IFNULL(SUM(t.transfer_share), 0) AS total_transfer_share, IFNULL( SUM(t.should_count_interest), 0 ) AS total_should_count_interest FROM lender_bulk_standard_order_creditor_right_transfer_in t WHERE t.transfer_in_user_id = ?";
		Record transfer_all_money_in_record = Db.findFirst(transfer_in_all_money_sql, new Object[] { user_id });
		// 成功转让金额
		BigDecimal total_transfer_in_all_money = transfer_all_money_in_record.getBigDecimal("total_transfer_in_all_money").setScale(2, BigDecimal.ROUND_DOWN);
		// 成功转让债权份数
		BigDecimal total_transfer_in_share = transfer_all_money_in_record.getBigDecimal("total_transfer_share");
		// 成功转入时的盈亏
		BigDecimal total_should_count_interest = transfer_all_money_in_record.getBigDecimal("total_should_count_interest").setScale(2, BigDecimal.ROUND_DOWN);
		setAttribute("total_transfer_in_all_money", total_transfer_in_all_money);
		setAttribute("total_transfer_in_share", total_transfer_in_share);
		setAttribute("total_should_count_interest", total_should_count_interest);

		/**
		 * 查询成功转出总金额
		 */
		String transfer_out_all_money_sql = "SELECT IFNULL( SUM(t1.transfer_all_money), 0 ) AS total_transfer_out_all_money, IFNULL(SUM(t1.transfer_share), 0) AS total_transfer_share, IFNULL(SUM(t1.transfer_fee), 0) AS total_transfer_fee FROM lender_bulk_standard_order_creditor_right_transfer_in t1 WHERE t1.transfer_out_user_id = ?";
		Record transfer_all_money_out_record = (Record) Db.findFirst(transfer_out_all_money_sql, new Object[] { user_id });
		// 成功转出金额
		BigDecimal total_transfer_out_all_money = transfer_all_money_out_record.getBigDecimal("total_transfer_out_all_money").setScale(2, BigDecimal.ROUND_DOWN);
		// 成功转出份数
		BigDecimal total_transfer_out_share = transfer_all_money_out_record.getBigDecimal("total_transfer_share");
		// 转出管理费(手续费)
		BigDecimal total_transfer_out_fee = transfer_all_money_out_record.getBigDecimal("total_transfer_fee").setScale(2, BigDecimal.ROUND_DOWN);
		;
		setAttribute("total_transfer_out_all_money", total_transfer_out_all_money);
		setAttribute("total_transfer_out_share", total_transfer_out_share);

		/**
		 * 查询债权折溢价交易盈亏
		 */
		String select_debt_discount_less_sql = "SELECT IFNULL( SUM( T.transfer_debt_value * (1 - T.transfer_factor) * T.transfer_share ), 0 ) AS discount_less FROM ( SELECT t1.transfer_debt_value AS transfer_debt_value, t1.transfer_share AS transfer_share, t2.transfer_factor AS transfer_factor FROM lender_bulk_standard_order_creditor_right_transfer_in t1, lender_bulk_standard_order_creditor_right_transfer_out t2 WHERE t1.transfer_out_order_id = t2.id AND t1.transfer_out_user_id = ? ) AS T";
		BigDecimal debt_discount_less = Db.queryBigDecimal(select_debt_discount_less_sql, new Object[] { user_id });
		// 债权转让盈亏
		BigDecimal total_debt_discount_less = new BigDecimal("0.00");
		if (total_transfer_out_fee != null && debt_discount_less != null) {
			// 债权转让盈亏 = 债权折溢价交易盈亏 +　转出管理费(手续费)
			total_debt_discount_less = total_transfer_out_fee.add(debt_discount_less).setScale(2, BigDecimal.ROUND_DOWN);
		}
		setAttribute("total_debt_discount_less", total_debt_discount_less);
		/**
		 * 查询转出中的份额和数量
		 */
		String select_transfering_count_share_sql = "SELECT IFNULL( SUM(t.transfer_share_remainder), 0 ) AS transfer_share_remainder, COUNT(1) AS debt_out_count FROM lender_bulk_standard_order_creditor_right_transfer_out t WHERE t.creditor_right_hold_user_id = ? AND t.state = 1";
		Record transfering_count_share_record = Db.findFirst(select_transfering_count_share_sql, new Object[] { user_id });
		// 份额
		BigDecimal transfer_share_remainder = transfering_count_share_record.getBigDecimal("transfer_share_remainder");
		// 数量
		Long debt_out_count = transfering_count_share_record.getLong("debt_out_count");
		setAttribute("transfer_share_remainder", transfer_share_remainder);
		setAttribute("debt_out_count", debt_out_count);
		renderJsp("/user/usercenter/financial_manage/debt_transfer.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  理财统计页面", last_update_author = "lh")
	public void to_finance_statistics() {
		renderJsp("/user/usercenter/financial_manage/finance_statistics.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "用户中心 理财管理  回帐查询页面", last_update_author = "lh")
	public void to_recieve_money() {

		UserM user = getSessionAttribute("user");
		int lender_user_id = user.getInt("id");

		String sql = "select * from lender_bulk_standard_creditor_right_hold_repayment_record where lender_user_id = ?";
		List<LenderBulkStandardCreditorRightHoldRepaymentRecordM> lender_bulk_standard_creditor_right_hold_repayment_record_list = LenderBulkStandardCreditorRightHoldRepaymentRecordM.dao.find(sql, new Object[] { lender_user_id });

		BigDecimal to_collect_pricipal_interest = (BigDecimal) CommonRepayMethod.get_elite_bid_account(lender_user_id).get("to_collect_pricipal_interest");

		List<Map<String, Object>> repayment_record_list = new ArrayList<Map<String, Object>>();
		for (LenderBulkStandardCreditorRightHoldRepaymentRecordM lender_bulk_standard_creditor_right_hold_repayment_record : lender_bulk_standard_creditor_right_hold_repayment_record_list) {
			Map<String, Object> repayment_record = lender_bulk_standard_creditor_right_hold_repayment_record.getM();
			BigDecimal principal_interest = lender_bulk_standard_creditor_right_hold_repayment_record.getBigDecimal("principal_interest");
			principal_interest = principal_interest.setScale(2, BigDecimal.ROUND_DOWN);
			repayment_record.put("principal_interest", principal_interest);
			repayment_record_list.add(repayment_record);

		}

		setAttribute("repayment_record_list", repayment_record_list);
		setAttribute("to_collect_pricipal_interest", to_collect_pricipal_interest);

		renderJsp("/user/usercenter/financial_manage/recieve_money.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "获取投标中的债权", last_update_author = "lh")
	public void get_bidding_debt() {

		// int lend_user_id = getParameterToInt("lend_user_id");
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		/* 债权ID(其实是理财单id) 原始投资金额 年利率 期限 信用等级 剩余时间 投标进度 状态 */
		String sql = "SELECT b.id as gather_id, a.invest_money,a.invest_share, b.annulized_rate,b.annulized_rate_int, b.borrow_duration, b.credit_rating,b.deadline, b.gather_progress FROM lender_bulk_standard_order a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND a.lend_user_id = ? AND a.state = 1";
		List<Record> bidding_debt_record_list = Db.find(sql, new Object[] { user_id });
		List<Map<String, Object>> bidding_debt_list = new ArrayList<Map<String, Object>>();
		for (Record bidding_debt_record : bidding_debt_record_list) {
			Date deadline = bidding_debt_record.getTimestamp("deadline");
			Date now = new Date();
			int remain_second = (int) ((deadline.getTime() - now.getTime()) / 1000);

			int day = remain_second / (3600 * 24);
			int hour = (remain_second % (3600 * 24)) / 3600;
			int minute = (remain_second % 3600) / 60;
			int second = remain_second % 60;
			String remain_time_str = day + "天" + hour + "时" + minute + "分";

			Map<String, Object> record_inside = bidding_debt_record.getR();
			record_inside.put("remain_time_str", remain_time_str);

			bidding_debt_list.add(record_inside);
		}

		// 获取投标中的的新手标
		String sql_newer = "SELECT * FROM lender_bulk_standard_newer_bid_gather WHERE id = ( SELECT newer_bid_gather_id FROM lender_bulk_standard_newer_bid_record WHERE user_id = ? ) AND is_full = 0";
		LenderBulkStandardNewerBidGatherM lender_bulk_standard_newer_bid_gather = LenderBulkStandardNewerBidGatherM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> newer_bid_gather = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_gather != null) {
			newer_bid_gather = lender_bulk_standard_newer_bid_gather.getM();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bidding_debt_list", bidding_debt_list);

		// 没有投标中的新手标
		map.put("bidding_newer_bid", new HashMap<String, Object>());

		Gson gson = new Gson();
		String str = gson.toJson(map);

		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "获取已结清的债权", last_update_author = "lh")
	public void get_pay_off_debt() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		// 已经结清的新手精英标
		String sql_newer = "SELECT * FROM `lender_bulk_standard_newer_bid_record` where user_id = ? and is_return_interest = 1";
		LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = LenderBulkStandardNewerBidRecordM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> pay_off_newer_bid = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_record != null) {
			pay_off_newer_bid = lender_bulk_standard_newer_bid_record.getM();
		}

		// 接下来获取已结清精英散标list
		/*
		 * 债权ID 投资金额 年利率 回收金额 已赚金额 结清日期 结清方式
		 */
		List<Map<String, Object>> pay_off_debt_list = new ArrayList<Map<String, Object>>();
		String sql_pay_off_debt_id_list = "SELECT gather_money_order_id from lender_bulk_standard_creditor_right_hold where user_id = ? and gather_money_order_id in (select id from borrower_bulk_standard_gather_money_order where payment_state = 3)";
		// 该用户持有的 已结清的债权id list
		List<Long> debt_id_list = Db.query(sql_pay_off_debt_id_list, new Object[] { user_id });
		for (long debt_id : debt_id_list) {
			String sql_creditor_right_hold = "select hold_share from lender_bulk_standard_creditor_right_hold where gather_money_order_id = ? and user_id = ? ";
			Record record = Db.findFirst(sql_creditor_right_hold, new Object[] { debt_id, user_id });
			int hold_share = record.getInt("hold_share"); //
			// 投资金额，通过投标来的 用持有债权份数的50倍计算
			// 通过债权转让来的怎么查（计算）？@Q
			int invest_money = hold_share * 50;
			record.add("invest_money", invest_money);
			record.add("hold_share", hold_share);

			record.add("debt_id", debt_id);

			String sql_gather_order_money = "select annulized_rate_int,borrow_type from borrower_bulk_standard_gather_money_order where id = ? ";
			Record gather_order_money = Db.findFirst(sql_gather_order_money, new Object[] { debt_id });
			// 年化利率
			int annulized_rate_int = gather_order_money.getInt("annulized_rate_int");
			record.add("annulized_rate_int", annulized_rate_int);
			//
			int borrow_type = gather_order_money.getInt("borrow_type");
			record.add("borrow_type", borrow_type);

			String sql_creditor_right_hold_repayment_record = "select sum(principal_interest) as sum_principal_interest ,sum(interest) as sum_interest from lender_bulk_standard_creditor_right_hold_repayment_record where lender_user_id = ? and gather_money_order_id = ? ";
			// 回收金额
			Record creditor_right_hold_repayment_record = Db.findFirst(sql_creditor_right_hold_repayment_record, new Object[] { user_id, debt_id });
			BigDecimal sum_principal_interest = creditor_right_hold_repayment_record.getBigDecimal("sum_principal_interest");
			sum_principal_interest = sum_principal_interest.setScale(2, BigDecimal.ROUND_DOWN);
			record.add("sum_principal_interest", sum_principal_interest);
			// 已赚金额
			BigDecimal sum_interest = creditor_right_hold_repayment_record.getBigDecimal("sum_interest");
			sum_interest = sum_interest.setScale(2, BigDecimal.ROUND_DOWN);
			record.add("sum_interest", sum_interest);

			String sql_ = "select add_time from lender_bulk_standard_creditor_right_hold_repayment_record where lender_user_id = ? and gather_money_order_id = ? order by add_time desc limit 1";
			Date pay_off_time = Db.queryTimestamp(sql_, new Object[] { user_id, debt_id });
			// 结清日期
			String pay_off_time_str = Model.Date.format(pay_off_time);
			record.add("pay_off_time_str", pay_off_time_str);

			pay_off_debt_list.add(record.getR());
		}

		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pay_off_debt_list", pay_off_debt_list);
		map.put("pay_off_newer_bid", pay_off_newer_bid);
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "获取回收中的债权", last_update_author = "lh")
	public void get_repaying_debt() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		/*
		 * 重新查询 回收中的债权
		 */

		/*
		 * -- 获取回收中的债权(2) (指定债权持有表的id) -- --O 债权ID --O 原始投资金额(hold_share*50) --O
		 * 年利率(筹集单) -- 待收本息(筹集单的总份数，还款计划中的未还) -- 月收本息(筹集单) -- 期数(还款计划) -- 下个还款日
		 * (还款计划)
		 * 
		 * -- 待收本金(筹集)-- -- 待收利息(筹集) -- 状态 --
		 */
		// 原始投资金额 ， 债权id
		String sql_creditor_right_hold = "select id as creditor_right_hold_id , hold_share,transfer_share, gather_money_order_id from lender_bulk_standard_creditor_right_hold where user_id = ? and gather_money_order_id in (select id from borrower_bulk_standard_gather_money_order where payment_state != 3) ";
		List<Record> record_list = Db.find(sql_creditor_right_hold, new Object[] { user_id });

		List<Map<String, Object>> repaying_debt_list = new ArrayList<Map<String, Object>>();

		for (Record record : record_list) {
			long gather_order_id = record.getLong("gather_money_order_id");// 债权id

			BigDecimal debt_value = new BigDecimal("0.00");// 债权价值

			int hold_share = record.getInt("hold_share");// 持有份额
			int origin_money = hold_share * 50;// 原始投资金额
			record.add("origin_money", origin_money);

			// 年利率 ， 月收本息
			String sql_gather_order = "select finish_time, borrow_type, annulized_rate_int, borrow_all_share, monthly_principal_and_interest from borrower_bulk_standard_gather_money_order where id = ?";
			BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst(sql_gather_order, new Object[] { gather_order_id });
			int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");// 借款人的借款总额

			// 如果当前系统时间-满标时间>90天 ，则去计算债权价值
			Date finish_time = borrower_bulk_standard_gather_money_order.getTimestamp("finish_time");
			long finish_time_int = ((System.currentTimeMillis() - finish_time.getTime()) / (Day));

			boolean can_transfer; // 是否可以债权转让
			if (finish_time_int > 89) {
				can_transfer = true;
			} else {
				can_transfer = false;
			}
			record.add("can_transfer", can_transfer);

			if (finish_time_int >= 90) {
				debt_value = CalculationFormula.get_debt_value(gather_order_id).divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);
				debt_value = debt_value.setScale(2, BigDecimal.ROUND_DOWN);
			}
			record.add("debt_value", debt_value);

			int annulized_rate_int = borrower_bulk_standard_gather_money_order.getInt("annulized_rate_int");// 年利率
			record.add("annulized_rate_int", annulized_rate_int);
			BigDecimal monthly_principal_and_interest = borrower_bulk_standard_gather_money_order.getBigDecimal("monthly_principal_and_interest");// 月还本息
			int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");
			record.add("borrow_type", borrow_type);

			String sql_money = "SELECT SUM(should_repayment_total) as sum_should_repayment_total,SUM(should_repayment_principle) as sum_should_repayment_principle ,SUM(should_repayment_interest) as sum_should_repayment_interest FROM borrower_bulk_standard_repayment_plan WHERE is_repay = 0 AND gather_money_order_id = ?";
			Record money_record = Db.findFirst(sql_money, new Object[] { gather_order_id });

			String sql_date = "select current_period,total_periods,automatic_repayment_date from borrower_bulk_standard_repayment_plan where ? < repay_end_time and gather_money_order_id = ? limit 1";
			Record date_record = Db.findFirst(sql_date, new Object[] { new Date(), gather_order_id });

			int current_period = date_record.getInt("current_period");
			int total_periods = date_record.getInt("total_periods");
			// 剩余期数
			int remain_period = total_periods - current_period;
			String automatic_repayment_date = date_record.getString("automatic_repayment_date");

			record.add("current_period", current_period);
			record.add("total_periods", total_periods);
			record.add("remain_period", remain_period);
			record.add("automatic_repayment_date", automatic_repayment_date);

			BigDecimal sum_should_repayment_total = money_record.getBigDecimal("sum_should_repayment_total");// 待还总额
			BigDecimal sum_should_repayment_principle = money_record.getBigDecimal("sum_should_repayment_principle");// 待还本金
			BigDecimal sum_should_repayment_interest = money_record.getBigDecimal("sum_should_repayment_interest");// 待还利息

			// 每份的 待收本息
			BigDecimal daishou_benxi_per_share = sum_should_repayment_total.divide(new BigDecimal(borrow_all_share), 2, BigDecimal.ROUND_DOWN);

			/* 待收本息 月收本息 待收本金 待收利息 */
			BigDecimal sum_should_recieve_total = sum_should_repayment_total.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);
			BigDecimal recieve_monthly_principal_and_interest = monthly_principal_and_interest.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);
			BigDecimal sum_should_recieve_principle = sum_should_repayment_principle.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);
			BigDecimal sum_should_recieve_interest = sum_should_repayment_interest.multiply(new BigDecimal(hold_share)).divide(new BigDecimal(borrow_all_share), 10, BigDecimal.ROUND_DOWN);

			sum_should_recieve_total = sum_should_recieve_total.setScale(2, BigDecimal.ROUND_DOWN);
			recieve_monthly_principal_and_interest = recieve_monthly_principal_and_interest.setScale(2, BigDecimal.ROUND_DOWN);
			sum_should_recieve_principle = sum_should_recieve_principle.setScale(2, BigDecimal.ROUND_DOWN);
			sum_should_recieve_interest = sum_should_recieve_interest.setScale(2, BigDecimal.ROUND_DOWN);

			record.add("sum_should_recieve_total", sum_should_recieve_total);
			record.add("recieve_monthly_principal_and_interest", recieve_monthly_principal_and_interest);
			record.add("sum_should_recieve_principle", sum_should_recieve_principle);
			record.add("sum_should_recieve_interest", sum_should_recieve_interest);
			record.add("daishou_benxi_per_share", daishou_benxi_per_share);

			repaying_debt_list.add(record.getR());

		}

		/* ************************************************************************************************************** *
		 * String sql =
		 * "SELECT * FROM ( SELECT b.id AS debt_id, a.hold_money, a.hold_share, a.transfer_share, b.annulized_rate, b.annulized_rate_int, b.borrow_type, c.total_periods, c.current_period, b.borrow_all_share, c.should_repayment_total,c.automatic_repayment_date, c.repay_end_time FROM lender_bulk_standard_creditor_right_hold a, borrower_bulk_standard_gather_money_order b, borrower_bulk_standard_repayment_plan c WHERE a.gather_money_order_id = b.id AND c.gather_money_order_id = b.id AND b.payment_state = 2 AND a.user_id = ? AND ? < c.repay_end_time ) AS new_table WHERE current_period = ( SELECT min(current_period) FROM ( SELECT b.id AS debt_id, a.hold_share, b.annulized_rate, b.annulized_rate_int, b.borrow_type, c.total_periods, c.current_period, b.borrow_all_share, c.should_repayment_total,c.automatic_repayment_date, c.repay_end_time FROM lender_bulk_standard_creditor_right_hold a, borrower_bulk_standard_gather_money_order b, borrower_bulk_standard_repayment_plan c WHERE a.gather_money_order_id = b.id AND c.gather_money_order_id = b.id AND b.payment_state = 2 AND a.user_id = ? AND ? < c.repay_end_time ) AS new_table )"
		 * ; List<Record> repaying_debt_record_list = Db.find(sql, new Object[]
		 * { user_id, new Date(), user_id, new Date() });
		 * 
		 * List<Map<String, Object>> repaying_debt_list = new
		 * ArrayList<Map<String, Object>>(); for (Record repaying_debt_record :
		 * repaying_debt_record_list) { Map<String, Object> repaying_debt =
		 * repaying_debt_record.getR(); int hold_share =
		 * repaying_debt_record.getInt("hold_share");// 持有的份数 BigDecimal
		 * should_repayment_total =
		 * repaying_debt_record.getBigDecimal("should_repayment_total");//
		 * 借款人要还的总额 int borrow_all_share =
		 * repaying_debt_record.getInt("borrow_all_share");// 总份数 int
		 * total_periods = repaying_debt_record.getInt("total_periods");// 总期数
		 * long current_period =
		 * repaying_debt_record.getLong("current_period");// 下次还款期数
		 * 
		 * int origin_money = hold_share * 50;// 原始投资金额 // 月收本息 借款人要还的本息 * (
		 * 持有的份数 / 总份数 ) double month_benxi =
		 * should_repayment_total.doubleValue() * (new Double(hold_share) /
		 * borrow_all_share); double remian_benxi = month_benxi * (total_periods
		 * - current_period + 1);// 待收本息
		 * 
		 * repaying_debt.put("origin_money", origin_money);
		 * repaying_debt.put("month_benxi", month_benxi);
		 * repaying_debt.put("remian_benxi", remian_benxi);
		 * 
		 * repaying_debt_list.add(repaying_debt); }
		 */

		// 获取回收中的新手标
		String sql_newer = "SELECT * FROM lender_bulk_standard_newer_bid_record where user_id = ? and is_return_interest = 0";
		LenderBulkStandardNewerBidRecordM lender_bulk_standard_newer_bid_record = LenderBulkStandardNewerBidRecordM.dao.findFirst(sql_newer, new Object[] { user_id });
		Map<String, Object> repaying_newer_bid = new HashMap<String, Object>();
		if (lender_bulk_standard_newer_bid_record != null) {
			repaying_newer_bid = lender_bulk_standard_newer_bid_record.getM();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("repaying_debt_list", repaying_debt_list);
		map.put("repaying_newer_bid", repaying_newer_bid);
		Gson gson = new Gson();
		String str = gson.toJson(map);

		renderText(str);
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { Ajax.class, UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "获取已转出的债权", last_update_author = "lh")
	public void get_transfered_out_debt() {

		/*
		 * 债权ID 成交份数 转出时债权总价值 转出时总成交金额 实际收入 交易费用 转让盈亏
		 */

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql = "select * from lender_bulk_standard_order_creditor_right_transfer_in where transfer_out_user_id = ? ";
		List<Record> record_list = Db.find(sql, new Object[] { user_id });

		List<Map<String, Object>> transfered_out_debt_list = new ArrayList<Map<String, Object>>();

		for (Record record : record_list) {

			transfered_out_debt_list.add(record.getR());
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("transfered_out_debt_list", transfered_out_debt_list);

		Gson gson = new Gson();
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

}

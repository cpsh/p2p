package com.tfoll.web.action.index.activity;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model._____Activity_1_Recommend_UrlM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.WebApp;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@ActionKey("/index/activity")
public class Activity1Action extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "用户登录", function_description = "跳到用户活动", last_update_author = "向旋")
	public void to_activity() {

		UserM user = getSessionAttribute(SystemConstantKey.User);
		int id = user.getInt("id");

		/**
		 * 查询奖励金额和被推荐人的信息
		 */
		String sql_select = "SELECT * FROM user_info u, _____activity___reward_record a WHERE a.user_id = u.id AND u.recommend_id = ? ";

		List<Record> activity_record_list = Db.find(sql_select.toString(), new Object[] { id });
		BigDecimal sum_recommend = new BigDecimal("0");
		BigDecimal sum_financial = new BigDecimal("0");
		String select_sql = "select * from user_info where recommend_id=?";

		List<UserM> user_list = UserM.dao.find(select_sql.toString(), id);
		setAttribute("user_list", user_list);
		/**
		 * <pre>
		 * 
		 * Activity_1_Recommend_UrlM _____activity_1_recommend_url_of_has_success = _____Activity_1_Recommend_UrlM.dao.findFirst(&quot;select * from _____activity_1_recommend_url where user_id = ? for update&quot;, new Object[] { id });// 必须加锁
		 * 
		 * String user_id_map = _____activity_1_recommend_url_of_has_success.getString(&quot;user_id_map&quot;);
		 * Map&lt;String, String&gt; user_id_map_map = null;
		 * if (!Utils.isNotNullAndNotEmptyString(user_id_map)) {
		 * 	user_id_map_map = new HashMap&lt;String, String&gt;();
		 * } else {
		 * 	Type type = new TypeToken&lt;Map&lt;String, String&gt;&gt;() {
		 * 	}.getType();
		 * 	user_id_map_map = gson.fromJson(user_id_map, type);
		 * }
		 * Map&lt;String, String&gt; map_key = null;
		 * for (Entry&lt;String, String&gt; entry : user_id_map_map.entrySet()) {
		 * 	//123:0/1
		 * 	String key = entry.getKey();
		 * 	String value = entry.getValue();
		 * }
		 * </pre>
		 */
		for (Record record : activity_record_list) {
			if (record.getInt("type") == 1) {
				String real_name = record.getString("real_name");
				if (Utils.isNotNullAndNotEmptyString(real_name)) {
					if (real_name.length() == 2) {
						real_name = real_name.substring(0, 1) + "*";
					} else {
						int len = real_name.length();
						String xx = "";
						for (int i = 0; i < len - 2; i++) {
							xx += "*";
						}
						real_name = real_name.substring(0, 1) + xx + real_name.substring(len - 1, len);
					}
				}
				BigDecimal reward_amount = record.getBigDecimal("reward_amount");
				reward_amount = reward_amount.setScale(2, BigDecimal.ROUND_DOWN);
				sum_recommend = sum_recommend.add(reward_amount);
				record.add("reward_amount", reward_amount);
				record.add("real_name", real_name);
			}
		}
		String sql_select2 = "select * from _____activity___reward_record where user_id = ?";
		List<Record> activity_record_list2 = Db.find(sql_select2.toString(), new Object[] { id });
		for (Record record2 : activity_record_list2) {
			if (record2.getInt("type") == 2) {
				BigDecimal reward_amount = record2.getBigDecimal("reward_amount");
				reward_amount = reward_amount.setScale(2, BigDecimal.ROUND_DOWN);
				sum_financial = sum_financial.add(reward_amount);
				record2.add("reward_amount", reward_amount);
			}
		}

		setAttribute("sum_recommend", sum_recommend);
		setAttribute("sum_financial", sum_financial);
		setAttribute("activity_record_list", activity_record_list);
		setAttribute("activity_record_list2", activity_record_list2);

		_____Activity_1_Recommend_UrlM _____activity_1_recommend_url = _____Activity_1_Recommend_UrlM.dao.findById(id);
		if (_____activity_1_recommend_url == null) {
			_____activity_1_recommend_url = new _____Activity_1_Recommend_UrlM();
			String url = UUID.randomUUID().toString().replace("-", "");

			_____activity_1_recommend_url.set("user_id", id).set("url", url).save();
		}
		String link_url = WebApp.getWebRootPath(getRequest()) + "/index/index.html?url=" + _____activity_1_recommend_url.get("url");
		setAttribute("url", link_url);
		renderJsp("/index/activities/account_activity.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "用户登录", function_description = "跳到推荐活动的推广页面", last_update_author = "向旋")
	public void to_recommend() {
		renderJsp("/index/activities/account_recommend.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "用户登录", function_description = "跳到理财活动的推广页面", last_update_author = "向旋")
	public void to_account_activity_financial() {
		renderJsp("/index/activities/account_activity_financial.jsp");
		return;
	}
}
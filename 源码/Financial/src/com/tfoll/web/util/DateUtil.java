package com.tfoll.web.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * 返回当前时间的秒数
	 * 
	 */
	public static int getNowTime() {
		return (int) (System.currentTimeMillis() / 1000);
	}

	/**
	 * 判断输入的时间几个月之后的日期
	 */
	public static Date getDateAfterN(Date date, int month) {
		if (date == null) {
			throw new NullPointerException("data is null");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();

	}
}

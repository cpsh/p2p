package com.tfoll.web.listener;

import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.common.sessionmap.SessionContextManager;
import com.tfoll.web.model.UserM;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

public class HttpSessionListener implements javax.servlet.http.HttpSessionListener {
	public void sessionCreated(HttpSessionEvent hse) {
	}

	public void sessionDestroyed(HttpSessionEvent hse) {
		HttpSession session = hse.getSession();
		/**
		 * 如果用户登录了-需要从Session管理容器里面移除掉
		 */
		if (session != null) {
			UserM user = (UserM) session.getAttribute(SystemConstantKey.User);
			if (user != null) {
				Integer user_id = user.getInt("id");
				if (user_id != null) {
					int user_id_int = user_id.intValue();
					SessionContextManager.removeSessionContent(user_id_int);
				}
			}
		}
	}

}

package com.tfoll.web.domain;

import com.tfoll.trade.activerecord.model.Model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * 还款计划
 * 
 */
public class RepaymentPlan {

	public Date getRepayment_start_time() {
		return repayment_start_time;
	}

	public void setRepayment_start_time(Date repaymentStartTime) {
		repayment_start_time = repaymentStartTime;
	}

	public Date getRepayment_end_time() {
		return repayment_end_time;
	}

	public void setRepayment_end_time(Date repaymentEndTime) {
		repayment_end_time = repaymentEndTime;
	}

	public String getAuto_repayment_date() {
		return auto_repayment_date;
	}

	public void setAuto_repayment_date(String autoRepaymentDate) {
		auto_repayment_date = autoRepaymentDate;
	}

	/**
	 * 月还等额本息
	 */
	private BigDecimal month_pricipal_and_interest;

	/**
	 * 月还本金
	 */
	private BigDecimal month_pricipal;

	/**
	 * 月还利息
	 */
	private BigDecimal month_interest;

	/**
	 * 剩余未还本金
	 */
	private BigDecimal remain_principal;

	/**
	 * 还款时间开始
	 */
	private Date repayment_start_time;
	/**
	 * 还款结束时间
	 */
	private Date repayment_end_time;
	/**
	 * 自动还款
	 */
	private String auto_repayment_date;

	public BigDecimal getRemain_principal() {
		return remain_principal;
	}

	public void setRemain_principal(BigDecimal remainPrincipal) {
		remain_principal = remainPrincipal;
	}

	@Override
	public String toString() {

		DecimalFormat format = new DecimalFormat("######.00");
		System.out.println(repayment_start_time);
		System.out.println(repayment_end_time);
		System.out.println("---------------------");
		return "RepaymentPlan [月还本金=" + format.format(month_interest) + ", 月还利息=" + format.format(month_pricipal) + ", 剩余未还本金=" + format.format(remain_principal) + "]" + "    还款开始时间" + Model.Date.format(repayment_start_time) + "    还款结束时间" + Model.Date.format(repayment_end_time) + "    自动还款时间" + auto_repayment_date;
	}

	public void setMonth_interest(BigDecimal month_interest) {
		this.month_interest = month_interest;
	}

	public BigDecimal getMonth_interest() {
		return month_interest;
	}

	public void setMonth_pricipal(BigDecimal month_pricipal) {
		this.month_pricipal = month_pricipal;
	}

	public BigDecimal getMonth_pricipal() {
		return month_pricipal;
	}

	public void setMonth_pricipal_and_interest(BigDecimal month_pricipal_and_interest) {
		this.month_pricipal_and_interest = month_pricipal_and_interest;
	}

	public BigDecimal getMonth_pricipal_and_interest() {
		return month_pricipal_and_interest;
	}

}
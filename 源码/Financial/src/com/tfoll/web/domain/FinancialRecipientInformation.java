package com.tfoll.web.domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 理财人收款信息
 * 
 * <pre>
 * CREATE TABLE `lender_bulk_standard_creditor_right_hold_repayment_record` (
 * 			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
 * 			  `user_id` int(11) DEFAULT NULL COMMENT '债权持有人id',
 * 			  `gather_money_order_id` bigint(20) DEFAULT NULL COMMENT '筹集单ID-该ID用来进行还款',
 * 			  `user_id_of_borrower` int(11) DEFAULT NULL COMMENT 'gather_money_order_id对应的借款用户ID',
 * 			  `repayment_plan_id` bigint(13) DEFAULT NULL,
 * 			  `total_periods` int(11) DEFAULT NULL,
 * 			  `current_period` int(11) DEFAULT NULL,
 * 			  `creditor_right_hold_id` bigint(13) DEFAULT NULL,
 * 			  `hold_money` decimal(10,0) DEFAULT '0' COMMENT '持有金额',
 * 			  `hold_share` int(11) DEFAULT NULL COMMENT '持有份额',
 * 			  `principal_interest` decimal(20,2) DEFAULT '0.00' COMMENT '本息',
 * 			  `punish_interest` decimal(20,2) DEFAULT NULL COMMENT '逾期罚息',
 * 			  `add_time` timestamp NULL DEFAULT NULL COMMENT '开始持有债权时间',
 * 			  `add_time_long` bigint(13) DEFAULT NULL,
 * 			  PRIMARY KEY (`id`)
 * 			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='精英标债权持有还款记录表';
 *</pre>
 */
public class FinancialRecipientInformation {

	public FinancialRecipientInformation(//
			int user_id,//
			long gather_money_order_id, //
			int user_id_of_borrower, //
			long repayment_plan_id,//
			int total_periods, //
			int current_period,//
			long creditor_right_hold_id,//
			BigDecimal hold_money,//
			int hold_share, //
			BigDecimal principal_interest,//
			BigDecimal punish_interest, //
			Date add_time, //
			long add_time_long, //
			String borrow_title) {
		super();
		this.user_id = user_id;//
		this.gather_money_order_id = gather_money_order_id;
		this.user_id_of_borrower = user_id_of_borrower;
		this.repayment_plan_id = repayment_plan_id;
		this.total_periods = total_periods;
		this.current_period = current_period;
		this.creditor_right_hold_id = creditor_right_hold_id;
		this.hold_money = hold_money;
		this.hold_share = hold_share;
		this.principal_interest = principal_interest;
		this.punish_interest = punish_interest;
		this.add_time = add_time;
		this.add_time_long = add_time_long;
		this.borrow_title = borrow_title;
	}

	private int user_id = 0;
	private long gather_money_order_id = 0;
	private int user_id_of_borrower = 0;
	private long repayment_plan_id = 0;
	private int total_periods = 0;
	private int current_period = 0;
	private long creditor_right_hold_id = 0;
	private BigDecimal hold_money = null;
	private int hold_share = 0;
	private BigDecimal principal_interest = null;
	private BigDecimal punish_interest = null;
	private Date add_time = null;
	private long add_time_long = 0;
	private String borrow_title = "";

	public void setUser_id(int userId) {
		user_id = userId;
	}

	public void setGather_money_order_id(long gatherMoneyOrderId) {
		gather_money_order_id = gatherMoneyOrderId;
	}

	public void setUser_id_of_borrower(int userIdOfBorrower) {
		user_id_of_borrower = userIdOfBorrower;
	}

	public void setRepayment_plan_id(long repaymentPlanId) {
		repayment_plan_id = repaymentPlanId;
	}

	public void setTotal_periods(int totalPeriods) {
		total_periods = totalPeriods;
	}

	public void setCurrent_period(int currentPeriod) {
		current_period = currentPeriod;
	}

	public void setCreditor_right_hold_id(long creditorRightHoldId) {
		creditor_right_hold_id = creditorRightHoldId;
	}

	public void setHold_money(BigDecimal holdMoney) {
		hold_money = holdMoney;
	}

	public void setHold_share(int holdShare) {
		hold_share = holdShare;
	}

	public void setPrincipal_interest(BigDecimal principalInterest) {
		principal_interest = principalInterest;
	}

	public void setPunish_interest(BigDecimal punishInterest) {
		punish_interest = punishInterest;
	}

	public void setAdd_time(Date addTime) {
		add_time = addTime;
	}

	public void setAdd_time_long(long addTimeLong) {
		add_time_long = addTimeLong;
	}

	public int getUser_id() {
		return user_id;
	}

	public long getGather_money_order_id() {
		return gather_money_order_id;
	}

	public int getUser_id_of_borrower() {
		return user_id_of_borrower;
	}

	public long getRepayment_plan_id() {
		return repayment_plan_id;
	}

	public int getTotal_periods() {
		return total_periods;
	}

	public int getCurrent_period() {
		return current_period;
	}

	public long getCreditor_right_hold_id() {
		return creditor_right_hold_id;
	}

	public BigDecimal getHold_money() {
		return hold_money;
	}

	public int getHold_share() {
		return hold_share;
	}

	public BigDecimal getPrincipal_interest() {
		return principal_interest;
	}

	public BigDecimal getPunish_interest() {
		return punish_interest.setScale(2, BigDecimal.ROUND_DOWN);
	}

	public Date getAdd_time() {
		return add_time;
	}

	public long getAdd_time_long() {
		return add_time_long;
	}

	public void setBorrow_title(String borrow_title) {
		this.borrow_title = borrow_title;
	}

	public String getBorrow_title() {
		return borrow_title;
	}

}

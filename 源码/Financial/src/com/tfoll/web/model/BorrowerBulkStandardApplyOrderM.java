package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_apply_order", primaryKey = "id")
public class BorrowerBulkStandardApplyOrderM extends Model<BorrowerBulkStandardApplyOrderM> {
	public static BorrowerBulkStandardApplyOrderM dao = new BorrowerBulkStandardApplyOrderM();

	// 查询用户最后一条订单
	public static BorrowerBulkStandardApplyOrderM get_last_apply_order(int user_id) {
		return BorrowerBulkStandardApplyOrderM.dao.findFirst("select * from borrower_bulk_standard_apply_order where user_id = ?  order by  id desc limit 1", new Object[] { user_id });
	}
}

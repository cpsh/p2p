package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_message_request_success", primaryKey = "id")
public class UserMessageRequestSuccessM extends Model<UserMessageRequestSuccessM> {
	public static UserMessageRequestSuccessM dao = new UserMessageRequestSuccessM();
}

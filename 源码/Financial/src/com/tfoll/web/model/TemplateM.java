package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "sys_template", primaryKey = "id")
public class TemplateM extends Model<TemplateM> implements Serializable {

	private static final long serialVersionUID = 1258462970131571663L;
	public static TemplateM dao = new TemplateM();
}

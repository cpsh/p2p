package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.util.List;

@TableBind(tableName = "user_automatically_bid", primaryKey = "user_id")
public class UserAutomaticallyBidM extends Model<UserAutomaticallyBidM> {
	public static UserAutomaticallyBidM dao = new UserAutomaticallyBidM();

	public static List<UserAutomaticallyBidM> get_user_automatically_bid_list() {
		List<UserAutomaticallyBidM> user_automatically_bid_list = UserAutomaticallyBidM.dao.find("select * from user_automatically_bid where disjunctor = 1 ");
		return user_automatically_bid_list;
	}

	public static List<UserAutomaticallyBidM> get_user_automatically_bid_list_of_only_user_id() {
		return UserAutomaticallyBidM.dao.find("select user_id from user_automatically_bid where disjunctor = 1 ");
	}
}

package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 联富定投表
 * 
 * @author HangSun
 * 
 */
@TableBind(tableName = "fix_bid_system_order", primaryKey = "id")
public class FixBidSystemOrderM extends Model<FixBidSystemOrderM> implements Serializable {

	private static final long serialVersionUID = 8714753820330201896L;
	public static FixBidSystemOrderM dao = new FixBidSystemOrderM();

	/**
	 * 根据ID获取联富宝基本信息 for update(加行级锁)
	 */
	private static FixBidSystemOrderM get_fix_bid_system_order_for_update(Long id) {
		String sql = "SELECT * from fix_bid_system_order t WHERE t.id = ? FOR UPDATE";
		return FixBidSystemOrderM.dao.findFirst(sql, new Object[] { id });
	}

	/**
	 * 多个用户并发访问同一条定投信息时，根据定投ID获取改定投记录信息
	 * 
	 * @author HangSun
	 * 
	 */
	public static class GetMonthFixBidByIdTask implements Callable<FixBidSystemOrderM> {
		private long order_id;

		public GetMonthFixBidByIdTask(long orderId) {
			super();
			this.order_id = orderId;
		}

		public FixBidSystemOrderM call() throws Exception {
			return FixBidSystemOrderM.get_fix_bid_system_order_for_update(this.order_id);
		}
	}

	/**
	 * <pre>
	 * 1 该方法可能由于数据库锁等待超时，会返回为null
	 * 2 该方法只能在原子事务里面调用-否则会死锁
	 * </pre>
	 */
	public static FixBidSystemOrderM get_month_fix_bid_by_id(final long order_id) throws Exception {

		FixBidSystemOrderM.GetMonthFixBidByIdTask get_fix_bid_task = new FixBidSystemOrderM.GetMonthFixBidByIdTask(order_id);
		ExecutorService get_fix_bid_task_service = Executors.newFixedThreadPool(1);
		// 对task对象进行各种set操作以初始化任务
		Future<FixBidSystemOrderM> future = get_fix_bid_task_service.submit(get_fix_bid_task);
		try {
			return future.get(3, TimeUnit.SECONDS);
		} catch (Exception e) {
			return null;
		} finally {
			if (future.isCancelled()) {
				future.cancel(true);
			}
			get_fix_bid_task_service.shutdownNow();
		}

	}
}

package com.tfoll.web.model;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.annotation.Function;

@TableBind(tableName = "user_message_request_waiting", primaryKey = "id")
public class UserMessageRequestWaitingM extends Model<UserMessageRequestWaitingM> {
	public static UserMessageRequestWaitingM dao = new UserMessageRequestWaitingM();

	/**
	 * 1贷款驳回
	 */
	@Function(for_people = "所有人", function_description = "贷款驳回", last_update_author = "czh")
	public static boolean add_loans_rejected_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 1).//
						set("request_type_name", "贷款驳回发送验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 1).// 
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 1绑定手机
	 */
	@Function(for_people = "所有人", function_description = "绑定手机", last_update_author = "czh")
	public static boolean add_phone_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 2).//
						set("request_type_name", "绑定手机发送验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 2).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 1修改手机发送验证码
	 */
	@Function(for_people = "所有人", function_description = "修改手机", last_update_author = "czh")
	public static boolean add_update_phone_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 3).//
						set("request_type_name", "绑定手机发送验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 3).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 1人民币提现发送短信验证码
	 */
	@Function(for_people = "所有人", function_description = "人民币提现", last_update_author = "czh")
	public static boolean add_cny_withdraw_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0] + "#" + data[1]).//
						set("request_type", 4).//
						set("request_type_name", "人民币提现发送验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0] + "#" + data[1]).//
						set("request_type", 4).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 1绑定提现验证码发送短信验证码
	 */
	@Function(for_people = "所有人", function_description = "绑定提现验证码发送短信验证码", last_update_author = "czh")
	public static boolean binding_withdraw_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 5).//
						set("request_type_name", "绑定提现密码发送验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 5).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 找回提现密码发送短信验证码
	 */
	@Function(for_people = "所有人", function_description = "找回提现密码发送短信验证码", last_update_author = "czh")
	public static boolean back_withdraw_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 6).//
						set("request_type_name", "找回提现密码发送短信验证码").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 6).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 修改提现密码发送短信验证码
	 */
	@Function(for_people = "所有人", function_description = "找回提现密码发送信息", last_update_author = "czh")
	public static boolean back_withdraw_message2(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 8).//
						set("request_type_name", "找回提现密码发送信息").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 8).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * rmb提现发送短信提示
	 */
	@Function(for_people = "所有人", function_description = "rmb提现发送短信提示", last_update_author = "czh")
	public static boolean send_rmb_withdraw_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 9).//
						set("request_type_name", "rmb提现发送短信提示").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 9).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

	/**
	 * 找回密码
	 */
	@Function(for_people = "所有人", function_description = "找回登录密码发送短信", last_update_author = "czh")
	public static boolean send_back_password_message(final String phone, final String[] data, final int id) {
		return Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserMessageRequestLogM user_message_request_log = new UserMessageRequestLogM();
				boolean is_ok_add_user_message_request_log = user_message_request_log.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 12).//
						set("request_type_name", "找回登录密码发送短信").//
						save();

				UserMessageRequestWaitingM user_message_request_waiting = new UserMessageRequestWaitingM();
				boolean is_ok_add_user_message_request_waiting = user_message_request_waiting.set("user_id", id).//
						set("ip", ActionContext.getRequest().getRemoteAddr()).//
						set("phone", phone).//
						set("content", data[0]).//
						set("request_type", 12).//
						set("retry_times", 0).//
						save();
				return is_ok_add_user_message_request_log && is_ok_add_user_message_request_waiting;
			}
		});

	}

}

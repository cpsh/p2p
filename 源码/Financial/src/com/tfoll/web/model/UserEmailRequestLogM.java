package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_email_request_log", primaryKey = "id")
public class UserEmailRequestLogM extends Model<UserEmailRequestLogM> {
	public static UserEmailRequestLogM dao = new UserEmailRequestLogM();
}

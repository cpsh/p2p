package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "sys_user_investors_income_config", primaryKey = "id")
public class SysUserInvestorsIncomeConfigM extends Model<SysUserInvestorsIncomeConfigM> implements Serializable {

	private static final long serialVersionUID = 1262241552051672915L;
	public static SysUserInvestorsIncomeConfigM dao = new SysUserInvestorsIncomeConfigM();
}

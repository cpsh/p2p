package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_recharge_log", primaryKey = "id")
public class UserRechargeLogM extends Model<UserRechargeLogM> {
	public static UserRechargeLogM dao = new UserRechargeLogM();
}

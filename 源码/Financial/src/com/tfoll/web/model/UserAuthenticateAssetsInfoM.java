package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_assets_info", primaryKey = "user_id")
public class UserAuthenticateAssetsInfoM extends Model<UserAuthenticateAssetsInfoM> {
	public static UserAuthenticateAssetsInfoM dao = new UserAuthenticateAssetsInfoM();
}

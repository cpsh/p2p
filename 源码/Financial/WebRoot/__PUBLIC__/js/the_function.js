//信用等级颜色
function change_color(credit_rating,credit_rating_id){
	var arr = ["ui-creditlevel ui-creditlevel-s AA snow","ui-creditlevel ui-creditlevel-s A snow","ui-creditlevel ui-creditlevel-s BB snow",
	           "ui-creditlevel ui-creditlevel-s B snow","ui-creditlevel ui-creditlevel-s CC snow","ui-creditlevel ui-creditlevel-s C snow"];
	var arr2 = ["A+","A","B+","B","C+","C"];
	for(var i = 0;i<arr2.length;i++){
		if(credit_rating == arr2[i]){
			$("#"+credit_rating_id).addClass(arr[i]);
		}
	}
}

//信用档案资金信息取2位小数
function get_fund_info(line_of_credit,overdue_amount,total_loan,repay){
	$("#line_of_credit").html(line_of_credit.toFixed(2));
	$("#overdue_amount").html(overdue_amount.toFixed(2));
	$("#total_loan").html(total_loan.toFixed(2));
	$("#repay").html(repay.toFixed(2));
}
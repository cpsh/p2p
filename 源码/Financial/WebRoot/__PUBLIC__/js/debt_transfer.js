/**
 * 债权转让js
 */
var path = "http://" + window.location.host
var can_transfered_debt_url = path + "/user/user_center/user_debt_transfer/get_user_can_transfered_debt.html";
var transfering_debt_url = path + "/user/user_center/user_debt_transfer/get_user_transfering_debt.html";
var already_transfered_out_debt_url = path + "/user/user_center/user_debt_transfer/get_already_transfered_out_debt.html";
var transfered_out_debt_detail_url = path + "/user/user_center/user_debt_transfer/get_debt_transfer_out_detail.html";//转出债权的明细
var transfered_in_debt_url = path + "/user/user_center/user_debt_transfer/get_already_transfered_in_debt.html";//已转入的债权
var login_url = path+"/index/to_login.html";

$(document).ready(function (){
	$("#transfering_debt").click(
			 function(){transfering_debt();}
	 );
	 $("#can_transfered_debt").click(
			 function(){can_transfered_debt();}
	 );
	 $("#already_transfered_out_debt").click(
			function(){already_transfered_out_debt();}
	 );
	 $("#already_transfered_in_debt").click(
			 function(){already_transfered_in_debt();}
	 );
	
});

/**
 * 查询可转让的债权列表
 * @return
 */

function can_transfered_debt(){
	 $.ajax({
		 url:can_transfered_debt_url,
		 type:"post",
		 success:function(data){
	 		var can_transfer_debt_table = "#can_transfer_debt_table";
	 		var can_transfer_debt_first_tr = "#can_transfer_debt_first_tr";
	 		$(can_transfer_debt_table + " tr:not(:first)").empty(); 
	 		var str = "";
	 		if(data == "NoLogined"){
	 			window.location.href = login_url;
	 		}else{
				 if(data == ""){
					 str = str + "<tr><td colspan=\"9\" class=\"td_1\">没有可转让的债权</td></tr>";
					 $(can_transfer_debt_first_tr).after(str);
				 }else{
					 var arrays = eval("("+ data +")");
					 var __PUBLIC__ = "";
					 if(path == "http://www.lfoll.com"){
			 				__PUBLIC__ = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";
			 		 }else{
			 				__PUBLIC__ = path + "/__PUBLIC__";
			 		 }
					 
					 for(var i in arrays){
						 var borrow_type = arrays[i].borrow_type;
						 var gather_money_order_id = arrays[i].gather_money_order_id;//债权ID（筹集单ID）
						 var remain_periods = arrays[i].remain_periods;//剩余期数:格式为：1/6
						 var remain_periods_int = arrays[i].remain_periods_int;//剩余期数
						 var next_payment_date = arrays[i].next_payment_date;//下一个还款日期
						 var annulized_rate = arrays[i].annulized_rate;//年化利率 如 20%
						 var should_repayment_total = arrays[i].should_repayment_total;//待收本息
						 var debt_value = arrays[i].debt_value;//债权价值
						 var can_transfer_share = arrays[i].can_transfer_share;//可转出的份额
						 var origin_money = arrays[i].origin_money;//原始投资金额
						 var hold_share = arrays[i].hold_share;//总持有份额
						 var annulized_rate_int =  arrays[i].annulized_rate_int;//年化率int
						 var daishou_benxi_per_share = arrays[i].daishou_benxi_per_share;//每份待收本息
						 var debt_value_per = arrays[i].debt_value_per;//债权转让每份的价值
						 var creditor_right_hold_id = arrays[i].creditor_right_hold_id;//债权持有ID
						 var calss_type = "";
						 if((i+1) % 2 != 0){
							 calss_type =  "class=\"tr_1\"";
						 }
						
						 str = str + "<tr " +calss_type+ "><td><input type=\"checkbox\" /></td>";
						 var str2 = "";
						 if(borrow_type == 1){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1.jpg\"/>";
						 }else if(borrow_type == 2){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1_2.jpg\" />";
						 }else if(borrow_type == 3){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1_3.jpg\" />";
						 }
						 str = str + "<td>" + str2 + "<span>" + gather_money_order_id + "</span></td>"//筹集单ID（债权ID）
						  + "<td>" + remain_periods + "</td>"//剩余期数 
						  + "<td>" + next_payment_date + "</td>" //下一个还款日期
						  + "<td>" + annulized_rate + "</td>" //年化利率
						  + "<td>" + should_repayment_total + "</td>"//待收本息
						  + "<td>" + debt_value + "</td>" //债权价值
						  + "<td>" + can_transfer_share + "份" + "</td>"//可转份额
						  + "<td>" + "<a onClick=\"open_debt_transfer(1,2,"
					             + gather_money_order_id +","
					             + origin_money +","
					             + hold_share +","
					             + can_transfer_share +","
					             + annulized_rate_int +","
					             + daishou_benxi_per_share +","
					             + debt_value_per + ","
					             + creditor_right_hold_id + ","//债权持有表id
					             + remain_periods_int 
					             + ")\">转让</a>" + "</td></tr>"
						  
					 }
					 $(can_transfer_debt_first_tr).after(str);
				 }
	 		}
		 }
	 });
}

/**
 * 查询正在转让中的债权
 */

function transfering_debt(){
	$.ajax({
		url:transfering_debt_url,
		type:"post",
		success:function(data){
			var debt_transfering_table = "#debt_transfering_table";
			var debt_transfering_table_first_tr = "#debt_transfering_table_first_tr";
			$(debt_transfering_table + " tr:not(:first)").empty(); 
			var str = "";
			if(data == "NoLogined"){
	 			window.location.href = login_url;
	 		}else{
				 if(data == ""){
					 str = str + "<tr><td colspan=\"9\" class=\"td_1\">没有转让中的债权</td></tr>";
					 $(debt_transfering_table_first_tr).after(str);
				 }else{
					 var arrays = eval("("+ data +")");
					 var __PUBLIC__ = "";
					 if(path == "http://www.lfoll.com"){
			 				__PUBLIC__ = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";
			 		 }else{
			 				__PUBLIC__ = path + "/__PUBLIC__";
			 		 }
					 
					 for(var i in arrays){
						 var id = arrays[i].id;//转让ID
						 var gather_money_order_id = arrays[i].gather_money_order_id;//债权ID(筹集单ID)
						 var remain_periods = arrays[i].remain_periods;//剩余期数
						 var annulized_rate_int = arrays[i].annulized_rate_int;//年化利率
						 var current_creditor_right_value = arrays[i].current_creditor_right_value;//当前债权价值
						 var transfer_price = arrays[i].transfer_price;//转让价格
						 var transfer_factor = arrays[i].transfer_factor;//转让系数
						 var remain_share = arrays[i].remain_share;//剩余份数
						 var borrow_type = arrays[i].borrow_type;//借款类型
						 
						 var str2 = "";
						 if(borrow_type == 1){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1.jpg\"/>";
						 }else if(borrow_type == 2){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1_2.jpg\" />";
						 }else if(borrow_type == 3){
							 str2 = "<img src=\"" + __PUBLIC__ + "/images/icon1_3.jpg\" />";
						 }
						 var num = Number(i) + Number(1);
						 var class_type = "";
						 if(num % Number(2) != 0){
							 class_type = "class=\"tr_1\"";
						 }
						 
						 var debt_transfering_id = "debt_transfering_" + num;
						 str =  str + "<tr " + class_type +" id=\""+debt_transfering_id+"\">"
						 		+ "<td><span>"+ id +"</span></td>"
						 		+ "<td>" + str2 + "<span>" + gather_money_order_id + "</span></td>"
						 		+ "<td>"+ remain_periods +"</td>"
						 		+ "<td>"+ annulized_rate_int +"</td>"
						 		+ "<td>"+ current_creditor_right_value +"</td>"
						 		+ "<td>"+ transfer_price +"</td>"
						 		+ "<td>"+ transfer_factor +"</td>"
						 		+ "<td>"+ remain_share +"</td>"
						 		+ "<td><a href=\"javascript:void(0);\" onclick=\"console_transfering_debt('" + debt_transfering_id + "','" + id + "','" + gather_money_order_id + "')\">撤销</a></td></tr>";
						 
				      /*<tr class="tr_1">
			             <td><span>207594</span></td>
			             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
			             <td>6/6</td>
			             <td>22.80%</td>
			             <td>50.40/份</td>
			             <td>50.40/份</td>
			             <td>100.00%</td>
			             <td>1/1份</td>
			             <td><a href="#">撤销</a></td>
			          </tr>*/
					  
					 }
					 //alert(str);
					 $(debt_transfering_table_first_tr).after(str);
				 }
	 		}
		}
		
		
	});
}

/**
 * 转让中的债权---》撤单
 * @param debt_transfering_id :对应的一行tr 的id
 * @param id :对应债权转出表id
 * @param gather_money_order_id:筹集单id
 * @return
 */
var console_url = path + "/user/user_center/user_debt_transfer/user_cancel_transfering_debt.html";
function console_transfering_debt(debt_transfering_id,id,gather_money_order_id){
	var bool = true;
	if(bool){
		bool = false;
		$.ajax({
			url:console_url,
			async:false,
			type:"post",
			data:{"id":id,"gather_money_order_id":gather_money_order_id},
			success:function(data){
				if(data == "NoLogined"){
					window.location.href = login_url;
				}else{
					if(data == "1"){
						showMessage(["提示","撤单操作失败"]);
					}else if(data == "2"){
						showMessage(["提示","撤单操作失败"]);
					}else if(data == "4"){
						showMessage(["提示","撤单失败，请联系客服人员!"]);
					}else if(data == "3"){
						$("#"+debt_transfering_id).empty();
						showMessage(["提示","撤单成功!"]);
					}
				}
				bool = true;
			}
		});
	}
	
}

	/**
	 * 已经转出的债权
	 * @return
	 */
	function already_transfered_out_debt(){
		$.ajax({
			url:already_transfered_out_debt_url,
			type:"post",
			success:function(data){
					var transfered_out_debt_table = "#transfered_out_debt_table";
					var transfered_out_debt_table_first_tr = "#transfered_out_debt_table_first_tr";
					$(transfered_out_debt_table + " tr:not(:first)").empty(); 
					var str = "";
					if(data == "NoLogined"){
			 			window.location.href = login_url;
			 		}else{
						 if(data == ""){
							 str = str + "<tr><td colspan='8' class='td_1'>没有转出的债权</td></tr>";
							 $(transfered_out_debt_table_first_tr).after(str);
						 }else{
							 var arrays = eval("("+ data +")");
							 var __PUBLIC__ = "";
							 if(path == "http://www.lfoll.com"){
					 				__PUBLIC__ = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";
					 		 }else{
					 				__PUBLIC__ = path + "/__PUBLIC__";
					 		 }
							 
							 for(var i in arrays){
								 var transfer_out_order_id = arrays[i].transfer_out_order_id;//转出订单表id
								 var borrow_type = arrays[i].borrow_type;//借款类型
								 
								 var gather_money_order_id = arrays[i].gather_money_order_id;//债权ID(筹集单ID)
								 var total_transfer_share = arrays[i].total_transfer_share;//成交份数
								 var total_transfer_debt_value = arrays[i].total_transfer_debt_value;//转出时债权总价值
								 var total_transfer_price = arrays[i].total_transfer_price;//转出时总成交金额
								 var total_transfer_fee = arrays[i].transfer_fee;//交易费用
								 
								 var actual_income_price = arrays[i].actual_income_price;//实际收入
								 var transfer_profit_loss = arrays[i].transfer_profit_loss;//转让盈亏
								 
								 var out_detail_list = arrays[i].out_detail_list;//明细集合
//								 alert(out_detail_list.length);
								 var transfer_out_content = "";
								 for(var j in out_detail_list){
										var nickname = out_detail_list[j].nickname;//昵称
										var transfer_debt_value = out_detail_list[j].transfer_debt_value;//每份债权价值
										var transfer_price = out_detail_list[j].transfer_price;//每份价格
										var transfer_share = out_detail_list[j].transfer_share;//份额
										var transfer_fee = out_detail_list[j].transfer_fee;//管理费
										var real_income = out_detail_list[j].real_income;//实际收入
										var profit_loss = out_detail_list[j].profit_loss;//盈亏
										var add_time_str = out_detail_list[j].add_time_str;//成交时间
										
										transfer_out_content = transfer_out_content + "<tr><td><span>" + nickname + "</span></td>"
																  + "<td>"+transfer_debt_value+"/份</td>"
																  + "<td>"+transfer_price+"/份</td>"
																  + "<td>"+ transfer_share +"份</td>"
																  + "<td>"+ transfer_fee +"</td>"
																  + "<td>"+ real_income +"</td>"
																  + "<td>-"+ profit_loss +"</td>" 
																  + "<td>"+ add_time_str +"</td>"
																  + "<td><a href=\"#\">合同</a></td>"
																  + "</tr>";
								}
								 var images_url = "";
								 if(borrow_type == 1){
									 images_url = "<img src='" + __PUBLIC__ + "/images/icon1.jpg'/>";
								 }else if(borrow_type == 2){
									 images_url = "<img src='" + __PUBLIC__ + "/images/icon1_2.jpg' />";
								 }else if(borrow_type == 3){
									 images_url = "<img src='" + __PUBLIC__ + "/images/icon1_3.jpg' />";
								 }
								 var class_type = "";
								 var n = Number(i) + Number(1);
								 if(n % Number(2) != 0){
									 class_type = "class='tr_1'";
								 }
								 
								 var id_index = "transfered_out_debt_table_tr_" + n;
								 str =  str + "<tr " + class_type +" id='"+id_index+"'>"
								 		+ "<td>"+ images_url+"<span>"+ gather_money_order_id +"</span></td>"
								 		+ "<td>" + total_transfer_share + "</td>"
								 		+ "<td>"+ total_transfer_debt_value +"</td>"
								 		+ "<td>"+ total_transfer_price +"</td>"
								 		+ "<td>"+ actual_income_price +"</td>"
								 		+ "<td>"+ total_transfer_fee +"</td>"
								 		+ "<td>-"+ transfer_profit_loss +"</td>"
								 		+ "<td><a onclick=\"show_transfer_out_detail('" + n + "')\">明细</a></td></tr>";
								 //指定并列行
								 var submenu_id = "submenu_detail_" + n;
								 //指定下级table的第二个tr
								 var submenu_table_tr_id = "submenu_table_tr_id_" + n;
								 str = str + 
								 "<tr id='" + submenu_id + "' " + class_type + " style='display:none;'>" +
						             "<td colspan='8'>" +
						               "<table width='100%' border='0' cellspacing='0' cellpadding='0'>" + 
						                  "<tr>" +
						                     "<td colspan='9' style='font-size:16px'>债权转让明细</td>" +
						                  "</tr>" +
						                  "<tr id='" + submenu_table_tr_id + "'>"  +
						                     "<td width='14%'>购买人</td>" +
						                     "<td width='16%'>转出时债权价值</td>" +
						                     "<td width='11%'>转让价格</td>" +
						                     "<td width='11%'>成交份数</td>" +
						                     "<td width='11%'>交易费用</td>" +
						                     "<td width='11%'>实际收入</td>" +
						                     "<td width='8%'>盈亏</td>" +
						                     "<td width='14%'>成交时间</td>" +
						                     "<td width='4%'>&nbsp;</td>" +
						                  "</tr>" + transfer_out_content +
						               "</table>" +
						             "</td>" +
						          "</tr>";
							 }
							 $(transfered_out_debt_table_first_tr).after(str);
						 }
			 		}
				
			}
		});
	}

	/**
	 * 显示指定ID隐藏部分
	 * @param id
	 * @return
	 */
	function show_transfer_out_detail(sid){
	    var whichEl = document.getElementById("submenu_detail_" + sid);
		whichEl.style.display = whichEl.style.display =='none'?'':'none';
	}
	/**
	 * 点击显示明细
	 * n :第n行记录
	 * transfer_out_order_id：转出ID
	 */
	function get_detail(n,transfer_out_order_id){
		/*var id = "submenu" + index;
		alert($("#"+id));
		$("#"+id).empty();*/
		$.ajax({
			url:transfered_out_debt_detail_url,
			type:"post",
			data:{"transfer_out_order_id":transfer_out_order_id},
			success:function(data){
									
				var current_tr_id = "#submenu_table_tr_id_" + n;
				
				var str_content = "";
				if(data == ""){
					str_content = "<tr><td colspan='9' style='text-align: center;'>暂无数据</td></tr>";
					
				}else{
					var arrays = eval("("+data+")");
					
					for(var i in arrays){
						var nickname = arrays[i].nickname;//昵称
						var transfer_debt_value = arrays[i].transfer_debt_value;//每份债权价值
						var transfer_price = arrays[i].transfer_price;//每份价格
						var transfer_share = arrays[i].transfer_share;//份额
						var transfer_fee = arrays[i].transfer_fee;//管理费
						var real_income = arrays[i].real_income;//实际收入
						var profit_loss = arrays[i].profit_loss;//盈亏
						var add_time_str = arrays[i].add_time_str;//成交时间
						
						str_content = str_content + "<tr><td><span>" + nickname + "</span></td>"
												  + "<td>"+transfer_debt_value+"/份</td>"
												  + "<td>"+transfer_price+"/份</td>"
												  + "<td>"+ transfer_share +"份</td>"
												  + "<td>"+ transfer_fee +"</td>"
												  + "<td>"+ real_income +"</td>"
												  + "<td>-"+ profit_loss +"</td>" 
												  + "<td>"+ add_time_str +"</td>"
												  + "<td><a href=\"#\">合同</a></td>"
												  + "</tr>";
					}
					
				}
				$(current_tr_id).after(str_content);
				
				var submenu_id = "#submenu" + n;
				alert($(submenu_id).css('display') == "list-item");
				if($(submenu_id).css('display') == "list-item"){
//					$(submenu_id).css('display','block');
					$(submenu_id).show();
				}else{
//					$(submenu_id).css('display','none');
					$(submenu_id).hide();
				}
				
//				alert("submenu"+n);
//				alert(document.getElementById("submenu"+n).style.display);
//				alert($(submenu_id).css('display'));
				
//				$(submenu_id).css('display','block');
				
				
//				var whichEl = document.getElementById(submenu_id);
				/*alert(whichEl);
				alert(whichEl.style.display == '');*/
//				whichEl.style.display = whichEl.style.display =='none'?'':'none';
				
				
			}
		});
		
	}
	
	

	/**
	 * 已转入的债权
	 * @return
	 */
	function already_transfered_in_debt(){
		$.ajax({
			url:transfered_in_debt_url,
			type:"post",
			success:function(data){
				var transfered_in_debt_table = "#transfered_in_debt_table";
				var transfered_in_debt_table_first_tr = "#transfered_in_debt_table_first_tr";
				$(transfered_in_debt_table + " tr:not(:first)").empty(); 
				var str="";
				if(data == "NoLogined"){
		 			window.location.href = login_url;
		 		}else{
		 			if(data == ""){
		 				str = str + "<tr><td colspan=\"9\" class=\"td_1\">没有已转入的债权</td></tr>";
		 				$(transfered_in_debt_table_first_tr).after(str);
		 			}else{
			 			var arrays = eval("("+data+")");
			 			 var __PUBLIC__ = "";
			 			 if(path == "http://www.lfoll.com"){
			 				__PUBLIC__ = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";
			 			 }else{
			 				__PUBLIC__ = path + "/__PUBLIC__";
			 			 }
			 			for(var i in arrays){
			 				var borrow_type = arrays[i].borrow_type;//借款类型
			 				var gather_money_order_id = arrays[i].gather_money_order_id;//筹集单ID
			 				var remain_period = arrays[i].remain_period;//剩余期数
			 				var annulized_rate_int = arrays[i].annulized_rate_int;//年化利率
			 				var transfer_debt_value = arrays[i].transfer_debt_value;//每份转入时的债权价值
			 				var transfer_share = arrays[i].transfer_share;//转入份额
			 				var transction_amount = arrays[i].transction_amount;//交易金额
			 				var should_count_interest = arrays[i].should_count_interest;//盈亏
			 				var add_time_str = arrays[i].add_time_str;//转入时间
			 				
			 				var images_url = "";
							 if(borrow_type == 1){
								 images_url = "<img src=\"" + __PUBLIC__ + "/images/icon1.jpg\"/>";
							 }else if(borrow_type == 2){
								 images_url = "<img src=\"" + __PUBLIC__ + "/images/icon1_2.jpg\" />";
							 }else if(borrow_type == 3){
								 images_url = "<img src=\"" + __PUBLIC__ + "/images/icon1_3.jpg\" />";
							 }
							 var class_type = "";
							 var n = Number(i) + Number(1);
							 if(n % 2 != 0){
								 class_type = "class=\"tr_1\"";
							 }
							 
							 str = str + "<tr "+class_type+">"
							 		   + "<td>" + images_url + "<span>" + gather_money_order_id + "</span></td>"
							 		   + "<td>" + remain_period + "个月</td>"
							 		   + "<td>" + annulized_rate_int + "%</td>"
							 		   + "<td>" + transfer_debt_value + "/份</td>"
							 		   + "<td>" + transfer_share + "</td>"
							 		   + "<td>" + transction_amount + "</td>"
							 		   + "<td>" + should_count_interest +"</td>"
							 		   + "<td>" + add_time_str +"</td>"
							 		   + "<td><a href=\"#\"></a></td>"
							 		   + "</tr>";
			 			 /* <tr class="tr_1">
			 	             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
			 	             <td>15个月</td>
			 	             <td>11.40%</td>
			 	             <td>42.12/份</td>
			 	             <td>1</td>
			 	             <td>42.12</td>
			 	             <td>0.00</td>
			 	             <td>2014-01-12</td>
			 	             <td><a href="#">合同</a></td>
			 	          </tr>*/
			 				
			 			}
			 			//alert(str);
			 			$(transfered_in_debt_table_first_tr).after(str);
		 			}
		 		}
			}
		});
		
	}

 var path = "http://"+window.location.host;
 
//确定还款 要传给后台的id字符串
 var pass_id_str = "";
 
 
//借款查询页面	
//获取还款计划
	function get_repayment_record(){

		var url = path + "/user/usercenter/loan_manage/get_repayment_plan.html";

		$.ajax({
			url:url,
			type:'post',
			cache:false,
			success:function(data)
			{
				var jsObject = JSON.parse(data);
				//var count = jsObject.weihuan_count;
				//alert(count);
				$("#table1 tr:not(:first)").empty(); 
				var tr_str ='';
				
					for(var i=0;i<jsObject.repayment_plans.length;i++ ){

						var state ;
						var checkbox_str;
						if(jsObject.repayment_plans[i].is_repay == 1){
							state = '已还';
							checkbox_str = '';
						}else{
							if(jsObject.repayment_plans[i].repayment_period == 0){
								state = '待还';
							}else if(jsObject.repayment_plans[i].repayment_period == 1){
								state ='待还';
							}else if(jsObject.repayment_plans[i].repayment_period == 2){
								state ='待还';
							}else if(jsObject.repayment_plans[i].repayment_period == 3){
								state ='普通逾期';
							}else if(jsObject.repayment_plans[i].repayment_period == 4){
								state ='严重逾期';
							}

							checkbox_str = '<input type="checkbox" class="checkbox_1" onclick="cal_sum(this);" />';

							
						}
						
						tr_str += 
							'<tr class="tr_1">'
			             + '<td>'+ checkbox_str +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].automatic_repayment_date +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].total_total.toFixed(2) +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].should_repayment_principle.toFixed(2) +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].should_repayment_interest.toFixed(2) +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].manage_fee.toFixed(2) +'</td>'
			             + '<td>'+ jsObject.repayment_plans[i].over_total_fee.toFixed(2) +'</td>'
			             + '<td>'+ state +'</td>'
			             + '<td><input type="hidden" id="'+ jsObject.repayment_plans[i].id +'" value="'+ jsObject.repayment_plans[i].id +'" /></td>'
			          + '</tr>' ;
							
					}
				$("#table1 tr:eq(0)").after(tr_str);
				$("#weihuan").html(jsObject.weihuan.toFixed(2));
//				$("#to_repay_money").html(jsObject.weihuan.toFixed(2));
				$("#yihuan").html(jsObject.yihuan.toFixed(2)); 		
				$("#cny_can_used").html(jsObject.cny_can_used.toFixed(2));

				check_money_enough();
				
			}
		},"json");
		
	}
	
	//借款查询页面	
	//提前还款  还款计划
		function get_pre_repayment_record(){

			var url = path + "/user/usercenter/loan_manage/get_pre_repayment_plan.html";

			$.ajax({
				url:url,
				type:'post',
				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					var count = jsObject.weihuan_count;
					$("#advance dd").first().nextAll().remove();
					var tr_str ="";
					
						for(var i=0;i<jsObject.repayment_plans.length;i++ ){

							var state ;
							if(jsObject.repayment_plans[i].is_repay == 1){
								state = '已还';
							}else{
								if(jsObject.repayment_plans[i].repayment_period == 0){
									state = '待还';
								}else if(jsObject.repayment_plans[i].repayment_period == 1){
									state ='待还';
								}else if(jsObject.repayment_plans[i].repayment_period == 2){
									state ='待还';
								}else if(jsObject.repayment_plans[i].repayment_period == 3){
									state ='普通逾期';
								}else if(jsObject.repayment_plans[i].repayment_period == 4){
									state ='严重逾期';
								}

								
							}
							
							tr_str +=
			                     '<dd class="dd_2"> <span>'+ jsObject.repayment_plans[i].automatic_repayment_date +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].total_total.toFixed(2) +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].should_repayment_principle.toFixed(2) +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].should_repayment_interest.toFixed(2) +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].manage_fee.toFixed(2) +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].over_manage_fee.toFixed(2) +'</span>'
			                     + '<span>'+ jsObject.repayment_plans[i].over_faxi.toFixed(2) +'</span>'
			                     + '<span>'+ state +'</span></dd>';
								
						}
					$("#advance").append(tr_str);
					$("#weihuan_2").html(jsObject.weihuan.toFixed(2));
					$("#repay_this_2").html(jsObject.weihuan.toFixed(2));
					$("#yihuan_2").html(jsObject.yihuan.toFixed(2)); 		
					$("#cny_can_used_2").html(jsObject.cny_can_used.toFixed(2));

					//check_money_enough();
					var cny_can_used = $("#cny_can_used_2").html();
					var repay_this = $("#repay_this_2").html();
					if(cny_can_used - repay_this < 0){
						$("#msg_2").show();
						}else{
							$("#msg_2").hide();
						}
				}
			},"json");
			
		}
	
	
	//计算本次应还总额
	function cal_sum(event){
		var $obj = $(event);
		var total = $obj.parent().siblings().eq(1).html();

		var box_length = $("input[type=checkbox]").length;
		var sum = new Number(0.00);
		
		pass_id_str = "";
		
			for(var i=0;i<box_length;i++){
				var $box = $("input[type=checkbox]").eq(i);
				var box = $box[0];
				if(box.checked){
					var should_repay = new Number($box.parent().siblings().eq(1).html());
					sum += should_repay;

					var id = $box.parent().parent().children(":last").children(":first").val();
					pass_id_str += id+",";
				}
			}
			//alert(pass_id_str);
			if(sum == 0){
				$("#repay_this").html("0.00");	
			}else{
			$("#repay_this").html(sum.toFixed(2));	
			}
		
			check_money_enough();
			
			
	}


	//获取已还清列表
	function get_pay_off_loan(){

		var url = path + "/user/usercenter/loan_manage/get_pay_off_loan.html";

		$.ajax({

			url:url,
			type:'get',
			cache:false,
			success:function(data){
				var jsObject = JSON.parse(data);
				$("#table2 tr:not(:first)").empty(); 
				var tr_str ='';

				if(jsObject.pay_off_loan_list.length == 0){
					tr_str = '<tr> <td colspan="7" class="td_1">没有记录</td></tr>';
				}else{
					for(var i=0;i<jsObject.pay_off_loan_list.length;i++ ){

						tr_str += 
							'<tr>'
			             + '<td width="14%">'+ jsObject.pay_off_loan_list[i].borrow_title +'</td>'
			             + '<td width="14%">'+ jsObject.pay_off_loan_list[i].borrow_all_money +'</td>'
			             + '<td width="14%">'+ jsObject.pay_off_loan_list[i].annulized_rate +'</td>'
			             + '<td width="14%">'+ jsObject.pay_off_loan_list[i].borrow_duration +'</td>'
			             + '<td width="15%">'+ jsObject.pay_off_loan_list[i].payment_total_money +'</td>'
			             
			             + '<td width="19%">'+ new Date(jsObject.pay_off_loan_list[i].payment_finish_time).format("yyyy-MM-dd") +'</td>'
			             + '<td width="10%">合同</td>'
			          + '</tr>';
					}
				}
				$("#table2 tr:eq(0)").after(tr_str);	
				}
		},"json");
		
	}

	//检测资金是否充足
	function check_money_enough(){
		var cny_can_used = $("#cny_can_used").html();
		var repay_this = $("#repay_this").html();
		if(cny_can_used - repay_this < 0){
			$("#msg").show();
		}else{
			$("#msg").hide();
		}
	}

	//确认还款
	function confirm_repay(){
		var url = path + "/user/usercenter/loan_manage/confirm_repay_money.html";

		$.ajax({

			url:url,
			type:'get',
			data:{"pass_id_str":pass_id_str},
			cache:false,
			success:function(data){//window.location.reload();

				if(data != null){
					showMessage(["提示",data]);
				}
			}
		},"json");
		
	}
	
	//确认  提前还款
	function confirm_pre_repay(){
		var url = path + "/user/usercenter/loan_manage/pre_repay_money.html";
		var gather_money_order_id = $("#gather_money_order_id").html();
		$.ajax({

			url:url,
			type:'get',
			data:{"gather_money_order_id":gather_money_order_id},
			cache:false,
			success:function(data){//window.location.reload();

				if(data != null){
					showMessage(["提示",data]);
				}
			}
		},"json");
		
	}
	function myFunction(){
		x=document.getElementById("submenu6"); 
		x.style.display="none";
	}	
var path = "http://"+window.location.host;

//获取投标记录
function get_bid_record(){
	var url = path + "/user/financial/financial/get_bid_record.html";
	var gather_order_id = $("#gather_order_id").val();
	$.ajax({
		url:url,
		type:'post',
		data:{
			"gather_order_id":gather_order_id
			},
		cache:false,
		success:function(data)
		{
			var jsObject = JSON.parse(data);
			$("#person_time1").html(jsObject.person_time);
			$("#full_scale_use_time").html(jsObject.full_scale_use_time_str);
			$("#table1 tr:not(:first)").empty(); 
			
			var tr_str ='';
			for(var i=0;i<jsObject.bid_record.length;i++ ){

				var tr_class;
				if(i%2 == 0){
					tr_class = 'tr_2';
				}else{
					tr_class = 'tr_3';
				}

				tr_str += '<tr class="'+ tr_class +'">'
		              + '<td>'+(i+1)+'</td>'
		              + '<td class="td_2">'+ jsObject.bid_record[i].nickname +'</td>'
		              + '<td>'+ jsObject.bid_record[i].invest_money +'元</td>'
		              + '<td>' +( new Date((jsObject.bid_record[i].bid_time)).format("yyyy-MM-dd hh:mm:ss")) + '</td>'
		          + '</tr>';
			}
			$("#table1 tr:eq(0)").after(tr_str);
			$("#sum_invest_money").html("投标总额"+ jsObject.sum_invest_money +"元");
			$("#person_time").html("加入"+jsObject.person_time+"人次");
			
			
			//$("#asyncPageDiv").html(jsObject.asyncPageDiv);
		}
	},"json");	
}

 //获取还款表现
function get_repayment_behavior(){
	var url = path + "/user/financial/financial/get_repayment_behavior.html";
	var gather_order_id = $("#gather_order_id").val();
	$.ajax({
		url:url,
		type:'post',
		data:{
			"gather_order_id":gather_order_id
			},
		cache:false,
		success:function(data)
		{
			var jsObject = JSON.parse(data);
			
			$("#table2 tr:not(:first)").empty(); 
			
			var tr_str ='';
			for(var i=0;i<jsObject.repayment_plans.length;i++ ){

				//条纹样式的控制
				var tr_class;
				if(i%2 == 0){
					tr_class = 'tr_2';
				}else{
					tr_class = 'tr_3';
				}

				var is_repay ;
				if(jsObject.repayment_plans[i].is_repay == 0){
					is_repay = '未偿还';
				}else{
					is_repay = '已偿还';
				}

				var repay_time = '';
				if(jsObject.repayment_plans[i].repay_actual_time==null){
					repay_time = '--';
				}else{
					repay_time = jsObject.repayment_plans[i].repay_actual_time;
				}
				
				tr_str += 

						'<tr class="'+ tr_class +'">'
		              + '<td>'+ jsObject.repayment_plans[i].automatic_repayment_date +'</td>'
		              + '<td>'+ is_repay +'</td>'
		              + '<td>'+ jsObject.repayment_plans[i].should_repayment_total +'元</td>'
		              + '<td>'+ jsObject.repayment_plans[i].over_faxi +'元</td>'
		              + '<td>'+ repay_time +'</td>'
		           + '</tr>';
			}
			$("#table2 tr:eq(0)").after(tr_str);
			$("#yihuan_beixi").html("已还本息"+ jsObject.yihuan_benxi_total +"元");
			$("#weihuan_benxi").html("待还本息"+ jsObject.weihuan_beixi_total +"元");
			
		}
	},"json");	
}

//获取债权信息
function get_debt_info(){

	var url = path + "/user/financial/financial/get_debt_info.html";
	var gather_order_id = $("#gather_order_id").val();
	
	$.ajax({
		url:url,
		type:'post',
		data:{
			"debt_id":gather_order_id
		},
		cache:false,
		success:function(data)
		{
			var jsObject = JSON.parse(data);
			
			$("#table3 tr:not(:first)").empty(); 
			
			var tr_str ='';
				//肯定会有债权信息的
			for(var i=0;i<jsObject.debt_info_list.length;i++ ){

				//条纹样式的控制
				var tr_class;
				if(i%2 == 0){
					tr_class = 'tr_2';
				}else{
					tr_class = '';
				}
				tr_str += 
					'<tr class="'+ tr_class +'">'
	              + '<td>'+ (i+1) +'</td>'
	              + '<td class="td_2">'+ jsObject.debt_info_list[i].nickname +'</td>'
	              + '<td>'+ jsObject.debt_info_list[i].remain_to_collect.toFixed(2) +'元</td>'
	              + '<td>'+ jsObject.debt_info_list[i].hold_share +'份</td>'
	           + '</tr>';
			}
			$("#table3 tr:eq(0)").after(tr_str);
			$("#hold_count").html("持有债权人数"+ jsObject.debt_info_list.length +"人");
		}
	},"json");	
}

//获取转让记录
function get_transfer_record(){

	var url = path + "/user/financial/financial/get_transfer_record.html";
	var gather_order_id = $("#gather_order_id").val();
	
	$.ajax({
		url:url,
		type:'post',
		data:{
			"debt_id":gather_order_id
		},
		cache:false,
		success:function(data)
		{
			var jsObject = JSON.parse(data);
			
			$("#table4 tr:not(:first)").empty(); 
			
			var tr_str ='';

			if(jsObject.transfer_record_list.length == 0){
				
			}
				
			for(var i=0;i<jsObject.transfer_record_list.length;i++ ){

				//条纹样式的控制
				var tr_class;
				if(i%2 == 0){
					tr_class = 'tr_2';
				}else{
					tr_class = '';
				}

				tr_str += 

					'<tr class="'+ tr_class +'">'
		              + '<td class="td_2">'+ jsObject.transfer_record_list[i].nickname_out +'</td>'
		              + '<td class="td_2">'+ jsObject.transfer_record_list[i].nickname_in +'</td>'
		              + '<td>'+ jsObject.transfer_record_list[i].trade_money +'元('+ jsObject.transfer_record_list[i].transfer_share +'份)</td>'
		              + '<td>'+ jsObject.transfer_record_list[i].trade_time_str +'</td>'
		           + '</tr>';

			}
			$("#table4 tr:eq(0)").after(tr_str);
			$("#trade_total_money").html("已交易总金额"+ jsObject.trade_total_money.toFixed(2) +"元");

		}
	},"json");	
	
}




<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=__ROOT_PATH__%>">

		<title>${applicationScope.title}</title>
		<link href="<%=__PUBLIC__ %>/css/easydialog.css" rel="stylesheet"  type="text/css" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
		<link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/jisuanji.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/one.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__ %>/style/prod.css" rel="stylesheet"
			type="text/css" />
		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/script/tab.js"></script>
		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/script/pngAlaph.js"></script>
		<script language="JavaScript" type="text/javascript"
			src="<%=__PUBLIC__ %>/js/jquery-1.8.0.min.js"></script>
		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">

		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	</head>

	<body>
		<div class="head_top">
			<h1>
				<a href="<%=__ROOT_PATH__ %>/index/index.html"><img src="<%=__PUBLIC__ %>/images/logo.png" /></a>
			</h1>
		</div>

		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="registration registration_4">
					<div class="registration_top">
						<div class="up">
							<span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">3</span>
           </div>
           <div class="down">
              <span style="color:#cc0000; margin-left:17px;">填写账户信息</span>
              <span style="color:#cc0000; margin-left:135px;">手机信息验证</span>
              <span style="color:#cc0000; margin-left:130px;">注册成功</span>
						</div>
					</div>
					<div class="successful_up">
						<a style="color:#cc0000" href="#">跳过此步>></a>
					</div>
					<div class="successful">
						<div class="successful_top">
							<img src="<%=__PUBLIC__ %>/images/registration_cg.png" alt="" />
						</div>
						<div class="successful_top1">
							恭喜，
							<h id=email>${user.m.nickname}</h>已经注册成功！
						</div>
						<div class="successful_top1 successful_top2">
							超过80%的用户立即进行实名认证，账户更安全理财更方便。
						</div>
						<div class="con">
              <label class="label_1"><span>*</span>&nbsp;姓名：</label>
              <input class="input_1" value="" type="text" name="realname" id="realname">
           </div>
           <div class="con">
              <label class="label_1"><span>*</span>&nbsp;身份证号：</label>
              <input class="input_1 input_2" value="" type="text" name="identity" id="identity">
           </div>
           <div class="con">
              <p class="label_p2"><a href="javascript:void(0);" onclick="authenticate();">认证</a></p>
           </div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/jsp/index/index_foot.jsp" %>
	</body>

	<script type="text/javascript">
  		var i = true;
  		function authenticate() {
		if(i){
			i = false;
			var identity = $("#identity").val();
			var real_name = $("#realname").val();
			$.ajax({
						url:'<%=__ROOT_PATH__%>/user/usercenter/real_name.html',
						type:'post',
						cache:false,
						async:false,
						data:{"identity":identity,"real_name":real_name},
						success:function(data){
							if(data==null){
									showMssage(["警告","<span class=\"hongse\">交易失败,请检查网络连接是否通畅,或服务器处于正常状态！</span>"]);
							}else{
									if(data == 0){//请输入姓名
										showMessage(["提示","请输入姓名"]);
									}
									if(data == 1){//包含非法字符
										showMessage(["提示","包含非法字符"]);
									}
									if(data == 2){//请输入您的二代身份证号码
										showMessage(["提示","请输入您的二代身份证号码"]);
									} 
									if(data == 4){//您的身份证号码有误
										showMessage(["提示","您的身份证号码有误"]);
									}
									if(data == 6){//认证成功
										//window.location.href="<%=__ROOT_PATH__%>/index/index.html";
										//showMessage(["提示","认证成功"]);
										window.location.href="<%=__ROOT_PATH__ %>/user/usercenter/into_safety_information.html";
									}
									if(data == 3){//不可重复认证
										showMessage(["提示","不可重复认证"]);
									}
									if(data == 5){
										showMessage(["提示","实名认证失败"]);
									}
								}
						}
					},'html','application/x-www-form-urlencoded; charset=utf-8');
			}
			i=true;
		}
		  		
  		function showMessage(message){
				var btnFn = function(){
					  easyDialog.close();
					  };
			   var btnFs = function(){
				   window.location.reload();
			   };
				if(message[2]!=null){
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFs,
							 noFn:false
							}
					});	
				}else{
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFn,
							 noFn:false
							}
					});	
				}
			}
  		
  </script>

</html>

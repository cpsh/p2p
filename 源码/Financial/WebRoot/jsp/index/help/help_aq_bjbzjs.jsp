﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html" style="background:#f8bbb2;color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc_zqzr_js">
       <div class="up">什么是本金保障计划</div>
       <div class="con">          
    “本金保障计划”是广州天的富网络科技有限公司(下称“天的富”，运营www.lfoll.com网站（下称“平台”）为保护平台全体理财人的共同权益而建立的信用风险共担机制。除另有说明外，平台的所有理财人经过平台身份认证后，在平台的投资行为均适用于“本金保障计划”，理财人无需为此支付任何费用。<br />
    “本金保障计划”是指在平台发生的适用本金保障计划的每笔借款（下称“受保障借款”，是否适用以平台明示为准）成交时，提取一定比例的金额放入“风险备用金账户”。当理财人投资的某笔受保障借款出现严重逾期时（即逾期超过30天），联富金融将根据“风险备用金账户使用规则”通过“风险备用金”向理财人偿付此笔借款的剩余出借本金或剩余出借本金和逾期当期利息（具体情况视投资标的类型的具体偿付规则为准）。“本金保障计划”为理财人提供了有效的风险共担机制，分散了理财人投资行为所带来的信用风险，营造了一个安全健康的投资环境，保障了理财人的权益。<br />

          <h1>风险备用金账户规则</h1>
          “风险备用金账户”是指联富金融为所服务的所有理财人的共同利益考虑，以联富金融名义单独开设并由招商银行上海分行进行资金托管的一个专款专用账户，服务于平台资金出借人的本金保障计划。<br />

          <h1>1.1. “风险备用金账户”资金来源</h1>
          “风险备用金账户”资金当前全部来源于联富金融向每笔受保障借款的借款人或合作机构（具体以联富金融签署的相关法律文件约定为准）所收取的服务费，联富金融在收取该等服务费的同时，将在收取的该等服务费中按照贷款产品类型及借款人的信用等级等信息计提风险备用金（详见《联富金融风险备用金账户--产品偿付规则明细表》）。计提的风险备用金将存放入“风险备用金账户”进行专户管理。各产品类型及信用等级等所对应的风险备用金的计提标准和方式由联富金融制定并解释，联富金融有权根据实际业务需要对相关内容进行调整，如作修改，联富金融将及时进行披露。<br />

          <h1>1.2. “风险备用金账户”资金用途</h1>
          “风险备用金账户”资金将专门用于在一定限额内补偿联富金融所服务的受保障借款的理财人（债权人）在借款人（债务人）逾期还款超过30日时剩余未还本金或剩余未还本金和逾期当期利息（具体偿付金额以所投资的产品类型的偿付规则为准），即当借款人（债务人）逾期还款超过30日时，联富金融将按照“风险备用金账户资金使用规则”从该账户中提取相应资金用于偿付理财人（债权人）在该笔受保障借款项下剩余未还本金或剩余未还本金和逾期当期利息金额（不同产品的偿付范围请参考《联富金融风险备用金账户—产品偿付规则明细表》）（以下统一简称“逾期应收赔付金额”）。<br />

          <h1>1.3. “风险备用金账户”资金使用规则</h1>
          “风险备用金账户”资金使用遵循以下规则：<br />
          （1）违约偿付规则，即当受保障借款的借款人（债务人）逾期还款超过30日时，方才从“风险备用金账户”资金中提取相应资金偿付理财人（债权人）逾期应收赔付金额。<br />
          （2）时间顺序规则，即“风险备用金账户”资金对受保障借款的理财人（债权人）逾期应收赔付金额的偿付按照该债权逾期的时间顺序进行偿付分配。先偿付逾期在先的债权，后偿付逾期在后的债权。<br />
          （3）债权比例规则，即“风险备用金账户”资金对同一受保障借款的《借款协议》项下不同理财人（债权人）逾期应收赔付金额的偿付按照各债权金额占同协议内发生的债权总额的比例进行偿付分配；<b>当截至某日“风险备用金账户”资金当期余额不足以支付该日所有应享受“风险备用金账户”资金赔付的受保障借款的理财人所对应的逾期应收赔付金额总额时，则该等理财人按照各自对应的逾期应收赔付金额占当期所有理财人对应的逾期应收赔付金额总额的比例进行偿付分配。<br />
          （4）有限偿付规则，即“风险备用金账户”资金对受保障借款的理财人（债权人）逾期应收赔付金额的偿付以该账户的资金总额为限。当该账户余额为零时，自动停止对理财人逾期应收赔付金额的偿付，直到该账户获得新的风险备用金。<br />
          （5）收益转移规则，即当受保障借款的理财人享有了“风险备用金账户”对某笔受保障借款按照既定规则进行的偿付后，联富金融即取得对应债权；该债权对应的借款人其后为该笔受保障借款所偿还的全部本金、利息及罚息归属“风险备用金账户”；如该笔受保障借款有抵押、质押或其他担保的，则联富金融处置抵押质押物或行使其他担保权利的所得等也归属“风险备用金账户”。<br />
          （6）金额上限规则，即当“风险备用金账户”内金额超过届时平台上发生的所有债权未清偿本金金额的10%时，联富金融有权将超出部分转出该账户并自行支配。</b><br />

          <h1>1.4. “风险备用金账户”资金管理原则</h1>
          联富金融已与招商银行股份有限公司上海分行（下称“招商银行上海分行”）就联富金融风险备用金托管问题正式签署协议。招商银行上海分行会对联富金融的风险备用金专户资金进行认真、独立的托管，并针对风险备用金专户资金的实际进出情况每月出具托管报告。自2014年1月1日开始，联富金融将每月公布风险备用金的情况，供用户监督。以下是联富金融风险备用金资金托管及每月公布的一些细则：<br />
          （1）联富金融于每月第一个工作日与招商银行上海分行核对上月末风险备用金余额数据，招商银行上海分行于每月初出具联富金融风险备用金的资金托管报告；<br />
          （2）联富金融于每月10日前，公布上月底的风险备用金余额情况并公布招商银行上海分行出具的资金托管报告；<br />
          （3）联富金融风险备用金在招商银行上海分行托管期间产生的所有利息收入均归入风险备用金并按照本规则进行使用。<br />
          附表：《联富金融风险备用金账户-产品偿付规则明细表》<br />
          
          <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td width="19%" class="td_1">产品类型</td>
                <td colspan="2">风险备用金计提比例<br />（以成交借款金额为基数）</td>
                <td width="23%">偿付范围</td>
                <td width="30%">偿付资金来源</td>
             </tr>
             <tr class="tr_2">
                <td width="19%" class="td_1">信用认证标</td>
                <td width="14%"><p style="background:#e62129;">A+</p><span>A+级</span></td>
                <td width="14%">0%</td>
                <td width="23%">未还本金</td>
                <td width="30%">联富金融风险备用金</td>
             </tr>
             <tr class="tr_2">
                <td width="19%" class="td_1">&nbsp;</td>
                <td width="14%"><p style="background:#ff5a08;">A</p><span>A级</span></td>
                <td width="14%">1.0%</td>
                <td width="23%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
             </tr>
             <tr class="tr_2">
                <td width="19%" class="td_1">&nbsp;</td>
                <td width="14%"><p style="background:#ffcc30;">B+</p><span>B+级</span></td>
                <td width="14%">1.5%</td>
                <td width="23%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
             </tr>
             <tr class="tr_2">
                <td width="19%" class="td_1">&nbsp;</td>
                <td width="14%"><p style="background:#7ad84c;">B</p><span>B级</span></td>
                <td width="14%">2.0%</td>
                <td width="23%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
             </tr>
             <tr class="tr_2">
                <td width="19%" class="td_1">&nbsp;</td>
                <td width="14%"><p style="background:#2ec6f5;">C+</p><span>C+级</span></td>
                <td width="14%">2.5%</td>
                <td width="23%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
             </tr>
             <tr class="tr_3">
                <td width="19%" class="td_1">&nbsp;</td>
                <td width="14%"><p style="background:#2a62f7;">C</p><span>C级</span></td>
                <td width="14%">3.0%</td>
                <td width="23%">&nbsp;</td>
                <td width="30%">&nbsp;</td>
             </tr>
             <tr class="tr_4">
                <td width="19%" class="td_1">智能理财标</td>
                <td colspan="2">大于等于1.0%</td>
                <td width="23%">未还本金；逾期当期利息</td>
                <td width="30%">联富金融风险备用金</td>
             </tr>
             <tr class="tr_3">
                <td width="19%" class="td_1">实地认证标</td>
                <td colspan="2">大于等于1.0%</td>
                <td width="23%">未还本金；逾期当期利息</td>
                <td width="30%">1、实地认证机构风险备用金；<br />2、1项保障不足时，联富金融<br />风险备用金。</td>
             </tr>
             <tr class="tr_4">
                <td width="19%" class="td_1">机构担保标</td>
                <td colspan="2">0%</td>
                <td width="23%">未还本金；逾期当期利息</td>
                <td width="30%">担保机构担保责任</td>
             </tr>
          </table>
          
          注：信用认证标中与部分渠道合作的产品风险备用金计提标准不适用上表规则，将根据合作渠道的具体情形单独设定。
       </div>
    </div>
    
    
  </div>    
</div>
<input type="hidden" name="type" id="type" value="${type }"/>
<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html" style="background:#f8bbb2;color:#fff;"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html">登录注册</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html">账户密码</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html">充值</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html">提现</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html">安全认证</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html">消息中心</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>登录注册</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>什么是合作账号登录？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">合作网站账号登录就是通过将联富金融账号与合作网站账号进行绑定，从而实现用合作网站的账号登录联富金融的功能。目前我们支持新浪微博、QQ合作网站账号登录。</div>
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>如何关联合作账号？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                如果您有联富金融账号，在联富金融合作账号登录页面输入账号、密码关联即可；<br />
                如果您没有联富金融账号，则需要在联富金融合作账号登录页面完善信息并进行手机短信认证，以便注册联富金融并关联账号。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>如何进行合作账号登录？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <h1>合作网站尚未登录</h1>
                点击联富金融登录入口处该合作网站对应的图标，在合作网站账号登录页面中输入合作网站的账号、密码即可登录联富金融。<br />
                <h1>合作网站已经登录</h1>
                点击登录入口处该合作网站账号对应的图标，即可登录联富金融。
             </div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>注册时，进行手机验证，收不到短信怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none;">
                1.请确认手机是否安装短信拦截或过滤软件；<br />
                2.请确认手机是否能够正常接收短信（信号问题、欠费、停机等）；<br />
                3.短信收发过程中可能会存在延迟，请耐心等待，短信在30分钟内均有效；<br />
                4.您还可以联系客服，寻求帮助（4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>注册成功之后昵称可以修改吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                不可以。昵称一旦注册成功不能再修改。
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp"%>

</body>

</html>

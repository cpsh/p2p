<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
	<link href="style/main.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
	<script type="text/javascript">
	function showsubmenu(sid){
	    var whichEl = document.getElementById("submenu" + sid);
	    whichEl.style.display = whichEl.style.display =='none'?'':'none';
	}
	</script>
	<script type="text/javascript" src="<%=__PUBLIC__ %>/js/loadpdfjs.js"></script>
	<script type="text/javascript">
	function load_lfoll_instruction (){
	  alert(1);
	  window.onload =  function(){
        var mPDFHandler = new PDFHandler({url:"<%=__PUBLIC__ %>/template/lian_fu_bao_instructions.pdf"}).embed();
      }
	}
	</script>
  </head>
  
  <body>
  
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>
<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html">理财新手必读</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">收益与费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html">联富宝</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html">散标投资</a>
             <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">债权转让</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>新手必读</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>我可以理财吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">可以，年满18周岁，具有完全民事权利能力和民事行为能力，可以在联富金融网站上进行注册、完成实名认证、绑定银行卡，成为理财人。</div>
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>怎样进行投资？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                <h1>请您按照以下步骤进行投资：</h1>
                1. 在联富金融网站上进行注册、通过实名认证、成功绑定银行卡；<br />
                2. 账户充值；<br />
                3. 浏览联富宝、散标投资列表、债权转让列表，选择自己感兴趣的投资方式；<br />
                4. 确认投资，投资成功。
             </div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>理财收益有多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <h1>联富宝</h1>
                联富宝L03预期年化收益12%。<br />
                联富宝L06预期年化收益14%。<br />
                联富宝L12预期年化收益16%。<br />
                <h1>散标</h1>
                年利率区间10%—24%。
                <h1>债权转让</h1>
                年利率区间10%—24%。
             </div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>理财期限有多久？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none;">
                <h1>联富宝</h1>
                联富宝L03锁定期3个月，到期后自动退出。<br />
                联富宝L06锁定期6个月，到期后自动退出。<br />
                联富宝L12锁定期12个月，到期后自动退出。<br />
                <h1>散标投资</h1>
                借款期限3-36个月，持有90天后可提前转让退出。
                <h1>购买转让的债权</h1>
                借款期限3-36个月，随时可提前转让退出。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=64#64">锁定期是什么？</a>
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>投资后是否可以提前退出？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                <h1>可以</h1>
                联富宝可随时提前退出。<br />
                散标投资持有90天后，可通过转出债权退出。<br />
                买入的转让债权可随时退出。<br />
             </div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>理财是否有额外费用？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                理财过程中可能产生的费用如下：<br />
                1.联富宝相关费用，详情可参见<a href="<%=__ROOT_PATH__ %>/help/help_lc_syyfy.html?type=25#25">联富宝费用。</a><br />
                2.提前赎回散标或债权时产生的债权转让管理费（目前为转出价格的0.5%）。
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>什么是等额本息？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<br />
                具体计算公式如下：<br />
                <img src="<%= __PUBLIC__%>/images/help_2.png" /><br />
                P&nbsp;:每月还款额<br />
                A&nbsp;:借款本金<br />
                b&nbsp;:月利率<br />
                n&nbsp;:还款总期数<br />
                因计算中存在四舍五入，最后一期还款金额与之前略有不同。
             </div>
             
             <div class="down_up"><span>&gt;</span>产品介绍</div>
             
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>联富金融的理财方式有哪些？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                联富金融的理财方式分别有：联富宝、散标投资、债权转让。
             </div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>什么是联富宝？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                联富宝是联富金融推出的便捷高效的自动投标工具。联富宝在用户认可的标的范围内，对符合要求的标的进行自动投标，且回款本金在相应期限内自动复投，期限结束后联富宝会通过联富金融债权转让平台进行转让退出。该计划所对应的标的均100%适用于联富金融本金保障计划并由系统实现标的分散投资。出借所获利息收益可选择每月复投或提取，更好的满足用户多样化的理财需求。<br />
                理财人加入联富宝后，会进入锁定期，锁定期内，投资的回款本金将继续进行投资直到锁定期结束，充分提高资金利用效率。<br />
                锁定期结束后，理财人自动退出联富宝，理财人在该计划内投资的债权将优先进行债权转让。债权转让所得资金及投资回款所得等将不再继续自动投资，系统将在指定时间将此资金转移至用户的主账户供用户自行支配。
             </div>
             
             <div onClick="showsubmenu(15)" class="district"><s>&bull;</s><span>什么是散标？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                散标是消费贷款，生意贷款，净值贷款，所发标的的统称。
             </div>
             <div onClick="showsubmenu(18)" class="district"><s>&bull;</s><span>什么是债权转让？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu18" class="area" style="display:none;">
                指债权持有人通过联富金融债权转让平台将债权挂出且与购买人签订债权转让协议，将所持有的债权转让给购买人的操作。<a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr_js.html">查看详情>></a>
             </div>
             
             <div class="down_up"><span>&gt;</span>收益与费用</div>
             
             <div onClick="showsubmenu(19)" class="district"><s>&bull;</s><span>联富宝收益</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
                联富宝分为三种，联富宝L03锁定期3个月，预期年化收益12%；<br />
                联富宝L06锁定期6个月，预期年化收益14%；<br />
                联富宝L12锁定期12个月，预期年化收益16%。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=60#60">收益处理方式</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=62#62">联富宝安全吗？</a>
             </div>
             
             <div onClick="showsubmenu(20)" class="district"><s>&bull;</s><span>散标收益</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                年利率区间10%—24%，具体收益由您所投资的借款标的利率确定。
             </div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>购买债权转让收益</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                年利率区间10%—24%，具体收益由您所投资的转让债权利率及出让人设定的折让系数确定。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr.html?type=58#58">什么是折价转让？</a>
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>等额本息借款的收益该如何计算？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<br />
                具体计算公式如下：<br />
                <img src="<%= __PUBLIC__%>/images/help_2.png" /><br />
                P&nbsp;:每月还款额<br />
                A&nbsp;:借款本金<br />
                b&nbsp;:月利率<br />
                n&nbsp;:还款总期数<br />
                因计算中存在四舍五入，最后一期还款金额与之前略
             </div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>充值提现费用</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
              <h1>充值费用</h1>
               所有投资用户只要参与一次及一次以上投标（新手标除外），提现免除手续费。如果投资客户（不包括贷款人）没有参与过除新手标以外的投标，则取现的手续费为取现金额的3%。<br />
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>散标费用</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none;">
                散标投资无费用。
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>联富宝费用</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                加入联富宝服务后，可能产生的费用包括：<br />
          <%--
                <h1>加入费用：</h1>
                a%；加入计划金额的a%，期初额外收取，即加入计划金额为10万元，则另行收取10万元*a% 作为加入计划费用归联富金融所有。（当前加入费率为0.0%）<br />
                <h1>管理费用：</h1>
                联富宝对应借款实际利息收入中，超过联富宝预期年化收益以外的部分作为管理费用归联富金融所有，若实际收益不及预期年化收益的，联富金融不收取管理费用。详情参见<a href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/get_lfoll_fix_bid_contract.html?id=${fix_bid_system_order.m.id}&bid_name=${fix_bid_system_order.m.bid_name}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">《联富宝服务协议》</a>。<br />
           --%>
             <h1>提前退出费用：</h1>
               	提前退出联富宝时按加入联富宝金额的3%收取，提前退出费用归联富金融所有。(当前提前退出费率为3.0%)。
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>债权转让管理费</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                债权转让的费用为转让管理费。平台向转出人收取，不向购买人收取任何费用。<br />
                转让管理费金额为成交金额*转让管理费率，转让管理费率在目前的运营中按0.5%收取，具体金额以债权转让页面显示为准。债权转让管理费在成交后直接从成交金额中扣除，不成交平台不向用户收取转让管理费。
             </div>
             
             <div class="down_up"><span>&gt;</span>联富宝</div>
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>什么是联富宝？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                联富宝是联富金融推出的便捷高效的自动投标工具。联富宝在用户认可的标的范围内，对符合要求的标的进行自动投标，且回款本金在相应期限内自动复投，期限结束后联富宝会通过联富金融债权转让平台进行转让退出。该计划所对应的标的均100%适用于联富金融本金保障计划并由系统实现标的分散投资。出借所获利息收益可选择每月复投或提取，更好的满足用户多样化的理财需求。<br />
                理财人加入联富宝后，会进入锁定期，锁定期内，投资的回款本金将继续进行投资直到锁定期结束，充分提高资金利用效率。<br />
                锁定期结束后，理财人自动退出联富宝，理财人在该计划内投资的债权将优先进行债权转让。债权转让所得资金及投资回款所得等将不再继续自动投资，系统将在指定时间将此资金转移至用户的主账户供用户自行支配。<br />相关阅读：<a href="<%=__ROOT_PATH__ %>/template/lian_fu_bao_instructions.pdf" target="_blank">《联富金融联富宝说明书》</a>
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>联富宝的收益有多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                联富宝分为三种，联富宝L03锁定期3个月，预期年化收益12%；联富宝L06锁定期6个月，预期年化收益14%；联富宝L12锁定期12个月，预期年化收益16%。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=60#60">收益处理方式</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfb.html?type=62#62">联富宝安全吗？</a>
             </div>
              <div onClick="showsubmenu(59)" class="district"><s>&bull;</s><span>联富宝的发布时间</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu59" class="area" style="display:none;">
                联富宝的发布时间通常是每周一、周三上午10:00，如遇到假期或者特殊情况，联富金融会调整联富宝发布时间，具体发布时间可在网站首页查看。<br />
                您还可以关注联富金融官方微信获取最新联富宝发布时间。<br />
                <img src="<%=__PUBLIC__ %>/images/qr_code.jpg" />
             </div>
             
             <div onClick="showsubmenu(60)" class="district"><s>&bull;</s><span>收益处理方式</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu60" class="area" style="display:none;">
                联富宝提供以下两种收益处理方式：收益再投资和提取至联富金融账户。用户在加入联富宝时可进行选择，暂不支持中途修改。<br />
                另外，在联富宝退出后，债权转让所得资金及投资回款所得资金将每日定时提取至联富金融主账户。               
             </div>
             
             <div onClick="showsubmenu(61)" class="district"><s>&bull;</s><span>联富宝的期限有多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu61" class="area" style="display:none;">
                联富宝L03的锁定期为3个月，到期后自动退出。<br />
                联富宝L06的锁定期为6个月，到期后自动退出。<br />
                联富宝L12的锁定期为12个月，到期后自动退出。<br />               
             </div>
             
             <div onClick="showsubmenu(62)" class="district"><s>&bull;</s><span>联富宝安全吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu62" class="area" style="display:none;">
                联富宝所投借款100%适用联富金融<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjs.html">本金保障计划</a>。             
             </div>
             
             <div onClick="showsubmenu(63)" class="district"><s>&bull;</s><span>加入联富宝需要什么费用？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu63" class="area" style="display:none;">
                参考<a href="<%=__ROOT_PATH__ %>/help/help_lc_syyfy.html">联富宝费用</a>。        
             </div>
             
             <div onClick="showsubmenu(64)" class="district"><s>&bull;</s><span>锁定期是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu64" class="area" style="display:none;">
                联富宝具有锁定期限制。锁定期内，您可操作提前退出，但会产生相应费用，提前退出费用=加入联富宝金额*3.0%。锁定期满后自动退出。      
             </div>
             
             <div onClick="showsubmenu(65)" class="district"><s>&bull;</s><span>如何加入联富宝</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu65" class="area" style="display:none;">                
                <h1>加入计划</h1>
                在申请期内，并且计划开放额度及加入条件均符合的情况下，可以预定联富宝，预定金额的1%视为定金将被冻结，剩余金额需要在规定的时间内另行支付，在规定时间内未完成支付视为主动放弃，定金将不予返还。<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfbjs.html">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(66)" class="district"><s>&bull;</s><span>预定成功后，如何支付剩余金额？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu66" class="area" style="display:none;">                
                登录联富金融，打开【我的联富金融】--【理财管理】--【联富宝】页面；点击【预定中】栏目下将显示您预定的所有联富宝，点击右侧【支付】；核对信息无误后，点击【确认】；即可加入成功。<a href="<%=__ROOT_PATH__ %>/help/help_lc_lfbzf.html">查看操作流程>></a>       
             </div>
             
             <div onClick="showsubmenu(67)" class="district"><s>&bull;</s><span>预定后，未能按时支付剩余金额，定金将如何处理？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu67" class="area" style="display:none;">                
                若您未能在限定时间内完成剩余金额的支付, 则视为您主动放弃加入联富宝, 定金将不予返还。   
             </div>
             
             <div onClick="showsubmenu(68)" class="district"><s>&bull;</s><span>如何退出联富宝并收回本金？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu68" class="area" style="display:none;">    
                <h1>到期退出</h1>
                锁定期结束当日，联富宝将自动退出。
                <h1>提前退出</h1>
                锁定期内，您可操作提前退出，但会产生相应费用，提前退出费用=加入联富宝金额*3.0%。当前仅支持全额退出。
             </div>
             <div class="down_up"><span>&gt;</span>散标投资</div>
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>信用认证标是什么？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                信用认证标是联富金融通过对借款用户的个人信用资质进行全面审核后，允许用户发布的借款标。
             </div>
             <div onClick="showsubmenu(32)" class="district"><s>&bull;</s><span>散标的发布时间</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                您可打开【我要理财】--【散标投资列表】查看橙色信息栏中的发标时间或<a href="#">查看相关公告>></a>。
             </div>
             
             <div onClick="showsubmenu(33)" class="district"><s>&bull;</s><span>散标的收益有多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                散标年利率区间10%—24%，具体收益由您所投资的借款标的利率确定。
             </div>
             
             <div onClick="showsubmenu(34)" class="district"><s>&bull;</s><span>散标的投资期限有多久？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                从5天-36个月，具体期限由您所投资的借款标的确定。
             </div>
             
             <div onClick="showsubmenu(35)" class="district"><s>&bull;</s><span>是否可以提前收回出借的本金？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu35" class="area" style="display:none;">
                可以，持有90天后您可通过债权转让提前收回出借的资金。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr_js.html">如何转出债权提前收回投资？</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr_js.html">什么是债权转让？</a>
             </div>
             
             <div onClick="showsubmenu(36)" class="district"><s>&bull;</s><span>散标投资安全吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu36" class="area" style="display:none;">
                为保障理财人的出借安全，联富金融设有适用于全体理财用户的<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjh.html">本金保障计划</a>。
             </div>
             
             <div onClick="showsubmenu(37)" class="district"><s>&bull;</s><span>散标投资有费用吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu37" class="area" style="display:none;">
                散标投资无费用。
             </div>
             
             <div onClick="showsubmenu(38)" class="district"><s>&bull;</s><span>投标前需要注意哪些事项？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu38" class="area" style="display:none;">
                1.投标前认真阅读该笔借款的详细信息（包括借款金额、利息率、借款期限、借款者信用等级等），以确定您所要投的标符合您的风险承受能力和所要求的投资回报率。<br />
                2.投标前应知道若您所投标的借款人发生违约，违约损失由投资该标的所有理财人共同承担。<br />
                3.投标前请核实自己将要理财的金额，确认无误后再进行操作。
             </div>
             
             <div onClick="showsubmenu(39)" class="district"><s>&bull;</s><span>如何投资散标？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu39" class="area" style="display:none;">
                登录联富金融，点击【我要理财】--【散标投资列表】，选择借款标的，输入金额，点击【确认】完成投资。<a href="<%=__ROOT_PATH__ %>/help/help_lc_sbrhtz.html">查看操作流程</a>
             </div>
             
             <div onClick="showsubmenu(40)" class="district"><s>&bull;</s><span>如何分散投标？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu40" class="area" style="display:none;">
                1.尽量进行分散投资，这样可降低单一借款人违约对投资收益的影响。例如您可以把5000元借给10个借款人。<br />
                2.在同等收益的情况下投标给信用等级较高的借款人。<br />
                3. 在投标时注意了解对应标的保障范围。如保障本金还是保障本金+利息。
             </div> 
             
             <div onClick="showsubmenu(41)" class="district"><s>&bull;</s><span>如何收取还款？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu41" class="area" style="display:none;">
                借款人在规定的还款时间内将钱充值到与联富金融合作的第三方支付平台上，联富金融系统会发送邮件通知所有理财人借款人已成功还款。理财人可按个人需求选择提现或继续投资。<br />
                注：借款人也可能提前偿还全部借款或者在到期日前手动提前偿还借款，请理财人注意查收联富金融发出的通知。
             </div> 
             
             <div onClick="showsubmenu(42)" class="district"><s>&bull;</s><span>借款人逾期怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu42" class="area" style="display:none;">
                自逾期开始之后，正常利息停止计算。按照下面公式计算罚息：<br />
                <img src="<%= __PUBLIC__%>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="30%">金额</td>
                      <td width="30%">2万元以下</td>
                      <td width="40%">2万(含)-5万元</td>
                   </tr>
                   <tr>
                      <td width="30%">手续费</td>
                      <td width="30%">1元/笔</td>
                      <td width="40%">3元/笔</td>
                   </tr>
                </table>
                注：本计算公式按50元一份计算。<br />
                联富金融有一套完善的催收机制，通过电话短信提醒、上门拜访、法律诉讼等多种方式，有效的保证了平台上绝大部分借款人的及时还款。延迟还款的借款人需按约定交纳罚息和违约金。<br />
             </div>
             
             <div onClick="showsubmenu(43)" class="district"><s>&bull;</s><span>借款人提前还款怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu43" class="area" style="display:none;">
                借款人可以提前偿还借款，如果借款人提前偿还全部借款，需要额外支付给理财人借款剩余本金的1%作为违约金，不用再支付后续的利息及管理费用。<br />
                注：部分借款标的违约金费率为0%。<br />
                具体计算公式如下：<br />
                <img src="<%= __PUBLIC__%>/images/help_4.png" /><br />
             </div> 
             
             <!-- <div onClick="showsubmenu(44)" class="district"><s>&bull;</s><span>自动投标工具是什么？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu44" class="area" style="display:none;">
                是一款依据您设定的规则，由系统自动代您进行投标操作的工具。可以在【我的联富金融】—【理财管理】—【自动投标工具】中进行设置。<a href="#">马上开启</a>
             </div> 
              -->
             <div class="down_up"><span>&gt;</span>债权转让</div>
             
             <div onClick="showsubmenu(45)" class="district"><s>&bull;</s><span>什么是债权转让？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu45" class="area" style="display:none;">
                指债权持有人通过联富金融债权转让平台将债权挂出且与购买人签订债权转让协议，将所持有的债权转让给购买人的操作。<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr_js.html">查看详情>></a>
             </div> 
             
             <div onClick="showsubmenu(46)" class="district"><s>&bull;</s><span>什么样的债权可以转让？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu46" class="area" style="display:none;">
                1.债权持有90天后即可进行转让。<br />
                2.债权没有处于逾期状态。如转让挂出时没有处于逾期，但在转让中时债权变为逾期状态，系统将会把债权停止转让。<br />
             </div> 
             
             <div onClick="showsubmenu(47)" class="district"><s>&bull;</s><span>如何转出债权提前收回投资？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu47" class="area" style="display:none;">
                您可以在债权转让列表页面进行购买债权。<br />
                登录后可以按照份数购买债权，在确认购买份数及金额等信息后点击确定按钮，即完成债权的购买，此笔债权的持有人即发生了变更。购入人可以在已转入的债权或者还款中的债权页面看到买入的债权。<a href="#">查看操作流程>></a><br />
             </div> 
             
             <div onClick="showsubmenu(48)" class="district"><s>&bull;</s><span>如何买入债权，进行理财？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu48" class="area" style="display:none;">
                当您持有的债权处于可转让状态时，您可以打开【我的联富金融】--【理财管理】--【债权转让】--【可转出的债权】页面进行债权转让操作<br />
                您也可以在【债权转让】--【可转让的债权】页面进行批量债权转让操作，批量债权转让不可以填写份数，执行批量债权转让操作视为所选债权可以全部转让，且转让系数统一设置，不可单独设置。<a href="#">查看操作流程>></a><br />
             </div> 
             
             <div onClick="showsubmenu(49)" class="district"><s>&bull;</s><span>买入转让债权的收益有多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu49" class="area" style="display:none;">
                年利率区间10%—24%，具体收益由您所投资的转让债权利率及出让人设置的转让系数确定。<br />
             </div> 
             
             <div onClick="showsubmenu(50)" class="district"><s>&bull;</s><span>债权转让管理费如何收取？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu50" class="area" style="display:none;">
                参考<a href="<%=__ROOT_PATH__ %>/help/help_lc_syyfy.html?type=26#glf">债权转让管理费</a>。<br />
             </div> 
             
             <div onClick="showsubmenu(51)" class="district"><s>&bull;</s><span>为什么买入债权后，收益会是负数？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu51" class="area" style="display:none;">
                不必担心，您并未受到损失。<br />
                由于购买转让的债权时您先支付了应计利息给出让人，所以此数值暂时为负，待借款人下次还款时，此负值会被抹平。<br />
                相关阅读：<a href="<%=__ROOT_PATH__ %>/help/help_lc_zqzr.html?type=55#55">债权价值是如何计算的？</a><br />
             </div>
             
             <div onClick="showsubmenu(52)" class="district"><s>&bull;</s><span>什么情况下债权的价值会发生变化？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu52" class="area" style="display:none;">
                1.当借款人还款后，债权价值的待收本金部分发生变化导致债权发生变化。<br />
                2.当日期发生变化时，债权价值的应计利息部分会发生变化。<br />
             </div>
             
             <div onClick="showsubmenu(53)" class="district"><s>&bull;</s><span>什么情况下债权会被锁定？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu53" class="area" style="display:none;">
                1.在借款人发生还款时，债权会被锁定。<br />
                2.联富金融系统每天进行标的状态(还款中、逾期等)扫描时，债权会被锁定。<br />
             </div>
             
             <div onClick="showsubmenu(54)" class="district"><s>&bull;</s><span>什么情况下购买债权会失败？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu54" class="area" style="display:none;">
                1.在购买时债权被锁定。<br />
                2.在购买时债权的价值发生了变化。<br />
                3.购买时债权的状态发生了变化，如变为逾期债权、债权已被借款人还清等。<br />
                4. 您的账户余额不足。
             </div>
             
             <div onClick="showsubmenu(55)" class="district"><s>&bull;</s><span>债权价值是如何计算的？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu55" class="area" style="display:none;">
                <h1>1.转让时，当期还款处于未还款状态</h1>
                <img src="<%= __PUBLIC__%>/images/help_5.png" /><br />
                F&nbsp;:债权公允价值<br />
                D&nbsp;:应计利息天数<br />
                b&nbsp;:月利率<br />
                x&nbsp;:产品折让参数, 本息保障类产品为1，本金保障类产品为0<br />
                <img src="<%= __PUBLIC__%>/images/help_6.png" /><br />
                注：应计利息天数超过30天按30天计算<br />
                注：产品保障类型参数：本金保障的债权为0，本息保障的债权为1。<br />
                <h1>2.转让时，当期还款处于已还款状态，但下一期处于未还款状态</h1>
                <img src="<%= __PUBLIC__%>/images/help_7.png" /><br />
                F&nbsp;:债权公允价值<br />
                b&nbsp;:月利率<br />
                注：应计利息天数超过30天按30天计算<br />
                注：产品保障类型参数：本金保障的债权为0，本息保障的债权为1。<br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
                <h1>3.转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清</h1>
                <img src="<%= __PUBLIC__%>/images/help_9.png" /><br />
                F&nbsp;:债权公允价值<br />
                b&nbsp;:月利率<br />
                t&nbsp;:最后还款所在期数与成交日期所在期数之差<br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
             </div>
             
             <div onClick="showsubmenu(56)" class="district"><s>&bull;</s><span>债权的转让价格是如何确定的？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu56" class="area" style="display:none;">
                1.转让时，当期还款处于未还款状态<br />
                <img src="<%= __PUBLIC__%>/images/help_11.png" /><br />                
                <h1>1.转让时，当期还款处于未还款状态。</h1>
                <img src="<%= __PUBLIC__%>/images/help_12.png" /><br />
                <img src="<%= __PUBLIC__%>/images/help_13.png" /><br />                
                <img src="<%= __PUBLIC__%>/images/help_6.png" /><br />
                R&nbsp;:转让系数<br />
                b&nbsp;:月利率<br />
                x&nbsp;:产品折让参数, 本息保障类产品为1，本金保障类产品为0<br />
                <h1>2.转让时，当期还款处于已还款状态，但下一期处于未还款状态。</h1>
                <img src="<%= __PUBLIC__%>/images/help_12.png" /><br />
                <img src="<%= __PUBLIC__%>/images/help_14.png" /><br /><br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
                b&nbsp;:月利率<br />
                <h1>3.转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清。</h1>
                <img src="<%= __PUBLIC__%>/images/help_15.png" /><br />
                <img src="<%= __PUBLIC__%>/images/help_16.png" /><br /><br />
                <img src="<%= __PUBLIC__%>/images/help_8.png" /><br />
                b&nbsp;:月利率<br />
                t&nbsp;:最后还款所在期数与成交日期所在期数之差<br />
             </div>
             
             <div onClick="showsubmenu(57)" class="district"><s>&bull;</s><span>什么是应计利息？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu57" class="area" style="display:none;">
                应计利息：在债权转让过程中，在还款日前售出债权，利息不归转出人所有，不过购买债权的人必须依据上一次还款日至交易日的持有时间按比例给转出人相应的补偿，此补偿的利息称为应计利息。即实际上转出人通过应计利息获得该还款期内其持有此债权的利息，而转入人获得之后持有期的利息。<br />
                <img src="<%= __PUBLIC__%>/images/help_17.png" /><br /><br />
                <img src="<%= __PUBLIC__%>/images/help_6.png" /><br />
                b&nbsp;:月利率<br />
                举例：假如A持有一份剩余未还本金为20元的实地认证标，年利率为12%，还款日为每月8号，在6号以100%的转让系数挂出转让（即平价转让）。B在6号成功购买，成交价格包括两部分，即未还本金20元和应计利息20*1%*28/30=0.19元，共计20.19元。成交后<br />
                A回收本金20元，收到应计利息0.19元并计入已赚利息，债权转移至B名下，资产减少20元；<br />
                B支出20.19元获得此债权，其中债权的待收本金为20元，债权记为资产，同时0.19元为应计利息的支出，记（-0.19）元为已赚利息。等到8号，借款人支付该月还款时，B获得利息收入0.2元，与之前的应计利息支出合计为 0.2+(-0.19)=0.01元；此合计利息即为B于6号购买债权后，持有此债权2天所应获得的利息。<br />
                总结：通过债权转让和应计利息的计算，假设该持有期有30天，A获得了28天的利息，B获得了2天的利息。
             </div>
             
             <div onClick="showsubmenu(58)" class="district"><s>&bull;</s><span>什么是折价转让？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu58" class="area" style="display:none;">
                债权转出人在出售债权时，选择在待收本金价格上打折，给予债权购买人折扣的让利行为。即债权转出人将转让系数设置在100%以下。
             </div>
               
          </div>
       </div>
    </div>
    
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>

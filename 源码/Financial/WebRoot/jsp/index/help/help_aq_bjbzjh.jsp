﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html" style="background:#f8bbb2;color:#fff;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">本金保障计划</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">法律与政策保障</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">借款审核与风控</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">账户及隐私安全</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">用户的自我保护</a>
             <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">网站相关协议</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>本金保障计划</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>在联富金融投资能否保障本金安全？</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">
                为保障理财人的出借安全，联富金融设有适用于全体理财用户的<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjs.html">本金保障计划。</a>
             </div>
             
             <div onClick="showsubmenu(7)" id="7" class="district"><s>&bull;</s><span>什么是本金保障计划</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                “本金保障计划”是联富金融(下称“平台”)为保护平台全体理财人的共同权益而建立的信用风险共担机制。除另有说明外，平台的所有理财人经过平台身份认证后，在平台的投资行为均适用于“本金保障计划”，理财人无需为此支付任何费用。<br />
                “本金保障计划”是指在平台发生的适用本金保障计划的每笔借款（下称“受保障借款”，是否适用以平台明示为准）成交时，提取一定比例的金额放入“风险备用金账户”。当理财人投资的某笔受保障借款出现严重逾期时（即逾期超过30天），联富金融将根据“风险备用金账户使用规则”通过“风险备用金”向理财人偿付此笔借款的剩余出借本金或剩余出借本金和逾期当期利息（具体情况视投资标的类型的具体偿付规则为准）。“本金保障计划”为理财人提供了有效的风险共担机制，分散了理财人投资行为所带来的信用风险，营造了一个安全健康的投资环境，保障了理财人的权益。
            	<a href="<%=__ROOT_PATH__ %>/help/help_aq_bjbzjs.html">查看详情>></a>
             </div>  
               
          </div>
       </div>
    </div>
    
  </div>    
</div>
<input type="hidden" name="type" id="type" value="${type }"/>
<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
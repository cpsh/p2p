﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html" style="background:#f8bbb2;color:#fff;"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html">产品介绍</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html">借款费用</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html">如何申请</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html">认证资料</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html">信用审核</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html">信用等级与额度</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html">筹款与提现</a>
             <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">             
             
             <div class="down_up"><span>&gt;</span>信用审核</div>
             
             <div onClick="showsubmenu(45)" class="district"><s>&bull;</s><span>什么是信用审核？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu45" class="area" style="display:none;">
                申请借款的用户需要根据不同的产品填写借款信息，包括个人信息、家庭信息、工作信息、资产信息、信用信息，并提交相应的信用认证材料。信审部门会综合评估借款人的个人、家庭、工作、资产、信用情况，最终根据借款人的整体信用资质给出相应的信用分数、信用等级及借款额度。
             </div> 
             
             <div onClick="showsubmenu(46)" class="district"><s>&bull;</s><span>提交认证资料后何时开始审核？审核时间多久？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu46" class="area" style="display:none;">
                我们将在您点击【提交申请】后，确认您的必要认证资料已上传齐全，才会为您提交审核。
    您的借款申请将于3个工作日内为您审核完成。
             </div> 
             
             <div onClick="showsubmenu(47)" class="district"><s>&bull;</s><span>为何我提交一部分认证资料，但3个工作日后没有收到审核结果？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu47" class="area" style="display:none;">
                请确认您的必要认证资料已上传完整，且已点击【提交申请】，才会进入审核。
             </div> 
             
             <div onClick="showsubmenu(48)" class="district"><s>&bull;</s><span>如何查看我的审核进度和结果？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu48" class="area" style="display:none;">
                登陆联富金融网站，打开【我要借款】--【工薪贷】查看详情页面（以工薪贷为例），点击【申请借款】，【填写借款信息】并点击【提交申请】后，即可查看您的审核进度。<a href="<%=__ROOT_PATH__ %>/help/help_jk_rhcksh.html">查看操作流程>></a><br />
                审核完成后，如果您的申请通过，您将收到审核通过的站内信和邮件通知，同时客户经理将致电您沟通借款金额、期限和利率；<br />
                如果您的申请未通过，您将收到申请驳回的短信、站内信和邮件通知，您可登陆联富金融账户或邮箱查看驳回原因。
             </div> 
             
             <div onClick="showsubmenu(49)" class="district"><s>&bull;</s><span>如何联系审核人员？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu49" class="area" style="display:none;">
                您无需主动联系审核员，审核员在需要时会主动联系您。如遇特殊情况时，客户经理将致电与您沟通，您也可以联系客服寻求帮助（4006-888-923）。
             </div> 
             
             <div onClick="showsubmenu(50)" class="district"><s>&bull;</s><span>审核员会打电话给我的工作单位或亲属朋友吗？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu50" class="area" style="display:none;">
                审核人员会根据您在申请表中填写的工作单位或亲属朋友联系人信息，进行电话核实。在致电您的亲属朋友联系人核实前，会先与您进行电话确认。
             </div> 
             
             <div onClick="showsubmenu(51)" class="district"><s>&bull;</s><span>审核过程主要看哪些方面？如何提高我的借款成功率？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu51" class="area" style="display:none;">
                信审部门会综合评估您的整体资质，包括您的个人情况、家庭情况、工作情况、资产情况、信用情况等。我们建议您尽量全面的上传真实有效的必要认证资料和可选认证资料，以提高您的借款成功率。
             </div>
             
             <div onClick="showsubmenu(52)" class="district"><s>&bull;</s><span>资料被驳回的原因是什么？可否重新认证？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu52" class="area" style="display:none;">
                1.资料上传不完整：您可以按照资料驳回的站内信和邮件通知，重新认证上传；<br />
                2.资料暂不符合认证要求：您的资料未能通过审核，无法再次认证。
             </div>
             
             <div onClick="showsubmenu(53)" id="53" class="district"><s>&bull;</s><span>被拒贷或流标的原因是什么？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu53" class="area" style="display:none;">
                可能的原因有：<br />
                1.您的年龄暂不符合借款要求（22-55周岁）。<br />
                2.您本人不符合申请人身份条件。<br />
                3.您的工作年限或经营时间不符合申请条件。<br />
                4.您的企业或网店不符合申请条件。<br />
                5.您的月收入或月交易额不符合申请条件。<br />
                6.您在审核过程中未配合完成补充资料或信息核实工作，或您主动放弃申请。<br />
                7.审核人员对您的身份信息、工作信息、信用记录、还款能力等进行审核后认为您目前综合评分不足，暂时不符合借款要求。
             </div>
             
             <div onClick="showsubmenu(54)" class="district"><s>&bull;</s><span>审核失败后如何操作？何时能再次申请？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu54" class="area" style="display:none;">
                审核失败后，您将收到资料驳回和借款申请驳回的短信、站内信和邮件通知，您可登陆联富金融账户或邮箱查看驳回原因。（<a href="<%=__ROOT_PATH__ %>/help/help_jk_xysh.html?type=53#53">点击这里</a>了解可能导致审核失败的原因）<br />
                如果您因个人信息或认证资料未提交齐全导致审核失败，您可随时再次发布借款申请；<br />
                如果您因申请条件不符合要求或认证资料未通过审核，您可在半年后再次发布借款申请。
             </div>
             
             <div onClick="showsubmenu(55)" class="district"><s>&bull;</s><span>审核成功后如何操作？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu55" class="area" style="display:none;">
                审核成功后，您将收到审核通过的站内信和邮件通知，同时客户经理将致电您沟通借款金额、期限和利率，确认后帮助您进入筹款。                
             </div>
             
             <div onClick="showsubmenu(56)" class="district"><s>&bull;</s><span>申请资料能否退还给我？</span><a href="#"><img src="<%=__PUBLIC__ %>/images/help_ck.png" /></a></div>
             <div id="submenu56" class="area" style="display:none;">
                根据行业规范，对于用户申请过程中提交的信息和资料不予退还。<br />
                联富金融严格遵守国家相关的法律法规，对用户的隐私信息进行保护。未经您的同意，联富金融不会向任何第三方公司、组织和个人披露您的个人信息、账户信息以及交易信息（法律法规另有规定的除外）。<br />
                同时，联富金融公司内部也设有严格、完善的权限管理体系，以保证每一位内部员工都只能查看自己职责和权限之内的数据和信息。
             </div>     
               
          </div>
       </div>
    </div>
    
  </div>    
</div>

 <input type="hidden" name="type" id="type" value="${type }"/>
	<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
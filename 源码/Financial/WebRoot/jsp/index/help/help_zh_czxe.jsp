﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;<a href="#">帮助中心</a>&nbsp;<span>></span>&nbsp;<a href="#">理财帮助</a>&nbsp;<span>></span>联富宝
    </div>
   <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html" style="background:#f8bbb2;color:#fff;"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
   <div class="help_lc_zqzr_js">
       <div class="up">充值限额是多少？</div>
       <div class="con">
          充值限额是由银行支付限额、第三方支付平台支付限额和用户自己设定的支付限额三者共同决定，限额取三者最小值。例如：工商银行网银用户（办理U盾）的每笔支付限额是100万，但是工商银行和快钱的协议规定用户使用工商银行网 银的每笔支付限额为30万，然而用户自己设定的每笔支付限额为10万，那么用户每次可以充值的金额为10万。<br />
          第三方支付限额：各大银行与第三方支付平台协议规定的每笔支付限额为30万。<br />
          银行限额参考下表：<br />
         <table class="table_1 table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr class="tr_1">
              <td class="td_2">银行</td>
              <td class="td_3">单笔限额</td>
              <td class="td_3">单日限额</td>
              <td class="td_2">必要条件</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">工商银行</td>
              <td class="td_3">1000万</td>
              <td class="td_3">无限额</td>
              <td class="td_2">工行网银U盾用户</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">2000</td>
              <td class="td_3">5000</td>
              <td class="td_2">电子银行口令卡及手机短信认证</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">农业银行</td>
              <td class="td_3">100万</td>
              <td class="td_3">500万</td>
              <td class="td_2">开通2代K宝证书</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">50万</td>
              <td class="td_3">100万</td>
              <td class="td_2">开通1代K宝证书</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">1000</td>
              <td class="td_3">3000</td>
              <td class="td_2">开通动态口令卡，IE浏览器证书</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">招商银行</td>
              <td class="td_3">1000万</td>
              <td class="td_3">无限额</td>
              <td class="td_2">开通专业版UKey</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">500</td>
              <td class="td_3">500</td>
              <td class="td_2">网上或柜台开通大众版</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">中国银行</td>
              <td class="td_3">5万</td>
              <td class="td_3">10万</td>
              <td class="td_2">开通网银</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">建设银行</td>
              <td class="td_3">50万</td>
              <td class="td_3">50万</td>
              <td class="td_2">网银盾</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">1000</td>
              <td class="td_3">1000</td>
              <td class="td_2">动态口令卡、短信动态口令</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">500</td>
              <td class="td_3">500</td>
              <td class="td_2">无需开通、使用账号支付</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">广发银行</td>
              <td class="td_3">30万</td>
              <td class="td_3">30万</td>
              <td class="td_2">开通数字证书及Key盾</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">3000</td>
              <td class="td_3">3000</td>
              <td class="td_2">开通通用版（银行网站在线申请）</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">兴业银行</td>
              <td class="td_3">5000</td>
              <td class="td_3">5000</td>
              <td class="td_2">开通网银</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">交通银行</td>
              <td class="td_3">100万</td>
              <td class="td_3">100万</td>
              <td class="td_2">开通证书版</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">5000</td>
              <td class="td_3">5000</td>
              <td class="td_2">开通短信密码</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">邮储银行</td>
              <td class="td_3">100万</td>
              <td class="td_3">100万</td>
              <td class="td_2">办理UKey+开通银行短信服务</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">10万</td>
              <td class="td_3">10万</td>
              <td class="td_2">电子令牌</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">1万</td>
              <td class="td_3">1万</td>
              <td class="td_2">手机短信</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">浦发银行</td>
              <td class="td_3">5000</td>
              <td class="td_3">无限额</td>
              <td class="td_2">数字证书版</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">5000</td>
              <td class="td_3">5万</td>
              <td class="td_2">动态密码版</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">民生银行</td>
              <td class="td_3">50万</td>
              <td class="td_3">50万</td>
              <td class="td_2">开通网银u宝</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">5000</td>
              <td class="td_3">5000</td>
              <td class="td_2">开通网银贵宾版</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">300</td>
              <td class="td_3">300</td>
              <td class="td_2">开通网银大众版</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">光大银行</td>
              <td class="td_3">50万</td>
              <td class="td_3">50万</td>
              <td class="td_2">开通令牌或阳光网盾</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">5000</td>
              <td class="td_3">5000</td>
              <td class="td_2">开通手机动态密码</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">中信银行</td>
              <td class="td_3">100万</td>
              <td class="td_3">100万</td>
              <td class="td_2">开通Key证书默认额度（可修改）</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">1000</td>
              <td class="td_3">1000</td>
              <td class="td_2">开通文件证书</td>
            </tr>
            <tr class="tr_3">
              <td class="td_2">平安银行</td>
              <td class="td_3">5万</td>
              <td class="td_3">5万</td>
              <td class="td_2">开通网银</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">华夏银行</td>
              <td class="td_3">5万</td>
              <td class="td_3">10万</td>
              <td class="td_2">开通数字证书</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">1000</td>
              <td class="td_3">5000</td>
              <td class="td_2">开通手机动态密码</td>
            </tr>
            <tr class="tr_2">
              <td class="td_2">&nbsp;</td>
              <td class="td_3">300</td>
              <td class="td_3">1000</td>
              <td class="td_2">开通网银大众版</td>
            </tr>
         </table>
       </div>
    </div>
    
  </div>    
</div>
<input type="hidden" name="type" id="type" value="${type }"/>
<%@ include file="/jsp/index/index_foot.jsp" %>

</body>

</html>
<script>
	$(document).ready(function(){
		var sid = $("#type").val();
		var whichEl = document.getElementById("submenu" + sid);
   	 whichEl.style.display = whichEl.style.display =='none'?'':'none';
	});
</script>
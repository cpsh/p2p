<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%@taglib uri="http://com.tfoll.web/lable/sys_notification" prefix="tfoll"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
		
	    <link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/style/float.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/style/activities_2.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/style/activities.css" rel="stylesheet" type="text/css" />
        <link href="<%=__PUBLIC__ %>/style/jisuanji.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/style/one.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/style/prod.css" rel="stylesheet" type="text/css" />
	    
	    <link href="<%=__PUBLIC__ %>/css/clearbox.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/css/easydialog.css" rel="stylesheet"  type="text/css" />
	    <link href="<%=__PUBLIC__ %>/css/jquery.jslides.css" rel="stylesheet" type="text/css" />
	    <link href="<%=__PUBLIC__ %>/css/loginDialog.css" rel="stylesheet" type="text/css" />
		
        
       
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/loginDialog.js"></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/loginDialog_2.js"></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/pngAlaph.js"></script>

	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery-1.8.0.min.js"></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.upload.js"></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/clearbox.js" ></script>
	 <script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
	 
	  <script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.jslides.js"></script>
	  <script type="text/javascript" src="<%=__PUBLIC__ %>/js/u_jihua.js"></script>
	 
 <c:if test="${sessionScope.user==null}">    
	<div class="head">
				<h1>
					<%--<p style="float:left; color: #FFF">
						问候语！
					</p>
					--%>
					<p><a href="<%=__ROOT_PATH__ %>/index/registration.html" style="color:#c80000">快速注册</a><a href="<%=__ROOT_PATH__ %>/index/to_login.html" style="color:#ffbc00">立即登录</a><a href="<%=__ROOT_PATH__ %>/help/help_index.html">帮助</a><p style="color:#FFF">您好，欢迎来到联富金融!</p>
				</h1>
	
	</div>
</c:if>
<%-- 
<c:if test="${sessionScope.user!=null}">
	<div class="head">
				<h1>
					<p>
						<c:choose>
							<c:when test="${sessionScope.user==null}"></c:when>
							<c:otherwise>
								<a style="cursor:text;" href="<%=__ROOT_PATH__ %>/user/usercenter/into_user_center.html" title="用户中心">
									您好 , ${sessionScope.user.m.nickname}
								</a>
								<a href="<%=__ROOT_PATH__ %>/index/user_exit.html" style="color: #0CF">【退出】</a>
							</c:otherwise>
						</c:choose>
						<tfoll:sys_notification/>
						<a href="<%=__ROOT_PATH__ %>/help/help_index.html">帮助&nbsp;&nbsp;&nbsp;</a>
					</p>
				</h1>
	
	</div>
</c:if>
--%>
<c:if test="${sessionScope.user!=null}">
	<div class="head">
				<h1>
						<p>
							<a href="<%=__ROOT_PATH__ %>/index/user_exit.html" style="color: #0CF">【退出】</a>
							<tfoll:sys_notification/>
							<a href="<%=__ROOT_PATH__ %>/help/help_index.html">帮助&nbsp;&nbsp;&nbsp;</a>
						</p>
						
						<c:choose>
							<c:when test="${sessionScope.user==null}"></c:when>
							<c:otherwise>
								<dl><!--您好，欢迎来到联富金融！-->
      								<dt>您好，</dt>
							        <dd>
							           <a class="a_1" href="<%=__ROOT_PATH__ %>/user/usercenter/into_user_center.html" onmouseover="get_can_used_cny();">${sessionScope.user.m.nickname}</a>
							           <div class="area">
							              <div class="up">
							                 <a href="<%=__ROOT_PATH__ %>/user/usercenter/into_user_center.html">
							                        <c:if test="${user.m.url == null}"><img src="<%=__PUBLIC__%>/images/tou.jpg" /></c:if>
      												<c:if test="${user.m.url != null}"> <img src="${applicationScope.user_image_path}${user.m.url}" width="80px;" height="80px;"/></c:if>
							                 </a>
							                 <span class="span_1">账户余额 <s id="acount_balance">0.00元</s></span>
							                 <span class="span_2"><a href="<%=__ROOT_PATH__ %>/user/usercenter/recharge/into_user_recharge_index.html">充值</a><a style=" background:#ed5050;" href="<%=__ROOT_PATH__ %>/user/usercenter/withdrawal/to_user_withdraw.html">提现</a></span>
							              </div>
							              <div class="down"><a href="<%=__ROOT_PATH__ %>/user/usercenter/financial_manage/to_my_debt.html">理财管理</a>|<a href="<%=__ROOT_PATH__ %>/user/usercenter/loan_manage/to_my_loan_apply.html">借款管理</a>|<a href="<%=__ROOT_PATH__ %>/user/usercenter/into_user_center.html">我的联富金融</a></div>              
							           </div>
							        </dd>
								</dl>
								
								
							</c:otherwise>
						</c:choose>
				</h1>
	
	</div>
</c:if>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__%>/images/logo.png" /></a><a style="margin-left:30px;" href="<%=__ROOT_PATH__ %>/index/activity/to_recommend.html"><img src="<%=__PUBLIC__ %>/images/logo_1.gif" /></a></h1>
  <ul>
    <li>
    	<a href="<%=__ROOT_PATH__%>/index/user_to_index.html">首页</a>
    </li>
    <li>
    	<a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html" class="a">我要理财</a>
    	<span>
    	  <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">精英散标</a>
          <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a>
          <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_creditor_right.html">债权转让</a> 
          <img src="<%=__PUBLIC__%>/images/registration_pic9.png">
       </span>
    	
    </li>
    <li>
    	<a class="a">我要借款</a>
    	<span>
          <a href="<%=__ROOT_PATH__ %>/user/financial/borrow/loan_applay.html?borrow_type=1">消费贷</a>
          <a href="<%=__ROOT_PATH__ %>/user/financial/borrow/loan_applay.html?borrow_type=2">生意贷</a>
          <a href="<%=__ROOT_PATH__ %>/user/financial/borrow/loan_applay.html?borrow_type=3">净值贷</a>
          <img src="<%=__PUBLIC__ %>/images/registration_pic9.png">
       </span>
    </li>
    <li>
    	<a href="<%=__ROOT_PATH__ %>/index/newguidance/invest.html">新手指引</a>
    </li>
    <li><a href="<%=__ROOT_PATH__ %>/index/about/about_page.html">关于我们</a></li>
  </ul>
</div>
<script type="text/javascript" >
			/*
			* method:showMssage
		    * param:message 
			* explain:显示对话信息
			* showMessage(["提示","请输入有效的金额"]);
			*/
			function showMessage(message){
				var btnFn = function(){
					  easyDialog.close();
					  };
			   var btnFs = function(){
				   window.location.reload();
			   };
				if(message[2]!=null){
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFs,
							 noFn:false
							}
					});	
				}else{
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFn,
							 noFn:false
							}
					});	
				}
			}

			/**
				提示 几秒后消失
				params : #id
				params : 提示信息
			**/
			function prompt(id,message){
				$("#"+id).html("");
				$("#"+id).html(message);
				setTimeout(function(){$("#"+id).html("");},2000);

			}

			/**
				提示 几秒后消失
				params : #id
				params : 提示信息
				params : 颜色
			**/
		function prompt_success(id,message,color){
			$("#"+id).html("");
			$("#"+id).css("color",color);
			$("#"+id).html(message);
			setTimeout(function(){$("#"+id).html("");},2000);
			$("#"+id).css("color","");

		}
	</script>
	<script type="text/javascript">
				/**
				 * 时间对象的格式化;
				 */
				Date.prototype.format = function(format) {
				    /*
				     * eg:format="YYYY-MM-dd hh:mm:ss";
				     */
				    var o = {
				        "M+" :this.getMonth() + 1, // month
				        "d+" :this.getDate(), // day
				        "h+" :this.getHours(), // hour
				        "m+" :this.getMinutes(), // minute
				        "s+" :this.getSeconds(), // second
				        "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
				        "S" :this.getMilliseconds()
				    // millisecond
				    }

				    if (/(y+)/.test(format)) {
				        format = format.replace(RegExp.$1, (this.getFullYear() + "")
				                .substr(4 - RegExp.$1.length));
				    }

				    for ( var k in o) {
				        if (new RegExp("(" + k + ")").test(format)) {
				            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
				                    : ("00" + o[k]).substr(("" + o[k]).length));
				        }
				    }
				    return format;
				}

				
				 
				 /**
				 **鼠标移入事件
				 **/
				function get_can_used_cny(){
						$.ajax({
							url:"<%=__ROOT_PATH__ %>/user/usercenter/get_user_acount_can_used_cny.html",
							type:"post",
							success:function(data){
							//var jsonObject = eval("("+data+")");
								if(data != null && data != "" && data != "[]"){
									var jsonObject = eval("("+data+")");
									var acount_balance = jsonObject.acount_balance;
									$("#acount_balance").html(acount_balance + "元");
								}
							}
						});
				}
						
</script>
<script language="javascript">
//参数-url表示要打开的网站，winname表示打开后的窗体名称
//参数windth表示打开窗体的宽度，height表示打开窗体的高度
function openwindow( url,winName,width,height) 
{
    xposition=0; yposition=0;
    if ((parseInt(navigator.appVersion) >= 4 ))
    {
    xposition = (screen.width - width) / 2;                //窗体居中的x坐标
    yposition = (screen.height - height) / 2;             //窗体居中的y坐标
    }
    theproperty= "width=" + width + ","                     //打开窗口的属性
    + "height=" + height + "," 
    + "location=0," 
    + "menubar=0,"
    + "resizable=1,"
    + "scrollbars=0,"
    + "status=0," 
    + "titlebar=0,"
    + "toolbar=0,"
    + "hotkeys=0,"
    + "screenx=" + xposition + ","                       //仅适用于Netscape
    + "screeny=" + yposition + ","                       //仅适用于Netscape
    + "left=" + xposition + ","                      //IE
    + "top=" + yposition;                                //IE 
    window.open( url,winName,theproperty );                //打开窗口
}
</script>
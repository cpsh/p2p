<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>



<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
 <link href="<%=__PUBLIC__ %>/style/main.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/jisuanji.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/one.css" rel="stylesheet" type="text/css" />
    <link href="<%=__PUBLIC__ %>/style/prod.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/tab.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/script/pngAlaph.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.cookies.2.2.0.min.js"></script>
 <script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
 <link type="text/css" rel="stylesheet" href="<%=__PUBLIC__%>/css/easydialog.css"  />
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">

</head>
<body>

<div class="head_top">
  <h1><a href="<%=__ROOT_PATH__%>/index/user_to_index.html"><img src="<%=__PUBLIC__ %>/images/logo.png" /></a></h1>
  <div class="right">登录</div>
</div>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="registration registration_5">
        <img src="<%=__PUBLIC__ %>/images/loginad.jpg" width="630" height="410" alt="" />
       <div class="login">         
         <div class="con">
              <input class="input_1" type="text" name="email" id="email" />   
              <input class="input_1 input_2" type="text" id="password2" name="password2"/>
              <input class="input_1 input_2" type="password" id="password" name="password" style=" display:none;"  /> 
           		<%--<div align="center" id="1" style="color:#F00;"></div>--%>
           </div>
           <div class="con_up" id="1"></div>
           <div class="con con_down">
              <input id="checked" checked="checked" type="checkbox" class="checked_1" />
              <span>记住用户名</span>
              <a style="color:#cc0000" href="<%=__ROOT_PATH__ %>/index/to_back_password_send_mail_jsp.html">忘记密码？</a>
           </div>           
           <div class="con con_down2"><a onclick="user_login();" id="login" style="cursor:pointer;TEXT-DECORATION:none">立即登录</a></div>
           <div class="con con_down3">没有账号？&nbsp;<a style="color:#cc0000" href="<%=__ROOT_PATH__ %>/index/registration.html">免费注册</a></div>
           <!-- <div class="con_down4"><span>您还可以使用合作账号登录&nbsp;</span><a class="a" href="#"></a><a class="b" href="#"></a></div> -->
       </div>
     </div>
  </div>
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
</body>
</html>
<script type="text/javascript">
$("input[name=password2]").focus(function () { 
    $("input[name=password2]").hide(); 
    $("input[name=password]").show().focus(); 


}); 
$("input[name=password]").blur(function () { 
    
    if ($(this).val() == ""||$(this).val()==null) {
         
        $("input[name=password2]").show(); 
        $("input[name=password]").hide(); 
    } 
}) 

$("#email").focus(function(){
	if($("#email").val()=="请输入手机号/邮箱"){
	$("#email").val("");
	}
});
$("#email").blur(function(){
	if($("#email").val()==null||$("#email").val()==""){
		$("#email").val("请输入手机号/邮箱");
	}
});
$(document).ready(function(){ 
	$("#email").val("请输入手机号/邮箱");
	$("#password2").val("请输入密码");
});
$(function(){ 
	document.onkeydown = function(e){ 
	var ev = document.all ? window.event : e; 
	if(ev.keyCode==13) { 
		user_login(); 
	} 
	} 
	}); 
/*<div align="center" id="1" style="display:none" class="con_up">邮箱不能为空</div>
<div align="center" id="2" style="display:none" class="con_up">密码不能为空</div>
<div align="center" id="3" style="display:none" class="con_up">账号或密码错误</div>
<div align="center" id="4" style="display:none" class="con_up">该账户已经禁止登陆</div>*/
//错误提示 
function prompt_success(id,message){
	//alert(111);
	$("#"+id).html("");
	$("#"+id).html(message);
	setTimeout(function(){$("#"+id).html("");},2000);
}  

	function user_login(){
		var email = $("#email").val();
		var password = $("#password").val();
		var checked =$("#checked").attr("checked");
		$.post(
		'<%=__ROOT_PATH__ %>/index/user_login.html',
		{
			"email":email,
			"password":password,
			"checked":checked
		},
		
		function(data){
			if(data == 1){
				prompt_success("1","邮箱不能为空");
			}else if(data == 2){
				prompt_success("1","密码不能为空");
			}else if(data == 3){
				prompt_success("1","账号或密码错误");
			}else if(data == 4){
				prompt_success("1","系统禁止登陆,请联系管理员");
			}else if(data == 5){
				window.location.href="<%=__ROOT_PATH__ %>/index/index.html";
			}
		},"html","application/x-www-form-urlencoded; charset=utf-8");
	}
	
		
	$(document).ready(function(){
		var username = getCookie("username");
		if (username==null||username=="") {
			return;
		}
		$("#email").val(username.replace("\"","").replace("\"",""));
		});
</script>
<script type="text/javascript">
		function getCookie(c_name){
			if (document.cookie.length>0)
			  {
			  c_start=document.cookie.indexOf(c_name + "=")
			  if (c_start!=-1)
			    { 
			    c_start=c_start + c_name.length+1 
			    c_end=document.cookie.indexOf(";",c_start)
			    if (c_end==-1) c_end=document.cookie.length
			    return unescape(document.cookie.substring(c_start,c_end))
			    } 
			  }
			 return ""
			}
			function setCookie(c_name,value,expiredays){
			var exdate=new Date()
			exdate.setDate(exdate.getDate()+expiredays)
			document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
			}
			
</script>
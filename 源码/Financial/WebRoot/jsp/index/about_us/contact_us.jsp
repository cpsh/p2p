<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>

	</head>

	<body>

		
<%@ include file="/jsp/index/index_top.jsp"%>
		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="about_left">
				<%-- 
					<ul>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=intro" >公司简介</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=team" >管理团队</a>
						</li>
						
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=consult">最新资讯</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=notice">网站公告</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement">网站协议</a>
						</li>
					
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=join_us">加入我们</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact" class="hover">联系我们</a>
						</li>
					</ul>
					--%>
					${list_str}
				</div>
				 <div class="about_right">
     				 <h2>联系我们</h2>
				      <h3>联富金融（www.lfoll.com）是中国领先的综合型互联网金融平台，提供贷款，理财，保险，投融资等金融服务。理财投资轻松安全，5重安全保障，100%本息保障计划。贷款高效便捷提供更优质的服务<br />
				         <s>广州总部</s><br />
				         &bull;&nbsp;地址：广东省广州市白云区石井大道6号银马国际广场C栋610<br />
				         &bull;&nbsp;邮编：510400<br />
				         &bull;&nbsp;地图图片<br /><img src="<%=__PUBLIC__%>/images/map.jpg" />
				         <s>客服电话</s><br />
				      	   如果您在使用联富金融(www.lfoll.com)的过程中有任何疑问请您与联富金融客服人员联系。<br />
				       	  客服电话：4006-888-923&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;客服QQ：4006888923<br />
				     	    帮助中心：<a href="http://www.lfoll.com/help/help_index.html">http://www.lfoll.com/help/help_index.html</a><br />
				         <s>媒体采访</s><br />
				         	如果有媒体采访需求，请将您的媒体名称、采访提纲、联系方式发至：<br />
				         <a href="#">pr@lfoll.com</a>,我们会尽快与您联系。<br />
				         <s>商务合作</s><br />
				         	如果贵公司希望与我们建立商务合作关系，形成优势互补，请将合作意向进行简要描述并发送邮件至：<br />
				         <a href="#">bd@lfoll.com</a>‍，我们会尽快与您联系。<br />
				      	  	 备注：联富金融目前暂时没有线下加盟/线下开店等商务合作模式，谢谢关注。<br />
				         <s>信息同步</s><br />
				        	 如果您还希望了解我们更多信息，请关注我们的新浪微博和微信<br />
				        	 &bull;&nbsp;微信公众平台：lfollcom（扫描下方二维码关注联富金融微信）<br />         
				        	&bull;&nbsp;新浪微博：<a href="http://weibo.com/lfoll">http://weibo.com/lfoll</a>‍<br /> 
				         <img src="<%=__PUBLIC__%>/images/qr_code.jpg" />
				      </h3>
   			 </div>
  </div>
  
</div>
		<%@ include file="/jsp/index/index_foot.jsp" %>


	</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	 	<title>${applicationScope.title}</title>
		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">   

		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script language="JavaScript" type="text/javascript" src="<%=__PUBLIC__%>/script/tab.js"></script>
  </head>
  
  <body>
  	<%@ include file="/jsp/index/index_top.jsp"%>
	<span id="ny_pic"></span>
	<div class="about">
	  <div class="about_con">
	    <div class="about_left">
	      ${list_str}
	    </div>
	    <div class="about_right">
	      <h2>友情链接</h2>
	      <div class="links">
	         <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
	         <%--
	            <tr>
	               <td class="td_1">文字链接</td>
	               <td>&nbsp;</td>
	               <td>&nbsp;</td>
	               <td>&nbsp;</td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">ecshop官网</a></td>
	               <td width="25%"><a href="#">淘宝商家服务</a></td>
	               <td width="25%"><a href="#">淘宝快递单打印软件</a></td>
	               <td width="25%"><a href="#">ShopEx电商人才中心</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">ShopEx模版</a></td>
	               <td width="25%"><a href="#">女人志</a></td>
	               <td width="25%"><a href="#">企业邮局</a></td>
	               <td width="25%"><a href="#">虚拟主机评测</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">华夏名网</a></td>
	               <td width="25%"><a href="#">中国互联</a></td>
	               <td width="25%"><a href="#">网罗天下</a></td>
	               <td width="25%"><a href="#">天极软件</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">IT商网</a></td>
	               <td width="25%"><a href="#">网店网</a></td>
	               <td width="25%"><a href="#">互易中国</a></td>
	               <td width="25%"><a href="#">A5站长网</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">孕婴童商务网</a></td>
	               <td width="25%"><a href="#">箕斗网络</a></td>
	               <td width="25%"><a href="#">电子商务之家</a></td>
	               <td width="25%"><a href="#">在线客服系统</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">中国数据</a></td>
	               <td width="25%"><a href="#">天下网商</a></td>
	               <td width="25%"><a href="#">商派电商ERP</a></td>
	               <td width="25%"><a href="#">商派</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">中国诚商网</a></td>
	               <td width="25%"><a href="#">商品雷达</a></td>
	               <td width="25%"><a href="#">商品助理</a></td>
	               <td width="25%"><a href="#">新商界</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">服饰流行前线</a></td>
	               <td width="25%"><a href="#">好贷网</a></td>
	               <td width="25%"><a href="#">电子商务社区</a></td>
	               <td width="25%"><a href="#">互联网数据</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">中国电子商务研究中心</a></td>
	               <td width="25%"><a href="#">化妆品商城</a></td>
	               <td width="25%"><a href="#">京东商城</a></td>
	               <td width="25%"><a href="#">站长之家</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">母婴用品</a></td>
	               <td width="25%"><a href="#">苏宁易购</a></td>
	               <td width="25%"><a href="#">国美在线</a></td>
	               <td width="25%"><a href="#">网易科技</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">我买网上购物</a></td>
	               <td width="25%"><a href="#">海尔</a></td>
	               <td width="25%"><a href="#">派代网</a></td>
	               <td width="25%"><a href="#">思路网</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">韩都衣舍</a></td>
	               <td width="25%"><a href="#">格瓦拉</a></td>
	               <td width="25%"><a href="#">专业市场网</a></td>
	               <td width="25%"><a href="#">名鞋库官网</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">蜘蛛网</a></td>
	               <td width="25%"><a href="#">hc360慧聪网</a></td>
	               <td width="25%"><a href="#">服装批发</a></td>
	               <td width="25%"><a href="#">名品导购网</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">优购网上鞋城</a></td>
	               <td width="25%"><a href="#">服装批发</a></td>
	               <td width="25%"><a href="#">一比多</a></td>
	               <td width="25%"><a href="#">玩具视频</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">人人贷</a></td>
	               <td width="25%"><a href="#">义乌购</a></td>
	               <td width="25%"><a href="#">A5源码</a></td>
	               <td width="25%"><a href="#">步步高商城</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">中国广告买卖网</a></td>
	               <td width="25%"><a href="#">懒汉互联</a></td>
	               <td width="25%"><a href="#">蓝色理想</a></td>
	               <td width="25%"><a href="#">飞牛网</a></td>
	            </tr>
	            <tr>
	               <td width="25%"><a href="#">华为云服务</a></td>
	               <td width="25%">&nbsp;</td>
	               <td width="25%">&nbsp;</td>
	               <td width="25%">&nbsp;</td>
	            </tr>
	         --%>
	         	${words_link_str}
	         </table>
	      </div>  
	      <div class="links">
	         <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
	         <%--
	            <tr>
	               <td class="td_1" width="20%">图片链接</td>
	               <td width="20%">&nbsp;</td>
	               <td width="20%">&nbsp;</td>
	               <td width="20%">&nbsp;</td>
	               <td width="20%">&nbsp;</td>
	            </tr>
	            <tr>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_1.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_2.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_3.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_4.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_5.gif" /></a></td>
	            </tr>
	            <tr>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_6.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_7.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_8.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_9.jpg" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_10.gif" /></a></td>
	            </tr>
	            <tr>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_11.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_12.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_13.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_14.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_15.gif" /></a></td>
	            </tr>
	            <tr>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_16.jpg" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_17.jpg" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_18.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_19.gif" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_20.jpg" /></a></td>
	            </tr>
	            <tr>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_21.jpg" /></a></td>
	               <td width="20%"><a href="#"><img src="<%=__PUBLIC__ %>/images/Links_22.jpg" /></a></td>
	               <td width="20%">&nbsp;</td>
	               <td width="20%">&nbsp;</td>
	               <td width="20%">&nbsp;</td>
	            </tr>
	        --%>
	         ${images_link_str} 
	         </table>         
	      </div>
	      <div class="links">
	         <dl>
	            <dt>友情链接申请方式</dt>
	            <dd>互惠友情链接，将双方优势直接互补宣传，联富金融期待与您共赢！</dd>
	            <dd>诚邀电商类网站合作，有意合作者请与我们联系QQ：4006888923或Email：bd@lfoll.com‍</dd>
	         </dl> 
	      </div>    
	         
	    </div>
	  </div>
	  
	</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>

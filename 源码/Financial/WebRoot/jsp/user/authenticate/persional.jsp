<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">

<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="loan_bb">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
               <c:choose>
			   	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==3}">&nbsp;净值贷&nbsp;</c:when>
			   </c:choose>
              <span>></span>&nbsp;填写借款申请
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s></s>
              <span>3</span>
              <s></s>
              <span>4</span>
              <s></s>
              <span>5</span>
           </div>
           <div class="down">
              <span style="color:#ed5050;">填写借款申请</span>
              <span style="margin-left:55px; color:#ed5050;">填写借款信息</span>
              <span style="margin-left:60px;">审核</span>
              <span style="margin-left:60px;">筹集借款</span>
              <span style="margin-left:55px;">获得借款</span>
           </div>
        </div>
        
        <div class="loan_bb_con">
<%@ include file="/jsp/user/authenticate/authenticate_left.jsp"%>
          <div class="right">
            <div class="right_top">个人信息</div>
            <div class="right_up">温馨提示：我们将在您的必要认证资料上传齐全后为您提交审核。</div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;最高学历：</label>
                <dl class="select">
                    <dt id="education" >${user_authenticate_persional_info.m.highest_education }</dt>
                    <dd>
                       <ul>
                          <li><a>高中或以下</a></li>
                          <li><a>大专</a></li>
                          <li><a>本科</a></li>
                          <li><a>研究生或以上</a></li>
                       </ul>
                    </dd>
                 </dl>
                 <p id="0" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请选择学历</p>
             </div>
               <div class="con">
                <label class="label_1">&nbsp;入学年份：</label>
                <dl class="select">
                    <dt id="year_of_admission" >${user_authenticate_persional_info.m.year_of_admission }</dt>
                    <dd>
                       <ul>
                       <c:forEach begin="1960" end="2014" step="1" var="i">
									<li><a>${i}</a></li>	
						</c:forEach>
                       </ul>
                    </dd>
                 </dl>
                 <p id="year_of_admission——这里这里" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请选择入学年份</p>
             </div>
                <div  style="margin-top:20px;" class="con con4 con5">
                <label class="label_1">&nbsp;毕业学校：</label>
                <input id="school" class="input_1"  value="${user_authenticate_persional_info.m.school}" type="text" >
                 <p id="school2" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;格式有误！</p>
             </div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;籍贯：</label>
                <dl style="margin-right:20px;" class="select">
                    <dt id="province1" onclick="remove_city('city1');" style="width:109px;">${user_authenticate_persional_info.m.native_place_province }</dt>
                    <dd style="width:129px;">
                       <ul>
                      	<c:forEach items="${persional_province_list}" var="i">
                          <li><a>${i}</a></li>
						</c:forEach>
                       </ul>
                    </dd>
                 </dl>
                 <dl id="city1" onclick="return_city('province1','city1')" class="select">
                    <dt style="width:109px;">${user_authenticate_persional_info.m.native_place_city }</dt>
                    <dd style="width:129px;">
                       <ul>
                          <li><a></a></li>
                       </ul>
                    </dd>
                 </dl>
                 <p id="1" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请选择籍贯</p>
             </div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;户口所在地：</label>
                <dl style="margin-right:20px;" class="select">
                    <dt id="province2" onclick="remove_city('city2');"  style="width:109px;">${user_authenticate_persional_info.m.domicile_place_province }
                    </dt>
                    <dd style="width:129px;">
                       <ul>
                      	<c:forEach items="${persional_province_list}" var="i">
                          <li><a>${i}</a></li>
						</c:forEach>
                       </ul>
                    </dd>
                 </dl>
                 <dl id="city2" onclick="return_city('province2','city2')" class="select">
                    <dt style="width:109px;">${user_authenticate_persional_info.m.domicile_place_city }</dt>
                    <dd style="width:129px;">
                       <ul>
                          <li><a></a></li>
                       </ul>
                    </dd>
                 </dl>
                 <p id="3" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请选择户口所在地</p>
             </div>
             <div style="margin-top:20px;" class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;居住地址：</label>
                <input id="address" class="input_1" onfocus="hide(5,51)" onblur="check_addr('address',51)" value="${user_authenticate_persional_info.m.address}" type="text">
                <p id="5" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请填写居住地址</p>
                <p id="51" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;地址有误</p>
             </div>
             <div class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;居住地邮编：</label>
                <input id="zip_code" class="input_1" onfocus="hide(6,61)" onblur="check_num('zip_code',61)" value="${user_authenticate_persional_info.m.zip_code}" type="text" >
                <p id="6" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请填写居住邮编</p>
                <p id="61" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;邮编有误</p>
             </div>
             <div class="con con4 con5">
                <label class="label_1">&nbsp;居住电话：</label>
                <input id="landline" class="input_1" onfocus="hide(7,71)" onblur="check_long_num('landline',71)" value="${user_authenticate_persional_info.m.landline }" type="text">
            	<p id="71" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;电话号码有误</p>
             </div>
             <div class="con con6">
                <input type="hidden" id="apply_order_id" value="${apply_order_id}" />
                <p class="label_p5"><a onclick="save()">保存并继续</a></p>
             </div>
          </div>
        </div>
     </div>
  </div>  
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__%>/js/select.js"></script>
<script type="text/javascript">
function save(){
	var highest_education = $("#education").html();
	var native_place_province = $("#province1").html();
	var native_place_city = $("#city1 dt").html();
	var domicile_place_province = $("#province2").html();
	var domicile_place_city = $("#city2 dt").html();
	var address = $("#address").val();
	var zip_code = $("#zip_code").val();
	var landline = $("#landline").val();
	var apply_order_id=$("#apply_order_id").val();
	var year_of_admission = $("#year_of_admission").html();
	var school = $("#school").val()
$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/authentication/persional_info/save_persional_info.html",
		async:false,
		cache:false,
		data:{"highest_education":highest_education,"native_place_province":native_place_province,"native_place_city":native_place_city,
		"domicile_place_province":domicile_place_province,"domicile_place_city":domicile_place_city,"address":address,"zip_code":zip_code,
		"landline":landline,"apply_order_id":apply_order_id,"year_of_admission":year_of_admission,"school":school},
		success:function(data){
			if(data==2){$("#1").show();}
			else if(data==4){$("#3").show();}
			else if(data==5){$("#5").show();}
			else if(data=="ok"){
			window.location.href="<%=__ROOT_PATH__%>/user/authentication/family_info/family_index.html";}
			else if(data=="no"){
			alert("提交失败");
			}else {$("#"+data).show();}
		}
	},"html");
}


function return_city(_province,city){
		var province = $("#"+_province).html();
		$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/authentication/return_city/return_city.html",
		async:false,
		cache:false,
		data:{"province":province},
		success:function(data){
			if(data!=null){
				var city_list=eval(data);
	            $("#"+city+" li").remove();
				for(var city_index in city_list){
			    var citys=city_list[city_index];
			    $("#"+city+" ul").append("<li><a>"+citys+"</a></li>");
		        }
			}
		}
	},"html");
		$("#"+city).each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		//var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		//dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
}
function hide(id,id2){
	$("#"+id).hide();
	$("#"+id2).hide();
}
function remove_city(city){
	$("#"+city+" dt").html("");
}


</script>

</body>
</html>


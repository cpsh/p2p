<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<html xmlns="http://www.w3.org/1999/xhtml">

	<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
		<%@ include file="/jsp/index/index_top.jsp"%>

		<span id="ny_pic"></span>
		<div id="ok" style="display: none;" class="fudong">
			上传成功！
		</div>
		<div id="no" style="display: none;" class="fudong">
			上传失败，格式错误！
		</div>
		<div id="er" style="display: none;" class="fudong">
			上传失败，请检查网络！
		</div>
		<div id="over" style="display: none;" class="fudong">
			上传失败，超过上传限制次数！
		</div>
		<div id="out" style="display: none;" class="fudong">
			上传失败，文件过大，请上传2M以下图片！
		</div>
		<div class="about">
			<div class="about_con">
				<div class="loan_aa">
					<div class="loan_aa_top">
						<div class="up">
							<a href="#">我要借款</a>&nbsp;
							<span>></span>&nbsp;工薪贷&nbsp;
							<span>></span>&nbsp;填写借款申请
						</div>
						<div class="center">
							<span style="background: #ed5050; color: #fff;">1</span>
							<s style="background: #ed5050;"></s>
							<span style="background: #ed5050; color: #fff;">2</span>
							<s style="background: #ed5050;"></s>
							<span style="background: #ed5050; color: #fff;">3</span>
							<s></s>
							<span>4</span>
							<s></s>
							<span>5</span>
						</div>
						<div class="down">
							<span style="color: #ed5050;">填写借款申请</span>
							<span style="margin-left: 55px; color: #ed5050;">填写借款信息</span>
							<span style="margin-left: 60px; color: #ed5050;">等待审核</span>
							<span style="margin-left: 60px;">筹集借款</span>
							<span style="margin-left: 55px;">获得借款</span>
						</div>
					</div>
					<div id="wait1" class="loan_cc">
						<s>&bull;</s>您的借款申请我们将尽快为审核，请耐心等待。
					</div>
					<div id="wait2" class="loan_cc" style="display:none; text-align: center;">
					您还有   <span id="hour" style=" text-align: center; font-size: 30px; color: #fa6b01"> </span>时<span id="min" style=" text-align: center; font-size: 30px; color: #fa6b01"> </span>分<span id="second" style=" text-align: center; font-size: 30px; color: #fa6b01"> </span> 秒可以补充上传资料，时间截至 后您将无法继续补充。
					</div>
					<div class="loan_cc_con">
						<div class="up">
							<span>借款申请</span><a
								href="<%=__ROOT_PATH__ %>/user/authenticate/authenticate_info_preview/preview_index.html"
								target="_blank">借款申请信息预览</a>
						</div>
						<div class="loan_cc_up">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="35%" class="td_a">
										借款金额：
										<span>￥${borrower_bulk_standard_apply_order.m.borrow_all_money}</span>
									</td>
									<td width="30%" class="td_a">
										年化利率：
										<span>${borrower_bulk_standard_apply_order.m.annulized_rate_int}%</span>
									</td>
									<td width="35%" class="td_a">
										借款期限：
										<c:if test="${borrower_bulk_standard_apply_order.m.borrow_duration!=0}">
											<span>${borrower_bulk_standard_apply_order.m.borrow_duration}个月</span>
										</c:if>
										<c:if test="${borrower_bulk_standard_apply_order.m.borrow_duration==0}">
											<span>${borrower_bulk_standard_apply_order.m.borrow_duration_day}天</span>
										</c:if>
									</td>
								</tr>
								<tr>
									<td class="td_c" colspan="2">
										<p>
											温馨提示：您的借款申请将于3个工作日内为您审核完成。
										</p>
									</td>
								</tr>
							</table>
						</div>
					</div>

					<div class="loan_cc_con2">
           <div class="up">费用说明</div>
           <div class="up2">期初服务费</div>
             <div id="net" style="display:none" class="down">
              <p>服务费率<s>1%</s></p>
              <p>服务费<s>￥${borrow_all_money_1}</s>（由联富金融平台收取）</p>
           </div> 
           <br/>
           <div id="common" style="display:none" class="center">
 		<form  class="form_a">
              <table  width="580" border="0" cellspacing="0" cellpadding="0">
                 <tr>
               		<td width="90" height="39" style="border-bottom:1px #e0e0e0 solid">信用等级</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s AA snow">A+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s A snow">A</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s BB snow">B+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s B snow">B</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s CC snow">C+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s C snow">C</span></td>
                 </tr>
                 <tr>
                    <td height="46" style="border-bottom:1px #e0e0e0 solid">服务费率</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0%</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">1%</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">2%</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">3%</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">4%</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">5%</td>
                 </tr>
                 <tr>
                    <td height="37" style="border-bottom:1px #e0e0e0 solid">服务费</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">0</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">${borrow_all_money_1}</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">${borrow_all_money_2}</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">${borrow_all_money_3}</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">${borrow_all_money_4}</td>
                    <td width="70" height="40" align="center" style="padding-right:2px;border-bottom:1px #e0e0e0 solid">${borrow_all_money_5}</td>
                 </tr>
              </table>
           </form>
           </div>  
           <div class="up2" style="border-top:1px #eaeaea dashed;">每月还款额</div>  
           <div class="down">
              <p>本金及利息<s>￥${monthly_principal_interest}</s>（支付给理财人）</p>
              <p>借款管理费<s>￥${manage_fee}</s>（由联富金融平台收取）</p>
           </div>                  
       </div>


					<div id="upload" style="display: none;" class="loan_cc_con2 loan_cc_con3">
						<div class="up">
							认证状态
						</div>
						<div class="up2">
							必要认证资料
						</div>
						<div class="con">
							<s>身份认证</s>
							<span id="0" class="span_1 a"></span>
							<a id="0zi" onclick="openme2('0div1','0div2');">上传资料</a>
							<div id="0div1" class="loginDiv1"></div>
							<div id="0div2" class="loginDiv2"
								style="z-index: 10005; display: none;">
								<div class="area">
									<input type="button" class="guanbi"
										onClick="closeme2('0div1','0div2');" value="ｘ" />
									<div class="area_up">
										<span>身份认证</span>
										<br />
										您上传的身份证照片需和您绑定的身份证一致，否则将无法通过认证。
									</div>
									<p>
										认证说明:
										<br/>
										（1）本人身份证原件的正、反两面照片。
										<a href="<%= __PUBLIC__ %>/images/shengfenzheng.jpg" target="_blank" >查看示例</a>
										<br />
										（2）本人手持身份证正面头部照，（确保身份证上的信息没有被遮挡，避免证件与头部重叠）。<a href="<%= __PUBLIC__ %>/images/id_example_girl.jpg" target="_blank">查看示例</a>
										<br />
										认证有效期：
										<span>永久</span>
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
									</p>
									<input class="button_1" type="button" onclick="doUpload(0);"
										value="上传资料" />
								</div>
							</div>
						</div>
						<span id="hide_it">
						<div class="con">
							<s>工作认证</s>
							<span id="1" class="span_1 b"></span>
							<a id="1zi" onclick="openme2('1div1','1div2');">上传资料</a>
							<div id="1div1" class="loginDiv1"></div>
							<div id="1div2" class="loginDiv2"
								style="z-index: 10005; display: none;">
								<div class="area">
									<input type="button" class="guanbi"
										onClick="closeme2('1div1','1div2')" value="ｘ" />
									<div class="area_up">
										<span>工作认证</span>
										<br />
										您的工作状况是联富金融评估您信用状况的主要依据之一。
									</div>
									<p style="display: none" id="wi1">
										认证说明:
										<br />
										请上传以下任意一项资料：
										<br />
										（1）加盖单位公章（或劳动合同专用章）的劳动合同。
										<a href="<%= __PUBLIC__ %>/images/laodonghetong.jpg" target="_blank">查看示例</a>
										<br />
										（2）最近1个月内开具的加盖单位公章（或人力章、财务章）的机打（手写无效）在职证明。
										<a href="<%= __PUBLIC__ %>/images/zaizhizhengming.jpg" target="_blank">查看示例</a>
										<br />
										（3）带有姓名、照片、工作单位名称的工作证。
										<a href="<%= __PUBLIC__ %>/images/gongzuozheng.jpg" target="_blank">查看示例</a>
										<br />
										认证条件：
										<span>本人需在现单位工作满3个月。</span>
										<br />
										认证有效期：
										<span>6个月</span>
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
									</p>
									<p style="display: none" id="wi2">
										认证说明:
										<br />
										请上传以下任意两项资料：
										<br />
										（1）注册满1年的营业执照正、副本。。
										<a href="<%= __PUBLIC__ %>/images/yingyezhizhao.jpg" target="_blank">查看示例</a>
										<br />
										（2）经营场地租赁合同＋90天内的租金发票或水电单据。。
										<a href="<%= __PUBLIC__ %>/images/changdizulinhetong.jpg" target="_blank">查看示例</a>

										<br />
										认证条件：
										<span>本人名下的企业经营时间需满1年。</span>
										<br />
										认证有效期：
										<span>6个月</span>
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统

										黑名单，永久取消您在联富金融的借款资格。
									</p>

									<input class="button_1" type="button" onclick="doUpload(1);"
										value="上传资料" />
								</div>
							</div>
						</div>
						<div class="con">
							<s>信用认证</s>
							<span id="2" class="span_1 c"></span>
							<a id="2zi" onclick="openme2('2div1','2div2');">上传资料</a>
							<div id="2div1" class="loginDiv1"></div>
							<div id="2div2" class="loginDiv2"
								style="z-index: 10005; display: none;">
								<div class="area">
									<input type="button" class="guanbi"
										onClick="closeme2('2div1','2div2')" value="ｘ" />
									<div class="area_up">
										<span>个人信用报告认证</span>
										<br />
										个人信用报告是由中国人民银行出具，全面记录个人信用活动，反映个人信用基本状况的文件。本报告是联富金融了解您信用状况的一个重要参考资料。您信用报告内体现的信用记录和信用卡额度等数据，将在您发布借款时经联富金融工作人员整理，在充分保护您隐私的前提下披露给联富金融理财人，作为理财人投标的依据。
									</div>
									<p>
										认证说明:
										<br />
										（1）个人信用报告需15日内开具。
										<a href="<%= __PUBLIC__ %>/images/xinyongbaogao.jpg" target="_blank">查看示例</a>
										<br />
										<a href="http://www.pbccrc.org.cn/zxzx/lxfs/lxfs.shtml" target="_blank">全国各地征信中心联系方式查询</a>
										<br />
										<a href="https://ipcrs.pbccrc.org.cn/" target="_blank">个人信用信息服务平台</a>
										<br />
										认证条件：
										<span>信用记录良好</span>
										<br />
										认证有效期：
										<span>6个月</span>
										<br />
										如何办理个人信用报告：可去当地人民银行打印，部分地区可登陆个人信用信息服务平台。
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
									</p>
									<input class="button_1" type="button" onclick="doUpload(2);"
										value="上传资料" />
								</div>
							</div>
						</div>
						<div class="con">
							<s>收入认证</s>
							<span id="3" class="span_1 d"></span>
							<a id="3zi" onclick="openme2('3div1','3div2');">上传资料</a>
							<div id="3div1" class="loginDiv1"></div>
							<div id="3div2" class="loginDiv2"
								style="z-index: 10005; display: none;">
								<div class="area">
									<input type="button" class="guanbi"
										onClick="closeme2('3div1','3div2')" value="ｘ" />
									<div class="area_up">
										<span>收入认证</span>
										<br />
										您的银行流水单是联富金融评估您收入状况的主要依据之一。
									</div>
									<p id="in1" style="display: none">
										认证说明:
										<br />
										请提交下面
										<span>任意一项</span>资料：
										<br />
										（1）可体现工资项的最近3个月的工资卡银行流水单。
										<a href="<%= __PUBLIC__ %>/images/yinhangliushui.jpg" target="_blank">查看示例</a>
										<br />
										（2）可体现工资项的最近3个月的网银电脑截屏。
										<a href="<%= __PUBLIC__ %>/images/wangyinjietu.jpg" target="_blank">查看示例</a>
										<br />
										认证条件：
										<span>本人名下近3个月的月收入均在2000以上。</span>
										<br />
										认证有效期：
										<span>6个月</span>
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
									</p>
									<p id="in2" style="display: none">
										认证说明:
										<br />
										请提交下面
										<span>任意一项</span>资料：
										<br />
										（1）可体现经营情况的最近6个月的个人银行卡流水单，或网银电脑截屏。
										<a href="<%= __PUBLIC__ %>/images/yinhangliushui.jpg" target="_blank">查看示例</a>
										<br />
										（2）可体现经营情况的最近6个月企业银行卡流水单，或网银电脑截屏。
										<a href="<%= __PUBLIC__ %>/images/wangyinjietu.jpg" target="_blank">查看示例</a>
										<br />
										认证条件：
										<span>本人提交的流水可反应真实有效的经营情况。</span>
										<br />
										认证有效期：
										<span>6个月</span>
										<br />
										上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
										<br />
										<span>警告：</span>
										联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统

										黑名单，永久取消您在联富金融的借款资格。
									</p>
									<input class="button_1" type="button" onclick="doUpload(3);"
										value="上传资料" />
								</div>
							</div>
						</div>					
					<div class="up2">
						可选上传资料
					</div>
					<div class="con">
						<s>房产认证</s>
						<span id="4" class="span_1 e"></span>
						<a id="4zi" onclick="openme2('4div1','4div2');">上传资料</a>
						<div id="4div1" class="loginDiv1"></div>
						<div id="4div2" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="area">
								<input type="button" class="guanbi"
									onClick="closeme2('4div1','4div2')" value="ｘ" />
								<div class="area_up">
									<span>房产认证</span>
									<br />
									房产认证是证明借入者资产及还款能力的重要凭证之一。
								</div>
								<p>
									认证说明:
									<br />
									1、 请上传以下任意一项或多项资料。
									<br />
									（1）房屋产权证明。
									<a href="<%= __PUBLIC__ %>/images/fangchanzheng.jpg" target="_blank">查看示例</a>
									<br />
									（2）购房合同+近3个月的还贷流水。
									<a href="<%= __PUBLIC__ %>/images/goufanghetong.jpg" target="_blank">查看示例</a>
									<br />
									（3）购房发票+近3个月的还贷流水。
									<a href="<%= __PUBLIC__ %>/images/goufangfapiao.jpg" target="_blank">查看示例</a>
									<br />
									（4）按揭合同+近3个月的还贷流水。
									<br />
									认证条件：
									<span>必须是商品房，且房产是本人名下所有或共有的。</span>
									<br />
									认证有效期：
									<span>永久</span>
									<br />
									上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
									<br />
									<span>警告：</span>
									联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
								</p>
								<input class="button_1" type="button" onclick="doUpload(4);"
									value="上传资料" />
							</div>
						</div>
					</div>
					<div class="con">
						<s>购车认证</s>
						<span id="5" class="span_1 f"></span>
						<a id="5zi" onclick="openme2('5div1','5div2');">上传资料</a>
						<div id="5div1" class="loginDiv1"></div>
						<div id="5div2" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="area">
								<input type="button" class="guanbi"
									onClick="closeme2('5div1','5div2')" value="ｘ" />
								<div class="area_up">
									<span>购车认证</span>
									<br />
									购车认证是证明借入者资产及还款能力的重要凭证之一。
								</div>
								<p>
									认证说明:
									<br />
									请上传以下任意一项资料：
									<br />
									（1）车辆行驶证的原件照片。
									<a href="<%= __PUBLIC__ %>/images/xingshizheng.jpg" target="_blank">查看示例</a>
									<br />
									（2）本人和车辆的合影（照片需露出车牌号码）。
									<a href="<%= __PUBLIC__ %>/images/rencheheying.jpg" target="_blank">查看示例</a>
									<br />
									认证有效期：
									<span>永久</span>
									<br />
									上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
									<br />
									<span>警告：</span>
									联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
								</p>
								<input class="button_1" type="button" onclick="doUpload(5);"
									value="上传资料" />
							</div>
						</div>
					</div>
					<div class="con">
						<s>结婚认证</s>
						<span id="6" class="span_1 g"></span>
						<a id="6zi" onclick="openme2('6div1','6div2');">上传资料</a>
						<div id="6div1" class="loginDiv1"></div>
						<div id="6div2" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="area">
								<input type="button" class="guanbi"
									onClick="closeme2('6div1','6div2')" value="ｘ" />
								<div class="area_up">
									<span>结婚认证</span>
									<br />
									借入者婚姻状况的稳定性，是联富金融考核借款人信用的评估因素之一。
								</div>
								<p>
									认证说明:
									<br />
									1、请您上传以下资料：
									<br />
									（1）结婚证书的原件照片。
									<a href="<%= __PUBLIC__ %>/images/jiehunzheng.jpg" target="_blank">查看示例</a>
									<br />
									（2）配偶身份证原件的正、反两面照片。
									<br />
									（3）本人和配偶的近照合影一张。
									<br />
									认证有效期：
									<span>永久</span>
									<br />
									上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
									<br />
									<span>警告：</span>
									联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
								</p>
								<input class="button_1" type="button" onclick="doUpload(6);"
									value="上传资料" />
							</div>
						</div>
					</div>
					<div class="con">
						<s>学历认证</s>
						<span id="7" class="span_1 h"></span>
						<a id="7zi" onclick="openme2('7div1','7div2');">上传资料</a>
						<div id="7div1" class="loginDiv1"></div>
						<div id="7div2" class="loginDiv2"
							style="z-index: 10005; display: none;">
							<div class="area">
								<input type="button" class="guanbi"
									onClick="closeme2('7div1','7div2')" value="ｘ" />
								<div class="area_up">
									<span>学历认证</span>
									<br />
									借出者在选择借款申请投标时，借入者的学历也是一个重要的参考因素。为了让借出者更好、更快地相信您的学历是真实的，强烈建议您对学历进行在线验证。
								</div>
								<p>
									认证条件：
									<span>大专或以上学历（普通全日制）证书或相关文件原件照片</span>
									<br />
									认证有效期：
									<span>永久</span>
									<br />
									上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
									<br />
									<span>警告：</span>
									联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
								</p>
								<input class="button_1" type="button" onclick="doUpload(7);"
									value="上传资料" />
							</div>
						</div>
					</div>
				
				<div class="con">
					<s>职称认证</s>
					<span id="8" class="span_1 i"></span>
					<a id="8zi" onclick="openme2('8div1','8div2');">上传资料</a>
					<div id="8div1" class="loginDiv1"></div>
					<div id="8div2" class="loginDiv2"
						style="z-index: 10005; display: none;">
						<div class="area">
							<input type="button" class="guanbi"
								onClick="closeme2('8div1','8div2')" value="ｘ" />
							<div class="area_up">
								<span>技术职称认证</span>
								<br />
								技术职称是经专家评审、反映一个人专业技术水平并作为聘任专业技术职务依据的一种资格，不与工资挂钩，是联富金融考核借款人信用的评估因素之一。
							</div>
							<p>
								认证说明:
								<br />
								（1）技术职称证书的原件照片。
								<br />
								认证条件：
								<span>国家承认的二级及以上等级证书。例如律师证、会计证、工程师证等。</span>
								<br />
								认证有效期：
								<span>永久</span>
								<br />
								上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
								<br />
								<span>警告：</span>
								联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
							</p>
							<input class="button_1" type="button" onclick="doUpload(8);"
								value="上传资料" />
						</div>
					</div>
				</div>
				<div class="con">
					<s>居住认证</s>
					<span id="9" class="span_1 j"></span>
					<a id="9zi" onclick="openme2('9div1','9div2')">上传资料</a>
					<div id="9div1" class="loginDiv1"></div>
					<div id="9div2" class="loginDiv2"
						style="z-index: 10005; display: none;">
						<div class="area">
							<input type="button" class="guanbi"
								onClick="closeme2('9div1','9div2')" value="ｘ" />
							<div class="area_up">
								<span>居住地证明</span>
								<br />
								居住地的稳定性，是联富金融考核借款人的主要评估因素之一。
							</div>
							<p>
								认证说明:
								<br />
								请上传以下任意一项资料：
								<br />
								（1）用本人姓名登记的水、电、气最近3个月缴费单。
								<br />
								（2）用本人姓名登记的固定电话最近3个月缴费单。
								<br />
								（3）本人的信用卡最近2个月的月结单。
								<br />
								（4）本人的自有房产证明。
								<br />
								（5）本人父母的房产证明，及证明本人和父母关系的证明材料。
								<br />
								认证有效期：
								<span>6个月</span>
								<br />
								上传资料：请确认您上传的资料是清晰的、完整的、未经修改的数码照片或彩色扫描照片。每张图片大小不大于2M。
								<br />
								<span>警告：</span>
								联富金融是一个注重诚信的网络平台。如果我们发现您上传的资料系伪造或有人工修改痕迹，联富金融会将你加入系统黑名单，永久取消您在联富金融的借款资格。
							</p>
							<input class="button_1" type="button" onclick="doUpload(9);"
								value="上传资料" />
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="apply_order_id" value="${borrower_bulk_standard_apply_order.m.id}" />
		</div>
		</div>
		</div>
		<%@ include file="/jsp/index/index_foot.jsp"%>
		<script src="<%=__PUBLIC__%>/js/the_function.js"></script>
		<script type="text/javascript">
function doUpload(type){
	var apply_order_id = $("#apply_order_id").val();
		$.upload({
			// 上传地址				
			url: '<%=__ROOT_PATH__%>/user/authentication/upload_info/upload_info.html', 
			// 文件域名字
			fileName: 'filedata', 
			// 其他表单数据
			params: {name:type,apply_order_id:apply_order_id},
			// 上传完成后, 返回json, text
			dataType: 'json',
			// 上传之前回调,return true表示可继续上传
			onSend: function() {
					return true;
			},
			// 上传之后回调
			onComplate: function(data) {
				if(data == no){
				 	$("#no").show();
					setTimeout('hide()',2000);
				}else if(data == er){
					$("#er").show();
					setTimeout('hide()',2000);
				}else if(data == over){
					$("#over").show();
					setTimeout('hide()',2000);
				}else if(data == out){
					$("#out").show();
					setTimeout('hide()',2000);
				}else{
				window.location.reload();
				//	closeme2(data+"div1",data+"div2");
				//	$("#ok").show();
				//	$("#"+data+"zi").html("补充上传");
				//	$("#"+data).append("<img src='<%=__PUBLIC__%>/images/loan_b_11.png' />");
				//	setTimeout('hide()',2000);
				}
			}
		});
	}
	
function hide() {
	$("#ok").hide();
	$("#no").hide();	
	$("#er").hide();
	$("#over").hide();
	$("#out").hide();
}
var now_ux = ${now};
function change_time(){
	var time_ux = ${commit_time_next_long};
	
	var second = 1000;
	var min = second * 60;
	var hour = min * 60;
	
	now_ux = now_ux + second;
	
	var dif = hour * 3 - (now_ux - time_ux);
	
	if(0 < dif && dif< hour * 3){
		var num = dif % hour;
		var second_ux = num % min; //得到秒数时间戳
		var second_num = parseInt(second_ux / second); //得到秒数
		var min_ux = num - second_ux; //得到分数时间戳
		var min_num = parseInt(min_ux/min); //得到分数
		var hour_ux = dif - min_ux - second_ux; //得到小时时间戳
		var hour_num = parseInt(hour_ux/hour); //得到小时数
		$("#hour").html(hour_num);
		$("#min").html(min_num);
		$("#second").html(second_num);
		setTimeout("change_time()",1000);
	}else{
		$("#wait1").show();
		$("#wait2").hide();
		$("#upload").hide();
		//window.location.href="<%= __ROOT_PATH__%>/user/authentication/submit_application/auditing_index.html";
	}
}
function wait(){
	$("#wait2").show();
	$("#wait1").hide();
}

$(function(){
if(${user.m.borrow_type == 3 }){
	$("#net").show();
}else{
	$("#common").show();
}
	var url = [${user_authenticate_upload_info.m.id_authenticate_url},${user_authenticate_upload_info.m.work_authenticate_url},
			${user_authenticate_upload_info.m.credit_authenticate_url},${user_authenticate_upload_info.m.income_authenticate_url},
			${user_authenticate_upload_info.m.housing_authenticate_url},${user_authenticate_upload_info.m.car_authenticate_url},
			${user_authenticate_upload_info.m.marriage_authenticate_url},${user_authenticate_upload_info.m.education_authenticate_url},
			${user_authenticate_upload_info.m.title_authenticate_url},${user_authenticate_upload_info.m.living_authenticate_url}];
	var status = [${user_authenticate_upload_info.m.id_authenticate_status},${user_authenticate_upload_info.m.work_authenticate_status},
		       	${user_authenticate_upload_info.m.credit_authenticate_status},${user_authenticate_upload_info.m.income_authenticate_status},
		       	${user_authenticate_upload_info.m.housing_authenticate_status},${user_authenticate_upload_info.m.car_authenticate_status},
		       	${user_authenticate_upload_info.m.marriage_authenticate_status},${user_authenticate_upload_info.m.education_authenticate_status},
		       	${user_authenticate_upload_info.m.title_authenticate_status},${user_authenticate_upload_info.m.living_authenticate_status}];
//change_color("${credit_rating}","credit_rating");
//添加已上传图片
	for(var i = 0 ;i<10;i++){
		if(status[i] == 0){
			if(url[i] != null){
				$("#"+i+"zi").html("补充上传");
				$("#"+i).append("<img src='<%=__PUBLIC__%>/images/loan_b_11.png' />"); 
			}
		}else if(status[i] == 1){
			$("#"+i+"zi").html("补充上传");
			$("#"+i).append("<img src='<%=__PUBLIC__%>/images/loan_b_12.png' />"); 
		}else if(status[i] == 2){
			$("#"+i+"zi").html("补充上传");
			$("#"+i).append("<img src='<%=__PUBLIC__%>/images/loan_b_13.png' />"); 
		}
	}
	if(${borrower_bulk_standard_apply_order.m.state == 2}){
		change_time();
		wait();
	}
	if((${borrower_bulk_standard_apply_order.m.state==2}) || (${borrower_bulk_standard_apply_order.m.state==4})){
		$("#upload").show();
	}
	if(${borrower_bulk_standard_apply_order.m.borrow_type == 1}){
		$("#wi1").show();
		$("#in1").show();
	}else{
		$("#wi2").show();
		$("#in2").show();
	}
});
  function sures()  {
      if(confirm('确定提交？')){
      window.location.href="<%= __ROOT_PATH__%>/user/authentication/submit_application/change_four_to_three.html"
      }else{
            return false;    
      }
  }
</script>


	</body>
</html>


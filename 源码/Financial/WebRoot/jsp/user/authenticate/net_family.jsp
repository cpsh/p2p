<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">

<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="loan_bb loan_bb_2">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
               <c:choose>
			   	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==3}">&nbsp;净值贷&nbsp;</c:when>
			   </c:choose>
              <span>></span>&nbsp;填写借款申请
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s></s>
              <span>3</span>
              <s></s>
              <span>4</span>
              <s></s>
              <span>5</span>
           </div>
           <div class="down">
              <span style="color:#ed5050;">填写借款申请</span>
              <span style="margin-left:55px; color:#ed5050;">填写借款信息</span>
              <span style="margin-left:60px;">审核</span>
              <span style="margin-left:60px;">筹集借款</span>
              <span style="margin-left:55px;">获得借款</span>
           </div>
        </div>
        
        <div class="loan_bb_con">
	<%@ include file="/jsp/user/authenticate/authenticate_left.jsp"%>
          <div class="right">
             <div class="right_top">家庭信息</div>
             <div class="right_up">温馨提示：我们将在您的必要认证资料上传齐全后为您提交审核。</div>
             <div class="con">
                <label class="label_1"><span>*</span>&nbsp;婚姻状况：</label>
                <p class="label_p">
                   <input value="已婚" name="marriage" type="radio"> 已婚&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="未婚" name="marriage" type="radio" checked="checked"> 未婚&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="离异" name="marriage" type="radio"> 离异&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="丧偶" name="marriage" type="radio"> 丧偶&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
             </div>
             <div class="con con_1">
                <label class="label_1"><span>*</span>&nbsp;有无子女：</label>
                <p class="label_p">
                   <input value="有" name="children" type="radio"> 有&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="无" name="children" type="radio" checked="checked"> 无
                </p>
             </div>
             <div class="con con_2">紧急联系人</div>
             <div class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;姓名：</label>
                <input id="lineal_name" class="input_1" onFocus="hide(2,21)" onblur="check('lineal_name',21)" value="${user_authenticate_family_info.m.lineal_relatives_name}" type="text">
                <p id="2" class="label_p7" style="display:none;">*请填写姓名</p>
                <p id="21" class="label_p7" style="display:none;">*姓名有误</p>
             </div>
             <div class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;关系：</label>
                <input id="lineal_relation" class="input_1" onFocus="hide(3,31)" onblur="check('lineal_relation',31)" value="${user_authenticate_family_info.m.lineal_relatives_relation}" type="text">
                <p id="3" class="label_p7" style="display:none;">*请填写关系</p>
                <p id="31" class="label_p7" style="display:none;">*关系有误</p>
             </div>
             <div class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;手机：</label>
                <input id="lineal_phone" class="input_1" onFocus="hide(4,41)" onblur="check_phone('lineal_phone',41)" value="${user_authenticate_family_info.m.lineal_relatives_phone}" type="text">
             	<p id="4" class="label_p7" style="display:none;">*请填写手机</p>
             	<p id="41" class="label_p7" style="display:none;">*手机号码有误</p>
             </div>
             <div class="con con6">
              <input type="hidden" id="apply_order_id" value="${apply_order_id}" />
                <p class="label_p5"><a onclick="save();" href="#">保存并继续</a></p>
             </div>
          </div>
        </div>
     </div>
  </div>  
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__%>/js/select.js"></script>
<script type="text/javascript">
	
	$("input[name='marriage'][value='${user_authenticate_family_info.m.marriage}']").attr("checked",'checked');
	
	$("input[name='children'][value='${user_authenticate_family_info.m.children}']").attr("checked",'checked');
	
	function save() {
		var marriage = $("input[name='marriage']:checked").val();
		var children = $("input[name='children']:checked").val();

		var lineal_name = $("#lineal_name").val();
		var lineal_relation = $("#lineal_relation").val();
		var lineal_phone = $("#lineal_phone").val();
		
		var apply_order_id=$("#apply_order_id").val();

		$.ajax({url : '<%=__ROOT_PATH__%>/user/authentication/family_info/save_net_family_info.html',
				type:'post',
				cache:false,
				async:false,
				data:{"marriage":marriage,"children":children,"lineal_name":lineal_name,"lineal_relation":lineal_relation,"lineal_phone":lineal_phone,"apply_order_id":apply_order_id},
				success:function(data){
				if(data=="ok"){
					window.location.href="<%=__ROOT_PATH__ %>/user/authentication/work_info/work_index.html";
				}if(data=="no"){
					alert("提交失败");
				}
				else{
					$("#"+data).show();
				}
			}
		},"html")
	}
		
	function hide(id,id2){
		$("#"+id).hide();
		$("#"+id2).hide();
	}
		
</script>


	</body>
</html>


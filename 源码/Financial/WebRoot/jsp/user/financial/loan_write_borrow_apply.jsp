<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
  </head>
  <%
  //需要检查三个方面
  %>
  <body>
  <%@ include file="/jsp/user/financial/user_appointment.jsp" %>
<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>
<div class="about">
  <div class="about_con">
     <div class="loan_aa">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
              <c:choose>
              	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
              	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
              </c:choose>
              <span>></span>&nbsp;填写借款申请
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s></s>
              <span>2</span>
              <s></s>
              <span>3</span>
              <s></s>
              <span>4</span>
              <s></s>
              <span>5</span>
           </div>
           <div class="down">
              <span style="color:#cc0000;">填写借款申请</span>
              <span style="margin-left:55px;">填写借款信息</span>
              <span style="margin-left:60px;">审核</span>
              <span style="margin-left:60px;">筹集借款</span>
              <span style="margin-left:55px;">获得借款</span>
           </div>
        </div>
        
        <div class="loan_aa_con">
           <div class="up">借款申请信息填写</div>
           <div class="con">
              <label class="label_1">借款标题：</label>
              <input class="input_1" value="" type="text" id="title" name="title" onblur="check_title();">
              <p class="loan_p">&nbsp;&nbsp;不超过14字<s></s></p>
              <p id="title_msg" class="loan_p5" display="none">输入正确的</p>
           </div>
           <div class="con">
              <label class="label_1">借款用途：</label>
              <input class="input_1" value="" type="text" id="use" name="use" onblur="check_use();">
              <p id="use_msg" class="loan_p5">请输入正确的</p>
           </div>
           <div class="con">
              <label class="label_1">借款金额：</label>
              <input class="input_1" value="" type="text" id="money" name="money" onblur="adjust_money(),check_money(),get_monthly_principal_interst();change_service_fee();" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
              
              <p class="loan_p loan_p2">&nbsp;&nbsp;借款金额范围：<br/>&nbsp;&nbsp;3000-500000，且为50的<br />&nbsp;&nbsp;倍数<s></s></p>
              <p id="money_msg" class="loan_p5">请输入正确的</p>
           </div>
           <div class="con">
              <label class="label_1">借款期限：</label>
              <div class="con_a">
                 <dl class="select">
                    <dt id="duration">3个月</dt>
                    <dd>
                       <ul>
                          <li ><a href="javascript:void(0)">3个月</a></li>
                          <li ><a href="javascript:void(0)">6个月</a></li>
                          <li ><a href="javascript:void(0)">9个月</a></li>
                          <li ><a href="javascript:void(0)">12个月</a></li>
                          <li ><a href="javascript:void(0)">15个月</a></li>
                          <li ><a href="javascript:void(0)">18个月</a></li>
                          <li ><a href="javascript:void(0)">24个月</a></li>
                          <li ><a href="javascript:void(0)">36个月</a></li>
                       </ul>
                    </dd>
                 </dl>
              </div>
           </div>
           <div class="con con5">
              <label class="label_1">年化利率：</label>
              <input  style="width:250px; padding-right:30px;" class="input_1" value="" type="text" id="interest_rate" name="interest_rate" onblur="check_interest_rate(),get_monthly_principal_interst();" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"><span>%</span>
              <p class="loan_p loan_p3">&nbsp;&nbsp;利率精确到小数点后一位，<br />&nbsp;&nbsp;范围10%-24%之间，借款最<br />&nbsp;&nbsp;低利率有您的借款期限确<br />&nbsp;&nbsp;定，一般来说借款利率越<br />&nbsp;&nbsp;高，筹款速度越快。<s></s></p>
              <p id="rate_msg" class="loan_p5">请输入正确的</p>
           </div>
           <div class="con con2">
              <label class="label_1"></label>
              <div class="con_b"><span>等额本息</span><a href="#"></a></div>
              <p class="loan_p loan_p4">&nbsp;&nbsp;等额本息，即借款人每月以相<br />&nbsp;&nbsp;等的金额偿还本息，也是<br />&nbsp;&nbsp;银行房贷等采用的方法。因计<br />&nbsp;&nbsp;算中存在四舍五入，最后一期<br />&nbsp;&nbsp;还款金额略有所不同。<s></s></p>
           </div>
           <div class="con con2">
              <label class="label_1">每月还本金及利息：</label>
              <div class="con_c"><span id="pincipal_interest">￥0.00 </span>(支付给理财人)</div>
           </div>
           <div class="con con2">
             <label class="label_1">每月交借款管理费：</label>
             <div class="con_c"><span id="manage_fee">￥0.00 </span>(由联富金融平台收取)</div>
           </div>
           <div class="con con2">
             <label class="label_1">期初服务费：</label>
             <div class="con_c">由联富金融平台收取</div>
           </div>
           <form class="form_a">
              <table width="580" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                    <td width="90" height="39" style="border-bottom:1px #e0e0e0 solid">信用等级</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s AA snow">A+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s A snow">A</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s BB snow">B+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s B snow">B</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s CC snow">C+</span></td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid"><span class="ui-creditlevel ui-creditlevel-s C snow">C</span></td>
                 </tr>
                 <tr>
                    <td height="46" style="border-bottom:1px #e0e0e0 solid">服务费率</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">0%</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">1%</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">2%</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">3%</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">4%</td>
                    <td width="70" height="40" align="center" style="padding-left:7px;border-bottom:1px #e0e0e0 solid">5%</td>
                 </tr>
                 <tr>
                    <td height="37" style="border-bottom:1px #e0e0e0 solid">服务费</td>
                    <td width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                    <td id="sfee1" width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                    <td id="sfee2" width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                    <td id="sfee3" width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                    <td id="sfee4" width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                    <td id="sfee5" width="70" height="40" align="center" style="border-bottom:1px #e0e0e0 solid">0</td>
                 </tr>
              </table>
           </form>
           <div class="con con3">
             <label class="label_1">借款描述：</label>
             <textarea class="textarea_a" id="describe"></textarea>
           </div>
           <div class="con con4">
             <label class="label_1"></label>
             <input class="agree" name="agree" checked="checked" type="checkbox">
              <p class="label_p5">我已阅读并同意&nbsp;<a href="<%=__ROOT_PATH__%>/index/about/agreement_6.html" target="_blank">《联富金融网站服务协议》</a></p>
           </div>
           <div class="con con4">
             <label class="label_1"></label>
              <p class="label_p6"><a onclick="submit_apply();" href="javascript:void(0)">保存并继续</a></p>
           </div>
       </div>
        
     </div>
  </div>  
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
	
  </body>
  
  	<script type="text/javascript">

  	$(function(){$(".loan_p5").hide();});

	function check_title(){
		var title = $("#title").val();
		//为空验证
		if(title.trim()=="" || title.length==0){
			$("#title_msg").html("借款标题不能为空");
			$("#title_msg").show();
			return;
		}
		if(title.length>14){
			$("#title_msg").html("借款标题不能超过14个文字");
			$("#title_msg").show();
			return;
		}
		//
		$("#title_msg").html("");
		return;
	}
	function check_use(){
		var use = $("#use").val();
		if(use.trim()=="" || use.length==0){
			$("#use_msg").html("借款用途不能为空");
			$("#use_msg").show();
			return;
		}
		$("#use_msg").html("");
		return;
	}

	function check_money(){
		var money = $("#money").val();
		if(money.trim()=="" || money.length==0){
			$("#money_msg").html("借款金额不能为空");
			$("#money_msg").show();
			return;
		}
		if(money<3000 || money>500000){
			$("#money_msg").html("借款金额在3000-500000之间");
			$("#money_msg").show();
			return;
		}
		if(money%50!=0){
			$("#money_msg").html("借款金额必须是50的整数倍");
			$("#money_msg").show();
			return;
		}
		$("#money_msg").html("");
		return;
	}

	function check_interest_rate(){
		var interest_rate = $("#interest_rate").val();
		if(interest_rate.trim()=="" || interest_rate.length==0){
			$("#rate_msg").html("年化利率不能为空");
			$("#rate_msg").show();
			return;
		}
		if(interest_rate<10 || interest_rate>24){
			$("#rate_msg").html("年化利率在10%~24%之间");
			$("#rate_msg").show();
			return;
		}
		$("#rate_msg").html("");
		return;
	}
  	
	function submit_apply(){

		$(".loan_p5").hide();

		
		var can_submit = true;
		var title = $("#title").val();
		//标题验证两次
		var title_count=0;
		if(title.trim()=="" || title.length==0){
			$("#title_msg").html("借款标题不能为空");
			$("#title_msg").show();
			can_submit = false;
			title_count++;
		}
		//两次的验证条件
		if(title.length>14){
			if(title_count>0){
			//后面不需要验证提示
			}else {
				$("#title_msg").html("借款标题不能超过14个文字");
			    $("#title_msg").show();
			    can_submit = false;
			    title_count++; 
			}
		}
		
		var use = $("#use").val();
		if(use.trim()=="" || use.length==0){
			$("#use_msg").html("借款用途不能为空");
			$("#use_msg").show();
			can_submit = false;
		}
		
		var money = $("#money").val();
		var money_count=0;
		if(money.trim()=="" || money.length==0){
			$("#money_msg").html("借款金额不能为空");
			$("#money_msg").show();
			can_submit = false;
			money_count++;
		}
		if(money<3000 || money>500000){
			if (money_count>0) {
				//后面不需要验证提示
			}else {
				$("#money_msg").html("借款金额在3000-50000之间");
				$("#money_msg").show();
				can_submit = false;
				money_count++;
			}
		}
		if(money%50!=0){
			if (money_count>0) {
				//后面不需要验证提示
			}else {
				$("#money_msg").html("借款金额在必须是50的整数倍");
				$("#money_msg").show();
				can_submit = false;
				money_count++;
			}
		}

		var interest_rate = $("#interest_rate").val();
		if(interest_rate<10 || interest_rate>24){
			$("#rate_msg").html("年化利率在10%~24%之间");
			$("#rate_msg").show();
			can_submit = false;
			
		}
		//如果不满足条件后面的则不能执行
		if(!can_submit){
			return;
		}
		
		var duration = $("#duration").html();//后台判断
		var describe=$("#describe").val();
		$.post('<%=__ROOT_PATH__%>/user/financial/borrow/loan_submit_borrow_money_apply_order.html',
				{
					"borrow_title":title,
					"borrow_purpose":use,
					"borrow_all_money": money ,
					"borrow_duration":duration ,
					"annulized_rate_int":interest_rate,
					"describe" :describe,
					"borrow_type":${borrow_type}
				},
				function(data) //回传函数    
				{
					if(data == 1){		//保存成功,跳转到填写订单信息页面
						window.location.href="<%=__ROOT_PATH__%>/user/authentication/persional_info/persional_index.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码

		}

	//把借款金额变成50的整数倍   , 计算管理费
	function adjust_money(){		
		var pre_money = $("#money").val();
		var later_money = Math.floor(pre_money/50)*50;
		$("#money").val(later_money);
		var manage_fee = later_money/1000*3;
		$("#manage_fee").html('￥'+manage_fee.toFixed(2));
			
	}

	//获取每个月的等额本息
  	function get_monthly_principal_interst(){
  		var borrow_all_money = $("#money").val();
  		var borrow_duration = $("#duration").html();
  		var annulized_rate_int = $("#interest_rate").val();

  		if(borrow_all_money == ""){
			return ;
  	  	}
  	  	if(annulized_rate_int == ""){
			return ;
  	  	}
  		
  		var url = "<%=__ROOT_PATH__%>/user/financial/borrow/get_monthly_principal_interst.html";
  		
  		$.ajax({
  		
  			url:url,
  			type:'get',
  			data:{
  				borrow_all_money : borrow_all_money,
  				borrow_duration : borrow_duration,
  				annulized_rate_int : annulized_rate_int
  			},
  			cache:false,
  			success:function(data){
  				$("#pincipal_interest").html("￥"+data);
  			}
  		
  		},"json");
  	}
  	
  	function change_service_fee(){
  		var money = $("#money").val();

  		var n = [0.01,0.02,0.03,0.04,0.05];
  		for(var i = 1;i<6;i++){
  			$("#sfee"+i).html(accMul(money,n[i-1]));
  		}
  	}
  	
  	 function accMul(arg1, arg2) {
                var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
                try { m += s1.split(".")[1].length } catch (e) { }
                try { m += s2.split(".")[1].length } catch (e) { }
                return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
     }
            
$(function(){
	/*============================
	@author:flc
	@time:2014-02-11 18:16:09
	@qq:3407725
	============================*/
	$(".select").each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();get_monthly_principal_interst();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
})
</script>
  
</html>

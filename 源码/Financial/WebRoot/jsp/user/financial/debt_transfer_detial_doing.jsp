<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  </head>
  <body>
  
  <%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<input id="gather_order_id" type="hidden" value="${debt_detail_record.r.gather_order_id}" />
<input id="user" type="hidden" value="${sessionScope.user}" />
<input id="can_buy_share" type="hidden" value="${can_buy_share}" />
<div class="sanbiao">
  <h1><a href="#">我要理财</a> > <a href="#">债权转让列表</a> > 债权转让详情</h1>
  <div class="sanbiao_bt sanbiao_bt_2" style="height:340px">   
    <div class="up">
    	<span>
    		<c:if test="${debt_detail_record.r.borrow_type==1}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> 
    	</c:if>
    	<c:if test="${debt_detail_record.r.borrow_type==2}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_2.jpg" /> 
    	</c:if>
    	<c:if test="${debt_detail_record.r.borrow_type==3}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_3.jpg" /> 
    	</c:if>
    		${debt_detail_record.r.borrow_title}
    	</span>
    	<a class="a" href="#">债权转让及受让协议（范本）</a>
    </div> 
    <h2>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="88" colspan="2" style="font-size:14px;">转让单价 （元/份）<br /><span class="sanbiao_xx_text">${debt_detail_record.r.transfer_price}</span></td>
    <td height="88" colspan="2">年利率<br /><span class="sanbiao_xx_text">${debt_detail_record.r.annulized_rate_int}%</span></td>
    <td width="15%" height="88">剩余期限 （月）<br /><span class="sanbiao_xx_text">${debt_detail_record.r.remain_period}/${debt_detail_record.r.total_periods}</span></td>
  </tr>
  <tr>
    <td width="21%" height="35" style="font-size:14px;">原标的ID</td>
    <td width="20%" height="35" align="left" style="font-size:14px;" class="td_4"><a>${debt_detail_record.r.gather_order_id}</a></td>
    <td width="23%" height="35" align="left" style="font-size:14px;">转让系数</td>
    <td height="35" align="left" style="font-size:14px;" colspan="2">${debt_detail_record.r.transfer_factor}%</td>
  </tr>
  <tr>
    <td width="21%" height="35" style="font-size:14px;">保障方式</td>
    <td width="20%" height="35" align="left" style="font-size:14px;" class="td_bj">本金+利息&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>详情参见<s>本金保障计划</s><img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a></td>
    <td width="23%" height="35" align="left" style="font-size:14px;">待收本息</td>
    <td height="35" align="left" style="font-size:14px;" colspan="2">${debt_detail_record.r.weihuan_benxi_total_per_share}元/份</td>
    <td width="7%" rowspan="2"></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">下一还款日</td>
    <td height="35" align="left" style="font-size:14px;">${debt_detail_record.r.automatic_repayment_date}</td>
    <td height="35" align="left" style="font-size:14px;">逾期情况</td>
    <td height="35" align="left" style="font-size:14px;" colspan="2">未发生过逾期</td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">提前还款费率</td>
    <td height="35" align="left" style="font-size:14px;">1.00%</td>
    <td height="35" align="left" style="font-size:14px;">还款方式</td>
    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx" colspan="2">按月还款/等额本息&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>等额本息还款法是在还款期内，每月偿还同等数额的贷款(包括本金和利息)。<br />借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a></td>
  </tr>
</table>
</h2>
    <h5 class="h5_3">
       <div class="div_1">剩余份数（份）</div>
       <div class="div_1"><span>${debt_detail_record.r.transfer_share_remainder}</span>（${debt_detail_record.r.remain_money}元）</div>
       <div class="div_1"><s>账户余额&nbsp; ${cny_can_used}元</s><a href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></div>
       <div class="div_1 div_2"><input type="text" id="buy_share" onkeyup="this.value=this.value.replace(/\D/g,'');check_share();" onafterpaste="this.value=this.value.replace(/\D/g,'');check_share();" /><span>份</span></div>
       <div class="div_1 div_5" id="msg" style="display:block">最多可购买${can_buy_share}份</div>
       <script src="js/loginDialog.js" type="text/javascript"></script>
       <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
       <div class="div_1 div_4" onClick="open_buy_debt();" >购&nbsp;&nbsp;买</div>
       
       <div id="loginDiv1" class="loginDiv1"></div>
       <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
		<div id="confirm_transfer" class="sanbiao_toubiao">          
          	
          	<!-- 每份债权价值 -->
             <input type="hidden" id="debt_value_hidden" value="${debt_detail_record.r.creditor_right_value}" />
             <!-- 每份转让价格 -->
             <input type="hidden" id="transfer_price_hidden" value="${debt_detail_record.r.transfer_price}" />
             <!-- 每份待收本息 -->
             <input type="hidden" id="remain_to_collect_hidden" value="${debt_detail_record.r.weihuan_benxi_total_per_share}" />
             
             <input type="button" class="guanbi" onClick="closeme()" value="ｘ" />
             
             <div class="up">确认购买</div>
             <div class="con"><p>待转让债权</p><span>ID&nbsp;${debt_detail_record.r.gather_order_id}</span></div>
             <div class="con"><p>购买份数</p><span id="buy_share_span">0份</span></div>
             <div class="con"><p>债权总价值</p><span id="debt_total_value_span">0.0元</span></div>
             <div class="con con_5"><p></p><span>债权价值的通常算法：当时的待收本金与计算当日距离上一期还款的天数所对应的利息之和。<a href="#" style="color:#ed5050;">查看详情</a></span></div>
             <div class="con"><p>投资金额</p><span id="invest_money_span">0.0元</span></div>
             <div class="con"><p>待收本息</p><span id="remain_to_recieve_span">0.0元</span></div>
             <div class="con"><p>应付金额</p><span ><b id="should_pay_money_span">0.0</b>元</span></div>
             <div class="con"><p>折让收益</p><span id="off_benifit">0.0元</span></div>
             <div class="con"><input class="checkbox_1" type="checkbox" id="checkbox_1" /><s>我已阅读并同意签署</s><a style="color:#ed5050;" href="#">《债权转让及受让协议》</a></div>
             <div class="con con_2"><a class="a" href="javascript:void(0);" onclick="confirm_buy_debt();">确定</a><a href="#" onclick="closeme();">取消</a></div>
		</div>
		
			<div id="success" style="display:none" class="sanbiao_toubiao" >
				<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic7.png" />购买债权成功</div>
				<div class="con_4" ><a onClick="closeme_refresh()">关闭</a></div>
			</div>
			
		</div>
       
    </h5>    
  </div>
  
  
  <div class="sanbiao_xx_con">
    <h2>
		<p class="hover">标的详情</p>
	    <p onclick="get_bid_record();">投标记录</p>
	    <p onclick="get_repayment_behavior();">还款表现</p>
	    <p onclick="get_debt_info();" >债权信息</p>
	    <p onclick="get_transfer_record();">转让记录</p>
	</h2>
    <h3 style="display:block">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">用户信息</td>
    <td width="13%" height="45"><span style="float:left"><a href="#" style=" color:#09F">${gather_order.r.nickname }</a></span></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">用户名</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.nickname}</td>
    <td width="12%" height="30" class="kk_text">公司行业</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_trades}</td>
    <td width="13%" height="30" class="kk_text">收入范围</td>
    <td width="14%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.monthly_income}/月</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">年    龄</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.age}</td>
    <td height="30" class="kk_text">公司规模</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_size}</td>
    <td height="30" class="kk_text">房       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">学    历</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.highest_education}</td>
    <td height="30" class="kk_text">岗位职位</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.position}</td>
    <td height="30" class="kk_text">房       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing_mortgage}</td>
  </tr>
  <tr>
  <td height="30">&nbsp;</td>
	<td height="30" class="kk_text">学         校</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.school}</td>
    <td height="30" class="kk_text">工作城市</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_city}</td>
    <td height="30" class="kk_text">车       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">婚         姻</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.marriage}</td>
    <td height="30" class="kk_text">工作时间</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.year_limit}</td>
    <td height="30" class="kk_text">车       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car_mortgage}</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="30" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">信用档案</td>
    <td width="13%" height="30"><span id="rating_color" >${borrower_bulk_standard_order_user_info.m.credit_rating}</span></td>
    <td height="30" colspan="5">&nbsp;</td>
    </tr>
   <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">申请借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.loan_application }</td>
    <td height="30" class="kk_text">信用额度（元）</td>
    <td id="line_of_credit" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.line_of_credit }</td>
    <td height="30" class="kk_text">逾期金额（元）</td>
    <td id="overdue_amount" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_amount }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">成功借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.successful_loan }</td>
    <td height="30" class="kk_text">借款总额（元）</td>
    <td id="total_loan" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.total_loan }</td>
    <td height="30" class="kk_text">逾期次数（次）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_num }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">还清笔数（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.pay_off_the_pen_number }</td>
    <td height="30" class="kk_text">待还本息（元）</td>
    <td id="repay" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.repay }</td>
    <td height="30" class="kk_text">严重逾期（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.seriously_overdue }</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">审核状态</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
   </tr>
  <tr>
    <td height="40" colspan="3" align="center" class="sanbiao_xx_kk3"><span class="kk_text">审核项目</span></td>
    <td height="40" colspan="4" align="center" class="sanbiao_xx_kk3">状态</td>
    </tr>
   <tr id="0_tr">
    <td id="_0" height="45" colspan="3" align="center" ><span class="kk_text">身份认证</span></td>
    <td	id="0" height="45" colspan="4" align="center"  ></td>
    </tr>
  <tr id="1_tr">
    <td id="_1" height="45" colspan="3" align="center" ><span class="kk_text">工作认证</span></td>
    <td id="1" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="2_tr">
    <td id="_2" height="45" colspan="3" align="center" ><span class="kk_text">信用报告</span></td>
    <td id="2" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="3_tr">
    <td id="_3" height="45" colspan="3" align="center" ><span class="kk_text">收入认证</span></td>
    <td id="3" height="45" colspan="4" align="center" ></td>
   </tr>
     <tr id="4_tr">
    <td id="_4" height="45" colspan="3" align="center" ><span class="kk_text">房产认证</span></td>
    <td id="4" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="5_tr">
    <td id="_5" height="45" colspan="3" align="center" ><span class="kk_text">车产认证</span></td>
    <td id="5" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="6_tr">
    <td id="_6" height="45" colspan="3" align="center" ><span class="kk_text">婚姻认证</span></td>
    <td id="6" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="7_tr">
    <td id="_7" height="45" colspan="3" align="center" ><span class="kk_text">学历认证</span></td>
    <td id="7" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="8_tr">
    <td id="_8" height="45" colspan="3" align="center" ><span class="kk_text">职称认证</span></td>
    <td id="8" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="9_tr">
    <td id="_9" height="45" colspan="3" align="center" ><span class="kk_text">居住认证</span></td>
    <td id="9" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td height="45" colspan="6" class="kk_text">&bull;&nbsp;&nbsp;联富金融及其合作机构将始终秉持客观公正的原则，严控风险，最大程度的尽力确保借入者信息的真实性，但不保证审核信息100%无误。<br/>
                                              &bull;&nbsp;&nbsp;借入者若长期逾期，其个人信息将被公布。</td>
    </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">借款描述</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td colspan="6" rowspan="2" class="kk_text" style="line-height:30px">${gather_order.r.describe}</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    </tr>  
</table>
</h3>

	<!-- 投标记录 -->
	<h3 class="h3_2" style="display: none;">
		<div align="right">
			<span class="td_1" id="person_time">加入人次0人</span>&nbsp;&nbsp;&nbsp;<span class="td_1" id="sum_invest_money">投标总额0元</span>
		</div>
		<table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tr_1">
				<td>序号</td>
				<td>投标人</td>
				<td>投标金额</td>
				<td>投标时间</td>
			</tr>
		</table>
	</h3>
     
     <!-- 还款表现 -->
	<h3 class="h3_2">
		<div align="right">
			<span class="td_1" id="yihuan_beixi">已还本息0.00元</span>&nbsp;&nbsp;&nbsp;<span class="td_1" id="weihuan_benxi">待还本息0.00元</span>
		</div>
		<table id="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tr_1">
				<td>合约还款日期</td>
				<td>状态</td>
				<td>应还本息</td>
				<td>应付罚息</td>
				<td>实际还款日期</td>
			</tr>
		</table>
	</h3>
     
	<!-- 债权信息 -->
	<h3 class="h3_2">
		<div align="right">
			<span class="td_1" id="hold_count">持有债权人数0人</span>
		</div>
		<table id="table3" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tr_1">
				<td>序号</td>
				<td>债权人</td>
				<td>待收本息</td>
				<td>持有份数</td>
			</tr>
		</table>
	</h3>
     
     <!-- 转让记录 -->
	<h3 class="h3_2">
		<div align="right">
			<span class="td_1" id="trade_total_money">已交易总额0.00元</span>
		</div>
		<table id="table4" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="tr_1">
				<td>债权买入者</td>
				<td>债权卖出者</td>
				<td>交易金额</td>
				<td>交易时间</td>
			</tr>
		</table>
	</h3>
</div>
  
</div>




<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__ %>/js/the_function.js"></script>
<script src="<%=__PUBLIC__ %>/js/debt_detail_info.js"></script>
<script type="text/javascript">

	//
	function check_share(){
		var buy_share = $("#buy_share").val();

		if(buy_share > ${debt_detail_record.r.transfer_share_remainder}){
			$("#buy_share").val(${debt_detail_record.r.transfer_share_remainder});
		}
	}

	//确定买入债权
	function confirm_buy_debt(){

		var url = "<%=__ROOT_PATH__%>/user/financial/transfer/confirm_buy_debt.html";
		var transfer_out_order_id = ${debt_detail_record.r.id};
		var transfer_price = $("#transfer_price_hidden").val();
		var buy_share = $("#buy_share").val();
		var invest_money = $("#should_pay_money_span").html();

		if( !$("#checkbox_1").is(':checked') ){
			return ;
		}
		/*
		final long transfer_out_order_id = getParameterToLong("transfer_out_order_id"); //转出单  表的id
		double transfer_price_double = getParameterToDouble("transfer_price");//转让价格
			final BigDecimal transfer_price = new BigDecimal(transfer_price_double);
		final int buy_share = getParameterToInt("buy_share");					//要购买的份数 
		double invest_money_double = getParameterToDouble("invest_money");     	//花费的钱
			BigDecimal invest_money = new BigDecimal(invest_money_double+"");		//double 转成BigDecimal
		*/
		
		$.ajax({
			url:url,
			type:'post',
			data:{
				"transfer_out_order_id":transfer_out_order_id,
				"transfer_price":transfer_price,
				"buy_share":buy_share,
				"invest_money":invest_money
				
				},
			cache:false,
			success:function(data)
			{
					//alert(data);
				if(data == "0"){
					$("#confirm_transfer").hide();
					$("#success").show();
					//showMessage(["提示","买入债权成功"]);
					//window.location.reload();
				}else if(data == "1"){
					alert("你的余额不足");
				}else if(data=="2"){
					alert("债权剩余份数不足，请减少购买份数或者购买其它债权");
				}else if(data == "3"){
					alert("购买失败");
				}else{
					alert("else...");
				}
			}
		},"json");	
	}
</script>
</body>
</html>
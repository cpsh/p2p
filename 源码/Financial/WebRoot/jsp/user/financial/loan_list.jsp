<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
	
	<script src="js/loginDialog.js" type="text/javascript"></script>
    <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />

  </head>
  
  <body>
<%@ include file="/jsp/index/index_top.jsp" %>
<!-- 
<div class="head_top">
  <h1><img src="<%=__PUBLIC__ %>/images/logo.jpg" /></h1>
  <ul>
    <li><a href="/index/index.html">首页</a></li>
    <li><a href="sanbiao.html">我要理财</a></li>
    <li><a href="jiekuan.html">我要借款</a></li>
    <li><a href="zhiyin.html">新手指引</a></li>
    <li><a href="about.html">关于我们</a></li>
  </ul>
</div>
 -->
<span id="ny_pic"></span>


<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 散标投资列表</h1>
  <div class="sanbiao_bt">
    <h2><table id="table0" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" colspan="6"><p style=" font-size:18px; float:left">筛选投资项目</p> <span style="float:left"><a href="#" style="width:40px; background:#e5e5e5; margin-left:15px">多选</a></span></td>
    </tr>
  <tr>
    <td width="13%" height="35" style="font-size:14px;">标的类型</td>
    <td width="15%" height="35"><a href="#">信用认证标</a></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">借款期限</td>
    <td height="35"><a id="buxian1" href="javascript:void(0)" onclick="asychronized_query(0,-1,this,1);" style="width:40px">不限</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(1,-1,this,1);">3-6个月</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(2,-1,this,1);">9-15个月</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(3,-1,this,1);">18-24个月</a></td>
    <td height="35"><a href="javascript:void(0)" onclick="asychronized_query(4,-1,this,1);">24个月以上</a></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">认证等级</td>
    <td height="35"><a id="buxian2" href="javascript:void(0)" onclick="asychronized_query(-1,0,this,1);" class="hover" style="width:40px">不限</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,1,this,1);">A+</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,2,this,1);">A</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,3,this,1);">B+</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,4,this,1);">B</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,5,this,1);">C+</a></td>
    <td height="35" align="center"><a href="javascript:void(0)" onclick="asychronized_query(-1,6,this,1);">C</a></td>
  </tr>
</table>
</h2>
    <h3><span>新手引导</span><a href="#">什么是信用等级？</a> <a href="#">什么是信用认证标？</a> <a href="#">什么是实地认证标？</a> <a href="#">什么是机构担保标？</a></h3>    
  </div>
  <h4><img src="<%=__PUBLIC__ %>/images/sanbiao_icon.jpg" /> <span>温馨提示：近期工作日固定发标时间在11:00、13:30、17:00，其余时间与周末随机发标。</span></h4>
  <div class="sanbiao_list">
  <h2><p>投资列表</p><a href="#">理财计算器</a><span>累计成交总金额<br />44.53亿元</span><span>累计成交总笔数<br />81670笔</span><span>为用户累计赚取<br />29090.86万元</span></h2>
  <h3>
  <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
  
  
  <tr>
    <td width="29%" height="45" class="con2_kk">借款标题</td>
    <td width="10%" height="45" class="con2_kk">信用等级</td>
    <td width="12%" height="45" class="con2_kk">年利率</td>
    <td width="15%" height="45" class="con2_kk">金额</td>
    <td width="10%" height="45" class="con2_kk">期限</td>
    <td width="11%" height="45" class="con2_kk">进度</td>
    <td width="13%" height="45" class="con2_kk">&nbsp;</td>
  </tr>
  
  <!-- 
  <c:forEach items="${gather_money}" var="bid_info">
  	<tr>
    <td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id=${bid_info.m.id}">${bid_info.m.borrow_purpose}</a></td>
    <td height="55" class="con2_kk"><span class="con2_yuan">A</span></td>
    <td height="55" class="con2_kk">${bid_info.m.annulized_rate}</td>
    <td height="55" class="con2_kk">${bid_info.m.borrow_all_money}元</td>
    <td height="55" class="con2_kk">${bid_info.m.borrow_duration}个月</td>
    <td height="55" class="con2_kk"><span class="hongse">${bid_info.m.gather_progress}%</span></td>
    <c:if test="${bid_info.m.gather_state==1}">
    	<td height="55" align="center" class="con2_kk"><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id=${bid_info.m.id}"><p class="con2_anniu con2_anniu2">投标</p></a></td>
    </c:if>
    <c:if test="${bid_info.m.gather_state==2}">
    	<td height="55" align="center" class="con2_kk"><a href="javscript:void(0)"><p class="con2_anniu con2_anniu2">已满标</p></a></td>
    </c:if>
    <c:if test="${bid_info.m.gather_state==3}">
    	<td height="55" align="center" class="con2_kk"><a href="javscript:void(0)"><p class="con2_anniu con2_anniu2">流标</p></a></td>
    </c:if>
  </tr>
  </c:forEach>
   -->
  
 
  <div id="loginDiv1" class="loginDiv1"></div>
  <div id="loginDiv2" class="loginDiv2" style=" z-index:10005">
     <div class="sanbiao_tb">
        <input type="button" value="ｘ" onClick="closeme('loginDiv1','loginDiv2')" class="sanbiao_tb_x">
        <div class="up">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;投标确认</div>
        <div class="con">
           <p class="a"><span>精</span></p>
           <p class="c b">兼职创业(男,44岁,广西壮族自治区贵港市)</p>
           <p class="c">年利率：12%&nbsp;&nbsp;&nbsp;借款期限：36个月&nbsp;&nbsp;&nbsp;借款金额：120,000元</p>
        </div>
        <div class="con2">
           <label class="label_1"><span>*</span>投标金额：</label>
           <input class="input_1" value="" type="text" name="nickName">元
        </div>
        <div class="con2">
           <input class="agree" name="agree" checked="checked" type="checkbox">
           <p>我已阅读并同意<a href="#">《联富金融网站服务协议》</a></p>
        </div>
        <div class="con2 con3"><a class="a" href="#">确认</a><a class="b" href="#">返回修改</a></div>
     </div>
  </div>
  
  
</table>
</h3>
			<h6 id="asyncPageDiv" >
			  	${asyncPageDiv}
			 </h6>
</div>
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
   	<script type="text/javascript">

		$(function(){
			asychronized_query(0,0,null,1);
			$("#buxian1").css("color","red");
			$("#buxian2").css("color","red");
		});
	
		function asychronized_query(borrow_duration_type,credit_rating,event,pn){
			var url = "<%=__ROOT_PATH__%>/user/financial/financial/loan_list_query.html";
			var obj= event;	
			//alert(obj);
			var $obj = $(obj);
			$obj.css("color","red");
			$obj.parent().siblings().children("a").css("color","black");
			
			$.ajax({
				url:url,
				type:'post',
				data:{
					"borrow_duration_type":borrow_duration_type,
					"credit_rating":credit_rating,
					"pn":pn
					},
				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 
					var tr_str ='';
					for(var i=0;i<jsObject.bid_list.length;i++ ){

						var gather_str = '<td height="55" align="center" class="con2_kk"><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'"><p class="con2_anniu con2_anniu2">投标</p></a></td>';
						var full_str = '<td height="55" align="center" class="con2_kk"><span href="javascript:void(0);"><p class="con2_anniu con2_anniu2">已满标</p></span></td>';
						var out_of_date_str ='<td height="55" align="center" class="con2_kk"><span href="javascript:void(0);"><p class="con2_anniu con2_anniu2">流标</p></span></td>';

						var str ;
						
						if(jsObject.bid_list[i].gather_state==1){
							str = gather_str;
						}else if(jsObject.bid_list[i].gather_state==2){
							str = full_str;
						}else if(jsObject.bid_list[i].gather_state==3){
							str = out_of_date_str;
						}
						
						tr_str += '<tr>'
						+ '<td height="55" class="con2_kk"><img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+jsObject.bid_list[i].id+'">'+jsObject.bid_list[i].borrow_title+'</a></td>'
						+ '<td height="55" class="con2_kk"><span class="con2_yuan">'+jsObject.bid_list[i].credit_rating+'</span></td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].annulized_rate+'</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].borrow_all_money+'元</td>'
						+ '<td height="55" class="con2_kk">'+jsObject.bid_list[i].borrow_duration+'个月</td>'
						+ '<td height="55" class="con2_kk"><span class="hongse">'+jsObject.bid_list[i].gather_progress+'%</span></td>'
						+ str
						+ '</tr>';
					}
					$("#table1 tr:eq(0)").after(tr_str);
					$("#asyncPageDiv").html(jsObject.asyncPageDiv);
				}
			},"json");	
		}

		function change_page_of_async(pn){
			asychronized_query(-1,-1,null,pn);
		}
		
	</script>
</html>
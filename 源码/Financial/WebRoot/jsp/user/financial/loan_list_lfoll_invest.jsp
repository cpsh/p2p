<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
	<meta http-equiv="keywords" content="${applicationScope.keywords}">
	<meta http-equiv="description" content="${applicationScope.description}">
<link href="<%=__PUBLIC__ %>/style/fix_bid_order_payment.css" rel="stylesheet" type="text/css"/>
  </head>
  
  <body>
<%@ include file="/jsp/index/index_top.jsp" %>
	<script type="text/javascript" src="<%=__PUBLIC__%>/js/fix_bid_list_order_or_join.js"></script>
	<script type="text/javascript" src="<%=__PUBLIC__%>/js/loan_list_lfoll_invest_count_down.js"></script>
<span id="ny_pic"></span>
<div class="sanbiao">
  <h1><a href="#">我要理财</a> > 联富宝</h1>    
  <div class="sanbiao_head">
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html">精英散标</a>
  	<a class="hover" href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_lfoll_invest.html">联富宝</a>
  	<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_creditor_right.html">债权转让</a></div> 

<div class="sanbiao_list sanbiao_list2">
  <div class="sanbiao_con">
     <div class="left">联富宝是联富金融推出的便捷高效的自动投标工具。联富宝在用户认可的标的范围内，对符合要求的标的进行自动投标，且回款本金在相应期限内自动复投，提高了资金的流动率和利用率，从而增加实际收益。期限结束后联富宝会通过债权转让平台进行转让退出。联富宝所对应的标的均适用于本金保障计划</div>
     <div class="right">
        <p><span>&bull;</span>一键加入，自动投标，省心省力</p>
        <p><span>&bull;</span>收益稳定，预期年化收益率16%</p>
        <p><span>&bull;</span>低门槛，最低1000元即可加入</p>
        <p><span>&bull;</span>多种期限满足不同流动性需求</p>
     </div>
  </div>  
  <%--
  <h2>
  	<p>联富宝列表</p>
  	<a href="#">理财计算器</a>
  	<span>累计成交总金额<br />44.53亿元</span>
  	<span>累计成交总笔数<br />81670笔</span>
  	<span>为用户累计赚取<br />29090.86万元</span>
 </h2>  
  --%>
  	
	  <c:forEach items="${fix_bid_system_order_top_three_list}" var="fix_bid_sys_order" varStatus="n">
		 <div class="sanbiao_lfb">
		     <div class="left">
		        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		           <tr class="tr_1">
		              <td class="td_1" width="45%"><a href="#"><span>联富宝</span><s>${fix_bid_sys_order.m.bid_name}</s></a></td>
		              <td class="td_2" width="20%"><span>${fix_bid_sys_order.m.least_money}</span><s>元</s></td>
		              <td class="td_2 td_3" width="25%"><span>${fix_bid_sys_order.m.annualized_rate}</span><s>%</s></td>
		              <td class="td_2" width="10%"><span>${fix_bid_sys_order.m.closed_period}</span><s>月</s></td>
		           </tr>
		           <tr class="tr_2">
		              <td class="td_1">总发售金额：${fix_bid_sys_order.m.total_money}元</td>
		              <td class="td_2">起投金额</td>
		              <td class="td_2">预期年化收益率</td>
		              <td class="td_2">封闭期</td>
		           </tr>
		        </table>
		     </div>
		    
			     <div class="right">
			     <%--
			        <input class="input_1" type="text" id="bid_money_${n.index + 1}" /><span>元</span>
			        --%>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 1}">
				        <input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.reserve_start_time}" />
				        	<s id="down_${n.index + 1}"></s>
				    </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 2}">
			        	 <input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.reserve_end_time}" />
				        	<s id="down_${n.index + 1}"></s>
			        </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 3}">
			        	<input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.pay_end_time}" />
			        		<s id="down_${n.index + 1}"></s>
			        </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 4}">
			        	<input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.invite_start_time}" />
			        		<s id="down_${n.index + 1}"></s>
			        </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 5}">
			        	<input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.invite_end_time}" />
			        		<s id="down_${n.index + 1}"></s>
			        </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 6}">
			        	<input type="hidden" id="count_down_${n.index + 1}" value="${fix_bid_sys_order.m.repayment_time}" />
			        		<s id="down_${n.index + 1}"></s>
			        </c:if>
			        <c:if test="${fix_bid_sys_order.m.public_state_var == 7}">已结束</c:if>
			        
			        <p class="p1">剩余可投金额：<s>${fix_bid_sys_order.m.total_money - fix_bid_sys_order.m.sold_money}</s>元</p>
			        <input type="button" class="button" onclick="goto_fix_bid_detail('${fix_bid_sys_order.m.id}');"  value="详情" />
			     </div>
			     <div class="img">
			     	 	<c:if test="${fix_bid_sys_order.m.public_state_var == 1}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_1.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 2}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_2.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 3}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_3.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 4}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_4.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 5}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_5.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 6}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_6.png" /></c:if>
				        <c:if test="${fix_bid_sys_order.m.public_state_var == 7}"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb_7.png" /></c:if>
			     </div>
			 
			
		  </div>
	  </c:forEach>
  <div class="sanbiao_lfb sanbiao_lfb2">
     <div class="left">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="tr_1">
              <td width="16%" rowspan="2"><img src="<%=__PUBLIC__ %>/images/xsb.jpg" /></td>
              <td class="td_1" width="32%"><a href="#"><s>模拟</s><span>联富宝</span></a></td>
              <td class="td_2" width="19%"><span>1</span><s>万</s></td>
              <td class="td_2 td_3" width="24%"><span>12.0</span><s>%</s></td>
              <td class="td_2" width="9%"><span>3</span><s>天</s></td>
           </tr>
           <tr class="tr_2">
              <td class="td_1">总发售金额：1000万元</td>
              <td class="td_2">起投金额</td>
              <td class="td_2">预期年化收益率</td>
              <td class="td_2">封闭期</td>
           </tr>
        </table>
     </div>
     
     <div class="right">
        <input class="input_1" type="text" value="10000" readonly="readonly" /><span>元</span>
        <p class="p1">新手体验项目<s>全额代付</s></p>
        <input type="button" class="button" onclick="go_to_newer_fix_bid_detail();" value="详情" />
    </div>
     
     <div class="img"><img src="<%=__PUBLIC__ %>/images/sanbiao_lfb2.png" /></div>
  </div>
  <%--
  <div class="sanbiao_lfb_head">
  	<span>名称</span>
  	<span>发售金额</span>
  	<span>起投金额</span>
  	<span>年化利率</span>
  	<span>封闭期限</span>
  	<span>阶段</span>
  </div>
 --%>
 <div class="sanbiao_lfb_xia">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
     		<tr class="sanbiao_lfb_head">
     			<td width="29%" class="td_1">投资名称</td>
     			<td width="13%" class="td_2">发售金额</td>
     			<td width="13%" class="td_2">起投金额</td>
     			<td width="13%" class="td_2">年化利率</td>
     			<td width="13%" class="td_2">封闭期限</td>
     			<td width="13%" class="td_2">阶段</td>
     			<td width="13%" class="td_2"></td>
     		</tr>
     	<c:forEach items="${fix_bid_system_order_list}" var="order">
	        <tr>
	           <td width="29%" class="td_1">
	           	<a href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_lfoll_invest_info.html?id=${order.m.id}">${order.m.bid_name}</a>
	           	<c:if test="${order.m.public_state_var == 6}"><img src="<%=__PUBLIC__ %>/images/lfdy_1.gif" /></c:if>
	           	<c:if test="${order.m.public_state_var == 7}"><img src="<%=__PUBLIC__ %>/images/lfdy.gif" /></c:if>
	           
	           </td>
	           <td width="13%" class="td_2"><span>${order.m.total_money}万</span><s>|</s></td>
	           <td width="13%" class="td_2"><span>${order.m.least_money}</span>元<s>|</s></td>
	           <td width="13%" class="td_2"><span>${order.m.annualized_rate}</span>%<s>|</s></td>
	           <td width="13%" class="td_2"><span>${order.m.closed_period}</span>月</td>
	           <td width="13%" class="td_2">
	           	<span>
	           			<c:if test="${order.m.public_state_var == 1}">公告中</c:if>
	           			<c:if test="${order.m.public_state_var == 2}">预定中</c:if>
	           			<c:if test="${order.m.public_state_var == 3}">等待支付</c:if>
	           			<c:if test="${order.m.public_state_var == 4}">等待开放</c:if>
	           			<c:if test="${order.m.public_state_var == 5}">开放中</c:if>
	           			<c:if test="${order.m.public_state_var == 6}">收益中</c:if>
	           			<c:if test="${order.m.public_state_var == 7}">已结束</c:if>
	           </span>
	          </td>
	           <td width="19%" class="td_3"><a href="<%=__ROOT_PATH__%>/user/financial/lfollinvest/get_lfoll_invest_info.html?id=${order.m.id}">查看详情</a></td>
	        </tr>
        </c:forEach>
     </table>
  </div>
  ${pageDiv}
  <%--
  <h6 class="news_list2">
  	<a href="#">〈</a> 
  	<a href="#" class="hover">1</a> 
  	<a href="#">2</a> 
  	<a href="#">3</a> 
  	<a href="#">4</a>
  	<a href="#">5</a> 
  	<a href="#">6</a> 
  	<a href="#">〉</a>
  </h6>--%>
  
 <div id="loginDiv1" class="loginDiv1"></div>
 <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
    <div class="order_payment">
       <input type="button" class="guanbi" onclick="close_order_pay();" value="X" />
       <input type="hidden" id="order_id" value=""/>
       <div class="up" style="text-align: center">确定加入</div>
       <div class="con"><p>名称</p><span id="bid_name">联富宝新手标</span></div>
       <div class="con"><p>年化收益</p><span id="rate">12.0%/年</span></div>
       <div class="con"><p>锁定期限</p><span id="close_period">3 天</span></div>
       <div class="con"><p>处理方式</p><span>到期本息</span></div>
       <div class="con"><p>加入金额</p><span id="join_money">10000</span></div>
       <div class="con"><p>验证码</p>
       	<input class="input_1" type="text" id="validate_code" />
        <img id="CheckCodeServlet" width="122" height="24" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay();" style="margin-left:10px; font-color: white;" />
    	</div>
       <div class="con con_1" id="error_msg"></div>
       <div class="con con_2"><a class="a" href="javascript:void(0);" id="submit_new_bid">确定</a><a style="background:#dbdbdb; color:#68696e; border-radius:15px" href="javascript:void(0);" onclick="close_order_pay();">取消</a></div>
    </div>
 </div>
  </div>
  <div style="margin-left:235px;"><script type="text/javascript">
     document.write('<a style="display:none!important" id="tanx-a-mm_15962270_8698819_29284887"></a>');
     tanx_s = document.createElement("script");
     tanx_s.type = "text/javascript";
     tanx_s.charset = "gbk";
     tanx_s.id = "tanx-s-mm_15962270_8698819_29284887";
     tanx_s.async = true;
     tanx_s.src = "http://p.tanx.com/ex?i=mm_15962270_8698819_29284887";
     tanx_h = document.getElementsByTagName("head")[0];
     if(tanx_h)tanx_h.insertBefore(tanx_s,tanx_h.firstChild);
</script></div>
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
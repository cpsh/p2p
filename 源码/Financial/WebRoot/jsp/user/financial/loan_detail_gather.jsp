<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  </head>
  <body>
  
  <%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="sanbiao">
	<input id="gather_order_id" type="hidden" value="${gather_order.r.id}" />
	<h1><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">我要理财</a> > <a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_list_elite.html">散标投资列表</a> > 借款详情</h1>
	<div class="sanbiao_bt sanbiao_bt_2">
    <div class="up"><span>
    	<c:if test="${gather_order.r.borrow_type==1}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1.jpg" /> 
    	</c:if>
    	<c:if test="${gather_order.r.borrow_type==2}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_2.jpg" /> 
    	</c:if>
    	<c:if test="${gather_order.r.borrow_type==3}">
	    	<img src="<%=__PUBLIC__ %>/images/icon1_3.jpg" /> 
    	</c:if>
    ${gather_order.r.borrow_title }</span><a class="a" href="javascript:void(0);" onclick="javascript:window.open('<%=__ROOT_PATH__ %>/user/financial/financial/to_loan_contract.html?gather_order_id=${gather_order.r.id}','_blank','height=768,width=1024,toolbar=no,scrollbars=yes,menubar=no,status=no')">借款协议（范本）</a></div>
    <h2><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="88" colspan="2" style="font-size:14px;">标的总额 （元）<br /><span class="sanbiao_xx_text">￥${gather_order.r.borrow_all_money }</span></td>
    <td height="88" colspan="2">年利率<br /><span class="sanbiao_xx_text">${gather_order.r.annulized_rate_int}%</span></td>
    <c:if test="${gather_order.r.borrow_duration!=0}">
	    <td width="17%" height="88">还款期限 （月）<br /><span class="sanbiao_xx_text">${gather_order.r.borrow_duration }</span></td>
    </c:if>
    <c:if test="${gather_order.r.borrow_duration==0}">
	    <td width="17%" height="88">还款期限 （天）<br /><span class="sanbiao_xx_text">${gather_order.r.borrow_duration_day }</span></td>
    </c:if>
  </tr>
  <tr>
    <td width="16%" height="35" style="font-size:14px;">保障方式</td>
    <td width="32%" height="35" align="left" style="font-size:14px;" class="td_bj">本金+利息&nbsp;<a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>详情参见<s>本金保障计划</s><img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    <td width="21%" height="35" align="left" style="font-size:14px;">提前还款费率</td>
    <td width="14%" height="35" align="left" style="font-size:14px;">1.00%</td>
    <td rowspan="2"></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">还款方式</td>
    
    <c:if test="${gather_order.r.borrow_duration>1}">
	    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx">按月还款/等额本息&nbsp;<a href="#"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>等额本息还款法是在还款期内，每月偿还同等数额的贷款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    </c:if>
    <c:if test="${gather_order.r.borrow_duration==0 or gather_order.r.borrow_duration==1}">
	    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx">到期一次性还本付息&nbsp;</td>
     </c:if>
    
    <td height="35" align="left" style="font-size:14px;">月还本息（元）</td>
    <td height="35" align="left" style="font-size:14px;">${monthly_principal_and_interest}</td>
  </tr>
  <tr class="tr_2">
     <td class="td_1">投标进度</td> 
     <td class="td_2" colspan="2">
     	<p class="a">
     		<span style="width: ${gather_order.r.gather_progress}%"></span>
     	</p>
     	<p class="b">${gather_order.r.gather_progress}%<img src="<%=__PUBLIC__ %>/images/loan_b_1.png" /></p>
     </td>
     <td colspan="2" class="td_3">剩余时间<span>${day}</span>天<span>${hour}</span>时<span>${minute}</span>分</td>
  </tr>
</table>
</h2>
    <h5 class="h5_3">
       <div class="div_1">剩余金额（元）</div>
       <input type="hidden" id="remain_money" value="${gather_order.r.remain_money}" />
       <div class="div_1"><span>￥${remain_money}</span>（${gather_order.r.remain_share}份）</div>
       <div class="div_1"><s id="money_can_use">账户余额&nbsp;0.00元</s><a href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></div>
       <div class="div_1 div_2"><input id="invest_money" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onblur="adjust_money();" value="请输入50的倍数"/><span>元</span></div>
       <div class="div_1 div_3" id="money_msg"></div>
       <script src="js/loginDialog.js" type="text/javascript"></script>
       <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
       <c:choose>
       		<c:when test="${sessionScope.user!=null}"> 
       			<input type="button" class="button_1" id="bid" onClick="open_confirm();" value="投  标"/>
       		</c:when>
       		<c:otherwise> <input type="button" class="button_1" id="bid" onClick="need_login_in()" value="投  标"/></c:otherwise>
       </c:choose>
      
       <div id="loginDiv1" class="loginDiv1"></div>
       
       <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
          <div id="confirm_bid" class="sanbiao_toubiao">             
             <input type="button" class="guanbi" onClick="closeme()" value="ｘ" />
             
             <div class="up">确认投资</div>
             <div class="con"><p>借款标题</p><span>${gather_order.r.borrow_title}</span></div>
             <div class="con"><p>借款用户</p><span style="color:#ed5050;">${gather_order.r.nickname}</span></div>
             <div class="con"><p>年利率</p><span>${gather_order.r.annulized_rate_int}%</span></div>
             <div class="con"><p>还款期限</p><span>${gather_order.r.borrow_duration}个月</span></div>
             <div class="con"><p>还款方式</p><span>按月还款/等额本息</span></div>
             <div class="con"><p>保障方式</p><span>本金+利息</span></div>
             <div class="con"><p>投资金额</p><span id="invest_money_c"></span></div>
             
             <div class="con"><p>验证码</p><input class="input_1" type="text" id="check_code" />
				 <img id="CheckCodeServlet" width="122" height="42" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay();" style="border: 1px; font-color: white;" />
			 </div>
             
             <div class="con con_1" id="check_msg"></div>
             <div class="con">如遇流标情况，投标期内所冻结的金额将在流标后解冻</div>
             <div class="con"><input class="checkbox_1" type="checkbox" id="checkbox" checked="checked"/><s>我已阅读并同意签署</s><a style="color:#ed5050;" href="#">《借款协议》</a></div>
             <!-- 判断验证码 -->
             	
             <!-- 判断验证码 -->
             <div class="con con_2"><a class="a" href="javascript:void(0);" onclick="confirm_elite_bid();">确定</a><input id="is_submitted" type="hidden" name="is_submitted" value="0" /><a href="javascript:void(0);" onClick="closeme();">取消</a></div>
             
		</div>
          
		<div id="success" style="display:none" class="sanbiao_toubiao" >
			<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic7.png" />投标成功</div>
			<div class="con_4" ><a onClick="closeme_refresh()">关闭</a></div>
		</div>
		
		<div id="fail" style="display:none" class="sanbiao_toubiao" >
			<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic8.png" />对不起，您所投金额大于该标所需金额！</div>
			<div class="con_4" ><a onClick="closeme()">关闭</a></div>
		</div>
	</div>
    </h5>    
  </div>
  <div class="sanbiao_xx_con" >
    <h2><p class="hover">标的详情</p><p onclick="get_bid_record();">投标记录</p></h2>
    <h3 style="display: block">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">用户信息</td>
    <td width="13%" height="45"><span style="float:left"><a href="#" style=" color:red">${gather_order.r.nickname }</a></span></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">用户名</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.nickname}</td>
    <td width="12%" height="30" class="kk_text">公司行业</td>
    <td width="17%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_trades}</td>
    <td width="13%" height="30" class="kk_text">收入范围</td>
    <td width="14%" height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.monthly_income}/月</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">年    龄</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.age}</td>
    <td height="30" class="kk_text">公司规模</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_size}</td>
    <td height="30" class="kk_text">房       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">学    历</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.highest_education}</td>
    <td height="30" class="kk_text">岗位职位</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.position}</td>
    <td height="30" class="kk_text">房       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.housing_mortgage}</td>
  </tr>
  <tr>
  <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">学    校</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.school}</td>
    <td height="30" class="kk_text">工作城市</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.company_city}</td>
    <td height="30" class="kk_text">车       产</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car}</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>    
    <td height="30" class="kk_text">婚    姻</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.marriage}</td>
    <td height="30" class="kk_text">工作时间</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.year_limit}</td>
    <td height="30" class="kk_text">车       贷</td>
    <td height="30" class="kk_text">${ borrower_bulk_standard_order_user_info.m.car_mortgage}</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="30" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">信用档案</td>
    <td width="13%" height="30"><span id="rating_color">${borrower_bulk_standard_order_user_info.m.credit_rating}</span></td>
    <td height="30" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">申请借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.loan_application }</td>
    <td height="30" class="kk_text">信用额度（元）</td>
    <td id="line_of_credit" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.line_of_credit }</td>
    <td height="30" class="kk_text">逾期金额（元）</td>
    <td id="overdue_amount" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_amount }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">成功借款（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.successful_loan }</td>
    <td height="30" class="kk_text">借款总额（元）</td>
    <td id="total_loan" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.total_loan }</td>
    <td height="30" class="kk_text">逾期次数（次）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.overdue_num }</td>
  </tr>
  <tr>
    <td height="30">&nbsp;</td>
    <td height="30" class="kk_text">还清笔数（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.pay_off_the_pen_number }</td>
    <td height="30" class="kk_text">待还本息（元）</td>
    <td id="repay" height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.repay }</td>
    <td height="30" class="kk_text">严重逾期（笔）</td>
    <td height="30" class="kk_text">${borrower_bulk_standard_order_user_info.m.seriously_overdue }</td>
  </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">审核状态</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="40" colspan="3" align="center" class="sanbiao_xx_kk3"><span class="kk_text">审核项目</span></td>
    <td height="40" colspan="4" align="center" class="sanbiao_xx_kk3">状态</td>
    </tr>
  <tr id="0_tr">
    <td id="_0" height="45" colspan="3" align="center" ><span class="kk_text">身份认证</span></td>
    <td	id="0" height="45" colspan="4" align="center"  ></td>
    </tr>
  <tr id="1_tr">
    <td id="_1" height="45" colspan="3" align="center" ><span class="kk_text">工作认证</span></td>
    <td id="1" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="2_tr">
    <td id="_2" height="45" colspan="3" align="center" ><span class="kk_text">信用报告</span></td>
    <td id="2" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr id="3_tr">
    <td id="_3" height="45" colspan="3" align="center" ><span class="kk_text">收入认证</span></td>
    <td id="3" height="45" colspan="4" align="center" ></td>
   </tr>
     <tr id="4_tr">
    <td id="_4" height="45" colspan="3" align="center" ><span class="kk_text">房产认证</span></td>
    <td id="4" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="5_tr">
    <td id="_5" height="45" colspan="3" align="center" ><span class="kk_text">车产认证</span></td>
    <td id="5" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="6_tr">
    <td id="_6" height="45" colspan="3" align="center" ><span class="kk_text">婚姻认证</span></td>
    <td id="6" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="7_tr">
    <td id="_7" height="45" colspan="3" align="center" ><span class="kk_text">学历认证</span></td>
    <td id="7" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="8_tr">
    <td id="_8" height="45" colspan="3" align="center" ><span class="kk_text">职称认证</span></td>
    <td id="8" height="45" colspan="4" align="center" ></td>
    </tr>
      <tr id="9_tr">
    <td id="_9" height="45" colspan="3" align="center" ><span class="kk_text">居住认证</span></td>
    <td id="9" height="45" colspan="4" align="center" ></td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td height="45" colspan="6" class="kk_text">&bull;&nbsp;&nbsp;联富金融及其合作机构将始终秉持客观公正的原则，严控风险，最大程度的尽力确保借入者信息的真实性，但不保证审核信息100%无误。<br/>
                                              &bull;&nbsp;&nbsp;借入者若长期逾期，其个人信息将被公布。</td>
    </tr>
  <tr>
    <td height="15" colspan="7" style="border-bottom:1px solid #e5e5e5">&nbsp;</td>
    </tr>
  <tr>
    <td height="25" colspan="7">&nbsp;</td>
    </tr>
  <tr>
    <td width="14%" height="45" align="center" style="font-size:18px;font-family:'Microsoft Yahei'; ">借款描述</td>
    <td width="13%" height="45"></td>
    <td height="45" colspan="5">&nbsp;</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    <td colspan="6" rowspan="2" class="kk_text" style="line-height:30px">${gather_order.r.describe }</td>
    </tr>
  <tr>
    <td height="45">&nbsp;</td>
    </tr>  
  
</table>
</h3>
     <h3 class="h3_2" style="display: none;">
     
     	<div align="right">
     		<span class="td_1" id="person_time">加入人次0人</span>&nbsp;&nbsp;&nbsp;<span class="td_1" id="sum_invest_money">投标总额0元</span>
     	</div>
        <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">

           <tr class="tr_1">
              <td>序号</td>
              <td>投标人</td>
              <td>投标金额</td>
              <td>投标时间</td>
           </tr>
           
           <%--
           <tr class="tr_2">
              <td>1</td>
              <td class="td_2">Jedzhou（U-A）</td>
              <td>3000.00元</td>
              <td>2014-11-07 13:33</td>
           </tr>
           --%>
           
        </table>
     </h3>
  </div>
  
</div>




<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__ %>/js/the_function.js"></script>
	<script type="text/javascript">

	//提示需要登录
	function need_login_in(){
		showMessage(["提示","请先登录"]);
	}
	function judge_user_type(){

		var url = "<%=__ROOT_PATH__%>/user/financial/financial/judge_borrower.html";
		$.ajax({
			url:url,
			type:'get',
			cache:false,
			success:function(data)
			{
				if(data=='1'){
					//alert("1");
					open_confirm();
				}else if(data == '2'){
					showMessage(["提示","对不起，您不是理财账户，不能投标"]);
				}else{
					showMessage(["提示","else..."]);
				}
			}
		},"json");	
	}
	var num = 0;
	$(function(){
		get_fund_info(${borrower_bulk_standard_order_user_info.m.line_of_credit },${borrower_bulk_standard_order_user_info.m.overdue_amount },
		${borrower_bulk_standard_order_user_info.m.total_loan },${borrower_bulk_standard_order_user_info.m.repay });
		get_money_can_use();
		change_color("${borrower_bulk_standard_order_user_info.m.credit_rating}","rating_color");
		var upload_info_status = [${borrower_bulk_standard_order_user_info.m.id_authenticate_status},${borrower_bulk_standard_order_user_info.m.work_authenticate_status},${borrower_bulk_standard_order_user_info.m.credit_authenticate_status},${borrower_bulk_standard_order_user_info.m.income_authenticate_status},${borrower_bulk_standard_order_user_info.m.housing_authenticate_status},${borrower_bulk_standard_order_user_info.m.car_authenticate_status},${borrower_bulk_standard_order_user_info.m.marriage_authenticate_status},${borrower_bulk_standard_order_user_info.m.education_authenticate_status},${borrower_bulk_standard_order_user_info.m.title_authenticate_status},${borrower_bulk_standard_order_user_info.m.living_authenticate_status}];
		for(var i = 0 ;i<upload_info_status.length;i++){
			if(upload_info_status[i]==1){
				$("#"+i).append("<img src='<%=__PUBLIC__%>/images/dui_cion.png' />");
				if(num % 2 != 0){
					$("#"+i).addClass("sanbiao_xx_kk1");
					$("#_"+i).addClass("sanbiao_xx_kk1");
				}else{
					$("#"+i).addClass("sanbiao_xx_kk2");
					$("#_"+i).addClass("sanbiao_xx_kk2");
				}
				num += 1;
			}else{
				$("#"+i+"_tr").hide();
			}
		}

	});

	$("#invest_money").focus(function(){
		$("#invest_money").val("");
	});
	
	function get_money_can_use(){
		$.get('<%=__ROOT_PATH__%>/user/financial/financial/get_money_can_use.html',
				{
			
				},
				function(data) //回传函数
				{
					$("#money_can_use").html("账户余额  "+data+"元");
				}, "json");//浏览器编码	
	}
	
	//把借款金额变成50的整数倍   
	function adjust_money(){		
		var pre_money = $("#invest_money").val();
		var later_money = Math.floor(pre_money/50)*50;
		$("#invest_money").val(later_money);	
		$.post('<%=__ROOT_PATH__%>/user/financial/financial/check_enough_money.html',
				{
					"invest_money" : later_money
				},
				function(data) //回传函数
				{
					if(data == '2'){
						$("#money_msg").html("账户余额不足");
						$("#bid").attr("disabled",true);
					}
					else if(data == '1'){
						$("#money_msg").html("");
						$("#bid").attr("disabled",false);
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");	//浏览器编码		
	}

	function verify_check_code(){
		var check_code = $("#check_code").val();
		var result;

		if(check_code == null || check_code == ""){
			$("#check_msg").html("请输入验证码");
			result = false;
		}		
		var url = "<%=__ROOT_PATH__%>/user/financial/financial/verify_check_code.html";
		$.get(url,
				{
					"check_code" : $("#check_code").val()
				},
				function(data) //回传函数
				{
					if(data==""){
						alert("正确");
						$("#check_msg").html(data);
						result = true;
					}else{
						$("#check_msg").html(data);
						result = false;
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码	
		return result;	
  	 }

	function confirm_elite_bid(){

		var $checkbox = $("#checkbox");
		var checkbox = $checkbox[0];    //DOM对象 

	
		
		var is_submitted = $("#is_submitted").val();
		if(is_submitted == 1){
			showMessage(["提示","系统处理中"]);
			return;
		}


		if(!checkbox.checked){
			$("#check_msg").html("请先同意签署借款协议");
			return;
		}
		
		var gather_order_id = $("#gather_order_id").val();
		var invest_money = $("#invest_money").val();
		var check_code = $("#check_code").val();
		
		try {
			$("#is_submitted").val(1);
			$.ajaxSetup({
		        async: false
		    });
			$.post('<%=__ROOT_PATH__%>/user/financial/financial/confirm_elite_bid.html',
					{
						"invest_money" : invest_money,
						"gather_order_id" : gather_order_id,
						"check_code" : check_code
					},
					function(data) //回传函数
					{
						$("#is_submitted").val(0);
						if(data == '1'){
							$("#check_msg").html("验证码错误");
						} else if(data == '2'){
							$("#check_msg").html("你的余额不足,请重新填写投资金额");
						}else if(data == '3'){
							$("#check_msg").html("系统错误");
						}else if(data == '4'){
							$("#check_msg").html("请求超时,请重新操作 ");
						}else if(data == '5'){
							$("#check_msg").html("该筹款单已经过期,不能投资");
						}else if(data == '6'){
							$("#check_msg").html("剩余的份额不足,不能投资");
						}else if(data == '7'){
							$("#check_msg").html("验证码错误");
						}else if(data == '8'){
							$("#check_msg").html("理财成功");
							$("#confirm_bid").hide();
							$("#success").show();
						}else if(data == '9'){
							$("#check_msg").html("理财失败");
						}else{
							$("#check_msg").html("...else");
						}
					}, "html","application/x-www-form-urlencoded; charset=utf-8");	//浏览器编码	
		} catch (e) {
			$("#is_submitted").val(0);
		}

		
        
			
	}

	function get_bid_record(){
		var url = "<%=__ROOT_PATH__%>/user/financial/financial/get_bid_record.html";
		var gather_order_id = $("#gather_order_id").val();
		$.ajax({
			url:url,
			type:'post',
			data:{
				"gather_order_id":gather_order_id
				},
			cache:false,
			success:function(data)
			{
				var jsObject = JSON.parse(data);
				$("#table1 tr:not(:first)").empty(); 
				var tr_str ='';
				for(var i=0;i<jsObject.bid_record.length;i++ ){

					var tr_class;
					if(i%2 == 0){
						tr_class = 'tr_2';
					}else{
						tr_class = 'tr_3';
					}

					tr_str += '<tr class="'+ tr_class +'">'
			              + '<td>'+(i+1)+'</td>'
			              + '<td class="td_2">'+ jsObject.bid_record[i].nickname +'</td>'
			              + '<td>'+ jsObject.bid_record[i].invest_money +'元</td>'
			              + '<td>' +( new Date((jsObject.bid_record[i].bid_time)).format("yyyy-MM-dd hh:mm:ss")) + '</td>'
			          + '</tr>';
					
				}
				$("#table1 tr:eq(0)").after(tr_str);
				$("#person_time").html("投标总额"+ jsObject.sum_invest_money +"元");
				$("#sum_invest_money").html("加入"+jsObject.person_time+"人次"	);
				
				//$("#asyncPageDiv").html(jsObject.asyncPageDiv);
			}
		},"json");
	}

	/**
  	 * 获取验证码：
  	 * @return
  	 */
  	  var path = "http://"+window.location.host;
  	function refresh_order_pay(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
			CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}

	

  	
	</script>

</body>
</html>
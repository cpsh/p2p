<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>${applicationScope.title}</title>
  </head>
  <body>
  
<%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>


<div class="sanbiao">
   <div class="calculator_side">
      <h1><a href="#">我要理财</a> > 理财计算器</h1>
  
      <div class="calculator_lc">
         <div class="up">理财计算器</div>
         <div class="up_1"><a class="hover">联富宝</a><a>散标投资</a></div>
         <div class="content_out" style="display:block;">
            <div class="content">
               <div class="con_1">
                  <label class="label_1">产品类型</label>
                  <dl class="select">
                     <dt id="type">联富宝 - (3个月)</dt>
                     <dd>
                        <ul>
                           <li><a>联富宝 - (3个月)</a></li>
                           <li><a>联富宝 - (6个月)</a></li>
                           <li><a>联富宝 - (12个月)</a></li>
                        </ul>
                     </dd>
                  </dl>
               </div>
               <div class="con_1">
                  <label class="label_1">投资金额</label>
                  <input id="invest_money_lfoll" class="text_1" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" onfocus="showP1();" onblur="hideP1();" />
                  <span class="span_1">元</span>
                  <p id="p1" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />投资金额为1000的整数倍</p>
               </div>
               <div class="con_2">
                  <input class="button_1" type="button" onclick="begin_calculate_lfoll_invest();" value="开始计算" />
               </div>            
            </div>
            <div class="up">收益描述</div>
            <div class="content_2">
               <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td>投资金额</td>
                     <td>预期收益</td>
                  </tr>
                  <tr class="tr_1">
                     <td id="invest_money_lfoll_1">￥0.00</td>
                     <td id="earnings_lfoll">￥0.00</td>
                  </tr>
               </table>
            </div>
         </div>
         
         <div class="content_out">
            <div class="content">
               <div class="con_1">
                  <label class="label_1">出借金额</label>
                  <input id="invest_money" class="text_1" onfocus="showP2();" onblur="hideP2();adjust_money();" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" type="text" />
                  <span class="span_1">元</span>
                  <p id="p2" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />出借金额为<br />50的整数倍</p>
               </div>
            
               <div class="con_1">
                  <label class="label_1">年化利率</label>
                  <input id="annulized_rate_int" class="text_1" onfocus="showP3();" onblur="hideP3();" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" type="text" />
                  <span class="span_1">%</span>
                  <p id="p3" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />利率范围<br />10%-24%</p>
               </div>
            
               <div class="con_1">
                  <label class="label_1">借款期限</label>
                  <input id="borrow_duration" class="text_1" onfocus="showP4();" onblur="hideP4();" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" type="text" />
                  <span class="span_1">个月</span>
                  <p id="p4" class="p_1"><img src="<%=__PUBLIC__%>/images/loan_1.png" />可填写1到36任意整数月数</p>
               </div>
            
               <div class="con_1">
                 <label class="label_1">还款方式</label>
                 <input  class="text_1" type="text" value="等额本息" readonly="true" />
               </div>   
               <!--          
               <div class="con_1">
                  <label class="label_1">&nbsp;</label>
                  <input class="checkbox_1" type="checkbox" />显示还款时间表
               </div>
                -->
            
               <div class="con_2">
                  <input class="button_1" type="button" onclick="begin_calculate();" value="开始计算" />
               </div>            
            </div>
            <div class="up">收益描述</div>
            <div class="content_2">
               <table class="table_2" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td width="25%">出借金额</td>
                     <td width="25%" id="invest_money_1">0.00元</td>
                     <td width="25%">应收利息</td>
                     <td width="25%" id="interest">0.00元</td>
                  </tr>
                  <tr>
                     <td width="25%">月收本息</td>
                     <td width="25%" id="pricipal_interest">0.00元</td>
                     <td width="25%">
                   <!--   您将在<span>1</span>个月后收回全部本息   -->
                     </td>
                     <td width="25%">&nbsp;</td>
                  </tr>
               </table>
            </div>
            
            <div class="up">本息回收时间表</div>
            <div class="content_2">
               <table id="table1" class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr class="tr_1">
                     <td class="td_1" width="20%">月份</td>
                     <td width="20%">月收本息</td>
                     <td width="20%">月收本金</td>
                     <td width="20%">月收利息</td>
                     <td width="20%">待收本金</td>
                  </tr>
                  <!-- 
                  <tr class="tr_2">
                     <td class="td_1" width="20%">1月</td>
                     <td width="20%">202.60</td>
                     <td width="20%">198.40</td>
                     <td width="20%">4.20</td>
                     <td width="20%">810.00</td>
                  </tr>
                  <tr class="tr_3">
                     <td class="td_1" width="20%">1月</td>
                     <td width="20%">202.60</td>
                     <td width="20%">198.40</td>
                     <td width="20%">4.20</td>
                     <td width="20%">810.00</td>
                  </tr>
                  -->
               </table>
            </div>
         
            <ol>
               <li><span>等额本息</span>，即借款人每月以相等的金额偿还借款本息，也是银行房贷等采用的方法。因计算中存在四舍五入，最后一期还款金额与之前略有不同。</li>
               <li><span>注</span>：因计算器采用了以50元为一份的计算方式，计算结果与2013年10月14日之前的债权存在偏差，具体情况以账户中显示的信息为准。</li>
            </ol>
         </div>      
      </div>      
      
   </div>
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>

	<script type="text/javascript">

	function adjust_money(){
		var invest_money = $("#invest_money").val();
		var adjust_money = Math.floor(invest_money / 50) * 50 ;
		$("#invest_money").val(adjust_money);
	}
	
	function begin_calculate(){
		var invest_money = $("#invest_money").val();
		var annulized_rate_int = $("#annulized_rate_int").val();
		var borrow_duration = $("#borrow_duration").val();
		if(invest_money == null || invest_money == ""){
			return ;
		}
		if(annulized_rate_int == null || annulized_rate_int == ""){
			return ;
		}
		if(annulized_rate_int < 10 || annulized_rate_int > 24){
			return ;
		}
		if(borrow_duration == null || borrow_duration == ""){
			return ;
		}
		if(borrow_duration < 1 || borrow_duration > 36){
			return ;
		}

		var url = "<%=__ROOT_PATH__%>/user/financial/financial/calculate_elite_bid_lend.html";
		
		$.ajax({
			url:url,
			type:'post',
			data:{
				"invest_money":invest_money,
				"annulized_rate_int":annulized_rate_int,
				"borrow_duration":borrow_duration
			},
			cache:false,
			success:function(data)
			{
				var jsObject = JSON.parse(data);

				$("#table1 tr:not(:first)").empty(); 
				
				var tr_str ='';
				for(var i=0;i<jsObject.repayment_plan_list.length;i++ ){

					//条纹样式的控制
					var tr_class;
					if(i%2 == 0){
						tr_class = 'tr_2';
					}else{
						tr_class = 'tr_3';
					}
					tr_str += 

						'<tr class="'+ tr_class +'">'
	                    + '<td class="td_1" width="20%">'+ (i+1) +'月</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].month_pricipal_and_interest.toFixed(2) +'</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].month_pricipal.toFixed(2) +'</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].month_interest.toFixed(2) +'</td>'
	                    + '<td width="20%">'+ jsObject.repayment_plan_list[i].remain_principal.toFixed(2) +'</td>'
	                 + '</tr>';
				}
				$("#table1 tr:eq(0)").after(tr_str);
				
				$("#invest_money_1").html(jsObject.invest_money+"元");
				$("#interest").html(jsObject.interest+"元");
				$("#pricipal_interest").html(jsObject.monthly_principal_and_interest+"元");
			}
		},"json");	
		
	}

	function begin_calculate_lfoll_invest(){

		var invest_money = $("#invest_money_lfoll").val();
		var type = $("#type").html();
		
		var url = "<%=__ROOT_PATH__%>/user/financial/financial/calculate_lfoll_invest.html";
		
		$.ajax({
			url:url,
			type:'post',
			data:{
				"invest_money":invest_money,
				"type":type
			},
			cache:false,
			success:function(data)
			{				
				var jsObject = JSON.parse(data);
				$("#invest_money_lfoll_1").html("￥"+jsObject.invest_money.toFixed(2));
				$("#earnings_lfoll").html("￥"+jsObject.earnings.toFixed(2));
			}
		},"json");	
	}

	$(function(){
		$(".select").each(function(){
			var s=$(this);
			var z=parseInt(s.css("z-index"));
			var dt=$(this).children("dt");
			var dd=$(this).children("dd");
			var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
			var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
			dt.click(function(){dd.is(":hidden")?_show():_hide();});
			dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
			$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
		})
	})

	function showP1(){
		$("#p1").show();
	}
	function hideP1(){
		$("#p1").hide();
	}
	function showP2(){
		$("#p2").show();
	}
	function hideP2(){
		$("#p2").hide();
	}
	function showP3(){
		$("#p3").show();
	}
	function hideP3(){
		$("#p3").hide();
	}
	function showP4(){
		$("#p4").show();
	}
	function hideP4(){
		$("#p4").hide();
	}
  	
	</script>

</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;联富金融
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq geren_ujh">
       <div class="left">
          <p>U计划已赚总额</p><span><s>0.00</s>元</span>
          <p class="p_1">账户总资产&nbsp;&nbsp;0.00元</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td>U计划加入总额</td>
                <td>收益再投资金额</td>
                <td>已提取金额</td>
             </tr>
             <tr class="tr_1">
                <td><span>0.00</span>元</td>
                <td><span>0.00</span>元</td>
                <td><span>0.00</span>元</td>
             </tr>
             <tr>
                <td class="td_1" colspan="3">&nbsp;&nbsp;&nbsp;原优选理财计划照常运行，规则保持不变。<a href="#">查看详情</a></td>
             </tr>
          </table>
       </div>    
    </div> 
    <div class="geren_ujh_head"><span><a class="hover" href="#">持有中<s>0</s></a><a href="#">退出中<s>0</s></a><a href="#">已退出<s>0</s></a><a href="#">预定中<s>0</s></a></span></div>
    <div class="geren_ujh_con" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td colspan="6" class="td_1"><a href="#">全部</a><a href="#">U计划A</a><a href="#">U计划B</a><a href="#">U计划C</a><a href="#">优选理财计划</a></td>
          </tr>
          <tr class="tr_1">
             <td>计划名称</td>
             <td>预期年化收益</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>投标次数</td>
             <td>状态</td>
          </tr>
          <tr class="tr_2">
             <td>计划名称</td>
             <td>预期年化收益</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>投标次数</td>
             <td>状态</td>
          </tr>
          <tr class="tr_3">
             <td>计划名称</td>
             <td>预期年化收益</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>投标次数</td>
             <td>状态</td>
          </tr>
          <tr class="tr_2">
             <td>计划名称</td>
             <td>预期年化收益</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>投标次数</td>
             <td>状态</td>
          </tr>
          <tr>
             <td colspan="6" class="td_2">没有持有中的U计划</td>
          </tr>
       </table>
    </div>       
    <div class="geren_ujh_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">          
          <tr class="tr_1">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>退出前已提取金额</td>
             <td>退出中已转出金额</td>
             <td>待转出金额</td>
             <td>剩余债权价值</td>
             <td>转让中的债权</td>
             <td>无法转让的债权</td>
          </tr>
          <tr class="tr_2">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>退出前已提取金额</td>
             <td>退出中已转出金额</td>
             <td>待转出金额</td>
             <td>剩余债权价值</td>
             <td>转让中的债权</td>
             <td>无法转让的债权</td>
          </tr>
          <tr class="tr_3">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>退出前已提取金额</td>
             <td>退出中已转出金额</td>
             <td>待转出金额</td>
             <td>剩余债权价值</td>
             <td>转让中的债权</td>
             <td>无法转让的债权</td>
          </tr>
          <tr>
             <td colspan="8" class="td_2">没有退出中的U计划</td>
          </tr>
       </table>
    </div>
    <div class="geren_ujh_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">          
          <tr class="tr_1">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>预期年化收益</td>
             <td>投资债权数</td>
             <td>完成时间</td>
             <td>退出途径</td>
          </tr>
          <tr class="tr_2">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>预期年化收益</td>
             <td>投资债权数</td>
             <td>完成时间</td>
             <td>退出途径</td>
          </tr>
          <tr class="tr_3">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>预期年化收益</td>
             <td>投资债权数</td>
             <td>完成时间</td>
             <td>退出途径</td>
          </tr>
          <tr>
             <td colspan="8" class="td_2">没有已退出的U计划</td>
          </tr>
       </table>
    </div>
    <div class="geren_ujh_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">          
          <tr class="tr_1">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已付定金</td>
             <td>待付金额</td>
             <td>支付截止时间</td>
             <td>状态</td>
          </tr>
          <tr class="tr_2">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已付定金</td>
             <td>待付金额</td>
             <td>支付截止时间</td>
             <td>状态</td>
          </tr>
          <tr class="tr_3">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已付定金</td>
             <td>待付金额</td>
             <td>支付截止时间</td>
             <td>状态</td>
          </tr>
          <tr>
             <td colspan="8" class="td_2">没有预定中的U计划</td>
          </tr>
       </table>
    </div>
    
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">借款管理</a>&nbsp;<span>></span>&nbsp;我的借款
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq geren_ujh geren_wdlk">
       <div class="left">
          <p>借款总金额</p><span><s>0.00</s>元</span>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>逾期金额</td>
                <td>待还金额</td>
                <td>近30天应还金额</td>
             </tr>
             <tr>
                <td><span>0.00</span>元</td>
                <td><span>0.00</span>元</td>
                <td><span>0.00</span>元</td>
             </tr>
             <tr>
                <td class="td_1" colspan="3">&nbsp;&nbsp;&nbsp;您最近10天内有 0笔 借款须归还，总额 0.00元</td>
             </tr>
          </table>
       </div>    
    </div> 
    <div class="geren_wdzq_head"><span><a class="hover" href="#">还款中借款</a><a href="#">已还清借款</a></span></div>    
    <div class="geren_wdzq_con" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>借款标题</td>
             <td>金额</td>
             <td>年利率</td>
             <td>期限</td>
             <td>还款总额</td>
             <td>还清日期</td>
             <td>合同</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有记录</td>
          </tr>
       </table>
    </div>  
    <div class="geren_wdzq_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>借款标题</td>
             <td>金额</td>
             <td>年利率</td>
             <td>期限</td>
             <td>还款总额</td>
             <td>还清日期</td>
             <td>合同</td>
          </tr>
          <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
          </tr>
          <tr>
             <td colspan="7" class="td_1">没有记录</td>
          </tr>
       </table>
    </div> 
           
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


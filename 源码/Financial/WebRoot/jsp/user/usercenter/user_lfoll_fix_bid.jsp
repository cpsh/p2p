<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
 <meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">  

<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>
 <script type="text/javascript" src="<%=__PUBLIC__ %>/js/lfoll_invest.js"></script>
 <script type="text/javascript" src="<%=__PUBLIC__ %>/js/fix_bid_order.js"></script>
 <link type="text/css" rel="stylesheet" href="<%=__PUBLIC__ %>/style/fix_bid_order_payment.css" />
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">用户中心</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;我的联富宝
    </div>
     <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
     <div id="loginDiv1" class="loginDiv1"></div>
     <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
        <div class="order_payment">
           <input type="button" class="guanbi" onclick="close_order_pay()" value="X" />
           <input type="hidden" id="order_id" value=""/>
           <div class="up" style="text-align: center">确认支付</div>
           <div class="con"><p>联富宝名称</p><span id="bid_name"></span></div>
           <div class="con"><p>投资人：</p><span style="color:#92b1bd;" id="nick_name"></span></div>
           <div class="con"><p>年利率</p><span id="rate"></span></div>
           <div class="con"><p>期限</p><span id="close_period"></span></div>
           <div class="con"><p>保障方式</p><span>本金+利息</span></div>
           <div class="con"><p>待付金额</p><span id="wait_pay_amount"></span></div>
           <div class="con"><p>可用RMB</p><span id="can_used_cny"></span></div>
           <div class="con"><p>验证码</p><input class="input_1" type="text" id="verificte_code" />
	           <img id="CheckCodeServlet" width="122" height="42" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay();" style="border: 1px; font-color: white; margin-left: 20px" />
	       </div>
           <div class="con con_1" id="error_msg"></div>
           <div class="con">如遇流标情况，投标期内所冻结的金额将在流标后解冻</div>
           <div class="con"><input class="checkbox_1" type="checkbox" id="check_box" checked="checked"/><s>我已阅读并同意签署</s><a style="color:#92b1bd;" href="#">《借款协议》</a></div>
           <div class="con con_2"><a class="a" id="submit_pay_order">确定</a><a style="background:#dbdbdb; color:#68696e; border-radius:15px" onclick="close_order_pay();">取消</a></div>
        </div>
     </div>
     <div id="loginDiv3" class="loginDiv3"></div>
     <div id="loginDiv4" class="loginDiv4" style=" z-index:10005; display:none;">
        <div class="exit_fix_bid">
           <input type="button" class="guanbi" onclick="close_div2_div3()" value="X" />
           <input type="hidden" id="order_id" value=""/>
           <div class="up" style="text-align: center">确认提前退出</div>
           <div class="con"><p>联富宝期数</p><span id="exit_bid_name"></span></div>
           <div class="con"><p>投资金额</p><span id="exit_bid_money"></span></div>
           <div class="con"><p>预计回收金额</p><span id="exit_back_money"></span></div>
           <div class="con"><p>退出费用</p><span id="exie_fee"></span></div>
            <div class="con"><p>提现密码 ：</p><input class="input_1" type="password" id="money_pwd" value="" /></div>
           <div class="con"><p>验证码 ：</p><input class="input_1" type="text" id="exit_verificte_code" />
	           <img id="CheckCodeServlet_2" width="122" height="42" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay_div2();" style="border: 1px; font-color: white; margin-left: 20px" />
	       </div>
           <div class="con con_1" id="exit_error_msg"></div>
           <div class="con con_2"><a class="a" id="submit_exit_fix_bid">确定</a><a style="background:#dbdbdb; color:#68696e; border-radius:15px" onclick="close_div2_div3();">取消</a></div>
        </div>
     </div>
    <div class="geren_jyjl geren_wdzq geren_ujh">
       <div class="left">
          <p>联富宝已赚总额</p><span><s>${totail_in_come}</s>元</span>
          <%--<p class="p_1">账户总资产&nbsp;&nbsp;0.00元</p> --%>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td>联富宝加入总额</td>
             </tr>
             <tr class="tr_1">
                <td>
                	<span>
                		<c:if test="${empty join_money}">0.00</c:if>
                		<c:if test="${not empty join_money}">${join_money}</c:if>
                	</span>元
                </td>
              </tr>
            </table>
       </div>    
    </div> 
    <div class="geren_ujh_head">
    	<span>
    		<a class="hover" href="javascript:void(0);" onclick="find_fix_bid_hold(1);">持有中<s>${hold_num}</s></a>
    		<a href="javascript:void(0);" onclick="find_fix_bid_order_list();">预定中<s>${fix_bid_order_num }</s></a>
    		<a href="javascript:void(0);" onclick="find_fix_bid_exited();">已退出<s>${exit_num }</s></a>
    		
    	</span>
    </div>
    <div class="geren_ujh_con" style="display:block">
    			<a href="javascript:void(0);" id="lfoll_fix_bid_all">全部</a>
             	<a href="javascript:void(0);" id="lfoll_fix_bid_l03">联富宝L03</a>
             	<a href="javascript:void(0);" id="lfoll_fix_bid_l06">联富宝L06</a>
             	<a href="javascript:void(0);" id="lfoll_fix_bid_l12">联富宝L12</a>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="fix_bid_hold_all">
         <tr class="tr_1" id="fix_bid_hold_all_first_tr">
             <td>计划名称</td>
             <td>预期年化收益</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>投标次数</td>
             <td>状态</td>
             <td>是否退出</td>
          </tr>
          <%--
          	<c:if test="${not empty fix_bid_user_hold_list}">
          		<c:forEach items="${fix_bid_user_hold_list}" var="fix_bid_user_hold">
          			 <tr class="tr_2">
			             <td>${fix_bid_user_hold.m.bid_name}</td>
			             <td>${fix_bid_user_hold.m.annual_earning * 100}%</td>
			             <td>${fix_bid_user_hold.m.bid_money}</td>
			             <td>${fix_bid_user_hold.m.earned_incom}</td>
			             <td>${fix_bid_user_hold.m.bid_num}</td>
			             <td>
			             	<c:if test="${fix_bid_user_hold.m.state == 1}">持有中</c:if>
			             	<c:if test="${fix_bid_user_hold.m.state == 0}">已退出</c:if>
			             </td>
			             <td><input type="button" onclick="alert_exit_fix_bid_div();" value="退出联富宝" /></td>
			          </tr>
          		</c:forEach>
          	</c:if>
          	<c:if test="${empty fix_bid_user_hold_list}">
	          	<tr>
	           		<td colspan="6" class="td_2">${tips}</td>
	          	</tr>
          	</c:if>
       --%></table>
    </div>
   
    <div class="geren_ujh_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="order_table">          
          <tr class="tr_1" id="order_table_first_tr">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已付定金</td>
             <td>待付金额</td>
             <td>支付截止时间</td>
             <td>状态</td>
          </tr>
       </table>
    </div>
     <div class="geren_ujh_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="exit_table">          
          <tr class="tr_1" id="exit_table_first_tr">
             <td>计划名称</td>
             <td>加入金额</td>
             <td>已获收益</td>
             <td>预期年化收益</td>
             <td>退出类型</td>
             <td>退出时间</td>
          </tr>
       </table>
    </div>
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>
</body>
</html>

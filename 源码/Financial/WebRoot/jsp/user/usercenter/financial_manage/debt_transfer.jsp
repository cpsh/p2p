<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
    <%--<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
    <link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
    --%><link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
<script src="<%=__PUBLIC__%>/js/debt_transfer.js"></script>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;债权转让
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq geren_zqzr">
       <div class="left">
          <p>债权转让盈亏&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>债权转让盈亏 = 债权折溢价交易盈亏 - 债权交易费用<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>
          </p><span><s>${total_debt_discount_less}</s>元</span>
          <p class="p_1">转出中的份额&nbsp;&nbsp;${transfer_share_remainder}份</p>
          <p class="p_1">转出中的数量&nbsp;&nbsp;${debt_out_count}个</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>成功转入金额</td>
                <td>债权转入盈亏</td>
                <td>已转入债权数量</td>
             </tr>
             <tr class="tr_2">
                <td>${total_transfer_in_all_money}元</td>
                <td>${total_should_count_interest}元</td>
                <td>${total_transfer_in_share}份</td>
             </tr>
             <tr class="tr_1">
                <td>成功转出金额</td>
                <td>债权转出盈亏</td>
                <td>已转出债权数量</td>
             </tr>
             <tr>
                <td>${total_transfer_out_all_money}元</td>
                <td>${total_debt_discount_less}元</td>
                <td>${total_transfer_out_share}份</td>
             </tr>
          </table>
       </div>    
    </div> 
    
    <div class="geren_zqzr_con">
       <p>债权转让中常遇到的问题</p>
       <span>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr_js.html">什么是债权转让？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">什么是债权价值？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">什么情况下债权价值会变？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">如何转出债权？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html">债权转让如何收费？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">什么情况下债权会被锁定？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzrrhmr.html">如何转入债权？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">什么样的债权可以转让？</a>
          <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html">什么情况下购买债权会失败？</a>
       </span>
    </div>
    
    <div class="geren_wdzq_head">
	    <span>
	    	<a class="hover" href="javascript:void(0);" id="transfering_debt">转让中的债权</a>
	    	<a href="javascript:void(0);" id="can_transfered_debt">可转出的债权</a>
	    	<a href="javascript:void(0);" id="already_transfered_out_debt">已转出的债权</a>
	    	<a href="javascript:void(0);" id="already_transfered_in_debt">已转入的债权</a>
	    </span>
    </div>
    <div class="geren_wdzq_con geren_zqzr_con2" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="debt_transfering_table">
          <tr id="debt_transfering_table_first_tr">
             <td width="15%">转让ID</td>
             <td width="15%">债权ID</td>
             <td width="11%">剩余期数</td>
             <td width="10%">年利率</td>
             <td width="12%" class="td_a"><div>债权价值</div><div class="a"><a><img src="<%=__PUBLIC__%>/images/loan_3.png" /></a><span>根据定价公式计算出的债权当前的公允价值，会根据债权的交易日期、状态等进行更新，具体公式请<a href="#">查看详情</a><img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></div></td>
             <td width="11%">转让价格</td>
             <td width="11%">转让系数</td>
             <td width="11%">剩余份数</td>
             <td width="4%">&nbsp;</td>
          </tr>
          <c:if test="${not empty list}">
	          <c:forEach items="${list}" var="record" varStatus="status">
	          	<c:if test="${(status.count) % 2 != 0}">
	          		<tr class="tr_1" id="debt_transfering_${status.count}">
	          	</c:if>
		         <c:if test="${(status.count) % 2 == 0}">
	          		<tr id="debt_transfering_${status.count}">
	          	</c:if>
		             <td><span>${record.r.id}</span></td>
		             <td>
		             	<c:if test="${record.r.borrow_type == 1}">
		             		<img src="<%=__PUBLIC__%>/images/icon1.jpg" />
		             	</c:if>
		             	<c:if test="${record.r.borrow_type == 2}">
		             		<img src="<%=__PUBLIC__%>/images/icon1_2.jpg" />
		             	</c:if>
		             	<c:if test="${record.r.borrow_type == 3}">
		             		<img src="<%=__PUBLIC__%>/images/icon1_3.jpg" />
		             	</c:if>
		             	<span>${record.r.gather_money_order_id}</span>
		             </td>
		             <td>${record.r.remain_periods}</td>
		             <td>${record.r.annulized_rate_int}</td>
		             <td>${record.r.current_creditor_right_value}</td>
		             <td>${record.r.transfer_price}</td>
		             <td>${record.r.transfer_factor}</td>
		             <td>${record.r.remain_share}</td>
		             <td><a href="javascript:void(0);" onclick="console_transfering_debt('debt_transfering_${status.count}','${record.r.id}','${record.r.gather_money_order_id}')">撤销</a></td>
		          </tr>
	          </c:forEach>
          </c:if>
          <c:if test="${empty list}">
          	<tr><td colspan="9" class="td_1">没有转让中的债权</td></tr>
          </c:if>
          <%--
          <tr class="tr_1">
             <td><span>207594</span></td>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>6/6</td>
             <td>22.80%</td>
             <td>50.40/份</td>
             <td>50.40/份</td>
             <td>100.00%</td>
             <td>1/1份</td>
             <td><a href="#">撤销</a></td>
          </tr>
          <tr>
             <td><span>207594</span></td>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>6/6</td>
             <td>22.80%</td>
             <td>50.40/份</td>
             <td>50.40/份</td>
             <td>100.00%</td>
             <td>1/1份</td>
             <td><a href="#">撤销</a></td>
          </tr>
       --%>
       </table>
    </div> 
    <div class="geren_wdzq_con geren_zqzr_con2">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="can_transfer_debt_table">
          <tr id="can_transfer_debt_first_tr">
             <td width="3%">&nbsp;</td>
             <td width="18%">债权ID</td>
             <td width="11%">剩余期数</td>
             <td width="14%">下个还款日</td>
             <td width="12%">年利率</td>             
             <td width="14%">待收本息</td>
             <td width="12%">债权价值</td>
             <td width="12%">可转份数</td>
             <td width="4%">&nbsp;</td>
          </tr>
          
          <%--<tr class="tr_1">
             <td><input type="checkbox" /></td>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>16/36</td>
             <td>2015-05-02</td>
             <td>13.50%</td>
             <td>105.02</td>
             <td>98.60</td>
             <td>4份</td>
             <script src="js/loginDialog_2.js" type="text/javascript"></script>
             <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
             <td><a onClick="openme(1,2)">转让</a></td>
             <div id="1" class="loginDiv1"></div>
             <div id="2" class="loginDiv2" style=" z-index:10005; display:none;">
             <div class="geren_wdzq_div">                
                <input type="button" class="guanbi" onClick="closeme(1,2)" value="ｘ" />
                
                <div class="up"><span>债权转出</span>ID&nbsp;123425</div>
                <div class="up_con">
                   <label class="label_1">原始投资额</label>
                   <p class="P1">100元（<span>2份</span>）</p>
                </div>
                <div class="up_con">
                   <label class="label_1">已收利息</label>
                   <p class="P2"><span>0.00元</span><span>利率</span><span>0.00%</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">可转份数</label>
                   <p class="P2"><span>2份</span><span>剩余期数</span><span>24个月</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让份数</label>
                   <input class="text_1" type="text" />
                </div>
                <div class="up_con">
                   <label class="label_1">当前债权价值</label>
                   <p class="P2"><span>35.12元/份</span><span>当前待收本息</span><span>49393元/份</span></p>
                </div>
                <div class="up_con up_con_2">一般为转让时的待回收本金与应计利息之和。<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">转让系数</label>
                   <select class="select_1">
                      <option>100%</option>
                      <option>10%</option>
                      <option>20%</option>
                   </select>
                </div>
                <div class="up_con up_con_2">此转让系数紧作用于债权待收本金部分</div>
                <div class="up_con">
                   <label class="label_1">转让价格</label>
                   <p class="P1">38.12元/份</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让总价格</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让费用</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">预计收入金额</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con up_con_2">由于债权价值会发生变动，最终收入金额可能发生变动<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">提现密码</label>
                   <input class="text_1" type="password"/>
                   <a class="a_1">忘记密码？</a>
                </div>
                <div class="up_con up_con_3">
                   <input class="checkbox_1" type="checkbox" />
                   <span>我已阅读并同意签署</span>
                   <a href="#">《债权转让及受让协议》</a>
                </div>
                <div class="up_con up_con_4"><a class="a" href="#">转让</a><a href="#">取消</a></div>
                <!--
                <div class="up_con up_con_5">
                   <img src="<%=__PUBLIC__%>/images/registration_pic8.png" />
                   <span>密码输入错误，您还剩余4次输入机会，请重新输入。</span>
                </div>
                -->
             </div>
             </div>
          </tr>
          <tr>
             <td><input type="checkbox" /></td>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>16/36</td>
             <td>2015-05-02</td>
             <td>13.50%</td>
             <td>105.02</td>
             <td>98.60</td>
             <td>4份</td>             
             <td><a onClick="openme(3,4)">转让</a></td>   
             <div id="3" class="loginDiv1"></div> 
             <div id="4" class="loginDiv2" style=" z-index:10005; display:none;">
             <div class="geren_wdzq_div">                
                <input type="button" class="guanbi" onClick="closeme(3,4)" value="ｘ" />
                
                <div class="up"><span>债权转出</span>ID&nbsp;123425</div>
                <div class="up_con">
                   <label class="label_1">原始投资额</label>
                   <p class="P1">100元（<span>2份</span>）</p>
                </div>
                <div class="up_con">
                   <label class="label_1">已收利息</label>
                   <p class="P2"><span>0.00元</span><span>利率</span><span>0.00%</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">可转份数</label>
                   <p class="P2"><span>2份</span><span>剩余期数</span><span>24个月</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让份数</label>
                   <input class="text_1" type="text" />
                </div>
                <div class="up_con">
                   <label class="label_1">当前债权价值</label>
                   <p class="P2"><span>35.12元/份</span><span>当前待收本息</span><span>49393元/份</span></p>
                </div>
                <div class="up_con up_con_2">一般为转让时的待回收本金与应计利息之和。<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">转让系数</label>
                   <select class="select_1">
                      <option>100%</option>
                      <option>10%</option>
                      <option>20%</option>
                   </select>
                </div>
                <div class="up_con up_con_2">此转让系数紧作用于债权待收本金部分</div>
                <div class="up_con">
                   <label class="label_1">转让价格</label>
                   <p class="P1">38.12元/份</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让总价格</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让费用</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">预计收入金额</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con up_con_2">由于债权价值会发生变动，最终收入金额可能发生变动<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">提现密码</label>
                   <input class="text_1" type="password"/>
                   <a class="a_1">忘记密码？</a>
                </div>
                <div class="up_con up_con_3">
                   <input class="checkbox_1" type="checkbox" />
                   <span>我已阅读并同意签署</span>
                   <a href="#">《债权转让及受让协议》</a>
                </div>
                <div class="up_con up_con_4"><a class="a" href="#">转让</a><a href="#">取消</a></div>
                <!--
                <div class="up_con up_con_5">
                   <img src="<%=__PUBLIC__%>/images/registration_pic8.png" />
                   <span>密码输入错误，您还剩余4次输入机会，请重新输入。</span>
                </div>
                -->
             </div>
             </div>
          </tr>
          <tr>
             <td colspan="9" class="td_1">没有可转让的债权</td>
          </tr>
          --%>
       </table>
       	<div id="1" class="loginDiv1"></div>
		<div id="2" class="loginDiv2" style=" z-index:10005; display:none;">     
             <div id="confirm_transfer" class="geren_wdzq_div">  
                <input type="button" class="guanbi" onClick="closeme(1,2)" value="ｘ" />
                
                <input type="hidden" id="hidden_creditor_right_hold_id" value="" />
                <input type="hidden" id="hidden_gather_money_order_id" value="" />
                <input type="hidden" id="hidden_creditor_right_value" value="" />
                <input type="hidden" id="hidden_can_transfer_share" value="" />
                
                <div class="up"><span>债权转出</span>ID&nbsp;<span id="debt_id">001</span></div>
                <div class="up_con">
                   <label class="label_1">原始投资额</label>
                   <p class="P1"><span id="invest_money">0.00元</span>（<span id="invest_share">0份</span>）</p>
                </div>
                <div class="up_con">
                   <label class="label_1">已收利息</label>
                   <p class="P2"><span>0.00元</span><span>利率</span><span id="interest_rate">%</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">可转份数</label>
                   <p class="P2"><span id="can_transfer_share">0份</span><span>剩余期数</span><span id="remain_period">24个月</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让份数</label>
                   <input class="text_1" id="transfer_share" type="text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" onblur="count_transfer_price();check_share();" />
                </div>
                <div class="up_con">
                   <label class="label_1">当前债权价值</label>
                   <p class="P2"><span id="debt_value">0.0元/份</span>
                   <span>当前待收本息</span>
                   <span id="daishou_benxi_per_share">0.00元/份</span></p>
                </div>
                <div class="up_con up_con_2">一般为转让时的待回收本金与应计利息之和。<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">转让系数</label>
                   <select id="transfer_factor" class="select_1" onchange="count_transfer_price();">
                      <option value="1">100%</option>
                      <option value="0.9">90%</option>
                      <option value="0.8">80%</option>
                      <option value="0.7">70%</option>
                      <option value="0.6">60%</option>
                      <option value="0.5">50%</option>
                      <option value="0.4">40%</option>
                      <option value="0.3">30%</option>
                      <option value="0.2">20%</option>
                      <option value="0.1">10%</option>
                   </select>
                </div>
                <div class="up_con up_con_2">此转让系数仅作用于债权待收本金部分</div>
                <div class="up_con">
                   <label class="label_1">转让价格</label>
                   <p class="P1" id="transfer_price_per_share">0.00</p>元/份
                </div>
                <div class="up_con">
                   <label class="label_1">转让总价格</label>
                   <p class="P1" id="transfer_price">0.00</p>元
                </div>
                <div class="up_con">
                   <label class="label_1">转让费用</label>
                   <p class="P1" id="transfer_fee">0.00</p>元
                </div>
                <div class="up_con">
                   <label class="label_1">预计收入金额</label>
                   <p class="P1" id="pre_recieve_money">0.00</p>元
                </div>
                <div class="up_con up_con_2">由于债权价值会发生变动，最终收入金额可能发生变动<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">提现密码</label>
                   <input class="text_1" id="money_password" type="password"/>
                   <a class="a_1">忘记密码？</a>
                </div>
                <div class="up_con up_con_3">
                   <input class="checkbox_1" type="checkbox" id="checkbox_1" />
                   <span>我已阅读并同意签署</span>
                   <a href="#">《债权转让及受让协议》</a>
                </div>
                <div class="up_con up_con_4"><a class="a" href="javascript:void(0);" onclick="confirm_debt_transfer();" >转让</a><a href="javascript:void(0);" onclick="closeme();">取消</a></div>
                
                <div id="tip" class="up_con up_con_5" style="display:none">
                   <img src="<%=__PUBLIC__%>/images/registration_pic8.png" />
                   <span id="error_msg">密码输入错误，您还剩余4次输入机会，请重新输入。</span>
                </div>                
             </div>
             <div id="success" style="display:none" class="geren_wdzq_div" >
			       	<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic7.png" />挂出转出单成功</div>
			 		<div class="con_4" ><a onClick="closeme_refresh(1,2)">关闭</a></div>
		        </div> 
          </div>
          <%-- 
       <div class="down">
       	<input type="checkbox" /><span>全选&nbsp;&nbsp;&nbsp;&nbsp;选中 0笔 债权，所选债权的当前总价值 0.00元</span>
       	<a href="#">批量转让</a>
       </div>
       --%>
    </div>  
    <div class="geren_wdzq_con geren_zqzr_con2">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="transfered_out_debt_table">
          <tr id="transfered_out_debt_table_first_tr">
             <td width="18%">债权ID</td>
             <td width="10%">成交份数</td>
             <td width="18%">转出时债权总价值</td>
             <td width="18%">转出时总成交金额</td>             
             <td width="10%">实际收入</td>
             <td width="11%">交易费用</td>
             <td width="11%">转让盈亏</td>
             <td width="4%">&nbsp;</td>
          </tr><%--
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(6)">明细</a></td>
          </tr>
          <tr id="submenu6" class="tr_1" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="14%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="11%">转让价格</td>
                     <td width="11%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="11%">实际收入</td>
                     <td width="8%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="4%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
                   <tr>
                     <td><span>789564</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(7)">明细</a></td>
          </tr>
          <tr id="submenu7" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="14%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="11%">转让价格</td>
                     <td width="11%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="11%">实际收入</td>
                     <td width="8%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="4%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          <tr>
             <td colspan="8" class="td_1">没有已转出的债权</td>
          </tr>
       --%></table>
    </div>  
    <div class="geren_wdzq_con geren_zqzr_con2">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" id="transfered_in_debt_table">
          <tr id="transfered_in_debt_table_first_tr">
             <td width="18%">债权ID</td>
             <td width="11%">剩余期数</td>
             <td width="10%">年利率</td>
             <td width="16%">转入时债权价值</td>             
             <td width="11%">转入份数</td>
             <td width="10%">交易金额</td>
             <td width="7%">盈亏</td>
             <td width="13%">转入时间</td>
             <td width="4%">&nbsp;</td>
          </tr><%--
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>15个月</td>
             <td>11.40%</td>
             <td>42.12/份</td>
             <td>1</td>
             <td>42.12</td>
             <td>0.00</td>
             <td>2014-01-12</td>
             <td><a href="#">合同</a></td>
          </tr>
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>15个月</td>
             <td>11.40%</td>
             <td>42.12/份</td>
             <td>1</td>
             <td>42.12</td>
             <td>0.00</td>
             <td>2014-01-12</td>
             <td><a href="#">合同</a></td>
          </tr>
          <tr>
             <td colspan="9" class="td_1">没有已转入的债权</td>
          </tr>
       --%></table>
    </div>  
    
  </div>    
</div>
<div class="bottom"></div>
<script type="text/javascript">
function check_share(){

	 var transfer_share = $("#transfer_share").val();

	 if(transfer_share == 0 || transfer_share == null){
		$("#error_msg").html("请输入正确的份数");
		$("#tip").css({"display":"block"});
	}else{
		$("#error_msg").html("");
		$("#tip").css({"display":"none"});
	}
}

function count_transfer_price(){
	//转让价格
	var transfer_price_per_share = $("#hidden_creditor_right_value").val() * $("#transfer_factor").val();
	//装让份数
	var transfer_share = $("#transfer_share").val();
	//可转份数
	var can_transfer_share = $("#hidden_can_transfer_share").val();
	
	if(transfer_share == null || transfer_share == ""){
		return ;
	}

	if(transfer_share > can_transfer_share){
		$("#transfer_share").val(can_transfer_share);
	}
	
	//转让总价格 = 转让价格 * 份数
	var transfer_price = transfer_price_per_share * transfer_share;
	//转让费用 = 转让总价格 * 0.005
	var transfer_fee = transfer_price * 0.005;

	//预计收入金额
	var pre_recieve_money = transfer_price - transfer_fee;
	
	$("#transfer_price_per_share").html(transfer_price_per_share.toFixed(2));
	$("#transfer_price").html(transfer_price.toFixed(2));
	$("#transfer_fee").html(transfer_fee.toFixed(2));

	$("#pre_recieve_money").html(pre_recieve_money.toFixed(2));
	
}

function confirm_debt_transfer(){

	var url = "<%=__ROOT_PATH__%>/user/financial/transfer/confirm_debt_transfer.html";

	/*
		final long creditor_right_hold_id = getParameterToLong("creditor_right_hold_id");//债权持有表的id
		final long gather_money_order_id = getParameterToLong("gather_money_order_id");//债权id（筹款单id）
		final BigDecimal creditor_right_value = new BigDecimal(getParameterToDouble("creditor_right_value")); // 债权价值
		
		final BigDecimal transfer_factor = new BigDecimal(getParameterToDouble("transfer_factor"));//转让系数
		final BigDecimal transfer_price = new BigDecimal(getParameterToDouble("transfer_price"));//转让价格(每份)
		final int transfer_share = getParameterToInt("transfer_share");  //转让份数
		
		//转让总价格
		//final BigDecimal transfer_total_value = transfer_price.multiply(new BigDecimal(transfer_share));
		final BigDecimal transfer_manage_fee = new BigDecimal(getParameterToDouble("transfer_manage_fee"));
	 */

	 var creditor_right_hold_id = $("#hidden_creditor_right_hold_id").val();
	 var gather_money_order_id = $("#hidden_gather_money_order_id").val();
	 var creditor_right_value = $("#hidden_creditor_right_value").val();

	 var transfer_factor = $("#transfer_factor").val();
	 var transfer_price = $("#transfer_price_per_share").html();
	 var transfer_share = $("#transfer_share").val();

	 var transfer_manage_fee = $("#transfer_fee").html();
	 var money_password = $("#money_password").val();

	 $("#error_msg").html("");
		$("#tip").css({"display":"none"});
	 
	 if(transfer_share == 0 || transfer_share == null){
		 $("#error_msg").html("请输入正确的份数");
			$("#tip").css({"display":"block"});
			return ;
	}
	if( !$("#checkbox_1").is(':checked') ){
		$("#error_msg").html("请先同意转让协议");
		$("#tip").css({"display":"block"});
		return ;
	}
	if(money_password == "" || money_password == null){
		$("#error_msg").html("请输入提现密码");
		$("#tip").css({"display":"block"});
		return ;
	}

	$.ajax({
		url:url,
		type:'post',
		data:{
			"creditor_right_hold_id":creditor_right_hold_id,
			"gather_money_order_id":gather_money_order_id,
			"creditor_right_value":creditor_right_value,

			"transfer_factor":transfer_factor,
			"transfer_price":transfer_price,
			"transfer_share":transfer_share,

			"transfer_manage_fee":transfer_manage_fee,
			"money_password":money_password	
		
		},
		cache:false,
		success:function(data)
		{
			if(data=="1"){
				//alert(data);
				$("#confirm_transfer").hide();
				$("#success").show();
				//showMessage(["提示","挂出转让单成功"]);
				//window.location.reload();
			}else if(data == "2"){
				$("#error_msg").html("转出份数不能大于可转让份数");
				$("#tip").css({"display":"block"});
				//alert("挂出转出单失败");
			}else if(data == "7"){
				$("#error_msg").html("请先设置提现密码");
				$("#tip").css({"display":"block"});
			}else if(data == "8"){
				$("#error_msg").html("提现密码错误");
				$("#tip").css({"display":"block"});
			}
			else{
				alert("else..");
			}
		}
	},"json");
}

</script>

</body>
</html>


<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">账户管理</a>&nbsp;<span>></span>&nbsp;<a href="#">个人基础信息</a>
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jcxx" style="padding:40px; width:740px;">
    <div class="up"><span>个人基础信息</span><a style="display:none" href="#" onclick="show_editor()">修改信息</a></div>
       <div class="left" style="position:relative;"><a id="portrait" href="#">
       <c:if test="${user.m.url == null}"><img src="<%=__PUBLIC__%>/images/tou.jpg" /></c:if>
      <c:if test="${user.m.url != null}"> <img src="${applicationScope.user_image_path}${user.m.url}" /></c:if>
       </a><br/>
       <a onclick="upload_portrait()" style="background:#ed5050; color:#fff; padding:0 10px; font-size:5px; border-radius:5px;position:absolute;top:105px;left:15px;">上传头像</a>
       </div>
       <div class="con"><label class="label_1"><span>*</span>昵称：</label><p>${nickname2}</p></div>
       <div class="con"><label class="label_1"><span>*</span>真实姓名：</label><p><span>${name1==null?name:name1 }</span><s id="name">${name1==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>身份证号：</label><p><span>${user_identity2==null?user_identity1:user_identity2 }</span><s id="useridentity">${user_identity2==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>手机号码：</label><p><span>${phone2==null?phone1:phone2 }</span><s id="phone">${phone2==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>邮箱地址：</label><p><span>${email==null?null:email }</span><s id="email">${email==null?"未认证":"已认证" }</s></p></div>
       <div class="con"><label class="label_1"><span>*</span>性别：	  </label><p>${user_identity2==null?user_identity1:a%2==0?"女":"男"}</p></div>
       <div class="con"><label class="label_1"><span>*</span>出生日期：</label><p>${user_identity2==null?user_identity:birthday }</p></div>
       
       <a id="editor" style="display:none">
       <div class="con"><label class="label_1"><span>*</span>最高学历：</label><p>大专</p></div>
       <div class="con"><label class="label_1"><span></span> 毕业院校：</label><p></p></div>
       <div class="con"><label class="label_1"><span>*</span>婚姻状况：</label><p>已婚</p></div>
       <div class="con"><label class="label_1"><span>*</span>居住地址：</label><p>广州白云区石石井红星路东</p></div>
       <div class="con"><label class="label_1"><span>*</span>公司行业：</label><p>IT</p></div>
       <div class="con"><label class="label_1"><span>*</span>公司规模：</label><p>10-100人</p></div>
       <div class="con"><label class="label_1"><span>*</span>职位：</label><p>市场部总监</p></div>
       <div class="con"><label class="label_1"><span>*</span>月收入：</label><p>5000-10000元</p></div>
       </a>
       <a id="editor_hide" style="display:none">
               <div class="con">
                <label class="label_1"><span>*</span>&nbsp;最高学历：</label>
    				<select id="education">
    					<option></option>
	    				<option>高中或以下</option>
	    				<option>大专	</option>
	    				<option>本科</option>
	    				<option>研究生或以上</option>
    				</select>
                 <p id="0" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请选择学历</p>
             </div>
       <div class="con"><label class="label_1"><span></span> 毕业院校：</label><p><input id="school" type="text" /></p></div>
                   <div class="con">
                <label class="label_1"><span>*</span>&nbsp;婚姻状况：</label>
                <p class="label_p">
                   <input value="已婚" name="marriage" type="radio"> 已婚&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="未婚" name="marriage" type="radio" checked="checked"> 未婚&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="离异" name="marriage" type="radio"> 离异&nbsp;&nbsp;&nbsp;&nbsp;
                   <input value="丧偶" name="marriage" type="radio"> 丧偶&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
             </div>
              <div class="con con4 con5">
                <label class="label_1"><span>*</span>&nbsp;居住地址：</label>
                <input id="address" class="input_1" onfocus="hide(5,51)" onblur="check_addr('address',51)" value="${user_authenticate_persional_info.m.address}" type="text">
                <p id="address_1" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请填写居住地址</p>
                <p id="address_2" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;地址有误</p>
             </div>
       	<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;公司行业：
								</label>
								<select>
								<option></option>
								<option>制造业</option>
								<option>IT</option>
								<option>政府机关</option>
								<option>媒体/广告</option>
								<option>零售/批发</option>
								<option>教育/培训</option>
								<option>公共事业</option>
								<option>交通运输业</option>
								<option>房地产业</option>
								<option>能源业</option>
								<option>金融/法律</option>
								<option>餐饮/旅馆业</option>
								<option>医疗/卫生/保障</option>
								<option>建筑工程</option>
								<option>农业</option>
								<option>娱乐服务业</option>
								<option>体育/艺术</option>
								<option>公益组织</option>
								<option>其它</option>
								</select>
								<p id="8"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择公司行业</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;公司规模：
								</label>
								<select>
								<option></option>
								<option>10人以下</option>
								<option>10-100人</option>
								<option>100-500人</option>
								<option>500人以上</option>
								</select>
								<p id="9"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择公司规模</p>
							</div>
       <div class="con"><label class="label_1"><span>*</span>公司规模：</label><p>10-100人</p></div>
       						<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;职位：
								</label>
								<input onfocus="hide(1)" class="input_1" id="position" value="${work_info_list.m.position}" type="text">
								<p id="1"  style="color: red; display: none" class="label_p7" > &nbsp;&nbsp;请填写职位名称</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;月收入：
								</label>
								<select>
								<option></option>
								<option>1000元以下</option>
								<option>1001-2000元</option>
								<option>2000-5000元</option>
								<option>5000-10000元</option>
								<option>10000-20000元</option>
								<option>20000-50000元</option>
								<option>50000元以上</option>
								</select>
							<p id="2"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择月收入</p>
							</div>
       <div  style="display:none" class="con" style="margin-top:20px;">
       		<a  style="background:#ed5050; color:#fff; padding:10px 90px; font-size:16px; border-radius:5px; margin-left:100px;">保存</a>
       </div>	
       </a>

    </div>

  </div>    
</div>
<script src="<%=__PUBLIC__ %>/js/select.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var name = $("#name").html();
		if(name=="未认证"){
			$("#name").css("color","red");
		}else{
			$("#name").css("color","green");
		}
		var useridentity = $("#useridentity").html();
		if(useridentity=="未认证"){
			$("#useridentity").css("color","red");
		}else{
			$("#useridentity").css("color","green");
		}
		var phone = $("#phone").html();
		if(phone=="未认证"){
			$("#phone").css("color","red");
		}else{
			$("#phone").css("color","green");
		}
		var email = $("#email").html();
		if(email=="未认证"){
			$("#email").css("color","red");
		}else{
			$("#email").css("color","green");
		}
	});
	
	//上传头像
	function upload_portrait(){
		$.upload({
			// 上传地址				
			url: '<%=__ROOT_PATH__%>/user/usercenter/upload_portrait.html', 
			// 文件域名字
			fileName: 'filedata', 
			// 其他表单数据
			params: { },
			// 上传完成后, 返回json, text
			dataType: 'json',
			// 上传之前回调,return true表示可继续上传
			onSend: function() {
					return true;
			},
			// 上传之后回调
			onComplate: function(data) {
				if(data == 0){
					alert("图片过大，请上传2M以下图片");
				}else if(data == 3){
					refresh();
				}else if(data == 2){
					alert("上传失败，请检查网络。");
				}else if(data == 1){
					alert("请上传正确格式图片！");
				}
			}
		});
	}

	//显示修改
	function show_editor(){
		$("editor_hide").hide();
		$("editor_hide").show();
	}
	function refresh(){
		window.location.href="<%=__ROOT_PATH__%>/user/usercenter/to_personal_information.html";
	}
</script>


<%@ include file="/jsp/index/index_foot.jsp" %>


</body>
</html>

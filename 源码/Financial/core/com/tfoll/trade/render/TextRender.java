package com.tfoll.trade.render;

import com.tfoll.trade.config.Constants;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class TextRender extends Render {
	private String text;

	public TextRender(String text) {
		this.text = text;
	}

	@Override
	public void render() {

		// 默认处理
		PrintWriter writer = null;
		try {
			HttpServletResponse response = getResponse();
			response.setContentType("text/plain;charset=" + Constants.characterEncoding);
			response.setCharacterEncoding(Constants.characterEncoding);
			writer = response.getWriter();
			writer.write(text);
			writer.flush();
		} catch (Exception e1) {
			throw new RenderException(e1);
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (Exception e2) {
				writer = null;
				System.gc();
				System.runFinalization();
			}

		}

	}
}

package com.tfoll.trade.render;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.core.ActionContext;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public class HttpRender extends Render {

	private String url;

	public HttpRender(String url) {
		if (!url.contains("http")) {
			throw new RuntimeException("url is not contains http");
		}
		this.url = url;
	}

	@Override
	public void render() {
		HttpServletResponse response = ActionContext.getResponse();
		try {
			response.sendRedirect(url);
			if (Constants.devMode) {
				logger.error("url:" + url);
			}
		} catch (IOException e) {
			throw new RenderException(e);
		}
	}
}

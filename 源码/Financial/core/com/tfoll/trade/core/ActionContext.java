package com.tfoll.trade.core;

import com.tfoll.trade.render.Render;
import com.tfoll.web.util.Utils;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public final class ActionContext {

	private static ThreadLocal<HttpServletRequest> _request = new ThreadLocal<HttpServletRequest>();
	private static ThreadLocal<HttpServletResponse> _response = new ThreadLocal<HttpServletResponse>();
	private static ThreadLocal<Render> _render = new ThreadLocal<Render>();

	public static void setActionContext(HttpServletRequest request, HttpServletResponse response) {
		_request.set(request);
		_response.set(response);
		_render.set(null);
	}

	public static HttpServletRequest getRequest() {

		return _request.get();
	}

	public static HttpServletResponse getResponse() {
		return _response.get();
	}

	/**
	 * 一个ActionContext注册一个Render,要求调用这个方法一定不能为空
	 */

	public static Render getRender() {
		Render render = _render.get();
		if (render == null) {
			throw new RuntimeException("render is null");
		}
		return render;
	}

	public static void setRender(Render render) {
		_render.set(render);
	}

	public static void setAttribute(String name, Object object) {
		((HttpServletRequest) getRequest()).setAttribute(name, object);
	}

	public static Object getAttribute(String name) {
		return ((HttpServletRequest) getRequest()).getAttribute(name);
	}

	// 基本的三个对象获取
	public static HttpSession getHttpSession() {
		return ((HttpServletRequest) getRequest()).getSession();
	}

	/**
	 * 支持中文
	 * 
	 * @param name
	 * @return
	 */
	// 提供两个处理get提交Request请求的两个方法
	public static String getParameter(String name) {
		String value = getRequest().getParameter(name);
		if (Utils.isNullOrEmptyString(value)) {
			return null;
		}
		value = value.trim();
		if ("get".equalsIgnoreCase(getRequest().getMethod())) {
			try {
				return new String(value.getBytes("iso-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		} else {
			return value;
		}
	}

	public static String[] getParameterValues(String name) {
		String[] values = getRequest().getParameterValues(name);
		if (values == null) {
			return null;
		}
		if ("get".equalsIgnoreCase(getRequest().getMethod())) {
			if (values != null && values.length == 0) {
				return null;
			} else {
				int length = values.length;
				for (int i = 0; i < length; i++) {
					try {
						values[i] = new String(values[i].getBytes("iso-8859-1"), "utf-8");
					} catch (UnsupportedEncodingException e) {
						throw new RuntimeException(e);
					}
				}
				return values;
			}
		} else {
			if (values != null && values.length == 0) {
				return null;
			} else {
				return values;
			}
		}
	}

}

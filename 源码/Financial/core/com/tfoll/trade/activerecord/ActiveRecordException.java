package com.tfoll.trade.activerecord;

@SuppressWarnings("serial")
public class ActiveRecordException extends RuntimeException {

	public ActiveRecordException(String message) {
		super(message);
	}

	public ActiveRecordException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActiveRecordException(Throwable cause) {
		super(cause);
	}
}

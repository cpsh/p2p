package com.tfoll.trade.aop;

/**
 * 该类主要用于执行拦截器栈中的拦截器
 */
public class ActionExecutorWrapper extends ActionExecutor {
	private ActionExecutor actionExecutor;
	private Interceptor[] interceptors;

	private int index = 0;
	private int length = 0;

	public ActionExecutorWrapper(ActionExecutor actionExecutor, Interceptor[] interceptors) {
		this.actionExecutor = actionExecutor;
		this.interceptors = interceptors;
		if (interceptors != null && interceptors.length != 0) {
			this.length = interceptors.length;
		}

	}

	@Override
	public final void invoke() {
		if (length == 0) {
			actionExecutor.invoke();
		} else {
			if (index < length) {
				interceptors[index++].doIt(this);
			} else {
				actionExecutor.invoke();
			}
		}

	}
}

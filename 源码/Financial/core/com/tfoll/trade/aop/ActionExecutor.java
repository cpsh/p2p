package com.tfoll.trade.aop;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.core.Action;
import com.tfoll.trade.core.Controller;

import org.apache.log4j.Logger;

/**
 * 调用Action的容器:包含拦截器和目的方法<br/>
 * 包括Interceptor[]和Action[Controller.Method]
 */
public class ActionExecutor {
	private static Logger logger = Logger.getLogger(ActionExecutor.class);
	private static final Object[] args = new Object[0];

	private Controller controller;
	private Action action;

	public Action getAction() {
		return action;
	}

	private Interceptor[] interceptors;
	private int length = 0;
	private int index = 0;

	protected ActionExecutor() {
	}

	public ActionExecutor(Action action, Controller controller) {
		this.controller = controller;

		this.action = action;
		// list转数组
		Interceptor[] _s = new Interceptor[action.getInterceptorList().size()];
		action.getInterceptorList().toArray(_s);
		this.interceptors = _s;

		this.length = _s.length;

	}

	public void invoke() {
		if (length == 0) {
			try {
				// 调用Action中目的方法
				action.getMethod().invoke(controller, args);
			} catch (Exception e) {
				if (Constants.devMode) {
					e.printStackTrace();
				}
				// 获取最近的异常的原因,所以Action内部要仔细的处理异常
				String exceptionMsg = e.getCause().getMessage();
				logger.error(exceptionMsg);
				throw new RuntimeException(exceptionMsg);
			}
		} else {
			if (index <= length - 1) {
				interceptors[index++].doIt(this);
			} else {
				try {
					// 调用Action中目的方法
					action.getMethod().invoke(controller, args);
				} catch (Exception e) {
					if (Constants.devMode) {
						e.printStackTrace();
					}
					// 获取最近的异常的原因,所以Action内部要仔细的处理异常
					String exceptionMsg = e.getCause().getMessage();
					logger.error(exceptionMsg);
					throw new RuntimeException(exceptionMsg);
				}

			}

		}
	}

}

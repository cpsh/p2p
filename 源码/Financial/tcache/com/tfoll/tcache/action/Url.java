package com.tfoll.tcache.action;

public class Url {
	public static final long $ = 1000;

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public long getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(long activeTime) {
		this.activeTime = activeTime * $;
	}

	private String regex;
	private long activeTime;
}

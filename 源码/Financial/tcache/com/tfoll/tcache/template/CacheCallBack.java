package com.tfoll.tcache.template;

import com.tfoll.web.util.Utils;

import java.util.concurrent.locks.Lock;

/**
 * TCache缓存回调方法:如果不存在或者过期则创建
 */
public abstract class CacheCallBack {
	private CacheContext cacheContext = null;

	/**
	 * 缓存的参数初始化:将读写锁和缓存的key值绑定在一起
	 * 
	 * @param key
	 *            缓存的key值
	 * @param loseEffectivenessInterval
	 *            失效间隔
	 * @param timeType
	 *            时间类型
	 */
	public CacheCallBack(String key, long loseEffectivenessInterval, long timeType) {
		if (Utils.isNullOrEmptyString(key)) {
			throw new NullPointerException("key is Null Or EmptyString ");
		}
		if (loseEffectivenessInterval <= 0) {
			throw new NullPointerException("loseEffectivenessInterval<=0");
		}
		if (timeType <= 0) {
			throw new NullPointerException("timeType<=0");
		}
		/**
		 * 首先从Cache_Context_Map里面获取,不存在则放入到map里面
		 */
		CacheContext cacheContext = CacheContext.Cache_Context_Map.get(key);
		if (cacheContext == null) {
			/**
			 * 生成这个对象的时候里面是含有一把读写锁
			 */
			cacheContext = new CacheContext(loseEffectivenessInterval, timeType);
			CacheContext.Cache_Context_Map.put(key, cacheContext);
		}
		this.cacheContext = cacheContext;

	}

	/**
	 * 定义写的方法
	 */
	public abstract Object doWrite();

	public Object doWriteAndGet() {
		CacheContext cacheContext = this.cacheContext;// 只是好看一点
		long now = System.currentTimeMillis();
		if (cacheContext.getStart_time_long_or_update_cache_time() == 0l || //
				now - cacheContext.getStart_time_long_or_update_cache_time() >= cacheContext.getLose_effectiveness_interval()) {
			Lock lock = cacheContext.getLock().writeLock();
			lock.lock();
			try {
				Object object = doWrite();
				cacheContext.setObject(object);
				cacheContext.setStart_time_long_or_update_cache_time(now);
				return cacheContext.getObject();
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				lock.unlock();
			}

		} else {
			Lock lock = cacheContext.getLock().readLock();
			lock.lock();
			try {
				return cacheContext.getObject();
			} finally {
				lock.unlock();
			}
		}

	}
}

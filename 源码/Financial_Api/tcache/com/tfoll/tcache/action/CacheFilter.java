package com.tfoll.tcache.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class CacheFilter implements Filter {

	private static final String Has_Cache = "tcache_action";

	private final Map<String, CacheContent> cache = new ConcurrentHashMap<String, CacheContent>();
	private static List<Url> urlList = new ArrayList<Url>();
	private static Lock lock = new ReentrantLock();

	public void init(FilterConfig filterConfig) throws ServletException {

		try {
			urlList = getUrl();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);

		exec.scheduleWithFixedDelay(new Runnable() {
			public void run() {

				if (cache.size() > 0) {
					long now = System.currentTimeMillis();

					for (Entry<String, CacheContent> entry : cache.entrySet()) {

						CacheContent cc = entry.getValue();
						long overTime = cc.getOverTime();
						if (now > overTime) {
							lock.lock();
							try {
								cache.remove(entry.getKey());
							} catch (Exception e) {
							} finally {
								lock.unlock();
							}
						}

					}

				}

			}
		}, 1, 1, TimeUnit.SECONDS);

	}

	public void destroy() {
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		if (isFilteredBefore(request)) {
			filterChain.doFilter(request, servletResponse);
			return;
		}
		request.setAttribute(Has_Cache, Boolean.TRUE);

		String key = getCacheKey(request);
		// System.out.println("key="+key);
		CacheContent cacheContent = cache.get(key);
		if (cacheContent != null) {
			// System.out.println("from cache "+key);
			cacheContent.writeTo(servletResponse);
			return;
		} else {

			for (Url url : urlList) {
				String regexUrlPath = url.getRegex();

				if (!key.startsWith(regexUrlPath)) {
					continue;
				} else {
					CacheWrapper cacheWrapper = new CacheWrapper((HttpServletResponse) servletResponse);
					filterChain.doFilter(request, cacheWrapper);
					cacheWrapper.flushBuffer();
					CacheContent cc = cacheWrapper.getContent();
					cc.setCreateTime(System.currentTimeMillis());
					cc.setActiveTime(url.getActiveTime());
					cc.setOverTime();
					cache.put(key, cc);
					return;
				}

			}
			filterChain.doFilter(servletRequest, servletResponse);
			return;

		}

	}

	private String getCacheKey(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder(request.getServletPath());
		String qs = request.getQueryString();
		if (Utils.isNotNullAndNotEmptyString(qs)) {
			sb.append("?").append(qs);
		}
		return sb.toString();
	}

	public boolean isFilteredBefore(ServletRequest request) {
		return request.getAttribute(Has_Cache) != null;
	}

	/**
	 * @param /tfoll-url-cache.xml
	 */
	@SuppressWarnings("unchecked")
	public static List<Url> getUrl() {

		SAXReader reader = new SAXReader();
		String xmlPath = CacheFilter.class.getClassLoader().getResource("tcache-url.xml").getPath();
		Document document = null;
		try {
			document = reader.read(new FileInputStream(xmlPath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Element root = document.getRootElement();
		List<Url> urlList = new ArrayList<Url>();
		Map<String, String> map = new HashMap<String, String>();
		for (Iterator iterator = root.elementIterator(); iterator.hasNext();) {
			Element element = (Element) iterator.next();

			Url url = new Url();
			String url_path = element.getTextTrim();
			if (map.containsKey(url_path)) {
				throw new RuntimeException("url_path is null");
			}
			map.put(url_path, "");
			url.setRegex(url_path);
			url.setActiveTime(Long.parseLong(element.attributeValue("activeTime")));
			urlList.add(url);

		}
		return urlList;

	}

}
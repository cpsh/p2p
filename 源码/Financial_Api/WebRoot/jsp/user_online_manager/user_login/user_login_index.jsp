<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		
		<div class="container" style="margin-top: 120px;">
		<table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>管理员登录用户</th>
                <th>需要三个管理员一起进行登录     ${tips}</th>
              </tr>
            </thead>
           <form action="<%=__ROOT_PATH__ %>/user_login/user_login.html" method="post">
           <tbody>
              <tr>
                <td>用户邮箱</td>
                <td><input type="email" class="input-small" placeholder="Email" name="email"></td>
              </tr>
              <tr>
                <td>第一位管理员帐号</td>
                <td><input type="text" class="input-small" placeholder="帐号" name="admin1_username" required></td>
              </tr>
             <tr>
                <td>第一位管理员密码</td>
                <td><input type="text" class="input-small" placeholder="密码" name="admin1_password" required></td>
              </tr>
               <tr>
                <td>第二位管理员帐号</td>
                <td><input type="text" class="input-small" placeholder="帐号" name="admin2_username" required></td>
              </tr>
             <tr>
                <td>第二位管理员密码</td>
                <td><input type="text" class="input-small" placeholder="密码" name="admin2_password" required></td>
              </tr>
               <tr>
                <td>第三位管理员帐号</td>
                <td><input type="text" class="input-small" placeholder="帐号" name="admin3_username" required></td>
              </tr>
             <tr>
                <td>第三位管理员密码</td>
                <td><input type="text" class="input-small" placeholder="密码" name="admin3_password" required></td>
              </tr>
               <tr>
                <td></td>
                <td><input type="submit" class="submit"></td>
              </tr>
            </tbody>
           </form>
          </table>

		</div>
		
		
		
	</body>
</html>

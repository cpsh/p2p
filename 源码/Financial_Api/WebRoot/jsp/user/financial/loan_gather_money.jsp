<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>${applicationScope.title}</title>
  </head>
  <%
  //需要检查三个方面
  %>
  <body>
  	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>


<div class="about">
  <div class="about_con">
     <div class="loan_aa">
        <div class="loan_aa_top">
           <div class="up">
              <a href="#">我要借款</a>&nbsp;<span>></span>
               <c:choose>
			   	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
			   	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
			   </c:choose>
              <span>></span>&nbsp;筹集借款
           </div>
           <div class="center">
              <span style="background:#ed5050; color:#fff;">1</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">2</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">3</span>
              <s style="background:#ed5050;"></s>
              <span style="background:#ed5050; color:#fff;">4</span>
              <s></s>
              <span>5</span>
           </div>
           <div class="down">
              <span style="color:#cc0000;">填写借款申请</span>
              <span style="margin-left:55px; color:#cc0000;">填写借款信息</span>
              <span style="margin-left:60px; color:#cc0000;">等待审核</span>
              <span style="margin-left:60px;color:#cc0000;">筹集借款</span>
              <span style="margin-left:55px;">获得借款</span>
           </div>
        </div>
        <div class="loan_cc"><s>&bull;</s>正在筹集借款中，请耐心等待。</div>
        <div class="loan_dd">
           <div class="left">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                    <td class="td_1" colspan="5"><img src="<%=__PUBLIC__%>/images/icon1.jpg" />筹集名称<a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id=${gather_money_order.m.id}">${gather_money_order.m.borrow_title}</a></td>
                 </tr>
                 <tr>
                    <td colspan="2">筹集总额（元）</td>
                    <td colspan="2">总的份数（份）</td>
                 </tr>
                 <tr class="tr_1">
                    <td colspan="2">￥${gather_money_order.m.borrow_all_money}</td>
                    <td colspan="2">${gather_money_order.m.borrow_all_share}&nbsp;份</td>
                 </tr>
                 <tr>
                    <td width="19%">保障方式</td>
                    <td width="31%">本金</td>
                    <td width="30%">提前还款费率</td>
                    <td width="20%">1.00%</td>
                 </tr>
                 <tr>
                    <td>还款方式</td>
                    <td>按月还款/等额本息</td>
                    <td>月还本息（元）</td>
                    <td>${monthly_principal_and_interest}</td>
                 </tr>
              </table>
           </div>
           <div class="right">
              <p style="border-top:none;"><s>已筹集到的金额</s><span>￥${have_gather_money}</span></p>
              <p><s>已筹集到的份数</s><span>${gather_money_order.m.have_gather_share}份</span></p>
              <p style="border-bottom:none;"><s>筹款截止时间</s><span>${gather_money_order.m.deadline}</span></p>
           </div>
        </div>                     
              
     </div>
  </div>  
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
	
  </body>
  
  	<script type="text/javascript">

  	$(function(){$(".loan_p5").hide();});

	function check_title(){
		var title = $("#title").val();
		//为空验证
		if(title.trim()=="" || title.length==0){
			$("#title_msg").html("借款标题不能为空");
			$("#title_msg").show();
			return;
		}
		if(title.length>14){
			$("#title_msg").html("借款标题不能超过14个文字");
			$("#title_msg").show();
			return;
		}
		//
		$("#title_msg").html("");
		return;
	}
	function check_use(){
		var use = $("#use").val();
		if(use.trim()=="" || use.length==0){
			$("#use_msg").html("借款用途不能为空");
			$("#use_msg").show();
			return;
		}
		$("#use_msg").html("");
		return;
	}

	function check_money(){
		var money = $("#money").val();
		if(money.trim()=="" || money.length==0){
			$("#money_msg").html("借款金额不能为空");
			$("#money_msg").show();
			return;
		}
		if(money<3000 || money>500000){
			$("#money_msg").html("借款金额在3000-500000之间");
			$("#money_msg").show();
			return;
		}
		if(money%50!=0){
			$("#money_msg").html("借款金额必须是50的整数倍");
			$("#money_msg").show();
			return;
		}
		$("#money_msg").html("");
		return;
	}

	function check_interest_rate(){
		var interest_rate = $("#interest_rate").val();
		if(interest_rate.trim()=="" || interest_rate.length==0){
			$("#rate_msg").html("年化利率不能为空");
			$("#rate_msg").show();
			return;
		}
		if(interest_rate<10 || interest_rate>24){
			$("#rate_msg").html("年化利率在10%~24%之间");
			$("#rate_msg").show();
			return;
		}
		$("#rate_msg").html("");
		return;
	}
  	
	function submit_apply(){

		$(".loan_p5").hide();

		
		var can_submit = true;
		var title = $("#title").val();
		//标题验证两次
		var title_count=0;
		if(title.trim()=="" || title.length==0){
			$("#title_msg").html("借款标题不能为空");
			$("#title_msg").show();
			can_submit = false;
			title_count++;
		}
		//两次的验证条件
		if(title.length>14){
			if(title_count>0){
			//后面不需要验证提示
			}else {
				$("#title_msg").html("借款标题不能超过14个文字");
			    $("#title_msg").show();
			    can_submit = false;
			    title_count++; 
			}
		}
		
		var use = $("#use").val();
		if(use.trim()=="" || use.length==0){
			$("#use_msg").html("借款用途不能为空");
			$("#use_msg").show();
			can_submit = false;
		}
		
		var money = $("#money").val();
		var money_count=0;
		if(money.trim()=="" || money.length==0){
			$("#money_msg").html("借款金额不能为空");
			$("#money_msg").show();
			can_submit = false;
			money_count++;
		}
		if(money<3000 || money>500000){
			if (money_count>0) {
				//后面不需要验证提示
			}else {
				$("#money_msg").html("借款金额在3000-50000之间");
				$("#money_msg").show();
				can_submit = false;
				money_count++;
			}
		}
		if(money%50!=0){
			if (money_count>0) {
				//后面不需要验证提示
			}else {
				$("#money_msg").html("借款金额在必须是50的整数倍");
				$("#money_msg").show();
				can_submit = false;
				money_count++;
			}
		}

		var interest_rate = $("#interest_rate").val();
		if(interest_rate<10 || interest_rate>24){
			$("#rate_msg").html("年化利率在10%~24%之间");
			$("#rate_msg").show();
			can_submit = false;
			
		}
		//如果不满足条件后面的则不能执行
		if(!can_submit){
			return;
		}
		
		var duration = $("#duration").html();//后台判断
		var describe=$("#describe").val();
		$.post('<%=__ROOT_PATH__%>/user/financial/borrow/loan_submit_borrow_money_apply_order.html',
				{
					"borrow_title":title,
					"borrow_purpose":use,
					"borrow_all_money": money ,
					"borrow_duration":duration ,
					"annulized_rate":interest_rate,
					"describe" :describe,
					"borrow_type":${borrow_type}
				},
				function(data) //回传函数    
				{
					if(data == 1){		//保存成功,跳转到填写订单信息页面
						window.location.href="<%=__ROOT_PATH__%>/user/authentication/persional_info/persional_index.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码

		}

	//把借款金额变成50的整数倍   , 计算管理费
	function adjust_money(){		
		var pre_money = $("#money").val();
		var later_money = Math.floor(pre_money/50)*50;
		$("#money").val(later_money);
		var manage_fee = later_money/1000*3;
		$("#manage_fee").html('￥'+manage_fee.toFixed(2));
			
	}
  	
$(function(){
	/*============================
	@author:flc
	@time:2014-02-11 18:16:09
	@qq:3407725
	============================*/
	$(".select").each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
})
</script>
  
</html>

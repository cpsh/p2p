<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>${applicationScope.title}</title>
  </head>
  <body>
  
  <%@ include file="/jsp/index/index_top.jsp" %>

<span id="ny_pic"></span>

<div class="sanbiao">
	<input id="newer_gather_id" type="hidden" value="${newer_bid_gather.m.id}" />
  <h1><a href="#">我要理财</a> > <a href="#">散标投资列表</a> > 借款详情</h1>
  <div class="sanbiao_bt sanbiao_bt_2">
    <h2><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" colspan="5" class="td_mz"><p style=" font-size:18px; float:left"><img src="<%=__PUBLIC__ %>/images/icon2.jpg" /> 新手体验-模拟精英散标12月期 年利16% </p> <span style="float:left"><a href="#" style="margin-left:15px; color:#09F">联富金融</a></span></td>
    </tr>
  <tr>
    <td height="88" colspan="2" style="font-size:14px;">标的总额 （元）<br /><span class="sanbiao_xx_text">￥500000</span></td>
    <td height="88" colspan="2">年利率<br /><span class="sanbiao_xx_text">16%</span></td>
    <td width="17%" height="88">还款期限<br /><span class="sanbiao_xx_text">1天</span></td>
  </tr>
  <tr>
    <td width="16%" height="35" style="font-size:14px;">保障方式</td>
    <td width="32%" height="35" align="left" style="font-size:14px;" class="td_bj">本金+利息&nbsp;<a href="#"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>详情参见<s>本金保障计划</s><img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    <td width="21%" height="35" align="left" style="font-size:14px;">提前还款费率</td>
    <td width="14%" height="35" align="left" style="font-size:14px;">1%</td>
    <td rowspan="2"></td>
  </tr>
  <tr>
    <td height="35" style="font-size:14px;">还款方式</td>
    <td height="35" align="left" style="font-size:14px;" class="td_bj td_bx">按月还款/等额本息&nbsp;<a href="#"><img src="<%=__PUBLIC__ %>/images/loan_3.png" /><span>等额本息还款法是在还款期内，每月偿还同等数额的贷款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、利息比重逐月递减。<img src="<%=__PUBLIC__ %>/images/loan_4.png" /></span></a></td>
    <!-- 
    <td height="35" align="left" style="font-size:14px;">月还本息（元）</td>
    <td height="35" align="left" style="font-size:14px;">￥0.1</td>
    -->
  </tr>
  <tr class="tr_2">
     <td class="td_1">投标进度</td> 
     <td class="td_2" colspan="2">
     	<p class="a">
     		<span style="width: ${newer_bid_gather.m.gather_progress}%"></span>
     	</p>
     	<p class="b">${newer_bid_gather.m.gather_progress}%<img src="<%=__PUBLIC__ %>/images/loan_b_1.png" /></p>
     </td>
     <td colspan="2" class="td_3"></td>
  </tr>
</table>
</h2>
    <h5 class="h5_3">
       <div class="div_1">剩余金额（元）</div>
       <input type="hidden" id="remain_money" value="" />
       <div class="div_1"><span>￥${newer_bid_gather.m.remain_money}</span>（${newer_bid_gather.m.remain_share}份）</div>
       <div class="div_1"><s id="money_can_use">账户余额&nbsp;0.00元</s><a href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></div>
       <div class="div_1 div_2"><input id="invest_money" type="text" readonly="true" onkeyup="this.value=this.value.replace(/\D/g,'')" value="20000" /><span>元</span></div>
       <div class="div_1 div_3" id="money_msg"></div>
       <script src="js/loginDialog.js" type="text/javascript"></script>
       <link href="css/loginDialog.css" type="text/css" rel="stylesheet" />
       <c:choose>
       		<c:when test="${sessionScope.user!=null}"> <input type="button" class="button_1" id="bid" onClick="judge_user_type();" value="投  标"/></c:when>
       		<c:otherwise> <input type="button" class="button_1" id="bid" onClick="need_login_in()" value="投  标"/></c:otherwise>
       </c:choose>
      
       <div id="loginDiv1" class="loginDiv1"></div>
       
       <div id="loginDiv2" class="loginDiv2" style=" z-index:10005; display:none;">
          <div id="confirm_bid" class="sanbiao_toubiao">             
             <input type="button" class="guanbi" onClick="closeme()" value="ｘ" />
             
             <div class="up">确认投资</div>
             <div class="con"><p>借款标题</p><span>新手精英散标 年利16% </span></div>
             <div class="con"><p>借款用户</p><span style="color:#ed5050;">联富金融</span></div>
             <div class="con"><p>年利率</p><span>16%</span></div>
             <div class="con"><p>还款期限</p><span>1天</span></div>
             <div class="con"><p>还款方式</p><span>按月还款/等额本息</span></div>
             <div class="con"><p>保障方式</p><span>本金+利息</span></div>
             <div class="con"><p>投资金额</p><span id="invest_money_c"></span></div>
             
             <div class="con"><p>验证码</p><input class="input_1" type="text" id="check_code" />
				 <img id="CheckCodeServlet" width="122" height="42" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh_order_pay();" style="border: 1px; font-color: white;" />
			 </div>
             
             <div class="con con_1" id="check_msg"></div>
             <!-- 
             <div class="con">如遇流标情况，投标期内所冻结的金额将在流标后解冻</div>
              -->
             <div class="con"><input class="checkbox_1" type="checkbox" id="checkbox" checked="checked"/><s>我已阅读并同意签署</s><a style="color:#ed5050;" href="#">《借款协议》</a></div>
             <!-- 判断验证码 -->
             	
             <!-- 判断验证码 -->
             <div class="con con_2"><a class="a" href="javascript:void(0);" onclick="confirm_elite_new_bid();">确定</a><a href="javascript:void(0);" onClick="closeme();">取消</a></div>
             
          </div>
          
		<div id="success" style="display:none" class="sanbiao_toubiao" >
			<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic7.png" />投标成功</div>
			<div class="con_4" ><a onClick="closeme_refresh()">关闭</a></div>
		</div>
		
		<div id="fail" style="display:none" class="sanbiao_toubiao" >
			<div class="con_3" ><img src="<%=__PUBLIC__ %>/images/registration_pic8.png" />对不起，您所投金额大于该标所需金额！</div>
			<div class="con_4" ><a onClick="closeme()">关闭</a></div>
		</div>
       </div>
    </h5>    
  </div>

	   <div class="xin_xq">
     <div class="up">新手体验活动</div>
     <div class="con">
        &bull;&nbsp;本活动中，用户无需支付任何真实本金。奖励金额直接充入帐号且无使用限制。<br />
        &bull;&nbsp;参加活动的用户可以获得等值于投资收益的活动奖励。<br />
        &bull;&nbsp;本活动参与权无任何充值、投资等前置限制。联富金融的任何出借人仅需绑定手机，并成功进行身份验证就可以参与活动。<br />
        &bull;&nbsp;本体验产品仅供注册用户模拟联富金融产品的购买和收益过程，且每个体验产品仅能参与一次。<br />
        &bull;&nbsp;联富金融有权取消涉嫌骗取活动奖励的用户获奖资格。本活动最终解释权归联富金融。<br />
     </div>
  </div>
  

  <div class="xin_xq">
     <div class="up">常见问题</div>
     <div class="con">
        <h2>如何在联富金融出借？</h2>
        您需要注册成为联富金融的出借人，之后可以选择自己满意的借款项目进行投标。确认投标并支付后，您就完成了一次出借。在联富金融，您可以自主投标、购买转让债权，也可以参加联富宝服务。<br />
        <h2>出借人的收益如何计算？</h2>
        联富金融平台采用目前流行的等额本息按月还款。出借人每月都有资金收回。由于已收回的本金不再产生收益，利率不变的情况下，利息收入会逐渐降低。为了达到较高的收益，我们建议您循环出借，将收回的资金再借出去。<br />  
        <h2>什么时候能收回借款？</h2>     
        为了增加出借人资金的流动性，联富金融平台目前采用的是“等额本息还款法”，出借人每个月都有资金收回。由借款资金放款成功日（如：11月14日）开始计算，1个自然月后的日期（如：12月14日）为第一个还款日，以后每月的还款时间以此类推。<br /> 
 
        <h2>线上加入精英散标理财服务如何支付？</h2>
        线上加入精英散标理财服务，支持网银支付（需注意单日、单笔最大交易金额限制）。联富金融建议您使用如下网银： 中国工商银行、中国建设银行、中国农业银行、中国银行、招商银行、交通银行、邮政储蓄银行、民生银行、中信银行、光大银行、兴业银行、广发银行、深圳发展银行、平安银行。 如在支付过程中遇到问题，请联系联富金融客服：4006-888-923。
     </div>
  </div>
  
</div>




<%@ include file="/jsp/index/index_foot.jsp" %>

	<script type="text/javascript">

	$(function(){
		get_money_can_use();
	});
	
	//提示需要登录
	function need_login_in(){
		showMessage(["提示","请先登录"]);
	}
	// 判断用户是否为理财人
	function judge_user_type(){
		var url = "<%=__ROOT_PATH__%>/user/financial/financial/judge_borrower.html";
		$.ajax({
			url:url,
			type:'get',
			cache:false,
			success:function(data)
			{
				if(data=='1'){
					//alert("1");
					open_confirm_newer();
				}else if(data == '2'){
					showMessage(["提示","对不起，您不是理财账户，不能投标"]);
				}else{
					showMessage(["提示","else..."]);
				}
			}
		},"json");	
	}

	//确定投资新手标
	function confirm_elite_new_bid(){

		var $checkbox = $("#checkbox");
		var checkbox = $checkbox[0];    //DOM对象 

		var check_code = $("#check_code").val();
		var newer_gather_id = $("#newer_gather_id").val();

		if(!checkbox.checked){
			$("#check_msg").html("请先同意签署借款协议");
			return;
		}

		$.post('<%=__ROOT_PATH__%>/user/financial/financial/confirm_elite_new_bid.html',
				{
					"check_code" : check_code,
					"newer_gather_id" : newer_gather_id
				},
				function(data) //回传函数
				{
					
					if(data == '1'){
						$("#check_msg").html("验证码错误");
					} else if(data == '2'){
						$("#check_msg").html("你已经成功投资过新手体验项目，请投资其它项目");
					}else if(data == '3'){ //投新手标成功
						$("#confirm_bid").hide();
						$("#success").show();
					}else if(data == '5'){
						$("#check_msg").html("该新手标已满，请投下一个新手标");
					}else{
						$("#check_msg").html("...else");
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");	//浏览器编码		
	}





	/**
  	 * 获取验证码：
  	 * @return
  	 */
  	  var path = "http://"+window.location.host;
  	function refresh_order_pay(){
		var CheckCodeServlet=document.getElementById("CheckCodeServlet");
			CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
	}

  	function closeme_refresh(){
		document.getElementById('loginDiv1').style.display='none';
		document.getElementById('loginDiv2').style.display='none';
		window.location.reload();
	}

  	function get_money_can_use(){
		$.get('<%=__ROOT_PATH__%>/user/financial/financial/get_money_can_use.html',
				{
			
				},
				function(data) //回传函数
				{
					$("#money_can_use").html("账户余额  "+data+"元");
					$("#remain_money").val(data);
				}, "json");//浏览器编码	
	}

  	
	</script>

</body>
</html>
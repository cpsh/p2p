<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>${applicationScope.title}</title>

		<link href="style/main.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
	</head>
	<body>
	<%@ include file="/jsp/index/index_top.jsp"%>
<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">账户管理</a>&nbsp;<span>></span>&nbsp;认证信息
    </div>
  	<%@ include file="/jsp/user/usercenter/user_center_left.jsp"%>
    <div class="geren_rzxx">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="25%">信用等级</td>
             <td width="25%">信用总分</td>
             <td width="25%">信用额度</td>
             <td width="25%">可用额度</td>
          </tr>
          <tr>
             <td class="td_2"><span>${credit_rating }</span></td>
             <td class="td_1">${sum_scores}</td>
             <td class="td_1">￥&nbsp;0</td>
             <td class="td_1">￥&nbsp;0</td>
          </tr>
       </table>
    </div>
    <div class="geren_rzxx_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="15%">&nbsp;</td>
             <td width="55%">项目</td>
             <td width="16%">状态</td>
             <td class="td_1" width="14%">信用分数</td>
          </tr>
          <tr>
             <td>基本信息</td>
             <td>个人详细信息，工作信息</td>
             <td>&nbsp;</td>
             <td class="td_1">${user_credit_files.m.basic_info_credit_scores}&nbsp;分</td>
          </tr>
          <tr>
             <td rowspan="4">必要申请资料</td>
             <td>身份证认证</td>
             <td id="0" >待完成</td>
             <td rowspan="9" class="td_1 td_a">${upload_sum_scores }&nbsp;分&nbsp;<a href="#"><img src="<%= __PUBLIC__ %>/images/loan_3.png" /><span>该分数由用户的综合情况决定，单项认证通过不额外加分。<img src="<%= __PUBLIC__ %>/images/loan_5.png" /></span></a></td>
          </tr>
          <tr>
             <td>工作认证</td>
             <td id="1" >待完成</td>
          </tr>
          <tr>
             <td>收入认证</td>
             <td id="2" >待完成</td>
          </tr>
          <tr>
             <td>信用报告</td>
             <td id="3" >待完成</td>
          </tr>
          <tr>
             <td rowspan="6">可选申请资料</td>
             <td>房产认证</td>
             <td id="4" >待完成</td>
          </tr>
          <tr>
             <td>技术职称认证</td>
             <td id="5" >待完成</td>
          </tr>
          <tr>
             <td>购车证明</td>
             <td id="6" >待完成</td>
          </tr>
          <tr>
             <td>结婚认证</td>
             <td id="7" >待完成</td>
          </tr>
          <tr>
             <td>居住地证明</td>
             <td id="8" >待完成</td>
          </tr>
          <tr>
             <td>学历认证</td>
             <td id="9" >待完成</td>
          </tr>
          <tr>
             <td rowspan="3">联富金融记录</td>
             <td>还清笔数（+1分/笔，加分间隔28天，上限20分）</td>
             <td>${user_credit_files.m.pay_off_the_pen_number }&nbsp;笔</td>
             <td class="td_1">${user_credit_files.m.pay_off_the_pen_number }&nbsp;分</td>
          </tr>
          <tr>
             <td>逾期次数（-1分/次）</td>
             <td>${user_credit_files.m.overdue_num }&nbsp;次</td>
             <td class="td_1">- ${user_credit_files.m.overdue_num }&nbsp;分</td>
          </tr>
          <tr>
             <td>严重逾期笔数（-30分/次）</td>
             <td>${user_credit_files.m.seriously_overdue }&nbsp;次</td>
             <td class="td_1">- ${user_credit_files.m.seriously_overdue * 30 }&nbsp;分</td>
          </tr>
       </table>
       <div class="down"> 温馨提示：您可以在发布借款申请时按照网站要求进行信息认证，本页仅供认证状态查询。</div>
    </div>
       
  </div>    
</div>
<%@ include file="/jsp/index/index_foot.jsp"%>
</script>
<script type="text/javascript">
$(function(){
	var url = [${user_authenticate_upload_info.m.id_authenticate_url},${user_authenticate_upload_info.m.work_authenticate_url},${user_authenticate_upload_info.m.credit_authenticate_url},${user_authenticate_upload_info.m.income_authenticate_url},${user_authenticate_upload_info.m.housing_authenticate_url},${user_authenticate_upload_info.m.car_authenticate_url},${user_authenticate_upload_info.m.marriage_authenticate_url},${user_authenticate_upload_info.m.education_authenticate_url},${user_authenticate_upload_info.m.title_authenticate_url},${user_authenticate_upload_info.m.living_authenticate_url}];
	var status = [${user_authenticate_upload_info.m.id_authenticate_status},${user_authenticate_upload_info.m.work_authenticate_status},${user_authenticate_upload_info.m.credit_authenticate_status},${user_authenticate_upload_info.m.income_authenticate_status},${user_authenticate_upload_info.m.housing_authenticate_status},${user_authenticate_upload_info.m.car_authenticate_status},${user_authenticate_upload_info.m.marriage_authenticate_status},${user_authenticate_upload_info.m.education_authenticate_status},${user_authenticate_upload_info.m.title_authenticate_status},${user_authenticate_upload_info.m.living_authenticate_status}];
	
	for(var i = 0;i<attr.length;i++){
		if(url[i] != null && url[i] != ""){
			if(status[i] == 0){
					$("#"+i).html("审核中");
				}else if(status[i] == 1){
					$("#"+i).html("已通过");
				}else{
					$("#"+i).html("不通过");
					}			
		}else{
			$("#"+i).html("待完成");
			}	
	}
})



</script>
</body>


</html>


<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">账户管理</a>&nbsp;<span>></span>&nbsp;认证信息
    </div>
    <div class="geren_left">
      <ul>
        <li><a class="a" href="geren.html"><span></span>我的联富金融</a></li>
        <li onClick="showsubmenu(1)"><a class="b" href="#"><span></span>资金管理</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="geren_jyjl.html"><span></span>交易记录</a>
           <a href="geren_cz.html"><span></span>充值</a>
           <a href="geren_tx.html"><span></span>提现</a>
        </li>
        <li onClick="showsubmenu(2)"><a class="c" href="#"><span></span>理财管理</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="geren_wdzq.html"><span></span>我的债权</a>
           <a href="geren_ujh.html"><span></span>联富金融</a>
           <a href="geren_zqzr.html"><span></span>债权转让</a>
           <a href="geren_lctj.html"><span></span>理财统计</a>
        </li>
        <li onClick="showsubmenu(3)"><a class="d" href="#"><span></span>借款管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="geren_wdjk.html"><span></span>我的借款</a>
           <a href="geren_jksqcx.html"><span></span>借款申请查询</a>
           <a href="geren_jktj.html"><span></span>借款统计</a>
        </li>
        <li onClick="showsubmenu(4)"><a class="e" href="#" style="background:#2ea7e0;color:#fff;"><span style="background:url(images/geren_1.png) right -80px no-repeat;"></span>账户管理</a></li>
        <li id="submenu4" class="submenu">
           <a href="geren_jcxx.html"><span></span>个人基础信息</a>
           <a href="geren_rzxx.html" style="background:#7fd4f3;color:#fff;"><span></span>认证信息</a>
           <a href="geren_aqxx.html"><span></span>安全信息</a>
           <a href="geren_yhkxx.html"><span></span>银行卡信息</a>
        </li>
        <li onClick="showsubmenu(5)"><a class="f" href="geren_znxx.html"><span></span>站内消息</a></li>
        <li id="submenu5" class="submenu" style="display:none;"></li>
      </ul>
    </div>
    <div class="geren_rzxx">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="25%">信用等级</td>
             <td width="25%">信用总分</td>
             <td width="25%">信用额度</td>
             <td width="25%">可用额度</td>
          </tr>
          <tr>
             <td class="td_2"><span>HR</span></td>
             <td class="td_1">0</td>
             <td class="td_1">￥&nbsp;0</td>
             <td class="td_1">￥&nbsp;0</td>
          </tr>
       </table>
    </div>
    <div class="geren_rzxx_con">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="15%">&nbsp;</td>
             <td width="55%">项目</td>
             <td width="16%">状态</td>
             <td class="td_1" width="14%">信用分数</td>
          </tr>
          <tr>
             <td>基本信息</td>
             <td>个人详细信息，工作信息</td>
             <td>&nbsp;</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td rowspan="4">必要申请资料</td>
             <td>身份证认证</td>
             <td class="td_2">审核中</td>
             <td rowspan="9" class="td_1 td_a">0&nbsp;分&nbsp;<a href="#"><img src="images/loan_3.png" /><span>该分数由用户的综合情况决定，单项认证通过不额外加分。<img src="images/loan_5.png" /></span></a></td>
          </tr>
          <tr>
             <td>工作认证</td>
             <td class="td_2">审核中</td>
          </tr>
          <tr>
             <td>收入认证</td>
             <td class="td_2">审核中</td>
          </tr>
          <tr>
             <td>信用报告</td>
             <td class="td_3">未通过</td>
          </tr>
          <tr>
             <td rowspan="8">可选申请资料</td>
             <td>房产认证</td>
             <td>待完成</td>
          </tr>
          <tr>
             <td>技术职称认证</td>
             <td>待完成</td>
          </tr>
          <tr>
             <td>购车证明</td>
             <td>待完成</td>
          </tr>
          <tr>
             <td>结婚认证</td>
             <td>待完成</td>
          </tr>
          <tr>
             <td>居住地证明</td>
             <td>待完成</td>
          </tr>
          <tr>
             <td>学历认证（10分）</td>
             <td>待完成</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td>手机实名认证（10分）</td>
             <td>待完成</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td>微博认证（2分）</td>
             <td>待完成</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td rowspan="3">联富金融记录</td>
             <td>还清笔数（+1分/笔，加分间隔28天，上限20分）</td>
             <td>0&nbsp;笔</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td>逾期次数（-1分/次）</td>
             <td>0&nbsp;次</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
          <tr>
             <td>严重逾期笔数（-30分/次）</td>
             <td>0&nbsp;次</td>
             <td class="td_1">0&nbsp;分</td>
          </tr>
       </table>
       <div class="down"> 温馨提示：您可以在发布借款申请时按照网站要求进行信息认证，本页仅供认证状态查询。</div>
    </div>
       
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


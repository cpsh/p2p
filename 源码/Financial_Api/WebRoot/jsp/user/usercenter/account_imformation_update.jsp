<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">账户管理</a>&nbsp;<span>></span>&nbsp;安全信息
    </div>
   <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_aqxx">
       <div class="up">安全信息</div>
       <div class="geren_aqxx_con">
          <ul>
             <li><div class="con"><h6></h6><p>昵称</p><span>已设置</span><s>123456</s></div></li>
             <li><div class="con"><h6 style=" background-position:0 -88px"></h6><p>实名认证</p><span>51****************</span><s>*23456</s></div></li>
             <li><div class="con"><h6 style=" background-position:0 -44px"></h6><p>绑定邮箱</p><span>已设置</span><s>4452456587@qq.com</s></div></li>
             <li><div class="con"><h6 style=" background-position:0 -137px"></h6><p>登录密码</p><span>已设置</span><s><a onClick="showsubmenu(6)">修改</a></s></div></li>
             <li id="submenu6" class="submenu_1" style="display:none;">
                <div class="con_2">为了您的账户安全，请定期更换登录密码，并确保登录密码设置与提现密码不同。</div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;原密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;新密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;确认新密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_4"><a>提交</a></div>
             </li>
             <li><div class="con"><h6 style=" background-position:0 -188px"></h6><p>绑定手机</p><span>158****5568</span><s><a onClick="showsubmenu(7)">修改</a></s></div></li>
             <li id="submenu7" class="submenu_1 submenu_2" style="display:none;">
                <div class="con_2">为了您的账户安全，请验证手机！</div>
                <div class="con_2 con_3">
                   <label class="label_1">新手机号：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_2 con_3">
                   <label class="label_1">短信验证码：</label>
                   <input class="password_1 password_2" value="" type="text">
                   <input class="button_1" type="button" value="发送验证码" />
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_4"><a>提交</a></div>
             </li>
             <li><div class="con"><h6 style=" background-position:0 2px"></h6><p>提现密码</p><span>已设置</span><s><a onClick="showsubmenu(8)">修改</a>|<a>找回</a></s></div></li>
             <li id="submenu8" class="submenu_1" style="display:none;">
                <div class="con_2">为了您的账户安全，请定期更换提现密码，并确保提现密码设置与登录密码不同。</div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;原提现密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;新提现密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_2 con_3">
                   <label class="label_1"><span>*</span>&nbsp;确认提现密码：</label>
                   <input class="password_1" value="" type="password">
                   <p>长度为6-16个字符之间</p>
                </div>
                <div class="con_4"><a>提交</a></div>
                <div class="con_2">如果您在操作过程中出现问题，请点击页面右侧在线客服，或拨打联富金融客服电话：4006-888-923</div>
             </li>
          </ul>
       </div>
    </div>
           
  </div>    
</div>



<%@ include file="/jsp/index/index_foot.jsp" %>



</body>
</html>


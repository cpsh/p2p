<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
	<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
	<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
 <%--<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
--%><span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">借款管理</a>&nbsp;<span>></span>&nbsp;我的借款
    </div>
    <%@include file="/jsp/user/usercenter/user_center_left.jsp" %>

	 <div class="geren_jyjl geren_wdzq geren_ujh geren_wdlk">
       <div class="left">
          <p>借款总金额</p><span><s>${borrow_all_money}</s>元</span>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>逾期未还金额</td>
                <td>待还金额</td>
             </tr>
             <tr>
                <td><span>${ over_weihuan}</span>元</td>
                <td><span id="to_repay_money">${weihuan}</span>元</td>
             </tr>

          </table>
       </div>    
    </div> 
    <div class="geren_wdzq_head"><span><a class="hover" href="javascript:void(0);">还款中借款</a><a href="javascript:void(0);" onclick="myFunction(),get_pay_off_loan();">已还清借款</a></span></div>    
    <div class="geren_wdzq_con" style="display:block">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
       
          <c:if test="${loan_info == null}">
	          <tr>
	             <td colspan="6" class="td_1">没有记录</td>
	          </tr>
          </c:if>
          
          <c:if test="${loan_info != null}">
          
          <tr class="tr_1">
             <td class="td_2" width="16%"><a onClick="showsubmenu(6),get_repayment_record();">还款</a></td>
             <td class="td_2" width="17%"><a href="#" style="background:#ff9900;" >提前还清</a></td>
             <td width="17%">&nbsp;</td>
             <td width="17%">&nbsp;</td>
             <td width="17%">&nbsp;</td>
             <td width="16%"><a href="#">查看合同</a></td>             
          </tr>
          
          
          <tr>
             <td>借款金额</td>
             <td>${loan_info.r.borrow_all_money}元</td>
             <td>年利率</td>
             <td>${loan_info.r.annulized_rate_int}%</td>
             <td>还款期数</td>
             <td>${loan_info.r.current_period}/${loan_info.r.total_periods}个月</td>
          </tr>
          <tr>
             <td>下个还款日</td>
             <td>${loan_info.r.automatic_repayment_date}</td>
             <td>还款金额</td>
             <td>${loan_info.r.should_repayment_total}元</td>
             <td>状态</td>
             <td>还款中</td>
          </tr>
          
          </c:if>
          
       </table>
    </div>  
    <div class="geren_wdzq_con">
       <table id="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="tr_1">
             <td width="14%">借款标题</td>
             <td width="14%">金额</td>
             <td width="14%">年利率</td>
             <td width="14%">期限</td>
             <td width="15%">还款总额</td>
             <td width="19%">还清日期</td>
             <td width="10%">合同</td>
          </tr>
          <!-- 
	          <tr>
	             <td width="14%">借款标题</td>
	             <td width="14%">金额</td>
	             <td width="14%">年利率</td>
	             <td width="14%">期限</td>
	             <td width="15%">还款总额</td>
	             <td width="19%">还清日期</td>
	             <td width="10%">合同</td>
	          </tr>
           -->
          
          <tr>
             <td colspan="7" class="td_1">没有记录</td>
          </tr>
       </table>
    </div> 
    
    <div id="submenu6" class="geren_wdjk" style="display:none;">
    	<div>
    		<span class="td_1">还款记录</span>
    	</div>
       <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
       	<!-- 
          <tr>
             <td class="td_1" width="9%">还款记录</td>
             <td width="14%">&nbsp;</td>
             <td width="14%">&nbsp;</td>
             <td width="14%">&nbsp;</td>
             <td width="14%">&nbsp;</td>
             <td width="14%">&nbsp;</td>
             <td width="14%">&nbsp;</td>
             <td width="7%">&nbsp;</td>
          </tr>
           -->
          <tr>
             <td>&nbsp;</td>
             <td>还款日</td>
             <td>应还金额</td>
             <td>应还本金</td>
             <td>应还利息</td>
             <td>借款管理费</td>
             <td>逾期费用</td>
             <td>状态</td>
          </tr>
 

       </table>
       
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="16%">已还总额</td>
             <td colspan="2" id="yihuan">0.00元</td>
          </tr>
          <tr>
             <td width="16%">待还总额</td>
             <td colspan="2" id="weihuan">0.00元</td>
          </tr>
          <tr>
             <td width="16%">本次还款金额</td>
             <td class="td_2" colspan="2"><span id="repay_this">0.00</span>元</td>
          </tr>
          <tr>
             <td width="16%">我的可用金额</td>
             <td width="18%" id="cny_can_used">0.00元</td>
             <td class="td_3" width="66%"><a href="<%=__ROOT_PATH__%>/user/usercenter/recharge/into_user_recharge_index.html">充值</a></td>
          </tr>
          <tr>
             <td class="td_4" colspan="2" width="16%"><span style="display:none" id="msg">您的余额不足，请先充值</span></td>
          </tr>
          <tr>
             <td class="td_5" colspan="2" width="16%"><a href="javascript:void(0);" onclick="confirm_repay();" >确认还款</a></td>
          </tr>
       </table>

    </div>


  </div>    
</div>



<div class="bottom"></div>

	<script src="<%=__PUBLIC__%>/js/loan_manage.js" type="text/javascript"></script>

	<script type="text/javascript">
		

		
		

	</script>

</body>
</html>


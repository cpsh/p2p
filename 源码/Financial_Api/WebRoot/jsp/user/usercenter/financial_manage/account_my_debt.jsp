<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
   
			             <script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
			             <link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css" rel="stylesheet" />
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script language="JavaScript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/u_jihua.js"></script>
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>
 <%--<script src="<%=__PUBLIC__%>/js/loginDialog_2.js" type="text/javascript"></script>
--%><span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">理财管理</a>&nbsp;<span>></span>&nbsp;我的债权
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
    <div class="geren_jyjl geren_wdzq">
       <div class="left">
          <p>债权已赚金额&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>债权已赚金额 = 债权已赚利息罚息及违约金 + 债权转让盈亏 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>
          </p><span><s>0.00</s>元</span>
          <p class="p_1">利息收益&nbsp;<a href="#"><img src="<%=__PUBLIC__%>/images/loan_3.png" /><span>利息收益 = 债权已赚利息罚息及违约金 - 债权转让应计利息支出<br />（由于购买转让的债权时可能需要先支付应计利息，所以此数值可能为负）<img src="<%=__PUBLIC__%>/images/loan_4.png" /></span></a>&nbsp;0.00元</p>
          <p class="p_1">债权转让盈亏&nbsp;&nbsp;0.00元</p>
       </div>
       <div class="right">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr class="tr_1">
                <td>债权账户资产</td>
                <td>回收中的债权数量</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td>0.00元</td>
                <td>0个</td>
                <td>&nbsp;</td>
             </tr>
             <tr>
                <td colspan="2">没时间亲自投标？试试联富金融自动投标功能吧。</td>
                <td><a href="#">自动投标</a></td>
             </tr>
          </table>
       </div>    
    </div> 
    <div class="geren_wdzq_head"><span><a class="hover" href="javascipt:void(0);" onclick="get_repaying_debt();">回收中的债权</a><a href="javascript:void(0);" onclick="get_pay_off_debt();">已结清的债权</a><a href="javascript:void(0);" onclick="get_bidding_debt();">投标中的债权</a><a href="#">已转出的债权</a></span><s><a href="#">回账查询</a></s></div>
    <div class="geren_wdzq_con" style="display:block">
       <table id="table1" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>待收本息</td>
             <td>月收本息</td>
             <td>期数</td>
             <td>下个还款日</td>
             <td>状态</td>
          </tr>
           <div id="1" class="loginDiv1"></div>
          <div id="2" class="loginDiv2" style=" z-index:10005; display:none;">
             <div class="geren_wdzq_div">                
                <input type="button" class="guanbi" onClick="closeme(1,2)" value="ｘ" />
                
                <div class="up"><span>债权转出</span>ID&nbsp;123425</div>
                <div class="up_con">
                   <label class="label_1">原始投资额</label>
                   <p class="P1">100元（<span>2份</span>）</p>
                </div>
                <div class="up_con">
                   <label class="label_1">已收利息</label>
                   <p class="P2"><span>0.00元</span><span>利率</span><span>0.00%</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">可转份数</label>
                   <p class="P2"><span>2份</span><span>剩余期数</span><span>24个月</span></p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让份数</label>
                   <input class="text_1" type="text" />
                </div>
                <div class="up_con">
                   <label class="label_1">当前债权价值</label>
                   <p class="P2"><span>35.12元/份</span><span>当前待收本息</span><span>49393元/份</span></p>
                </div>
                <div class="up_con up_con_2">一般为转让时的待回收本金与应计利息之和。<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">转让系数</label>
                   <select class="select_1">
                      <option>100%</option>
                      <option>10%</option>
                      <option>20%</option>
                   </select>
                </div>
                <div class="up_con up_con_2">此转让系数紧作用于债权待收本金部分</div>
                <div class="up_con">
                   <label class="label_1">转让价格</label>
                   <p class="P1">38.12元/份</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让总价格</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">转让费用</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con">
                   <label class="label_1">预计收入金额</label>
                   <p class="P1">38.12元</p>
                </div>
                <div class="up_con up_con_2">由于债权价值会发生变动，最终收入金额可能发生变动<a href="#">查看详情</a></div>
                <div class="up_con">
                   <label class="label_1">提现密码</label>
                   <input class="text_1" type="password"/>
                   <a class="a_1">忘记密码？</a>
                </div>
                <div class="up_con up_con_3">
                   <input class="checkbox_1" type="checkbox" />
                   <span>我已阅读并同意签署</span>
                   <a href="#">《债权转让及受让协议》</a>
                </div>
                <div class="up_con up_con_4"><a class="a" href="#">转让</a><a href="#">取消</a></div>
                <!--
                <div class="up_con up_con_5">
                   <img src="images/registration_pic8.png" />
                   <span>密码输入错误，您还剩余4次输入机会，请重新输入。</span>
                </div>
                -->
             </div>
          </div>
          
       </table>
    </div>   
    <div class="geren_wdzq_con">
       <table id="table2" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>投资金额</td>
             <td>年利率</td>
             <td>回收金额</td>
             <td>已赚金额</td>
             <td>结清日期</td>
             <td>结清方式</td>
             <td>&nbsp;</td>
          </tr>
          
          <!-- 
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>200.00(1份)</td>
             <td>10.00%</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2014-11-24</td>
             <td>还款</td>
             <td></td>
          </tr>
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>200.00(1份)</td>
             <td>10.00%</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2014-11-24</td>
             <td>还款</td>
          </tr>
           -->

          <tr>
             <td colspan="8" class="td_1">没有已结清的债权</td>
          </tr>
          
       </table>
    </div> 
    <div class="geren_wdzq_con">
        <table id="table3" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td>债权ID</td>
             <td>原始投资金额</td>
             <td>年利率</td>
             <td>期限</td>
             <td>信用等级</td>
             <td>剩余时间</td>
             <td>投标进度</td>
          </tr>
          
       </table>
    </div>
    <div class="geren_wdzq_con">
       <table id="table4" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
             <td width="14%">债权ID</td>
             <td width="10%">成交份数</td>
             <td width="18%">转出时债权总价值</td>
             <td width="17%">转出时总成交金额</td>
             <td width="12%">实际收入</td>
             <td width="11%">交易费用</td>
             <td width="10%">转让盈亏</td>
             <td width="8%">&nbsp;</td>
          </tr>
          
          <!-- 
          
          <tr class="tr_1">
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(6)">明细</a></td>
          </tr>
          <tr id="submenu6" class="tr_1" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="12%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="12%">转让价格</td>
                     <td width="10%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="10%">实际收入</td>
                     <td width="9%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="6%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          
          
          <tr>
             <td><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>123456</span></td>
             <td>2份</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>100.00</td>
             <td>2.00</td>
             <td>+20.00</td>
             <td><a onClick="showsubmenu(7)">明细</a></td>
          </tr>
          <tr id="submenu7" style="display:none;">
             <td colspan="8">
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td colspan="9" style="font-size:16px">债权转让明细</td>
                  </tr>
                  <tr>
                     <td width="12%">购买人</td>
                     <td width="16%">转出时债权价值</td>
                     <td width="12%">转让价格</td>
                     <td width="10%">成交份数</td>
                     <td width="11%">交易费用</td>
                     <td width="10%">实际收入</td>
                     <td width="9%">盈亏</td>
                     <td width="14%">成交时间</td>
                     <td width="6%">&nbsp;</td>
                  </tr>
                  <tr>
                     <td><span>123456</span></td>
                     <td>32.16/份</td>
                     <td>32.16/份</td>
                     <td>2份</td>
                     <td>0.38</td>
                     <td>78.00</td>
                     <td>-0.38</td>
                     <td>2014-11-12</td>
                     <td><a>合同</a></td>
                  </tr>
               </table>
             </td>
          </tr>
          
          -->
          
          <tr>
             <td colspan="8" class="td_1">没有已转出的债权</td>
          </tr>
          
          
       </table>
    </div>
  </div>    
</div>



<div class="bottom"></div>

	<script type="text/javascript">
		$(function(){
			get_repaying_debt();
		});


		//回收中的债权
		function get_repaying_debt(){

			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_repaying_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table1 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';
					//回收中新手标字符串
					var newer_str = '';

					if(jsObject.repaying_newer_bid.id != null){
						newer_str = 
							'<tr class="tr_1">'
				             + '<td><a href="javascript:void(0);"><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+  jsObject.repaying_newer_bid.newer_bid_gather_id +'</span></a></td>'
				             + '<td>5000元(100份)</td>'
				             + '<td>16%</td>'
				             + '<td>2.20</td>'
				             + '<td></td>'
				             + '<td>1/1</td>'
				             + '<td>'+ jsObject.repaying_newer_bid.recieve_date +'</td>'
				             + '<td>还款中</td>'
				             + '<td></td>'
				          + '</tr>';

					}
					
					
					if(jsObject.repaying_debt_list.length == 0 && jsObject.repaying_newer_bid.id == null){
						tr_str = '<tr><td colspan="8" class="td_1">没有回收中的债权</td></tr>';
					}else{
						for(var i=0;i<jsObject.repaying_debt_list.length;i++ ){
							tr_str += 
								'<tr class="tr_1">'
				             + '<td><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+ jsObject.repaying_debt_list[i].debt_id +'"><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>'+  jsObject.repaying_debt_list[i].debt_id +'</span></a></td>'
				             + '<td>'+ jsObject.repaying_debt_list[i].origin_money +'元('+ jsObject.repaying_debt_list[i].hold_share +'份)</td>'
				             + '<td>'+ jsObject.repaying_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td>' + jsObject.repaying_debt_list[i].remian_benxi.toFixed(2)+ '</td>'
				             + '<td>' +  jsObject.repaying_debt_list[i].month_benxi.toFixed(2)+ '</td>'
				             + '<td>' +  jsObject.repaying_debt_list[i].current_period+ '/'+ jsObject.repaying_debt_list[i].total_periods +'</td>'
				             + '<td>' + jsObject.repaying_debt_list[i].automatic_repayment_date +'</td>'
				             + '<td>还款中</td>'
				             + '<td><a onClick="openme(1,2)" >转让</a></td>'
				          + '</tr>';
						}
					}
					$("#table1 tr:eq(0)").after(newer_str + tr_str);
					
				}
			},"json");
		}


		//获取已结清债权
		function get_pay_off_debt(){

			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_pay_off_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table2 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';

					//新手标的字符串
					var newer_str = '';

					//查不到结果字符串
					var no_result_str = '';

					if(jsObject.pay_off_newer_bid.newer_bid_gather_id == null){
						no_result_str = '<tr><td colspan="8" class="td_1">没有已结清的债权</td></tr>';
					}
					else if(jsObject.pay_off_newer_bid.newer_bid_gather_id != null){
						newer_str = 
								'<tr>'
				             + '<td><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+ jsObject.pay_off_newer_bid.newer_bid_gather_id +'</span></td>'
				             + '<td>5000.00(10份)</td>'
				             + '<td>16%</td>'
				             + '<td>2.20</td>'
				             + '<td>2.20</td>'
				             + '<td>'+ jsObject.pay_off_newer_bid.recieve_date +'</td>'
				             + '<td></td>'
			     	    	+ '</tr>';
					}
					
					
					
					$("#table2 tr:eq(0)").after(newer_str + tr_str + no_result_str);
					
				}
			},"json");
			
		}
		


		//获取投标中的债权
		function get_bidding_debt(){
			var url = "<%=__ROOT_PATH__%>/user/usercenter/financial_manage/get_bidding_debt.html";
			$.ajax({
				url:url,
				type:'post',

				cache:false,
				success:function(data)
				{
					var jsObject = JSON.parse(data);
					$("#table3 tr:not(:first)").empty(); 

					//散标集合字符串
					var tr_str ='';

					//新手标的字符串
					var newer_str = '';

					//alert(jsObject.bidding_newer_bid.id == null);
					if(jsObject.bidding_newer_bid.id != null){
						newer_str = 
							'<tr class="tr_1">'
				             + '<td><a href="javascript:void(0);"><img src="<%=__PUBLIC__%>/images/icon2.jpg" /><span>'+  jsObject.bidding_newer_bid.id +'</span></a></td>'
				             + '<td>5000元(100份)</td>'
				             + '<td>16%</td>'
				             + '<td>1天</td>'
				             + '<td>A</td>'
				             + '<td>投满为止</td>'
				             + '<td>'+ jsObject.bidding_newer_bid.gather_progress +'%</td>'
				          + '</tr>';
					}
					
					if(jsObject.bidding_debt_list.length==0 && jsObject.bidding_newer_bid.id == null){
						tr_str = '<tr><td colspan="7" class="td_1">没有投标中的债权</td></tr>';
					}else{
						for(var i=0;i<jsObject.bidding_debt_list.length;i++){
							tr_str += 
								'<tr class="tr_1">'
				             + '<td><a href="<%=__ROOT_PATH__%>/user/financial/financial/loan_detail.html?gather_order_id='+ jsObject.bidding_debt_list[i].gather_id +'"><img src="<%=__PUBLIC__%>/images/icon1.jpg" /><span>'+  jsObject.bidding_debt_list[i].gather_id +'</span></a></td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].invest_money +'元('+ jsObject.bidding_debt_list[i].invest_share +'份)</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].annulized_rate_int +'%</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].borrow_duration +'个月</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].credit_rating +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].remain_time_str +'</td>'
				             + '<td>'+ jsObject.bidding_debt_list[i].gather_progress +'%</td>'
				          + '</tr>';
						}
					}
					
					$("#table3 tr:eq(0)").after(newer_str + tr_str);
					
				}
			},"json");
		}
		
	</script>

</body>
</html>


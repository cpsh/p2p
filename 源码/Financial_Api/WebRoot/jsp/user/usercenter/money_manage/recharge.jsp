<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
	 WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>${applicationScope.title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	 <meta http-equiv="keywords" content="${applicationScope.keywords}">
	 <meta http-equiv="description" content="${applicationScope.description}">  
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
  </head>
  
  <body>
    
<%@ include file="/jsp/index/index_top.jsp" %>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="geren.html">我的联富金融</a>&nbsp;<span>></span>&nbsp;<a href="#">资金管理</a>&nbsp;<span>></span>&nbsp;充值
    </div>
    <%@ include file="/jsp/user/usercenter/user_center_left.jsp" %>
  

     <div class="geren_cz">
       <div class="up">填写充值金额</div>
       <div class="geren_cz_con">
          <label class="label_1"><span>*</span>&nbsp;选择充值银行：</label>
          <dl class="select">
             <dt>请选择</dt>
             <dd>
                <ul>
                   <li><a>中国工商银行</a></li>
                   <li><a>中国农业银行</a></li>
                   <li><a>招商银行</a></li>
                   <li><a>中国银行</a></li>
                </ul>
             </dd>
          </dl> 
       </div>                  
       
       <div class="up_3">
          <label class="label_1">账户余额：</label>
          <p class="P1">${user_now_money.m.cny_can_used+user_now_money.m.cny_freeze }<span>元</span></p>
       </div>
       <div class="up_3 up_4">
          <label class="label_1"><span>*</span>&nbsp;充值金额：</label>
          <input class="input_1" value="" type="text">元
       </div>
       <div class="up_3">
          <label class="label_1">充值费用：</label>
          <p class="P1">0.00<span>元</span>
             <a class="a_a" href="#">
                <img src="<%=__PUBLIC__ %>/images/loan_3.png" />
                <span>充值费用按充值金额的0.5%由第三方平台收取，上限100元，超出部分由联富金融承担。<s></s></span>
             </a>
          </p>
       </div>
       <div class="up_3">
          <label class="label_1"><span>*</span>&nbsp;实际支付金额：</label>
          <p class="P1">0.00<span>元</span></p>
       </div>
       <div class="up_3 up_5">
          <label class="label_1"></label>
          <p class="p3"><a href="#">充&nbsp;值</a></p>
       </div>
       <div class="up_6">
          <p class="p3">             
             温馨提示：<br />
             1.&nbsp;为了您的账户安全，请在充值前进行身份认证、手机绑定以及提现密码设置。<br />
             2.&nbsp;您的账户资金将通过第三方平台进行充值。<br />
             3.&nbsp;请注意您的银行卡充值限制，以免造成不便。<br />
             4.&nbsp;禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用。<br />
             5.&nbsp;如果充值金额没有及时到账，请联系客服，4006-888-923。
          </p>
       </div>
    </div>
        
  </div>    
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
  </body>
</html>
<script language="JavaScript" type="text/javascript">
$(function(){
	$(".select").each(function(){
		var s=$(this);
		var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
})
</script>

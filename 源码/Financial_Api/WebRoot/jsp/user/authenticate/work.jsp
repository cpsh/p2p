<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">

	<body>
	<%@ include file="/jsp/user/financial/user_appointment.jsp"%>
		<%@ include file="/jsp/index/index_top.jsp"%>

		<span id="ny_pic"></span>

		<div class="about">
			<div class="about_con">
				<div class="loan_bb loan_bb_3">
					<div class="loan_aa_top">
						<div class="up">
			              <a href="#">我要借款</a>&nbsp;<span>></span>
			               <c:choose>
						   	<c:when test="${borrow_type==1}">&nbsp;消费贷&nbsp;</c:when>
						   	<c:when test="${borrow_type==2}">&nbsp;生意贷&nbsp;</c:when>
						   </c:choose>
			              <span>></span>&nbsp;填写借款申请
			           </div>
						<div class="center">
							<span style="background: #ed5050; color: #fff;">1</span>
							<s style="background: #ed5050;"></s>
							<span style="background: #ed5050; color: #fff;">2</span>
							<s></s>
							<span>3</span>
							<s></s>
							<span>4</span>
							<s></s>
							<span>5</span>
						</div>
						<div class="down">
							<span style="color: #ed5050;">填写借款申请</span>
							<span style="margin-left: 55px; color: #ed5050;">填写借款信息</span>
							<span style="margin-left: 60px;">审核</span>
							<span style="margin-left: 60px;">筹集借款</span>
							<span style="margin-left: 55px;">获得借款</span>
						</div>
					</div>

					<div class="loan_bb_con">
	<%@ include file="/jsp/user/authenticate/authenticate_left.jsp"%>
						<div class="right">
							<div class="right_top">
								工作信息
							</div>
							<div class="right_up">
								温馨提示：我们将在您的必要认证资料上传齐全后为您提交审核。
							</div>
							<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;职业状态：
								</label>
								<input onfocus="hide(12)" class="input_1" id="occupation_status" value="${work_info_list.m.occupation_status }" type="text">
								<p id="12" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请填写职业状态</p>
							</div>
							<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;单位名称：
								</label>
								<input onfocus="hide(0)" class="input_1" id="company_name" value="${work_info_list.m.company_name}" type="text">
								<p id="0" style="color: red;display: none" class="label_p7" > &nbsp;&nbsp;请填写单位名称</p>
							</div>
							<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;职位：
								</label>
								<input onfocus="hide(1)" class="input_1" id="position" value="${work_info_list.m.position}" type="text">
								<p id="1"  style="color: red; display: none" class="label_p7" > &nbsp;&nbsp;请填写职位名称</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;月收入：
								</label>
								<dl  class="select">
									<dt id="income">
										${work_info_list.m.monthly_income }
									</dt>
									<dd>
										<ul>
											<li>
												<a >1000元以下</a>
											</li>
											<li>
												<a >1001-2000元</a>
											</li>
											<li>
												<a >2000-5000元</a>
											</li>
											<li>
												<a >5000-10000元</a>
											</li>
											<li>
												<a >10000-20000元</a>
											</li>
											<li>
												<a >20000-50000元</a>
											</li>
											<li>
												<a >50000元以上</a>
											</li>
										</ul>
									</dd>
								</dl>
							<p id="2"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择月收入</p>
							</div>
							<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;工作邮箱：
								</label>
								<input onfocus="hide(3,301)" onblur="check_email('work_email',301)" class="input_1" id="work_email" value="${work_info_list.m.work_email}" type="text">
								<p id="3" style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请填写工作邮箱</p>
								<p id="301" style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;工作邮箱有误</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;工作城市：
								</label>
								<dl   style="margin-right: 20px;" class="select">
									<dt id="province" onclick="remove_city()" style="width: 109px;">
										${work_info_list.m.work_province}
									</dt>
									<dd style="width: 129px;">
										<ul >
											<c:forEach items="${work_province_list}" var="i">
												<li>
													<a>${i}</a>
												</li>
											</c:forEach>
										</ul>
									</dd>
								</dl>
								<dl  class="select" style="margin-right: 20px;" id="city">
									<dt onfocus="hide(4)" onclick="return_city('province','city')" style="width: 109px;">${work_info_list.m.work_city}</dt>
									<dd style="width: 129px;">
										<ul>
											<li><a> </a></li>
										</ul>
									</dd>
								</dl>
								<p id="4" style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请填选择工作省份城市</p>
							</div>
							<div class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;公司地址：
								</label>
								<input onfocus="hide(6,601)" onblur="check_addr('company_address',601)" class="input_1" id="company_address" value="${work_info_list.m.company_address}" type="text">
								<p id="6"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请填写公司地址</p>
								<p id="601"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;公司地址有误</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;公司类型：
								</label>
								<dl  class="select">
									<dt id="company_type">
										${work_info_list.m.company_type}
									</dt>
									<dd>
										<ul>
											<li>
												<a>国家机关</a>
											</li>
											<li>
												<a>事业单位</a>
											</li>
											<li>
												<a>央企（包括下级单位）</a>
											</li>
											<li>
												<a>地方国资委直属企业</a>
											</li>
											<li>
												<a>世界500强（包括合资企业及下级单位）</a>
											</li>
											<li>
												<a>外资企业（包括合资企业）</a>
											</li>
											<li>
												<a>一般上市公司（包括国外上市）</a>
											</li>
											<li>
												<a>一般民营企业</a>
											</li>
											<li>
												<a>个体经营者</a>
											</li>
											<li>
												<a>其它</a>
											</li>
										</ul>
									</dd>
								</dl>
								<p id="7"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择公司类型</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;公司行业：
								</label>
								<dl class="select">
									<dt id="company_trades">
										${work_info_list.m.company_trades}
									</dt>
									<dd>
										<ul>
											<li>
												<a  >制造业</a>
											</li>
											<li>
												<a  >IT</a>
											</li>
											<li>
												<a  >政府机关</a>
											</li>
											<li>
												<a  >媒体/广告</a>
											</li>
											<li>
												<a  >零售/批发</a>
											</li>
											<li>
												<a  >教育/培训</a>
											</li>
											<li>
												<a >公共事业</a>
											</li>
											<li>
												<a >交通运输业</a>
											</li>
											<li>
												<a >房地产业</a>
											</li>
											<li>
												<a >能源业</a>
											</li>
											<li>
												<a >金融/法律</a>
											</li>
											<li>
												<a >餐饮/旅馆业</a>
											</li>
											<li>
												<a >医疗/卫生/保障</a>
											</li>
											<li>
												<a >建筑工程</a>
											</li>
											<li>
												<a >农业</a>
											</li>
											<li>
												<a >娱乐服务业</a>
											</li>
											<li>
												<a >体育/艺术</a>
											</li>
											<li>
												<a >公益组织</a>
											</li>
											<li>
												<a >其它</a>
											</li>
										</ul>
									</dd>
								</dl>
								<p id="8"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择公司行业</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;公司规模：
								</label>
								<dl class="select">
									<dt id="company_size">
										${work_info_list.m.company_size}
									</dt>
									<dd>
										<ul>
											<li>
												<a >10人以下</a>
											</li>
											<li>
												<a >10-100人</a>
											</li>
											<li>
												<a >100-500人</a>
											</li>
											<li>
												<a  >500人以上</a>
											</li>
										</ul>
									</dd>
								</dl>
								<p id="9"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择公司规模</p>
							</div>
							<div class="con">
								<label class="label_1">
									<span>*</span>&nbsp;在现单位工作年限：
								</label>
								<dl class="select">
									<dt id="year_limit">
										${work_info_list.m.year_limit}
									</dt>
									<dd>
										<ul>
											<li>
												<a >1年（含）以下</a>
											</li>
											<li>
												<a >1-3年（含）</a>
											</li>
											<li>
												<a >3-5年（含）</a>
											</li>
											<li>
												<a >5年以上</a>
											</li>
										</ul>
									</dd>
								</dl>
								<p id="10"  style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请选择工作年限</p>
							</div>
							<div id="ex" class="con con4 con5">
								<label class="label_1">
									<span>*</span>&nbsp;公司电话：
								</label>
								<input onfocus="hide(11,111)" onblur="check_long_num('company_phone',111)" class="input_1" id="company_phone" value="${work_info_list.m.company_phone}" type="text">
								<p id="11" style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;请填写工作电话</p>
								<p id="111" style="color:red; display: none" class="label_p7" > &nbsp;&nbsp;工作电话有误</p>
							</div>
							<div class="con con6">
								<p class="label_p5">
								 <input type="hidden" id="apply_order_id" value="${apply_order_id}" />
									<a onclick="save();" >保存并继续</a>
								</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		</div>



	<%@ include file="/jsp/index/index_foot.jsp" %>
<script src="<%=__PUBLIC__%>/js/select.js"></script>
<script type="text/javascript">

function save(){
		var company_name = $("#company_name").val();
		var position = $("#position").val();
		var monthly_income = $("#income ").html();
		var work_email = $("#work_email").val();
		var work_province = $("#province").html();
		
		var work_city = $("#city dt").html();
		var company_address = $("#company_address").val();
		var company_type = $("#company_type").html();
		var company_trades = $("#company_trades").html();
		var company_size = $("#company_size").html();
		
		var year_limit = $("#year_limit").html();
		var company_phone = $("#company_phone").val();
		var apply_order_id = $("#apply_order_id").val();
		var occupation_status = $("#occupation_status").val();
		$.ajax(
			{	url:"<%=__ROOT_PATH__%>/user/authentication/work_info/save_work_info.html",
				type:'post',
				async:false,
				cache:false,
				data:{"company_name":company_name,"position":position,"monthly_income":monthly_income,"work_email":work_email,"work_province":work_province,"work_city":work_city,"company_address":company_address,"company_type":company_type,"company_trades":company_trades,"company_size":company_size,"year_limit":year_limit,"company_phone":company_phone,"occupation_status":occupation_status,"apply_order_id":apply_order_id},
				success:function(data){
				if(data==5){$("#4").show();}
				else if(data=="ok"){
				window.location.href="<%=__ROOT_PATH__ %>/user/authentication/assets_info/assets_index.html";
				}
				else if(data=="no"){
				alert("提交失败");
				}else{$("#"+data).show();
				}
			}
		},"html")
}

function hide(id,id2){
	$("#"+id).hide();
	$("#"+id2).hide();
}
function remove_city(){
	$("#city dt").html("");
}

//根据省 返回市
function return_city(_province,city){
		var province = $("#"+_province).html();
		$.ajax({
		type:'post',
		url:"<%=__ROOT_PATH__%>/user/authentication/return_city/return_city.html",
		async:false,
		cache:false,
		data:{"province":province},
		success:function(data){
			if(data!=null){
				var city_list=eval(data);
	            $("#"+city+" li").remove();
				for(var city_index in city_list){
			    var citys=city_list[city_index];
			    $("#"+city+" ul").append("<li><a>"+citys+"</a></li>");
		        }
			}
		}
	},"html");
		$("#"+city).each(function(){
		//var s=$(this);
		//var z=parseInt(s.css("z-index"));
		var dt=$(this).children("dt");
		var dd=$(this).children("dd");
		//var _show=function(){dd.slideDown(200);dt.addClass("cur");s.css("z-index",z+1);};   //展开效果
		var _hide=function(){dd.slideUp(200);dt.removeClass("cur");s.css("z-index",z);};    //关闭效果
		//dt.click(function(){dd.is(":hidden")?_show():_hide();});
		dd.find("a").click(function(){dt.html($(this).html());_hide();});     //选择效果（如需要传值，可自定义参数，在此处返回对应的“value”值 ）
		$("body").click(function(i){ !$(i.target).parents(".select").first().is(s) ? _hide():"";});
	})
}
</script>

</body>
</html>


<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request, __ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>

		<title>${applicationScope.title}</title>

		<meta http-equiv="keywords" content="${applicationScope.keywords}">
		<meta http-equiv="description" content="${applicationScope.description}">
	</head>

	<body>
		<%@ include file="/jsp/index/index_top.jsp"%>

		<span id="ny_pic"></span>


		<div class="zhiyin_con">
			<h1>
				新手指引
			</h1>
			<div class="zhiyin">
				<h2>
					<a class="hover"
						href="<%=__ROOT_PATH__%>/index/newguidance/invest.html">我要理财</a>
					<a href="<%=__ROOT_PATH__%>/index/newguidance/borrow.html">我要借款</a>
					<a href="<%=__ROOT_PATH__%>/index/newguidance/security.html">安全保障</a>
				</h2>
				<ul>
					<li class="zhiyin_list1">
						<h6 class="zhiyin_list1_h6">
							什么是联富金融
						</h6>
						<p>
							联富金融(lfoll.com)是目前中国互联网金融中P2P信贷行业的领军企业，是一个以个人对个人小额借贷为主要产品，为借贷两端搭建的公平、透明、安全、高效的互联网金融服务平台。
							<br />
							借款用户可以在联富金融上获得信用评级、发布借款请求来实现个人快捷的融资需要；理财用户可以把自己的部分闲余资金通过联富金融平台出借给信用良好有资金需求的个人，在获得有保障，高收益的理财回报的同时帮助了优质的借款人。
						</p>
						<img src="<%=__PUBLIC__%>/images/zhiyin_pic1.png" />
					</li>
					<li class="zhiyin_list2">
						<h6 class="zhiyin_list2_h6">
							为什么选择联富金融
						</h6>
						<p class="a">
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic2.png" />
							<span><s>16%~20%的高收益</s>成为理财人后，通过投资散标或加入联富宝，最高获得16%~20%的预期年化收益。<s>理财"零"门槛</s>只要您是18岁以上中国公民，最低50元就可以在联富金融进行理财。</span>
						</p>
						<p class="b">
							<span><s>100%本金保障计划</s>
								所有理财用户的每笔出借资金均在联富金融的本金保障计划覆盖之内，一旦出现逾期坏账，联富金融通过风险备用金优先垫付，保证理财人的资金安全。
								<s>资金灵活周转</s> 不用担心较长的资金锁定期，您可以进行债权转让，快速收回资金。</span>
							<img src="<%=__PUBLIC__%>/images/zhiyin_pic3.png" />
						</p>
					</li>
					<li class="zhiyin_list3">
						<h6 class="zhiyin_list3_h6">
							联富金融的业绩
						</h6>
						<p>
							<span>总交易金额达到3000万元</span> 累计总交易额达到3000万元
							<span>理财用户累计收益16万多元</span> 累计帮助理财用户实现收益16万元，平均收益率高达13.6%
							<span>累计交易1万人次</span> 网站从开展业务到现在交易次数达到1万人次
							<span>服务遍及全国2,000多个城市</span>
						</p>
						<img src="<%=__PUBLIC__%>/images/zhiyin_pic4.png" />
					</li>
					<li class="zhiyin_list4">
						<h6 class="zhiyin_list4_h6">
							理财，就是如此简单
						</h6>
						<p>
							<span>简单3步，完成理财投资</span> 第一步 完成注册成为理财人
							<br />
							第二步 账户充值
							第三步 加入联富宝或投资散标
							<br />
							第三步 加入联富宝或投资散标
							<br />
							三步走完，轻松赚取收益
							<br />
							<a href="<%=__ROOT_PATH__ %>/user/financial/financial/loan_list_elite.html">我要去理财</a>
						</p>
						<img src="<%=__PUBLIC__%>/images/zhiyin_pic5.png" />
					</li>
					<li class="zhiyin_list5">
						<h6 class="zhiyin_list5_h6">
							产品介绍
						</h6>
						<p>
							<a href="#">加入联富宝</a>
							加入联富宝，资金以用户认可的既定规则自动进行投标，用户每月的回款也会循环再投资，更安全，更便捷，更高效。
							<a href="#">投资散标</a>
							平台通过严格稳定的风险把控，提供了多种信用认证标的，用户可以根据借款人信用等级、利率、期限等信息，自选合适的借款标的，构建符合个人意愿的投资组合。
							<a href="#">债权转让</a>
							通过联富金融债权转让平台，用户可以自行转让资产组合中符合条件的债权，也可以购买其他用户转让的债权，从而获得折让收益及借款标的后续收益。
						</p>
						<img src="<%=__PUBLIC__%>/images/zhiyin_pic6.png" />
					</li>
				</ul>
			</div>
		</div>




		<%@ include file="/jsp/index/index_foot.jsp"%>

	</body>
</html>

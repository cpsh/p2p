<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="Utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="script/tab.js"></script>

</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="back_password">
        <div class="con">使用绑定邮箱找回密码</div>
        <div class="con_a">
           <label class="label_1"><span>*</span>手机号码：</label>
           <input class="input_1" type="text" name="phone" id="phone">
        </div>
        <div class="con_a">
           <label class="label_1"><span>*</span>验证码：</label>
           <input class="input_1" value="" type="text" name="code" id="code">
           <p class="label_p2" ><a href="#">
              <img id="CheckCodeServlet" width="122" height="42" src="<%=__ROOT_PATH__%>/code.jpg" onclick="refresh();" style="border: 1px; border:0px solid:0px;"/></a></p>
        </div>
         <script>
				function refresh(){
				var CheckCodeServlet=document.getElementById("CheckCodeServlet");
				CheckCodeServlet.src="<%=__ROOT_PATH__%>/code.jpg?id="+ Math.random();
				}
			</script>
        <div class="con"><a  id="yanzheng_code" style="text-decoration:none;" href="javascript:void(0);">提交</a></div>
        <div class="con_c">若您无法使用上述方法找回，请联系客服4006-888-923</div>
     </div>
  </div>  
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
</body>
</html>
<script type="text/javascript">
	$("#yanzheng_code").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","请填写手机号码"]);
			return;
		}
		var code = $("#code").val();
		if(code==null||code==""){
			showMessage(["提示","请填写图形验证码"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/index/back_password.html',
			{
				"phone":phone,
				"code":code
			},function(data){
				if(data == 1){
					showMessage(["提示","请输入手机号"]);
				}else if(data == 2){
					showMessage(["提示","请确认该手机号是否是您注册时的手机号"]);
				}else if(data == 3){
					showMessage(["提示","请填写图形验证码"]);
				}else if(data == 4){
					showMessage(["提示","图形验证码错误"]);
				}else if(data == 5){
					window.location.href="<%=__ROOT_PATH__%>/index/to_back_password2.html";
				}
				},'html');
	});
					
</script>
﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html">登录注册</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html">账户密码</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html">充值</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html">提现</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html">安全认证</a>
             <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html">消息中心</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">
             <div class="down_up"><span>&gt;</span>登录注册</div>
             
             <div onClick="showsubmenu(6)" class="district"><s>&bull;</s><span>什么是合作账号登录？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu6" class="area" style="display:none;">合作网站账号登录就是通过将联富金融账号与合作网站账号进行绑定，从而实现用合作网站的账号登录联富金融的功能。目前我们支持新浪微博、QQ合作网站账号登录。</div>
             
             <div onClick="showsubmenu(7)" class="district"><s>&bull;</s><span>如何关联合作账号？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu7" class="area" style="display:none;">
                如果您有联富金融账号，在联富金融合作账号登录页面输入账号、密码关联即可；<br />
                如果您没有联富金融账号，则需要在联富金融合作账号登录页面完善信息并进行手机短信认证，以便注册联富金融并关联账号。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(8)" class="district"><s>&bull;</s><span>如何进行合作账号登录？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu8" class="area" style="display:none;">
                <h1>合作网站尚未登录</h1>
                点击联富金融登录入口处该合作网站对应的图标，在合作网站账号登录页面中输入合作网站的账号、密码即可登录联富金融。<br />
                <h1>合作网站已经登录</h1>
                点击登录入口处该合作网站账号对应的图标，即可登录联富金融。
             </div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>注册时，进行手机验证，收不到短信怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none;">
                1.请确认手机是否安装短信拦截或过滤软件；<br />
                2.请确认手机是否能够正常接收短信（信号问题、欠费、停机等）；<br />
                3.短信收发过程中可能会存在延迟，请耐心等待，短信在30分钟内均有效；<br />
                4.您还可以联系客服，寻求帮助（4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>注册成功之后昵称可以修改吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                不可以。昵称一旦注册成功不能再修改。
             </div>
             
             <div class="down_up"><span>&gt;</span>账户密码</div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>联富金融账户的密码都有哪些？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                <h1>登录密码</h1>
                登录联富金融、合作账号关联联富金融账号时，需要输入的密码。<br />
                <h1>提现密码</h1>
                退出优选理财计划、债权转让、提现时需要输入的密码。
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>如何修改登录密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【登录密码】信息栏，点击【修改】，重置登录密码。<a href="#">查看操作流程>></a>
             </div>   
             
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>如何找回登录密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                打开登录页面，点击【忘记密码】；跳转至【找回密码】页面，可以选择通过绑定邮箱或者绑定手机号找回登录密码。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>通过邮箱找回登录密码时，收不到邮件，怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                请您在垃圾邮件中查找，如果没有，则可能是您邮箱服务器存在问题，你可以选择<a href="#">更换绑定邮箱</a>。
             </div>
             
             <div onClick="showsubmenu(15)" class="district"><s>&bull;</s><span>如何修改提现密码？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu15" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【提现密码】信息栏，点击【修改】，重置提现密码。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(16)" class="district"><s>&bull;</s><span>如何通过手机号找回提现密码?</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu16" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，找到【提现密码】信息栏，点击【找回】，通过绑定手机号找回并重置提现密码。<a href="#">查看操作流程>></a>
             </div>
             
             <div class="down_up"><span>&gt;</span>充值</div>
             
             <div onClick="showsubmenu(17)" class="district"><s>&bull;</s><span>充值介绍。</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu17" class="area" style="display:none;">
                联富金融用户可以通过与联富金融合作的第三方支付平台以及网上银行给联富金融账户充值，成功后可以用联富金融账户的余额进行理财。<br />
                如果通过网上银行支付，需要先开通网上银行功能。
             </div>
             
             <div onClick="showsubmenu(18)" class="district"><s>&bull;</s><span>如何申请开通网上银行？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu18" class="area" style="display:none;">
                目前所有商业银行都支持个人网银业务，您只需要携带有效身份证件，到当地您所持银行卡的发卡行任意营业网点，即可申请开通网上银行业务。您还可以到商业银行官网查看个人网上银行详细信息。
             </div>   
             
             <div onClick="showsubmenu(19)" class="district"><s>&bull;</s><span>如何给账户充值？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu19" class="area" style="display:none;">
                1.登录联富金融，打开【我的联富金融】页面，点击【充值】；<br />
                2.跳转至充值页面，选择充值银行，输入充值金额，点击【充值】；<br />
                3.跳转至银行或者第三方支付页面，按照页面的提示输入银行账户和密码等信息即可完成充值。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(20)" class="district"><s>&bull;</s><span>充值限额是多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu20" class="area" style="display:none;">
                充值限额是由银行支付限额、第三方支付平台支付限额和用户自己设定的支付限额三者共同决定，取三者最小值。<a href="#">查看详情>></a>
             </div>
             
             <div onClick="showsubmenu(21)" class="district"><s>&bull;</s><span>充值会不会扣手续费？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu21" class="area" style="display:none;">
                第三方支付平台将收取您充值金额的0.5%作为转账费用。扣除手续费的上限为100元。超过100元的部分将由联富金融承担。在充值时，用户也可以使用免费充值券，抵去该笔充值费用。
             </div>
             
             <div onClick="showsubmenu(22)" class="district"><s>&bull;</s><span>什么是免费充值券，怎么获得免费充值券？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu22" class="area" style="display:none;">
                免费充值券可用来抵去充值时第三方支付平台收取的转账费用，该笔费用将由联富金融承担。<br />
                每位新注册（实名认证、设置提现密码）用户，会自动获得一张免费充值券，同时联富金融还将不定期以不同形式的活动发放免费充值券。
             </div>
             
             <div class="down_up"><span>&gt;</span>提现</div>
             
             <div onClick="showsubmenu(23)" class="district"><s>&bull;</s><span>申请提现后，多久能到账？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu23" class="area" style="display:none;">
                联富金融收到用户提现申请后即对提现进行转账操作，由于不同银行处理速度不同，提现资金将会当天或下个工作日到账（如遇双休日或法定节假日顺延）。如果用户迟迟未收到提现资金，可能为银行卡信息填写有误，银行做正在退票操作，预计会在7个工作日内完成退票，请用户耐心等候。用户还可以联系客服（4006-888-923），寻求帮助。
             </div>
             
             <div onClick="showsubmenu(24)" class="district"><s>&bull;</s><span>提现手续费是多少？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu24" class="area" style="display:none">
                手续费将作为第三方平台的转账费用。第三方支付平台将按以下标准收取相关费用。<br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="20%">金额</td>
                      <td width="20%">2万元以下</td>
                      <td width="20%">2万(含)-5万元</td>
                      <td width="40%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="20%">手续费</td>
                      <td width="20%">1元/笔</td>
                      <td width="20%">3元/笔</td>
                      <td width="40%">5元/笔</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(25)" class="district"><s>&bull;</s><span>如何提现？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu25" class="area" style="display:none;">
                您可以随时将您在联富金融账户中的可用余额申请提现到您现有的任何一家银行的账号上。<a href="#">查看操作流程>></a><br />
                注意：请提供申请提现的银行卡账号，并确保该账号的开户人姓名和您在联富金融上提供的身份证上的真实姓名一致，否则无法成功提现。
             </div>
             
             <div onClick="showsubmenu(26)" class="district"><s>&bull;</s><span>添加银行卡，为什么要填写省市和开户行字段？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu26" class="area" style="display:none;">
                联富金融是通过第三方支付平台为用户提供付款服务，第三方支付平台将用户银行卡信息上传至银行做付款操作时，可能会进行省市和开户行字段的校验，如果为空或不正确可能会导致付款失败。<br />
                所以为了您提现的便利和安全，请您认真填写省市和开户行。
             </div> 
             
             <div onClick="showsubmenu(27)" class="district"><s>&bull;</s><span>添加银行卡，不知道银行卡的省市和开户行怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu27" class="area" style="display:none">
                1.您可以持银行卡及有效证件到发卡行的营业网点查询；<br />
                2.您可以拨打您银行卡开户行的24小时客服电话，选择人工服务，按照语音提示输入要查询的银行卡号和户主姓名，客服人员就会告知你开户行和所在地；<br />
                3.各大银行的客服电话：<br />
                <table class="table_4" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="20%">招商银行</td>
                      <td width="20%">中国银行</td>
                      <td width="20%">建设银行</td>
                      <td width="20%">工商银行</td>
                      <td class="td_1" width="20%">中信银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95555</td>
                      <td width="20%">95566</td>
                      <td width="20%">95533</td>
                      <td width="20%">95588</td>
                      <td class="td_1" width="20%">95558</td>
                   </tr>
                   <tr class="tr_1">
                      <td width="20%">农业银行</td>
                      <td width="20%">民生银行</td>
                      <td width="20%">光大银行</td>
                      <td width="20%">交通银行</td>
                      <td class="td_1" width="20%">广发银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95599</td>
                      <td width="20%">95568</td>
                      <td width="20%">95595</td>
                      <td width="20%">95559</td>
                      <td class="td_1" width="20%">400-830-8003</td>
                   </tr>
                   <tr class="tr_1">
                      <td width="20%">浦发银行</td>
                      <td width="20%">邮储银行</td>
                      <td width="20%">华夏银行</td>
                      <td width="20%">兴业银行</td>
                      <td class="td_1" width="20%">平安银行</td>
                   </tr>
                   <tr class="tr_2">
                      <td width="20%">95528</td>
                      <td width="20%">95580</td>
                      <td width="20%">95577</td>
                      <td width="20%">95561</td>
                      <td class="td_1" width="20%">95511转3</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(28)" class="district"><s>&bull;</s><span>如果所填写的银行卡信息不正确，能否提现成功？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu28" class="area" style="display:none;">
                1.如果您填写的银行卡号有误或该银行卡开户名不是您实名认证的姓名，不会提现成功；<br />
                2.如果您填写的银行、开户行、开户行所在地信息不正确，则该笔提现在提交到银行做处理时，有可能由于信息校验不正确而提现失败。
             </div>
             
             <div onClick="showsubmenu(29)" class="district"><s>&bull;</s><span>提现未到账怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu29" class="area" style="display:none;">
                1.可能是银行卡信息填写有误，银行做退票操作，大概会在7个工作日内完成退票，请用户耐心等候。<br />
                2.您还可以联系客服，寻求帮助（4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(30)" class="district"><s>&bull;</s><span>为什么会提现失败？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu30" class="area" style="display:none;">
                造成您提现失败的原因可能有以下几种：<br />
                1.银行账号/户名错误，或是账号和户名不符；<br />
                2.银行账户冻结或正在办理挂失；<br />
                3.省份、城市、开户行等银行信息错误；<br />
                4.使用信用卡提现。<br />
                如果遇到以上情况，我们会在收到支付机构转账失败的通知后解除您的资金冻结（手续费不退还），请您不必担心资金安全。
             </div>
             
             <div class="down_up"><span>&gt;</span>安全认证</div>
             
             <div onClick="showsubmenu(31)" class="district"><s>&bull;</s><span>为什么要进行安全认证？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu31" class="area" style="display:none;">
                为了保障用户资金的安全性和合同的有效性，联富金融要求所有理财人及借款人必须通过身份证绑定、手机号绑定以及提现密码设置。安全认证的过程简单便捷，联富金融对于所有个人资料均作严格保密。
             </div>
             
             <div onClick="showsubmenu(32)" class="district"><s>&bull;</s><span>如何进行实名认证？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu32" class="area" style="display:none;">
                1.用户可以在【注册成功】页面进行实名认证；<br />
                2.用户可以打开【我的联富金融】--【账户管理】--【安全信息】页面，在实名认证信息栏，点击【设置】，输入姓名和身份证号，进行实名认证。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(33)" class="district"><s>&bull;</s><span>身份信息正确，实名认证未通过怎么办？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu33" class="area" style="display:none;">
                可能是由于第三方认证系统没有及时更新您的身份信息导致的。您可以联系客服寻求帮助<br />
                （4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(34)" class="district"><s>&bull;</s><span>进行实名认证后，能解绑或重新认证吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu34" class="area" style="display:none;">
                1.用户在网站不可以进行解绑操作；<br />
                2.如果账户资金余额为0且没有借款和理财记录的用户，可以联系客服解绑（4006-888-923）。
             </div>
             
             <div onClick="showsubmenu(35)" class="district"><s>&bull;</s><span>一个手机号能绑定几个联富金融账户？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu35" class="area" style="display:none;">
                一个
             </div>
             
             <div onClick="showsubmenu(36)" class="district"><s>&bull;</s><span>如何修改绑定手机号？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu36" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，用户可以选择通过原手机号或者身份证号进行修改。<a href="#">查看操作流程>></a>
             </div>
             
             <div onClick="showsubmenu(37)" class="district"><s>&bull;</s><span>如何更换绑定邮箱？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu37" class="area" style="display:none;">
                登录联富金融，打开【我的联富金融】--【账户管理】--【安全信息】页面，用户可以选择通过原邮箱或绑定手机号修改绑定邮箱。<a href="#">查看操作流程>></a>
             </div>
             
             <div class="down_up"><span>&gt;</span>消息中心</div>
             
             <div onClick="showsubmenu(38)" class="district"><s>&bull;</s><span>这些消息对我有什么用？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu38" class="area" style="display:none;">
                如果您的账户信息发生变动，我们就会推送消息给您，这些消息与您的账户息息相关，包括账户安全、交易变化、金额变化等等。使您及时收到您联富金融账户有关信息的变动情况。
             </div>
             
             <div onClick="showsubmenu(39)" class="district"><s>&bull;</s><span>我可以退订一部分消息吗？</span><a href="#"><img src="<%= __PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu39" class="area" style="display:none;">
                您可以打开【我的联富金融】页面，点击【互动管理】--【通知设置】，勾选适合自己的信息推送方式及内容；点击【保存设置】保存设置结果。<a href="#">查看操作流程>></a>
             </div>     
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp"%>

</body>

</html>

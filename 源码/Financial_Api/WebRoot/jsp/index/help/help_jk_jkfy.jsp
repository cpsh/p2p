﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />
<script type="text/javascript">
function showsubmenu(sid){
    var whichEl = document.getElementById("submenu" + sid);
    whichEl.style.display = whichEl.style.display =='none'?'':'none';
}
</script>
</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="geren_up">
       <a href="index.html">首页</a>&nbsp;<span>></span>&nbsp;帮助中心
    </div>
    <div class="geren_left">
      <ul>
        <li><a href="<%=__ROOT_PATH__ %>/help/help_index.html"><span></span>帮助中心</a></li>
        <li onClick="showsubmenu(1)"><a href="<%=__ROOT_PATH__%>/help/help_lc.html"><span></span>理财帮助</a></li>
        <li id="submenu1" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_lc_xsbd.html"><span></span>新手必读</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_syyfy.html"><span></span>收益与费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_lfb.html"><span></span>联富宝</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_sbtz.html"><span></span>散标投资</a>
           <a href="<%=__ROOT_PATH__%>/help/help_lc_zqzr.html"><span></span>债权转让</a>
        </li>
        <li onClick="showsubmenu(2)"><a style="background:#ed5050;color:#fff;" href="<%=__ROOT_PATH__%>/help/help_jk.html"><span></span>借款帮助</a></li>
        <li id="submenu2" class="submenu">
           <a href="<%=__ROOT_PATH__%>/help/help_jk_cpjs.html"><span></span>产品介绍</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_jkfy.html" style="background:#f8bbb2;color:#fff;"><span></span>借款费用</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhsq.html"><span></span>如何申请</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rzzl.html"><span></span>认证资料</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xysh.html"><span></span>信用审核</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_xydjyed.html"><span></span>信用等级与额度</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_ckytx.html"><span></span>筹款与提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_jk_rhhk.html"><span></span>如何还款</a>
        </li>
        <li onClick="showsubmenu(3)"><a href="<%=__ROOT_PATH__%>/help/help_zh.html"><span></span>账户管理</a></li>
        <li id="submenu3" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_zh_dlzc.html"><span></span>登陆注册</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_zhmm.html"><span></span>账户密码</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_cz.html"><span></span>充值</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_tx.html"><span></span>提现</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_aqrz.html"><span></span>安全认证</a>
           <a href="<%=__ROOT_PATH__%>/help/help_zh_xxzx.html"><span></span>消息中心</a>
        </li>
        <li onClick="showsubmenu(4)"><a href="<%=__ROOT_PATH__%>/help/help_aq.html"><span></span>安全保障</a></li>
        <li id="submenu4" class="submenu" style="display:none;">
           <a href="<%=__ROOT_PATH__%>/help/help_aq_bjbzjh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本金保障计划</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_flyzcbz.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;法律与政策保障</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_jkshyfk.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;借款审核与风控</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_zhjysaq.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;账户及隐私安全</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_yhdzwbh.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户的自我保护</a>
           <a href="<%=__ROOT_PATH__%>/help/help_aq_wzxgxy.html">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;网站相关协议</a>
        </li>
        <li onClick="showsubmenu(5)"><a href="<%=__ROOT_PATH__%>/help/help_mc.html"><span></span>名词解释</a></li>
      </ul>
    </div> 
    
    <div class="help_lc">
       <div class="up">
          <div class="up_con">
             <span>索引</span>
             <a href="#">产品介绍</a>
             <a href="#">借款费用</a>
             <a href="#">如何申请</a>
             <a href="#">认证资料</a>
             <a href="#">信用审核</a>
             <a href="#">信用等级与额度</a>
             <a href="#">筹款与提现</a>
             <a href="#">如何还款</a>
          </div>
       </div>
       <div class="down">
          <div class="down_con">             
             
             <div class="down_up"><span>&gt;</span>借款费用</div>
             
             <div onClick="showsubmenu(9)" class="district"><s>&bull;</s><span>借款服务费</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu9" class="area" style="display:none">
                联富金融收取的借款服务费将全部存于风险备用金账户用于联富金融的本金保障计划。<br />
                服务费将按照借款人的信用等级来收取：<br />
                <table class="table_3" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="16%">信用等级</td>
                      <td width="12%"><span>AA</span></td>
                      <td width="12%"><span>A</span></td>
                      <td width="12%"><span>B</span></td>
                      <td width="12%"><span>C</span></td>
                      <td width="12%"><span>D</span></td>
                      <td width="12%"><span>E</span></td>
                      <td width="12%"><span>HR</span></td>
                   </tr>
                   <tr>
                      <td width="16%">服务费率</td>
                      <td width="12%">0%</td>
                      <td width="12%">1%</td>
                      <td width="12%">1.5%</td>
                      <td width="12%">2%</td>
                      <td width="12%">2.5%</td>
                      <td width="12%">3%</td>
                      <td width="12%">5%</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(10)" class="district"><s>&bull;</s><span>借款管理费</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu10" class="area" style="display:none;">
                联富金融将按照借款人的借款期限，每月向借款人收取其借款本金的0.3%作为借款管理费。
             </div>
             
             <div onClick="showsubmenu(11)" class="district"><s>&bull;</s><span>充值提现费用</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu11" class="area" style="display:none;">
                充值/提现时由第三方支付收取的支付费用。<br />
                <h1>充值费用</h1>
                第三方支付平台将收取您充值金额的0.5%作为转账费用。扣除手续费的上限为100元。超过100元的部分将由联富金融承担。<br />
                在充值时，用户也可以使用<a href="#">免费充值券</a>，抵去该笔充值费用。<a href="#">如何获得免费充值券？</a><br />
                <h1>提现费用</h1>
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="25%">金额</td>
                      <td width="25%">2万元以下</td>
                      <td width="25%">2万(含)-5万元</td>
                      <td width="25%">5万(含)-100万元</td>
                   </tr>
                   <tr>
                      <td width="25%">手续费</td>
                      <td width="25%">1元/笔</td>
                      <td width="25%">3元/笔</td>
                      <td width="25%">5元/笔</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(12)" class="district"><s>&bull;</s><span>逾期罚息</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu12" class="area" style="display:none;">
                当用户的借款发生逾期时，正常利息停止计算。按照下面公式计算罚息：<br />
                <img src="<%=__PUBLIC__%>/images/help_3.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="40%">逾期天数</td>
                      <td width="30%">1—30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="40%">罚息利率</td>
                      <td width="30%">0.05%</td>
                      <td width="30%">0.1%</td>
                   </tr>
                </table>
             </div>
                          
             <div onClick="showsubmenu(13)" class="district"><s>&bull;</s><span>逾期管理费</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu13" class="area" style="display:none;">
                用户的借款发生逾期时，正常借款管理费用停止计算。按照下面公式计算：<br />
                <img src="<%=__PUBLIC__%>/images/help_18.png" /><br />
                <table class="table_1" width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr class="tr_1">
                      <td width="40%">逾期天数</td>
                      <td width="30%">1—30天</td>
                      <td width="30%">31天以上</td>
                   </tr>
                   <tr>
                      <td width="40%">逾期管理费率</td>
                      <td width="30%">0.1%</td>
                      <td width="30%">0.5%</td>
                   </tr>
                </table>
             </div>
             
             <div onClick="showsubmenu(14)" class="district"><s>&bull;</s><span>联富金融年利率范围</span><a href="#"><img src="<%=__PUBLIC__%>/images/help_ck.png" /></a></div>
             <div id="submenu14" class="area" style="display:none;">
                联富金融目前的利率范围为10%-24%。在联富金融平台上借贷的最高年利率设定为同期银行借款年利率的4倍。且随着银行借款利率的调整，联富金融的利率上限也将随之调整。<br />
                注：<br />
                1.联富金融的利率的调整会在商业银行借款年利率调整后1个月内进行调整。<br />
                2.在利率调整之前成功的借款不受调整的影响。
             </div>  
               
          </div>
       </div>
    </div>
    
  </div>    
</div>


<%@ include file="/jsp/index/index_foot.jsp" %>
</body>

</html>

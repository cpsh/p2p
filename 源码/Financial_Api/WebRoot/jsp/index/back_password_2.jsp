<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
WebApp.setCharacterEncoding(request,response);
    String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   

<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="script/tab.js"></script>

</head>
<body>

<%@ include file="/jsp/index/index_top.jsp"%>

<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
     <div class="back_password_2">
        <div class="con">联富金融手机验证短信已发送至您的手机</div>
        <div class="con con_b">${phone }
         </div>
        <div class="con_a">
           <label class="label_1"><span>*</span>收到的验证码：</label>
           <input type="hidden" value="${phone }" name="phone" id="phone"/>
          <input class="label_p"  id="binding_phone_code" style="margin-bottom: 5px;" type="text">
           <input class="button_2" style="background-color: #ed5050;color:white;"  type="button" id="binding_phone_send_message" value="发送验证码" />
			<input class="button_2" style="display:none;background-color: #ed5050;color:white;" type="button" id="binding_phone_send_voice" value="语音验证码" />
        	<br/><span id="bingd_phone" style="display:none;margin-left: 350px;">没有收到短信验证码 ?<a style="corsur:pointer;color:red;" id="bingd_phone_send">发送语音验证码</a></span>
        </div>
        <div class="con"><a id="tijiao" type="text-decoration:none;" href="javascript:void(0);">提交</a></div>
     </div>
  </div>  
</div>

<%@ include file="/jsp/index/index_foot.jsp" %>
</body>
</html>
<script type="text/javascript">
	//找回密码发送验证码
	//倒计时
					var a = 1;
					var b;
					function countdown1(){
						if(a==90){
							a= 0;
							clearTimeout(b);
							$("#binding_phone_send_message").val("发送验证码");
							$("#binding_phone_send_message").attr("disabled",false);
							$("#bingd_phone").show();
							return;		
						}else{
							$("#binding_phone_send_message").val((90-a)+"秒后可重发");
							$("#binding_phone_send_message").css("color","white");
							$("#binding_phone_send_message").attr("disabled",true);
							a++;
							b = setTimeout("countdown1()",1000);
						}
					}
					
			
	$("#binding_phone_send_message").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendmessage/back_password_send_message.html',
			{
				"phone":phone
			},
			function(data){
				if(data == 4){
					showMessage(["提示","手机号不能为空"]);
				}else if(data == 1){
					showMessage(["提示","该手机号已经被绑定"]);
				}else if(data == 2){
					showMessage(["提示","请使用语音验证码"]);
					$("#bingd_phone").show();
				}else{
					//showMessage(["提示","短信验证码发送成功"]);
					countdown1();
				}
			},'html','application/x-www-form-urlencoded; charset=utf-8');
	});
	$("#bingd_phone_send").click(function(){
		$("#binding_phone_send_message").hide();
		$("#binding_phone_send_voice").show();
	});
	var a1 = 1;
	var b1;
	function countdown11(){
		if(a1==90){
			a1= 0;
			clearTimeout(b1);
			$("#binding_phone_send_voice").val("语音验证码");
			$("#bingd_phone").show();
			$("#binding_phone_send_voice").attr("disabled",false);
			return;		
		}else{
			$("#binding_phone_send_voice").val((90-a1)+"秒后可重发");
			$("#binding_phone_send_voice").css("color","white");
			$("#binding_phone_send_voice").attr("disabled",true);
			a1++;
			b1 = setTimeout("countdown11()",1000);
		}
	}
	$("#binding_phone_send_voice").click(function(){
		var phone = $("#phone").val();
		if(phone==null||phone==""){
			showMessage(["提示","手机号码不能为空"]);
			return;
		}
		if(!(/^[1][3458][0-9]{9}$/.test(phone))){
			showMessage(["提示","手机格式不对"]);
			return;
		}
		$.post('<%=__ROOT_PATH__%>/user/sendvoice/back_password_send_voice.html',
				{
					"phone":phone
				},
				function(data){
					if(data ==1){
						showMessage(["提示","短信验证码发送失败,请使用语音验证码"]);
						$("#bingd_phone").show();
						
					}else if(data == 2){
						//showMessage(["提示","短信验证码发送成功"]);
						countdown11();
					}
				},'html','application/x-www-form-urlencoded; charset=utf-8');
	});
	
	$("#tijiao").click(function(){
		var binding_phone_code = $("#binding_phone_code").val();
		if(binding_phone_code==null||binding_phone_code==""){
			showMessage(["提示","验证码不能为空"]);
		} 
		$.post('<%=__ROOT_PATH__%>/index/find_password_emial_code_isok.html',
		{
			"page_code" : binding_phone_code
		},function(data){
			if(data == 1){
				showMessage(["提示","验证码不能为空"]);
			}else if(data == 2){
				showMessage(["提示","验证码错误"]);
			}else if(data == 3){
				window.location.href="<%=__ROOT_PATH__%>/index/to_update_password_page.html";
			}
		});		
	});
</script>











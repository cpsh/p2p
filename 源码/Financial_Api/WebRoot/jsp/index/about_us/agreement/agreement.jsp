<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="com.tfoll.web.util.WebApp"%>
<%
    WebApp.setCharacterEncoding(request,response);
	String __ROOT_PATH__ = WebApp.getWebRootPath(request);
	String __PUBLIC__ = WebApp.getPublicPath(request,__ROOT_PATH__);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>${applicationScope.title}</title>
<meta http-equiv="keywords" content="${applicationScope.keywords}">
<meta http-equiv="description" content="${applicationScope.description}">   
   
<link href="style/main.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"  />

<script language="JavaScript" type="text/javascript" src="script/tab.js"></script>

</head>
<body>
<%@ include file="/jsp/index/index_top.jsp"%>
<span id="ny_pic"></span>

<div class="about">
  <div class="about_con">
    <div class="about_left">
    			<%-- 
      				<ul>
        				<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=intro" >公司简介</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=team" >管理团队</a>
						</li>
						
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=consult" >最新资讯</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=notice">网站公告</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=agreement" class="hover">网站协议</a>
						</li>
					
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=join_us">加入我们</a>
						</li>
						<li>
							<a href="<%=__ROOT_PATH__%>/index/about/about_page.html?flag=contact" >联系我们</a>
						</li>
     				 </ul>
     			--%>
     			${list_str}
    </div>
    <div class="about_right">
      <h2>网站协议</h2>
      <h3>
         <p>法律声明</p>         
         1.&nbsp;&nbsp;&nbsp;&nbsp;本网站只为符合中华人民共和国法律规定的具有完全民事权利和民事行为能力，能够独立承担民事责任的自然人提供产品及服务。所发布的信息无意提供给不符合上述限定的人士。<br /><br />
         2.&nbsp;&nbsp;&nbsp;&nbsp;除非另外约定，本网站所刊载所有内容（包括但不限于商标、标志、文本、图片、数据等）及网站设计的版权均归联富金融及其权利人依法拥有。未经联富金融或其权利人书面同意，任何人不得擅自使用、修改、复制、公开发布联富金融程序或内容。<br /><br />
         3.&nbsp;&nbsp;&nbsp;&nbsp;本网站为用户提供借贷交易居间服务时，会根据实际情况进行评估。本网站不承诺所有用户都能享受所有的产品和服务。对于用户是否符合享受产品和服务的条件，本网站具有最终的解释权。<br /><br />
         4.&nbsp;&nbsp;&nbsp;&nbsp;本网站尊重并保护所有用户的个人隐私权。本网站严格依照国家法律法规的规定保护用户的个人信息。下列情况惠民公司有权披露用户的个人信息。<br />
         &nbsp;&nbsp;&nbsp;&nbsp;1）&nbsp;&nbsp;事先经过用户的明确授权时；<br />
         &nbsp;&nbsp;&nbsp;&nbsp;2）&nbsp;&nbsp;相关机构依照法定程序要求本网站进行披露时；<br />
         &nbsp;&nbsp;&nbsp;&nbsp;3）&nbsp;&nbsp;为维护本网站其他用户及社会公众合法权益而有必要披露时。<br /><br />
         5.&nbsp;&nbsp;用户通过本网站发布的交易信息，及本网站提供的评价信息均仅供参考。用户应在独立判断后作出交易决策。<br /><br />
         6.&nbsp;&nbsp;本网站力求发布客观真实的信息，但无法保证所有资料准确无误。由完全信赖该资料而产生的法律后果，本网站不承担任何责任。<br /><br />
         7.&nbsp;&nbsp;为了方便用户，本网站允许用户通过链接访问其他站点。但本网站无法保证其他站点的安全性和可靠性。用户由网站链接访问到其他站点而造成的损失，本网站不承担任何责任。<br /><br />
         8.&nbsp;&nbsp;&nbsp;&nbsp;本网站合作机构提供的服务由该合作单位负责。用户享用相关服务时产生的争议、纠纷及损失，本网站不承担任何责任。<br /><br />
         9.&nbsp;&nbsp;&nbsp;&nbsp;本网站特定产品及服务可能包含单独条款，作为对本声明的补充。如有任何冲突，该条款仅对相关服务适用。<br /><br />
         10.&nbsp;&nbsp;&nbsp;&nbsp;鉴于互联网传输的特殊性，本网站无法保证所有交易的准确性及及时性。由本网站不可控因素而导致的任何法律后果，本网站不承担任何责任。<br /><br />
         11.&nbsp;&nbsp;&nbsp;&nbsp;本声明及其他附属协议的修改权、更新权及最终解释权均归本网站所有。<br /><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_1.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务计划授权委托书</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_2.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务计划预约服务协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_3.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融自动投标服务协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_4.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝出借咨询与服务协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_5.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融债权转让协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_6.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融借款及服务协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_7.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融注册协议</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_8.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务（12个月）退出确认书</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_9.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务（3个月）退出确认书</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_10.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务（6个月）退出确认书</a><br />
        <%--
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_11.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务（9月期）退出确认书</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_12.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富宝服务（1月期）退出确认书</a><br />
         --%>
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_13.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融出借人服务协议（2014年版）</a><br />
         <a href="<%=__ROOT_PATH__%>/index/about/agreement_14.html" target="_blank"><img src="<%=__PUBLIC__%>/images/agreement.png" />&nbsp;&nbsp;&nbsp;&nbsp;联富金融借款人服务协议（2014年版）</a>
      </h3>       
    </div>
  </div>
  
</div>
<%@ include file="/jsp/index/index_foot.jsp" %>

</body>
</html>

var path = "http://"+window.location.host;
		var validate_is_login = path + "/user/financial/lfollinvest/validate_user_is_logined.html";//校验是否登陆
		var login_url = path+"/index/to_login.html";//
		var new_fix_bid_path = "";
		
		function goto_fix_bid_detail(id){
			window.location.href = path+"/user/financial/lfollinvest/get_lfoll_invest_info.html?id="+id;
		}

		/**
		* 弹出 DIV
		**/
		function buy_experience(){
			/**
	  		 * 判断用户是否登陆
	  		 */
	  	  	$.ajax({
	  	  		url:validate_is_login,
	  	  		type:"post",
	  	  		async:false,
	  	  		success:function(data){
	  	  			if(data != null){
	  	  				if(data == "非理财身份"){
	  	  					showMessage(["提示","非理财账户，请先注册理财账户!"]);
	  	  					return;
	  	  				}else if(data == "NoLogined"){
	  	  					window.location.href = login_url;
	  	  					return;
	  	  				}else{
			  	  			refresh_order_pay();
			  	  			$("#validate_code").val("");
			  	  			$("#submit_new_bid").attr("onclick","sumbmit_new_order()");
			  	  			document.getElementById('loginDiv1').style.display='block';
			  	  			document.getElementById('loginDiv2').style.display='block';
	  	  				}
	  	  			}
	  	  		}
	  	  	});
			
		}

		/**
		  *关闭DIV
		  **/
		function close_order_pay(){
			document.getElementById('loginDiv1').style.display='none';
	  		document.getElementById('loginDiv2').style.display='none';
		}

		/**
	  	 * 获取验证码：
	  	 * @return
	  	 */
	  	function refresh_order_pay(){
			var CheckCodeServlet=document.getElementById("CheckCodeServlet");
				CheckCodeServlet.src= path+"/code.jpg?id="+ Math.random();
		}

		
		
		/**
		 * 跳转到新手标详情页面
		 * @return
		 */
		function go_to_newer_fix_bid_detail(){
			
			window.location.href = path + "/user/financial/lfollinvest/goto_new_fix_bid_detail.html";
		}
package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "_____activity___reward_record", primaryKey = "user_id")
public class _____Activity___Reward_RecordM extends Model<_____Activity___Reward_RecordM> implements Serializable {

	private static final long serialVersionUID = 1262241552051672915L;
	public static _____Activity___Reward_RecordM dao = new _____Activity___Reward_RecordM();
}

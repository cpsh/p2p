package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_bank_info", primaryKey = "user_id")
public class UserBankInfoM extends Model<UserBankInfoM> {
	public static UserBankInfoM dao = new UserBankInfoM();
}

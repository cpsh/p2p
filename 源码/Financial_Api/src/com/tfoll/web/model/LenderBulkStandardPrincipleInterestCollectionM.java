package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_principle_interest_collection", primaryKey = "id")
public class LenderBulkStandardPrincipleInterestCollectionM extends Model<LenderBulkStandardPrincipleInterestCollectionM> {
	public static LenderBulkStandardPrincipleInterestCollectionM dao = new LenderBulkStandardPrincipleInterestCollectionM();
}

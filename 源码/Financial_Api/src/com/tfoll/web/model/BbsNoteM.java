package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "bbs_note", primaryKey = "id")
public class BbsNoteM extends Model<BbsNoteM> {
	public static BbsNoteM dao = new BbsNoteM();
}

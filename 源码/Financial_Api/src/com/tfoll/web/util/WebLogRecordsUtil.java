package com.tfoll.web.util;

import javax.servlet.http.HttpServletRequest;

public class WebLogRecordsUtil {
	@Deprecated
	public static String getRemoteAddrIP2(HttpServletRequest request) {
		if (request.getHeader("x-forwarded-for") == null) {
			return request.getRemoteAddr();
		} else {
			return request.getHeader("x-forwarded-for");
		}
	}

	/**
	 *获取远程IP客户信息
	 */
	public static String getIpAddr(HttpServletRequest request) {
		if (request == null) {
			return "request is null";
		}
		return getVistiedInfo(request);
	}

	/**
	 *果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
	 * 
	 * 　　答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。如：
	 * X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
	 * 192.168.1.100 用户真实IP为： 192.168.1.110
	 */
	public static String getRemoteAddrIp(HttpServletRequest request) {
		if (request == null) {
			return "request is null";
		}
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getVistiedInfo(HttpServletRequest request) {
		if (request == null) {
			return "request is null";
		}
		String url = request.getServletPath();
		String query_string = request.getQueryString();
		String referer = request.getHeader("Referer");
		return "url:" + url + "query_string" + query_string + "referer" + referer + "from ip:" + WebLogRecordsUtil.getRemoteAddrIp(request);
	}

	/**
	 * 访问的地址
	 */
}

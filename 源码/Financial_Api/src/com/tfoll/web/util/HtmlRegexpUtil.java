package com.tfoll.web.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Title: HTML相关的正则表达式工具类 Description: 包括过滤HTML标记，转换HTML标记，替换特定HTML标记
 */

public class HtmlRegexpUtil {
	private final static String regxpForHtml = "<([^>]*)>"; // 过滤所有以<开头以>结尾的标签

	private final static String regxpForImgTag = "<\\s*img\\s+([^>]*)\\s*>"; // 找出IMG标签

	private final static String regxpForImaTagSrcAttrib = "src=\"([^\"]+)\""; // 找出IMG标签的SRC属性

	/**  
     *   
     */
	public HtmlRegexpUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * 基本功能：替换标记以正常显示
	 * <p>
	 * 
	 * @param input
	 * @return String
	 */
	public String replaceTag(String input) {
		if (!hasSpecialChars(input)) {
			return input;
		}
		StringBuffer filtered = new StringBuffer(input.length());
		char c;
		for (int i = 0; i <= input.length() - 1; i++) {
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");
				break;
			case '>':
				filtered.append("&gt;");
				break;
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			default:
				filtered.append(c);
			}

		}
		return (filtered.toString());
	}

	/**
	 * 
	 * 基本功能：判断标记是否存在
	 * <p>
	 * 
	 * @param input
	 * @return boolean
	 */
	public boolean hasSpecialChars(String input) {
		boolean flag = false;
		if ((input != null) && (input.length() > 0)) {
			char c;
			for (int i = 0; i <= input.length() - 1; i++) {
				c = input.charAt(i);
				switch (c) {
				case '>':
					flag = true;
					break;
				case '<':
					flag = true;
					break;
				case '"':
					flag = true;
					break;
				case '&':
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * 基本功能：过滤所有以"<"开头以">"结尾的标签
	 * <p>
	 * 
	 * @param str
	 * @return String
	 */
	public static String filterHtml(String str) {
		Pattern pattern = Pattern.compile(regxpForHtml);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * tfoll 过滤文章内容
	 * 
	 * @param str
	 * @return
	 */
	public static String filterHtmlContent4Show(String str) {
		// 首先去掉<>
		String msg = "";
		Pattern pattern = Pattern.compile(regxpForHtml);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		msg = sb.toString();
		msg = msg.trim();

		msg = msg.replace("&nbsp ;", "");
		msg = msg.replace("nbsp;", "");
		msg = msg.replace("&nbs;", "");
		msg = msg.replace("&quot;", "");
		msg = msg.replace("&amp;", "&");
		msg = msg.replace("&lt;", "<");
		msg = msg.replace("&gt;", ">");
		msg = msg.replace("&", "");
		return msg;
	}

	/**
	 * 
	 * 基本功能：过滤指定标签
	 * <p>
	 * 
	 * @param str
	 * @param tag
	 *            指定标签
	 * @return String
	 */
	public static String fiterHtmlTag(String str, String tag) {
		String regxp = "<\\s*" + tag + "\\s+([^>]*)\\s*>";
		Pattern pattern = Pattern.compile(regxp);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 
	 * 基本功能：替换指定的标签
	 * <p>
	 * 
	 * @param str
	 * @param beforeTag
	 *            要替换的标签
	 * @param tagAttrib
	 *            要替换的标签属性值
	 * @param startTag
	 *            新标签开始标记
	 * @param endTag
	 *            新标签结束标记
	 * @return String
	 * @如：替换img标签的src属性值为[img]属性值[/img]
	 */
	public static String replaceHtmlTag(String str, String beforeTag, String tagAttrib, String startTag, String endTag) {
		String regxpForTag = "<\\s*" + beforeTag + "\\s+([^>]*)\\s*>";
		String regxpForTagAttrib = tagAttrib + "=\"([^\"]+)\"";
		Pattern patternForTag = Pattern.compile(regxpForTag);
		Pattern patternForAttrib = Pattern.compile(regxpForTagAttrib);
		Matcher matcherForTag = patternForTag.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result = matcherForTag.find();
		while (result) {
			StringBuffer sbreplace = new StringBuffer();
			Matcher matcherForAttrib = patternForAttrib.matcher(matcherForTag.group(1));
			if (matcherForAttrib.find()) {
				matcherForAttrib.appendReplacement(sbreplace, startTag + matcherForAttrib.group(1) + endTag);
			}
			matcherForTag.appendReplacement(sb, sbreplace.toString());
			result = matcherForTag.find();
		}
		matcherForTag.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 
	 * 获取文章内容中第一张图片的地址
	 * 
	 * @param str
	 * @return
	 */
	public static String getFirstImage(String str) {
		try {
			int indexFirst = str.indexOf("https");
			// System.out.println("--------http出现的index---------"+indexFirst);
			int indexJPG = str.indexOf(".jpg");// 10002
			int indexPNG = str.indexOf(".png");// 100
			int indexGIF = str.indexOf(".gif");// 1231

			List<Integer> intList = new ArrayList<Integer>();
			if (indexJPG > 0) {
				intList.add(indexJPG);
			}
			if (indexPNG > 0) {
				intList.add(indexPNG);
			}
			if (indexGIF > 0) {
				intList.add(indexGIF);
			}

			// 如果没有图片，集合长度为0
			if (intList.size() == 0) {
				return "";
			} else if (intList.size() == 1) {
				return str.substring(indexFirst, intList.get(0) + 4);
			} else if (intList.size() == 2) {
				int int_first = intList.get(0);
				int int_two = intList.get(1);
				if (int_first < int_two) {
					return str.substring(indexFirst, int_first + 4);
				} else {
					return str.substring(indexFirst, int_two + 4);
				}
			} else if (intList.size() == 3) {
				int int_first = intList.get(0);
				int int_two = intList.get(1);
				int int_three = intList.get(2);
				if (int_first < int_two && int_first < int_three) {
					return str.substring(indexFirst, int_first + 4);
				} else if (int_two < int_first && int_two < int_three) {
					return str.substring(indexFirst, int_two + 4);
				} else if (int_three < int_first && int_three < int_two) {
					return str.substring(indexFirst, int_three + 4);
				} else {
					return "";
				}
			} else {
				return "";
			}

		} catch (IndexOutOfBoundsException e) {
			return "";
		}
	}
}
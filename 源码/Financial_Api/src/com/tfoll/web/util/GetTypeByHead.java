package com.tfoll.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

/**
 * 获取和判断文件头信息
 * 
 */
public class GetTypeByHead {
	// 缓存文件头信息-文件头信息
	public static final HashMap<String, String> File_Header_Type_Map = new HashMap<String, String>();
	static {
		// images
		File_Header_Type_Map.put("FFD8FF", "jpg");
		File_Header_Type_Map.put("FFD8FFE0", "jpeg");
		File_Header_Type_Map.put("89504E47", "png");
		File_Header_Type_Map.put("47494638", "gif");
		File_Header_Type_Map.put("49492A00", "tif");
		File_Header_Type_Map.put("424D", "bmp");
		//
		File_Header_Type_Map.put("41433130", "dwg"); // CAD
		File_Header_Type_Map.put("38425053", "psd");
		File_Header_Type_Map.put("7B5C727466", "rtf"); // 日记本
		File_Header_Type_Map.put("3C3F786D6C", "xml");
		File_Header_Type_Map.put("68746D6C3E", "html");
		File_Header_Type_Map.put("44656C69766572792D646174653A", "eml"); // 邮件
		File_Header_Type_Map.put("D0CF11E0", "doc");
		File_Header_Type_Map.put("5374616E64617264204A", "mdb");
		File_Header_Type_Map.put("252150532D41646F6265", "ps");
		File_Header_Type_Map.put("255044462D312E", "pdf");
		File_Header_Type_Map.put("504B0304", "docx");
		File_Header_Type_Map.put("52617221", "rar");
		File_Header_Type_Map.put("57415645", "wav");
		File_Header_Type_Map.put("41564920", "avi");
		File_Header_Type_Map.put("2E524D46", "rm");
		File_Header_Type_Map.put("000001BA", "mpg");
		File_Header_Type_Map.put("000001B3", "mpg");
		File_Header_Type_Map.put("6D6F6F76", "mov");
		File_Header_Type_Map.put("3026B2758E66CF11", "asf");
		File_Header_Type_Map.put("4D546864", "mid");
		File_Header_Type_Map.put("1F8B08", "gz");
		File_Header_Type_Map.put("4D5A9000", "exe/dll");
		File_Header_Type_Map.put("75736167", "txt");
	}

	/**
	 * 根据文件路径获取文件头信息
	 * 
	 * @param filePath
	 *            文件路径
	 * @return 文件头信息
	 */
	public static String getFileType(File file) {
		if (file == null) {
			throw new NullPointerException("file is null");
		}
		String fileHeader = getFileHeader(file);
		if (fileHeader == null || "".equals(fileHeader)) {
			throw new NullPointerException("fileHeader is null or ''");
		}
		String fileHeaderType = File_Header_Type_Map.get(fileHeader);
		if (fileHeaderType == null || "".equals(fileHeaderType)) {
			throw new NullPointerException("fileHeaderType is null or ''");
		}
		return fileHeaderType;
	}

	/**
	 * 根据文件路径获取文件头信息
	 * 
	 * @param filePath
	 *            文件路径
	 * @return 文件头信息
	 */
	private static String getFileHeader(File file) {
		if (file == null) {
			throw new NullPointerException("file is null");
		}
		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			byte[] bytes = new byte[4];
			is.read(bytes, 0, bytes.length);
			return bytesToHexString(bytes);
		} catch (Exception e) {
			return null;
		} finally {
			if (null != is) {
				try {
					is.close();
				} catch (IOException e) {
					//
				}
			}
		}

	}

	/**
	 * 二进制转化为16进制
	 */
	private static String bytesToHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		if (bytes == null || bytes.length <= 0) {
			throw new NullPointerException("bytes is null or length<=0");
		}
		String hex;
		int length = bytes.length;
		for (int i = 0; i < length; i++) {
			hex = Integer.toHexString(bytes[i] & 0xFF).toUpperCase();
			if (hex.length() < 2) {
				sb.append(0);
			}
			sb.append(hex);
		}
		return sb.toString();
	}

	public static class Image {
		public static final Set<String> Image_Type_Set = new HashSet<String>();
		static {
			// images
			Image_Type_Set.add("jpg");
			Image_Type_Set.add("jpeg");
			Image_Type_Set.add("png");
			Image_Type_Set.add("gif");
			Image_Type_Set.add("tif");
			Image_Type_Set.add("bmp");

		}

		@SuppressWarnings("unchecked")
		public static String getImageFormatName(File file) {
			ImageInputStream iis = null;

			try {
				iis = ImageIO.createImageInputStream(file);
				Iterator i = ImageIO.getImageReaders(iis);
				if (!i.hasNext()) {
					return null;
				}
				ImageReader reader = (ImageReader) i.next();
				return reader.getFormatName();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (iis != null) {
					try {
						iis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}
	}

	public static void main(String[] args) throws Exception {

		// 文件操作
		File file = new File("C:/Users/ibm/Desktop/1.jpg");
		final String file_type = GetTypeByHead.getFileType(file);

		System.out.println(file_type);
		if (file_type == null) {
			System.out.println("不是文件-但是系统根本执行不到这来");
			return;
		}
		/**
		 * 先判断是不是文件-才能判断是不是图片
		 */
		// 下面是图片操作
		if (Image.Image_Type_Set.contains(file_type)) {
			String image_name = GetTypeByHead.Image.getImageFormatName(file);
			if (image_name == null) {
				System.out.println("不是图片");
			} else {
				System.out.println("是图片");
			}
		}

	}
}
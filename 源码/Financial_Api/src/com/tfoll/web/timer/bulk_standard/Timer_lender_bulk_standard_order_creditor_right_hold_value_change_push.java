package com.tfoll.web.timer.bulk_standard;

import com.googlecode.asyn4j.core.callback.AsynCallBack;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.plugin.AsynServicePlugin;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightHoldValueChangePushM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.util.Utils;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *理财的时候债权价值改变消息推送定时器
 */
public class Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			// logger.debug("系统框架还没有启动好,定时任务:" +
			// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName()
			// + "暂时还不能执行");
			return;
		}
		if (isRunning.get()) {
			// logger.debug("还有其他的线程正在执行定时任务:" +
			// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				// logger.debug("定时任务:" +
				// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName()
				// + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e);
						e.printStackTrace();
					}

				}
				// logger.debug("定时任务:" +
				// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName()
				// + "结束");
			} catch (Exception e) {
				// logger.debug("定时任务:" +
				// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.class.getName()
				// + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}

			} finally {
				isRunning.set(false);
			}
		}

	}

	public static final ConcurrentSkipListSet<LenderBulkStandardOrderCreditorRightHoldValueChangePushM> Lender_Bulk_Standard_Order_Creditor_Right_Hold_Value_Change_Push_Set = new ConcurrentSkipListSet<LenderBulkStandardOrderCreditorRightHoldValueChangePushM>();

	/**
	 * 创建对象一般不会抛出异常
	 * 
	 * @param lender_bulk_standard_order_creditor_right_transfer_out_list
	 */
	public static boolean add_creditor_right_hold_value_change_push(List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list) {
		if (Utils.isHasData(lender_bulk_standard_order_creditor_right_transfer_out_list)) {
			ConcurrentSkipListSet<LenderBulkStandardOrderCreditorRightHoldValueChangePushM> lender_bulk_standard_order_creditor_right_hold_value_change_push_sub_set = new ConcurrentSkipListSet<LenderBulkStandardOrderCreditorRightHoldValueChangePushM>();
			for (LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out : lender_bulk_standard_order_creditor_right_transfer_out_list) {
				/**
				 * 里面只要id
				 */
				long creditor_right_transfer_out_id = lender_bulk_standard_order_creditor_right_transfer_out.getLong("id");
				lender_bulk_standard_order_creditor_right_hold_value_change_push_sub_set.add(LenderBulkStandardOrderCreditorRightHoldValueChangePushM.create_creditor_right_hold_value_change_push(creditor_right_transfer_out_id));
			}
			if (lender_bulk_standard_order_creditor_right_hold_value_change_push_sub_set.size() > 0) {
				Lender_Bulk_Standard_Order_Creditor_Right_Hold_Value_Change_Push_Set.addAll(lender_bulk_standard_order_creditor_right_hold_value_change_push_sub_set);
			}
		}
		return true;

	}

	/**
	 * 添加异步添加债权转让价值变动通知
	 * 
	 */
	public static boolean add_creditor_right_hold_value_change_push_asyn_request(List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list) {
		try {
			AsynServicePlugin.asynService.addWork(Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.AddCreditorRightHoldValueChangePush.class, //
					"add_creditor_right_hold_value_change_push", //
					new Object[] { lender_bulk_standard_order_creditor_right_transfer_out_list },//

					new Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.AddCreditorRightHoldValueChangePush.AAsynCallBack());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		return true;
	}

	public static class AddCreditorRightHoldValueChangePush {
		public void add_creditor_right_hold_value_change_push(List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list) {
			Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.add_creditor_right_hold_value_change_push(lender_bulk_standard_order_creditor_right_transfer_out_list);
		}

		public static class AAsynCallBack extends AsynCallBack {
			private static final long serialVersionUID = 1L;

			@Override
			public void doNotify() {
			}
		}

	}

	/**
	 * 具体业务处理
	 */
	private void doTask() throws Exception {
		if (Lender_Bulk_Standard_Order_Creditor_Right_Hold_Value_Change_Push_Set.size() > 0) {
			for (LenderBulkStandardOrderCreditorRightHoldValueChangePushM lender_bulk_standard_order_creditor_right_hold_value_change_push : Lender_Bulk_Standard_Order_Creditor_Right_Hold_Value_Change_Push_Set) {
				try {
					Lender_Bulk_Standard_Order_Creditor_Right_Hold_Value_Change_Push_Set.remove(lender_bulk_standard_order_creditor_right_hold_value_change_push);
					lender_bulk_standard_order_creditor_right_hold_value_change_push.save();
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e);
				}
			}
		} else {
			TimeUnit.MICROSECONDS.sleep(1);
		}

	}

}
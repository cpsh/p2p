package com.tfoll.web.domain;

import java.util.Comparator;

/**
 * ConcurrentSkipListMap中Key为long类型的比较
 */
public class ConcurrentSkipListMapLongKeyComparator implements Comparator<Long> {

	public int compare(Long o1, Long o2) {
		long $o1 = o1.longValue();
		long $o2 = o2.longValue();

		if ($o1 > $o2) {
			return 1;
		} else if ($o1 < $o2) {
			return -1;
		} else {
			return 0;
		}

	}

}

package com.tfoll.web.domain;

import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import org.apache.log4j.Logger;

/**
 * 定义抢标等待状态-散标
 * 
 */
public class BulkStandardBiddingStatus {

	/**
	 * 添加等待状态需要加入写锁,修改状态信息需要加上读锁即可
	 */
	private Date bid_end_date = null;
	private AtomicInteger waiting_num = new AtomicInteger(0);

	public BulkStandardBiddingStatus() {
		super();
		this.bid_end_date = new Date(new Date().getTime() + Manager.Sacn_Interval);
	}

	public boolean is_can_end() {
		Date now = new Date();
		return now.after(bid_end_date);
	}

	public void increase() {
		waiting_num.incrementAndGet();
	}

	public void decrease() {
		waiting_num.decrementAndGet();
	}

	/**
	 * 是否达到最大值
	 */
	public boolean is_max() {
		int num = waiting_num.get();
		if (num >= Manager.Max_Waitting_Num) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 管理器
	 * 
	 */
	public static class Manager {
		public static final Logger logger = Logger.getLogger(Manager.class);
		/**
		 * 定义抢标等待队列
		 * 
		 */
		public static final ConcurrentMap<Long, BulkStandardBiddingStatus> Bulk_Standard_Bidding_Status_Map = new ConcurrentHashMap<Long, BulkStandardBiddingStatus>();
		public static final ReentrantReadWriteLock Read_Write_Lock = new ReentrantReadWriteLock();
		public static final long Max_Waitting_Num = 30;
		public static final long Sacn_Interval = 1000 * 60 * 4;// 4分钟扫描一次

		public static int num_for(long order_id) {
			BulkStandardBiddingStatus bidding_status = Bulk_Standard_Bidding_Status_Map.get(order_id);
			if (bidding_status == null) {
				return 0; // 可能已经被删除了
			}
			try {
				return bidding_status.waiting_num.get();// 队列里面为空或者找不到这个对象
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e);
				return 0;// 对象为null的时候调用添加计数器的方法来增加一个对象
			}
		}

		/**
		 * 判断一个计数器对象是否达到最大值
		 */
		public static boolean is_max(long order_id) {
			BulkStandardBiddingStatus bidding_status = Bulk_Standard_Bidding_Status_Map.get(order_id);
			if (bidding_status == null) {
				return false; // 可能已经被删除了
			}
			try {
				return bidding_status.is_max();// 队列里面为空或者找不到这个对象
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e);
				return false;// 对象为null的时候调用添加计数器的方法来增加一个对象
			}
		}

		/**
		 * 配置状态的最大失效时间
		 */

		public static void increase_waiting_num(long order_id) {
			BulkStandardBiddingStatus bidding_status = Bulk_Standard_Bidding_Status_Map.get(order_id);
			if (bidding_status == null) {
				WriteLock write_lock = Read_Write_Lock.writeLock();
				write_lock.lock();
				try {
					if (bidding_status == null) {
						bidding_status = new BulkStandardBiddingStatus();
						Bulk_Standard_Bidding_Status_Map.put(order_id, bidding_status);
					}
				} finally {
					write_lock.unlock();
				}

			}

			try {
				if (bidding_status.waiting_num.get() >= Max_Waitting_Num) {
					return;
				}
				bidding_status.increase();// 队列里面为空或者找不到这个对象
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e);
			}

		}

		/**
		 * 只有增加后才能有减少计数器这个操作
		 */
		public static void decrease_waiting_num(long order_id) {
			BulkStandardBiddingStatus bidding_status = Bulk_Standard_Bidding_Status_Map.get(order_id);
			if (bidding_status == null) {
				return;// 可能已经被删除了
			}

			try {
				if (bidding_status.waiting_num.get() <= 0) {
					return;
				}
				bidding_status.decrease();// 队列里面为空或者找不到这个对象
			} catch (Exception e) {
				// e.printStackTrace();
				logger.error(e);
			}
		}

		public static void sacn() {
			for (Entry<Long, BulkStandardBiddingStatus> entry : Bulk_Standard_Bidding_Status_Map.entrySet()) {
				long key = entry.getKey();
				try {
					BulkStandardBiddingStatus value = entry.getValue();
					if (value == null) {
						Bulk_Standard_Bidding_Status_Map.remove(key);// 此处被删掉了-其他地方可能是空指针错误
					} else {
						if (value.is_can_end()) {
							Bulk_Standard_Bidding_Status_Map.remove(key);// 此处被删掉了-其他地方可能是空指针错误
						}
					}

				} catch (Exception e) {
					// e.printStackTrace();
					logger.error(e);
				}

			}
		}

		/**
		 * 建立计划任务-清除过期的散标状态对象
		 */
		public static void schedule() {
			ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
			exec.scheduleWithFixedDelay(new Runnable() {
				public void run() {
					try {
						sacn();
					} catch (Exception e) {
						// e.printStackTrace();
						logger.error(e);
					}
				}
			}, 1, 60, TimeUnit.SECONDS);// 1分钟扫描一次
		}

	}

	public static void main(String[] args) {
		System.out.println(BulkStandardBiddingStatus.Manager.is_max(1));
		for (int i = 1; i <= 10; i++) {
			BulkStandardBiddingStatus.Manager.increase_waiting_num(1);// increase
			System.out.println(BulkStandardBiddingStatus.Manager.num_for(1));
		}
		System.out.println(BulkStandardBiddingStatus.Manager.is_max(1));// is_max
		for (int i = 1; i <= 10; i++) {
			BulkStandardBiddingStatus.Manager.decrease_waiting_num(1);// decrease_waiting_num
			System.out.println(BulkStandardBiddingStatus.Manager.num_for(1));
		}
		System.out.println(BulkStandardBiddingStatus.Manager.is_max(1));
	}

}

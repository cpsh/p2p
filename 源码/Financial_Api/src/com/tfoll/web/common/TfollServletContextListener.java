package com.tfoll.web.common;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * 
 * 通过在一个static类里面注册一个ServletContextListener实例-将ServletContext分享出来
 * 
 */
public class TfollServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {

		ServletContext context = servletContextEvent.getServletContext();
		TfollServletContextMapExchanger.registerServletContext(context);

		setSeoInfo(context);

	}

	/**
	 * 设置Seo信息到Application
	 */
	private void setSeoInfo(ServletContext context) {
		context.setAttribute("title", "联富金融-专业安全的互联网金融服务平台 网络P2P理财贷款投资");
		context.setAttribute("keywords", "联富金融-专业全面的互联网金融服务平台 保本P2P理财贷款投资");
		context.setAttribute("description", "联富金融是中国领先的综合型互联网金融平台，提供贷款，理财，保险，投融资等金融服务。理财投资轻松安全，5重安全保障，100%本息担保。贷款高效便捷提供更优质的服务");
	}

}

package com.tfoll.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserLoginFilter implements Filter {

	public void destroy() {

	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;

		HttpSession session = request.getSession();
		Object user = (Object) session.getAttribute("user");
		if (user == null) {
			HttpServletResponse response = (HttpServletResponse) servletResponse;
			response.sendRedirect("https://www.tfoll.com");
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}

	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

}

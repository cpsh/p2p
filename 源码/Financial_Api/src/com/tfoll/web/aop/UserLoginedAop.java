package com.tfoll.web.aop;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.RootRender;
import com.tfoll.web.model.UserM;

import javax.servlet.http.HttpSession;

/**
 * 判读普通用户是否登录--暂时这么做
 * 
 */
public class UserLoginedAop implements Interceptor {

	public void doIt(ActionExecutor ae) {
		HttpSession session = ActionContext.getRequest().getSession();
		UserM user = (UserM) session.getAttribute("user");
		if (user == null) {
			ActionContext.getRequest().setAttribute("loginLink", "1");
			ActionContext.setRender(new RootRender("/index/login.jsp"));
			return;
		}
		Object user_id_object = user.get("id");
		if (user_id_object != null) {
			ae.invoke();
		} else {
			ActionContext.getHttpSession().invalidate();// 清空session
			ActionContext.getRequest().setAttribute("loginLink", "1");
			ActionContext.setRender(new RootRender("/index/login.jsp"));
			return;
		}

	}

}

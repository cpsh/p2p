package com.tfoll.web.action.index;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.action.user.financial.LfollInvestAction;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.common.code.CheckCodeServlet;
import com.tfoll.web.model.AboutInfoM;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.SystemNotificationM;
import com.tfoll.web.model.UserAuthenticateAssetsInfoM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticateLeftStatusM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserBankInfoM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserEmailRequestWaitingM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model._____Activity_1_Recommend_UrlM;
import com.tfoll.web.util.GetCode;
import com.tfoll.web.util.HtmlRegexpUtil;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * 该类无须登录即可访问
 * 
 */
@ActionKey("/index")
public class IndexAction extends Controller {

	/**
	 * 普通用户登录
	 * 
	 * @throws Exception
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入首页", last_update_author = "曹正辉")
	public void index() throws Exception {
		// 查询最新资讯
		String select_least_consult_sql = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '最新资讯' ORDER BY t1.id DESC";
		List<AboutInfoM> least_consult_list = AboutInfoM.dao.find(select_least_consult_sql);
		// 查询网站公告
		String select_site_notic_sql = "SELECT t1.* FROM about_info t1, about_type t2 WHERE t2.id = t1.type_id AND t2.type_name = '网站公告' ORDER BY t1.id DESC";
		List<AboutInfoM> site_notic_list = AboutInfoM.dao.find(select_site_notic_sql);

		List<Record> least_consult_record_list = new ArrayList<Record>();
		List<Record> site_notic_record_list = new ArrayList<Record>();
		if (least_consult_list != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for (AboutInfoM aboutInfoM : least_consult_list) {
				Record least_consult_record = new Record();
				int id = aboutInfoM.getInt("id");
				String content = aboutInfoM.getString("content");
				content = HtmlRegexpUtil.filterHtmlContent4Show(content);
				if (content.length() > 30) {
					content = content.substring(0, 25);
					content = content + "...";
				}

				Date add_time = aboutInfoM.getTimestamp("add_time");
				String add_time_str = format.format(add_time);

				least_consult_record.add("id", id);
				least_consult_record.add("content", content);
				least_consult_record.add("add_time_str", add_time_str);
				least_consult_record_list.add(least_consult_record);
			}
			setAttribute("least_consult_record_list", least_consult_record_list);

		} else {
			setAttribute("tips_1", "暂无最新公告");
		}
		if (site_notic_list != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for (AboutInfoM aboutInfoM : site_notic_list) {
				Record site_notic_record = new Record();
				int id = aboutInfoM.getInt("id");
				String content = aboutInfoM.getString("content");
				content = HtmlRegexpUtil.filterHtmlContent4Show(content);
				if (content.length() > 30) {
					content = content.substring(0, 25);
					content = content + "...";
				}

				Date add_time = aboutInfoM.getTimestamp("add_time");
				String add_time_str = format.format(add_time);

				site_notic_record.add("id", id);
				site_notic_record.add("content", content);
				site_notic_record.add("add_time_str", add_time_str);
				site_notic_record_list.add(site_notic_record);
			}
			setAttribute("site_notic_record_list", site_notic_record_list);

		} else {
			setAttribute("tips_2", "暂无最新公告");
		}

		/**
		 * 获取推荐url
		 */

		String url = getParameter("url");
		if (Utils.isNotNullAndNotEmptyString(url) && url.length() == 32) {// 必须是32位
			// 进行处理
			setSessionAttribute(SystemConstantKey.Recommend_Url, url);
			// System.out.println(UUID.randomUUID().toString().replace("-",
			// ""));
		}
		System.out.println("-------------------" + getSessionAttribute(SystemConstantKey.Recommend_Url));
		renderJsp("/index/index.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入注册页", last_update_author = "向旋")
	public void registration() throws Exception {
		renderJsp("/index/registration.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "找回密码发送邮箱页面", last_update_author = "向旋")
	public void to_back_password_send_mail_jsp() throws Exception {
		renderJsp("/index/back_password_send_email.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "注册3", last_update_author = "向旋")
	public void registration2() throws Exception {
		// int id = getSessionAttribute("id");
		// if(String.valueOf(id)==null||String.valueOf(id)==""){
		// renderText("请从注册第一步开始操作");
		// return;
		// }
		renderJsp("/index/registration_3.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入首页", last_update_author = "曹正辉")
	public void registration3() throws Exception {
		// String phone = getSessionAttribute("phone");
		// if(!Utils.isNotNullAndNotEmptyString(phone)){
		// renderText("请从注册的第一步开始操作");
		// }
		renderJsp("/index/registration_4.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "邮箱输入框失去焦点判断邮箱是否存在", last_update_author = "向旋")
	public void email_onblur_validate() {
		String email = getParameter("email");
		if (!Utils.isNotNullAndNotEmptyString(email)) {
			renderText("1");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where email=?", new Object[] { email });
		if (user != null) {
			renderText("2");
			return;
		}
		Pattern p = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
		Matcher m = p.matcher(email);
		// Mather m = p.matcher("wangxu198709@gmail.com.cn");这种也是可以的！
		boolean b = m.matches();
		if (!b) {
			renderText("4");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "手机输入框onkeyup判断邮箱是否存在", last_update_author = "向旋")
	public void phone_onblur_validate() {
		String phone = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("1");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", new Object[] { phone });
		if (user != null) {
			renderText("2");
			return;
		}
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(14[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(phone);
		// Mather m = p.matcher("wangxu198709@gmail.com.cn");这种也是可以的！
		boolean b = m.matches();
		if (!b) {
			renderText("4");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "昵称框upkeyup判断昵称是否存在", last_update_author = "向旋")
	public void nickname_onblur_validate() {
		String nickname = getParameter("nickname");
		if (!Utils.isNotNullAndNotEmptyString(nickname)) {
			renderText("1");
			return;
		}
		int len = nickname.length();
		String temp = null;
		Pattern p = Pattern.compile("[\u4E00-\u9FA5]+");
		Matcher m = p.matcher(nickname);
		if (m.find()) {
			temp = m.group(0);
			len = len + temp.length();
		}
		if (len < 4 || len > 16) {
			renderText("4");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where nickname=?", new Object[] { nickname });
		if (user != null) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "注册", last_update_author = "向旋")
	public void user_register() throws Exception {
		final String nick_name = getParameter("nickName");
		final String phone = getParameter("phone");
		final String password = getParameter("password");
		String password2 = getParameter("password2");
		final String verification_code = getParameter("verification_code");
		String session_code = getSessionAttribute(CheckCodeServlet.Code);
		final int user_type = getParameterToInt("user_type");
		final String url = getSessionAttribute("url");
		if (user_type != 1 && user_type != 2) {
			renderText("10");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(nick_name)) {
			renderText("1");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("2");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(password)) {
			renderText("3");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(password2)) {
			renderText("3");
			return;
		}
		if (!password.equals(password2)) {
			renderText("4");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(verification_code)) {
			renderText("5");
			return;
		}
		if (!verification_code.equals(session_code)) {
			renderText("6");
			return;
		}
		if (Utils.isNotNullAndNotEmptyString(url) && url.length() == 32) {
			_____Activity_1_Recommend_UrlM _____activity_1_recommend_url = _____Activity_1_Recommend_UrlM.dao.findFirst("select * from _____activity_1_recommend_url where url = ?", url);
			if (_____activity_1_recommend_url == null) {
				renderText("查询推荐链接失败");// 链接安全监测
				return;
			}
		}

		String sql_email_exists = "select count(1) from user_info where phone=?";
		boolean phone_exists = Db.queryLong(sql_email_exists, phone) >= 1;
		if (phone_exists) {
			renderText("7");
			return;
		}
		String sql_nikename_exists = "select count(1) from user_info where nickname=?";
		boolean nickname_exists = Db.queryLong(sql_nikename_exists, nick_name) > 1;
		if (nickname_exists) {
			renderText("10");
			return;
		}
		setSessionAttribute("nickname", nick_name);
		setSessionAttribute("phone", phone);
		setSessionAttribute("password", password);
		setSessionAttribute("user_type", user_type);
		renderText("0");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "手机验证", last_update_author = "")
	public void phone_validate() throws Exception {
		String phone = getParameter("phone");
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("1");
			return;
		}
		String sql_email_exists = "select count(1) from user_info where phone=?";
		boolean phone_exists = Db.queryLong(sql_email_exists, phone) >= 1;
		if (phone_exists) {
			renderText("6");
			return;
		}
		String v_code = getParameter("v_code");
		if (!Utils.isNotNullAndNotEmptyString(v_code)) {
			renderText("4");
			return;
		}
		String short_code = getSessionAttribute("binding_phone");
		String voice_code = getSessionAttribute("binding_phone_voice");
		final UserM user = new UserM();

		if (!v_code.equals(short_code) && !v_code.equals(voice_code)) {
			renderText("3");
			return;
		} else {
			boolean user_save = Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {
					UserNowMoneyM user_now_money = new UserNowMoneyM();
					UserAuthenticateAssetsInfoM user_authenticate_assetsinfo = new UserAuthenticateAssetsInfoM();
					UserAuthenticateFamilyInfoM user_authenticate_familyinfo = new UserAuthenticateFamilyInfoM();
					UserAuthenticatePersionalInfoM user_authenticate_persionalinfo = new UserAuthenticatePersionalInfoM();
					UserAuthenticateUploadInfoM user_authenticate_uploadinfo = new UserAuthenticateUploadInfoM();
					UserAuthenticateWorkInfoM user_authenticate_wordinfo = new UserAuthenticateWorkInfoM();
					UserAuthenticateLeftStatusM user_authenticate_letf_status = new UserAuthenticateLeftStatusM();

					UserBankInfoM user_bank_info = new UserBankInfoM();
					UserCreditFilesM user_credit_files = new UserCreditFilesM();

					String password = getSessionAttribute("password");
					int recommend_id = 0;
					boolean is_ok_add_user = false;
					final String url = getSessionAttribute("url");

					_____Activity_1_Recommend_UrlM _____activity_1_recommend_url = null;
					if (Utils.isNotNullAndNotEmptyString(url)) {
						_____activity_1_recommend_url = _____Activity_1_Recommend_UrlM.dao.findFirst("select * from _____activity_1_recommend_url where url = ? for update", url);// 必须加锁
						if (_____activity_1_recommend_url == null) {
							return false;// 有人故意构造url-链接安全监测
						} else {
							recommend_id = _____activity_1_recommend_url.getInt("user_id");

						}

					}

					is_ok_add_user = user.set("user_type", getSessionAttribute("user_type")).set("borrow_type", 0).set("phone", getSessionAttribute("phone")).set("is_active", 1).set("nickname", getSessionAttribute("nickname")).set("add_time", new Date()).set("password", MD5.md5(password)).set("recommend_id", recommend_id).save();
					if (recommend_id != 0) {// 是通过推荐人来的
						String user_id_map = _____activity_1_recommend_url.getString("user_id_map");
						Map<String, String> user_id_map_map = null;
						if (!Utils.isNotNullAndNotEmptyString(user_id_map)) {
							user_id_map_map = new HashMap<String, String>();

						} else {
							Type type = new TypeToken<Map<String, String>>() {
							}.getType();
							user_id_map_map = gson.fromJson(user_id_map, type);
						}
						user_id_map_map.put(user.getInt("id") + "", "0");
						user_id_map = gson.toJson(user_id_map_map);
						_____activity_1_recommend_url.set("user_id_map", user_id_map);
						//

						int count = _____activity_1_recommend_url.getInt("count");// 默认是0
						_____activity_1_recommend_url.set("count", count + 1);// 加1
						boolean is_ok_update______activity_1_recommend_url = _____activity_1_recommend_url.update();
						if (!is_ok_update______activity_1_recommend_url) {
							return false;
						}

					}
					boolean is_ok_send_system_notification = SystemNotificationM.send_system_notification(0, user.getInt("id"), "注册提示", "尊敬的客户，感谢您注册联富金融，联富金融使用邮箱号作为系统交易，修改信息的凭据，请妥善保管这个帐号，谢谢您的支持。");
					boolean is_ok_add_user_authenticate_assetsinfo = user_authenticate_assetsinfo.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_authenticate_familyinfo = user_authenticate_familyinfo.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_authenticate_persionalinfo = user_authenticate_persionalinfo.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_authenticate_uploadinfo = user_authenticate_uploadinfo.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_authenticate_wordinfo = user_authenticate_wordinfo.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_now_money = user_now_money.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_authenticate_letf_status = user_authenticate_letf_status.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_bank_info = user_bank_info.set("user_id", user.getInt("id")).save();
					boolean is_ok_add_user_credit_files = user_credit_files.set("user_id", user.getInt("id")).save();

					if (!is_ok_send_system_notification || !is_ok_add_user || !is_ok_add_user_authenticate_assetsinfo || !is_ok_add_user_authenticate_familyinfo || !is_ok_add_user_authenticate_persionalinfo || !is_ok_add_user_authenticate_uploadinfo || !is_ok_add_user_authenticate_wordinfo || !is_ok_add_user_now_money || !is_ok_add_user_authenticate_letf_status || !is_ok_add_user_bank_info
							|| !is_ok_add_user_credit_files) {
						return false;
					} else {
						setSessionAttribute(SystemConstantKey.User, user);
						// setSessionAttribute("user", user);
						return true;
					}
				}
			});
			UserNowMoneyM user_now_money = UserNowMoneyM.dao.findFirst("select * from user_now_money where user_id=?", user.getInt("id"));
			if (user_now_money == null) {
				user_now_money = new UserNowMoneyM();// 注册成功的时候进行添加
				user_now_money.set("user_id", user.getInt("id")).set("cny_can_used", 0).set("cny_freeze", 0).save();
			}
			if (user_save) {
				renderText("0");
				return;
			} else {
				renderText("5");
				return;
			}
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "手机号修改", last_update_author = "向旋")
	public void phone_update() throws Exception {
		final String phone = getParameter("phone");
		final UserM user = UserM.dao.findById(getSessionAttribute("id"));
		if (user == null) {
			renderText("6");
			return;
		}
		if (user.getInt("is_active") == 1) {
			renderText("7");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("1");
			return;// 为空
		}
		// 先到数据库里面查询该邮箱是否存在-如果存在这提示该账号已经被使用
		String sql_email_exist = "select count(1) from user_info where phone=?";
		boolean phone_exist = Db.queryLong(sql_email_exist, phone) >= 1;
		if (phone_exist) {
			renderText("2");
			return;
		}
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				boolean ok_update_email = user.set("phone", phone).update();
				if (!ok_update_email) {

					return false;

				}
				return true;

			}
		});
		if (is_ok) {
			setSessionAttribute("phone", phone);
			renderText("3");
			return;
		} else {
			renderText("4");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "邮箱修改", last_update_author = "向旋")
	public void email_update() throws Exception {
		final String email = getParameter("email");
		final UserM user = UserM.dao.findById(getSessionAttribute("id"));
		if (user == null) {
			renderText("6");
			return;
		}
		if (user.getInt("is_active") == 1) {
			renderText("7");
			return;
		}
		Pattern p = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
		Matcher m = p.matcher(email);
		// Mather m = p.matcher("wangxu198709@gmail.com.cn");这种也是可以的！
		boolean b = m.matches();
		if (!b) {
			renderText("5");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(email)) {
			renderText("1");
			return;// 为空
		}
		// 先到数据库里面查询该邮箱是否存在-如果存在这提示该账号已经被使用
		String sql_email_exist = "select count(1) from user_info where email=?";
		boolean email_exist = Db.queryLong(sql_email_exist, email) >= 1;
		if (email_exist) {
			renderText("2");
			return;
		}
		String code_b = GetCode.get_code();
		boolean send_email = UserEmailRequestWaitingM.add_user_email_request_of_user_register(user.getString("nickname"), email, code_b);
		if (!send_email) {
			renderText("8");
			return;
		}
		setSessionAttribute("code", code_b);
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				boolean ok_update_email = user.set("email", email).update();
				if (!ok_update_email) {

					return false;

				}
				return true;

			}
		});
		if (is_ok) {
			renderText("3");
			return;
		} else {
			renderText("4");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "跳转到登录页面", last_update_author = "向旋")
	public void to_login() {
		renderJsp("/index/login.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "登录", last_update_author = "向旋")
	public void user_login() throws Exception {
		final String username = getParameter("email");
		final String password = getParameter("password");
		String checked = getParameter("checked");
		if (!Utils.isNotNullAndNotEmptyString(username)) {
			renderText("1");
			return;
		}
		if (!Utils.isNotNullAndNotEmptyString(password)) {
			renderText("2");
			return;
		}
		final UserM user = (UserM) UserM.dao.findFirst("select * from user_info where (phone=? or email = ?) and password =? and is_active!=-1", new Object[] { username, username, MD5.md5(password) });
		if (user == null) {
			renderText("3");// 账号或者密码错误
			return;
		}
		int is_active = user.getInt("is_active");
		if (is_active == 0) {
			renderText("4");// 系统禁止登陆
			return;
		}
		if ("checked".equals(checked)) {
			HttpServletResponse response = getResponse();
			String username_of_old = username + "";
			Cookie username_of_cookie = new Cookie("username", username_of_old);
			username_of_cookie.setMaxAge(24 * 60 * 60 * 1);
			response.addCookie(username_of_cookie);
		}

		UserNowMoneyM user_now_money = UserNowMoneyM.dao.findFirst("select * from user_now_money where user_id=?", new Object[] { user.getInt("id") });
		setSessionAttribute(SystemConstantKey.User_Now_Money, user_now_money);
		setSessionAttribute(SystemConstantKey.User, user);
		renderText("5");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { UserLoginedAop.class })
	@Function(for_people = "所有人", function_description = "退出", last_update_author = "向旋")
	public void user_exit() throws Exception {
		ActionContext.getHttpSession().removeAttribute(SystemConstantKey.User);
		ActionContext.getHttpSession().invalidate();
		// renderJsp("/index/index.jsp");
		// renderAction("/index/index");
		/**
		 * 跳转到针对用户跳转会首页的Servlet
		 */
		renderJsp("/index/user_exit_to_index.html");
		return;
	}

	// @AllowNotLogin
	// @ClearInterceptor(ClearLayer.Before)
	// @Function(for_people = "所有人", function_description = "找回登录密码发送邮箱验证码",
	// last_update_author = "向旋")
	// public void find_password_send_email() throws Exception {
	// String email = getParameter("email");
	// if (!Utils.isNotNullAndNotEmptyString(email)) {
	// renderText("1");
	// }
	// UserM user =
	// UserM.dao.findFirst("select * from user_info where email = ?", email);
	// if (user == null) {
	// renderText("2");// 该邮箱没有绑定
	// return;
	// }
	// String system_code = getSessionAttribute(CheckCodeServlet.Code);
	// String page_code = getParameter("page_code");
	// if (!Utils.isNotNullAndNotEmptyString(page_code)) {
	// renderText("3");
	// return;
	// }
	// if (!system_code.equals(page_code)) {
	// renderText("4");
	// return;
	// }
	// String code_v = GetCode.get_code();
	// boolean ok_send_register_email =
	// UserEmailRequestWaitingM.add_user_email_request_of_user_back_password(email,
	// code_v);
	// if (!ok_send_register_email) {
	// renderText("5");
	// return;
	// }
	// setSessionAttribute("find_password_email", email);
	// setSessionAttribute("code_v", code_v);
	// renderText("6");
	// return;
	// }

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "提交验证码页面", last_update_author = "向旋")
	public void back_password() {
		String phone = getParameter("phone");
		setSessionAttribute("phone", phone);
		if (!Utils.isNotNullAndNotEmptyString(phone)) {
			renderText("1");
			return;
		}
		UserM user = UserM.dao.findFirst("select * from user_info where phone = ?", phone);
		if (user == null) {
			renderText("2");
			return;
		}
		String code = getParameter("code");
		if (!Utils.isNotNullAndNotEmptyString(code)) {
			renderText("3");
			return;
		}
		String session_code = getSessionAttribute(CheckCodeServlet.Code);
		if (!code.equals(session_code)) {
			renderText("4");
			return;
		}
		setSessionAttribute("phone", phone);
		renderText("5");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "提交验证码页面", last_update_author = "向旋")
	public void to_back_password2() {
		renderJsp("/index/back_password_2.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "找回登录密码验证短信验证码是否正确", last_update_author = "向旋")
	public void find_password_emial_code_isok() {
		String code = getParameter("page_code");
		if (!Utils.isNotNullAndNotEmptyString(code)) {
			renderText("1");
			return;
		}
		String e_code = getSessionAttribute("back_password_voice");
		String message_code = getSessionAttribute("back_password_code");
		if (!code.equals(e_code) && !code.equals(message_code)) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "找回密码", last_update_author = "向旋")
	public void find_password() {
		String password = getParameter("password");
		if (!Utils.isNotNullAndNotEmptyString(password)) {
			renderText("1");
			return;
		}
		if (password.length() < 6 || password.length() > 35) {
			renderText("2");
			return;
		}
		String password2 = getParameter("password2");
		if (!Utils.isNotNullAndNotEmptyString(password2)) {
			renderText("3");
			return;
		}
		if (!password.equals(password2)) {
			renderText("4");
			return;
		}
		String phone = getSessionAttribute("phone");
		UserM user = UserM.dao.findFirst("select * from user_info where phone=?", phone);
		boolean find_isok = user.set("password", MD5.md5(password)).update();
		if (!find_isok) {
			renderText("5");
			return;
		}
		renderText("6");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "跳转到密码修改成功页面", last_update_author = "向旋")
	public void to_cg_page() {
		renderJsp("/index/back_password_cg.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "找回密码的图形验证码验证码是否正确", last_update_author = "向旋")
	public void code_validate() {
		String page_code = getParameter("page_code");
		if (!Utils.isNotNullAndNotEmptyString(page_code)) {
			renderText("1");
			return;
		}
		String session_code = getSessionAttribute("send_code");
		if (!session_code.equals(page_code)) {
			renderText("2");
			return;
		}
		renderText("3");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "跳转到找回密码的修改密码页面", last_update_author = "向旋")
	public void to_update_password_page() {
		renderJsp("/index/back_password_cz.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "跳转到修改密码页面", last_update_author = "向旋")
	public void to_password_sb() {
		renderJsp("/index/back_password_sb.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "获取首页显示的三个数据", last_update_author = "hx")
	public void get_three_data() {
		/**
		 * 查询注册联富宝的人数
		 */
		String join_count_sql = "select count(1) from user_info tt ";
		long join_count = Db.queryLong(join_count_sql);
		/**
		 * 查询成功投资散标的总金额
		 */
		String join_total_invest_money_sql = "SELECT SUM(invest_money) AS total_invest_money from lender_bulk_standard_order ";
		BigDecimal total_invest_money = Db.queryBigDecimal(join_total_invest_money_sql);
		/**
		 * 查询成功投资联富宝的总金额
		 */
		String fix_bid_user_bid_money_sql = "SELECT SUM(t.bid_money) AS total_bid_money FROM fix_bid_user_hold t ";
		BigDecimal total_bid_money = Db.queryBigDecimal(fix_bid_user_bid_money_sql);
		/**
		 * 投资总金额 = 投资联富宝的总金额 + 投资散标的总金额
		 */

		BigDecimal n_w = new BigDecimal("0");
		if (total_bid_money != null && total_invest_money != null) {
			BigDecimal join_total_money = total_bid_money.add(total_invest_money).setScale(2, BigDecimal.ROUND_DOWN);
			n_w = join_total_money.divide(new BigDecimal("10000"), BigDecimal.ROUND_DOWN);// 投资金额多少万
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("join_count", join_count);// 加入人数
		map.put("n_w", n_w);// 投资总金融

		Gson gson = new Gson();
		renderText(gson.toJson(map));
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "获取当期发行的联富宝L12类型的一条数据", last_update_author = "hx")
	public void get_current_lfoll_bid_l12() {
		String curr_lfoll_bid_l12_sql = "SELECT * FROM fix_bid_system_order t WHERE t.is_show = 1 AND t.closed_period = 12 ORDER BY t.id DESC LIMIT 0,1";
		FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.dao.findFirst(curr_lfoll_bid_l12_sql);
		if (fix_bid_system_order != null) {
			LfollInvestAction.get_lfoll_fix_bid_cycle(fix_bid_system_order);
			Map<String, Object> map = fix_bid_system_order.getM();
			Gson gson = new Gson();
			renderText(gson.toJson(map));
			return;
		} else {
			renderText("1");
			return;
		}

	}

	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString().replace("-", ""));
	}

}

package com.tfoll.web.action.user.usercenter;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.BorrowerAop;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.timer.bulk_standard.Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@ActionKey("/user/usercenter/loan_manage")
public class LoanManageAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--申请查询", last_update_author = "lh")
	public void to_my_loan_apply() {

		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql = "select * from borrower_bulk_standard_apply_order where user_id = ?";
		List<BorrowerBulkStandardApplyOrderM> borrower_bulk_standard_apply_order_list = BorrowerBulkStandardApplyOrderM.dao.find(sql, new Object[] { user_id });
		setSessionAttribute("borrower_bulk_standard_apply_order_list", borrower_bulk_standard_apply_order_list);

		renderJsp("/user/usercenter/loan_manage/my_loan_apply.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAjax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--还款", last_update_author = "lh or czh")
	public void to_my_loan_page() {

		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		Date now = new Date();
		/**
		 * 查询当前时间的下一个还款日
		 */
		// repay_end_time-还款截至时间
		String sql_next_repayment_plan = "SELECT a.borrow_all_money, a.annulized_rate_int, b.current_period, b.total_periods, b.automatic_repayment_date, b.should_repayment_total, b.repay_end_time " + //
				"FROM borrower_bulk_standard_gather_money_order a, borrower_bulk_standard_repayment_plan b WHERE " + //
				" b.gather_money_order_id = a.id AND b.repay_end_time_long > ? AND b.borrower_user_id = ? ORDER BY b.repay_end_time ASC LIMIT 1";
		Record loan_info = Db.findFirst(sql_next_repayment_plan, new Object[] { now.getTime(), user_id });
		BigDecimal borrow_all_money = new BigDecimal("0.00");
		/**
		 * 如果下一次存在则获得所有的借贷金额
		 */
		if (loan_info != null) {
			borrow_all_money = loan_info.getBigDecimal("borrow_all_money");
		}
		borrow_all_money = borrow_all_money.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("loan_info", loan_info);
		setSessionAttribute("borrow_all_money", borrow_all_money);

		//
		/**
		 * 查询正在还款的所有还款记录
		 */
		String sql_repayment_plan_all_pay = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find(sql_repayment_plan_all_pay, new Object[] { user_id });
		List<Map<String, Object>> borrower_bulk_standard_repayment_plan_map_list = new ArrayList<Map<String, Object>>();

		BigDecimal has_pay = new BigDecimal("0"); // 已还的（可能未逾期,可能逾期）
		BigDecimal not_has_pay = new BigDecimal("0"); // 未还的（可能未逾期,可能逾期）
		BigDecimal not_has_pay_of_overdueverdue = new BigDecimal("0"); // 逾期未还的

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			Map<String, Object> borrower_bulk_standard_repayment_plan_map = borrower_bulk_standard_repayment_plan.getM();// getMap映射-可以扩容-Keys-但是不能更新数据
			borrower_bulk_standard_repayment_plan_map_list.add(borrower_bulk_standard_repayment_plan_map);
			// 还款日期
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");

			BigDecimal normal_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("normal_manage_fee");

			BigDecimal over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
			BigDecimal over_punish_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");
			//

			/**
			 * 是否支付0/1
			 */
			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");// 是否支付-决定逾期管理费和逾期罚息是否需要从数据库计算还是从数据库记录中取值
			/**
			 * <pre>
			 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
			 * </pre>
			 */
			int repayment_period = borrower_bulk_standard_repayment_plan.getInt("repayment_period");

			if (is_repay == 0) {
				// 正常管理费
				normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
				// 管理费-该公式里面已经计算了逾期管理费
				over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
				// 罚息-该公式里面已经计算了逾期罚息
				over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
				// actual_repayment==0
				repayment_period = CalculationFormula.get_repayment_period(repay_end_time);// 仅仅供外面查询-当还款成功的时候-该值会写入数据库

			} else if (is_repay == 1) {
				// 这里就不需要进行处理了
			} else {
				renderText("该状态不存在");
				return;
			}
			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_total_fee = over_manage_fee.add(over_punish_interest);
			// 应还总额=月还本息+管理费 +逾期费用
			BigDecimal total_total = should_repayment_total.add(normal_manage_fee).add(over_total_fee);// 理财那边的还款和借款这边的还款是不一样的

			borrower_bulk_standard_repayment_plan_map.put("manage_fee", normal_manage_fee);// 正常的管理费用
			borrower_bulk_standard_repayment_plan_map.put("over_total_fee", over_total_fee);// 包含逾期的管理费用
			borrower_bulk_standard_repayment_plan_map.put("total_total", total_total);// 该月应该还的所有

			if (is_repay == 0) {// 没有支付
				if (repayment_period == 1 || repayment_period == 2) {
					not_has_pay = not_has_pay.add(should_repayment_total).add(normal_manage_fee);
				} else if (repayment_period == 3 || repayment_period == 4) { // 所处时期为普通逾期，或者严重逾期
					not_has_pay = not_has_pay.add(should_repayment_total).add(normal_manage_fee).add(over_total_fee);
					not_has_pay_of_overdueverdue = not_has_pay_of_overdueverdue.add(should_repayment_total).add(normal_manage_fee).add(over_total_fee);
				} else {
					renderText("该状态不存在");
					return;
				}
			} else if (is_repay == 1) {// 支付
				has_pay = has_pay.add(total_total);// 支付了的所有总的累加
			} else {
				renderText("该状态不存在");
				return;
			}

		}
		not_has_pay = not_has_pay.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("weihuan", not_has_pay); // 未还的
		not_has_pay_of_overdueverdue = not_has_pay_of_overdueverdue.setScale(2, BigDecimal.ROUND_DOWN);
		setSessionAttribute("over_weihuan", not_has_pay_of_overdueverdue); // 逾期未还的

		renderJsp("/user/usercenter/loan_manage/loan_query.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { UserLoginedAjax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "个人信息--借款管理--借款统计", last_update_author = "lh")
	public void to_loan_statistics() {

		renderJsp("/user/usercenter/loan_manage/loan_statistics.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "获取借款人的还款计划", last_update_author = "lh")
	public void get_repayment_plan() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");

		String sql_money = "select cny_can_used from user_now_money where user_id = ?";
		BigDecimal cny_can_used = Db.queryBigDecimal(sql_money, new Object[] { user_id });

		String sql = "SELECT a.* FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";

		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plans = BorrowerBulkStandardRepaymentPlanM.dao.find(sql, new Object[] { user_id });

		// 查找对应的借款单的借款总金额
		String sql_all_money = "SELECT b.borrow_all_money FROM borrower_bulk_standard_repayment_plan a, borrower_bulk_standard_gather_money_order b WHERE a.gather_money_order_id = b.id AND b.payment_state = 2 AND b.user_id = ?";
		BigDecimal borrow_all_money = Db.queryBigDecimal(sql_all_money, new Object[] { user_id });

		List<Map<String, Object>> repayment_plans = new ArrayList<Map<String, Object>>();

		BigDecimal yihuan = new BigDecimal("0");
		BigDecimal weihuan = new BigDecimal("0");

		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plans) {
			Map<String, Object> repayment_plan = borrower_bulk_standard_repayment_plan.getM();
			repayment_plans.add(repayment_plan);

			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");

			BigDecimal total_total = new BigDecimal("0");

			// 要还的本息
			BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
			Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
			// 正常管理费
			BigDecimal manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
			// 逾期罚息
			BigDecimal over_faxi = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
			// 逾期管理费
			BigDecimal over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);

			// 逾期费用 = 逾期罚息 + 逾期管理费
			BigDecimal over_total_fee = over_faxi.add(over_manage_fee);

			// 应还总额=月还本息+管理费 +逾期费用
			total_total = total_total.add(should_repayment_total).add(manage_fee).add(over_total_fee);

			repayment_plan.put("manage_fee", manage_fee);
			repayment_plan.put("over_total_fee", over_total_fee);
			repayment_plan.put("total_total", total_total);

			if (is_repay == 0) {
				weihuan = weihuan.add(should_repayment_total).add(manage_fee).add(over_total_fee);
			} else if (is_repay == 1) {
				yihuan = yihuan.add(should_repayment_total).add(manage_fee).add(over_total_fee);
			}

		}
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("repayment_plans", repayment_plans);
		map.put("yihuan", yihuan);
		map.put("weihuan", weihuan);
		map.put("cny_can_used", cny_can_used);
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "异步获取易还清借款", last_update_author = "lh")
	public void get_pay_off_loan() {
		UserM user = getSessionAttribute("user");
		int user_id = user.getInt("id");
		String sql = "select * from borrower_bulk_standard_gather_money_order where payment_state = 3 and user_id = ? ";
		List<BorrowerBulkStandardGatherMoneyOrderM> borrower_bulk_standard_gather_money_order_list = BorrowerBulkStandardGatherMoneyOrderM.dao.find(sql, new Object[] { user_id });
		List<Map<String, Object>> pay_off_loan_list = new ArrayList<Map<String, Object>>();
		for (BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order : borrower_bulk_standard_gather_money_order_list) {
			Map<String, Object> pay_off_loan = borrower_bulk_standard_gather_money_order.getM();
			pay_off_loan_list.add(pay_off_loan);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pay_off_loan_list", pay_off_loan_list);
		Gson gson = new Gson();
		String str = gson.toJson(map);
		renderText(str);
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class, BorrowerAop.class })
	@Function(for_people = "登录", function_description = "确定还款", last_update_author = "lh")
	public void confirm_repay_money_3() {
		String id_str = getParameter("pass_id_str");
		if (Utils.isNotNullAndNotEmptyString(id_str)) {
			renderText("");// 没有选中不返回-非法操作
			return;
		}
		// 传递过来的参数格式id_str = "1,2,3,";
		String[] ids = id_str.split(",");// 还款计划id数组
		final int periods = ids.length;
		if (periods == 0) {
			renderText("");// 没有选中不返回
			return;
		}

		final long[] repayment_plan_ids = new long[periods]; // 要还款的(还款计划)id数组
		int id_length = ids.length;
		for (int i = 0; i < id_length; i++) {
			try {
				repayment_plan_ids[i] = Long.parseLong(ids[i]);
			} catch (Exception e) {
				logger.error(e);// 转换失败-非法操作
				renderText("");
				return;
			}
		}
		/**
		 * 检查传入的ID是否有序-保证我们还款是有序的-不能中间跳着的
		 */
		if (!Utils.is_asc_order(repayment_plan_ids)) {
			renderText("传入的值顺序没有按照递增的顺序");
			return;
		}

		/**
		 * 还款是针对成功还款一次则记录一次的原则-同时需要保证整个还款的过程中数据是否一致
		 * 
		 * <pre>
		 * 系统还款由N部分构成:还款截止日之前手动还款，系统还款日自动还款，普通逾期，严重逾期
		 * 
		 * 提前还掉所有的款项[还款截止日之前手动还款，系统还款日自动还款，普通逾期，严重逾期]-必须一次性还清所有的单子-那么这个是很复杂的-需要一个事务-这个时候需要把用户的债权转让计划给撤掉,
		 * 
		 * <pre>
		 * 单月
		 * 手动还当期[如果出现严重逾期，那么这笔钱则自动系统],
		 * 单独的系统自动还款[如果出现严重逾期，那么这笔钱则自动系统][30-31天问题],
		 * 手动还普通逾期[如果出现严重逾期，那么这笔钱则自动系统],
		 * 手动还严重逾期[如果出现严重逾期，那么这笔钱则自动系统],
		 * 
		 * 手动还当期后一个月的还款[如果出现严重逾期，那么这笔钱则自动系统],
		 * 手动还当期后第二个月的还款[如果出现严重逾期，那么这笔钱则自动系统],
		 * </pre>
		 * 
		 * <pre>
		 * 根据上面单个的还款标准-同时需要加上一个还款的约束的条件这个就是还款必须是如果还款还后几个月的话如果不是完全还掉所有，那么最多只能还当月后两个月
		 * </pre>
		 * 
		 * 针对上述每个单子我们需要分别对待-每个还款计划处于那个阶段-执行那种还款指标. 说明一点：还款是不能跳序还款 </pre>
		 * 
		 * <pre>
		 * 处理思路
		 * 首先检查传入的ID所对应的还款计划是否存在,是否还了，是否到了还款开始阶段。是否普通逾期，验证逾期.是否出现传入的ID相对当期还款为基准的跳序还款。
		 * </pre>
		 * 
		 */

		final AtomicReference<String> error_code = new AtomicReference<String>("");
		/**
		 * 检查凑集单根据还款计划-user_id去查询凑集单信息
		 */
		UserM user = getSessionAttribute("user");
		final int user_id_borrower = user.getInt("id");
		/**
		 * 查询第一条数据
		 */
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order_first = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT gmo.* from borrower_bulk_standard_gather_money_order gmo,borrower_bulk_standard_repayment_plan  rp  WHERE rp.id=? and rp.borrower_user_id=? and rp.gather_money_order_id=gmo.id ", new Object[] { repayment_plan_ids[0], user_id_borrower });// 至少含有几个还款计划
		if (borrower_bulk_standard_gather_money_order_first == null) {
			renderText("查询不到凑集单");
			return;
		}
		final long gather_money_order_id = borrower_bulk_standard_gather_money_order_first.getLong("gather_money_order_id");
		/**
		 * 需要把还款的计划都查询出来?我们做的还款计划最多是36个-那么这个性能的要求就不怎样多-只查询一次的性能更加好-毕竟是一个月还款一次至三次
		 */
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT * from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id=1", new Object[] { gather_money_order_id });
		if (!Utils.isHasData(borrower_bulk_standard_repayment_plan_list)) {
			throw new NullPointerException("borrower_bulk_standard_repayment_plan_list is null");
		}
		/**
		 * key id,value m
		 */
		Map<String, BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_map = new HashMap<String, BorrowerBulkStandardRepaymentPlanM>();
		/**
		 * key current_period_id,value repayment_plan_id
		 */
		Map<String, String> borrower_bulk_standard_repayment_plan_current_period_map = new HashMap<String, String>();
		//
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			long id = borrower_bulk_standard_repayment_plan.getLong("id");
			int current_period = borrower_bulk_standard_repayment_plan.getInt("current_period");

			borrower_bulk_standard_repayment_plan_map.put(id + "", borrower_bulk_standard_repayment_plan);
			borrower_bulk_standard_repayment_plan_current_period_map.put(current_period + "", id + "");// 他们之间存在一一对应的关系
		}
		/**
		 * 检查传入的ID是否非法
		 */
		for (long repayment_plan_id : repayment_plan_ids) {
			if (!borrower_bulk_standard_repayment_plan_map.containsKey(repayment_plan_id + "")) {
				renderText("页面传递过来的id在数据库里面查询不到应该有的信息");
				return;
			}
		}

		/**
		 * 检查所有的ID是否在这个map里面,特殊的规则是必须检测出如果当月需要还款，则只能最多下月或者再下月，
		 * 且如果有两个月则之前的一个月必须是还款了才行[当次业务和以前的业务]
		 */
		boolean is_need_repayment_at_this_month = false;
		long repayment_plan_id_need_repayment_at_this_month = 0;
		Date now = new Date();
		long now_long = now.getTime();
		/**
		 * 寻找当月还款计划的ID
		 */
		for (long repayment_plan_id : repayment_plan_ids) {
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan_map.get(repayment_plan_id + "");
			// repay_start_time_long<=? and repay_end_time_long>=
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if (repay_start_time_long <= now_long && now_long <= repay_end_time_long) {
				is_need_repayment_at_this_month = true;
				repayment_plan_id_need_repayment_at_this_month = repayment_plan_id;// ID
			}
		}

		/**
		 * 传入的还款计划ID需要满足还款2月连续的规则
		 */
		if (!is_need_repayment_at_this_month) {
			// 表示按照正常的还款进度，所有的需要还的都逾期了
		} else {
			/**
			 * 依次进行检测是否符合连续二月规则
			 */
			// 下一个月是否还了或者该次业务执行中进行还款-这两者需要同等的看到
			Long next_month_repayment_plan_id = repayment_plan_id_need_repayment_at_this_month + 1;
			Long next_next_month_repayment_plan_id = next_month_repayment_plan_id + 2;
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan_of_next_month_repayment_plan_id = borrower_bulk_standard_repayment_plan_map.get(next_month_repayment_plan_id + "");
			BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan_of_next_next_month_repayment_plan_id = borrower_bulk_standard_repayment_plan_map.get(next_next_month_repayment_plan_id + "");

			boolean next_month_has_or_need_pay = false;
			boolean next_next_month_has_or_need_pay = false;

			for (long repayment_plan_id : repayment_plan_ids) {
				if (repayment_plan_id <= repayment_plan_id_need_repayment_at_this_month) {
					// 默认不需要规则进行校验
				} else {
					BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan_map.get(repayment_plan_id + "");
				}

			}

		}

		final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order_first.getBigDecimal("borrow_all_money");
		final BigDecimal borrow_all_share = borrower_bulk_standard_gather_money_order_first.getBigDecimal("borrow_all_share");// 需要关注份额之比

		/**
		 * 分开还钱
		 */
		for (int i = 0; i < id_length; i++) {
			final long repayment_plan_id = repayment_plan_ids[i];
			boolean is_ok = Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {

					BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = BorrowerBulkStandardRepaymentPlanM.dao.findById(repayment_plan_id);

					// 实际还款时间
					Date repay_actual_time = new Date();
					long repay_actual_time_long = repay_actual_time.getTime();
					borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
					borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

					// 还款日期
					Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
					/**
					 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
					 * 系统收取管理费用
					 */
					// 要还的本息
					BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");

					BigDecimal normal_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("normal_manage_fee");

					BigDecimal over_manage_fee = borrower_bulk_standard_repayment_plan.getBigDecimal("over_manage_fee");
					BigDecimal over_punish_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("over_punish_interest");
					//

					/**
					 * 是否支付0/1
					 */
					int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");// 是否支付-决定逾期管理费和逾期罚息是否需要从数据库计算还是从数据库记录中取值
					if (is_repay != 0) {
						return true;// 不需要计算后面
					}
					/**
					 * <pre>
					 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
					 * </pre>
					 */
					// int repayment_period =
					// borrower_bulk_standard_repayment_plan.getInt("repayment_period");

					// 正常管理费
					normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
					// 管理费-该公式里面已经计算了逾期管理费
					over_manage_fee = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
					// 罚息-该公式里面已经计算了逾期罚息
					over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
					// actual_repayment==0

					// 逾期费用 = 逾期罚息 + 逾期管理费
					BigDecimal over_total_fee = over_manage_fee.add(over_punish_interest);
					// 应还总额=月还本息+管理费 +逾期费用
					BigDecimal total_total = should_repayment_total.add(normal_manage_fee).add(over_total_fee);// 理财那边的还款和借款这边的还款是不一样的

					/**
					 * 针对各种借款还款阶段做出响应的操作:判断正常还款,自动还款,是否严重逾期,逾期 <br/>
					 * 
					 * <pre>
					 * 正常还款和自动还款是没有罚息的
					 * 逾期是含有罚息的，那么这部分的资金是打给理财用户的
					 * 严重逾期那么这部分的资金是还给系统的-而不是理财的用户-理财用户是我们系统指定的一个帐号id= 4id=[1-4]都被系统征用
					 * </pre>
					 */
					// 记录上面的费用
					borrower_bulk_standard_repayment_plan.set("normal_manage_fee", normal_manage_fee);
					borrower_bulk_standard_repayment_plan.set("over_manage_fee", over_manage_fee);
					borrower_bulk_standard_repayment_plan.set("over_punish_interest", over_punish_interest);// 罚息利息

					// 实际还款额=两个管理费用+一个罚息
					BigDecimal actual_repayment = should_repayment_total.add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);
					borrower_bulk_standard_repayment_plan.set("actual_repayment", actual_repayment);
					borrower_bulk_standard_repayment_plan.set("is_repay", 1);
					int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time);// 得到当前的还款期间
					borrower_bulk_standard_repayment_plan.set("repayment_period", repayment_period);
					boolean is_ok_update_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.update();
					if (is_ok_update_borrower_bulk_standard_repayment_plan) {
						error_code.set("");
						return false;
					}
					// 处理借款人的资金表
					UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_borrower);
					BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used"); // 还款之前的金额
					cny_can_used_of_borrower = cny_can_used_of_borrower.subtract(actual_repayment); // 还款之后的

					boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower).update();

					if (!is_ok_update_user_now_money_of_borrower) {
						error_code.set("");
						return false;
					}
					/**
					 * 为还款建立相应的还款资金变动信息
					 */

					/**
					 * 本息是所有的理财用户都需要进行平均分配的
					 */
					BigDecimal each_principal_interest = should_repayment_total.divide(borrow_all_share); // 平均每份所对应的本息
					/**
					 * 定义借款人给理财人的还款罚息
					 */

					BigDecimal each_punish_interest = new BigDecimal("0"); // 平均每份所对应的借款罚息

					/**
					 * 根据还款区间来判断
					 */
					if (repayment_period == 1 || repayment_period == 2) {
						// 1,2区间主要讲的是无超期还款
					} else if (repayment_period == 3) {
						// 普通超期-这部分管理费归系统-罚息归理财用户-需要平分
						each_punish_interest = over_punish_interest.divide(borrow_all_share);
					} else if (repayment_period == 4) {
						// 严重超期-这部分管理费归系统-罚息归系统-本息归系统指定的用户
					} else {
						return false;
					}

					// 到债权持有表中查找每个理财人对应的持有份数, 来增加他们资金表中的可用余额
					String sql_find_holders = "select id,user_id,hold_share,transfer_share,is_transfer_all from lender_bulk_standard_creditor_right_hold where gather_money_order_id =? and hold_share!=0 ";
					List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find(sql_find_holders, new Object[] { gather_money_order_id });
					/**
					 * user_id,hold_share,transfer_share-会涉及到一个债权转让的一个问题,
					 * 债权价值需要被改变
					 */
					if (Utils.isHasData(lender_bulk_standard_creditor_right_hold_list)) {
						error_code.set("");
						return false;
					}
					/**
					 * 还款的时候债权的价值以及对应的价格和手续费会相应的按照折价的系数修改-
					 * 同时系统需要一种机制告诉系统所有需要通过债权转让获取的债权持有的理财人暂时不能操作
					 */
					for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {

						int financial_user_id = lender_bulk_standard_creditor_right_hold.getInt("user_id"); // 理财人id
						int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 原始购买的份额
						int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 转出的份额
						if (transfer_share != 0) {// 正在转让过程中需要对外面的债权转让实现冻结

							/**
							 * 债权转让的时候才做这个东西
							 */
							if ("1".equals("2")) {
								long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");// 持仓ID
								/**
								 * 根据持仓ID进行查询所有正在债权转让记录
								 */
								List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list = LenderBulkStandardOrderCreditorRightTransferOutM.dao.find("SELECT id from lender_bulk_standard_order_creditor_right_transfer_out WHERE creditor_right_hold_id=?", new Object[] { creditor_right_hold_id });
								if (Utils.isHasData(lender_bulk_standard_order_creditor_right_transfer_out_list)) {
									// 推送消息
									// 同步方法调用-code
									// Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.add_creditor_right_hold_value_change_push(lender_bulk_standard_order_creditor_right_transfer_out_list);
									// 异步推送消息
									Timer_lender_bulk_standard_order_creditor_right_hold_value_change_push.add_creditor_right_hold_value_change_push_asyn_request(lender_bulk_standard_order_creditor_right_transfer_out_list);
								}
							}

						}
						// 应该收到的钱由两部分构成

						BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 每个人的
						BigDecimal punish_interest = each_punish_interest.multiply(new BigDecimal(hold_share + ""));// 每个人的
						// 应该回收的金额
						BigDecimal recieve_money = principal_interest.add(punish_interest);

						UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(financial_user_id);
						BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
						cny_can_used_of_lender = cny_can_used_of_lender.add(recieve_money);
						/**
						 * 资金转移
						 */
						boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender).update();
						/**
						 * 记录本息还款记录
						 */
						if (!is_ok_update_user_now_money_of_lender) {
							error_code.set("");
							return false;
						}
						/**
						 * 需要记录资金信息
						 * 
						 * <pre>
						 * sys_expenses_records 
						 * sys_income_records
						 * 
						 * user_money_change_records
						 * user_transaction_records
						 * </pre>
						 */

						// 添加本息
						// 添加罚息
						// boolean
						// is_ok_add_sys_income_records=SysExpensesRecordsM.add_sys_income_records(financial_user_id,
						// type, money, detail);
						// boolean is_ok_add_sys_income_records=
						// SysIncomeRecordsM.add_sys_income_records(user_id,
						// type, money, detail);

						// boolean is_ok_add_sys_income_records=
						// UserMoneyChangeRecordsM.addUserTransactionRecordsByType(user_id,
						// type, start_money, pay, income, end_money);
						// boolean is_ok_add_sys_income_records=
						// UserTransactionRecordsM.addUserTransactionRecordsByType(user_id,
						// type, start_money, pay, income, end_money);

					}
					return true;

				}
			});

			if (!is_ok) {
				error_code.get();
				renderText("");
				return;
			}

		}// for i
		renderText("还款成功");
		return;

	}

}

package com.tfoll.web.action.user.authentication;

import com.google.gson.Gson;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.common.SystemConfig;
import com.tfoll.web.util.Utils;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/authentication/return_city")
public class ReturnCityAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "省城信息", last_update_author = "zjb")
	public void return_city() {
		String province = ActionContext.getParameter("province");
		List<String> city_list = SystemConfig.Sys_Province_City_Map.get(province);
		if (!Utils.isHasData(city_list)) {
			city_list = new ArrayList<String>();
		}
		Gson gson = new Gson();
		renderText(gson.toJson(city_list));
		return;
	}

}

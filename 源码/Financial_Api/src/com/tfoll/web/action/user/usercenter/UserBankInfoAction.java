package com.tfoll.web.action.user.usercenter;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserBankInfoM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;

@ActionKey("/user/usercenter/bank_info")
public class UserBankInfoAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "跳转银行卡信息页面", last_update_author = "zjb")
	public void bank_info_index() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ?  ", new Object[] { user_id });
		String bank_card_num_one = user_bank_info.getString("bank_card_num_one");
		String bank_card_num_two = user_bank_info.getString("bank_card_num_two");
		String bank_card_num_three = user_bank_info.getString("bank_card_num_three");

		setAttribute("bank_card_num_one", bank_card_num_one);
		setAttribute("bank_card_num_two", bank_card_num_two);
		setAttribute("bank_card_num_three", bank_card_num_three);

		setAttribute("user_bank_info", user_bank_info);

		renderJsp("/user/usercenter/account_card_information.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "添加银行卡信息", last_update_author = "zjb")
	public void bank_info_add() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");
		String money_password = user.getString("money_password");
		String real_name = user.getString("real_name");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}

		if (!Utils.isNotNullAndNotEmptyString(real_name)) {
			renderText("no_real_name");
			return;
		}
		String opening_bank = getParameter("opening_bank");
		String bank_name = getParameter("bank_name");
		String bank_address = getParameter("bank_address");
		String bank_card_num = getParameter("bank_card_num");
		String code = getParameter("code");

		String bank[] = { bank_name, bank_address, opening_bank, bank_card_num, code };
		String bank2[] = { "bank_name", "bank_address", "opening_bank", "bank_card_num" };
		for (int i = 0; i < bank.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(bank[i])) {
				renderText(i + "");
				return;
			}
		}

		if (!money_password.equals(MD5.md5(code))) {
			renderText("err");
			return;
		}

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ?  ", new Object[] { user_id });
		String bank_card_num_one = user_bank_info.get("bank_card_num_one");
		String bank_card_num_two = user_bank_info.get("bank_card_num_two");
		String bank_card_num_three = user_bank_info.get("bank_card_num_three");
		String num;

		if (!Utils.isNotNullAndNotEmptyString(bank_card_num_one)) {
			num = "_one";
		} else if (!Utils.isNotNullAndNotEmptyString(bank_card_num_two)) {
			num = "_two";
		} else if (!Utils.isNotNullAndNotEmptyString(bank_card_num_three)) {
			num = "_three";
		} else {
			renderText("over");
			return;
		}

		for (int i = 0; i < bank.length - 1; i++) {
			user_bank_info.set(bank2[i] + num, bank[i]);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "修改银行卡信息", last_update_author = "zjb")
	public void bank_info_update() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		String money_password = user.getString("money_password");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}
		String opening_bank = getParameter("opening_bank");
		String bank_name = getParameter("bank_name");
		String bank_address = getParameter("bank_address");
		String code = getParameter("code");
		String num = getParameter("num");

		String bank[] = { bank_name, bank_address, opening_bank, code };
		String bank2[] = { "bank_name", "bank_address", "opening_bank" };
		for (int i = 0; i < bank.length; i++) {
			if (!Utils.isNotNullAndNotEmptyString(bank[i])) {
				renderText(i + num);
				return;
			}
		}

		if (!money_password.equals(MD5.md5(code))) {
			renderText("err");
			return;
		}

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ? ", new Object[] { user_id });

		for (int i = 0; i < bank.length - 1; i++) {
			user_bank_info.set(bank2[i] + num, bank[i]);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserLoginedAop.class)
	@Function(for_people = "登录", function_description = "删除银行卡信息", last_update_author = "zjb")
	public void bank_info_delete() {
		UserM user = getSessionAttribute(SystemConstantKey.User);
		int user_id = user.getInt("id");

		String money_password = user.getString("money_password");

		if (!Utils.isNotNullAndNotEmptyString(money_password)) {
			renderText("no_money_password");
			return;
		}

		String num = getParameter("num");

		String bank_string[] = { "bank_name", "bank_address", "opening_bank", "bank_card_num" };

		UserBankInfoM user_bank_info = UserBankInfoM.dao.findFirst("select * from user_bank_info where user_id = ? ", new Object[] { user_id });

		for (int i = 0; i < bank_string.length; i++) {
			user_bank_info.set(bank_string[i] + num, null);
		}
		boolean ok = user_bank_info.update();

		if (ok) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

}

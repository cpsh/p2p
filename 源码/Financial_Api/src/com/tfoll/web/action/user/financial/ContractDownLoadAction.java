package com.tfoll.web.action.user.financial;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.ajax.Ajax;
import com.tfoll.web.aop.ajax.UserLoginedAjax;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.xhtmlrenderer.pdf.ITextRenderer;

@ActionKey("/user/financial/contractdownload")
public class ContractDownLoadAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(value = { Ajax.class, UserLoginedAjax.class })
	@Function(for_people = "所有人", function_description = "HTML 转为PDF 文档", last_update_author = "hx")
	public void html_transfer_to_pdf() throws DocumentException, IOException {

		String doctype = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\">";
		StringBuffer buffer = new StringBuffer(doctype);
		buffer.append("<head></head>");
		buffer.append("<body>");
		String content = getParameter("content");
		buffer.append(content);
		buffer.append("</body>");
		String html_str = "</html>";
		buffer.append(html_str);
		String HtmlDocment = buffer.toString();

		// System.out.println("----------content-----------"+content);
		// ================================================
		// METHOD 1 方法一 itextpdf 5.4.2 ；xmlworker 5.4.1
		/*
		 * //step 1 Document document = new Document(); //step 2 PdfWriter
		 * writer = PdfWriter.getInstance(document, new
		 * FileOutputStream("TEST.PDF")); //step 3 document.open(); //step 4
		 * XMLWorkerHelper.getInstance().parseXHtml(writer, document,new
		 * FileInputStream("index.html")); //step 5 document.close();
		 */

		// ======================================================
		// method 2 方法二
		// step 1
		// String inputFile = "index.html";
		// String url = new File(inputFile).toURI().toURL().toString();

		String outputFile = "C:/Users/tf/Desktop/PDF_PDF.pdf";
		// System.out.println(url);
		// step 2
		OutputStream os = new FileOutputStream(outputFile);
		org.xhtmlrenderer.pdf.ITextRenderer renderer = new ITextRenderer();
		// renderer.setDocument();
		renderer.setDocumentFromString(HtmlDocment);

		// step 3 解决中文支持
		org.xhtmlrenderer.pdf.ITextFontResolver fontResolver = renderer.getFontResolver();
		fontResolver.addFont("c:/Windows/Fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

		renderer.layout();
		renderer.createPDF(os);
		os.close();

		System.out.println("create pdf done!!");
		// ==================================================

		/*
		 * HttpServletResponse response = getResponse(); HttpServletRequest
		 * request = getRequest(); String path =
		 * request.getSession().getServletContext
		 * ().getRealPath("/upload/temp_upload_folder/hx_contract.pdf"); File
		 * file = new File(path); renderFile(file); return;
		 */

		// String path = request.getContextPath();//文件路径 （指定生产的PDF文件）
		/*
		 * System.out.println("-----------path-----------"+path); String
		 * filename = path.substring(path.lastIndexOf("\\")+1);//生产文件的默认文件名
		 * System.out.println("------------filename------"+filename);
		 * InputStream in = null; OutputStream out = null; try {
		 * response.setHeader("content-disposition", "attachment;filename=" +
		 * URLEncoder.encode(filename, "UTF-8"));
		 * 
		 * in = new FileInputStream(path); int len = 0; byte[] buffer = new
		 * byte[1024]; out = response.getOutputStream(); while((len =
		 * in.read(buffer)) > 0) { out.write(buffer,0,len); } renderText(""); }
		 * catch (UnsupportedEncodingException e1) { e1.printStackTrace(); }
		 * catch(Exception e) { throw new RuntimeException(e); } finally { if(in
		 * != null) { try { in.close(); return; }catch(Exception e) { throw new
		 * RuntimeException(e); }
		 * 
		 * } }
		 */
	}
}

package com.tfoll.web.action.user_online_manager;

import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.UserOnLineManagerLoginedAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.AdminM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.MD5;
import com.tfoll.web.util.Utils;

@ActionKey("/user_login")
public class UserLoginAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserOnLineManagerLoginedAop.class)
	@Function(for_people = "所有人", function_description = "管理员登录用户", last_update_author = "曹正辉")
	public void index() {
		renderJsp("/user_online_manager/user_login/user_login_index.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(UserOnLineManagerLoginedAop.class)
	@Function(for_people = "所有人", function_description = "管理员登录用户", last_update_author = "曹正辉")
	public void user_login() {
		String email = getParameter("email");

		// 1
		String admin1_username = getParameter("admin1_username");
		String admin1_password = getParameter("admin1_password");

		// 2
		String admin2_username = getParameter("admin2_username");
		String admin2_password = getParameter("admin2_password");
		// 3
		String admin3_username = getParameter("admin3_username");
		String admin3_password = getParameter("admin3_password");

		/**
		 * 首先判断是否为空，如果其中任何一个为空则...
		 */

		if (Utils.isNullOrEmptyString(email)) {
			renderText("");
			return;
		}
		//
		if (Utils.isNullOrEmptyString(admin1_username)) {
			renderText("");
			return;
		}
		if (Utils.isNullOrEmptyString(admin1_password)) {
			renderText("");
			return;
		}
		//
		if (Utils.isNullOrEmptyString(admin2_username)) {
			renderText("");
			return;
		}
		if (Utils.isNullOrEmptyString(admin2_password)) {
			renderText("");
			return;
		}
		//
		if (Utils.isNullOrEmptyString(admin3_username)) {
			renderText("");
			return;
		}
		if (Utils.isNullOrEmptyString(admin3_password)) {
			renderText("");
			return;
		}

		/**
		 * 判断三个是否一致-为同一个账户
		 */
		if (admin1_username.equals(admin2_username)) {
			logger.error("用户名:" + admin1_username + "尝试使用一个账户登录几个管理员用户");
			renderText("尝试使用一个账户登录几个管理员用户");
			return;
		}
		if (admin1_username.equals(admin3_username)) {
			logger.error("用户名:" + admin1_username + "尝试使用一个账户登录几个管理员用户");
			renderText("尝试使用一个账户登录几个管理员用户");
			return;
		}
		if (admin2_username.equals(admin3_username)) {
			logger.error("用户名:" + admin2_username + "尝试使用一个账户登录几个管理员用户");
			renderText("尝试使用一个账户登录几个管理员用户");
			return;
		}

		/**
		 * 校验该用户存在不
		 */

		UserM user = UserM.dao.findFirst("SELECT * from user_info WHERE email=?", new Object[] { email });
		if (user == null) {
			setAttribute("tips", "该用户不存在");
			renderJsp("/user_online_manager/user_login/user_login_index.jsp");
			return;
		}
		AdminM admin_1 = AdminM.dao.findFirst("SELECT * FROM admin WHERE login_account =? and login_pwd=?", new Object[] { admin1_username, MD5.md5(admin1_password) });
		AdminM admin_2 = AdminM.dao.findFirst("SELECT * FROM admin WHERE login_account =? and login_pwd=?", new Object[] { admin2_username, MD5.md5(admin2_password) });
		AdminM admin_3 = AdminM.dao.findFirst("SELECT * FROM admin WHERE login_account =? and login_pwd=?", new Object[] { admin3_username, MD5.md5(admin3_password) });

		if (!(admin_1 != null && admin_2 != null && admin_3 != null)) {
			setAttribute("tips", "管理员验证...");
			renderJsp("/user_online_manager/user_login/user_login_index.jsp");
			return;
		}

		setSessionAttribute(SystemConstantKey.User, user);
		renderJsp("/index/user_exit_to_index.html");
		return;

	}

}
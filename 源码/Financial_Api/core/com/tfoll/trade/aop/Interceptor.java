package com.tfoll.trade.aop;

/**
 * Interceptor在Jfly框架里面是单向的,使用的数组模型而不是责任链模式<br/>
 */
public interface Interceptor {

	/**
	 * 一定要调用ActionContent.invoke()方法进行下一个拦截器的执行 <br/>
	 * 执行拦截行为
	 */
	public void doIt(ActionExecutor ae);
}

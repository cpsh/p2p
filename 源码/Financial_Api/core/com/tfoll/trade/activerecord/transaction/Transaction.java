package com.tfoll.trade.activerecord.transaction;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 * 本地连接事物管理容器. 注意本框架不支持嵌套事务即-事务[事务[]...].
 */
public class Transaction {
	public static Logger logger = Logger.getLogger(Transaction.class);
	/**
	 * 数据库连接
	 */
	private static ThreadLocal<Connection> $a = new ThreadLocal<Connection>();
	/**
	 * 支持事务?
	 */
	private static ThreadLocal<Boolean> $b = new ThreadLocal<Boolean>();

	public static final Connection getConnection() {
		Connection conn = Transaction.$a.get();
		try {
			if (conn != null && !conn.isClosed()) {
				return conn;
			} else {
				return null;
			}
		} catch (SQLException e) {
			logger.error(e);
			return null;
		}
	}

	/**
	 * @return 返回两个值null和boolean.该框架速度很快的核心
	 */
	public static final boolean isSurpportTransaction() {
		Boolean _$b = Transaction.$b.get();

		if (_$b != null && _$b == true) {
			return true;
		} else {
			return false;
		}

	}

	public static final void removeTransaction() {
		if (Transaction.$b.get() != null) {
			Transaction.$b.remove();
		}

	}

	public static final void removeConnection() {
		if (Transaction.$a.get() != null) {
			Transaction.$a.remove();
		}

	}

	public static final void set(Boolean transaction) {
		Transaction.$b.set(transaction);
	}

	public static final void set(Connection conn) {
		// 永远不能判断是否为空-因为一定不为空
		Transaction.$a.set(conn);
	}
}

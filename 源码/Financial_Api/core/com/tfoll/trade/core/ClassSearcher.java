package com.tfoll.trade.core;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * 特定类扫描器--系统自动化构建器<br/>
 * 在框架启动的时候进行反射取得标记了注解的类上面的业务注解,提升性能<br/>
 */
public class ClassSearcher {

	private static Logger logger = Logger.getLogger(ClassSearcher.class);

	/**
	 * /WebRoot/WEB-INF/classes/目录
	 */
	public final static URL classPathUrl = ClassSearcher.class.getResource("/");

	/**
	 * 在ClassPath下面查找指定类的子类
	 * 
	 */

	@SuppressWarnings("unchecked")
	public static List<Class> findSubClassInClasspath(Class clazz) {
		return findSubClassInClassList(findClassFilePathList(classPathUrl.getFile(), "*.class"), clazz);
	}

	/**
	 * 在ClassPath下面查找指定Annotation
	 * 
	 */
	public static List<Class<?>> findAnnotationClassInClasspath(Class<? extends Annotation> annotationClass) {
		return findAnnotaionClassInClassList(findClassFilePathList(classPathUrl.getFile(), "*.class"), annotationClass);
	}

	/**
	 * 在ClassPath下面查找含有目标接口的一级实现类
	 * 
	 */
	public static List<Class<?>> findImplementsClassInClassList(Class<?> clazz) {
		return findImplementsClassInClassList(findClassFilePathList(classPathUrl.getFile(), "*.class"), clazz);
	}

	/**
	 * 递归查找文件--CLASSPATH
	 * 
	 * @param baseDirectoryPath
	 *            需要查找的文件的父文件夹路径
	 * @param targetFileName
	 *            需要查找的文件的文件名
	 */
	private static List<String> findClassFilePathList(String baseDirectoryPath, String targetFileName) {
		/**
		 * 算法简述： 从某个给定的需查找的文件夹出发，搜索该文件夹的所有子文件夹及文件，
		 * 若为文件，则进行匹配，匹配成功则加入结果集，若为子文件夹，则进队列。 队列不空，重复上述操作，队列为空，程序结束，返回结果。
		 */
		List<String> classFilePathList = new ArrayList<String>();
		String subFileName = null;
		File baseDirectory = new File(baseDirectoryPath);
		if (!baseDirectory.exists() || !baseDirectory.isDirectory()) {
			logger.debug("查找错误:" + baseDirectoryPath + "不是一个目录!");
		} else {
			String[] filelist = baseDirectory.list();
			int length = filelist.length;
			for (int i = 0; i < length; i++) {
				File subFile = new File(baseDirectoryPath + File.separator + filelist[i]);
				if (!subFile.isDirectory()) {
					subFileName = subFile.getName();
					if (ClassSearcher.wildcardMatch(subFileName, targetFileName)) {
						String classname;
						String tempString = subFile.getAbsoluteFile().toString().toString().replaceAll("\\\\", "/");

						classname = tempString.substring(tempString.indexOf("/classes") + "/classes".length(), tempString.indexOf(".class"));
						if (classname.startsWith("/")) {
							classname = classname.substring(classname.indexOf("/") + 1);
						}
						classname = classname.replaceAll("\\\\", "/").replaceAll("/", ".");
						classFilePathList.add(classname);
					}
				} else if (subFile.isDirectory()) {
					// 文件夹
					classFilePathList.addAll(findClassFilePathList(baseDirectoryPath + File.separator + filelist[i], targetFileName));
				}
			}
		}
		return classFilePathList;
	}

	/**
	 * 核心方法:在类文件中查找含有目标类子类的类
	 */

	@SuppressWarnings("unchecked")
	private static List<Class> findSubClassInClassList(List<String> classFileList, Class superclass) {
		List<Class> classList = new ArrayList<Class>();
		for (String classFile : classFileList) {

			try {
				Class<?> classInFile = Class.forName(classFile);
				if (classInFile.getSuperclass() == superclass) {
					classList.add(classInFile);
				}
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return classList;
	}

	/**
	 * 核心方法:在类文件中查找含有目标注解的类
	 */
	private static List<Class<?>> findAnnotaionClassInClassList(List<String> classFileList, Class<? extends Annotation> annotationClass) {
		List<Class<?>> classList = new ArrayList<Class<?>>();
		for (String classFile : classFileList) {

			try {
				Class<?> classInFile = Class.forName(classFile);
				if (classInFile.isAnnotationPresent(annotationClass)) {
					classList.add(classInFile);
				}
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return classList;
	}

	/**
	 * 核心方法:在类文件中查找含有目标接口的一级实现类
	 */
	private static List<Class<?>> findImplementsClassInClassList(List<String> classFileList, Class<?> clazz) {
		List<Class<?>> classList = new ArrayList<Class<?>>();
		for (String classFile : classFileList) {

			try {
				Class<?> classInFile = Class.forName(classFile);
				Class<?>[] interfaces = classInFile.getInterfaces();
				if (interfaces != null && interfaces.length > 0) {
					int length = interfaces.length;
					for (int i = 0; i < length; i++) {
						if (interfaces[i].getName().equals(clazz.getName())) {
							classList.add(classInFile);
							break;
						}
					}
				}
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
		return classList;
	}

	/**
	 * 通配符匹配--核心算法--CLASSPATH
	 * 
	 * @param string
	 *            待匹配的字符串
	 * @param pattern
	 *            通配符模式--eg:x.class
	 * 
	 * @return 匹配成功则返回true，否则返回false
	 */
	private static boolean wildcardMatch(String string, String pattern) {
		int patternLength = pattern.length();
		int stringLength = string.length();
		int stringIndex = 0;
		char ch;
		for (int patternIndex = 0; patternIndex < patternLength; patternIndex++) {

			ch = pattern.charAt(patternIndex);
			if (ch == '*') {
				// 通配符星号*表示可以匹配任意多个字符
				while (stringIndex < stringLength) {
					/**
					 * 继续匹配尝试stringIndex<->patternIndex + 1
					 */
					if (wildcardMatch(string.substring(stringIndex), pattern.substring(patternIndex + 1))) {
						return true;
					}
					stringIndex++;
				}
			} else if (ch == '?') {
				// 通配符问号?表示匹配任意一个字符
				stringIndex++;
				if (stringIndex > stringLength) {
					// 表示string中已经没有字符匹配?了。
					return false;
				}
			} else {
				/**
				 * 字符搜索完毕或者不匹配
				 */
				if ((stringIndex >= stringLength) || (ch != string.charAt(stringIndex))) {
					return false;
				}
				stringIndex++;
			}
		}
		return (stringIndex == stringLength);
	}

}
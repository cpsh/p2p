package com.tfoll.trade.render;

import com.tfoll.trade.core.ActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public abstract class Render {

	public static Logger logger = Logger.getLogger(Render.class);

	public HttpServletRequest getRequest() {
		return ActionContext.getRequest();
	}

	public HttpServletResponse getResponse() {
		return ActionContext.getResponse();
	}

	public abstract void render();

}

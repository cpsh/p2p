package com.tfoll.trade.render;

import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.RouteSet;
import com.tfoll.trade.core.ActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RootRender extends Render {

	public String view;

	/**
	 * 自定义路径
	 */
	public RootRender(String view) {
		this.view = view;
	}

	@Override
	public void render() {
		HttpServletRequest request = ActionContext.getRequest();
		HttpServletResponse response = ActionContext.getResponse();
		try {
			request.setCharacterEncoding(Constants.characterEncoding);
			request.setCharacterEncoding(Constants.characterEncoding);
			response.setContentType("text/html; charset=" + Constants.characterEncoding);
			// /jsp/*
			if (Constants.devMode) {
				logger.debug("跳转的url" + RouteSet.baseViewPath + view);
			}
			request.getRequestDispatcher(RouteSet.baseViewPath + view).forward(request, response);

		} catch (Exception e) {
			throw new RenderException(e);
		}
	}

}

package com.tfoll.trade.core;

import com.tfoll.trade.config.InterceptorList;
import com.tfoll.trade.config.PluginList;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.plugin.IPlugin;

import java.util.List;

import org.apache.log4j.Logger;

/**
 * 框架初始化顺序:initActionMapping()initActiveRecord()
 */
public class WebContent {
	public static Logger logger = Logger.getLogger(WebContent.class);

	public static final InterceptorList interceptorList = new InterceptorList();

	public static final PluginList pluginList = new PluginList();

	/**
	 * 设置用户项目开发配置到框架配置中 : interceptors plugins
	 * 
	 * @return
	 */
	public static boolean initWebContent(UserConfig userConfig) {
		// jsp存放的文件
		logger.info("====================配置jsp存放的文件====================");
		userConfig.configRoute();
		// 基础性拦截器
		logger.info("====================配置基础性拦截器====================");
		userConfig.configInterceptor(WebContent.interceptorList);

		// 配置和启动插件
		logger.info("====================配置和启动插件====================");
		userConfig.configPlugin(WebContent.pluginList);
		startPlugins(WebContent.pluginList);

		logger.info("====================ActionMapping构建开始====================");
		ActionMapping.buildActionMapping(WebContent.interceptorList.getInterceptorList());
		logger.info("====================ActionMapping构建成功====================");

		logger.info("====================读取数据库信息，构建ActiveRecord模块开始====================");
		userConfig.configActiveRecord();
		logger.info("====================读取数据库信息，构建ActiveRecord模块成功====================");

		userConfig.afterBuildOK();
		logger.info("====================框架信息构建完毕，项目配置OK====================");
		return true;
	}

	// 插件配置
	private static void startPlugins(PluginList plugins) {
		logger.info("====================开始启动插件集====================");
		List<IPlugin> pluginList = plugins.getPluginList();
		if (pluginList != null) {
			for (IPlugin plugin : pluginList) {
				try {
					boolean success = plugin.start();
					if (!success) {
						logger.error("插件启动失败: " + plugin.getClass().getName());
						throw new RuntimeException("插件启动失败: " + plugin.getClass().getName());
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("插件启动失败: " + plugin.getClass().getName());
					throw new RuntimeException("插件启动失败: " + plugin.getClass().getName(), e);
				}
			}
		}
		logger.info("====================成功启动插件集====================");
	}

	// 插件配置
	public static void stopPlugins(PluginList plugins) {
		logger.info("====================开始关闭插件集====================");
		List<IPlugin> pluginList = plugins.getPluginList();

		if (pluginList != null) {
			for (IPlugin plugin : pluginList) {
				try {
					boolean success = plugin.stop();
					if (!success) {
						logger.error("插件关闭失败: " + plugin.getClass().getName());
						throw new RuntimeException("插件关闭失败: " + plugin.getClass().getName());
					}
				} catch (Exception e) {
					logger.error("插件关闭失败: " + plugin.getClass().getName());
					throw new RuntimeException("插件关闭失败: " + plugin.getClass().getName(), e);
				}
			}
		}

		logger.info("====================成功关闭插件集====================");
	}
}

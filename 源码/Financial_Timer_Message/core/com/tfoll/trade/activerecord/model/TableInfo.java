package com.tfoll.trade.activerecord.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 本类记录表字段名及其类型
 */
public class TableInfo {

	private Map<String, Class<?>> columnTypeMap = new HashMap<String, Class<?>>();

	public Map<String, Class<?>> getColumnTypeMap() {
		return columnTypeMap;
	}

	private Class<? extends Model<?>> modelClass;

	private String primaryKey;

	private String tableName;

	public TableInfo() {
	}

	/**
	 * 默认值是ID
	 */
	public TableInfo(String tableName, Class<? extends Model<?>> modelClass) {
		this(tableName, "id", modelClass);
	}

	// 构造方法
	public TableInfo(String tableName, String primaryKey, Class<? extends Model<?>> modelClass) {
		if (tableName == null || "".equals(tableName.trim())) {
			throw new NullPointerException("表名为空");
		}
		if (primaryKey == null || "".equals(primaryKey.trim())) {
			throw new NullPointerException("主键为空");
		}
		if (modelClass == null) {
			throw new NullPointerException("Model类为空");
		}
		this.tableName = tableName.trim();
		this.primaryKey = primaryKey.trim();
		this.modelClass = modelClass;
	}

	/**
	 * 添加字段信息
	 */
	public void addColumnInfo(String columnName, Class<?> columnType) {
		if (columnTypeMap.containsKey(columnName)) {
			throw new IllegalArgumentException("属性:" + columnName + "重复");
		}
		columnTypeMap.put(columnName, columnType);
	}

	@SuppressWarnings("unchecked")
	public void getAllColumnMapInfo() {
		if (columnTypeMap != null && columnTypeMap.size() != 0) {
			System.out.println("打印字段类型");
			for (Entry<String, Class<?>> entry : columnTypeMap.entrySet()) {
				String key = entry.getKey();
				Class clazz = entry.getValue();
				System.out.println("key:" + key + "类型" + clazz.getName());

			}

		}
	}

	public boolean containsColumnName(String columnLabel) {
		return columnTypeMap.containsKey(columnLabel);
	}

	public Class<?> getColumnType(String columnName) {
		return columnTypeMap.get(columnName);
	}

	public Class<? extends Model<?>> getModelClass() {
		return modelClass;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public String getTableName() {
		return tableName;
	}

}

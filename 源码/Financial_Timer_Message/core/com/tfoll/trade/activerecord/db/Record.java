package com.tfoll.trade.activerecord.db;

import com.tfoll.trade.config.Constants;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Record并不记录数据库字段信息,所以无法获取主键信息
 * 
 */
public class Record {
	/**
	 * 一行记录
	 */
	private Map<String, Object> R = new HashMap<String, Object>();// 修改底层的时候不能进行修改-否则导致外面去数据的时候有问题

	public Map<String, Object> getR() {
		return R;
	}

	// 添加字段
	public Record add(String column, Object value) {
		R.put(column, value);
		return this;
	}

	public Record addColumnMap(Map<String, Object> columnMap) {
		this.R.putAll(columnMap);
		return this;
	}

	public Record clear() {
		if (R != null) {
			R.clear();
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String column) {
		return (T) R.get(column);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String column, Object defaultValue) {
		Object result = R.get(column);
		return (T) (result != null ? result : defaultValue);
	}

	public BigInteger getBigInteger(String column) {
		return (BigInteger) R.get(column);
	}

	public BigDecimal getBigDecimal(String column) {
		return (BigDecimal) R.get(column);
	}

	public Boolean getBoolean(String column) {
		return (Boolean) R.get(column);
	}

	public byte[] getBytes(String column) {
		return (byte[]) R.get(column);
	}

	public Map<String, Object> getColumnMap() {
		return R;
	}

	public java.sql.Date getDate(String column) {
		return (java.sql.Date) R.get(column);
	}

	public Double getDouble(String column) {
		return (Double) R.get(column);
	}

	public Float getFloat(String column) {
		return (Float) R.get(column);
	}

	public Integer getInt(String column) {
		return (Integer) R.get(column);
	}

	public Long getLong(String column) {
		return (Long) R.get(column);
	}

	public Number getNumber(String column) {
		return (Number) R.get(column);
	}

	public Short getShort(String column) {
		return (Short) R.get(column);
	}

	public String getString(String column) {
		return (String) R.get(column);
	}

	public java.sql.Time getTime(String column) {
		return (java.sql.Time) R.get(column);
	}

	public java.sql.Timestamp getTimestamp(String column) {
		return (java.sql.Timestamp) R.get(column);
	}

	/**
	 * Timestamp t=(java.sql.Timestamp) R.get(column); return new
	 * java.util.Date(t.getTime());
	 */
	public java.util.Date getUtilDate(String column) {
		Timestamp t = (java.sql.Timestamp) R.get(column);
		return new java.util.Date(t.getTime());
	}

	public void setColumnMap(Map<String, Object> columnMap) {
		this.R = columnMap;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Record)) {
			return false;
		}
		if (o == this) {
			return true;
		}
		return this.R.equals(((Record) o).R);
	}

	@Override
	public int hashCode() {
		return R == null ? 0 : R.hashCode();
	}

	@Override
	public String toString() {
		if (Constants.devMode) {
			if (R.size() > 0) {
				Set<String> keySet = R.keySet();
				Iterator<String> it = keySet.iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					System.out.println("key:" + key + "-value:" + R.get(key));

				}
			}

		}
		return "Record [columnMap=" + R + "]";
	}
}

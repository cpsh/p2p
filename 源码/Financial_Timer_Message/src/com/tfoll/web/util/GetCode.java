package com.tfoll.web.util;

import java.util.Random;

public class GetCode {

	static String[] message_array = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
	static Random message_random = new Random();

	public static String get_code() {
		StringBuilder sb = new StringBuilder(6);
		for (int i = 0; i < 6; i++) {
			try {
				sb.append(message_random.nextInt(9));
			} catch (Exception e) {
				sb.append(7);
			}
		}
		String msg = sb.toString();
		return msg;
	}
}

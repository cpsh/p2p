package com.tfoll.web.util;

import com.tfoll.trade.activerecord.model.Model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 理财和借贷和债权转让公式
 * 
 */
public class CalculationFormula {
	/**
	 * 等额本息还款法是一种被广泛采用的还款方式。在还款期内，每月偿还同等数额的借款(包括本金和利息)。借款人每月还款额中的本金比重逐月递增、
	 * 利息比重逐月递减。
	 * 
	 * @param all_money
	 *            借款本金
	 * @param month_rates
	 *            每月利率
	 * @param periods_number
	 *            还款总期数
	 * @return 每月还款额(月还本金 + 利息)
	 */
	public static BigDecimal get_principal_and_interest(BigDecimal all_money, BigDecimal month_rates, int periods_number) {
		BigDecimal top = all_money.multiply(month_rates).multiply((new BigDecimal(1).add(month_rates)).pow(periods_number));
		BigDecimal bottom = ((new BigDecimal(1).add(month_rates)).pow(periods_number)).subtract(new BigDecimal(1));
		return top.divide(bottom, 3, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 如果逾期还款，您要承担罚息与逾期后的管理费用
	 * 
	 * 罚息总额计算公式
	 * 
	 * @param benxi
	 *            逾期本息
	 * 
	 * @param date
	 *            还款截止时间
	 * 
	 * @return zonge 罚息总额
	 * 
	 */
	public static BigDecimal get_punish_principal_and_interest(BigDecimal benxi, Date date) {

		BigDecimal zonge = new BigDecimal(0);
		Date now = new Date();

		int pass_day = (int) Math.ceil(new Long(now.getTime() - date.getTime()).doubleValue() / new Long(24 * 60 * 60 * 1000).doubleValue()); // 逾期天数

		if (pass_day <= 0) {

		} else if (pass_day < 31) {
			zonge = benxi.multiply(new BigDecimal(pass_day * 0.0005));
		} else {
			zonge = benxi.multiply(new BigDecimal(pass_day * 0.001));
		}

		return zonge;

	}

	/**
	 * 如果逾期还款，您要承担罚息与逾期后的管理费用
	 * 
	 * 获取逾期管理费
	 * 
	 * @param benxi
	 *            逾期本息
	 * 
	 * @param date
	 *            还款截止时间
	 * 
	 * @return manage_fee 逾期管理费
	 * 
	 */
	public static BigDecimal get_punish_manage_fee(BigDecimal benxi, Date date) {

		BigDecimal manage_fee = new BigDecimal(0);
		Date now = new Date();

		int pass_day = (int) Math.ceil(new Long(now.getTime() - date.getTime()).doubleValue() / new Long(24 * 60 * 60 * 1000).doubleValue()); // 逾期天数

		if (pass_day <= 0) {

		} else if (pass_day < 31) {
			manage_fee = benxi.multiply(new BigDecimal(pass_day * 0.001));
		} else {
			manage_fee = benxi.multiply(new BigDecimal(pass_day * 0.005));
		}

		return manage_fee;
	}

	/**
	 * 还款日期表
	 * 
	 * @param borrowDate
	 *            借款日期
	 * @param period
	 *            总期数
	 * @return
	 */
	public Date get_repayment_date(Date borrowDate, int period) {

		return null;
	}

	/**
	 * 
	 * 还款计划，该方法只是用于测试 算法写得对不对
	 * 
	 * @param remain_principal
	 *            借款金额
	 * @param year_interest_rate
	 *            年化利率
	 * @param total_period
	 *            总期数
	 * @param cuurent_period
	 *            前n期的
	 * @param plan
	 * @return
	 */
	public static List<RepaymentPlan> get_repayment_plan(double remain_principal, double year_interest_rate, int total_period, int cuurent_period) {
		List<RepaymentPlan> repayment_plan_list = new ArrayList<RepaymentPlan>();
		get_interest_month(remain_principal, year_interest_rate, total_period, cuurent_period, repayment_plan_list);
		/**
		 * list是顺序存储数据的
		 */
		Date now = new Date();
		/**
		 * 上一次的还款时间
		 */
		Date last_month_date = now;
		String last_month_date_string = Model.Date.format(last_month_date);

		if (Utils.isHasData(repayment_plan_list)) {
			int size = repayment_plan_list.size();

			for (int i = 0; i < size; i++) {
				RepaymentPlan repayment_plan = repayment_plan_list.get(i);
				int month_count = i + 1;

				String next_payment_date_day = create_the_repayment_date(now, month_count);

				/**
				 * 需要计算出上次还款的那天结束时间和这个还款的结束时间和自动还款日期
				 */
				// 上个月还款那天的晚上+1毫秒，第一个月是创建当时.
				repayment_plan.setRepayment_start_time(get_date_just_other_day(last_month_date_string));
				last_month_date_string = next_payment_date_day;// 将这个月的自动还款的那天作为下个月的开始的第一天的前一天

				repayment_plan.setRepayment_end_time(get_date_just_last(next_payment_date_day));
				repayment_plan.setAuto_repayment_date(next_payment_date_day);
			}
		}
		return repayment_plan_list;
	}

	public static double get_interest_month(double remain_principal, double year_interest_rate, int total_period, int cuurent_period, List<RepaymentPlan> plan) {

		// 等额本息
		double month_interest_rate = year_interest_rate / 12;
		final double principal_and_interest = 888.488;
		// get_principal_and_interest(new BigDecimal(remain_principal),new
		// BigDecimal(month_interest_rate),total_period).doubleValue();

		RepaymentPlan p = new RepaymentPlan();

		// 月还利息
		double interest_month = remain_principal * (year_interest_rate / 12);
		p.setInterest_month(interest_month);

		// 月还本金
		double pricipal_month = principal_and_interest - interest_month;
		p.setPricipal_month(pricipal_month);

		// 剩余本金
		remain_principal = remain_principal - pricipal_month;
		p.setRemain_principal(remain_principal);

		// System.out.println(
		// "  等额本息:"+principal_and_interest+"\t月还本金:"+format.format(pricipal_month)+"\t月还利息:"+format.format(interest_month)+"\t剩余未还本金"+format.format(remain_principal));
		plan.add(p);

		cuurent_period = cuurent_period - 1;
		if (cuurent_period == 0)
			return interest_month;
		else {
			return get_interest_month(remain_principal, year_interest_rate, total_period, cuurent_period, plan);
		}
	}

	/**
	 * 转让时，当期还款处于未还款状态 的债权价值
	 * 
	 * @param remain_money
	 *            剩余未还本金
	 * @param deal_date
	 *            债权转让成交日期
	 * @param last_repay_date
	 *            上期还款对应的应还款日期
	 * @param year_interest_rate
	 *            年化利率
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value1(BigDecimal remain_money, Date deal_date, Date last_repay_date, BigDecimal year_interest_rate) {

		BigDecimal debt_value = null;
		BigDecimal month_interest_rate = year_interest_rate.divide(new BigDecimal(12), 3, BigDecimal.ROUND_DOWN);

		// 应计利息天数
		int should_count_day = (int) ((deal_date.getTime() - last_repay_date.getTime()) / (1000 * 60 * 60 * 24));
		should_count_day = should_count_day < 30 ? should_count_day : 30;

		debt_value = remain_money.add(remain_money.multiply(month_interest_rate).multiply(new BigDecimal(new Double(should_count_day) / 30)));

		return debt_value;
	}

	/**
	 * 转让时，当期还款处于已还款状态，但下一期处于未还款状态
	 * 
	 * @param remain_money
	 *            剩余未还本金
	 * @param deal_date
	 *            成交日期
	 * @param repay_date
	 *            成交日期所在期的应还款日期
	 * @param year_interest_rate
	 *            年化利率
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value2(BigDecimal remain_money, Date deal_date, Date repay_date, BigDecimal year_interest_rate) {

		BigDecimal debt_value = null;
		BigDecimal month_interest_rate = year_interest_rate.divide(new BigDecimal(12), 3, BigDecimal.ROUND_DOWN);

		// 天数差
		int should_count_day = (int) ((repay_date.getTime() - deal_date.getTime()) / (1000 * 60 * 60 * 24));
		should_count_day = should_count_day < 30 ? should_count_day : 30;

		debt_value = remain_money.subtract(remain_money.multiply(month_interest_rate).multiply(new BigDecimal(new Double(should_count_day) / 30)));

		// debt_value = remain_money.multiply(new
		// BigDecimal(1).subtract(month_interest_rate.multiply(new
		// BigDecimal(new Double(should_count_day)/30))));

		return debt_value;
	}

	/**
	 * 转让时，当期还款处于已还款状态，且下面的N期处于已还款状态，但还未完全还清
	 * 
	 * @param remain_money
	 *            剩余未还本金
	 * @param deal_date
	 *            成交日期
	 * @param repay_date
	 *            成交日期所在期的应还款日期
	 * @param year_interest_rate
	 *            年化利率
	 * @param N
	 *            从下期开始算，已经还款的期数
	 * @return debt_value 债权价值
	 */
	public static BigDecimal get_debt_value3(BigDecimal remain_money, Date deal_date, Date repay_date, BigDecimal year_interest_rate, int N) {

		BigDecimal debt_value = null;
		BigDecimal month_interest_rate = year_interest_rate.divide(new BigDecimal(12), 3, BigDecimal.ROUND_DOWN);

		// 最后还款所在期数与成交日期所在期数之差
		int period_different = N;

		// 天数差
		int should_count_day = (int) ((repay_date.getTime() - deal_date.getTime()) / (1000 * 60 * 60 * 24));
		should_count_day = should_count_day < 30 ? should_count_day : 30;

		// debt_value =
		// remain_money.add(remain_money.multiply(month_interest_rate).multiply(new
		// BigDecimal(new Double(should_count_day)/30)));

		BigDecimal debt_value_left = remain_money.divide((new BigDecimal(1).add(month_interest_rate)).pow(period_different), 10, BigDecimal.ROUND_DOWN);

		BigDecimal debt_value_right = new BigDecimal(1).subtract(new BigDecimal(new Double(should_count_day) / 30).multiply(month_interest_rate));

		debt_value = debt_value_left.multiply(debt_value_right);

		return debt_value;
	}

	/**
	 *创建每个月份的还款日期
	 * 
	 * @param start_time
	 *            开始日期
	 * @param month
	 *            第几次还款
	 */
	public static String create_the_repayment_date(Date start_time, int count) {
		String date = Model.Date.format(start_time);
		Date day_date = null;
		try {
			day_date = Model.Date.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}

		Calendar calendar = Calendar.getInstance(); // 创建一个日历对象
		calendar.setTime(day_date);
		int year = calendar.get(Calendar.YEAR);// 年份
		// int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.add(Calendar.MONTH, count);// 添加月份
		int year_next = calendar.get(Calendar.YEAR);
		int month_next = calendar.get(Calendar.MONTH) + 1;

		/**
		 * <pre>
		 * 1 3 5 7 8 10 12 -31
		 *   4 6   9 11    -30
		 *   2 28/29
		 * </pre>
		 */
		if (month_next == 1 || month_next == 3 || month_next == 5 || month_next == 7 || month_next == 8 || month_next == 10 || month_next == 12) {// 31天满的

		}
		if (month_next == 4 || month_next == 6 || month_next == 9 || month_next == 11) {// 30天-如果以前是31天则需要改为30
			if (day == 31) {
				calendar.set(Calendar.DAY_OF_MONTH, 30);
			}
		}
		if (month_next == 2) {

			if (1 <= day && day <= 28) {

			}
			if (day == 29 || day == 30 || day == 31) {
				boolean is_leap_year = is_leap_year(year_next);
				if (is_leap_year) {
					calendar.set(Calendar.DAY_OF_MONTH, 29);
				} else {
					calendar.set(Calendar.DAY_OF_MONTH, 28);
				}
			}
		}
		// Date next_day = calendar.getTime();
		// System.out.println(month_next + "--" + day_format.format(next_day));
		return Model.Date.format(calendar.getTime());

	}

	public static final long Day = 1000 * 60 * 60 * 24;

	/**
	 * 获取某个日期的早上的日期
	 */
	public static Date get_date_just_moning(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return date;

	}

	/**
	 * 获取某个日期的晚上最后一刻个日期
	 */
	public static Date get_date_just_last(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return new Date(date.getTime() + Day - 1);

	}

	public static Date get_date_just_other_day(String date_string) {
		Date date = null;
		try {
			date = Model.Date.parse(date_string);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return new Date(date.getTime() + Day);

	}

	/**
	 * 判断当前年份是不是闰年
	 */
	private static boolean is_leap_year(int year) {
		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
			return true;
		} else {
			return false;
		}

	}

	public static void main1(String[] args) throws Exception {
		for (int j = 1; j < 13; j++) {
			System.out.println(create_the_repayment_date(new Date(), j));
		}

	}

	public static void main(String[] args) throws ParseException {

		Date date1 = Model.Time.parse("2014-10-27 00:00:00");
		Date date2 = Model.Time.parse("2014-10-28 00:00:00");

		// System.out.println("每月还款额："+get_principal_and_interest(new
		// BigDecimal("10000"),new BigDecimal("0.0091"),6));

		List<RepaymentPlan> plan = get_repayment_plan(10000, 0.12, 12, 12);

		for (RepaymentPlan p : plan)
			System.out.println(p);

		// System.out.println(get_debt_value3(new BigDecimal(10000), date1,
		// date2, new BigDecimal(0.12),1));

	}
}

class RepaymentPlan {

	public Date getRepayment_start_time() {
		return repayment_start_time;
	}

	public void setRepayment_start_time(Date repaymentStartTime) {
		repayment_start_time = repaymentStartTime;
	}

	public Date getRepayment_end_time() {
		return repayment_end_time;
	}

	public void setRepayment_end_time(Date repaymentEndTime) {
		repayment_end_time = repaymentEndTime;
	}

	public String getAuto_repayment_date() {
		return auto_repayment_date;
	}

	public void setAuto_repayment_date(String autoRepaymentDate) {
		auto_repayment_date = autoRepaymentDate;
	}

	/**
	 * 月还本金
	 */
	private double pricipal_month;

	/**
	 * 月还利息
	 */
	private double interest_month;

	/**
	 * 剩余未还本金
	 */
	private double remain_principal;

	/**
	 * 还款时间开始
	 */
	private Date repayment_start_time;
	/**
	 * 还款结束时间
	 */
	private Date repayment_end_time;
	/**
	 * 自动还款
	 */
	private String auto_repayment_date;

	public double getPricipal_month() {
		return pricipal_month;
	}

	public void setPricipal_month(double pricipalMonth) {
		pricipal_month = pricipalMonth;
	}

	public double getInterest_month() {
		return interest_month;
	}

	public void setInterest_month(double interestMonth) {
		interest_month = interestMonth;
	}

	public double getRemain_principal() {
		return remain_principal;
	}

	public void setRemain_principal(double remainPrincipal) {
		remain_principal = remainPrincipal;
	}

	@Override
	public String toString() {

		DecimalFormat format = new DecimalFormat("######.00");
		System.out.println(repayment_start_time);
		System.out.println(repayment_end_time);
		System.out.println("---------------------");
		return "RepaymentPlan [月还本金=" + format.format(interest_month) + ", 月还利息=" + format.format(pricipal_month) + ", 剩余未还本金=" + format.format(remain_principal) + "]" + "    还款开始时间" + Model.Date.format(repayment_start_time) + "    还款结束时间" + Model.Date.format(repayment_end_time) + "    自动还款时间" + auto_repayment_date;
	}

}

package com.tfoll.web.util;

public class DateUtil {
	/**
	 * 返回当前时间的秒数
	 * 
	 */
	public static int getNowTime() {
		return (int) (System.currentTimeMillis() / 1000);
	}
}

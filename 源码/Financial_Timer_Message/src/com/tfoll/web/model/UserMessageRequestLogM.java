package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_message_request_log", primaryKey = "id")
public class UserMessageRequestLogM extends Model<UserMessageRequestLogM> {
	public static UserMessageRequestLogM dao = new UserMessageRequestLogM();
}

package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_message_request_waiting", primaryKey = "id")
public class UserMessageRequestWaitingM extends Model<UserMessageRequestWaitingM> {
	public static UserMessageRequestWaitingM dao = new UserMessageRequestWaitingM();
}

package com.tfoll.web.timer.message_engine;

import com.tfoll.web.common.message.CCPRestSmsUtil;

/**
 * 邮箱模版定制
 * 
 */
public class MessageEngineTemplate {
	/**
	 * <pre>
	 * 定义规则 
	 * 模版 [1] 贷款驳回
	 * </pre>
	 */

	/**
	 * 模版 [1] 贷款驳回
	 */
	public static boolean send_loans_rejected_message(String phone, String[] code) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6701", phone, code);
	}

	/**
	 * 模版 [2] 绑定手机
	 */
	public static boolean send_binding_phone_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("8887", phone, content);
	}

	/**
	 * 模版 [3] 修改手机
	 */
	public static boolean send_update_phone_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("8886", phone, content);
	}

	/**
	 * 模版 [4]提现成功提交
	 */
	public static boolean send_recharge_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6668", phone, content);
	}

	/**
	 * 模版 [5] 设置提现密码
	 */
	public static boolean send_binding_withdraw_password_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6973", phone, content);
	}

	/**
	 * 模版 [6] 找回提现密码
	 */
	public static boolean send_back_withdraw_password_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("8885", phone, content);
	}

	/**
	 * 模版 [7] 提现驳回（不需要）
	 */
	public static boolean send_withdraw_back_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("", phone, content);
	}

	/**
	 * 模版 [8] 修改提现密码发送提示短信
	 */
	public static boolean send_update_withdraw_password_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6669", phone, content);
	}

	/**
	 * 模版 [9] 人民币提取发送提示消息
	 */
	public static boolean send_rmb_withdraw_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6668", phone, content);
	}

	/**
	 * 模版 [10] 人民币借款审核通过发送短信
	 */
	public static boolean send_borrow_success_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("7145", phone, content);
	}

	/**
	 * 模版 [11] 人民币贷款资料驳回
	 */
	public static boolean send_borrow_fail_message(String phone) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("6671", phone, new String[] { "" });
	}

	/**
	 * 模版 [12] 找回登录密码
	 */
	public static boolean send_back_password_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("8530", phone, content);
	}

	/**
	 * 模版 [13] 收到还款
	 */
	public static boolean send_receive_repayment_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("11289", phone, content);
	}

	/**
	 * 模版 [14] 还款成功
	 */
	public static boolean send_reimbursement_success_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("11290", phone, content);
	}

	/**
	 * 模版 [15] 提前还款成功
	 */
	public static boolean send_early_repayment_success_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("11291", phone, content);
	}

	/**
	 * 模版 [16] 严重逾期
	 */
	public static boolean send_seriously_delinquent_message(String phone, String[] content) throws Exception {

		return CCPRestSmsUtil.send_message_by_templateid("11292", phone, content);
	}

	public static void main(String[] args) throws Exception {
		send_loans_rejected_message("13697309712", new String[] { "123" });
	}
}

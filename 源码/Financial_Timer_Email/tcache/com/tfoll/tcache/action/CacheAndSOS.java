package com.tfoll.tcache.action;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

/**
 * 扩展基础servletoutputstream类使流可以被捕获了写。这是通过重写write()方法和输出数据的两个流-
 * 原始流和二次流是用来捕捉写入数据的实现。
 */
public class CacheAndSOS extends ServletOutputStream {
	OutputStream cacheOutputStream = null;
	OutputStream servletOutputStream = null;

	public CacheAndSOS(OutputStream cacheOutputStream, OutputStream servletOutputStream) {
		this.cacheOutputStream = cacheOutputStream;
		this.servletOutputStream = servletOutputStream;
	}

	public void write(int value) throws IOException {
		this.cacheOutputStream.write(value);
		this.servletOutputStream.write(value);
	}

	public void write(byte[] value) throws IOException {
		this.cacheOutputStream.write(value);
		this.servletOutputStream.write(value);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		this.cacheOutputStream.write(b, off, len);
		this.servletOutputStream.write(b, off, len);
	}

	public void flush() throws IOException {
		super.flush();
		this.cacheOutputStream.flush(); // why not?
		this.servletOutputStream.flush();
	}

	public void close() throws IOException {
		super.close();
		this.cacheOutputStream.close();
		this.servletOutputStream.close();
	}

}

package com.tfoll.console.util;

import com.tfoll.trade.activerecord.ActiveRecord;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.plugin.DbcpPlugin;
import com.tfoll.web.common.TfollConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 控制台初始化
 */
public class ConsoleInit {
	public static void initTables() throws Exception {
		UserConfig.logging.info("====================开始配置插件集====================");
		Properties properties = new Properties();
		InputStream in = null;
		in = TfollConfig.class.getResourceAsStream("dbcp.properties");
		try {
			properties.load(in);
		} catch (IOException e) {
			throw new RuntimeException("静态资源文件不存在");
		}
		DbcpPlugin dbcpPlugin = null;
		try {
			dbcpPlugin = new DbcpPlugin(properties);
		} catch (Exception e) {
			e.printStackTrace();

		}

		UserConfig.logging.info("====================成功配置插件集====================");

		ActiveRecord autoTableBindPlugin = new ActiveRecord(DbcpPlugin.getDataSource());
		autoTableBindPlugin.buildAllModelAndTableInfoMapping();
	}

	public boolean isWindows() {
		boolean flag = false;
		if (System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1) {
			flag = true;
		}
		return flag;
	}
}

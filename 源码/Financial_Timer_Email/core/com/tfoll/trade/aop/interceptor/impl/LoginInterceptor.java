package com.tfoll.trade.aop.interceptor.impl;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 一般不要进行使用-我们建议针对不同的用户,进行不同的拦截器处理 用于进行严格安全访问的-需要使用注解忽略
 */
public class LoginInterceptor implements Interceptor {
	/**
	 * 框架绑定的一个登录验证的属性
	 */
	public static String LoginState = "Login";

	public void doIt(ActionExecutor actionExecutor) {
		HttpServletRequest request = ActionContext.getRequest();
		HttpSession session = request.getSession(false);
		if (session != null && session.getAttribute(LoginState) != null) {
			try {
				actionExecutor.invoke();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		} else {
			ActionContext.setRender(new TextRender("禁止访问"));
		}
	}

}

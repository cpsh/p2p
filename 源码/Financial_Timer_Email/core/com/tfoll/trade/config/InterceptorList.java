package com.tfoll.trade.config;

import com.tfoll.trade.aop.Interceptor;

import java.util.ArrayList;
import java.util.List;

public final class InterceptorList {

	private final List<Interceptor> interceptorList = new ArrayList<Interceptor>();

	public InterceptorList add(Interceptor interceptor) {
		if (interceptor != null) {
			this.interceptorList.add(interceptor);
		}
		return this;
	}

	public InterceptorList addAll(List<Interceptor> interceptorList) {
		if (interceptorList != null) {
			this.interceptorList.addAll(interceptorList);
		}
		return this;
	}

	public Interceptor[] getInterceptorArray() {
		Interceptor[] interceptors = interceptorList.toArray(new Interceptor[interceptorList.size()]);
		return interceptors == null ? new Interceptor[0] : interceptors;
	}

	public List<Interceptor> getInterceptorList() {
		return this.interceptorList;
	}
}

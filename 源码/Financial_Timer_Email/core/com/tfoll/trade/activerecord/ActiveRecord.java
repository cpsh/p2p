package com.tfoll.trade.activerecord;

import com.tfoll.trade.activerecord.dialect.Dialect;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.ModelAndTableInfoMapping;
import com.tfoll.trade.activerecord.model.TableBind;
import com.tfoll.trade.activerecord.model.TableInfo;
import com.tfoll.trade.activerecord.model.TableInfoBuilder;
import com.tfoll.trade.core.ClassSearcher;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * 核心类:ActiveRecord核心模块支持 <br/>
 * ActiveRecord不是以一种插件的形式集成在框架中而是框架的一部分
 */

public class ActiveRecord {
	static final Logger logger = Logger.getLogger(ActiveRecord.class);
	static final ModelAndTableInfoMapping modelAndTableInfoMapping = ModelAndTableInfoMapping.getInstance();
	static final List<TableInfo> tableInfoList = new ArrayList<TableInfo>();

	/**
	 * 
	 * @param dataSource
	 *            在构造函数时传入数据源实例
	 */
	public ActiveRecord(DataSource dataSource) {
		if (dataSource == null) {
			throw new RuntimeException("ActiveRecord模块 需要传入一个 DataSource实例");
		}
		DataSourceKit.setDataSource(dataSource);
	}

	/**
	 * 默认是mysql数据库
	 * 
	 * @param dialect
	 * @return
	 */
	public ActiveRecord setDialect(Dialect dialect) {
		if (dialect != null) {
			DataSourceKit.setDialect(dialect);
		}
		return this;
	}

	/**
	 * ----------------------------------------------
	 */
	/**
	 * 不含有主键
	 * 
	 * @param tableName
	 * @param modelClass
	 * @return
	 */
	public ActiveRecord addMapping(String tableName, Class<? extends Model<?>> modelClass) {
		tableInfoList.add(new TableInfo(tableName, modelClass));
		return this;
	}

	/**
	 * 含有主键
	 * 
	 * @param tableName
	 * @param primaryKey
	 *            主键
	 * @param modelClass
	 */
	public ActiveRecord addMapping(String tableName, String primaryKey, Class<? extends Model<?>> modelClass) {
		tableInfoList.add(new TableInfo(tableName, primaryKey, modelClass));
		return this;
	}

	/**
	 * 根据所有表名建立字段对应信息
	 */
	// 选择性设置

	/**
	 * 使用注解进行设置表信息
	 */
	@SuppressWarnings("unchecked")
	public boolean buildAllModelAndTableInfoMapping() {
		try {
			List<Class> modelClassList = ClassSearcher.findSubClassInClasspath(Model.class);

			TableBind tableBind = null;
			for (Class<Model<Model>> modelClass : modelClassList) {
				tableBind = modelClass.getAnnotation(TableBind.class);
				String tableName;
				if (tableBind != null) {

					// 如果含有注解则自动按照注解的形式配置--建议全部由注解配置[明确表名正确与否]
					tableName = tableBind.tableName();
					if (tableName == null || (tableName != null && "".equals(tableName))) {
						logger.debug("Model Class注解上没有表名");
						throw new RuntimeException("Model Class注解上没有表名");
					}
					String primaryKey = tableBind.primaryKey();
					if (primaryKey == null || (primaryKey != null && "".equals(primaryKey))) {
						addMapping(tableName, modelClass);// 默认是ID
						logger.debug("addMapping(表名:" + tableName + ", 主键:id,模型类名:" + modelClass.getName() + ")");
					} else {
						addMapping(tableName, primaryKey, modelClass);
						logger.debug("addMapping(表名:" + tableName + ", 主键:" + tableBind.primaryKey() + ",模型类名:" + modelClass.getName() + ")");
					}

				}
			}
			TableInfoBuilder.buildAllModelAndTableInfoMapping(tableInfoList);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return true;
	}
}

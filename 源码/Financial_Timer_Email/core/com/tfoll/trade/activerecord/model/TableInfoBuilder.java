package com.tfoll.trade.activerecord.model;

import com.tfoll.trade.activerecord.ActiveRecordException;
import com.tfoll.trade.activerecord.DataSourceKit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * ActiveRecord+Model 核心思想类
 * 
 */
public class TableInfoBuilder {

	private static Logger logger = Logger.getLogger(TableInfoBuilder.class);

	/**
	 * 最简单的说法就是根据所有表名建立字段对应信息
	 */
	public static void buildAllModelAndTableInfoMapping(List<TableInfo> tableInfoList) {
		Connection conn = null;
		try {
			conn = DataSourceKit.getConnection();
			ModelAndTableInfoMapping modelAndTableInfoMapping = ModelAndTableInfoMapping.getInstance();
			/**
			 * 暂时就只有表名,主键,所对应的Model类
			 */
			for (TableInfo tableInfo : tableInfoList) {
				logger.info("table info :" + tableInfo.getTableName());
				try {
					tableInfo = buildTableInfo(tableInfo, conn);
					modelAndTableInfoMapping.addModelAndTableInfoMapping(tableInfo.getModelClass(), tableInfo);
				} catch (Exception e) {
					throw new ActiveRecordException("构建模型:" + tableInfo.getModelClass().getName() + "和表:" + tableInfo.getTableName() + "之间映射关系失败" + e);
				}
			}
		} finally {
			DataSourceKit.close(conn);
		}

	}

	/**
	 * 
	 * @param tableInfo
	 *            暂时只有表名 主键 模型类<br/>
	 * @return 含有其他字段的TableInfo实例
	 */
	private static TableInfo buildTableInfo(TableInfo tableInfo, Connection conn) throws SQLException {
		logger.debug("buildTableInfo-Table:" + tableInfo.getTableName() + "	Modle:" + tableInfo.getModelClass().getName());

		/**
		 * 利用select * from tableName查出该表所有的字段信息<br/>
		 */
		String sql = DataSourceKit.getDialect().buildSqlForTableInfo(tableInfo.getTableName());
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		if (columnCount == 0) {
			throw new IllegalArgumentException("表" + tableInfo.getTableName() + "没有字段信息");
		}
		for (int i = 1; i <= columnCount; i++) {
			String columnName = rsmd.getColumnName(i);
			// ~1直接通过数据库ResultSetMetaData.getColumnClassName来查找字段类型
			String columnClassName = rsmd.getColumnClassName(i);
			// 原来是用[if-else]结构,效率很高.但为了好看,故使用[continue]结构

			/**
			 * 取值的顺序是根据天富网具体业务构建
			 */
			if ("java.lang.String".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.String.class);
				continue;
			}
			if ("java.lang.Long".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.Long.class);
				continue;
			}
			if ("java.lang.Integer".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.Integer.class);
				continue;
			}

			if ("java.math.BigDecimal".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.math.BigDecimal.class);
				continue;
			}
			if ("java.sql.Date".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.sql.Date.class);
				continue;
			}
			if ("java.math.BigInteger".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.math.BigInteger.class);
				continue;
			}

			if ("java.lang.Boolean".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.Boolean.class);
				continue;
			}
			if ("[B".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, byte[].class);
				continue;
			}

			if ("java.lang.Float".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.Float.class);
				continue;
			}
			if ("java.lang.Double".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.lang.Double.class);
				continue;
			}

			if ("java.sql.Time".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.sql.Time.class);
				continue;
			}
			if ("java.sql.Timestamp".equals(columnClassName)) {
				tableInfo.addColumnInfo(columnName, java.sql.Timestamp.class);
				continue;
			} else {// ~2直接通过数据库ResultSetMetaData.getColumnType来查找字段类型
				int type = rsmd.getColumnType(i);
				// byte
				if (type == Types.BLOB) {
					tableInfo.addColumnInfo(columnName, byte[].class);
					// char
				} else if (type == Types.CLOB || type == Types.NCLOB) {
					tableInfo.addColumnInfo(columnName, String.class);
				} else {
					tableInfo.addColumnInfo(columnName, String.class);
				}
			}
		}

		rs.close();
		stmt.close();
		return tableInfo;
	}

}

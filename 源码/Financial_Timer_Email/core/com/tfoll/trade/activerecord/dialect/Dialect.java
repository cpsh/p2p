package com.tfoll.trade.activerecord.dialect;

import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.TableInfo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 仅支持Mysql Oracle数据库.Oracle支持序列,Mysql支持自增
 */
public abstract class Dialect {
	/**
	 * select * from tableName.用于获取数据库所有的字段信息
	 */
	public abstract String buildSqlForTableInfo(String tableName);

	// ForDb or ForModel两种
	public abstract String deleteByIdForDb(String tableName, String primaryKey);

	public abstract String deleteByIdForModel(TableInfo tableInfo);

	public abstract String findByIdForDb(String tableName, String primaryKey, String columns);

	public abstract String findByIdForModel(TableInfo tableInfo, String columns);

	// 获取插入数据后的主键值
	public abstract boolean isSupportAutoIncrementKey();

	public abstract void saveForDb(String tableName, Record record, StringBuilder sql, List<Object> params);

	public abstract void saveForModel(TableInfo tableInfo, Map<String, Object> attributeMap, StringBuilder sql, List<Object> params);

	public abstract void updateForDb(String tableName, String primaryKey, Object id, Record record, StringBuilder sql, List<Object> params);

	public abstract void updateForModel(TableInfo tableInfo, Map<String, Object> attributeMap, Set<String> modifyFlag, String primaryKey, Object id, StringBuilder sql, List<Object> params);

	// 暂时不支持分页

}

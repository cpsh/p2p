package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_info", primaryKey = "id")
public class UserM extends Model<UserM> {
	public static UserM dao = new UserM();
}

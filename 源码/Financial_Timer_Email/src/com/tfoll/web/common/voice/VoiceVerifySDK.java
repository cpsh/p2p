package com.tfoll.web.common.voice;

import com.cloopen.rest.sdk.CCPRestSDK;

import java.util.HashMap;
import java.util.Random;

public class VoiceVerifySDK {
	public static final String AccountSid = "aaf98f8947d761ba0147d792d4dc0076";
	public static final String AccountToken = "0b6873b9798747c0a0b49dd53ad672d3";
	public static final String AppId = "8a48b55147d260a50147d79850090836";
	/**
	 * @param verifyCode
	 *            4-8
	 * @param to
	 * @param displayNum
	 * @return
	 */
	static String[] message_array = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
	static Random message_random = new Random();

	public static String getCode() {
		StringBuilder sb = new StringBuilder(4);
		for (int i = 0; i < 6; i++) {
			try {
				sb.append(message_random.nextInt(9));
			} catch (Exception e) {
				sb.append(7);
			}
		}
		String msg = sb.toString();
		return msg;
	}

	public static boolean voiceVerify(String verifyCode, String to) {
		if (verifyCode == null) {
			throw new RuntimeException("verifyCode");
		}
		if (verifyCode.length() < 6 || verifyCode.length() > 8) {
			throw new RuntimeException("verifyCode <4 or verifyCode >8");
		}
		HashMap<String, Object> result = null;
		CCPRestSDK restAPI = new CCPRestSDK();
		// sandboxapp.cloopen.com
		// app.cloopen.com
		restAPI.init("app.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(AccountSid, AccountToken);// 初始化主帐号名称和主帐号令牌
		restAPI.setAppId(AppId);// 初始化应用ID
		// 验证码内容，为数字和英文字母，不区分大小写，长度4-8位
		// 未上线的应用只能呼叫测试号码
		result = restAPI.voiceVerify(verifyCode, to, "", "", "");
		if ("000000".equals(result.get("statusCode"))) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		// voiceVerify("1234","13697309712");
	}

}

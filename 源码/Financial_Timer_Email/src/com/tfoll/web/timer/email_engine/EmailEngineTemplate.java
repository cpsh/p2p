package com.tfoll.web.timer.email_engine;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

/**
 * 邮箱模版定制
 * 
 */
public class EmailEngineTemplate {
	/**
	 * <pre>
	 * 定义规则 
	 * 模版	[1]	对应用户注册，邮箱发送验证码
	 * 模版	[2]	对应用户找回密码，邮箱发送验证码
	 * </pre>
	 */

	private static final String HostName = "smtp.exmail.qq.com";
	private static final int Port = 25;
	private static final String UserName = "webmaster@tfoll.com";
	private static final String PassWord = "U154jhwiu78989DYWQ98D1";

	private static final String __ROOT_PATH__ = "http://tf-api.oss-cn-hangzhou.aliyuncs.com";

	/**
	 * 模版 [1] 对应用户注册，邮箱发送验证码
	 */
	public static boolean send_emial_rgister_code(String nickname, String email, String code) throws Exception {

		if (email == null) {
			throw new NullPointerException("emial is null");
		}
		if (nickname == null) {
			throw new NullPointerException("nickname is null");
		}
		if (code == null) {
			throw new NullPointerException("code is null");
		}

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融注册成功");

		String imgUrl = __ROOT_PATH__ + "/images/logo.png";

		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{font-size:25px; border-bottom:2px solid #0ca0c4;}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				"  <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>亲爱的用户，" + nickname + "，您好：<br /><br />" + //
				"感谢您注册联富金融，联富金融使用邮箱号作为系统交易，修改信息的凭据，请妥善保管这个帐号，您的验证码为" + code + "，谢谢您的支持。<br />" + //
				"我们收集您的个人资料，是为您提供更优质的服务。我们不会把您的个人资料告知任何其他机构或个人。我们会确保所收集到个人资料的安全和机密。您将个人财务资料委托给我们，我们会尽一切力量维护您对我们的信任。" + //
				"如有任何疑问，请联系联富金融客服，或者访问联富金融<a href=\"http://www.lfoll.com\">www.lfoll.com</a>与我们取得联系。<br/>Lfoll运营团队</h2>" + //
				"  <h3>系统发信，请勿回复<br />" + //
				"客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 找回密码发送邮箱验证码 2
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_find_password_email(String email, String code) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融找回登录密码");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户,您好:<br /><br />" + "您在联富金融网站申请找回密码操作的验证码为" + code + "为保障您的帐号安全性,请妥善保管验证码<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 提现驳回 3
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_user_withdraw_back_email(String email, String nickname) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融提现驳回提示");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{ border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				"  <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户：" + nickname + "您好:<br /><br />" + "您在联富金融网站的人民币提现申请已经被驳回，具体原因请联系客服，感谢您的支持!<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 提现成功 7
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_user_withdraw_success_email(String email, String nickname) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融提现申请提示");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{ border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				"  <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户：" + nickname + "您好:<br /><br />" + "您在联富金融网站的人民币提现申请已经被通过，感谢您对联富金融的支持!<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 贷款驳回 4
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_borrow_back_email(String email, String nickname) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融贷款驳回提示");

		// HttpServletRequest request = ActionContext.getRequest();
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{ border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				"  <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户：" + nickname + "您好:<br /><br />" + "您在联富金融网站的人民币借款申请已经被驳回，具体原因请联系客服，感谢您的支持!<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 贷款审核通过 5
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_borrow_success_email(String email, String nickname) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融借贷提示");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{ border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				"  <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户：" + nickname + "您好:<br /><br />" + "您在联富金融网站的人民币借款申请已经审核通过，请查看筹款进度获得资金，感谢您的支持!<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 绑定邮箱邮箱验证码 6
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_bingdingemail(String nickname, String email, String code) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("联富金融邮箱绑定");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户," + nickname + ",您好:<br /><br />" + "您在联富金融网站绑定邮箱操作的验证码为" + code + "为保障您的帐号安全性,请妥善保管验证码<br />" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 回款成功 8
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_outstanding_success_email(String nickname1, String nickname2, String email, String borrow_title, String date, String principal_interest, String qishu, String late_fee) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("您收到来自" + nickname2 + "的一笔还款");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>"
				+ //
				"  <h2>尊敬的用户" + nickname1 + "：您好:<br /><br />" + "感谢您对用户" + nickname2 + "发布的借款" + borrow_title + "的支持与帮助。</br>您于" + date + "收到来自该用于的一笔还款。正常本息￥" + principal_interest + "(共" + qishu + ")。" + "逾期费用￥" + late_fee + "。</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />"
				+ "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 还款成功 9
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_reimbursement_success_email(String email, String date, String principal_interest, String qishu, String late_fee) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("还款成功");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
				+ //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
				+ //
				"<head>"
				+ //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
				+ //
				"<title></title>"
				+ //
				"<style>"
				+ //
				"*{margin:0; padding:0;}"
				+ //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd"
				+ //
				"body{color:#444;font-size:12px}"
				+ //
				"li{list-style-type:none;}"
				+ //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}"
				+ //
				"img{ border:0px;}"
				+ //
				"body{ font-family:\"微软雅黑\"}"
				+ //
				".youjian{ width:660px; margin:0 auto}"
				+ // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}"
				+ //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}"
				+ //
				".youjian a{ color:#00bce9;}"
				+ //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}"
				+ //
				"</style>"
				+ //
				"</head>"
				+ //
				"<body>"
				+ //
				"<div class=\"youjian\">"
				+ //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>"
				+ //
				"  <h2>尊敬的用户：您好:<br /><br />" + "您于" + date + "有一笔借款已经还款成功，正常本息￥" + principal_interest + "(共" + qishu + "),逾期费用￥" + late_fee + "</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />"
				+ "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 提前还款成功 10
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_early_repayment_of_success_message(String email, String date, String principal_interest, String qishu, String late_fee) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("提前还款成功");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
				+ //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
				+ //
				"<head>"
				+ //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
				+ //
				"<title></title>"
				+ //
				"<style>"
				+ //
				"*{margin:0; padding:0;}"
				+ //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd"
				+ //
				"body{color:#444;font-size:12px}"
				+ //
				"li{list-style-type:none;}"
				+ //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}"
				+ //
				"img{ border:0px;}"
				+ //
				"body{ font-family:\"微软雅黑\"}"
				+ //
				".youjian{ width:660px; margin:0 auto}"
				+ // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}"
				+ //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}"
				+ //
				".youjian a{ color:#00bce9;}"
				+ //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}"
				+ //
				"</style>"
				+ //
				"</head>"
				+ //
				"<body>"
				+ //
				"<div class=\"youjian\">"
				+ //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>"
				+ //
				"  <h2>尊敬的用户：您好:<br /><br />" + "您于" + date + "还款￥" + principal_interest + "(共（18/18）)，已成功还清本期借款感谢您对我们的关注与支持</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />"
				+ "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}

	/**
	 * 严重逾期通知 11
	 * 
	 * @param email
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public static boolean send_seriously_overdue_notice_email(String email, String date) throws Exception {

		// /jtfoll/WebRoot/img/tfoll_ok.jpg
		if (email == null) {
			throw new NullPointerException("emial is null");
		}

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet","true");
		 * props.setProperty("ProxyHost","10.242.175.127");
		 * props.setProperty("ProxyPort","3128"); Authenticator.setDefault(new
		 * Authenticator() { protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HttpClient httpClient = new HttpClient();
		httpClient.getHostConfiguration().setProxy("10.242.175.127", 3128);
		Credentials credentials = new UsernamePasswordCredentials("1841395636288587_default_31", "rjcyq5b38n");
		httpClient.getState().setProxyCredentials(AuthScope.ANY, credentials);
		httpClient.getParams().setAuthenticationPreemptive(true);

		/*
		 * Properties props = System.getProperties();
		 * props.setProperty("proxySet", "true");
		 * props.setProperty("http.proxyHost", "10.242.175.127");
		 * props.setProperty("http.proxyPort", "3128");
		 * Authenticator.setDefault(new Authenticator() { protected
		 * PasswordAuthentication getPasswordAuthentication() { return new
		 * PasswordAuthentication("1841395636288587_default_31", new
		 * String("rjcyq5b38n").toCharArray()); } });
		 */

		HtmlEmail htmlEmail = new HtmlEmail();
		htmlEmail.setHostName(HostName);
		htmlEmail.setSmtpPort(Port);
		htmlEmail.setAuthenticator(new DefaultAuthenticator(UserName, PassWord));
		htmlEmail.setSSLOnConnect(true);
		htmlEmail.setCharset("gbk");

		htmlEmail.setFrom(UserName, "联富金融");
		htmlEmail.addTo(email);
		htmlEmail.setSubject("严重逾期通知");

		// HttpServletRequest request = ActionContext.getRequest();
		// String path = request.getContextPath();
		// String __ROOT_PATH__ = request.getScheme() + "://" +
		// request.getServerName() + ":" + request.getServerPort() + path;
		// String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
		String imgUrl = __ROOT_PATH__ + "/images/logo.png";
		// http://tf-api.oss-cn-hangzhou.aliyuncs.com/img/tfoll_ok.jpg
		String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" + //
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" + //
				"<head>" + //
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" + //
				"<title></title>" + //
				"<style>" + //
				"*{margin:0; padding:0;}" + //
				"body,ul,ol,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div,dl,dt,dd" + //
				"body{color:#444;font-size:12px}" + //
				"li{list-style-type:none;}" + //
				"h1,h2,h3,h4,h5,h6 { font-weight:normal;}" + //
				"img{ border:0px;}" + //
				"body{ font-family:\"微软雅黑\"}" + //
				".youjian{ width:660px; margin:0 auto}" + // 
				".youjian h1{align:center; border-bottom:2px solid #0ca0c4;font-size:25px}" + //
				".youjian h2{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8; padding:25px 0 25px 0;}" + //
				".youjian a{ color:#00bce9;}" + //
				".youjian h3{ font-size:14px; line-height:24px; border-bottom:2px solid #e8e8e8;padding:25px 0 25px 0;}" + //
				"</style>" + //
				"</head>" + //
				"<body>" + //
				"<div class=\"youjian\">" + //
				" <h1><img src=\"" + imgUrl + "\" />&nbsp;&nbsp;联富金融邮件验证</h1>" + //
				"  <h2>尊敬的用户：您好:<br /><br />" + "您的借款于" + date + "已经严重逾期，请尽快还款，否则将承担法律责任</br>点击<a href=\"http://www.lfoll.com/user/usercenter/into_user_center.html\">这里</a>查看您的账户余额。</br>感谢您对我们的关注与支持。</br>" + "Lfoll运营团队</h2>" + "  <h3>系统发信，请勿回复<br />" + "客服热线：4006-888-923  客服QQ：4006888923<br/>此邮件为系统自动发送，请勿回复!<br/><a href=\"http://www.lfoll.com\">www.lfoll.com</a> &copy; Copyright 2014 </h3>" + //
				"</div>" + //
				"</body>" + //
				"</html>";//
		htmlEmail.setHtmlMsg(html);
		htmlEmail.setContent(html, "text/html;charset=gb2312");
		htmlEmail.send();
		return true;
	}
}

package com.tfoll.web.aop.ajax;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;

import javax.servlet.http.HttpSession;

public class AdminAjax implements Interceptor {

	public void doIt(ActionExecutor actionInvocation) {
		HttpSession session = ActionContext.getRequest().getSession();
		if (session != null && session.getAttribute("admin") != null) {
			actionInvocation.invoke();
		} else {

			ActionContext.setRender(new TextRender("admin is not logined"));
		}
	}

}

package com.tfoll.web.aop.ajax;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.aop.Interceptor;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.render.TextRender;

import javax.servlet.http.HttpSession;

/**
 * 判读普通用户是否登录
 * 
 */
public class UserLoginedAjax implements Interceptor {

	public void doIt(ActionExecutor actionInvocation) {
		HttpSession session = ActionContext.getRequest().getSession();
		if (session != null && session.getAttribute("user") != null) {
			actionInvocation.invoke();
		} else {

			ActionContext.setRender(new TextRender("user_is_not_logined"));
		}

	}

}

package com.tfoll.web.action.index;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.MD5;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.AdminM;
import com.tfoll.web.model.SysStatsM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.filter.AdminLoginFilter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ActionKey("/index")
public class IndexAction extends Controller {

	/**
	 * 普通用户登录
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "进入首页", last_update_author = "hx")
	public void index() {
		renderText("");
		return;
	}

	/**
	 * 管理员登录
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "管理员登录导航", last_update_author = "hx")
	public void $my_admin_login_index() {
		setSessionAttribute(AdminLoginFilter.isLogined, "1");
		renderJsp("/index/bak_login.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "所有人", function_description = "管理员登录", last_update_author = "hx")
	public void $my_admin_login() {
		/**
		 * 记录管理员登录Ip
		 */
		HttpServletRequest request = ActionContext.getRequest();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String code = request.getParameter("code");
		if (Utils.isNullOrEmptyString(username)) {
			setAttribute("tips", "用户名为空！");
			renderJsp("/index/bak_login.jsp");
			return;
		}
		if (Utils.isNullOrEmptyString(password)) {

			setAttribute("tips", "密码为空！");
			renderJsp("/index/bak_login.jsp");
			return;
		}
		if (Utils.isNullOrEmptyString(code)) {

			setAttribute("tips", "验证码为空！");
			renderJsp("/index/bak_login.jsp");
			return;
		}
		if (!"@@123".equals(code)) {
			request.setAttribute("tips", "验证码错误!");
			renderJsp("/index/bak_login.jsp");
			return;
		}
		password = MD5.md5(password);// 1-9502d5d403ed69ebfffbd7a3e66a77e3
		AdminM admin = (AdminM) AdminM.dao.findFirst("select * from admin where login_pwd=? and login_account=?", new Object[] { password, username });
		if (admin == null) {
			request.setAttribute("tips", "用户名和密码不正确");

			renderJsp("/index/bak_login.jsp");
			return;
		} else {
			HttpSession session = getSession();
			session.setAttribute("admin", admin);
			// 获取角色ID
			int role_id = admin.getInt("role_id");
			getLoginPowerModule(role_id);
			// 控制具有不同权限的管理员
			renderJsp("/admin/index/index_main.jsp");
			return;
		}

	}

	/**
	 * 用户权限模块控制方法
	 * 
	 * @param role_id
	 */

	private void getLoginPowerModule(int role_id) {
		HttpServletRequest request = ActionContext.getRequest();
		String path = request.getContextPath();
		String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;

		// 查询当前角色的父级模块权限
		String SQL = "SELECT t2.* FROM admin_role_ralation_module t1, admin_module t2 WHERE t1.role_id = ? AND t1.module_id = t2.id AND t2.parent_module_id = 0 ORDER BY t2.parent_module_id, t2.id ASC";
		List<Record> parent_module_list = Db.find(SQL, new Object[] { role_id });

		if (parent_module_list == null) {
			parent_module_list = new ArrayList<Record>();
		}
		setAttribute("parentModuleList", parent_module_list);

		// 查询当前角色的二级模块的权限
		String SQL2 = "SELECT t2.* FROM admin_role_ralation_module t1, admin_module t2 WHERE t1.role_id = ? AND t1.module_id = t2.id AND t2.parent_module_id !=0 ORDER BY t2.parent_module_id, t2.id ASC";
		List<Record> secondModuleList = Db.find(SQL2, new Object[] { role_id });
		if (secondModuleList == null) {
			secondModuleList = new ArrayList<Record>();
		}

		if (parent_module_list != null && parent_module_list.size() > 0) {
			StringBuffer buffer1 = new StringBuffer("[");
			StringBuffer buffer = new StringBuffer();
			for (Record r1 : parent_module_list) {
				int module1_id = r1.getInt("id");
				buffer.append("{id:'" + module1_id + "',menu:[{text:'" + r1.getString("module_name") + "',items:[");

				String str2 = "";
				for (int i = 0; i < secondModuleList.size(); i++) {
					Record r2 = secondModuleList.get(i);
					int module2_id = r2.getInt("id");
					int module2_parent_id = r2.getInt("parent_module_id");

					if (module1_id == module2_parent_id) {
						str2 = str2 + "{id:'" + module2_id + "',text:'" + r2.getString("module_name") + "',href:'" + __ROOT_PATH__ + "" + r2.getString("module_url") + "'},";
					}

				}
				if (!str2.equals("")) {
					String str3 = str2.substring(0, str2.length() - 1);
					buffer.append(str3);
				}

				buffer.append("]}]  },");
			}
			String str4 = buffer.toString();
			String str5 = str4.substring(0, str4.length() - 1);
			buffer1.append(str5);
			buffer1.append("]");

			String lastStr = buffer1.toString();
			setAttribute("str", lastStr);
		} else {
			request.setAttribute("tips", "你没有任何权限!");
			admin_ip();// 记录管理员登陆的IP
			renderJsp("/index/bak.jsp");
			return;
		}

	}

	/**
	 * 管理员用户退出登录
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "管理员", function_description = "退出登录进入登陆页", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void admin_quit() {
		ActionContext.getHttpSession().invalidate();
		ActionContext.getHttpSession().setAttribute(AdminLoginFilter.isLogined, "1");
		setSessionAttribute(AdminLoginFilter.isLogined, "1");
		renderJsp("/index/bak_login.jsp");
		return;
	}

	/**
	 * 记录管理用户每次登录的ip和登录时间
	 * 
	 */
	public void admin_ip() {
		HttpServletRequest request = ActionContext.getRequest();
		final AdminM admin = (AdminM) getSession().getAttribute("admin");
		int admin_id = 0;
		try {
			admin_id = admin.getInt("admin_id");
		} catch (Exception e) {

		}
		// String last_ip = request.getRemoteAddr();
		// 获取当然用户的IP地址
		String last_ip = request.getHeader("x-forwarded-for");
		if (last_ip == null || last_ip.length() == 0 || "unknown".equalsIgnoreCase(last_ip)) {
			last_ip = request.getHeader("Proxy-Client-IP");
		}
		if (last_ip == null || last_ip.length() == 0 || "unknown".equalsIgnoreCase(last_ip)) {
			last_ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (last_ip == null || last_ip.length() == 0 || "unknown".equalsIgnoreCase(last_ip)) {
			last_ip = request.getRemoteAddr();
		}

		// 更新管理员最后登录的ip和时间
		AdminM adm = new AdminM();
		adm.set("admin_id", admin_id).set("last_ip", last_ip).set("last_login", new Date()).update();
		// 把管理员登录的信息写入sys_stats表
		SysStatsM sys_stats = new SysStatsM();
		sys_stats.set("admin_id", admin_id).set("ip", last_ip).save();
	}

}

package com.tfoll.web.action.index;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

@ActionKey("/exportExcel")
public class ExportExcelAction extends Controller {

	/**
	 * 获取充值记录
	 * 
	 * @param request
	 * @return
	 */
	private List<Record> get_user_recharge_record(HttpServletRequest request) {
		String real_name = getParameter("real_name");
		String user_identity = getParameter("user_identity");
		String start_time = getParameter("start_time");
		String end_time = getParameter("end_time");

		String count_sql = "SELECT count(1) FROM user_recharge_log t1, user_info t2 WHERE t1.user_id = t2.id";
		String list_sql = "SELECT ifnull(t2.real_name,'') AS real_name, ifnull(t2.user_identity,'') AS user_identity, t1.amount AS amount, t1.actual_amount AS actual_amount, t1.add_time AS add_time, t1.type AS type FROM user_recharge_log t1, user_info t2 WHERE t1.user_id = t2.id";
		StringBuffer buffer_count = new StringBuffer(count_sql);
		StringBuffer buffer_list = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_count.append(" AND t2.real_name like ? ");
			buffer_list.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			buffer_count.append(" AND t2.user_identity like ? ");
			buffer_list.append(" AND t2.user_identity like ? ");
			params.add(user_identity + "%");

		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			buffer_count.append(" AND t1.add_time >= ? ");
			buffer_list.append(" AND t1.add_time >= ? ");
			params.add(start_time);

		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			buffer_count.append(" AND t1.add_time <= ? ");
			buffer_list.append(" AND t1.add_time <= ? ");
			params.add(end_time);
		}
		buffer_count.append(" ORDER BY t1.add_time DESC");
		buffer_list.append(" ORDER BY t1.add_time DESC");

		Long total_rows = Db.queryLong(buffer_count.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			List<Record> user_recharge_log_list = Db.find(buffer_list.toString(), params.toArray());
			return user_recharge_log_list;
		} else {
			return null;
		}

	}

	/**
	 * 获取用户提现记录
	 * 
	 * @param request
	 * @return
	 */
	private List<Record> get_user_withdraw_record(HttpServletRequest request) {
		String real_name = getParameter("real_name");
		String user_identity = getParameter("user_identity");
		String start_time = getParameter("start_time");
		String end_time = getParameter("end_time");

		String count_sql = "SELECT count(1) FROM user_withdraw_cash_log t1, user_info t2 WHERE t1.user_id = t2.id";
		String list_sql = "SELECT ifnull(t2.real_name,'') AS real_name, ifnull(t2.user_identity,'') AS user_identity, t1.account_holder AS account_holder, t1.bank_name AS bank_name, t1.bank_address AS bank_address, t1.bank_card_num AS bank_card_num, t1.amount AS amount, t1.fee AS fee,t1.add_time as add_time,t1.pay_time AS pay_time FROM user_withdraw_cash_log t1, user_info t2 WHERE t1.user_id = t2.id AND t1.is_through = 3 AND t1.is_pay = 2";
		StringBuffer buffer_count = new StringBuffer(count_sql);
		StringBuffer buffer_list = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			buffer_count.append(" AND t2.real_name like ? ");
			buffer_list.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			buffer_count.append(" AND t2.user_identity like ? ");
			buffer_list.append(" AND t2.user_identity like ? ");
			params.add(user_identity + "%");

		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			buffer_count.append(" AND t1.pay_time >= ? ");
			buffer_list.append(" AND t1.pay_time >= ? ");
			params.add(start_time);

		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			buffer_count.append(" AND t1.pay_time <= ? ");
			buffer_list.append(" AND t1.pay_time <= ? ");
			params.add(end_time);
		}
		buffer_count.append(" ORDER BY t1.add_time DESC");
		buffer_list.append(" ORDER BY t1.add_time DESC");

		Long total_rows = Db.queryLong(buffer_count.toString(), params.toArray());
		if (total_rows != null && total_rows > 0) {
			List<Record> user_withdraw_cash_log_list = Db.find(buffer_list.toString(), params.toArray());
			return user_withdraw_cash_log_list;
		} else {
			return null;
		}

	}

	/**
	 * 获取财务收入记录
	 * 
	 * @param request
	 * @return
	 */
	private List<Record> get_financial_income_record(HttpServletRequest request) {
		String start_time = getParameter("start_time");
		String end_time = getParameter("end_time");
		String real_name = getParameter("real_name");
		String count_sql = "SELECT COUNT(1) FROM sys_income_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		String list_sql = "SELECT IFNULL(t2.real_name,'')  AS real_name ,t1.money as money,t1.detail as detail,t1.add_time as add_time FROM sys_income_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		StringBuffer count_buffer = new StringBuffer(count_sql);
		StringBuffer list_buffer = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_buffer.append(" AND t2.real_name like ? ");
			list_buffer.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			count_buffer.append(" AND t1.add_time >= ? ");
			list_buffer.append(" AND t1.add_time >= ? ");
			params.add(start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			count_buffer.append(" AND t1.add_time < ? ");
			list_buffer.append(" AND t1.add_time < ? ");
			params.add(end_time);
		}
		count_buffer.append(" ORDER BY t1.id DESC");
		list_buffer.append(" ORDER BY t1.id DESC");

		Long total_rows = Db.queryLong(count_buffer.toString(), params.toArray());

		if (total_rows != null && total_rows > 0) {
			List<Record> sys_income_record_list = Db.find(list_buffer.toString(), params.toArray());
			for (Record record : sys_income_record_list) {
				BigDecimal decimal_money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("money", decimal_money);
			}
			return sys_income_record_list;
		} else {
			return null;
		}
	}

	/**
	 * 获取财务支出记录
	 * 
	 * @param request
	 * @return
	 */
	private List<Record> get_financial_expenses_record(HttpServletRequest request) {
		String start_time = getParameter("start_time");
		String end_time = getParameter("end_time");
		String real_name = getParameter("real_name");
		String count_sql = "SELECT COUNT(1) FROM sys_expenses_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		String list_sql = "SELECT ifnull(t2.real_name,'') as real_name ,t2.nickname AS nickname,t1.money as money,t1.detail as detail,t1.add_time as add_time FROM sys_expenses_records t1, user_info t2 WHERE t1.user_id = t2.id ";
		StringBuffer count_buffer = new StringBuffer(count_sql);
		StringBuffer list_buffer = new StringBuffer(list_sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_buffer.append(" AND t2.real_name like ? ");
			list_buffer.append(" AND t2.real_name like ? ");
			params.add(real_name + "%");
		}
		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			count_buffer.append(" AND t1.add_time >= ? ");
			list_buffer.append(" AND t1.add_time >= ? ");
			params.add(start_time);
		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			count_buffer.append(" AND t1.add_time < ? ");
			list_buffer.append(" AND t1.add_time < ? ");
			params.add(end_time);
		}
		count_buffer.append(" ORDER BY t1.id DESC");
		list_buffer.append(" ORDER BY t1.id DESC");

		Long total_rows = Db.queryLong(count_buffer.toString(), params.toArray());

		if (total_rows != null && total_rows > 0) {
			List<Record> sys_income_record_list = Db.find(list_buffer.toString(), params.toArray());
			for (Record record : sys_income_record_list) {
				BigDecimal decimal_money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN);
				record.add("money", decimal_money);
			}
			return sys_income_record_list;
		} else {
			return null;
		}

	}

	/**
	 * 导出用户充值记录
	 * 
	 * @param request
	 * @return
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "导出用户充值记录EXCEL", last_update_author = "hx")
	public void export_charge_record_excel() {
		HttpServletRequest request = getRequest();
		HttpServletResponse response = getResponse();
		// 获取RMB充值记录
		List<Record> list_record = get_user_recharge_record(request);
		if (list_record != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List<Map> listmsg = new ArrayList<Map>();
			for (Record record : list_record) {
				Map<String, Object> map = new HashMap<String, Object>();
				String real_name = record.getString("real_name");
				String user_identity = record.getString("user_identity");
				String amount_str = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN).toString();
				String actual_amount_str = record.getBigDecimal("actual_amount").setScale(2, BigDecimal.ROUND_DOWN).toString();
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = sdf.format(add_time);
				int type = record.getInt("type");
				String type_str = "";
				if (type == 1) {
					type_str = "正常充值";
				} else {
					type_str = "其他奖励";
				}
				map.put("real_name", real_name);
				map.put("user_identity", user_identity);
				map.put("amount_str", amount_str);
				map.put("actual_amount_str", actual_amount_str);
				map.put("add_time_str", add_time_str);
				map.put("type_str", type_str);
				listmsg.add(map);
			}
			List<Map<String, Object>> excel_title_list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_title_1 = new HashMap<String, Object>();
			map_title_1.put("real_name", "用户姓名");
			Map<String, Object> map_title_2 = new HashMap<String, Object>();
			map_title_2.put("user_identity", "身份证号");
			Map<String, Object> map_title_3 = new HashMap<String, Object>();
			map_title_3.put("amount_str", "充值金额");
			Map<String, Object> map_title_4 = new HashMap<String, Object>();
			map_title_4.put("actual_amount_str", "实际充值金额");
			Map<String, Object> map_title_5 = new HashMap<String, Object>();
			map_title_5.put("add_time_str", "充值时间");
			Map<String, Object> map_title_6 = new HashMap<String, Object>();
			map_title_6.put("type_str", "充值类型");
			excel_title_list.add(map_title_1);
			excel_title_list.add(map_title_2);
			excel_title_list.add(map_title_3);
			excel_title_list.add(map_title_4);
			excel_title_list.add(map_title_5);
			excel_title_list.add(map_title_6);
			dooutof(request, response, listmsg, excel_title_list, "充值记录");
			return;
		} else {
			renderText("没有数据，不能导出");
		}

	}

	/**
	 * 导出用户提现记录
	 * 
	 * @return
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "导出用户提现记录EXCEL", last_update_author = "hx")
	public void export_user_withdraw_record_page() {
		HttpServletRequest request = getRequest();
		HttpServletResponse response = getResponse();
		// 获取RMB提现记录
		List<Record> list_record = get_user_withdraw_record(request);
		if (list_record != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List<Map> listmsg = new ArrayList<Map>();
			for (Record record : list_record) {
				Map<String, Object> map = new HashMap<String, Object>();
				String account_holder = record.getString("account_holder");
				String user_identity = record.getString("user_identity");
				String bank_name = record.getString("bank_name");
				String bank_address = record.getString("bank_address");
				String bank_card_num = record.getString("bank_card_num");
				String amount_str = record.getBigDecimal("amount").setScale(2, BigDecimal.ROUND_DOWN).toString();
				String fee_str = record.getBigDecimal("fee").setScale(2, BigDecimal.ROUND_DOWN).toString();
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = sdf.format(add_time);
				map.put("account_holder", account_holder);
				map.put("user_identity", user_identity);
				map.put("bank_name", bank_name);
				map.put("bank_address", bank_address);
				map.put("bank_card_num", bank_card_num);
				map.put("amount_str", amount_str);
				map.put("fee_str", fee_str);
				map.put("add_time_str", add_time_str);
				listmsg.add(map);
			}
			List<Map<String, Object>> excel_title_list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_title_1 = new HashMap<String, Object>();
			map_title_1.put("account_holder", "开户人");
			Map<String, Object> map_title_2 = new HashMap<String, Object>();
			map_title_2.put("user_identity", "身份证号");
			Map<String, Object> map_title_3 = new HashMap<String, Object>();
			map_title_3.put("bank_name", "银行名称");
			Map<String, Object> map_title_4 = new HashMap<String, Object>();
			map_title_4.put("bank_card_num", "开户地址");
			Map<String, Object> map_title_5 = new HashMap<String, Object>();
			map_title_5.put("amount_str", "提现金额");
			Map<String, Object> map_title_6 = new HashMap<String, Object>();
			map_title_6.put("fee_str", "手续费");
			Map<String, Object> map_title_7 = new HashMap<String, Object>();
			map_title_7.put("add_time_str", "申请提现时间");
			excel_title_list.add(map_title_1);
			excel_title_list.add(map_title_2);
			excel_title_list.add(map_title_3);
			excel_title_list.add(map_title_4);
			excel_title_list.add(map_title_5);
			excel_title_list.add(map_title_6);
			excel_title_list.add(map_title_7);
			dooutof(request, response, listmsg, excel_title_list, "提现记录");
			return;
		} else {
			renderText("没有数据，不能导出");
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "导出财务收入到EXCEL", last_update_author = "hx")
	public void export_financial_income_to_excel() {
		HttpServletRequest request = getRequest();
		HttpServletResponse response = getResponse();
		// 获取财务收入记录
		List<Record> list_record = get_financial_income_record(request);
		if (list_record != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List<Map> listmsg = new ArrayList<Map>();
			for (Record record : list_record) {
				Map<String, Object> map = new HashMap<String, Object>();

				String real_name = record.getString("real_name");
				String money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN).toString();
				String detail = record.getString("detail");
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = sdf.format(add_time);

				map.put("real_name", real_name);
				map.put("money", money);
				map.put("detail", detail);
				map.put("add_time_str", add_time_str);

				listmsg.add(map);
			}
			List<Map<String, Object>> excel_title_list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_title_1 = new HashMap<String, Object>();
			map_title_1.put("real_name", "用户姓名");
			Map<String, Object> map_title_2 = new HashMap<String, Object>();
			map_title_2.put("money", "收入金额");
			Map<String, Object> map_title_3 = new HashMap<String, Object>();
			map_title_3.put("detail", "明细");
			Map<String, Object> map_title_4 = new HashMap<String, Object>();
			map_title_4.put("add_time_str", "时间");

			excel_title_list.add(map_title_1);
			excel_title_list.add(map_title_2);
			excel_title_list.add(map_title_3);
			excel_title_list.add(map_title_4);

			dooutof(request, response, listmsg, excel_title_list, "财务收入记录");
			return;
		} else {
			renderText("无数据不能导出Excel");
		}

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "导出财务支出到EXCEL", last_update_author = "hx")
	public void export_financial_expenses_to_excel() {
		HttpServletRequest request = getRequest();
		HttpServletResponse response = getResponse();
		// 获取财务收入记录
		List<Record> list_record = get_financial_expenses_record(request);
		if (list_record != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List<Map> listmsg = new ArrayList<Map>();
			for (Record record : list_record) {
				Map<String, Object> map = new HashMap<String, Object>();
				String real_name = record.getString("real_name");
				String nickname = record.getString("nickname");

				String money = record.getBigDecimal("money").setScale(2, BigDecimal.ROUND_DOWN).toString();
				String detail = record.getString("detail");
				Date add_time = record.getTimestamp("add_time");
				String add_time_str = sdf.format(add_time);

				map.put("real_name", real_name);
				map.put("nickname", nickname);
				map.put("money", money);
				map.put("detail", detail);
				map.put("add_time_str", add_time_str);

				listmsg.add(map);
			}
			List<Map<String, Object>> excel_title_list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map_title_1 = new HashMap<String, Object>();
			map_title_1.put("real_name", "用户姓名");
			Map<String, Object> map_title_2 = new HashMap<String, Object>();
			map_title_2.put("nickname", "昵称");
			Map<String, Object> map_title_3 = new HashMap<String, Object>();
			map_title_3.put("money", "支出金额");
			Map<String, Object> map_title_4 = new HashMap<String, Object>();
			map_title_4.put("detail", "明细");
			Map<String, Object> map_title_5 = new HashMap<String, Object>();
			map_title_5.put("add_time_str", "时间");

			excel_title_list.add(map_title_1);
			excel_title_list.add(map_title_2);
			excel_title_list.add(map_title_3);
			excel_title_list.add(map_title_4);
			excel_title_list.add(map_title_5);

			dooutof(request, response, listmsg, excel_title_list, "财务收入记录");
			return;
		} else {
			renderText("暂无数据不能导出Excel");
		}

	}

	/**
	 * 公共方法导出EXECEL
	 * 
	 * @param request
	 * @param response
	 * @param listmsg
	 * @param excel_title_list
	 */
	@SuppressWarnings("deprecation")
	public void dooutof(HttpServletRequest request, HttpServletResponse response, List<Map> listmsg, List<Map<String, Object>> excel_title_list, String excel_name) {

		// 文件名称与路径
		String realPath = request.getSession().getServletContext().getRealPath("");
		// System.out.println("---realPath--"+realPath);
		String fileName = excel_name + ".xls";
		File file = new File(realPath + "//excel//tempPath//");// 导出文件存放的位置
		if (!file.exists()) {
			file.mkdirs();
		}
		realPath = realPath + "//excel//tempPath//" + fileName;

		// 建立工作薄并写表头
		try {
			WritableWorkbook wwb = Workbook.createWorkbook(new File(realPath));
			WritableSheet ws = wwb.createSheet("Sheet1", 0);// 建立工作簿

			// 写表头
			for (int i = 0; i < excel_title_list.size(); i++) {
				Set<String> title_set = excel_title_list.get(i).keySet();
				// Excel表格表头
				String excel_title_key = title_set.iterator().next();
				// System.out.println("================excel_title_key 表头==========>>>:"+excel_title_key);
				Label label = new Label(i, 0, excel_title_list.get(i).get(excel_title_key).toString());
				ws.setColumnView(i, 25);
				// 放入工作簿
				ws.addCell(label);
			}

			// 写入信息
			for (int j = 0; j < excel_title_list.size(); j++) {
				for (int i = 0; i < listmsg.size(); i++) {
					String excel_title_key = excel_title_list.get(j).keySet().iterator().next();
					// System.out.println("----------------excel_title_key------------"+excel_title_key);
					Label label = new Label(j, i + 1, listmsg.get(i).get(excel_title_key).toString());//
					ws.addCell(label);
				}
			}

			// 写入Exel工作表
			wwb.write();
			// 关闭Excel工作薄对象
			wwb.close();

			File file_2 = new File(realPath);

			renderFile(file_2);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}// 此处建立路径

	}
}

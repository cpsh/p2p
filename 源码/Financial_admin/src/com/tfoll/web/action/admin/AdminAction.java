package com.tfoll.web.action.admin;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.MD5;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.aop.ajax.AdminAjax;
import com.tfoll.web.model.AdminDeptM;
import com.tfoll.web.model.AdminM;
import com.tfoll.web.model.AdminModuleM;
import com.tfoll.web.model.AdminRoleM;
import com.tfoll.web.model.AdminRoleRalationModuleM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;
import com.tfoll.web.util.page.PageDiv;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ActionKey("/admin/admin")
public class AdminAction extends Controller {
	/**
	 * 查询部门列表
	 */

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "部门列表", last_update_author = "")
	@Before( { AdminAop.class })
	public void dept_list_index() {
		int pn = getParameterToInt("pn", 1);
		String count_query_sql = "select count(1) from admin_dept t order by t.dept_id desc";
		String query_dept_list_sql = "select * from admin_dept t order by t.dept_id desc ";
		Long totalRow = Db.queryLong(count_query_sql);
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<AdminDeptM> admin_dept_list = AdminDeptM.dao.find(page.creatLimitSql(query_dept_list_sql));
			if (admin_dept_list != null) {
				setAttribute("admin_dept_list", admin_dept_list);
			} else {
				admin_dept_list = new ArrayList<AdminDeptM>();
				setAttribute("admin_dept_list", admin_dept_list);
			}

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/admin/admin/dept_list_index");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());

			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂时无数据!");
		}
		renderJsp("/admin/dept/dept_manage.jsp");// 不同页面进行组合
		return;

	}

	/**
	 * 编辑部门详细信息
	 */

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "管理员", function_description = "部门详细信息表", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void dept_detail_info() {
		int dept_id = getParameterToInt("dept_id");

		AdminDeptM admin_dept = AdminDeptM.dao.findById(dept_id);
		if (admin_dept == null) {
			admin_dept = new AdminDeptM();
		}
		setAttribute("admin_dept", admin_dept);
		String sql = "select * from admin_dept t where t.parent_dept_id is null";
		List<AdminDeptM> all_parent_dept_list = AdminDeptM.dao.find(sql);
		setAttribute("all_parent_dept_list", all_parent_dept_list);

		renderJsp("/admin/dept/dept_edit.jsp");// 不同页面进行组合
		return;

	}

	/**
	 * 跳转到部门信息 添加页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void goto_add_dept_detail_page() {
		String sql = "select * from admin_dept t where t.parent_dept_id is null";
		List<AdminDeptM> all_parent_dept_list = AdminDeptM.dao.find(sql);
		setAttribute("all_parent_dept_list", all_parent_dept_list);
		renderJsp("/admin/dept/dept_edit_add.jsp");
		return;
	}

	/**
	 * 部门信息 保存
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void dept_detail_save_or_update() {
		int dept_id = getParameterToInt("dept_id", 0);

		String dept_no = getParameter("dept_no");
		String dept_name = getParameter("dept_name");
		Integer parent_dept_id = getParameterToInt("parent_dept_id", null);
		String dept_desc = getParameter("dept_desc");
		int is_effective = getParameterToInt("status");

		// save
		if (dept_id == 0) {
			AdminDeptM admin_dept = new AdminDeptM();
			boolean is_ok = admin_dept.set("dept_no", dept_no).set("dept_name", dept_name).set("parent_dept_id", parent_dept_id).set("dept_desc", dept_desc).set("is_effective", is_effective).save();
			if (is_ok) {
				renderAction("/admin/admin/dept_list_index");
			} else {
				renderAction("/admin/admin/goto_add_dept_detail_page");
			}
			// update
		} else {
			AdminDeptM admin_dept = AdminDeptM.dao.findById(dept_id);
			boolean is_ok = admin_dept.set("dept_no", dept_no).set("dept_name", dept_name).set("parent_dept_id", parent_dept_id).set("dept_desc", dept_desc).set("is_effective", is_effective).update();
			if (is_ok) {
				renderAction("/admin/admin/dept_list_index");
			} else {
				renderAction("/admin/admin/dept_list_index");
			}
		}
		return;
	}

	/**
	 * 查询部门信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void query_dept_info() {
		String dept_name = getParameter("dept_name");
		String sql = "select * from admin_dept ss where ss.dept_name like ?";
		List<AdminDeptM> admin_dept_list = AdminDeptM.dao.find(sql, new Object[] { dept_name + "%" });
		if (admin_dept_list == null || admin_dept_list.size() < 0) {
			admin_dept_list = new ArrayList<AdminDeptM>();
		}
		setAttribute("admin_dept_list", admin_dept_list);
		renderJsp("/admin/dept/dept_manage.jsp");// 不同页面进行组合
		return;

	}

	/**
	 * 跳转到 角色信息管理
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void role_list_index() {
		int pn = getParameterToInt("pn", 1);
		String count = "select count(1) from admin_role order by id desc";
		String sql = "select * from admin_role order by id desc";

		Long totalRow = Db.queryLong(count);
		if (totalRow != null && totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<AdminRoleM> admin_role_list = AdminRoleM.dao.find(page.creatLimitSql(sql));
			if (admin_role_list == null) {
				admin_role_list = new ArrayList<AdminRoleM>();
			}
			setAttribute("admin_role_list", admin_role_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/admin/admin/role_list_index");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		} else {
			setAttribute("tips", "暂无数据!");
		}

		renderJsp("/admin/role/role_manage.jsp");
		return;

	}

	/**
	 * 查询角色相关信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void query_role_info() {
		String role_name = getParameter("role_name");
		String sql = "select * from admin_role where role_name like ?";
		List<AdminRoleM> admin_role_list = AdminRoleM.dao.find(sql, role_name + "%");
		if (admin_role_list == null || admin_role_list.size() <= 0) {
			admin_role_list = new ArrayList<AdminRoleM>();
		}
		setAttribute("admin_role_list", admin_role_list);
		renderJsp("/admin/role/role_manage.jsp");
		return;

	}

	/**
	 * 跳转到 添加角色信息页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void goto_add_role_detail_page() {

		// 查询所有的 有效的 父级模块
		List<AdminModuleM> all_parent_module_list = AdminModuleM.dao.find("SELECT * FROM admin_module t WHERE t.parent_module_id = 0 and is_effective = 1");

		// 查询所有的二级模块
		List<AdminModuleM> all_second_module_list = AdminModuleM.dao.find("SELECT * FROM admin_module s WHERE s.parent_module_id IN ( SELECT t.id AS id FROM admin_module t WHERE t.parent_module_id = 0 ) ORDER BY s.parent_module_id, s.id ASC");

		String str_parent = "";

		StringBuffer buffer = new StringBuffer("[");
		for (AdminModuleM admin_module : all_parent_module_list) {
			int module_id = admin_module.getInt("id");
			String module_name = admin_module.getString("module_name");

			str_parent = "{ id:" + module_id + ", pId:0, name:\"" + module_name + "\", open:false},";
			buffer.append(str_parent);

			/********* 子级模块 拼接字符串 *********/
			String str_child = "";

			for (AdminModuleM child_module : all_second_module_list) {
				int child_module_id = child_module.getInt("id");// 子级模块ID
				String child_module_name = child_module.getString("module_name");// 子级模块名称
				int parent_module_id = child_module.getInt("parent_module_id");// 父级模块ID
				// 如果当前父级模块的ID == 次模块的父级模块ID
				if (module_id == parent_module_id) {
					str_child = "{ id:" + child_module_id + ", pId:" + module_id + ", name:\"" + child_module_name + "\", open:true},";
					buffer.append(str_child);

				}
			}
		}
		String str_1 = buffer.toString();
		String str_2 = str_1.substring(0, str_1.length() - 1);
		str_2 = str_2 + "]";

		setAttribute("str_2", str_2);

		renderJsp("/admin/role/role_edit_add.jsp");
		return;

	}

	/**
	 * 保存或更更新角色
	 */
	@SuppressWarnings( { "static-access", "unused" })
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAjax.class })
	public void save_or_update_role() {
		int id = getParameterToInt("role_id", 0);

		String role_name = getParameter("role_name", null);
		String role_desc = getParameter("role_desc", null);
		int is_effective = getParameterToInt("status", 0);

		Integer[] all_id = getParameterValuesToInt("arr[]");// 获取所有 ID的集合

		if (Utils.isNullOrEmptyString(role_name)) {
			renderText("0"); // 角色名称为空
			return;
		}

		if (Utils.isNullOrEmptyString(role_desc)) {
			renderText("1"); // 描述为空
			return;
		}

		if (all_id == null) {
			renderText("2"); // 请选择权限模块！
			return;
		}

		boolean is_save = false;
		// sava
		if (id == 0) {
			AdminRoleM admin_role = new AdminRoleM();
			admin_role.set("role_name", role_name).set("role_desc", role_desc).set("is_effective", is_effective).save();
			AdminRoleM adminRoleM_this = admin_role.dao.findFirst("select * from admin_role where role_name = ?", new Object[] { role_name });
			int role_id = adminRoleM_this.getInt("id");

			// 循环查入进 角色模块关系表
			for (int i = 0; i < all_id.length; i++) {
				AdminRoleRalationModuleM admin_role_ralation_module = new AdminRoleRalationModuleM();
				is_save = admin_role_ralation_module.set("role_id", role_id).set("module_id", all_id[i]).save();
			}

			// update
		} else {
			AdminRoleM admin_role = AdminRoleM.dao.findById(id);
			admin_role.set("role_name", role_name).set("role_desc", role_desc).set("is_effective", is_effective).update();

			// 先删除关联表
			String sql = "delete from admin_role_ralation_module where role_id = ?";

			int rows = Db.update(sql, new Object[] { id });

			// 重新插入关联表
			// 循环查入进 角色模块关系表
			if (all_id.length > 0) {
				for (int i = 0; i < all_id.length; i++) {
					AdminRoleRalationModuleM admin_role_ralation_module = new AdminRoleRalationModuleM();
					is_save = admin_role_ralation_module.set("role_id", id).set("module_id", all_id[i]).save();
				}
			}
		}

		if (is_save) {
			renderText("3"); // 成功
			return;
		} else {
			renderText("4"); // 失败
			return;
		}

	}

	/**
	 * 编辑角色
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void edit_role() {
		int role_id = getParameterToInt("role_id");
		AdminRoleM admin_role = AdminRoleM.dao.findById(role_id);
		setAttribute("adminRoleM", admin_role);

		/************* 查询所有有效的模块 ****************/
		List<AdminModuleM> allParent_module_list = AdminModuleM.dao.find("SELECT * FROM admin_module t WHERE t.parent_module_id = 0 ");

		// 查询所有的二级模块
		List<AdminModuleM> allSecond_module_list = AdminModuleM.dao.find("SELECT * FROM admin_module s WHERE s.parent_module_id IN ( SELECT t.id AS id FROM admin_module t WHERE t.parent_module_id = 0 ) ORDER BY s.parent_module_id, s.id ASC");

		List<Record> role_ralation_module_list = Db.find("SELECT tt.module_id as module_id FROM admin_role_ralation_module tt WHERE tt.role_id = ?", new Object[] { role_id });

		List<Integer> module_id_list = new ArrayList<Integer>();

		for (Record r : role_ralation_module_list) {
			module_id_list.add(r.getInt("module_id"));
		}

		String str_parent = "";

		StringBuffer buffer = new StringBuffer("[");
		for (AdminModuleM admin_module : allParent_module_list) {
			int module_id = admin_module.getInt("id");
			String module_name = admin_module.getString("module_name");
			boolean is_ok = false;
			for (int i = 0; i < module_id_list.size(); i++) {
				int module_id_int = module_id_list.get(i);
				if (module_id_int == module_id) {
					is_ok = true;
				}
			}

			if (is_ok) {
				str_parent = "{ id:" + module_id + ", pId:0, name:\"" + module_name + "\", open:false,checked:true},";
			} else {
				str_parent = "{ id:" + module_id + ", pId:0, name:\"" + module_name + "\", open:false},";
			}

			buffer.append(str_parent);

			/********* 子级模块 拼接字符串 *********/
			String str_child = "";

			for (AdminModuleM child_module : allSecond_module_list) {
				int child_module_id = child_module.getInt("id");// 子级模块ID
				String child_module_name = child_module.getString("module_name");// 子级模块名称
				int parent_module_id = child_module.getInt("parent_module_id");// 父级模块ID
				// 如果当前父级模块的ID == 次模块的父级模块ID
				if (module_id == parent_module_id) {
					boolean bool = false;
					for (int i = 0; i < module_id_list.size(); i++) {
						int module_id_int = module_id_list.get(i);
						if (child_module_id == module_id_int) {
							bool = true;
						}

					}
					if (bool) {
						str_child = "{ id:" + child_module_id + ", pId:" + module_id + ", name:\"" + child_module_name + "\", open:true,checked:true},";
					} else {
						str_child = "{ id:" + child_module_id + ", pId:" + module_id + ", name:\"" + child_module_name + "\", open:true},";
					}

					buffer.append(str_child);

				}
			}
		}
		String str_1 = buffer.toString();
		String str_2 = str_1.substring(0, str_1.length() - 1);
		str_2 = str_2 + "]";

		setAttribute("str_2", str_2);

		renderJsp("/admin/role/role_edit_update.jsp");
		return;
	}

	/**
	 * 模块列表信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void module_list_index() {
		int pn = getParameterToInt("pn", 1);
		String countQuery = "select count(1) FROM admin_module t1 LEFT JOIN admin_module t2 ON t1.parent_module_id = t2.id";
		String sql = "select t1.* ,t2.module_name as parent_module_name FROM admin_module t1 LEFT JOIN admin_module t2 ON t1.parent_module_id = t2.id ";

		Long totalRow = Db.queryLong(countQuery);
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> admin_module_record_list = Db.find(page.creatLimitSql(sql));
			if (admin_module_record_list == null) {
				admin_module_record_list = new ArrayList<Record>();
			}
			setAttribute("admin_module_record_list", admin_module_record_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/admin/admin/module_list_index");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		} else {
			setAttribute("tips", "暂无数据!");
		}

		renderJsp("/admin/module/module_manage.jsp");
		return;
	}

	/**
	 * 查询模块信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void query_module_info() {
		String module_name = getParameter("module_name");
		String sql = "select * from admin_module where module_name like  ?";
		List<Record> admin_module_record_list = Db.find(sql, module_name + "%");
		if (!Utils.isHasData(admin_module_record_list)) {
			admin_module_record_list = new ArrayList<Record>();
		}
		setAttribute("admin_module_record_list", admin_module_record_list);
		renderJsp("/admin/module/module_manage.jsp");
		return;
	}

	/**
	 * 模块 详细信息查询
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void module_detail_info() {
		int module_id = getParameterToInt("module_id");
		AdminModuleM admin_module = AdminModuleM.dao.findById(module_id);
		if (admin_module == null) {
			admin_module = new AdminModuleM();
		}
		setAttribute("adminModuleM", admin_module);
		// 查询所有的父级模块 的集合
		String query_parent_module_sql = "select * from admin_module tt where tt.parent_module_id = 0";
		List<AdminModuleM> admin_module_list = AdminModuleM.dao.find(query_parent_module_sql);
		if (admin_module_list == null || admin_module_list.size() < 0) {
			admin_module_list = new ArrayList<AdminModuleM>();
		}
		setAttribute("admin_module_list", admin_module_list);

		renderJsp("/admin/module/module_edit.jsp");
		return;

	}

	/**
	 * 保存 或 更新 模块信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void save_or_update_module_info() {
		int module_id = getParameterToInt("module_id", 0);

		String module_name = getParameter("module_name");
		Integer parent_module_id = getParameterToInt("parent_module_id", 0);

		String module_url = getParameter("module_url");
		String module_desc = getParameter("module_desc");
		int is_effective = getParameterToInt("status");

		// save
		if (module_id == 0) {
			AdminModuleM admin_module = new AdminModuleM();
			admin_module.set("module_name", module_name).set("parent_module_id", parent_module_id).set("module_url", module_url).set("is_effective", is_effective).set("module_desc", module_desc).save();

			// update
		} else {
			AdminModuleM admin_module = AdminModuleM.dao.findById(module_id);
			admin_module.set("module_name", module_name).set("parent_module_id", parent_module_id).set("module_url", module_url).set("module_desc", module_desc).set("is_effective", is_effective).update();

		}

		renderAction("/admin/admin/module_list_index");
		return;

	}

	/**
	 * 跳转到 模块添加页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void goto_add_module_detail_page() {
		// 查询所有的父级模块 的集合
		String query_parent_module = "select * from admin_module tt where tt.parent_module_id = 0 ";
		List<AdminModuleM> admin_module_list = AdminModuleM.dao.find(query_parent_module);
		if (admin_module_list == null || admin_module_list.size() < 0) {
			admin_module_list = new ArrayList<AdminModuleM>();
		}
		setAttribute("admin_module_list", admin_module_list);
		renderJsp("/admin/module/module_edit_add.jsp");
		return;
	}

	/**
	 * 员工列表信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void employee_list_index() {
		String sql = "SELECT t1.emplyee_no as emplyee_no,t1.id AS id, t1.login_account AS login_account, t1.email AS email, t1.real_name AS real_name, t1.is_effective AS is_effective, t2.dept_name AS dept_name, t3.role_name AS role_name FROM admin t1, admin_dept t2, admin_role t3 WHERE t1.dept_id = t2.dept_id AND t1.role_id = t3.id";
		List<Record> admin_record_list = Db.find(sql);

		if (admin_record_list == null || admin_record_list.size() < 0) {
			admin_record_list = new ArrayList<Record>();
		}

		setAttribute("admin_record_list", admin_record_list);
		renderJsp("/admin/employee/employee_manage.jsp");
		return;
	}

	/**
	 * 查询员工信息
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void query_employee_info() {
		String real_name = getParameter("real_name");
		String emplyee_no = getParameter("emplyee_no");
		String sql = "select * from admin where 1 = 1 ";
		StringBuffer buffer = new StringBuffer(sql);
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			params.add(real_name + "%");
			buffer.append(" and real_name like ?");
		}
		if (Utils.isNotNullAndNotEmptyString(emplyee_no)) {
			params.add(emplyee_no + "%");
			buffer.append(" and emplyee_no like ?");
		}
		String querySQL = buffer.toString();
		List<Record> admin_record_list = Db.find(querySQL, params.toArray());
		if (admin_record_list == null || admin_record_list.size() <= 0) {
			admin_record_list = new ArrayList<Record>();
		}
		setAttribute("admin_record_list", admin_record_list);
		renderJsp("/admin/employee/employee_manage.jsp");
		return;

	}

	/**
	 * 跳转到添加员工信息的页面
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void goto_add_employee_detail_page() {
		String select_all_dept = "select * from admin_dept t1 where t1.is_effective = 1";
		List<AdminDeptM> admin_dept_list = AdminDeptM.dao.find(select_all_dept);
		if (admin_dept_list == null || admin_dept_list.size() < 0) {
			admin_dept_list = new ArrayList<AdminDeptM>();
		}
		setAttribute("adminDeptMList", admin_dept_list);
		String select_all_role = "select * from admin_role t1 where t1.is_effective = 1";
		List<AdminRoleM> admin_role_list = AdminRoleM.dao.find(select_all_role);
		if (admin_role_list == null || admin_role_list.size() < 0) {
			admin_role_list = new ArrayList<AdminRoleM>();
		}
		setAttribute("admin_role_list", admin_role_list);
		renderJsp("/admin/employee/employee_edit_add.jsp");
	}

	/**
	 * 员工信息保存 或 更新
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void employee_detail_save_or_update() {
		int id = getParameterToInt("id", 0);
		String login_account = getParameter("login_account");
		String emplyee_no = getParameter("emplyee_no");
		String login_pwd = getParameter("login_pwd");
		String operation_code = getParameter("operation_code");
		String real_name = getParameter("real_name");
		String email = getParameter("email");
		Integer dept_id = getParameterToInt("dept_id", null);
		Integer role_id = getParameterToInt("role_id", null);
		int is_effective = getParameterToInt("status");

		// save
		if (id == 0) {
			AdminM admin = new AdminM();
			admin.set("login_account", login_account).set("emplyee_no", emplyee_no).set("login_pwd", MD5.md5(login_pwd)).set("real_name", real_name).set("dept_id", dept_id).set("role_id", role_id).set("is_effective", is_effective).set("add_time", new Date()).set("email", email).set("last_login_time", new Date()).save();

			// update
		} else {
			AdminM admin = AdminM.dao.findById(id);
			admin.set("login_account", login_account).set("emplyee_no", emplyee_no).set("login_pwd", MD5.md5(login_pwd)).set("operation_code", MD5.md5(operation_code)).set("real_name", real_name).set("dept_id", dept_id).set("role_id", role_id).set("is_effective", is_effective).set("add_time", new Date()).set("email", email).set("last_login_time", new Date()).update();
		}

		renderAction("/admin/admin/employee_list_index");
	}

	/**
	 * 员工明细
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void employee_detail_info() {
		int admin_id = getParameterToInt("admin_id", 0);

		AdminM admin = AdminM.dao.findById(admin_id);
		setAttribute("adminM", admin);
		// 查询所有的部门
		String select_all_dept = "select * from admin_dept";
		List<AdminDeptM> admin_dept_list = AdminDeptM.dao.find(select_all_dept);
		if (admin_dept_list == null || admin_dept_list.size() < 0) {
			admin_dept_list = new ArrayList<AdminDeptM>();
		}
		setAttribute("adminDeptMList", admin_dept_list);

		// 查询所有的角色
		String select_all_role = "select * from admin_role ";
		List<AdminRoleM> admin_role_list = AdminRoleM.dao.find(select_all_role);
		if (admin_role_list == null || admin_role_list.size() < 0) {
			admin_role_list = new ArrayList<AdminRoleM>();
		}
		setAttribute("admin_role_list", admin_role_list);
		renderJsp("/admin/employee/employee_edit.jsp");
		return;
	}

	/**
	 * 删除模块
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void delete_module_detail_info() {
		int module_id = getParameterToInt("module_id");
		AdminModuleM admin_module = AdminModuleM.dao.findById(module_id);
		admin_module.delete();
		renderAction("/admin/admin/module_list_index");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void gotoTest() {

		renderJsp("/admin/dept/test_manage.jsp");
		return;

	}

	/**
	 * 获取参数
	 */
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "", last_update_author = "hx")
	@Before( { AdminAop.class })
	public void testGetParams() {

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "获取数据库所有表名", last_update_author = "lh")
	@Before( { AdminAop.class })
	public void get_tables_name_index() {

		int pn = getParameterToInt("pn", 1);

		String sql_count = "SELECT count(1) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='tfoll_financial'";
		String sql_select = "SELECT TABLE_NAME,TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='tfoll_financial'";

		Long totalRow = Db.queryLong(sql_count.toString());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> table_name_list = Db.find(page.creatLimitSql(sql_select.toString()), new Object[] {});

			if (Utils.isHasData(table_name_list)) {
				setAttribute("table_name_list", table_name_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/admin/admin/get_tables_name_index");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/admin/admin/table_name_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Function(for_people = "", function_description = "获取数据库 某张表的信息", last_update_author = "lh")
	@Before( { AdminAop.class })
	public void get_some_table_info() {

		int pn = getParameterToInt("pn", 1);

		String table_name = getParameter("table_name");
		if (table_name == null) {
			table_name = getSessionAttribute("table_name");
		}
		setSessionAttribute("table_name", table_name);

		String sql_table_content_select = "select * from " + table_name;
		String sql_table_content_count = "select count(1) from " + table_name;
		// 获取表的所有列名
		// List<String> column_name_list =
		// Db.qureyColumnName(sql_table_content,new Object[]{});
		List<String> column_name_list = new ArrayList<String>();
		List<Record> record_list = new ArrayList<Record>();

		Long totalRow = Db.queryLong(sql_table_content_count.toString());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			record_list = Db.find(page.creatLimitSql(sql_table_content_select.toString()), new Object[] {});
			if (Utils.isHasData(record_list)) {
				setAttribute("record_list", record_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/admin/admin/get_some_table_info");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		if (record_list.size() > 0) {
			Map<String, Object> map = record_list.get(0).getR();
			for (String column_name : map.keySet()) {
				column_name_list.add(column_name);
			}
			setAttribute("column_name_list", column_name_list);
		}
		renderJsp("/admin/admin/table_detail.jsp");
		return;

	}

}

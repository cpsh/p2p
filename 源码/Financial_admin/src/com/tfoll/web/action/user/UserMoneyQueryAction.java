package com.tfoll.web.action.user;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.common.SystemConstantKey;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyUpdateM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ActionKey("/user/user_money_query")
public class UserMoneyQueryAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "用户基本信息", last_update_author = "zjb")
	public void user_money_query_index() {
		setSessionAttribute("real_name", null);
		setSessionAttribute("nickname", null);
		setSessionAttribute("email", null);
		setSessionAttribute("user_identity", null);
		setSessionAttribute("is_active", null);

		renderJsp("/user/user_money_query/user_now_money.jsp");// 
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "用户基本信息", last_update_author = "zjb")
	public void user_money_query_query() {

		String real_name = getParameter("real_name");
		String nick_name = getParameter("nickname");
		String email = getParameter("email");
		String user_identity = getParameter("user_identity");
		String is_active = getParameter("is_active", "-1");
		String phone = getParameter("phone");
		String id = getParameter("id");

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("nick_name", nick_name);
		setSessionAttribute("email", email);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("is_active", is_active);
		setSessionAttribute("phone", phone);
		setSessionAttribute("id", id);

		setAttribute("real_name", real_name);
		setAttribute("nick_name", nick_name);
		setAttribute("email", email);
		setAttribute("user_identity", user_identity);
		setAttribute("is_active", is_active);
		setAttribute("phone", phone);
		setAttribute("id", id);

		StringBuilder count_sql = new StringBuilder("select count(1) from user_info t1 ,user_now_money t2 where 1=1 and  t1.id = t2.user_id  ");
		StringBuilder select_sql = new StringBuilder("select * from user_info t1 ,user_now_money t2 where 1=1 and t1.id = t2.user_id  ");

		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			count_sql.append(" and t1.user_identity = ? ");
			select_sql.append(" and t1.user_identity = ?");
			params.add(user_identity);
		}
		if (Utils.isNotNullAndNotEmptyString(email)) {
			count_sql.append(" and t1.email = ? ");
			select_sql.append(" and t1.email = ? ");
			params.add(email);
		}
		if (Utils.isNotNullAndNotEmptyString(id)) {
			count_sql.append(" and t1.id = ? ");
			select_sql.append(" and t1.id = ? ");
			params.add(Integer.valueOf(id));
		}
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_sql.append(" and t1.real_name = ? ");
			select_sql.append(" and t1.real_name = ? ");
			params.add(real_name);
		}
		if (Utils.isNotNullAndNotEmptyString(phone)) {
			count_sql.append(" and t1.phone = ? ");
			select_sql.append(" and t1.phone = ? ");
			params.add(phone);
		}
		if (Utils.isNotNullAndNotEmptyString(nick_name)) {
			count_sql.append(" and t1.nickname = ?");
			select_sql.append(" and t1.nickname=?");
			params.add(nick_name);
		}
		if (Utils.isNotNullAndNotEmptyString(is_active) && !is_active.equals("-1")) {
			count_sql.append(" and t1.is_active = ?");
			select_sql.append(" and t1.is_active = ?");
			params.add(Integer.parseInt(is_active));// 数据库里面存放的数字
		}

		Long totalRow = Db.queryLong(count_sql.toString(), params.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, 1, 10);

			List<Record> user_info_list = Db.find(page.creatLimitSql(select_sql.toString()), params.toArray());
			setAttribute("user_info_list", user_info_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_money_query/user_money_query_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		}
		renderJsp("/user/user_money_query/user_now_money.jsp");// 
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "用户基本信息", last_update_author = "zjb")
	public void user_money_query_page() {
		int pn = getParameterToInt("pn", 1);

		String real_name = getSessionAttribute("real_name");
		String nick_name = getSessionAttribute("nickname");
		String email = getSessionAttribute("email");
		String user_identity = getSessionAttribute("user_identity");
		String is_active = getSessionAttribute("is_active");
		String phone = getSessionAttribute("phone");
		String id = getSessionAttribute("id");

		setAttribute("real_name", real_name);
		setAttribute("nick_name", nick_name);
		setAttribute("email", email);
		setAttribute("user_identity", user_identity);
		setAttribute("is_active", is_active);
		setAttribute("phone", phone);
		setAttribute("id", id);

		StringBuilder count_sql = new StringBuilder("select count(1) from user_info t1 ,user_now_money t2 where 1=1 and  t1.id = t2.user_id  ");
		StringBuilder select_sql = new StringBuilder("select * from user_info t1 ,user_now_money t2 where 1=1 and t1.id = t2.user_id  ");

		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			count_sql.append(" and t1.user_identity = ? ");
			select_sql.append(" and t1.user_identity = ?");
			params.add(user_identity);
		}
		if (Utils.isNotNullAndNotEmptyString(email)) {
			count_sql.append(" and t1.email = ? ");
			select_sql.append(" and t1.email = ? ");
			params.add(email);
		}
		if (Utils.isNotNullAndNotEmptyString(id)) {
			count_sql.append(" and t1.id = ? ");
			select_sql.append(" and t1.id = ? ");
			params.add(Integer.valueOf(id));
		}
		if (Utils.isNotNullAndNotEmptyString(phone)) {
			count_sql.append(" and t1.phone = ? ");
			select_sql.append(" and t1.phone = ? ");
			params.add(phone);
		}
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			count_sql.append(" and t1.real_name = ? ");
			select_sql.append(" and t1.real_name = ? ");
			params.add(real_name);
		}
		if (Utils.isNotNullAndNotEmptyString(nick_name)) {
			count_sql.append("and t1.nickname = ?");
			select_sql.append("and t1.nickname=?");
			params.add(nick_name);
		}

		Long totalRow = Db.queryLong(count_sql.toString(), params.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn, 10);

			List<Record> user_info_list = Db.find(page.creatLimitSql(select_sql.toString()), params.toArray());
			setAttribute("user_info_list", user_info_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_money_query/user_money_query_page");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);

		}
		renderJsp("/user/user_money_query/user_now_money.jsp");// 
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "需要登录", function_description = "冻结资产", last_update_author = "向旋")
	public void frozen_account() {
		String user_id = getParameter("user_id");
		UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
		int status = user_now_money.getInt("status");
		if (status == 1) {
			user_now_money.set("status", 0).update();
			setSessionAttribute(SystemConstantKey.User_Now_Money, user_now_money);
			renderAction("/user/user_money_query/user_money_query_query");
			return;
		}
		if (status == 0) {
			user_now_money.set("status", 1).update();
			setSessionAttribute(SystemConstantKey.User_Now_Money, user_now_money);
			renderAction("/user/user_money_query/user_money_query_query");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "需要登录", function_description = "禁止登陆", last_update_author = "向旋")
	public void ban() {
		String id = getParameter("id");
		UserM user = UserM.dao.findById(id);
		int is_active = user.getInt("is_active");
		if (is_active == 1) {
			user.set("is_active", 0).update();
			setSessionAttribute(SystemConstantKey.User, user);
			renderAction("/user/user_money_query/user_money_query_query");
			return;
		}
		if (is_active == 0) {
			user.set("is_active", 1).update();
			setSessionAttribute(SystemConstantKey.User, user);
			renderAction("/user/user_money_query/user_money_query_query");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "需要登录", function_description = "到修改用户资产页面", last_update_author = "向旋")
	public void to_update_money_page() {
		String id = getParameter("id");
		Record user_and_money = Db.findFirst("select * from user_info t1,user_now_money t2 where t1.id=? and t1.id = t2.user_id", new Object[] { id });
		BigDecimal cny_can_used = user_and_money.getBigDecimal("cny_can_used");
		cny_can_used = cny_can_used.setScale(4, BigDecimal.ROUND_DOWN);

		BigDecimal cny_freeze = user_and_money.getBigDecimal("cny_freeze");
		cny_freeze = cny_freeze.setScale(4, BigDecimal.ROUND_DOWN);

		user_and_money.add("cny_can_used", cny_can_used);
		user_and_money.add("cny_freeze", cny_freeze);
		setSessionAttribute("user_and_money", user_and_money);
		renderJsp("/user/user_money_query/update_user_now_money.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "管理员", function_description = "修改用户资产", last_update_author = "向旋")
	public void update_user_money() {
		String user_identity = getParameter("user_identity");
		final String cny_can_used = getParameter("cny_can_used");
		if (!Utils.isNotNullAndNotEmptyString(cny_can_used)) {
			renderText("2");
			return;
		}
		final BigDecimal cny_can_used1 = new BigDecimal(cny_can_used);
		final String cny_freeze = getParameter("cny_freeze");
		if (!Utils.isNotNullAndNotEmptyString(cny_freeze)) {
			renderText("3");
			return;
		}
		final BigDecimal cny_freeze1 = new BigDecimal(cny_freeze);
		UserM user = UserM.dao.findFirst("select * from user_info where user_identity = ? limit 1", user_identity);
		final int id = user.getInt("id");
		final UserNowMoneyM user_now_money = UserNowMoneyM.dao.findFirst("select * from user_now_money where user_id = ?", id);
		final BigDecimal cny_can_used_session = user_now_money.getBigDecimal("cny_can_used");
		final BigDecimal cny_freeze_session = user_now_money.getBigDecimal("cny_freeze");
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				user_now_money.set("cny_can_used", cny_can_used).set("cny_freeze", cny_freeze).update();
				UserMoneyUpdateM user_money_update = new UserMoneyUpdateM();
				boolean update_ok = user_money_update.set("user_id", id).set("old_cny_can_used", cny_can_used_session).set("old_cny_freeze", cny_freeze_session).set("new_cny_can_used", cny_can_used1).set("new_cny_freeze", cny_freeze1).set("cny_can_used_differential", cny_can_used1.subtract(cny_can_used_session)).set("cny_freeze_differential", cny_freeze1.subtract(cny_freeze_session)).set(
						"add_time", new Date()).save();
				if (!update_ok) {
					return false;
				}
				return true;
			}
		});
		if (!is_ok) {
			renderText("4");
			return;
		}
		setSessionAttribute("user_now_money", user_now_money);
		renderText("5");
		return;

	}

}

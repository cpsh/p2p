package com.tfoll.web.action.user.loan_order;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.AdminM;
import com.tfoll.web.model.BorrowerBulkStandardApplyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderWaitingM;
import com.tfoll.web.model.BorrowerBulkStandardOrderUserInfoM;
import com.tfoll.web.model.SysNotificationM;
import com.tfoll.web.model.UserAuthenticateAssetsInfoM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ActionKey("/user/loan_order")
public class LoanOrderAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入借款申请列表", last_update_author = "lh")
	public void loan_order_list_index() {

		setSessionAttribute("real_name", null);
		setSessionAttribute("borrow_duration", -1);
		setSessionAttribute("borrow_duration_day", -1);
		setSessionAttribute("state", 3);
		setSessionAttribute("commit_start_time", null);
		setSessionAttribute("commit_end_time", null);
		setSessionAttribute("phone", null);
		setSessionAttribute("user_id", null);

		setAttribute("real_name", null);
		setAttribute("borrow_duration", -1);
		setAttribute("borrow_duration_day", -1);
		setAttribute("state", 3);
		setAttribute("commit_start_time", null);
		setAttribute("commit_end_time", null);
		setAttribute("phone", null);
		setAttribute("user_id", null);

		renderJsp("/user/loan_order/applay_order/loan_order_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "借款申请列表查询", last_update_author = "lh")
	public void loan_order_list_query() {

		String real_name = getParameter("real_name");
		int borrow_duration = getParameterToInt("borrow_duration", -1);
		int borrow_duration_day = getParameterToInt("borrow_duration_day", -1);
		int state = getParameterToInt("state", 3);
		String commit_start_time = getParameter("commit_start_time", null);
		String commit_end_time = getParameter("commit_end_time", null);
		String phone = getParameter("phone", null);
		String user_id = getParameter("user_id", null);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("borrow_duration_day", borrow_duration_day);
		setSessionAttribute("state", state);
		setSessionAttribute("commit_start_time", commit_start_time);
		setSessionAttribute("commit_end_time", commit_end_time);
		setSessionAttribute("phone", phone);
		setSessionAttribute("user_id", user_id);

		setAttribute("real_name", real_name);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("borrow_duration_day", borrow_duration_day);
		setAttribute("state", state);
		setAttribute("commit_start_time", commit_start_time);
		setAttribute("commit_end_time", commit_end_time);
		setAttribute("phone", phone);
		setAttribute("user_id", user_id);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("select a.*,b.real_name,b.phone,b.user_identity from borrower_bulk_standard_apply_order a , user_info b where 1=1 and a.user_id = b.id ");
		sql_count.append("select count(1) from borrower_bulk_standard_apply_order a , user_info b where 1=1 and a.user_id = b.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and b.real_name = ? ");
			sql_count.append(" and b.real_name = ? ");
			params.add(real_name);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and a.borrow_duration = ? ");
			sql_count.append(" and a.borrow_duration = ? ");
			params.add(borrow_duration);
		}
		if (borrow_duration_day != -1) {
			sql_select.append(" and a.borrow_duration_day = ? ");
			sql_count.append(" and a.borrow_duration_day = ? ");
			params.add(borrow_duration_day);
		}
		if (Utils.isNotNullAndNotEmptyString(phone)) {
			sql_select.append(" and b.phone = ? ");
			sql_count.append(" and b.phone = ? ");
			params.add(phone);
		}
		if (Utils.isNotNullAndNotEmptyString(user_id)) {
			sql_select.append(" and b.id = ? ");
			sql_count.append(" and b.id = ? ");
			params.add(user_id);
		}
		if (Utils.isNotNullAndNotEmptyString(commit_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(commit_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.commit_time > ? ");
			sql_count.append(" and a.commit_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(commit_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(commit_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.commit_time < ? ");
			sql_count.append(" and a.commit_time < ? ");
			params.add(date);
		}
		if (1 == 1) {
			sql_select.append(" and a.state = ? ");
			sql_count.append(" and a.state = ? ");
			params.add(state);
		}

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> apply_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(apply_order_list)) {
				setAttribute("apply_order_list", apply_order_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/loan_order_list_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/applay_order/loan_order_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "借款申请列表查询", last_update_author = "lh")
	public void loan_order_list_page() {

		int pn = getParameterToInt("pn", 1);

		String real_name = getParameter("real_name");
		int borrow_duration = getParameterToInt("borrow_duration", -1);
		int borrow_duration_day = getParameterToInt("borrow_duration_day", -1);
		int state = getParameterToInt("state", -1);
		if (state == -1) {
			state = getSessionAttribute("state");
		}
		String commit_start_time = getParameter("commit_start_time", null);
		String commit_end_time = getParameter("commit_end_time", null);
		String phone = getParameter("phone", null);
		String user_id = getParameter("user_id", null);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("borrow_duration", borrow_duration);
		setSessionAttribute("borrow_duration_day", borrow_duration_day);
		setSessionAttribute("state", state);
		setSessionAttribute("commit_start_time", commit_start_time);
		setSessionAttribute("commit_end_time", commit_end_time);
		setSessionAttribute("phone", phone);
		setSessionAttribute("user_id", user_id);

		setAttribute("real_name", real_name);
		setAttribute("borrow_duration", borrow_duration);
		setAttribute("borrow_duration_day", borrow_duration_day);
		setAttribute("state", state);
		setAttribute("commit_start_time", commit_start_time);
		setAttribute("commit_end_time", commit_end_time);
		setAttribute("phone", phone);
		setAttribute("user_id", user_id);

		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql_select.append("select a.*,b.real_name,b.phone,b.user_identity from borrower_bulk_standard_apply_order a , user_info b where 1=1 and a.user_id = b.id ");
		sql_count.append("select count(1) from borrower_bulk_standard_apply_order a , user_info b where 1=1 and a.user_id = b.id ");
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_select.append(" and b.real_name = ? ");
			sql_count.append(" and b.real_name = ? ");
			params.add(real_name);
		}
		if (borrow_duration != -1) {
			sql_select.append(" and a.borrow_duration = ? ");
			sql_count.append(" and a.borrow_duration = ? ");
			params.add(borrow_duration);
		}
		if (borrow_duration_day != -1) {
			sql_select.append(" and a.borrow_duration_day = ? ");
			sql_count.append(" and a.borrow_duration_day = ? ");
			params.add(borrow_duration_day);
		}
		if (Utils.isNotNullAndNotEmptyString(phone)) {
			sql_select.append(" and b.phone = ? ");
			sql_count.append(" and b.phone = ? ");
			params.add(phone);
		}
		if (Utils.isNotNullAndNotEmptyString(user_id)) {
			sql_select.append(" and b.id = ? ");
			sql_count.append(" and b.id = ? ");
			params.add(user_id);
		}
		if (Utils.isNotNullAndNotEmptyString(commit_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(commit_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.commit_time > ? ");
			sql_count.append(" and a.commit_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(commit_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(commit_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.commit_time < ? ");
			sql_count.append(" and a.commit_time < ? ");
			params.add(date);
		}
		if (1 == 1) {
			sql_select.append(" and a.state = ? ");
			sql_count.append(" and a.state = ? ");
			params.add(state);
		}

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> apply_order_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(apply_order_list)) {
				setAttribute("apply_order_list", apply_order_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/loan_order_list_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/applay_order/loan_order_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "进入修改用户借款金额页面", last_update_author = "lh")
	public void to_modify_borrow_money() {
		int borrow_order_id = getParameterToInt("borrow_order_id");
		String sql = "select a.id as borrow_order_id, a.user_id,b.real_name,b.nickname,b.phone,a.borrow_all_money,a.annulized_rate_int from borrower_bulk_standard_apply_order a left join user_info b on a.user_id = b.id where a.id = ?";
		Record modify_info = Db.findFirst(sql, borrow_order_id);
		BigDecimal borrow_all_money = modify_info.getBigDecimal("borrow_all_money");
		borrow_all_money = borrow_all_money.setScale(0, BigDecimal.ROUND_DOWN);

		setAttribute("modify_info", modify_info);
		setAttribute("borrow_all_money", borrow_all_money);
		renderJsp("/user/loan_order/applay_order/modify_borrow_money.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "确定修改用户借款金额  年利率", last_update_author = "lh")
	public void modify_borrow_money() {

		int modify_money_int = getParameterToInt("modify_money");

		int modify_interest_rate_int = getParameterToInt("modify_interest_rate_int");//
		BigDecimal modify_interest_rate = new BigDecimal(new Integer(modify_interest_rate_int).doubleValue() / 100);// 修改后的年化利率
		BigDecimal modify_money = new BigDecimal(modify_money_int + "");// 修改后的借款金额
		int modify_share = modify_money_int / 50; // 修改后的借款份数
		int borrow_order_id = getParameterToInt("borrow_order_id");

		BorrowerBulkStandardApplyOrderM borrow_order = BorrowerBulkStandardApplyOrderM.dao.findById(borrow_order_id);

		boolean ok = borrow_order.set("borrow_all_money", modify_money).set("borrow_all_share", modify_share).set("annulized_rate_int", modify_interest_rate_int).set("annulized_rate", modify_interest_rate).update();

		if (ok) {
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "驳回用户借款申请", last_update_author = "lh")
	public void rejected_modify_loan_apply() {
		long loan_apply_order_id = getParameterToLong("loan_apply_order_id");
		String reason = getParameter("reason");
		final AdminM admin = getSessionAttribute("admin");
		final BorrowerBulkStandardApplyOrderM order = BorrowerBulkStandardApplyOrderM.dao.findById(loan_apply_order_id);
		final int user_id = order.getInt("user_id");
		final UserM user = UserM.dao.findFirst("select * from user_info where id = ?", user_id);

		System.out.println(reason);

		if (!Utils.isNotNullAndNotEmptyString(reason)) {
			renderText("no");// 填写驳回原因
			return;
		}

		boolean isOk = Db.tx(new IAtomic() {
			public boolean transactionProcessing() throws Exception {
				boolean ok = order.set("state", 1).update();
				if (ok) {
					return true;
				} else {
					return false;
				}
			}
		});

		if (isOk) {// 操作成功才发送信息
			SysNotificationM.send_sys_notification(admin.getInt("id"), user_id, "借款驳回", reason, user.getString("phone"), true, 4, user.getString("email") == null ? "@" : user.getString("email"));
			// , true, 11
			SysNotificationM.sys_send_message(user.getString("phone"), Model.Date.format(order.getTimestamp("commit_time")), 11);
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "拒绝用户借款申请", last_update_author = "lh")
	public void decline_loan_apply() {

		final long loan_apply_order_id = getParameterToLong("loan_apply_order_id");

		final AdminM admin = getSessionAttribute("admin");
		final BorrowerBulkStandardApplyOrderM order = BorrowerBulkStandardApplyOrderM.dao.findById(loan_apply_order_id);
		final int user_id = order.getInt("user_id");
		final UserM user = UserM.dao.findFirst("select * from user_info where id = ?", user_id);

		boolean isOk = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				BorrowerBulkStandardApplyOrderM order = BorrowerBulkStandardApplyOrderM.dao.findById(loan_apply_order_id);
				int loan_user_id = order.getInt("user_id"); // 借款用户的id
				UserM user = UserM.dao.findById(loan_user_id);
				user.set("borrow_type", 0);
				user.set("is_not_pay", 0);
				boolean ok1 = user.update();
				boolean ok2 = order.set("state", 5).update();
				boolean ok3 = true;

				int borrow_type = order.getInt("borrow_type");
				if (borrow_type == 3) {
					UserNowMoneyM user_now_money = UserNowMoneyM.dao.findById(user_id);
					ok3 = user_now_money.set("net_amount", 0).set("total_net_cash", 0).update();
				}

				if (ok1 && ok2 && ok3) {

					return true;
				} else {
					return false;
				}
			}
		});

		if (isOk) {
			SysNotificationM.send_sys_notification(admin.getInt("id"), user_id, "借款拒绝", "您的借款申请已经被拒绝，具体原因请联系客服。", user.getString("phone"), true, 4, user.getString("email") == null ? "@" : user.getString("email"));
			// true, 1
			SysNotificationM.sys_send_message(user.getString("phone"), Model.Date.format(order.getTimestamp("commit_time")), 1);
			renderText("1");
			return;
		} else {
			renderText("0");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "批准用户借款申请", last_update_author = "nicai")
	public void approve_loan_apply() {
		synchronized (LoanOrderAction.class) {
			final long loan_apply_order_id = getParameterToLong("loan_apply_order_id");// 借款申请单id

			final AdminM admin = getSessionAttribute("admin");
			final BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findById(loan_apply_order_id);
			int state = borrower_bulk_standard_apply_order.getInt("state");
			if (state != 3) {
				renderText("0");// 通过失败
				return;
			}
			final int user_id = borrower_bulk_standard_apply_order.getInt("user_id");
			final UserM user = UserM.dao.findFirst("select * from user_info where id = ?", user_id);

			boolean isOk = Db.tx(new IAtomic() {

				public boolean transactionProcessing() throws Exception {

					BorrowerBulkStandardApplyOrderM borrower_bulk_standard_apply_order = BorrowerBulkStandardApplyOrderM.dao.findById(loan_apply_order_id);
					if (borrower_bulk_standard_apply_order == null) {
						return false;
					}
					boolean is_ok_update_apply_order = borrower_bulk_standard_apply_order.set("state", 6).update();// 6

					int user_id = borrower_bulk_standard_apply_order.getInt("user_id"); // 借款人id

					String borrow_title = borrower_bulk_standard_apply_order.getString("borrow_title"); // 借款标题
					String borrow_purpose = borrower_bulk_standard_apply_order.getString("borrow_purpose");
					int borrow_type = borrower_bulk_standard_apply_order.getInt("borrow_type"); // 借款类型(
					// )
					BigDecimal borrow_all_money = borrower_bulk_standard_apply_order.getBigDecimal("borrow_all_money"); // 借款金额
					int borrow_all_money_level = borrower_bulk_standard_apply_order.getInt("borrow_all_money_level");
					int borrow_all_share = borrower_bulk_standard_apply_order.getInt("borrow_all_share"); // 总份数
					int borrow_duration = borrower_bulk_standard_apply_order.getInt("borrow_duration"); // 借款期限
					int borrow_duration_day = borrower_bulk_standard_apply_order.getInt("borrow_duration_day");
					BigDecimal annulized_rate = borrower_bulk_standard_apply_order.getBigDecimal("annulized_rate");// 年化利率
					int annulized_rate_int = borrower_bulk_standard_apply_order.getInt("annulized_rate_int");
					BigDecimal monthly_principal_and_interest = new BigDecimal("0");

					if (borrow_duration != 0) {
						monthly_principal_and_interest = CalculationFormula.get_principal_and_interest_by_year_rate(borrow_all_money, annulized_rate, borrow_duration);
					} else {
						monthly_principal_and_interest = CalculationFormula.get_principal_and_interest_by_year_rate_by_day(borrow_all_money, annulized_rate, borrow_duration_day);
					}

					UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
					if (borrow_type == 3) {// 信用等级评分
						user_credit_files.set("basic_info_credit_scores", 140).update();
					}
					String credit_rating = UserCreditFilesM.get_credit_rating(user_credit_files);// 信用等级
					// 字符
					int credit_rating_int = UserCreditFilesM.get_int_credit_rating(user_credit_files);// 信用等级
					// 对应数字
					// 1-6

					// 把借款申请单的信息 搬到筹款表 和 筹款_waiting单

					BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = new BorrowerBulkStandardGatherMoneyOrderM();

					borrower_bulk_standard_gather_money_order.set("user_id", user_id);
					borrower_bulk_standard_gather_money_order.set("apply_order_id", loan_apply_order_id);
					borrower_bulk_standard_gather_money_order.set("order_user_credit_info_id", 0); // 信用信息id
					borrower_bulk_standard_gather_money_order.set("order_user_info_id", 0); // 基本信息id
					// borrow_type
					borrower_bulk_standard_gather_money_order.set("borrow_type", borrow_type);
					borrower_bulk_standard_gather_money_order.set("borrow_title", borrow_title);
					borrower_bulk_standard_gather_money_order.set("borrow_purpose", borrow_purpose);
					borrower_bulk_standard_gather_money_order.set("borrow_all_money", borrow_all_money);
					borrower_bulk_standard_gather_money_order.set("borrow_all_share", borrow_all_share);
					borrower_bulk_standard_gather_money_order.set("remain_money", borrow_all_money); // 剩余金额，份数和借款金额，份数一样
					borrower_bulk_standard_gather_money_order.set("borrow_all_money_level", borrow_all_money_level);
					borrower_bulk_standard_gather_money_order.set("remain_share", borrow_all_share);
					borrower_bulk_standard_gather_money_order.set("borrow_duration", borrow_duration);
					borrower_bulk_standard_gather_money_order.set("borrow_duration_day", borrow_duration_day);
					borrower_bulk_standard_gather_money_order.set("annulized_rate", annulized_rate);
					borrower_bulk_standard_gather_money_order.set("annulized_rate_int", annulized_rate_int);
					borrower_bulk_standard_gather_money_order.set("credit_rating", credit_rating);
					borrower_bulk_standard_gather_money_order.set("credit_rating_int", credit_rating_int);
					Date now = new Date();
					borrower_bulk_standard_gather_money_order.set("add_time", now); // 审核通过时间

					Date deadline = new Date(new Date().getTime() + Day * 7);
					borrower_bulk_standard_gather_money_order.set("deadline", deadline); // 筹款截止时间
					borrower_bulk_standard_gather_money_order.set("monthly_principal_and_interest", monthly_principal_and_interest);

					boolean is_ok_add_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.save();

					long id = borrower_bulk_standard_gather_money_order.getLong("id");
					BorrowerBulkStandardGatherMoneyOrderWaitingM borrower_bulk_standard_gather_money_order_waiting = new BorrowerBulkStandardGatherMoneyOrderWaitingM();
					borrower_bulk_standard_gather_money_order_waiting.set("user_id", user_id);
					borrower_bulk_standard_gather_money_order_waiting.set("gather_money_order_id", id);
					borrower_bulk_standard_gather_money_order_waiting.set("add_time", now);
					borrower_bulk_standard_gather_money_order_waiting.set("deadline", deadline);

					boolean is_ok_add_borrower_bulk_standard_gather_money_order_waiting = borrower_bulk_standard_gather_money_order_waiting.save();

					UserM user = UserM.dao.findById(user_id);
					BorrowerBulkStandardOrderUserInfoM borrower_bulk_standard_order_user_info = new BorrowerBulkStandardOrderUserInfoM();
					UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findById(user_id);
					UserAuthenticateAssetsInfoM user_authenticate_assets_info = UserAuthenticateAssetsInfoM.dao.findById(user_id);
					UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findById(user_id);
					UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findById(user_id);
					UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);

					BigDecimal line_of_credit = user_credit_files.getBigDecimal("line_of_credit");// borrow_all_money
					if (line_of_credit.compareTo(borrow_all_money) == -1) {
						line_of_credit = borrow_all_money;
						user_credit_files.set("line_of_credit", line_of_credit);
					}

					BigDecimal principal_and_interest = monthly_principal_and_interest.multiply(new BigDecimal(borrow_duration)).setScale(2, BigDecimal.ROUND_DOWN);// 待还本息

					String sex = UserM.getSex(user_id);
					String age = UserM.getAge(user_id);
					String attribute[] = { "id", "work", "credit", "income", "housing", "car", "marriage", "education", "title", "living" };
					borrower_bulk_standard_order_user_info.set("gather_money_order_id", id).set("user_id", user_id)//
							.set("nickname", user.get("nickname")).set("sex", sex).set("age", age)//
							.set("highest_education", user_authenticate_persional_info.get("highest_education"))//
							.set("year_of_admission", user_authenticate_persional_info.get("year_of_admission"))//
							.set("school", user_authenticate_persional_info.get("school"))//
							.set("marriage", user_authenticate_family_info.get("marriage"))//
							.set("company_trades", user_authenticate_work_info.get("company_trades"))//
							.set("company_size", user_authenticate_work_info.get("company_size"))//
							.set("position", user_authenticate_work_info.get("position"))//
							.set("company_city", user_authenticate_work_info.get("work_city"))//
							.set("year_limit", user_authenticate_work_info.get("year_limit"))//
							.set("monthly_income", user_authenticate_work_info.get("monthly_income"))//
							.set("housing", user_authenticate_assets_info.get("housing"))//
							.set("housing_mortgage", user_authenticate_assets_info.get("housing_mortgage"))//
							.set("car", user_authenticate_assets_info.get("car"))//
							.set("car_mortgage", user_authenticate_assets_info.get("car_mortgage"))//
							.set("credit_rating", user_credit_files.get("credit_rating"))//
							.set("credit_sum_scores", user_credit_files.get("credit_sum_scores"))//
							.set("basic_info_credit_scores", user_credit_files.get("basic_info_credit_scores"))//
							.set("credit_rating", user_credit_files.get("credit_rating"))//
							.set("loan_application", user_credit_files.get("loan_application"))//
							.set("successful_loan", user_credit_files.getInt("successful_loan") + 1)//
							.set("pay_off_the_pen_number", user_credit_files.get("pay_off_the_pen_number"))//
							.set("line_of_credit", line_of_credit)//
							.set("total_loan", borrow_all_money)//
							.set("repay", principal_and_interest)//
							.set("overdue_amount", user_credit_files.get("overdue_amount"))//
							.set("overdue_num", user_credit_files.get("overdue_num"))//
							.set("seriously_overdue", user_credit_files.get("seriously_overdue"));//

					for (int i = 0; i < attribute.length; i++) {
						int scores = user_authenticate_upload_info.get(attribute[i] + "_authenticate_scores", 0);
						borrower_bulk_standard_order_user_info.set(attribute[i] + "_authenticate_status", user_authenticate_upload_info.get(attribute[i] + "_authenticate_status"))//
								.set(attribute[i] + "_authenticate_scores", scores);
					}

					boolean is_ok_add_borrower_bulk_standard_order_user_info = borrower_bulk_standard_order_user_info.save();
					int successful_loan = user_credit_files.getInt("successful_loan") + 1;
					boolean is_ok_update_user_credit_files = user_credit_files.set("successful_loan", successful_loan).update();
					if (is_ok_update_apply_order && is_ok_add_borrower_bulk_standard_gather_money_order && is_ok_add_borrower_bulk_standard_gather_money_order_waiting && is_ok_add_borrower_bulk_standard_order_user_info && is_ok_update_user_credit_files) {
						return true;
					} else {
						return false;
					}
				}
			});

			if (isOk) {

				SysNotificationM.send_sys_notification(admin.getInt("id"), user_id, "借款通过", "尊敬的客户，您的借款申请已经审核通过，感谢您对联富金融的支持。", user.getString("phone"), true, 5, user.getString("email") == null ? "@" : user.getString("email"));
				// , true, 10
				SysNotificationM.sys_send_message(user.getString("phone"), Model.Date.format(borrower_bulk_standard_apply_order.getTimestamp("commit_time")), 10);
				renderText("1");
				return;
			} else {
				renderText("0");
				return;
			}
		}

	}

}

package com.tfoll.web.action.user.loan_order;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ActionKey("/user/loan_order")
public class HoldCreditRightAction extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "债权信息列表导航", last_update_author = "lh")
	public void hold_credit_right_index() {

		setSessionAttribute("real_name", null);
		setSessionAttribute("debt_id", -1);
		setSessionAttribute("hold_debt_start_time", null);
		setSessionAttribute("hold_debt_end_time", null);

		renderJsp("/user/loan_order/creditor_right/user_creditor_right_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "债权信息列表查询", last_update_author = "lh")
	public void hold_credit_right_qurey() {

		String real_name = getParameter("real_name");
		int debt_id = getParameterToInt("debt_id", -1);
		String hold_debt_start_time = getParameter("hold_debt_start_time");
		String hold_debt_end_time = getParameter("hold_debt_end_time");

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("debt_id", debt_id);
		setSessionAttribute("hold_debt_start_time", hold_debt_start_time);
		setSessionAttribute("hold_debt_end_time", hold_debt_end_time);

		setAttribute("real_name", real_name);
		setAttribute("debt_id", debt_id);
		setAttribute("hold_debt_start_time", hold_debt_start_time);
		setAttribute("hold_debt_end_time", hold_debt_end_time);

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);
		List<Object> params = new ArrayList<Object>();

		sql_count.append("select count(1) from lender_bulk_standard_creditor_right_hold a, user_info b where a.user_id = b.id ");
		sql_select.append("select a.*,b.real_name,b.nickname from lender_bulk_standard_creditor_right_hold a, user_info b where a.user_id = b.id ");

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}
		if (debt_id != -1) {
			sql_count.append(" and a.gather_money_order_id = ? ");
			sql_select.append(" and a.gather_money_order_id = ? ");
			params.add(debt_id);
		}
		if (Utils.isNotNullAndNotEmptyString(hold_debt_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(hold_debt_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.add_time > ? ");
			sql_count.append(" and a.add_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(hold_debt_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(hold_debt_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.add_time < ? ");
			sql_count.append(" and a.add_time < ? ");
			params.add(date);
		}

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<Record> hold_credit_right_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(hold_credit_right_list)) {
				setAttribute("hold_credit_right_list", hold_credit_right_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/hold_credit_right_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/creditor_right/user_creditor_right_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "债权信息列表页面", last_update_author = "lh")
	public void hold_credit_right_page() {

		int pn = getParameterToInt("pn");
		String real_name = getParameter("real_name");
		int debt_id = getParameterToInt("debt_id");
		String hold_debt_start_time = getParameter("hold_debt_start_time");
		String hold_debt_end_time = getParameter("hold_debt_end_time");

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("debt_id", debt_id);
		setSessionAttribute("hold_debt_start_time", hold_debt_start_time);
		setSessionAttribute("hold_debt_end_time", hold_debt_end_time);

		setAttribute("real_name", real_name);
		setAttribute("debt_id", debt_id);
		setAttribute("hold_debt_start_time", hold_debt_start_time);
		setAttribute("hold_debt_end_time", hold_debt_end_time);

		StringBuilder sql_count = new StringBuilder(100);
		StringBuilder sql_select = new StringBuilder(100);
		List<Object> params = new ArrayList<Object>();

		sql_count.append("select count(1) from lender_bulk_standard_creditor_right_hold a, user_info b where a.user_id = b.id ");
		sql_select.append("select a.*,b.real_name,b.nickname from lender_bulk_standard_creditor_right_hold a, user_info b where a.user_id = b.id ");

		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and b.real_name = ? ");
			sql_select.append(" and b.real_name = ? ");
			params.add(real_name);
		}
		if (debt_id != -1) {
			sql_count.append(" and a.gather_money_order_id = ? ");
			sql_select.append(" and a.gather_money_order_id = ? ");
			params.add(debt_id);
		}
		if (Utils.isNotNullAndNotEmptyString(hold_debt_start_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(hold_debt_start_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.add_time > ? ");
			sql_count.append(" and a.add_time > ? ");
			params.add(date);
		}
		if (Utils.isNotNullAndNotEmptyString(hold_debt_end_time)) {
			Date date = null;
			try {
				date = Model.Time.parse(hold_debt_end_time);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			sql_select.append(" and a.add_time < ? ");
			sql_count.append(" and a.add_time < ? ");
			params.add(date);
		}

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);

			List<Record> hold_credit_right_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(hold_credit_right_list)) {
				setAttribute("hold_credit_right_list", hold_credit_right_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/loan_order/hold_credit_right_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/loan_order/creditor_right/user_creditor_right_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "债权详细页面", last_update_author = "lh")
	public void credit_right_detail() {

		int debt_id = getParameterToInt("debt_id");// 债权id
		int user_id = getParameterToInt("user_id");// 持有人id

		String sql = "SELECT b.*, a.borrow_all_money, a.borrow_all_share, a.monthly_principal_and_interest, c.hold_share, c.user_id FROM borrower_bulk_standard_gather_money_order a, borrower_bulk_standard_repayment_plan b, lender_bulk_standard_creditor_right_hold c WHERE a.id = ? AND b.gather_money_order_id = a.id AND c.gather_money_order_id = a.id AND c.user_id = ?";
		List<Record> record_list = Db.find(sql, new Object[] { debt_id, user_id });

		// 计算理财人每个月应该回收的金额
		for (Record record : record_list) {
			BigDecimal monthly_principal_and_interest = record.getBigDecimal("monthly_principal_and_interest");// 等额本息
			int borrow_all_share = record.getInt("borrow_all_share"); // 总份额
			int hold_share = record.getInt("hold_share"); // 持有份额
			BigDecimal factor = (new BigDecimal(hold_share + "")).divide(new BigDecimal(borrow_all_share + ""), 20, BigDecimal.ROUND_DOWN);

			BigDecimal recieve_money = monthly_principal_and_interest.multiply(factor);

			record.add("recieve_money", recieve_money);
		}

		setAttribute("debt_detail", record_list);
		renderJsp("/user/loan_order/creditor_right/credit_right_detail.jsp");

	}

}

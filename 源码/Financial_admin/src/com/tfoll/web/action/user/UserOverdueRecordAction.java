package com.tfoll.web.action.user;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.page.PageDiv;

import java.util.List;

@ActionKey("/user")
public class UserOverdueRecordAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "用户逾期记录查询", last_update_author = "lh")
	public void user_overdue_record_list() {
		int pn = getParameterToInt("pn", 1);
		StringBuffer select_count = new StringBuffer("SELECT count(1) FROM user_info u,borrower_bulk_standard_repayment_plan p,borrower_bulk_standard_gather_money_order b WHERE p.is_repay = 0 AND b.pay_for_object = 0 AND TO_DAYS(NOW()) - TO_DAYS(p.repay_end_time) >= -4 AND u.id = p.borrower_user_id AND u.id = b.user_id");
		StringBuffer select_sql = new StringBuffer(
				"SELECT u.nickname,u.real_name,u.user_identity,u.phone,p.should_repayment_total,p.repay_end_time, b.pay_for_object FROM user_info u,borrower_bulk_standard_repayment_plan p,borrower_bulk_standard_gather_money_order b WHERE p.is_repay = 0 AND TO_DAYS(NOW()) - TO_DAYS(p.repay_end_time) >= -4 AND u.id = p.borrower_user_id AND u.id = b.user_id order by p.repay_end_time asc");
		Long totalRow = Db.queryLong(select_count.toString());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> user_plan_list = Db.find(page.creatLimitSql(select_sql.toString()));
			/*
			 * for (Record user_plan : user_plan_list) { String repay_end_time =
			 * user_plan.getTimestamp("repay_end_time").toString();
			 * user_plan.add("repay_end_time", repay_end_time); }
			 */
			setAttribute("user_overdue_record_list", user_plan_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/user_overdue_record_list");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/user_overdue_record.jsp");
		return;
	}
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "借款人的严重逾期还款记录", last_update_author = "zjb")
	public void borrower_bulk_standard_overdue_repayment_record() {
		int pn = getParameterToInt("pn", 1);
		StringBuffer select_count = new StringBuffer("SELECT count(1) FROM borrower_bulk_standard_overdue_repayment_record a, user_info b, borrower_bulk_standard_gather_money_order c WHERE a.gather_money_order_id = c.id AND b.id = c.user_id");
		StringBuffer select_sql = new StringBuffer(
				"SELECT a.*, b.* FROM borrower_bulk_standard_overdue_repayment_record a, user_info b, borrower_bulk_standard_gather_money_order c WHERE a.gather_money_order_id = c.id AND b.id = c.user_id");
		Long totalRow = Db.queryLong(select_count.toString());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> borrower_bulk_standard_overdue_repayment_record_list = Db.find(page.creatLimitSql(select_sql.toString()));

			setAttribute("borrower_bulk_standard_overdue_repayment_record_list", borrower_bulk_standard_overdue_repayment_record_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/borrower_bulk_standard_overdue_repayment_record");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/borrower_bulk_standard_overdue_repayment_record.jsp");
		return;
	}
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "当借款人某个还款计划逾期的情况下面-给理财人的回款的记录", last_update_author = "zjb")
	public void borrower_bulk_standard_repayment_by_system() {
		int pn = getParameterToInt("pn", 1);
		StringBuffer select_count = new StringBuffer("SELECT count(1) FROM borrower_bulk_standard_repayment_by_system a,user_info b where a.borrower_user_id = b.id");
		StringBuffer select_sql = new StringBuffer(
				"SELECT a.*,b.* FROM borrower_bulk_standard_repayment_by_system a,user_info b where a.borrower_user_id = b.id");
		Long totalRow = Db.queryLong(select_count.toString());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> borrower_bulk_standard_repayment_by_system_list = Db.find(page.creatLimitSql(select_sql.toString()));

			setAttribute("borrower_bulk_standard_repayment_by_system_list", borrower_bulk_standard_repayment_by_system_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/borrower_bulk_standard_repayment_by_system");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/borrower_bulk_standard_repayment_by_system.jsp");
		return;
	}

}

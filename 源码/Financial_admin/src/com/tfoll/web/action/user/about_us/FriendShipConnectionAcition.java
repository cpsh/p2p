package com.tfoll.web.action.user.about_us;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.AboutFriendShipConnectionM;
import com.tfoll.web.util.Utils;

import java.util.List;

@ActionKey("/user/friendship_conn")
public class FriendShipConnectionAcition extends Controller {

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "跳转到友情连接管理页面", last_update_author = "hx")
	public void goto_friendship_conn_manage() {
		renderJsp("/user/about_us/friendship_conn_manage.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "添加友情链接", last_update_author = "hx")
	public void add_friendship_conn_page() {
		String sql = "SELECT ifnull(MAX(sort),0) FROM about_friendship_connetion";
		Long max_sort = Db.queryLong(sql);
		setAttribute("sort", max_sort + 1);
		renderJsp("/user/about_us/add_friendship_conn.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "保存友情链接", last_update_author = "hx")
	public void save_friendship_conn() {
		final int id = getParameterToInt("id", 0);
		final String site_name = getParameter("site_name");
		final int type = getParameterToInt("type");
		final String site_url = getParameter("site_url");
		final int is_effect = getParameterToInt("is_effect");
		final int sort = getParameterToInt("sort");
		final String image_name = getParameter("image_name", "");

		if (Utils.isNullOrEmptyString(site_name)) {
			renderText("1");
			return;
		}
		if (Utils.isNullOrEmptyString(site_url)) {
			renderText("2");
			return;
		}
		boolean is_ok = false;
		if (id > 0) {
			is_ok = Db.tx(new IAtomic() {
				public boolean transactionProcessing() throws Exception {
					AboutFriendShipConnectionM about_friendShip_connection = AboutFriendShipConnectionM.dao.findById(id);
					about_friendShip_connection.set("site_name", site_name);
					about_friendShip_connection.set("type", type);
					about_friendShip_connection.set("site_url", site_url);
					about_friendShip_connection.set("is_effect", is_effect);
					about_friendShip_connection.set("sort", sort);
					about_friendShip_connection.set("image_name", image_name);
					boolean bool1 = about_friendShip_connection.update();
					if (bool1) {
						return true;
					} else
						return false;
				}
			});
		} else {
			is_ok = Db.tx(new IAtomic() {
				public boolean transactionProcessing() throws Exception {
					AboutFriendShipConnectionM about_friendShip_connection = new AboutFriendShipConnectionM();
					about_friendShip_connection.set("site_name", site_name);
					about_friendShip_connection.set("type", type);
					about_friendShip_connection.set("site_url", site_url);
					about_friendShip_connection.set("is_effect", is_effect);
					about_friendShip_connection.set("sort", sort);
					about_friendShip_connection.set("image_name", image_name);
					boolean bool1 = about_friendShip_connection.save();
					if (bool1) {
						return true;
					} else
						return false;

				}
			});
		}

		if (is_ok) {
			renderText("3");
			return;
		} else {
			renderText("4");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "查询友情链接列表", last_update_author = "hx")
	public void find_all_friendship_conn() {
		String sql = "select * from about_friendship_connetion order by id desc";
		List<AboutFriendShipConnectionM> about_friendship_connection_List = AboutFriendShipConnectionM.dao.find(sql);
		if (Utils.isHasData(about_friendship_connection_List)) {
			setAttribute("about_friendship_connection_List", about_friendship_connection_List);
		} else {
			setAttribute("tips", "暂无数据");
		}
		renderJsp("/user/about_us/friendship_conn_manage.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "点击查看详情", last_update_author = "hx")
	public void get_friendship_detail() {
		Integer id = getParameterToInt("id");
		AboutFriendShipConnectionM about_friendship_connection = AboutFriendShipConnectionM.dao.findById(id);
		if (about_friendship_connection != null) {
			setAttribute("about_friendship_connection", about_friendship_connection);
		}
		renderJsp("/user/about_us/about_friendship_detail.jsp");
		return;
	}
}

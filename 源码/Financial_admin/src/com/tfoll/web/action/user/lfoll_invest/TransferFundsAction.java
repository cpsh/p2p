package com.tfoll.web.action.user.lfoll_invest;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.FixBidTransferFundsToUserAccountM;
import com.tfoll.web.util.DateTimeUtil;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.BootStraoDiv;

import java.util.ArrayList;
import java.util.List;

@ActionKey(value = "/user/lfoll_invest/transfer_funds")
public class TransferFundsAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "联富宝80%资金转移列表", last_update_author = "曹正辉")
	public void transfer_funds_index() {
		/**
		 * 3个用户1 2 3
		 */
		setSessionAttribute("user_id", -1);
		/**
		 * 选择期数-对应联富宝的订单表
		 */
		setSessionAttribute("start_time", null);// 查询开始时间
		setSessionAttribute("end_time", null);// 查询结束时间
		renderJsp("/user/lfoll_invest/lfoll_invest_transfer_funds_list.jsp");
		return;

	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "所有人", function_description = "联富宝80%资金转移列表", last_update_author = "曹正辉")
	public void transfer_funds_query() {
		int user_id = getParameterToInt("user_id", -1);//
		String start_time = getParameter("start_time");
		/**
		 * 多查询一周
		 */
		String end_time = getParameter("end_time");
		if (!(user_id == -1 || user_id == 1 || user_id == 2 || user_id == 3)) {
			renderText("");
			return;
		}

		setSessionAttribute("user_id", user_id);
		setSessionAttribute("start_time", start_time);
		setSessionAttribute("end_time", end_time);

		setAttribute("user_id", user_id);
		setAttribute("start_time", start_time);
		setAttribute("end_time", end_time);

		long start_time_long = 0;
		long end_time_long = 0;

		if (Utils.isNotNullAndNotEmptyString(start_time)) {
			start_time_long = (DateTimeUtil.my_get_morning_time(start_time)).getTime();
		}
		if (Utils.isNotNullAndNotEmptyString(end_time)) {
			end_time_long = (DateTimeUtil.my_get_night_time(end_time)).getTime() + Day * (7 + 1);
		}
		// SELECT * from fix_bid_transfer_funds_to_user_account WHERE
		// transfer_to_user_id=1 and add_time_long>=1 and add_time_long<=1
		StringBuilder sql_select = new StringBuilder();
		StringBuilder sql_count = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql_select.append("SELECT * from fix_bid_transfer_funds_to_user_account WHERE 1=1 ");
		sql_count.append("SELECT count(1) from fix_bid_transfer_funds_to_user_account WHERE 1=1 ");
		if (user_id != -1) {
			sql_select.append(" and  transfer_to_user_id=? ");
			sql_count.append(" and   transfer_to_user_id=? ");
			params.add(user_id);
		}
		if (start_time_long != 0) {
			sql_select.append(" and   add_time_long>=? ");
			sql_count.append(" and   add_time_long>=? ");
			params.add(start_time_long);
		}
		if (end_time_long != 0) {
			sql_select.append(" and   add_time_long<=? ");
			sql_count.append(" and   add_time_long<=? ");
			params.add(end_time_long);
		}

		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());

		if (totalRow > 0) {
			Page page = new Page(totalRow, 1);

			List<FixBidTransferFundsToUserAccountM> fix_bid_transfer_funds_to_user_account_list = FixBidTransferFundsToUserAccountM.dao.find(page.creatLimitSql(sql_select.toString()), params.toArray());

			if (Utils.isHasData(fix_bid_transfer_funds_to_user_account_list)) {
				setAttribute("fix_bid_transfer_funds_to_user_account_list", fix_bid_transfer_funds_to_user_account_list);
			}

			String url = BootStraoDiv.createUrl(ActionContext.getRequest(), "/user/lfoll_invest/transfer_funds/transfer_funds_page");
			String pageDiv = BootStraoDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}

		renderJsp("/user/lfoll_invest/lfoll_invest_transfer_funds_list.jsp");
		return;
	}

}

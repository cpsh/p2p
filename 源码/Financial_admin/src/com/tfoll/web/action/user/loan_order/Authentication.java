package com.tfoll.web.action.user.loan_order;

import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.UserAuthenticateAssetsInfoM;
import com.tfoll.web.model.UserAuthenticateFamilyInfoM;
import com.tfoll.web.model.UserAuthenticatePersionalInfoM;
import com.tfoll.web.model.UserAuthenticateUploadInfoM;
import com.tfoll.web.model.UserAuthenticateWorkInfoM;
import com.tfoll.web.model.UserCreditFilesM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.util.Utils;

import it.sauronsoftware.base64.Base64;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/loan_order")
public class Authentication extends Controller {

	public static class IndexUrl {
		public int getIndex() {
			return index;
		}

		public String getUrl() {
			return url;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public IndexUrl() {
		};

		private int index;// 索引
		private String url;// 地址
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "跳转到认证信息审核页面", last_update_author = "zjb")
	public void authentication_info_index() {
		String user_id = ActionContext.getParameter("user_id");
		String attribute[] = { "id_authenticate_url", "work_authenticate_url", "credit_authenticate_url", "income_authenticate_url", "housing_authenticate_url", "car_authenticate_url", "marriage_authenticate_url", "education_authenticate_url", "title_authenticate_url", "living_authenticate_url" };

		String real_name = ActionContext.getParameter("real_name");
		String user_identity = getParameter("user_identity");
		long loan_order_id = getParameterToLong("loan_order_id");
		String describe = ActionContext.getParameter("describe");

		setAttribute("loan_order_id", loan_order_id);
		setAttribute("real_name", real_name);
		setAttribute("user_id", user_id);
		setAttribute("user_identity", user_identity);
		setAttribute("describe", describe);

		System.out.println(describe);

		UserM user = UserM.dao.findById(user_id);
		int borrow_type = user.getInt("borrow_type");

		UserAuthenticateAssetsInfoM user_authenticate_assets_info = UserAuthenticateAssetsInfoM.dao.findById(user_id);
		UserAuthenticateFamilyInfoM user_authenticate_family_info = UserAuthenticateFamilyInfoM.dao.findById(user_id);
		UserAuthenticatePersionalInfoM user_authenticate_persional_info = UserAuthenticatePersionalInfoM.dao.findById(user_id);

		UserAuthenticateWorkInfoM user_authenticate_work_info = UserAuthenticateWorkInfoM.dao.findById(user_id);
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);

		setAttribute("user_authenticate_assets_info", user_authenticate_assets_info);
		setAttribute("user_authenticate_family_info", user_authenticate_family_info);
		setAttribute("user_authenticate_persional_info", user_authenticate_persional_info);

		setAttribute("user_authenticate_work_info", user_authenticate_work_info);
		setAttribute("user_authenticate_upload_info", user_authenticate_upload_info);
		setAttribute("user_credit_files", user_credit_files);
		setAttribute("borrow_type", borrow_type);

		List<String> id_authenticate_list = new ArrayList<String>();
		List<String> work_authenticate_list = new ArrayList<String>();
		List<String> credit_authenticate_list = new ArrayList<String>();
		List<String> income_authenticate_list = new ArrayList<String>();
		List<String> housing_authenticate_list = new ArrayList<String>();

		List<String> car_authenticate_list = new ArrayList<String>();
		List<String> marriage_authenticate_list = new ArrayList<String>();
		List<String> education_authenticate_list = new ArrayList<String>();
		List<String> title_authenticate_list = new ArrayList<String>();
		List<String> living_authenticate_list = new ArrayList<String>();

		List[] list_all = { id_authenticate_list, work_authenticate_list, credit_authenticate_list, income_authenticate_list, housing_authenticate_list, car_authenticate_list, marriage_authenticate_list, education_authenticate_list, title_authenticate_list, living_authenticate_list };

		// 取出数据库相关上传信息地址，加密
		for (int i = 0; i < 10; i++) {
			String path = user_authenticate_upload_info.get(attribute[i]);
			if (Utils.isNotNullAndNotEmptyString(path)) {
				List<IndexUrl> index_url_list = gson.fromJson(path, new TypeToken<List<IndexUrl>>() {
				}.getType());
				int index_url_list_size = index_url_list.size();

				for (int j = 0; j < index_url_list_size; j++) {
					String url = index_url_list.get(j).getUrl();
					String code = Base64.encode(url);
					list_all[i].add(code);
				}
			}
		}

		// 加密订单id ，供下载zip
		// String apply_order_id_code = Base64.encode(loan_order_id+"");

		setAttribute("loan_order_id", loan_order_id);
		setAttribute("upload_info", list_all);

		renderJsp("/user/loan_order/applay_order/authenticate_info.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "认证审核评分", last_update_author = "zjb")
	public void authentication_info_scores() {

		int user_id = getParameterToInt("user_id");
		int scores = getParameterToInt("scores");
		int type = getParameterToInt("type");
		int okorno = getParameterToInt("okorno");// 1 通过，2不通过
		String attribute[] = { "id", "work", "credit", "income", "housing", "car", "marriage", "education", "title", "living" };
		boolean ok = false;

		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
		// BorrowerBulkStandardOrderUserInfoM
		// borrower_bulk_standard_order_user_info =
		// BorrowerBulkStandardOrderUserInfoM.get_last_info(user_id);

		if (type == 100) {
			ok = user_credit_files.set("basic_info_credit_scores", scores).update();
		} else {
			if (type == 101) {
				type = 0;
			}

			if (okorno == 1 && scores == 0) {
				renderText("err");
				return;
			}
			if (okorno == 2) {// 不通过
				scores = 0;
			}
			ok = user_authenticate_upload_info.set(attribute[type] + "_authenticate_scores", scores).set(attribute[type] + "_authenticate_status", okorno).update();
		}

		String credit_rating = UserCreditFilesM.get_credit_rating(user_credit_files);
		boolean ok_rating = user_credit_files.set("credit_rating", credit_rating).update();

		if (ok && ok_rating) {

			renderText(scores + "");
			return;
		} else {
			renderText("no");
			return;
		}
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before( { AdminAop.class })
	@Function(for_people = "管理员", function_description = "认证审核下载信息评分", last_update_author = "zjb")
	public void upload_scores() {
		String user_id = getParameter("user_id");
		int scores = getParameterToInt("scores");
		int type = getParameterToInt("type");
		int status_in = getParameterToInt("status");

		UserAuthenticateUploadInfoM user_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);
		String attribute[] = { "id_authenticate_status", "work_authenticate_status", "credit_authenticate_status", "income_authenticate_status", "housing_authenticate_status", "car_authenticate_status", "marriage_authenticate_status", "education_authenticate_status", "title_authenticate_status", "living_authenticate_status" };
		int status = user_upload_info.get(attribute[type]);

		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);
		int credit_scores = user_credit_files.getInt("credit_sum_scores");

		int scores_now = 0;

		// 如果传入状态（status_in） 和 数据库 认证状态 （status） 相等则返回，不相等则 进行加减分
		if (status == status_in) {
			renderText("already");
			return;
		} else {
			user_upload_info.set(attribute[type], status_in);
			if (status_in == 1) {
				scores_now = credit_scores + scores;
			} else if (status_in == 2) {
				scores_now = credit_scores - scores;
			}
		}

		user_credit_files.set("credit_sum_scores", scores_now);

		boolean ok = user_credit_files.update();
		boolean ok2 = user_upload_info.update();

		if (ok && ok2) {
			renderText("ok");
			return;
		} else {
			renderText("no");
			return;
		}

	}

}

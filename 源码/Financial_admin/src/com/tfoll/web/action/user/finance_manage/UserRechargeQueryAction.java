package com.tfoll.web.action.user.finance_manage;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.util.Utils;
import com.tfoll.web.util.page.PageDiv;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/userrecharge")
public class UserRechargeQueryAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "跳转到用户充值记录界面", last_update_author = "向旋")
	public void to_user_recharge() {
		renderJsp("/user/finance_manage/handle_recharge_list.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "用户充值记录查询", last_update_author = "向旋")
	public void user_recharge_query() {
		int pn = getParameterToInt("pn", 1);
		String real_name = getParameter("real_name");
		String nickname = getParameter("nickname");
		String email = getParameter("email");
		String add_time_start = getParameter("add_time_start");
		String add_time_end = getParameter("add_time_end");
		String user_identity = getParameter("user_identity");
		int type = getParameterToInt("type", -1);

		setAttribute("real_name", real_name);
		setAttribute("nickname", nickname);
		setAttribute("email", email);
		setAttribute("add_time_start", add_time_start);
		setAttribute("add_time_end", add_time_end);
		setAttribute("user_identity", user_identity);
		setAttribute("type", type);

		setSessionAttribute("real_name", real_name);
		setSessionAttribute("nickname", nickname);
		setSessionAttribute("email", email);
		setSessionAttribute("add_time_start", add_time_start);
		setSessionAttribute("add_time_end", add_time_end);
		setSessionAttribute("user_identity", user_identity);
		setSessionAttribute("type", type);

		StringBuilder sql_count = new StringBuilder("select count(1) from user_recharge_log r,user_info u where 1=1 and u.id = r.user_id");
		StringBuilder sql_select = new StringBuilder("select u.real_name real_name,u.user_identity user_identity,u.add_time u_add_time,r.* from user_recharge_log r,user_info u where 1=1 and u.id = r.user_id");
		List<Object> params = new ArrayList<Object>();
		if (Utils.isNotNullAndNotEmptyString(real_name)) {
			sql_count.append(" and u.real_name = ?");
			sql_select.append(" and u.real_name = ?");
			params.add(real_name);
		}
		if (Utils.isNotNullAndNotEmptyString(nickname)) {
			sql_count.append(" and u.nickname = ?");
			sql_select.append(" and u.nickname = ?");
			params.add(nickname);
		}
		if (Utils.isNotNullAndNotEmptyString(email)) {
			sql_count.append(" and u.email = ?");
			sql_select.append(" and u.email = ?");
			params.add(email);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_start)) {
			sql_count.append(" and r.add_time_recharge>=?");
			sql_select.append(" and r.add_time_recharge>=?");
			params.add(add_time_start);
		}
		if (Utils.isNotNullAndNotEmptyString(add_time_end)) {
			sql_count.append(" and r.add_time_recharge<=?");
			sql_select.append(" and r.add_time_recharge<=?");
			params.add(add_time_end);
		}
		if (Utils.isNotNullAndNotEmptyString(user_identity)) {
			sql_count.append(" and u.user_identity = ?");
			sql_select.append(" and u.user_identity = ?");
			params.add(user_identity);
		}

		if (type == 1 || type == 2) {
			sql_count.append(" and r.type = ?");
			sql_select.append(" and r.type = ?");
			params.add(type);
		}
		Long totalRow = Db.queryLong(sql_count.toString(), params.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn);
			List<Record> user_recharge_list = Db.find(page.creatLimitSql(sql_select.toString()), params.toArray());
			setAttribute("user_recharge_list", user_recharge_list);
			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/userrecharge/user_recharge_query");
			String pageDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pageDiv);
		}
		renderJsp("/user/finance_manage/handle_recharge_list.jsp");
		return;
	}
}

package com.tfoll.web.action.user;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.trade.aop.AllowNotLogin;
import com.tfoll.trade.aop.Before;
import com.tfoll.trade.aop.ClearInterceptor;
import com.tfoll.trade.aop.ClearLayer;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.Controller;
import com.tfoll.trade.core.annotation.Function;
import com.tfoll.trade.core.annotation.actionbind.ActionKey;
import com.tfoll.trade.util.page.Page;
import com.tfoll.web.aop.AdminAop;
import com.tfoll.web.model.UserSuggestM;
import com.tfoll.web.util.page.PageDiv;

import java.util.ArrayList;
import java.util.List;

@ActionKey("/user/usersuggest")
public class UserSuggestionAction extends Controller {
	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "跳转到查询用户反馈界面", last_update_author = "向旋")
	public void to_usersuggest() {
		renderJsp("/user/user_suggest.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "查询用户反馈记录", last_update_author = "向旋")
	public void usersuggest_query() {
		int pn = getParameterToInt("pn", 1);
		StringBuilder sql_count = new StringBuilder("select count(1) from user_info u,user_suggestion s where u.id = s.user_id");
		StringBuilder sql_select = new StringBuilder("select u.*,s.id,s.is_read,s.type,s.content,s.add_time from user_info u,user_suggestion s where u.id = s.user_id");
		int type = getParameterToInt("type", 0);
		List<Object> parma = new ArrayList<Object>();
		if (type != 0) {
			sql_count.append(" and s.type = ?");
			sql_select.append(" and s.type = ?");
			parma.add(type);
		}
		int is_read = getParameterToInt("is_read", 0);
		if (is_read != 0) {
			sql_count.append(" and s.is_read = ?");
			sql_select.append(" and s.is_read = ?");
			parma.add(is_read);
		}

		setAttribute("type", type);
		setAttribute("is_read", is_read);

		setSessionAttribute("type", type);
		setSessionAttribute("is_read", is_read);

		Long totalRow = Db.queryLong(sql_count.toString(), parma.toArray());
		if (totalRow > 0) {
			Page page = new Page(totalRow, pn, 1);
			List<Record> user_suggest_list = Db.find(page.creatLimitSql(sql_select.toString()), parma.toArray());
			setAttribute("user_suggest_list", user_suggest_list);

			String url = PageDiv.createUrl(ActionContext.getRequest(), "/user/usersuggest/usersuggest_query");
			String pDiv = PageDiv.getDiv(url, page.getTotalPageNum(), page.getCurrentPageNum(), page.getPageSize());
			setAttribute("pageDiv", pDiv);
		}
		renderJsp("/user/user_suggest.jsp");
		return;
	}

	@AllowNotLogin
	@ClearInterceptor(ClearLayer.Before)
	@Before(AdminAop.class)
	@Function(for_people = "登录", function_description = "改变反馈的状态", last_update_author = "向旋")
	public void update_isread() {
		int id = getParameterToInt("id");
		int isread = getParameterToInt("is_read");
		UserSuggestM user_suggest = UserSuggestM.dao.findFirst("select * from user_suggestion where id = ? and is_read=?", new Object[] { id, isread });
		if (user_suggest != null) {
			user_suggest.set("is_read", 2).update();
		}
		renderJsp("/user/user_suggest.jsp");
		return;
	}
}

package com.tfoll.web.util;

import com.tfoll.trade.activerecord.model.Model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class LfollInvestConfigUtil {

	/**
	 * 配置开始时间"2014-10-29 09:00:00"
	 */
	private Date config_start_time;// 

	/**
	 * 配置结束时间 2018-10-29 09:00:00
	 */
	private Date config_end_time;

	/**
	 * 每 几天 生产下一期定投单
	 */
	private int index_day = 0;//

	/**
	 * 从通告开始到到预定开始的小时数 48 h
	 */
	private int from_notice_to_reservation_hours = 0;// 

	/**
	 * 预定开始到预定结束的小时数；31
	 */
	private int from_reservation_start_to_reservation_end = 0;//

	/**
	 * 从预定结束到支付截止时间 21 h
	 */
	private int from_reservation_end_to_paystop = 0;// 

	/**
	 * 从支付截止时间到开放定投时间为1h
	 */
	private int from_paystop_to_begain_order = 0;// 

	/**
	 * 
	 * 从开放定投时间到进入锁定期的时间67h
	 */
	private int from_begain_order_to_lock_order = 0;// 

	/**
	 * 从锁定时间到退出时间开发的月数 12 M;
	 */
	private int from_lock_time_to_exit_time = 0;//

	public LfollInvestConfigUtil() {
	}

	public LfollInvestConfigUtil(Date configStartTime, Date configEndTime, int indexDay, int fromNoticeToReservationHours, int fromReservationStartToReservationEnd, int fromReservationEndToPaystop, int fromPaystopToBegainOrder, int fromBegainOrderToLockOrder, int fromLockTimeToExitTime) {
		config_start_time = configStartTime;
		config_end_time = configEndTime;
		index_day = indexDay;
		from_notice_to_reservation_hours = fromNoticeToReservationHours;
		from_reservation_start_to_reservation_end = fromReservationStartToReservationEnd;
		from_reservation_end_to_paystop = fromReservationEndToPaystop;
		from_paystop_to_begain_order = fromPaystopToBegainOrder;
		from_begain_order_to_lock_order = fromBegainOrderToLockOrder;
		from_lock_time_to_exit_time = fromLockTimeToExitTime;
	}

	public Date getConfig_start_time() {
		return config_start_time;
	}

	public void setConfig_start_time(Date configStartTime) {
		config_start_time = configStartTime;
	}

	public Date getConfig_end_time() {
		return config_end_time;
	}

	public void setConfig_end_time(Date configEndTime) {
		config_end_time = configEndTime;
	}

	public int getIndex_day() {
		return index_day;
	}

	public void setIndex_day(int indexDay) {
		index_day = indexDay;
	}

	public int getFrom_notice_to_reservation_hours() {
		return from_notice_to_reservation_hours;
	}

	public void setFrom_notice_to_reservation_hours(int fromNoticeToReservationHours) {
		from_notice_to_reservation_hours = fromNoticeToReservationHours;
	}

	public int getFrom_reservation_start_to_reservation_end() {
		return from_reservation_start_to_reservation_end;
	}

	public void setFrom_reservation_start_to_reservation_end(int fromReservationStartToReservationEnd) {
		from_reservation_start_to_reservation_end = fromReservationStartToReservationEnd;
	}

	public int getFrom_reservation_end_to_paystop() {
		return from_reservation_end_to_paystop;
	}

	public void setFrom_reservation_end_to_paystop(int fromReservationEndToPaystop) {
		from_reservation_end_to_paystop = fromReservationEndToPaystop;
	}

	public int getFrom_paystop_to_begain_order() {
		return from_paystop_to_begain_order;
	}

	public void setFrom_paystop_to_begain_order(int fromPaystopToBegainOrder) {
		from_paystop_to_begain_order = fromPaystopToBegainOrder;
	}

	public int getFrom_begain_order_to_lock_order() {
		return from_begain_order_to_lock_order;
	}

	public void setFrom_begain_order_to_lock_order(int fromBegainOrderToLockOrder) {
		from_begain_order_to_lock_order = fromBegainOrderToLockOrder;
	}

	public int getFrom_lock_time_to_exit_time() {
		return from_lock_time_to_exit_time;
	}

	public void setFrom_lock_time_to_exit_time(int fromLockTimeToExitTime) {
		from_lock_time_to_exit_time = fromLockTimeToExitTime;
	}

	/**
	 * 根据系统配置的开始时间确定下一个定投的开始预告时间
	 * 
	 * @param start_time
	 *            开始时间
	 * @param index_day
	 *            最近两期定投之间间隔的天数
	 * @return
	 * @throws ParseException
	 */
	public static Date get_next_days_Date(Date config_start_time, int index_day) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(config_start_time);
		cal.add(Calendar.DAY_OF_YEAR, index_day);
		Date new_period_date = cal.getTime();
		if (new_period_date != null) {
			return new_period_date;
		} else {
			return null;
		}

	}

	/**
	 * 获取hours小时后的时间
	 * 
	 * @param start_time
	 *            开始时间
	 * @param hours
	 *            小时数
	 * @return
	 */
	public static Date get_next_hours_Date(Date start_time, int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(start_time);
		cal.add(Calendar.HOUR, hours);
		Date reseration_start_time = cal.getTime();
		if (reseration_start_time != null) {
			return reseration_start_time;
		} else {
			return null;
		}

	}

	/**
	 * 获取几个月后的时间
	 * 
	 * @param start_time
	 *            开始时间
	 * @param months
	 *            月数
	 * @return
	 */
	public static Date get_next_month_Date(Date start_time, int months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(start_time);
		cal.add(Calendar.MONTH, months);
		Date next_month_date = cal.getTime();
		if (next_month_date != null) {
			return next_month_date;
		} else {
			return null;
		}
	}

	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
		/*
		 * // fix_bid_system_order FixBidSystemOrderM fix_bid_system_orderM =
		 * new FixBidSystemOrderM(); SimpleDateFormat format = new
		 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 * 
		 * Date date = new Date(); String date_str = format.format(date); //
		 * System.out.println("------------date_str----------"+date_str); //
		 * Calendar calendar = Calendar.getInstance(); String
		 * fix_bid_notice_start_time = "2014-10-29 09:00:00";// 预告开始时间 int
		 * from_notice_to_reservation_hours = 48;// 从通告开始到到预定开始的小时数 48 h int
		 * from_reservation_start_to_reservation_end = 31;// 预定开始到预定结束的小时数；31 h
		 * int from_reservation_end_to_paystop = 21;// 从预定结束到支付截止时间 21 h int
		 * from_paystop_to_begain_order = 1;// 从支付截止时间到开放定投时间为1h int
		 * from_begain_order_to_lock_order = 67;// 从开放定投时间到进入锁定期的时间67h int
		 * from_lock_time_to_exit_time = 12;// 从锁定时间到退出时间开发的月数 12 M;
		 * 
		 * Date date_start = format.parse(fix_bid_notice_start_time); Calendar
		 * cal = Calendar.getInstance(); cal.setTime(date_start);
		 * cal.add(Calendar.HOUR, 21); Date one_hour_later_date = cal.getTime();
		 * System.out.println("------1小时以后------" +
		 * format.format(one_hour_later_date));
		 */
		Date str_date = Model.Date.parse("2014-10-29 10:00:00");
		Date next_date = LfollInvestConfigUtil.get_next_hours_Date(str_date, 72);

	}

}

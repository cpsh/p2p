package com.tfoll.web.util.filter;

import com.tfoll.web.util.Utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminLoginFilter implements Filter {
	public static final String isLogined = AdminLoginFilter.class.getName();

	/**
	 * 设置管理员登录界面
	 */

	public void destroy() {

	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		String path = request.getServletPath();
		String login_path = "/index/$my_admin_login_index.html";
		System.out.println(11);
		if (path != null && login_path.equals(path)) {
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			HttpSession session = request.getSession();
			String isLogined = (String) session.getAttribute(AdminLoginFilter.isLogined);
			if (Utils.isNotNullAndNotEmptyString(isLogined) && "1".equals(isLogined)) {
				filterChain.doFilter(servletRequest, servletResponse);
			} else {
				System.out.println(11);
				((HttpServletResponse) servletResponse).sendError(((HttpServletResponse) servletResponse).SC_FORBIDDEN, "");
			}
		}

	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

}

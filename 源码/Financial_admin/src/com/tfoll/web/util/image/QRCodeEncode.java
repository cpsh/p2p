package com.tfoll.web.util.image;

import com.swetake.util.Qrcode;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 二维码
 */
public class QRCodeEncode extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void destroy() {
		super.destroy();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		if (code == null) {
			return;
		}
		Qrcode qrcode = new Qrcode();
		qrcode.setQrcodeErrorCorrect('M');
		qrcode.setQrcodeEncodeMode('B');
		qrcode.setQrcodeVersion(7);

		byte[] bytes = code.getBytes("UTF-8");

		BufferedImage bi = new BufferedImage(139, 139, BufferedImage.TYPE_INT_RGB);

		// createGraphics
		Graphics2D g = bi.createGraphics();

		// set background
		g.setBackground(Color.WHITE);
		g.clearRect(0, 0, 139, 139);

		g.setColor(Color.BLACK);

		if (bytes.length > 0 && bytes.length < 123) {
			boolean[][] b = qrcode.calQrcode(bytes);

			for (int i = 0; i < b.length; i++) {

				for (int j = 0; j < b.length; j++) {
					if (b[j][i]) {
						g.fillRect(j * 3 + 2, i * 3 + 2, 3, 3);
					}
				}

			}
		}

		g.dispose();
		bi.flush();
		ImageIO.write(bi, "png", response.getOutputStream());
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.doPost(request, response);
	}

	public void init() throws ServletException {

	}

}

package com.tfoll.web.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Title: HTML相关的正则表达式工具类 Description: 包括过滤HTML标记，转换HTML标记，替换特定HTML标记
 */

public class HtmlRegexpUtil {
	private final static String regxpForHtml = "<([^>]*)>"; // 过滤所有以<开头以>结尾的标签

	private final static String regxpForImgTag = "<\\s*img\\s+([^>]*)\\s*>"; // 找出IMG标签

	private final static String regxpForImaTagSrcAttrib = "src=\"([^\"]+)\""; // 找出IMG标签的SRC属性

	/**  
     *   
     */
	public HtmlRegexpUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * 基本功能：替换标记以正常显示
	 * <p>
	 * 
	 * @param input
	 * @return String
	 */
	public String replaceTag(String input) {
		if (!hasSpecialChars(input)) {
			return input;
		}
		StringBuffer filtered = new StringBuffer(input.length());
		char c;
		for (int i = 0; i <= input.length() - 1; i++) {
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");
				break;
			case '>':
				filtered.append("&gt;");
				break;
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			default:
				filtered.append(c);
			}

		}
		return (filtered.toString());
	}

	/**
	 * 
	 * 基本功能：判断标记是否存在
	 * <p>
	 * 
	 * @param input
	 * @return boolean
	 */
	public boolean hasSpecialChars(String input) {
		boolean flag = false;
		if ((input != null) && (input.length() > 0)) {
			char c;
			for (int i = 0; i <= input.length() - 1; i++) {
				c = input.charAt(i);
				switch (c) {
				case '>':
					flag = true;
					break;
				case '<':
					flag = true;
					break;
				case '"':
					flag = true;
					break;
				case '&':
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * 基本功能：过滤所有以"<"开头以">"结尾的标签
	 * <p>
	 * 
	 * @param str
	 * @return String
	 */
	public static String filterHtml(String str) {
		Pattern pattern = Pattern.compile(regxpForHtml);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * tfoll 过滤文章内容
	 * 
	 * @param str
	 * @return
	 */
	public static String filterHtmlContent4Show(String str) {
		// 首先去掉<>
		String msg = "";
		Pattern pattern = Pattern.compile(regxpForHtml);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		msg = sb.toString();
		msg = msg.trim();

		msg = msg.replace("&nbsp ;", "");
		msg = msg.replace("nbsp;", "");
		msg = msg.replace("&nbs;", "");
		msg = msg.replace("&quot;", "");
		msg = msg.replace("&amp;", "&");
		msg = msg.replace("&lt;", "<");
		msg = msg.replace("&gt;", ">");
		msg = msg.replace("&", "");
		return msg;
	}

	public static void main(String[] args) throws Exception {

		/*
		 * ConsoleInit.initTables(); ArticleM xx = ArticleM.dao.findById(76);
		 * System.out.println(xx.getString("content")); String s =
		 * filterHtmlContent4Show(xx.getString("content"));
		 * System.out.println(s);
		 */

	}

	/**
	 * 
	 * 基本功能：过滤指定标签
	 * <p>
	 * 
	 * @param str
	 * @param tag
	 *            指定标签
	 * @return String
	 */
	public static String fiterHtmlTag(String str, String tag) {
		String regxp = "<\\s*" + tag + "\\s+([^>]*)\\s*>";
		Pattern pattern = Pattern.compile(regxp);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 
	 * 基本功能：替换指定的标签
	 * <p>
	 * 
	 * @param str
	 * @param beforeTag
	 *            要替换的标签
	 * @param tagAttrib
	 *            要替换的标签属性值
	 * @param startTag
	 *            新标签开始标记
	 * @param endTag
	 *            新标签结束标记
	 * @return String
	 * @如：替换img标签的src属性值为[img]属性值[/img]
	 */
	public static String replaceHtmlTag(String str, String beforeTag, String tagAttrib, String startTag, String endTag) {
		String regxpForTag = "<\\s*" + beforeTag + "\\s+([^>]*)\\s*>";
		String regxpForTagAttrib = tagAttrib + "=\"([^\"]+)\"";
		Pattern patternForTag = Pattern.compile(regxpForTag);
		Pattern patternForAttrib = Pattern.compile(regxpForTagAttrib);
		Matcher matcherForTag = patternForTag.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result = matcherForTag.find();
		while (result) {
			StringBuffer sbreplace = new StringBuffer();
			Matcher matcherForAttrib = patternForAttrib.matcher(matcherForTag.group(1));
			if (matcherForAttrib.find()) {
				matcherForAttrib.appendReplacement(sbreplace, startTag + matcherForAttrib.group(1) + endTag);
			}
			matcherForTag.appendReplacement(sb, sbreplace.toString());
			result = matcherForTag.find();
		}
		matcherForTag.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 
	 * 获取文章内容中第一张图片的地址
	 * 
	 * @param str
	 * @return
	 */
	public static String getFirstImage(String str) {
		// <p><img
		// src="http://upload.cankaoxiaoxi.com/2014/0610/1402371294267.jpg"
		// width="500" height="332"/></p><p>中新网6月10日电
		// 据台湾《旺报》报道，高调行善的大陆慈善家陈光标9日从厦门到金门，宣称要捐款人民币15亿元协助兴建“金厦和平大桥”，其中一张支票的抬头是“金门县人民政府李沃士县长同志”。但金门县长李沃士表示不清楚实际内容，给予“冷”处理，未与陈光标见面。</p><p>李沃士表示，对想帮忙金门做事的人，县府一向都特别欢迎，但必须是“脚踏实地”才行，对陈光标带团到金门，县府并不清楚实际内容，未来如果搞清楚他的真正意向，可以做一些妥适安排。</p><p>据报道，陈光标9日带领30余名大陆人士抵达金门，到金门各地参观。陈光标8日在厦门声称愿以个人名义捐款人民币15亿元协助兴建“金厦和平大桥”，并开出三张各5亿人民币的支票，分别给“台湾领导马英九同志”、李沃士、和平大桥公司。</p><p>陈光标表示，他在1997年时曾花25元人民币搭乘观光船到金厦海域旅游，当时就有捐建大桥的想法。陈光标还对大桥提出构想，称可建上下2层，上层车辆往来，下层行驶磁浮列车，桥底加挂天然气、电缆。</p><p><br/></p>
		// 获取src第一次出现的索引
		// System.out.println("----------------str------>>>>>>>>>>>>>"+str);
		try {
			int indexFirst = str.indexOf("http");
			// System.out.println("--------http出现的index---------"+indexFirst);
			int indexJPG = str.indexOf(".jpg");
			int indexPNG = str.indexOf(".png");
			int indexGIF = str.indexOf(".gif");
			String imageAddress = "";
			if (indexJPG != -1) {
				imageAddress = str.substring(indexFirst, indexJPG + 4);
				return imageAddress;
				// System.out.println("---------.jpg出现的index--------"+indexJPG);
			} else if (indexPNG != -1) {
				imageAddress = str.substring(indexFirst, indexPNG + 4);
				return imageAddress;
				// System.out.println("---------.png出现的index--------=="+indexPNG);
			} else if (indexGIF != -1) {
				imageAddress = str.substring(indexFirst, indexGIF + 4);
				return imageAddress;
				// System.out.println("---------.gif出现的index--------=="+indexGIF);
			} else {

				// System.out.println("-----------图片网络地址地址----------"+imageAddress);
				return imageAddress;
			}
		} catch (IndexOutOfBoundsException e) {
			return "";
		}
	}
}
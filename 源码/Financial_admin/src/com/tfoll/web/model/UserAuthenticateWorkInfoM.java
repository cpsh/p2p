package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_work_info", primaryKey = "user_id")
public class UserAuthenticateWorkInfoM extends Model<UserAuthenticateWorkInfoM> {
	public static UserAuthenticateWorkInfoM dao = new UserAuthenticateWorkInfoM();
}

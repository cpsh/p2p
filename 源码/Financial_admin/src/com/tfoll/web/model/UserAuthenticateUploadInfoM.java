package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_upload_info", primaryKey = "user_id")
public class UserAuthenticateUploadInfoM extends Model<UserAuthenticateUploadInfoM> {
	public static UserAuthenticateUploadInfoM dao = new UserAuthenticateUploadInfoM();

	// 获得上传信息信用总分
	public static int getUploadSumScores(int user_id) {
		UserAuthenticateUploadInfoM user_authenticate_upload_info = UserAuthenticateUploadInfoM.dao.findById(user_id);

		String attribute[] = { "id", "work", "credit", "income", "housing", "car", "marriage", "education", "title", "living" };
		int upload_sum_scores = 0;
		for (int i = 0; i < attribute.length; i++) {
			String arrt = attribute[i] + "_authenticate_scores";
			int scores = user_authenticate_upload_info.getInt(arrt);
			upload_sum_scores += scores;
		}
		return upload_sum_scores;
	}
}

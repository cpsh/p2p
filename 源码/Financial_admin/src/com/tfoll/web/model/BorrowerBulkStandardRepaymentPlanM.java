package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_repayment_plan", primaryKey = "id")
public class BorrowerBulkStandardRepaymentPlanM extends Model<BorrowerBulkStandardRepaymentPlanM> {
	public static BorrowerBulkStandardRepaymentPlanM dao = new BorrowerBulkStandardRepaymentPlanM();
}

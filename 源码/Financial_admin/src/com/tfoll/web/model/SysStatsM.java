package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "sys_stats", primaryKey = "id")
public class SysStatsM extends Model<SysStatsM> implements Serializable {
	private static final long serialVersionUID = 6604624322505003544L;
	public static SysStatsM dao = new SysStatsM();
}

package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_credit_files", primaryKey = "user_id")
public class UserCreditFilesM extends Model<UserCreditFilesM> {
	public static UserCreditFilesM dao = new UserCreditFilesM();

	// 根据user_id信用等级
	public static String get_credit_rating(int user_id) {
		String credit_rating = null;
		int sum_scores = UserCreditFilesM.get_sum_scores(user_id);

		if (0 <= sum_scores && sum_scores <= 89) {
			credit_rating = "C";
		} else if (90 <= sum_scores && sum_scores <= 104) {
			credit_rating = "C+";
		} else if (105 < sum_scores && sum_scores <= 119) {
			credit_rating = "B";
		} else if (119 < sum_scores && sum_scores <= 134) {
			credit_rating = "B+";
		} else if (134 < sum_scores && sum_scores <= 149) {
			credit_rating = "A";
		} else if (149 < sum_scores) {
			credit_rating = "A+";
		}
		return credit_rating;
	}

	// 根据model信用等级
	public static String get_credit_rating(UserCreditFilesM user_credit_files) {
		int user_id = user_credit_files.getInt("user_id");
		String credit_rating = null;

		int sum_scores = UserCreditFilesM.get_sum_scores(user_id);

		if (0 <= sum_scores && sum_scores <= 89) {
			credit_rating = "C";
		} else if (90 <= sum_scores && sum_scores <= 104) {
			credit_rating = "C+";
		} else if (105 < sum_scores && sum_scores <= 119) {
			credit_rating = "B";
		} else if (119 < sum_scores && sum_scores <= 134) {
			credit_rating = "B+";
		} else if (134 < sum_scores && sum_scores <= 149) {
			credit_rating = "A";
		} else if (149 < sum_scores) {
			credit_rating = "A+";
		}
		return credit_rating;
	}

	// 根据user_id得到信用等级 int型
	public static int get_int_credit_rating(int user_id) {
		int credit_rating = 0;
		int sum_scores = UserCreditFilesM.get_sum_scores(user_id);

		if (0 <= sum_scores && sum_scores <= 89) {
			credit_rating = 6;
		} else if (90 <= sum_scores && sum_scores <= 104) {
			credit_rating = 5;
		} else if (105 < sum_scores && sum_scores <= 119) {
			credit_rating = 4;
		} else if (119 < sum_scores && sum_scores <= 134) {
			credit_rating = 3;
		} else if (134 < sum_scores && sum_scores <= 149) {
			credit_rating = 2;
		} else if (149 < sum_scores) {
			credit_rating = 1;
		}
		return credit_rating;
	}

	// 根据model得到信用等级 int型
	public static int get_int_credit_rating(UserCreditFilesM user_credit_files) {
		int credit_rating = 0;
		int user_id = user_credit_files.getInt("user_id");
		int sum_scores = UserCreditFilesM.get_sum_scores(user_id);

		if (0 <= sum_scores && sum_scores <= 89) {
			credit_rating = 6;
		} else if (90 <= sum_scores && sum_scores <= 104) {
			credit_rating = 5;
		} else if (105 < sum_scores && sum_scores <= 119) {
			credit_rating = 4;
		} else if (119 < sum_scores && sum_scores <= 134) {
			credit_rating = 3;
		} else if (134 < sum_scores && sum_scores <= 149) {
			credit_rating = 2;
		} else if (149 < sum_scores) {
			credit_rating = 1;
		}
		return credit_rating;
	}

	// 信用总分
	public static int get_sum_scores(int user_id) {
		UserCreditFilesM user_credit_files = UserCreditFilesM.dao.findById(user_id);

		int basic_info_credit_scores = user_credit_files.get("basic_info_credit_scores");// 个人基本信息信用分
		int pay_off_the_pen_number_scores = user_credit_files.getInt("pay_off_the_pen_number");// 还清笔数
		int overdue_num_scores = user_credit_files.getInt("overdue_num");// 逾期扣除分数（逾期笔数）
		int seriously_overdue_scores = user_credit_files.getInt("seriously_overdue") * 30;// 严重逾期扣除分数（严重逾期笔数*30）
		int upload_info_scores = UserAuthenticateUploadInfoM.getUploadSumScores(user_id);// 获得上传信息总分

		int sum_scores = upload_info_scores + pay_off_the_pen_number_scores + basic_info_credit_scores - overdue_num_scores - seriously_overdue_scores;

		return sum_scores;
	}

	// 信用总分
	public static int get_sum_scores(UserCreditFilesM user_credit_files) {
		int user_id = user_credit_files.getInt("user_id");
		int basic_info_credit_scores = user_credit_files.get("basic_info_credit_scores");// 个人基本信息信用分
		int pay_off_the_pen_number_scores = user_credit_files.getInt("pay_off_the_pen_number");// 还清笔数
		int overdue_num_scores = user_credit_files.getInt("overdue_num");// 逾期扣除分数（逾期笔数）
		int seriously_overdue_scores = user_credit_files.getInt("seriously_overdue") * 30;// 严重逾期扣除分数（严重逾期笔数*30）
		int upload_info_scores = UserAuthenticateUploadInfoM.getUploadSumScores(user_id);// 获得上传信息总分

		int sum_scores = upload_info_scores + pay_off_the_pen_number_scores + basic_info_credit_scores - overdue_num_scores - seriously_overdue_scores;

		return sum_scores;
	}
}

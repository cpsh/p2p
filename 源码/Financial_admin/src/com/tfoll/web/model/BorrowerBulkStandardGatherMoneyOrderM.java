package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "borrower_bulk_standard_gather_money_order", primaryKey = "id")
public class BorrowerBulkStandardGatherMoneyOrderM extends Model<BorrowerBulkStandardGatherMoneyOrderM> {
	public static BorrowerBulkStandardGatherMoneyOrderM dao = new BorrowerBulkStandardGatherMoneyOrderM();
}
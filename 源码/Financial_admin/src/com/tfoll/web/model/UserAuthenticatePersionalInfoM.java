package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_authenticate_persional_info", primaryKey = "user_id")
public class UserAuthenticatePersionalInfoM extends Model<UserAuthenticatePersionalInfoM> {
	public static UserAuthenticatePersionalInfoM dao = new UserAuthenticatePersionalInfoM();
}

package com.tfoll.web.domain;

import com.tfoll.web.model.AdminModuleM;

import java.util.List;

public class AdminContent {
	private AdminModuleM adminModuleM;
	private List<AdminModuleM> adminModuleMList;

	public AdminContent() {
	}

	public AdminContent(AdminModuleM adminModuleM, List<AdminModuleM> adminModuleMList) {
		this.adminModuleM = adminModuleM;
		this.adminModuleMList = adminModuleMList;
	}

	public AdminModuleM getAdminModuleM() {
		return adminModuleM;
	}

	public List<AdminModuleM> getAdminModuleMList() {
		return adminModuleMList;
	}

	public void setAdminModuleM(AdminModuleM adminModuleM) {
		this.adminModuleM = adminModuleM;
	}

	public void setAdminModuleMList(List<AdminModuleM> adminModuleMList) {
		this.adminModuleMList = adminModuleMList;
	}

}

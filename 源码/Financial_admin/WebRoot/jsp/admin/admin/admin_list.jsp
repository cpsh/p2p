<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<!-- 进行查询 -->
		<div class="container" style="margin-top: 12px;">
			<legend>
				管理员管理
			</legend>
			<form method="post"
				action="<%=__ROOT_PATH__%>/admin/admin/admin_list_query.html" class="form-inline">
				<table class="table table-bordered table-hover table-condensed"
					id="table">
					<tbody>
						<tr>
							<td>
								用户名
							</td>
							<td>
								<input type="text" id="" name="admin_name" class="form-control"  placeholder="用户名" value="${admin_name}"/>
							</td>
							<td>
								Email
							</td>
							<td>
								<input type="text" id="" name="email" class="form-control"  placeholder="email" value="${email}"/>
							</td>
							<td>
								QQ
							</td>
							<td>
								<input type="text" id="" name="QQ" class="form-control"  placeholder="QQ" value="${QQ}" />
							</td>
							<td>
								是否超级管理员
							</td>
							<td>
								<select name="agency_id">
									<c:choose>
										<c:when test="${agency_id=='-1'}">
											<option value="-1" selected="selected"></option>
										</c:when>
										<c:otherwise>
											<option value="-1"></option>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${agency_id=='0'}">
											<option value="0" selected="selected">
												是
											</option>
										</c:when>
										<c:otherwise>
											<option value="0">
												是
											</option>
										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test="${agency_id=='1'}">
											<option value="1" selected="selected">
												不是
											</option>
										</c:when>
										<c:otherwise>
											<option value="1">
												不是
											</option>
										</c:otherwise>
									</c:choose>
								</select>
							</td>
							<td>
								<input type="submit" value="查询" class="form-control" />
							</td>
						</tr>
					</tbody>
				</table>
			</form><!--
			${pageDiv}
			--><table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							用户名
						</th>
						<th>
							Email
						</th>
						<th>
							QQ
						</th>
						<th>
							是否超级管理员
						</th>
						<th>
							创建时间
						</th>
						<th>
							权限设置
						</th>
						<th>
							修改
						</th><!--
						<th>
							管理日志
						</th>
					--></tr>
				</thead>
				<tbody>
					<c:forEach items="${admin_list}" var="admin">
						<tr>
							
							<td>
								${admin.m.admin_name}
							</td>
							<td>
								${admin.m.email}
							</td>
							<td>
								${admin.m.QQ}
							</td>
							<td>
								<c:choose>
									<c:when test="${admin.m.agency_id==1}">不是</c:when>
									<c:when test="${admin.m.agency_id==0}">是</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</td>
							<td last_login="add_time">
								${admin.m.add_time}
							</td>
							<td>
								<a href="<%=__ROOT_PATH__ %>/admin/admin/select_power.html?admin_id=${admin.m.admin_id}">设置用户权限</a>
							</td>
							<td admin_id="admin_id">
								<!-- <input type="checkbox" name="is_check" id="edit" /> -->
								<a href="<%=__ROOT_PATH__ %>/admin/admin/admin_edit_index.html?admin_id=${admin.m.admin_id}&admin_name=${admin.m.admin_name}&email=${admin.m.email}&QQ=${admin.m.QQ}&agency_id=${admin.m.agency_id}">修改</a>
							</td>
							<!--<td>
								 <input type="checkbox" name="is_check" id="edit" /> 
								<a href="<%=__ROOT_PATH__ %>/admin/admin/admin_log_query.html?admin_id=${admin.m.admin_id}">管理日志</a>
							</td>
						--></tr>
					</c:forEach>
				</tbody>
			</table>
			<div style="float:right;">${pageDiv}</div>
		</div>
	</body>
</html>

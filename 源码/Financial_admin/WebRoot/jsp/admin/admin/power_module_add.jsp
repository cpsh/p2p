<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>添加模块界面</title>
		
		<link type="text/css" href="<%=__PUBLIC__%>/css/easydialog.css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
		
	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
		<form action="<%=__ROOT_PATH__ %>/admin/admin/admin_power_module_save.html" method="post" name="form1">
			<table class="table table-bordered table-hover table-condensed"
				id="table">
				<legend>
					添加权限模块
				</legend>
		
				<tr>
					<td>
						模&nbsp;&nbsp;块&nbsp;&nbsp;名:
					</td>
					<td >
						<input type="text" name="power_name" id="power_name" class="form-control" required="required"/>
					</td>
				</tr>
				
				<tr>
					<td>
						URL
					</td>
					<td >
						<input type="text" name="power_url" id="power_url" class="form-control" />
					</td>
				</tr>
				<tr>
					<td>
						父级权限
					</td>
					<td>
						<select name="power_parent_id" id="power_parent_id">
							<option value="0">---选择父级模块---</option>
							<c:forEach items="${adminPowerList}" var="power">
								<option value="${power.m.power_id}">${power.m.power_name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
						<td>
							权限描述
						</td>
						<td>
							<input type="text" name="power_desc" class="form-control" id="power_desc"/>
						</td>
					</tr>
				<tr>
					<td>
						权限级别
					</td>
					<td>
						<select id="power_grade" name="power_grade">
							<option value="0">---选择权限模块级别---</option>
							<option value="1">一级模块</option>
							<option value="2">二级模块</option>
						</select>
					</td>
				</tr>
				
				<tr>
					<td>
						
					</td>
					<td>
						<input type="submit" id="add" class="btn btn-default" value="增加" style="margin-right: 480px;" onclick="return doAddInfo()" />
						<input type="button" value="刷新页面" id="msg_show"
							class="btn btn-default" onclick="f5();" />
					</td>
				</tr>
				</tbody>
			</table>
			</form>
		</div>
	</body>
	<script type="text/javascript">
	/*
	* method:showMssage
    * param:message 
	* explain:显示对话信息
	*/
	function showMssage(message)
	{
		var btnFn = function(){
			  easyDialog.close();
			  };
	   var btnFs = function(){
		   window.location.reload();
	   };
	   /**
	   var btnTz = function(){
		   
		   	easyDialog.close();
		  
		   	window.location.href(message[3]);
	   }**/
		if(message[2]!=null){
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFs,
					 noFn:false
					}
			});	
			//实名认证成功以后 跳向安全设置页面
		}else{
			   easyDialog.open({
					container:{
					 header:message[0],
					 content:message[1],
					 yesFn:btnFn,
					 noFn:false
					}
			});	
		}
	}

	
			$(function(){
			$("#pwd_confirm").blur(function(){
				$("#pwd_info").text("");
				if($("#pwd").val() != $("#pwd_confirm").val()){
					$("#pwd_info").text("两次输入的密码不一致");
					return false;
				}
			});
		});
		
		function doAddInfo(){
			if($("#power_name").val() ==null || $("#power_name").val() == ""){
					//alert("请填写权限模块！");
					showMssage(["提示","请填写权限模块名！"]);
					return false;
			}else if($("#power_grade").val() == 0){
				//alert("请权限级别的！");
				showMssage(["提示","请权限级别的！"]);
				return false;
			}else{
				return true;
			}
		}
	</script>
</html>

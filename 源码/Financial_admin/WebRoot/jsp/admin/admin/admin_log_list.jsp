<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<!-- 进行查询 -->
		<div class="container" style="margin-top: 12px;">
			<legend>
				管理员管理日志
			</legend>
			<form method="post"
				action="<%=__ROOT_PATH__%>/admin/admin/admin_list_query.html" class="form-inline">
				<table class="table table-bordered table-hover table-condensed"
					id="table">
					<tbody><!--
			${pageDiv}
			--><table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							#
						</th>
						<th>
							用户名
						</th>
						<th>
							登录 IP
						</th>
						<th>
							操作内容
						</th>
						<th>
							日志时间
						</th>
					</tr>
				</thead>
				<tbody>
				<input type="reset" name="btn2" id="btn2" class="btn btn-default"
								value="返回" onclick="javascript:history.go(-1);">
					<tr><td><c:choose>  
 						<c:when test="${admin_log_list ==null}"><B><font color="red">该用户暂时没有记录</font></B> </c:when>
						<c:otherwise> </c:otherwise>
					</c:choose>
					</td></tr>
					<c:forEach items="${admin_log_list}" var="admin_log">
						<tr>
							<td></td>
							<td>
								${admin_log.r.admin_name}
							</td>
							<td>
								${admin_log.r.ip}
							</td>
							<td>
								${admin_log.r.info}
							</td>
							<td>
								${admin_log.r.add_time}
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div style="float:right;">${pageDiv}</div>
		</div>
	</body>
</html>

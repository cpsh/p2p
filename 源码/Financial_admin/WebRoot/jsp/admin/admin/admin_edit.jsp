<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员列表界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		<script type="text/javascript">
			$(function(){
			$("#confirm_pwd").blur(function(){
				$("#pwd_info").text("");
				if($("#pwd").val() !==null && $("#pwd").val() != $("#confirm_pwd").val()){
					$("#pwd_info").text("两次输入的密码不一致");
				}
			});
		});
		</script>
	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<form action="<%=__ROOT_PATH__%>/admin/admin/admin_edit.html"
				method="post">
				<table class="table table-bordered table-hover table-condensed"
					id="table">
					<legend>
						修改管理员信息
					</legend>
					<tr>
						<td width="200px">
							用&nbsp;&nbsp;户&nbsp;&nbsp;名:
						</td>
						<td>
							<input type="text" name="admin_name" id="admin_name"
								class="form-control" value="<%=request.getParameter("admin_name") %>" required="required" />
						</td>
					</tr>
					<tr>
						<td>
							新&nbsp;&nbsp;密&nbsp;&nbsp;码:
						</td>
						<td>
							<input type="password" name="pwd" id="pwd" class="form-control" required="required" />
							<span id="pwd_start" style="color:red"></span>
						</td>
						</td>
					</tr>
					<tr>
						<td>
							确认密码:
						</td>
						<td>
							<input type="password" name="confirm_pwd" id="confirm_pwd"
								class="form-control" />
								<span id="pwd_info" style="color:red"></span>
						</td>
						</td>
					</tr>
					<tr>
						<td>
							邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:
						</td>
						<td>
							<input type="text" name="email" id="email" class="form-control"
								value="<%=request.getParameter("email") %>" required="required" />
						</td>
					</tr>
					<tr>
						<td>
							Q&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Q:
						</td>
						<td>
							<input type="text" name="QQ" id="QQ" class="form-control"
								value="<%=request.getParameter("QQ") %>" />
						</td>
					</tr>
					<tr>
						<td>
							权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限:
						</td>
						<td>
							<input type="text" name="agency_id" id="agency_id"
								class="form-control" value="<%=request.getParameter("agency_id") %>" required="required" />
						</td>
					</tr>
					<tr>
						<td>
							操作密码:
						</td>
						<td>
							<input type="password" name="operation_code" class="form-control" id="operation_code" required="required" />
						</td>
					</tr>
					<tr>
						<td>
							<input type="hidden" name="admin_id" id="admin_id" class="form-control"
								value="<%=request.getParameter("admin_id") %>" />
								(0为超级管理员，默认为 1)
						</td>
						<td>
							<input type="submit" id="edit" class="btn btn-default" value="修改" onclick="return doUpdateinfo()"
								style="margin-right: 200px;" />
							<input type="reset" name="btn2" id="btn2" class="btn btn-default"
								value="取消" onclick="javascript:history.go(-1);">
						</td>
					</tr>
					</tbody>
				</table>
				<div style="float:right;height:40px;">${pageDiv}</div>
			</form>
		</div>
	</body>
	<script type="text/javascript">
		function doUpdateinfo(){
			if($("#pwd").val() ==null || $("#pwd").val() == ""){
					//$("#pwd_start").text("请设置新密码！");
					alert("请设置新密码！");
					return false;
				}
		}
	
	</script>
</html>

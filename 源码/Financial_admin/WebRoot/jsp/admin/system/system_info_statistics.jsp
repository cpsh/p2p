<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>人民币代码生成界面</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/Calendar2.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<legend>
				<strong>系统用户基本信息</strong>
			</legend>
			</br>
			<span>&nbsp;&nbsp;&nbsp;&nbsp;下面的信息为系统总数</span>
			<table class="table table-striped table-bordered table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							昨天买量(btc)
						</th>
						<th>
							昨天卖量(btc)
						</th>
						<th>
							昨天买量(ltc)
						</th>
						<th>
							昨天卖量(ltc)
						</th>
						<th>
							昨天买量(ctc)
						</th>
						<th>
							昨天卖量(ctc)
						</th>
						<th>
							人民币余额
						</th>
						<th>
							BTC余额
						</th>
						<th>
							LTC余额
						</th>
						<th>
							CTC余额
						</th>
						<th>
							昨天登录次数
						</th>
						
					</tr>
				</thead>
				
				<tbody>
						<tr>
							<td>
								฿${user_buy_btc.m.quantity}
							</td>
							<td>
								฿${user_sell_btc.m.quantity}
							</td>
							<td>
								Ł${user_buy_ltc.m.quantity}
							</td>
							<td>
								Ł${user_sell_ltc.m.quantity}
							</td>
							<td>
								₡${user_buy_ctc.m.quantity}
							</td>
							<td>
								₡${user_sell_ctc.m.quantity}
							</td>
							<td>
								￥${user_money_list.m.CNY_remainder}
							</td>
							
							<td>
								฿${user_money_list.m.BTC_remainder}
							</td>
							<td>
								Ł${user_money_list.m.LTC_remainder}
							</td>
							<td>
								₡${user_money_list.m.CTC_remainder}
							</td>
							<td>
								${login_times}
							</td>
						</tr>
				</tbody>
			</table>
		</div>
	</body>
</html>

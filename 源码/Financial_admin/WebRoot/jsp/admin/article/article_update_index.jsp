<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.tfoll.web.model.ArticleM"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员——添加文章界面</title>
		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
		<script type="text/javascript" charset="utf-8"
			src="<%=__PUBLIC__%>/ue/ueditor.config.js"></script>
		<script type="text/javascript" charset="utf-8"
			src="<%=__PUBLIC__%>/ue/ueditor.all.min.js">
		</script>


	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<form
				action="<%=__ROOT_PATH__%>/admin/news/article/article_update.html"
				method="post" onsubmit="setContent();">
				<input type="hidden" name="aid" value=${aid} />
				"
				<table class="table table-bordered table-hover table-condensed">
					<tr>
						<td style="text-align: right;"></td>
						<td>
							${tips}
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							文章标题:
						</td>
						<td>
							<input type="text" name="title" id="title" class="form-control"
								required="required" style="margin-bottom: 10px;"
								value="${title}" />
						</td>
					</tr>
					<tr>
						<td style="text-align:right;">是否推荐</td>
						<td>
							<select name="is_recommend" class="form-control" required="required">
								<c:if test="${is_recommend == 1}">
									<option value="0">不推荐</option>
									<option value="1" selected="selected">推荐</option>
								</c:if>
								<c:if test="${is_recommend ==0}">
								   <option value="0" selected="selected">不推荐</option>
									<option value="1">推荐</option>
								</c:if>
								
								
							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							分类:
						</td>
						<td>
							<select name="type_id" class="form-control" required="required">

								<c:forEach items="${article_type_list}" var="article_type">
									<c:choose>
										<c:when
											test="${type_id==article_type.m.id}">
											<option value="${article_type.m.id}" selected="selected">
												${article_type.m.name}
											</option>
										</c:when>
										<c:otherwise>
											<option value="${article_type.m.id}">
												${article_type.m.name}
											</option>
										</c:otherwise>

									</c:choose>
								</c:forEach>

							</select>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							文章作者:
						</td>
						<td>
							<input type="text" name="author" id="author" class="form-control"
								required="required" style="margin-bottom: 10px;"
								value="${author}" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							关键字：
						</td>
						<td>
							<input type="text" name="keyword" id="keyword"
								class="form-control" required="required"
								style="margin-bottom: 10px;" value="${keyword}" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							文章描述:
						</td>
						<td>
							<input type="text" name="description" class="form-control"
								required="required" style="margin-bottom: 10px;"
								value="${description}" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							文章内容:
						</td>


						<td>
							<script id="container" name="content" type="text/plain">${content}</script>
							<script type="text/javascript">	var ue = UE.getEditor('container');</script>

						</td>

					</tr>
					<tr>
						<td style="text-align: right;"></td>
						<td>
							<input type="submit" class="form-control" value="修改文章" />
						</td>
					</tr>
				</table>


			</form>
		</div>
	</body>
</html>

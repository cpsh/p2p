<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
		<link rel="stylesheet" href="<%=__PUBLIC__ %>/css/demo.css" type="text/css">
		<link rel="stylesheet" href="<%=__PUBLIC__ %>/css/zTreeStyle/zTreeStyle.css" type="text/css">
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery-1.4.4.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.ztree.excheck-3.5.js"></script>

		<style type="text/css">
				body {
					padding-bottom: 40px;
				}
			
			.sidebar-nav {
				padding: 9px 0;
			}
			
			@media ( max-width : 980px) { /* Enable use of floated navbar text */
				.navbar-text.pull-right {
					float: none;
					padding-left: 5px;
					padding-right: 5px;
				}
			}
		</style>
	</head>
	<SCRIPT type="text/javascript">
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			async:{
					enable:true,
					type:"post",
					url:"<%=__ROOT_PATH__%>/admin/admin/testGetParams.html"
				}
		};

		var zNodes =${str_2};
		
		var code;
		
		function setCheck() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			py = "p",
			sy = "s",
			pn = "p",
			sn = "s",
			type = { "Y":py + sy, "N":pn + sn};
			zTree.setting.check.chkboxType = type;
			showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
		}
		function showCode(str) {
			if (!code) code = $("#code");
			code.empty();
			code.append("<li>"+str+"</li>");
		}
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			setCheck();
			
		});

		function doUpdate(){
			var role_id = $("#role_id").val();
			var role_name = $("#role_name").val();
			var role_desc = $("#role_desc").val();
			var radio = $('input[name="status"]:checked').val();
			
			if(role_name == null || role_name.length <=0 || role_name.length ==""){
				alert("角色名称不能为空！");
				return;
			}
			if(role_desc == null || role_desc.length <=0 || role_desc.length ==""){
				alert("描述不能为空！");
				return;
			}
			
			var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
			var sNodes = treeObj.getCheckedNodes(true);
			var arr = [];
			
			if (sNodes.length > 0) {
				for(var i=0;i<sNodes.length;i++){
					var tId = sNodes[i].id;
					arr.push(tId);
				}
				$.ajax({
					url:'<%=__ROOT_PATH__ %>/admin/admin/save_or_update_role.html',
					type:'post',
					data:{"arr[]":arr,"role_id":role_id,"role_name":role_name,"role_desc":role_desc,"status":radio},
					cache:false,
					async:false,
					success:function(data){
						if(data != null){
							if(data == 0){
								alert("角色名称不能为空！");
							}else if(data == 1){
								alert("描述不能为空！");
							}else if(data == 2){
								alert("请选择权限模块！");
							}else if(data == 3){
								alert("保存成功！");
							}else if(data == 4){
								alert("保存失败！");
							}
						}
					}
					
				},"html");
				
			}else{
				alert("请选择权限模块！");
			}
		}
	</SCRIPT>
	<body>
		<form action="<%=__ROOT_PATH__ %>/admin/admin/save_or_update_role.html" method="post" class="definewidth m20">
			<table class="table table-bordered table-hover definewidth m10">
				<tr>
					
					<td width="10%" class="tableleft">
						角色名称
					</td>
					<td>
						<input type="text" name="role_name" id="role_name" value="${adminRoleM.m.role_name}" />
						<INPUT type="hidden" name="role_id" id="role_id" value="${adminRoleM.m.id}"/>
					</td>
				</tr>
				<tr>
					<td class="tableleft">描述</td>
					<td><input type="text" name="role_desc" id="role_desc" value="${adminRoleM.m.role_desc}"/></td>
				</tr>
				<tr>
					<td class="tableleft">
						状态
					</td>
					<td>
						<c:if test="${adminRoleM.m.is_effective == 0}">
							<input type="radio" name="status" value="1"  />
							启用
							<input type="radio" name="status" value="0"  checked="checked"/>
							禁用
						</c:if>
						<c:if test="${adminRoleM.m.is_effective == 1}">
							<input type="radio" name="status" value="1" checked="checked" />
							启用
							<input type="radio" name="status" value="0" />
							禁用
						</c:if>
						
					</td>
				</tr>
				<tr>
					<td class="tableleft">
						权限
					</td>
					<td>
						<div class="zTreeDemoBackground left">
							<ul id="treeDemo" class="ztree"></ul>
						</div>
					</td>
				</tr>
				<tr>
					<td class="tableleft"></td>
					<td>
						<button type="button" class="btn btn-primary" type="button" onclick="doUpdate();">
							保存
						</button>
						&nbsp;&nbsp;
						<button type="button" class="btn btn-success" name="backid"
							id="backid">
							返回列表
						</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
<script type="text/javascript">
    
    
</script>
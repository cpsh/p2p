<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>注册/实名/绑定手机人数统计</title>

		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<!--<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet"
			type="text/css" />
		--><link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
			
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/Calendar2.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
	</head>
	<body>
		<!-- 复制功能 -->
		<script type="text/javascript">
		$(function() {
			$('#copy_to_it')
					.zclip(
							{
								path : '<%=__PUBLIC__%>/zclip/js/ZeroClipboard.swf',
								copy : function() {
									//判断该表格只能有一个进行选择
									var index = 0;
									$(
											'#table tr [name=is_check]:checkbox:checked')
											.each(function() {
												index++;
											});
									if (index == 0) {
										return "没有需要复制的东西";
									} else {
										var index = 0;
										var all_msg = "";
										$('#table tr')
												.each(
														function() {
															if (index == 0) {
																index++;
															} else {
																var $tr = $(this);
																//检测是否勾选
																var ok = false;
																if ($tr
																		.is(':has([name=is_check]:checkbox:checked)')) {
																	ok = true;
																}
																if (ok) {
																	var $tds = $tr
																			.children('td');
																	var $length = $tds.length;
																	var code = "";
																	var pwd = "";
																	for ( var i = 0; i < $length; i++) {
																		var $_td = $($tds[i]);
																		if ($_td.attr("code")) {
																			all_msg=all_msg+"帐号是:"+$_td.text();
																		} else if ($_td.attr("pwd")) {
																			all_msg=all_msg+"密码是:"+$_td.text();
																		}
																	}
																}

															}

														});
										return all_msg;
									}

								},
								afterCopy : function() {
									$("<span id='msg'/>").insertAfter(
											$('#copy_to_it')).text('复制成功')
											.fadeOut(2000);
								}
							});
		});
	</script>
		<!-- 进行查询 -->

		<div class="container" style="margin-top: 12px;">
			<legend>
				平台总额统计情况
			</legend>
			<form method="post"
				action="<%=__ROOT_PATH__%>/admin/report_statistic/stat_platform_total_acount_Query.html"
				class="form-inline">
				<!-- 
					<lable>申请日期 </lable>
					<input type="date" name="add_time_start" id="add_time_start" onfocus="setday(this)" 
						class="form-control" placeholder="开始日期" value="${add_time_start}" />
					<input type="date" name="add_time_end" id="add_time_end" onfocus="setday(this)" 
						class="form-control" placeholder="结束日期" value="${add_time_end}" />
				-->
				交易账户:
				<button type="submit" class="btn btn-default">
					查询
				</button>
			</form>
		
			<table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							平台RMB总额
						</th>
						<th>
							平台BTC总额
						</th>
						<th>
							平台LTC总额
						</th>
						<th>
							平台CTC总额
						</th>
						<th>
							平台冻结RMB总额
						</th>
						<th>
							平台冻结BTC总额
						</th>
						<th>
							平台冻结LTC总额
						</th>
						<th>
							平台冻结CTC总额
						</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${recharge_recordList}" var="record">
						<tr>
							<td>
								${record.r.totalCNY_remainder}
							</td>
							
							<td>
								${record.r.totalBTC_remainder}
							</td>
							<td>
								${record.r.totalLTC_remainder}
							</td>
							<td>
								${record.r.totalCTC_remainder}
							</td>
							<td>
								${record.r.totalCNY}
							</td>
							<td>
								${record.r.totalBTC}
							</td>
							<td>
								${record.r.totalLTC}
							</td>
							<td>
								${record.r.totalCTC}
							</td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="container" style="margin-top: 12px;">
			<legend>
				平台总额统计情况
			</legend>
		
			<table class="table table-bordered table-hover table-condensed"
				id="table">
				<thead>
					<tr>
						<th>
							借贷账户RMB总额
						</th>
						<th>
							借贷账户BTC总额
						</th>
						<th>
							借贷账户LTC总额
						</th>
						<th>
							借贷账户CTC总额
						</th>
						<th>
							借入RMB总额
						</th>
						<th>
							借入BTC总额
						</th>
						<th>
							借入LTC总额
						</th>
						<th>
							借入CTC总额
						</th>
						<th>
							放出RMB总额
						</th>
						<th>
							放出BTC总额
						</th>
						<th>
							放出LTC总额
						</th>
						<th>
							放出CTC总额
						</th>
						
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${borrow_lend_recordList}" var="record">
						<tr>
							<td>
								${record.r.total_transfer_cny}
							</td>
							
							<td>
								${record.r.total_transfer_btc}
							</td>
							<td>
								${record.r.total_transfer_ltc}
							</td>
							<td>
								${record.r.total_transfer_ctc}
							</td>
							
							<td>
								${record.r.total_borrow_cny}
							</td>
							
							<td>
								${record.r.total_borrow_btc}
							</td>
							<td>
								${record.r.total_borrow_ltc}
							</td>
							<td>
								${record.r.total_borrow_ctc}
							</td>
							
							<td>
								${record.r.total_lend_cny}
							</td>
							
							<td>
								${record.r.total_lend_btc}
							</td>
							<td>
								${record.r.total_lend_ltc}
							</td>
							<td>
								${record.r.total_lend_ctc}
							</td>
						</tr>

					</c:forEach>
				</tbody>
			</table>
		</div>
	</body>
</html>
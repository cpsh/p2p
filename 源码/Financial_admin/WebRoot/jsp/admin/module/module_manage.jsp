<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__ %>/css/style.css" />
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/bootstrap.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__ %>/js/common.js"></script>

		<style type="text/css">
			body {
				padding-bottom: 40px;
			}
			
			.sidebar-nav {
				padding: 9px 0;
			}
			
			@media ( max-width : 980px) { /* Enable use of floated navbar text */
				.navbar-text.pull-right {
					float: none;
					padding-left: 5px;
					padding-right: 5px;
				}
			}
		</style>
	</head>

	<body>
		<form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/admin/admin/query_module_info.html"
			method="post">
			模块名称：
			<input type="text" name="module_name" id="module_name"
				class="abc input-default" placeholder="" value="">
				&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary">
					查询
				</button>
				&nbsp;&nbsp;
				<button type="button" class="btn btn-success" id="addnew" >
					新增模块
				</button>
		</form>
		<table class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr>
					<th>
						模块ID
					</th>
					<th>
						模块名称
					</th>
					<th>
						父级模块					
					</th>
					<th>
						URL
					</th>
					
					<th>
						状态
					</th>
					<th>
						描述
					</th>
					<th>
						管理操作
					</th>
				</tr>
			</thead>
			<tbody>					
				<c:forEach items="${admin_module_record_list}" var="record">
					<tr>
						<td>
							${record.r.id}
						</td>
						<td>
							${record.r.module_name}
						</td>
						<td>
							${record.r.parent_module_name}
						</td>
						<td>
							${record.r.module_url}
						</td>
						<td>
							<c:if test="${record.r.is_effective == 0}"><span style="color: red">无效</span></c:if>
							<c:if test="${record.r.is_effective == 1}"><span style="color: green">有效</span></c:if>
						</td>
						<td>
							${record.r.module_desc}
						</td>
						<td>
							<a href="<%=__ROOT_PATH__%>/admin/admin/module_detail_info.html?module_id=${record.r.id}">编辑</a> ||
							<a href="#" onclick="del(${record.r.id})">删除</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
		</div>
	</body>
</html>
<script>
    $(function () {
		$('#addnew').click(function(){
				window.location.href="<%=__ROOT_PATH__%>/admin/admin/goto_add_module_detail_page.html";
		 });
    });

	function del(id){
		if(confirm("确定要删除吗？")){
			var url = "<%=__ROOT_PATH__%>/admin/admin/delete_module_detail_info.html?module_id="+id;
			window.location.href=url;	
		}
	}
	
	
</script>
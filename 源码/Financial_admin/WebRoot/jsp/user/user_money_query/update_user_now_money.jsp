<%@page import="com.tfoll.trade.config.Constants"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/style.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=__PUBLIC__%>/css/datepicker.css" />
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/js/bootstrap-datepicker.js"></script>

		<style type="text/css">
body {
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

@media ( max-width : 980px) { /* Enable use of floated navbar text */
	.navbar-text.pull-right {
		float: none;
		padding-left: 5px;
		padding-right: 5px;
	}
}
</style>
	</head>

	<body style="text-align: center;">
		<div style="margin: 0 auto;">
			<br />
			真实姓名：
			<input type="text" onfocus="this.blur()" name="real_name"
				id="real_name" class="abc input-default" placeholder=""
				style="width: 150px" value="${user_and_money.r.real_name}">
			<br />
			<br />
			昵称：
			<input type="text" onfocus="this.blur()" name="nickname"
				id="nickname" class="abc input-default" placeholder=""
				style="width: 150px" value="${user_and_money.r.nickname}">
			<br />
			<br />

			邮箱：
			<input type="text" onfocus="this.blur()" name="email" id="email"
				class="abc input-default" placeholder="" style="width: 150px"
				value="${user_and_money.r.email}">
			<br />
			<br />
			身份证号：
			<input type="text" onfocus="this.blur()" name="user_identity"
				id="user_identity" class="abc input-default" placeholder=""
				style="width: 150px" value="${user_and_money.r.user_identity}">
			<br />
			<br />
			电话号码：
			<input type="text" onfocus="this.blur()" name="phone" id="phone"
				class="abc input-default" placeholder="" style="width: 150px"
				value="${user_and_money.r.phone}">
			<br />
			<br />
			可用余额：
			<input type="text" name="cny_can_used" id="cny_can_used"
				class="abc input-default" placeholder="" style="width: 150px"
				value="${user_and_money.r.cny_can_used}">
			<br />
			<br />
			冻结人民币：
			<input type="text" name="cny_freeze" id="cny_freeze"
				class="abc input-default" placeholder="" style="width: 150px"
				value="${user_and_money.r.cny_freeze}">
			<br />
			<br />
			<input type="button" onclick="update_user_money();"
				class="btn btn-primary" value="修改" />
		</div>
	</body>
</html>
<script>
	function update_user_money(){
		var user_identity = $("#user_identity").val();
			var cny_can_used = $("#cny_can_used").val();
			var cny_freeze = $("#cny_freeze").val();
			$.post(
				'<%=__ROOT_PATH__%>/user/user_money_query/update_user_money.html',
				{
					"user_identity" : user_identity,
					"cny_can_used" : cny_can_used,
					"cny_freeze" : cny_freeze
				},
				function(data){
					if(data == 1){
						alert("账户余额不能为空");
					}else if(data == 2){
						alert("可用余额不能为空");
					}else if(data == 3){
						alert("冻结资产不能为空");
					}else if(data ==4){
						alert("修改失败");
					}else{
						alert("修改成功");
					}
				},'html');
	}
</script>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
    <%@ include file="/jsp/index/public.jsp" %>
    
    	

     <hr/>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th >id</th>
	     		<th >电话</th>
	     		<th >添加时间</th>
	     		<th >处理</th>
	     	</tr>
     	</thead>
   	  	
     	<c:forEach items="${user_appointment_list}" var="user_appointment">
     		<tr id="${user_appointment.m.id}">
     			<td>${user_appointment.m.id}</td>
     			<td>${user_appointment.m.phone}</td>
     			<td>${user_appointment.m.add_time}</td>
     			<td><input  type="button" onclick="deal('${user_appointment.m.id}');" value="${user_appointment.m.is_deal== 0 ? '处理':'已处理' }"/></td>
     		</tr>
     	</c:forEach>

     </table>
     	<div style="float:right;">${pageDiv}</div>
     <script type="text/javascript">
     function deal(id){
     	$.ajax(
     	{   url:'<%= __ROOT_PATH__%>/user/user_appointment/deal_appointment.html',
     		type:'post',
     		cache:false,
     		async:false,
     		data:{"id":id},
     		success:function (data){
		     	if(data == "ok"){
		     		window.location.href="<%= __ROOT_PATH__%>/user/user_appointment/query_appointment.html";
		     		alert("操作成功");
		     	}else{
		     		alert("操作失败");
		     	}
   	 		}
   		},"html")
   	}
     </script>
  </body>
</html>

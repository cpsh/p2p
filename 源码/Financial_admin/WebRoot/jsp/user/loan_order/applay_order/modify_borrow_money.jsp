<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>修改用户借款金额</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">修改用户借款金额  年利率</span></h3>
     <hr/>
     <input type="hidden" id="borrow_order_id" value="${modify_info.r.borrow_order_id}"/>
    <table  align="center">
    	<tr>
    		<td>用户id：</td>
    		<td>${modify_info.r.user_id}</td>
    	</tr>
    	<tr>
    		<td>真实姓名：</td>
    		<td>${modify_info.r.real_name}</td>
    	</tr>
    	<tr>
    		<td>昵称：</td>
    		<td>${modify_info.r.nickname}</td>
    	</tr>
    	<tr>
    		<td>手机：</td>
    		<td>${modify_info.r.phone}</td>
    	</tr>
    	<tr>
    		<td>原来的借款金额：</td>
    		<td>${borrow_all_money}</td>
    	</tr>
    	<tr>
    		<td>借款金额修改为：</td>
    		<td><input type="text" value="${borrow_all_money}" name="modify_money" id="modify_money" required="required" onkeyup="this.value=this.value.replace(/\D/g,'')" /></td>
    	</tr>
    	<tr>
    		<td>原来的借款年利率：</td>
    		<td>${modify_info.r.annulized_rate_int}</td>
    	</tr>
    	<tr>
    		<td>借款年利率修改为：</td>
    		<td><input type="text" value="${modify_info.r.annulized_rate_int}" name="modify_interest_rate_int" id="modify_interest_rate_int" required="required" onkeyup="this.value=this.value.replace(/\D/g,'')" /></td>
    	</tr>
    	<tr>
    		<td colspan="2" align="center"><input type="button" name="" id="confirm" onclick="confirm_modify()" value="确认修改" class="btn btn-primary"/> &nbsp;&nbsp;
    		<input type="button" name="" value="返回" class="btn btn-primary" onclick="javascript:history.go(-1);"/></td>
    	</tr>
    </table>
  </body>
  <script type="text/javascript">

  	function confirm_modify(){
  	  	
  		var modify_money=$("#modify_money").val();

  		var modify_interest_rate_int = $("#modify_interest_rate_int").val();
  	  	
  		$.post('<%=__ROOT_PATH__%>/user/loan_order/modify_borrow_money.html',
				{
					"modify_money" : modify_money,
					"modify_interest_rate_int" : modify_interest_rate_int ,
					"borrow_order_id" : $("#borrow_order_id").val()
				},
				function(data) //回传函数
				{
					if(data=='1'){//修改成功
						alert('修改成功');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
					if(data=='0'){//修改失败
						alert('修改失败');
						window.location.href="<%=__ROOT_PATH__%>/user/loan_order/loan_order_list_index.html";
					}
				}, "html","application/x-www-form-urlencoded; charset=utf-8");//浏览器编码
  	}
  </script>
</html>

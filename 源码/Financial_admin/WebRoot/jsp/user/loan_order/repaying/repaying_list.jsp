<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
    <h3 align="center"><span style="color: blue">还款列表</span></h3>
    
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__ %>/user/loan_order/repaying/repaying_list_query.html" method="post">
     	姓名：
     	<input type="text" name="real_name" id="real_name" value="" class="abc input-default"  style="width: 100px"/>
		期限
		<select id="borrow_duration" name="borrow_duration" style="width:135px">
		
				<c:choose>
					<c:when test="${borrow_duration==-1}">
						<option value="-1" selected="selected">全部</option>
					</c:when>
					<c:otherwise>
					    <option value="-1">全部</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==3}">
						<option value="3" selected="selected">3月</option>
					</c:when>
					<c:otherwise><option value="3">3月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==6}">
						<option value="6" selected="selected">6月</option>
					</c:when>
					<c:otherwise><option value="6">6月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==9}">
						<option value="9" selected="selected">9月</option>
					</c:when>
					<c:otherwise><option value="9">9月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==12}">
						<option value="12" selected="selected">12月</option>
					</c:when>
					<c:otherwise><option value="12">12月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==18}">
						<option value="18" selected="selected">18月</option>
					</c:when>
					<c:otherwise><option value="18">18月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==24}">
						<option value="24" selected="selected">24月</option>
					</c:when>
					<c:otherwise><option value="24">24月</option></c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_duration==36}">
						<option value="36" selected="selected">36月</option>
					</c:when>
					<c:otherwise><option value="36">36月</option></c:otherwise>
				</c:choose>
				
     	</select>		
		年化利率
		<select id="annulized_rate_int" name="annulized_rate_int" style="width:135px">
				<c:choose>
					<c:when test="${annulized_rate_int==-1}">
						<option value="-1" selected="selected">全部</option>
					</c:when>
					<c:otherwise>
					    <option value="-1">全部</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==10}">
						<option value="10" selected="selected">10%</option>
					</c:when>
					<c:otherwise>
					    <option value="10">10%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==11}">
						<option value="11" selected="selected">11%</option>
					</c:when>
					<c:otherwise>
					    <option value="11">11%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==12}">
						<option value="12" selected="selected">12%</option>
					</c:when>
					<c:otherwise>
					    <option value="12">12%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==13}">
						<option value="13" selected="selected">13%</option>
					</c:when>
					<c:otherwise>
					    <option value="13">13%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==14}">
						<option value="14" selected="selected">14%</option>
					</c:when>
					<c:otherwise>
					    <option value="14">14%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==15}">
						<option value="15" selected="selected">15%</option>
					</c:when>
					<c:otherwise>
					    <option value="15">15%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==16}">
						<option value="16" selected="selected">16%</option>
					</c:when>
					<c:otherwise>
					    <option value="16">16%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==17}">
						<option value="17" selected="selected">17%</option>
					</c:when>
					<c:otherwise>
					    <option value="17">17%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==18}">
						<option value="18" selected="selected">18%</option>
					</c:when>
					<c:otherwise>
					    <option value="18">18%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==19}">
						<option value="19" selected="selected">19%</option>
					</c:when>
					<c:otherwise>
					    <option value="19">19%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==20}">
						<option value="20" selected="selected">20%</option>
					</c:when>
					<c:otherwise>
					    <option value="20">20%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==21}">
						<option value="21" selected="selected">21%</option>
					</c:when>
					<c:otherwise>
					    <option value="21">21%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==22}">
						<option value="22" selected="selected">22%</option>
					</c:when>
					<c:otherwise>
					    <option value="22">22%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==23}">
						<option value="23" selected="selected">23%</option>
					</c:when>
					<c:otherwise>
					    <option value="23">23%</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${annulized_rate_int==24}">
						<option value="24" selected="selected">24%</option>
					</c:when>
					<c:otherwise>
					    <option value="24">24%</option>
					</c:otherwise>
				</c:choose>
				
     	      </select>		
		金额
		<select id="borrow_all_money_level" name="borrow_all_money_level" style="width:135px">
		        <c:choose>
					<c:when test="${borrow_all_money_level==-1}">
						<option value="-1" selected="selected">全部</option>
					</c:when>
					<c:otherwise>
					    <option value="-1">全部</option>
					</c:otherwise>
				</c:choose>
				 <c:choose>
					<c:when test="${borrow_all_money_level==1}">
						<option value="1" selected="selected">1-5万</option>
					</c:when>
					<c:otherwise>
					    <option value="1">1-5万</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_all_money_level==2}">
						<option value="2" selected="selected">5-10万</option>
					</c:when>
					<c:otherwise>
					    <option value="2">5-10万</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_all_money_level==3}">
						<option value="3" selected="selected">10-20万</option>
					</c:when>
					<c:otherwise>
					    <option value="3">10-20万</option>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${borrow_all_money_level==4}">
						<option value="4" selected="selected">20-50万</option>
					</c:when>
					<c:otherwise>
					    <option value="4">20-50万</option>
					</c:otherwise>
				</c:choose>
     	       </select>

     	
     	还款状态
     	<select id="payment_state" name="payment_state" style="width:135px">
     	
		
		<c:choose>
			<c:when test="${payment_state==2}">
				<option value="2" selected="selected">还款中</option>
			</c:when>
			<c:otherwise>
				<option value="2">还款中</option>
			</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${payment_state==3}">
				<option value="3" selected="selected">还款成功</option>
			</c:when>
			<c:otherwise>
				<option value="3">还款成功</option>
			</c:otherwise>
		</c:choose>

				
     	</select>
     	
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
     </form>

     <hr/>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     	
	     		<th>筹款单id</th>
	     		<th>真实姓名</th>
	     		<th>借款申请单ID</th>
	     		<th>借款标题</th>
	     		<th>借款用途</th>
	     		<th>借款金额</th>
	     		<th>借款期限</th>
	     		<th>年化利率</th>
	     		<th>筹款完成时间</th>
	     		<th>月还本息</th>
	     		<th>加入人次</th>
	     		<th>信用等级</th>
	     		<th>查看还款详细</th>

	     	</tr>
     	</thead>
     	<c:forEach items="${gather_money_order_list}" var="gather_money_order">
     		<tr>

     		    <!--
     		    <th>真实姓名</th>
	     		<th>借款申请单ID</th>
     			-->
     			
     			<td id="user_id">${gather_money_order.r.id}</td>
     			<td id="user_id">${gather_money_order.r.real_name}</td>
     			<td id="user_id">${gather_money_order.r.apply_order_id}</td>
     			
     			<!--
	            <th>借款标题</th>
	     		<th>借款用途 </th>
	     		<th>借款金额</th>
	     		<th>借款期限</th>
	     		<th>年化利率</th>
     			-->
     			<td id="user_id">${gather_money_order.r.borrow_title}</td>
     			<td id="user_id">${gather_money_order.r.borrow_purpose}</td>
     			<td id="user_id">${gather_money_order.r.borrow_all_money}</td>
     			<td id="user_id">${gather_money_order.r.borrow_duration}</td>
     			<td id="user_id">${gather_money_order.r.annulized_rate}</td>
     			
     			<!--
	     		<th>筹款完成时间</th>
	     		<th>月还本息</th>
	     		<th>加入人次</th>
	     		<th>信用等级</th>
     			-->
     			<td id="user_id">${gather_money_order.r.finish_time}</td>
     			<td id="user_id">${gather_money_order.r.monthly_principal_and_interest}</td>
     			<td id="user_id">${gather_money_order.r.join_people_num}</td>
     			<td id="user_id">${gather_money_order.r.credit_rating}</td>
     			<td><a href="<%=__ROOT_PATH__%>/user/loan_order/repaying/repayment_detail.html?gather_order_id=${gather_money_order.r.id}">查看</a></td>   
			
     		</tr>
     	</c:forEach>
     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv}
	</div>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>债权持有列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">债权持有列表</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/loan_order/hold_credit_right_qurey.html" method="post">
     	真实姓名：
     	<input type="text" name="real_name" id="bid_name" value="${real_name}" class="abc input-default"  style="width: 100px"/>
     	债权id
     	<input type="text" name="debt_id" id="debt_id" value="${debt_id}" class="abc input-default"  style="width: 100px"/>
     	开始持有债权时间 ：从
     	<input type="text" name="hold_debt_start_time" id="hold_debt_start_time" style="width: 150px" value="${hold_debt_start_time}"  onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	到：
     	<input type="text" name="hold_debt_end_time" id="hold_debt_end_time" style="width: 150px" value="${hold_debt_end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>序号</th>
	     		<th>债权持有人id</th>
	     		<th>真实姓名</th>
	     		<th>联富金融昵称</th>
	     		<th>债权id</th>
	     		<th>持有金额</th>
	     		<th>持有份额</th>
	     		<th>转出金额</th>
	     		<th>转出份额</th>
	     		<th>是否全部转让</th>
	     		<th>开始持有时间</th>
	     		<th>详情</th>
	     	</tr>
     </thead>
     	
		<c:forEach items="${hold_credit_right_list}" var="hold_credit_right">
     		<tr>
     			
     			<td>${hold_credit_right.r.id}</td>
     			<td>${hold_credit_right.r.user_id}</td>
     			<td>${hold_credit_right.r.real_name}</td>
     			<td>${hold_credit_right.r.nickname}</td>
     			<td><a href="<%=__ROOT_PATH__%>/user/loan_order/repaying/repayment_detail.html?gather_order_id=${hold_credit_right.r.gather_money_order_id}">${hold_credit_right.r.gather_money_order_id}</a></td>
     			<td>${hold_credit_right.r.hold_money}</td>
     			<td>${hold_credit_right.r.hold_share}</td>
     			<td>${hold_credit_right.r.transfer_money}</td>
     			<td>${hold_credit_right.r.transfer_share}</td>
     			<td>${hold_credit_right.r.is_transfer_all}</td>
     			<td>${hold_credit_right.r.add_time}</td>
     			<td><a href="<%=__ROOT_PATH__%>/user/loan_order/credit_right_detail.html?debt_id=${hold_credit_right.r.gather_money_order_id}&user_id=${hold_credit_right.r.user_id}">查看</a></td>
     				
     		</tr>
     	</c:forEach>


     </table>
     <div style="text-align: center"><span style="text-align: center"><%--${tips}--%></span></div>
		<div style="float:right;">
		<%--
			${pageDiv }
		--%>
		</div>
  </body>
  <script type="text/javascript">
  	$(function (){
  	  $('#add_new_fix_bid').click(
  		 function(){
  			window.location.href="<%=__ROOT_PATH__%>/user/lfoll_invest/goto_lfoll_invest_config.html";
	 	 }
  		);
  	});
  </script>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
    <%@ include file="/jsp/index/public.jsp" %>
    
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/activity/activity_query.html" method="post">
     	用户姓名：
     	<input type="text" name="real_name" id="real_name" class="abc input-default" style="width: 100px" value="" />
    	电话号码：
     	<input type="text" name="phone" id="phone" style="width: 150px" value=""  class="Wdate" />
     	奖励类型：
     	<select name="type" id="type">
     	<option value="">不限</option>
     	<option value="1">成功推荐</option>
     	<option value="2">理财交易</option>
     	</select>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     </form>

     <hr/>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th >id</th>
	     		<th>真实姓名</th>
	     		<th >手机号</th>
	     		<th >交易金额</th>
	     		<th > 交易类型  </th> 
	     		<th >奖励金额</th>  
	     		<th > 奖励类型</th>
	     	</tr>
     	</thead>
   	  	
     	<c:forEach items="${activity_list}" var="a">
     		<tr >
     			<td>${a.r.user_id}</td>
     			<td>${a.r.real_name}</td>
     			<td>${a.r.phone}</td>
     			<td>${a.r.amount}</td>
     			<td>
     			<c:if test="${a.r.order_type==1}">散标凑集</c:if>
     			<c:if test="${a.r.order_type==2}">投资散标</c:if>
     			<c:if test="${a.r.order_type==3}">投资联福宝</c:if>
     			</td>
     			<td>${a.r.reward_amount}</td>
     			<td>
     			<c:if test="${a.r.type==1}">成功推荐</c:if>
     			<c:if test="${a.r.type==2}">理财交易</c:if>
     			</td>
     		</tr>
     	</c:forEach>

     </table>
     	<div style="float:right;">${pageDiv}</div>
     <script type="text/javascript">
     </script>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>添加关于类型</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">添加链接</span></h3>
    <hr/>
    <div style="text-align: center">
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/" method="post">
    <table class="table table-bordered table-hover ">
    	<tr>
    		<td class="tableleft">网站名称：</td>
	    	<td><input type="text" name="site_name" id="site_name" /></td>
	    	<td></td>
    	</tr>
    	<tr>
    		<td class="tableleft">链接类型：</td>
	    	<td>
	    		<select name="type" id="type" onchange="showmsgmethod(this.value);">
	    			<option value="0">--选择链接类型--</option>
	    			<option value="1" >--文字链接--</option>
	    			<option value="2">--图片链接--</option>
	    		</select>
	    	</td>
	    	<td></td>
    	</tr>
    	<tr>
    		<td class="tableleft">图片名称</td>
    		<td><input type="text" name="image_name" id="image_name"/></td>
    		<td>/image/xxx.jpg（支持jpg,png,gif；文字链接可以不填该项）</td>
    	</tr>
    	<tr>
    		<td class="tableleft">URL路径：</td>
	    	<td><input type="text" name="site_url" id="site_url"/></td>
    		<td style="width:724px;color: red"><span id="showmsg"></span></td>
    	</tr>
    	<tr>
    		<td class="tableleft">排序：</td>
    		<td><input type="text" name="sort" value="${sort}" readonly="readonly" id="sort"/></td>
    		<td></td>
    	</tr>
  		<tr>
    		<td class="tableleft">是否有效：</td>
   	 		<td>
   	 			<input type="radio" name="status" value="1" /> 启用
				<input type="radio" name="status" value="0" checked="checked"/> 禁用
   	 		</td>
   	 		<td></td>
    	</tr>
    	<tr>
    		<td class="tableleft"></td>
    		<td>
    			<button type="button" class="btn btn-success" id="add_about_us_type" onclick="save_friendship_conn();">保存</button>
   				<button type="button" class="btn btn-success" id="back" >返回</button>
   			</td>
   			<td></td>
    	</tr>
    	</table>
    </form>
    </div>
  </body>
</html>
<script type="text/javascript">

	$(function(){
		$("#back").click(function(){
			window.location.href="<%=__ROOT_PATH__%>/user/friendship_conn/goto_friendship_conn_manage.html";
		});
	});
	 function save_friendship_conn(){
		var site_name = $("#site_name").val();
		var type = $("#type").val();
		var site_url = $("#site_url").val();
		var image_name = $("#image_name").val();
		var is_effect = $("input:radio:checked").val();
		var  sort = $("#sort").val();
		if(site_name == null || site_name ==""){
			showMessage(["提示","类型名称不能为空"]);
			return;
		}
		if(type == 0){
			showMessage(["提示","请选择链接类型"]);
			return;
		}
		if(site_url == null || site_url ==""){
			showMessage(["提示","URL不能为空"]);
			return;
		}
		
		$.ajax({
			url:"<%=__ROOT_PATH__%>/user/friendship_conn/save_friendship_conn.html",
			type:"post",
			data:{"site_name":site_name,"type":type,"site_url":site_url,"is_effect":is_effect,"sort":sort,"image_name":image_name},
			success:function(data){
				if(data == 1){
					showMessage(["提示","链接名称不能为空"]);
					return;
				}else if(data == 2){
					showMessage(["提示","URL不能为空"]);
					return;
				}else if(data == 3){
					showMessage(["提示","添加成功"]);
					return;
				}else if(data ==4){
					showMessage(["提示","添加失败"]);
					return;
				}
			}
		});
	}

	function showmsgmethod(value){
		if(value == 1){
			$("#showmsg").html("");
			$("#showmsg").html("网站跳转链接 如 ： http://www.baidu.com");
		}
		if(value == 2){
			$("#showmsg").html("");
			$("#showmsg").html("网站跳转链接 如 ：http://www.baidu.com");
		}
	}
	
</script>
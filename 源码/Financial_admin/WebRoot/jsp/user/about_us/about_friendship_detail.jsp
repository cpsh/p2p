<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>关于详细信息</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
  </head>
  <body>
    <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">友情链接编辑修改页面</span></h3>
    <hr/>
    <div style="text-align: center">
	    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/" method="post">
	    <table class="table table-bordered table-hover ">
	    	<tr>
	    		<td class="tableleft">网站名称：</td>
		    	<td><input type="text" name="site_name" id="site_name" value="${about_friendship_connection.m.site_name }" /></td>
	    	</tr>
	    	<tr>
    			<td class="tableleft">链接类型：</td>
		    	<td>
		    		<select name="type" id="type" onchange="showmsgmethod(this.value);">
		    			<c:if test="${about_friendship_connection.m.type ==1}">
			    			<option value="1" selected="selected">--文字链接--</option>
			    			<option value="2">--图片链接--</option>
		    			</c:if>
		    			<c:if test="${about_friendship_connection.m.type ==2}">
			    			<option value="1">--文字链接--</option>
			    			<option value="2" selected="selected">--图片链接--</option>
		    			</c:if>
		    		</select>
		    	</td>
	    		<td></td>
    		</tr>
    		<tr>
    			<td class="tableleft">图片名称</td>
    			<td><input type="text" name="image_name" id="image_name" value="${about_friendship_connection.m.image_name}"/></td>
    			<td>/image/xxx.jpg（支持jpg,png,gif；文字链接可以不填该项）</td>
    		</tr>
	    	<tr>
	    		<td class="tableleft">URL路径：</td>
		    	<td><input type="text" name="site_url" id="site_url" value="${about_friendship_connection.m.site_url}"/></td>
	    	</tr>
	    	
	    	<tr>
	    		<td class="tableleft">排序：</td>
	    		<td><input type="text" name="sort" value="${about_friendship_connection.m.sort}" id="sort"/></td>
	    	</tr>
	  		<tr>
	    		<td class="tableleft">是否有效：</td>
	   	 		<td>
	   	 			<c:if test="${about_friendship_connection.m.is_effect == 1}">
	   	 				<input type="radio" name="status" value="1" checked="checked" /> 启用
	   	 				<input type="radio" name="status" value="0" /> 禁用
	   	 			</c:if>
	   	 			<c:if test="${about_friendship_connection.m.is_effect == 0}">
	   	 				<input type="radio" name="status" value="1" /> 启用
	   	 				<input type="radio" name="status" value="0" checked="checked"/> 禁用
	   	 			</c:if>
	   	 		</td>
	    	</tr>
	    	<tr>
	    		<td class="tableleft"></td>
	    		<td>
	    			<input type="hidden" value="${about_friendship_connection.m.id}" id="id"/>
	    			<button type="button" class="btn btn-success" id="add_about_us_type" onclick="update_friendship();">保存修改</button>
	   				<button type="button" class="btn btn-success" id="back" >返回</button>
	   			</td>
	    	</tr>
	    	</table>
	    </form>
    </div>
  </body>
</html>
<script type="text/javascript">

	$(function(){
		$("#back").click(function(){
			window.location.href="<%=__ROOT_PATH__%>/user/friendship_conn/goto_friendship_conn_manage.html";
		});
	});
	 function update_friendship(){
		 var id = $("#id").val();
		var site_name = $("#site_name").val();
		var type = $("#type").val();
		var site_url = $("#site_url").val();
		var image_name = $("#image_name").val();
		var sort = $("#sort").val();
		var is_effect = $("input:radio:checked").val();
		
		if(site_name == null || site_name ==""){
			showMessage(["提示","类型名称不能为空"]);
			return;
		}
		if(site_url == null || site_url ==""){
			showMessage(["提示","URL不能为空"]);
			return;
		}
		if(type == 0){
			showMessage(["提示","请选择链接类型"]);
			return;
		}
		$.ajax({
			url:"<%=__ROOT_PATH__%>/user/friendship_conn/save_friendship_conn.html",
			type:"post",
			data:{"id":id,"site_name":site_name,"type":type,"site_url":site_url,"is_effect":is_effect,"sort":sort,"image_name":image_name},
			success:function(data){
				if(data == 1){
					showMessage(["提示","链接名称不能为空"]);
					return;
				}else if(data == 2){
					showMessage(["提示","URL不能为空"]);
					return;
				}else if(data == 3){
					showMessage(["提示","添加成功"]);
					return;
				}else if(data ==4){
					showMessage(["提示","添加失败"]);
					return;
				}
			}
		});
	}
	
</script>

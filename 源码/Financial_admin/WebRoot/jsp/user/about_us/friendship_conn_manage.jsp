<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>关于我们管理 </title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <%@ include file="/jsp/index/public.jsp" %>
     <h3 align="center"><span style="color: blue">友情链接管理</span></h3>
     <hr/>
      <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/friendship_conn/find_all_friendship_conn.html" method="post">
     	网站名称：
     	<input type="text" name="site_name" id="site_name" class="abc input-default" value="" />
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	<a href="<%=__ROOT_PATH__%>/user/friendship_conn/add_friendship_conn_page.html"><button type="button" class="btn btn-success" id="add_about_us_type">添加友情链接</button></a>
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>类型名称</th>
	     		<th>URL</th>
	     		<th>链接类型</th>
	     		<th>顺序</th>
	     		<th>是否启用</th>
	     		<th>操作</th>
	     	</tr>
     	</thead>
     	<c:if test="${not empty about_friendship_connection_List }">
	     	<c:forEach items="${about_friendship_connection_List}" var="friendship">
	     		<tr>
	     			<td>${friendship.m.site_name}</td>
	     			<td>${friendship.m.site_url}</td>
	     			<td>
	     				<c:if test="${friendship.m.type == 1}">
	     					<span style="color: red">文字链接</span>
	     				</c:if>
	     				<c:if test="${friendship.m.type == 2}">
	     					<span style="color: green">图片链接</span>
	     				</c:if>
	     			</td>
	     			<td>${friendship.m.sort}</td>
	     			<td>
	     				<c:if test="${friendship.m.is_effect == 0}">
	     					<span style="color:red">无效</span>
	     				</c:if>
	     				<c:if test="${friendship.m.is_effect == 1}">
	     					<span style="color:green">有效</span>
	     				</c:if>
	     			</td>
	     			<td><a href="<%=__ROOT_PATH__%>/user/friendship_conn/get_friendship_detail.html?id=${friendship.m.id}">详情</a></td>
	     		</tr>
	     	</c:forEach>
     	</c:if>
     	<c:if test="${empty about_friendship_connection_List }">
     		<tr>
     			<td colspan="3">${tips}</td>
     		</tr>
     	</c:if>
     </table>
  </body>
</html>

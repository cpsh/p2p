<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>新闻管理</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <%@ include file="/jsp/index/public.jsp" %>
     <h3 align="center"><span style="color: blue">新闻管理</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/about_us/get_all_about_info_list.html" method="post">
     	类型名称：
     	<select name="type_id" >
     		<option value="0">---请选择类型---</option>
     		<c:forEach items="${about_type_list}" var="about_type">
     			<option value="${about_type.m.id}">${about_type.m.type_name}</option>
     		</c:forEach>
     	</select>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>信息标题</th>
	     		<th>发布作者</th>
	     		<th>关键字</th>
	     		<th>信息描述</th>
	     		<th>操作</th>
	     	</tr>
     	</thead>
     	<c:forEach items="${about_info_list}" var="about_info">
     		<tr>
     			<td>${about_info.m.info_title }</td>
     			<td>${about_info.m.author }</td>
     			<td>${about_info.m.key_word }</td>
     			<td>${about_info.m.description}</td>
     			<td>
     				<a href="<%=__ROOT_PATH__%>/user/about_us/get_about_info_detail.html?id=${about_info.m.id}">详细</a>||
     				<a href="javascript:void(0);" onclick="delete_about_info(${about_info.m.id});">删除</a>
     			</td>
     		</tr>
     	</c:forEach>
     </table>
  </body>
</html>
<script type="text/javascript">
			function delete_about_info(id){
				if(confirm("您确定要删除该条记录吗?")){
					$.ajax({
						url:"<%=__ROOT_PATH__%>/user/about_us/delete_about_info_by_id.html",
						type:"post",
						data:{"id":id},
						success:function(data){
							if(data == 1){
								showMessage(["提示","删除成功!"]);
								window.location.href="<%=__ROOT_PATH__%>/user/about_us/get_all_about_info_list.html";
							}else if(data == 2){
								showMessage(["提示","删除失败!"]);
							}
						}
					});
				}else{
					return;
				}
			}
</script>

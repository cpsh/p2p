<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>管理员——添加文章界面</title>
		<link href="<%=__PUBLIC__%>/bootstrap/css/bootstrap.min.css"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/jquery-1.10.2.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap.min.js"
			rel="stylesheet">
		<link href="<%=__PUBLIC__%>/bootstrap/js/bootstrap-dropdown.js"
			rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy this line! -->
		<!--[if lt IE 9]><script src="<%=__PUBLIC__%>bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
		<script type="text/javascript" src="<%=__PUBLIC__%>/js/js.js"></script>
		<script type="text/javascript"
			src="<%=__PUBLIC__%>/zclip/js/jquery.zclip.min.js"></script>
			
			
			
			
			
	<script type="text/javascript" charset="utf-8" src="<%=__PUBLIC__%>/ue/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="<%=__PUBLIC__%>/ue/ueditor.all.min.js"> </script>
			

	</head>
	<body>
		<div class="container" style="margin-top: 12px;">
			<script type="text/javascript">
			function setContent(){
				var content=UE.getEditor('editor').getContent();
				$("#content").val(content);
			}
			
		
			</script>
			<form action="<%=__ROOT_PATH__%>/user/about_us/save_article_info.html" method="post"  onsubmit="setContent();">
			<input type="hidden" name="id" value="${about_info.m.id}" />
				<table class="table table-bordered table-hover table-condensed">
					<tr>
						<td style="text-align:right;">提示：</td>
						<td><span style="color: red">${tips}</span></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章标题:</td>
						<td><input type="text" name="info_title" value="${about_info.m.info_title }" id="title" class="form-control" required="required" style=" margin-bottom:10px;"/></td>
					</tr>
					<%--<tr>
						<td style="text-align:right;">是否推荐</td>
						<td>
							<select name="is_recommend" class="form-control" required="required">
								<option value="0">不推荐</option>
								<option value="1">推荐</option>
							</select>
						</td>
					</tr>
					--%>
					<tr>
						<td style="text-align:right;">分类:</td>
						<td>
						<select name="type_id" class="form-control" required="required">
										<option value="0">---请选择分类---</option>
							<c:forEach items="${aboutTypeList}" var="aboutType" >
										<c:if test="${aboutType.m.id == about_info.m.type_id}">
											<option value="${aboutType.m.id}" selected="selected">${aboutType.m.type_name}</option>
										</c:if>
										<c:if test="${aboutType.m.id != about_info.m.type_id}">
											<option value="${aboutType.m.id}" >${aboutType.m.type_name}</option>
										</c:if>
							</c:forEach>
						</select>
				   </td>
					</tr>
					<tr>
						<td style="text-align:right;">文章作者:</td>
						<td><input type="text" name="author" value="${about_info.m.author }" id="author" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">关键字：</td>
						<td><input type="text" name="key_word" value="${about_info.m.key_word }" id="key_word" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章描述:</td>
						<td><input type="text" name="description" value="${about_info.m.description}" class="form-control" required="required"  style=" margin-bottom:10px;"/></td>
					</tr>
					<tr>
						<td style="text-align:right;">文章内容:</td>
						<script type="text/javascript">

						    //实例化编辑器
						    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
						var ue=UE.getEditor('editor');
						</script>
						
						<td>
							<script id="container" name="content" type="text/plain">${about_info.m.content}</script>
							<script type="text/javascript">	var ue = UE.getEditor('container');</script>
						</td>	
					</tr>
					<tr>
						<td style="text-align:right;"></td>
						<td><input type="submit" class="form-control" value="修改"/></td>
					</tr>
				</table>
				
				
			</form>
		</div>
	</body>
</html>

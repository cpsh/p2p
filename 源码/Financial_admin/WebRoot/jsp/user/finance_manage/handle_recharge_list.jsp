<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>客户提现申请列表</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
  	<%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">客户充值记录列表</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/userrecharge/user_recharge_query.html" method="post">
     	昵称：
     	<input type="text" name="nickname" id="nickname" value="${nickname}" class="abc input-default"  style="width: 100px"/>
     	姓名：
     	<input type="text" name="real_name" id="real_name" value="${real_name}" class="abc input-default"  style="width: 100px"/>
     	身份证号：
     	<input type="text" name="user_identity" id="user_identity" value="${user_identity}" class="abc input-default"  style="width: 100px"/>
     	充值日期：
			
		<input type="date" name="add_time_start" id="add_time_start" style="width: 130px" value="${add_time_start}" />
		
		<input type="date" name="add_time_end" id="add_time_end" style="width: 130px" value="${add_time_end}" />
			
     	充值类型：
     	<select id="type" name="type" style="width:135px">
     	
     			<option value="-1">全部</option>
     			<option value="1">充值</option>
	     		<option value="2">其他奖励</option>
     	</select>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>真实姓名</th>
	     		<th>身份证</th>
	     		<th>充值前金额</th>
	     		<th>充值金额</th>
	     		<th>充值时间</th>
	     		<th>实际充值金额</th>
	     		<th>充值费用</th>
	     	</tr>
     	</thead>

     	<c:forEach items="${user_recharge_list}" var="user_recharge">
     	
     		<tr>
     			<td>${user_recharge.r.real_name }</td>
     			<td>${user_recharge.r.user_identity }</td>
     			<td>${user_recharge.r.balance }</td>
     			<td>${user_recharge.r.amount }</td>
     			<td>${user_recharge.r.add_time }</td>
     			<td>${user_recharge.r.actual_amount }</td>
     			<td>${user_recharge.r.fee}</td>
     		</tr>
     	</c:forEach>

     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
	</div>
  </body>
  <script type="text/javascript">
  	$(function (){
	  	var type = ${type};
		$("#type option").each(function(){
			if($(this).val() == type){
				$(this).attr("selected",true); 
			}
		});
  	});
  </script>
</html>

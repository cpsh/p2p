<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>联富定投设置页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">联富宝系统批量配置</span></h3>
     <hr/>
    <table  align="center">
    	<tr>
    		<td>定投计划金额：(元)</td>
    		<td><input type="text" name="total_money" id="total_money" required="required"/></td>
    	</tr>
    	<tr>
    		<td>年化利率：(n%)</td>
    		<td><input type="text" name="annualized_rate" id="annualized_rate" required="required"/></td>
    	</tr>
    	<tr>
    		<td>起投金额：(元)</td>
    		<td><input type="text" name="least_money" id="least_money" required="required"/></td>
    	</tr>
    	<tr>
    		<td>配置系统开始时间（yyyy-MM-dd HH:mm:ss）：</td>
    		<td><input type="text" name="system_start_time" id="system_start_time" required="required" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>配置系统结束时间（yyyy-MM-dd HH:mm:ss）：</td>
    		<td><input type="text" name="system_end_time" id="system_end_time" required="required" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>定投之间的间隔天数（天）</td>
    		<td><input type="text" name="index_day" id="index_day" required="required"/></td>
    	</tr>
    	<tr>
    		<td>通告开始到预订开始（小时）</td>
    		<td><input type="text" name="from_notice_to_reservation_hours" id="from_notice_to_reservation_hours" required="required"/></td>
    	</tr>
    	<tr>
    		<td>预定开始到预定结束（小时）</td>
    		<td><input type="text" name="from_reservation_start_to_reservation_end" id="from_reservation_start_to_reservation_end" required="required"/></td>
    	</tr>
    	<tr>
    		<td>预定结束到支付截止（小时）</td>
    		<td><input type="text" name="from_reservation_end_to_paystop" id="from_reservation_end_to_paystop" required="required"/></td>
    	</tr>
    	<tr>
    		<td>支付结束到开放加入（小时）</td>
    		<td><input type="text" name="from_paystop_to_begain_order" id="from_paystop_to_begain_order" required="required"/></td>
    	</tr>
    	<tr>
    		<td>开放定投到开始锁定（小时）</td>
    		<td><input type="text" name="from_begain_order_to_lock_order" id="from_begain_order_to_lock_order" required="required"/></td>
    	</tr>
    	<tr>
    		<td>锁定开始到退出定投（月）</td>
    		<td><input type="text" name="from_lock_time_to_exit_time" id="from_lock_time_to_exit_time" required="required"/></td>
    	</tr>
    	<tr>
    		<td colspan="2" align="center"><input type="button" name="" id="save_config" value="保存" class="btn btn-primary"/> &nbsp;&nbsp;
    		<input type="reset" name="" value="重置" class="btn btn-primary"/></td>
    	</tr>
    </table>
  </body>
  <script type="text/javascript">
  	$(function (){
		$("#save_config").click(function (){
			var total_money = $("#total_money").val();

			var annualized_rate = $("#annualized_rate").val();
			var least_money = $("#least_money").val();
			var system_start_time = $("#system_start_time").val();
			var system_end_time = $("#system_end_time").val();
			var index_day = $("#index_day").val();
			var from_notice_to_reservation_hours = $("#from_notice_to_reservation_hours").val();
			var from_reservation_start_to_reservation_end = $("#from_reservation_start_to_reservation_end").val();
			var from_reservation_end_to_paystop = $("#from_reservation_end_to_paystop").val();
			var from_paystop_to_begain_order = $("#from_paystop_to_begain_order").val();
			var from_begain_order_to_lock_order = $("#from_begain_order_to_lock_order").val();
			var from_lock_time_to_exit_time = $("#from_lock_time_to_exit_time").val();
			
			$.ajax({
				url:'<%=__ROOT_PATH__%>/user/lfoll_invest/create_all_lfoll_invest_order.html',
				type:"post",
				cache:false,
				data:{
						"total_money":total_money,
					  	"annualized_rate":annualized_rate,
					  	"least_money":least_money,
					  	"system_start_time":system_start_time,
					   	"system_end_time":system_end_time,
					   	"index_day":index_day,
					   	"from_notice_to_reservation_hours":from_notice_to_reservation_hours,
					   	"from_reservation_start_to_reservation_end":from_reservation_start_to_reservation_end,
					   	"from_reservation_end_to_paystop":from_reservation_end_to_paystop,
					   	"from_paystop_to_begain_order":from_paystop_to_begain_order,
					   	"from_begain_order_to_lock_order":from_begain_order_to_lock_order,
					   	"from_lock_time_to_exit_time":from_lock_time_to_exit_time
					},
				success:function(data){
					if(data == 1){
						alert("发售总额金额 不能为空或为0");
					}else if(data == 2){
						alert("设置年利率不能为空或小于0");
					}else if(data == 3){
						alert("设置年利率不能为空或小于0");
					}else if(data == 4){
						alert("设置起投金额 必须是1000的倍数");
					}else if(data == 4){
						alert("设置系统开始时间不能不空");
					}else if(data == 5){
						alert("设置系统结束时间不能为空");
					}else if(data == 6){
						alert("设置定投之间间隔天数不能为空或不能为0");
					}else if(data == 7){
						alert("设置通告开始 到预订开始时间的小时数不能为空或不能为0");
					}else if(data == 8){
						alert("设置预定开始到预定结束的小时数不能为空或不能为0");
					}else if(data == 9){
						alert("设置从预定结束到支付截止的小时数");
					}else if(data == 10){
						alert("设置从支付结束到开放加入的小时数");
					}else if(data == 11){
						alert("设置从开放定投到锁定开始的小时数");
					}else if(data == 12){
						alert("设置从锁定开始到退出定投的 月数");
					}else if(data == 13){
						alert("设置成功");
					}else{
						alert("系统异常!");
					}
				}
			
			});
		});
  	});
  </script>
</html>

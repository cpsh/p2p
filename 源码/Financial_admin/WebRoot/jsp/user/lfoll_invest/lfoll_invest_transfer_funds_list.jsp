<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>    
    <title>联富定投设置页面</title>
  </head>
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">联富宝资金转移</span></h3>
    <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__ %>/user/lfoll_invest/transfer_funds/transfer_funds_query.html" method="post" style="text-align: center;">
		期限
		<select id="user_id" name="user_id" style="width:135px">
					<c:choose>
						<c:when test="${user_id==-1}"> <option value="-1" selected="selected">全部</option></c:when>
						<c:otherwise> <option value="-1">全部</option></c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when test="${user_id==1}"> <option value="1" selected="selected">3个月</option></c:when>
						<c:otherwise> <option value="1">3个月</option></c:otherwise>
					</c:choose>
					
					
					<c:choose>
						<c:when test="${user_id==2}"> <option value="2" selected="selected">6个月</option></c:when>
						<c:otherwise> <option value="2">6个月</option></c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when test="${user_id==3}"> <option value="3" selected="selected">12个月</option></c:when>
						<c:otherwise> <option value="3">12个月</option></c:otherwise>
					</c:choose>

     	</select>		
		时间
		<input type="date" name="start_time" value="${start_time}"/>到<input type="date" name="end_time" value="${end_time}"/>
     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     </form>
     <table
			class="table table-bordered table-hover definewidth m10;border-bottom:none;">
			<thead>
				<tr>
					<th align="center">
						ID
					</th>
					<th align="center">
						投标名
					</th>
					<th align="center">
						封闭期（月）
					</th>
					
					<th align="center">
						80%转移资金
					</th>
					<th align="center">
						80%转移时间
					</th>
					
					<th align="center">
						80%剩下转移资金
					</th>
					<th align="center">
						80%剩下转移时间
					</th>
					<th align="center">
						80%实际转移资金
					</th>
					
					<th align="center">
						All转移资金
					</th>
					<th align="center">
						All转移时间
					</th>
					<th align="center">
						状态
					</th>
				</tr>
			</thead>
			<tbody>
					<c:forEach items="${fix_bid_transfer_funds_to_user_account_list}" var="fix_bid_transfer_funds_to_user_account">
					
					<tr>
						<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.system_order_id}
					    </td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.bid_name}
					</td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.closed_period}
					</td>
					
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_before}
					</td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_before_time}
					</td>
					
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_after}
					</td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_after_time}
					</td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_after_add}
					</td>
					
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_all}
					</td>
					<td align="center">
						${fix_bid_transfer_funds_to_user_account.m.money_80_all_time}
					</td>
					<td align="center">
						<c:choose>
							<c:when test="${fix_bid_transfer_funds_to_user_account.m.transfer_state==0}">等待转移资金</c:when>
							<c:when test="${fix_bid_transfer_funds_to_user_account.m.transfer_state==1}">已经转移了80%</c:when>
							<c:when test="${fix_bid_transfer_funds_to_user_account.m.transfer_state==2}">剩下20%资金到位</c:when>
							<c:when test="${fix_bid_transfer_funds_to_user_account.m.transfer_state==3}">资金一次性到位</c:when>
							<c:otherwise>系统错误</c:otherwise>
						</c:choose>
					</td>
					</tr>
					</c:forEach>
					
			</tbody>
		</table>
     
  </body>
</html>

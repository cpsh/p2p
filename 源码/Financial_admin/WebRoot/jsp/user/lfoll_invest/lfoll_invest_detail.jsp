<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>联富定投设置页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
  <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">定投配置详细信息</span></h3>
     <hr/>
    <table  align="center" class="table table-bordered table-hover definewidth m10">
    		<input type="hidden" name="id" id="id" value="${fix_bid_system_order.m.id}"/>
    	<tr>
    		<td>联富定投名称</td>
    		<td><input type="text" name="bid_name" id="bid_name" value="${fix_bid_system_order.m.bid_name}" required="required"/></td>
    	</tr>
    	<tr>
    		<td>定投计划发售金额：(元)</td>
    		<td><input type="text" name="total_money" id="total_money" value="${fix_bid_system_order.m.total_money}" required="required"/></td>
    	</tr>
    	<tr>
    		<td>已售出金额：(元)</td>
    		<td><input type="text" name="sold_money" id="sold_money" value="${fix_bid_system_order.m.sold_money}" required="required"/></td>
    	</tr>
    	<tr>
    		<td>起投金额：(元)</td>
    		<td><input type="text" name="least_money" id="least_money" value="${fix_bid_system_order.m.least_money}" required="required"/></td>
    	</tr>
    	<tr>
    		<td>年化利率：(0.000)</td>
    		<td><input type="text" name="annualized_rate" id="annualized_rate" value="${fix_bid_system_order.m.annualized_rate}"  required="required"/></td>
    	</tr>
    	<tr>
    		<td>是否挂出定投：</td>
    		<td>
    			<select name="is_show" id="is_show">
    				<c:if test="${fix_bid_system_order.m.is_show == 0}">
    					<option value="0" selected="selected">否</option>
    					<option value="1" >是</option>
    				</c:if>
    				<c:if test="${fix_bid_system_order.m.is_show == 1}">
    					<option value="0" >否</option>
    					<option value="1" selected="selected">是</option>
    				</c:if>
    			</select>
    		</td>
    	</tr>
    	<tr>
    		<td>预告开始时间:</td>
    		<td><input type="text" name="advance_notice_start_time" id="advance_notice_start_time" value="${fix_bid_system_order.m.advance_notice_start_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>预定开始时间:</td>
    		<td><input type="text" name="reserve_start_time" id="reserve_start_time" value="${fix_bid_system_order.m.reserve_start_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>预定结束时间:</td>
    		<td><input type="text" name="reserve_end_time" id="reserve_end_time" value="${fix_bid_system_order.m.reserve_end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>支付截止时间:</td>
    		<td><input type="text" name="pay_end_time" id="pay_end_time" value="${fix_bid_system_order.m.pay_end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>投标开始时间:</td>
    		<td><input type="text" name="invite_start_time" id="invite_start_time" value="${fix_bid_system_order.m.invite_start_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>投标结束时间:</td>
    		<td><input type="text" name="invite_end_time" id="invite_end_time" value="${fix_bid_system_order.m.invite_end_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td>封闭期（月）:</td>
    		<td><input type="text" name="closed_period" id="closed_period" value="${fix_bid_system_order.m.closed_period}"/></td>
    	</tr>
    	<tr>
    		<td>退出定投时间:</td>
    		<td><input type="text" name="repayment_time" id="repayment_time" value="${fix_bid_system_order.m.repayment_time}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate"/></td>
    	</tr>
    	<tr>
    		<td colspan="2" align="center"><input type="button" name="" id="save_config" value="保存" class="btn btn-primary"/> &nbsp;&nbsp;
    		<input type="reset" name="" value="重置" class="btn btn-primary"/></td>
    	</tr>
    </table>
  </body>
  <script type="text/javascript">
  	$(function (){
		$("#save_config").click(function (){
			var id = $("#id").val();
			var bid_name = $("#bid_name").val();
			var total_money = $("#total_money").val();
			var sold_money = $("#sold_money").val();
			var least_money = $("#least_money").val();
			var annualized_rate = $("#annualized_rate").val();
			var is_show = $("#is_show").val();
			var advance_notice_start_time = $("#advance_notice_start_time").val();
			var reserve_start_time = $("#reserve_start_time").val();
			var reserve_end_time = $("#reserve_end_time").val();
			var pay_end_time = $("#pay_end_time").val();
			var invite_start_time = $("#invite_start_time").val();
			var invite_end_time = $("#invite_end_time").val();
			var closed_period = $("#closed_period").val();
			var repayment_time = $("#repayment_time").val();
			
			$.ajax({
				url:'<%=__ROOT_PATH__%>/user/lfoll_invest/update_lfoll_invest_info.html',
				type:"post",
				cache:false,
				data:{
						"id":id,
						"bid_name":bid_name,
					  	"total_money":total_money,
					  	"sold_money":sold_money,
					  	"least_money":least_money,
					   	"annualized_rate":annualized_rate,
					   	"is_show":is_show,
					   	"advance_notice_start_time":advance_notice_start_time,
					   	"reserve_start_time":reserve_start_time,
					   	"reserve_end_time":reserve_end_time,
					   	"pay_end_time":pay_end_time,
					   	"invite_start_time":invite_start_time,
					   	"invite_end_time":invite_end_time,
					   	"closed_period":closed_period,
					   	"repayment_time":repayment_time
					},
				success:function(data){
					if(data == 1){
						alert("发售总额金额 不能为空或为0");
					}else if(data == 2){
						alert("设置年利率不能为空或小于0");
					}else if(data == 3){
						alert("设置年利率不能为空或小于0");
					}else if(data == 4){
						alert("设置起投金额 必须是1000的倍数");
					}else if(data == 4){
						alert("设置系统开始时间不能不空");
					}else if(data == 5){
						alert("设置系统结束时间不能为空");
					}else if(data == 6){
						alert("设置定投之间间隔天数不能为空或不能为0");
					}else if(data == 7){
						alert("设置通告开始 到预订开始时间的小时数不能为空或不能为0");
					}else if(data == 8){
						alert("设置预定开始到预定结束的小时数不能为空或不能为0");
					}else if(data == 9){
						alert("设置从预定结束到支付截止的小时数");
					}else if(data == 10){
						alert("设置从支付结束到开放加入的小时数");
					}else if(data == 11){
						alert("设置从开放定投到锁定开始的小时数");
					}else if(data == 12){
						alert("设置从锁定开始到退出定投的 月数");
					}else if(data == 13){
						alert("修改成功");
					}else{
						alert("系统异常!");
					}
				}
			
			});
		});
  	});
  </script>
</html>

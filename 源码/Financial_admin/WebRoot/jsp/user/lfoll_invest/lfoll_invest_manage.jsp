<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>联富定投设置页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
    <h3 align="center"><span style="color: blue">联富宝管理设置</span></h3>
     <hr/>
     <form class="form-inline definewidth m20" action="<%=__ROOT_PATH__%>/user/lfoll_invest/find_lfoll_invest_query.html" method="post">
     	联富定投名称：
     	<input type="text" name="bid_name" id="bid_name" class="abc input-default" value="${bid_name}" />
     	
     	预告开始时间
     	<input type="date" name="advance_notice_start_time" id="advance_notice_start_time" class="abc input-default" value="${advance_notice_start_time}" />
     	
     	开放结束时间
     	<input type="date" name="invite_end_time" id="invite_end_time" class="abc input-default" value="${invite_end_time}" />

     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
		<a href="<%=__ROOT_PATH__%>/user/lfoll_invest/goto_lfoll_invest_config.html"><button type="button" class="btn btn-success" id="add_new_fix_bid">定投设置</button></a>
     </form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>定投名称</th>
	     		<th>总发售金额</th>
	     		<th>已售出金额</th>
	     		<th>剩余金额</th>
	     		<th>起投金额</th>
	     		<th>年化利率</th>
	     		<th>设置时间</th>
	     		<th>是否投出</th>
	     		<th>预定开始时间</th>
	     		<th>预定结束时间</th>
	     		<th>支付截止时间</th>
	     		<th>投标开始时间</th>
	     		<th>操作</th>
	     		<th>挂单</th>
	     	</tr>
     	</thead>
     	<c:forEach items="${fix_bid_system_order_list}" var="bid_order">
     		<tr>
     			<td>${bid_order.m.bid_name}</td>
     			<td>${bid_order.m.total_money}</td>
     			<td>${bid_order.m.sold_money}</td>
     			<td>${bid_order.m.total_money - bid_order.m.sold_money}</td>
     			<td>${bid_order.m.least_money} </td>
     			<td>${bid_order.m.annualized_rate} </td>
     			<td>${bid_order.m.add_time} </td>
     			<td>
     				<c:if test="${bid_order.m.is_show == 0}">
     					<span style="color:red">否</span>
     				</c:if>
     				<c:if test="${bid_order.m.is_show == 1}">
     					<span style="color:blue">是</span>
     				</c:if>
     			</td>
     			<td>${bid_order.m.reserve_start_time}</td>
     			<td>${bid_order.m.reserve_end_time}</td>
     			<td>${bid_order.m.pay_end_time}</td>
     			<td>${bid_order.m.invite_start_time}</td>
     			<td>	
     				<a href="<%=__ROOT_PATH__%>/user/lfoll_invest/get_lfoll_invest_info.html?id=${bid_order.m.id}">详细</a>
     			</td>
     			<td>
     				<c:if test="${bid_order.m.is_show == 0}">
     					<span style="color:red"><input type="button" class="btn btn-info" value="挂出" /></span>
     				</c:if>
     				<c:if test="${bid_order.m.is_show == 1}">
     					<span style="color:red"><input type="button" class="btn-warning" value="已挂出" /></span>
     				</c:if>
     			</td>
     		</tr>
     	</c:forEach>
     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
	</div>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    
    <title>联富定投设置页面</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
  </head>
  
  <body>
   <%@ include file="/jsp/index/public.jsp" %>
    <h3 align="center"><span style="color: blue">联富宝退出管理</span></h3>
     <hr/>
     <form class="form-inline definewidth m20"  action="<%=__ROOT_PATH__%>/user/lfoll_invest/lfoll_invest_reserve/find_fix_bid_exited.html" method="post">
     	姓名：
     	<input type="text" name="user_name" id="user_name" class="abc input_default" value="" style="width:100px"/>
     	联富定投名称：
     	<input type="text" name="bid_name" id="bid_name" class="abc input-default" value="" style="width: 100px"/>
     	
     	退出时间 ：从
     	<input type="text" name="exit_start_time" id="exit_start_time" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" value=""  style="width: 150px"/>
     	~到
     	<input type="text" name="exit_end_time" id="exit_end_time" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',qsEnabled:true})" class="Wdate" value="" style="width: 150px" />

     	<button type="submit" class="btn btn-primary">查询</button>&nbsp;&nbsp; 
     	
		<%--<a href="<%=__ROOT_PATH__%>/user/lfoll_invest/goto_lfoll_invest_config.html"><button type="button" class="btn btn-success" id="add_new_fix_bid">定投设置</button></a>--%>
	</form>
     <table class="table table-bordered table-hover definewidth m10" >
     	<thead>
	     	<tr>
	     		<th>定投名称</th>
	     		<th>真实姓名</th>
	     		<th>昵称</th>
	     		<th>加入金额</th>
	     		<th>年化利率</th>
	     		<th>已获收益</th>
	     		<th>退出费用</th>
	     		<th>锁定时间</th>
	     		<th>到期时间</th>
	     		<th>退出时间</th>
	     		<th>退出状态</th>
	     	</tr>
     	</thead>
     	<c:forEach items="${fix_bid_user_exit_list}" var="exit_fix_bid">
     		<tr>
     			<td>${exit_fix_bid.m.bid_name}</td>
     			<td>${exit_fix_bid.m.real_name}</td>
     			<td>${exit_fix_bid.m.nick_name}</td>
     			<td>${exit_fix_bid.m.bid_money}</td>
     			<td>${exit_fix_bid.m.annual_earning * 100}% </td>
     			<td>${exit_fix_bid.m.earned_incom} </td>
     			<td>${exit_fix_bid.m.exit_fee} </td>
     			
     			<td>${exit_fix_bid.m.lock_time}</td>
     			<td>${exit_fix_bid.m.exit_time}</td>
     			<td>${exit_fix_bid.m.user_exit_time}</td>
     			<td>
     				<c:if test="${exit_fix_bid.m.state == 1}">
     					<span class="btn btn-info">正常退出</span>
     				</c:if>
     				<c:if test="${exit_fix_bid.m.state == 2}">
     					<span class="btn-warning">提前退出</span>
     				</c:if>
     			</td>
     		</tr>
     	</c:forEach>
     </table>
     <div style="text-align: center"><span style="text-align: center">${tips}</span></div>
		<div style="float:right;">
			${pageDiv }
	</div>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/easydialog.css" />
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/ckform.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/js/common.js"></script>
    <script type="text/javascript" src="<%=__PUBLIC__%>/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=__PUBLIC__%>/js/easydialog.min.js"></script>
	<script type="text/javascript" >
			/*
			* method:showMssage
		    * param:message 
			* explain:显示对话信息showMessage(["提示","请输入有效的金额"]);
			*/
			function showMessage(message){
				var btnFn = function(){
					  easyDialog.close();
					  };
			   var btnFs = function(){
				   window.location.reload();
			   };
			   /**
			   var btnTz = function(){
				   
				   	easyDialog.close();
				  
				   	window.location.href(message[3]);
			   }**/
				if(message[2]!=null){
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFs,
							 noFn:false
							}
					});	
					//实名认证成功以后 跳向安全设置页面
				}else{
					   easyDialog.open({
							container:{
							 header:message[0],
							 content:message[1],
							 yesFn:btnFn,
							 noFn:false
							}
					});	
				}
			}
	</script>
	
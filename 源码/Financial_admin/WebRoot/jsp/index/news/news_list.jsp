﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="天富网，比特币交易，比特币交易网，莱特币交易，莱特币交易网，凯特币交易，凯特币交易网，买比特币，卖比特币，挖比特币，bitcoin，litecoin，btc，ltc，ctc，交易，交易所" />
		<meta name="description" content="Tfoll天富网是国内最大最专业的比特币、莱特币、凯特币交易平台。采用ssl、冷存储、gslb、分布式服务器等先进技术，确保每一笔交易都安全可靠，而且高效便捷。" />				
				<title>新闻资讯-天富(TFoll)网-国内最大最专业的虚拟币交易平台</title>
		<link href="<%=__PUBLIC__%>/css/css.css" rel="stylesheet">
		<link href="<%=__PUBLIC__%>/style/main.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__%>/css/lrtk.css" rel="stylesheet"
			type="text/css" />
		<link href="<%=__PUBLIC__%>/css/loginDialog.css" type="text/css"
			rel="stylesheet" />

	</head>
	<body style="font-family: '微软雅黑'">
		<!-- 顶部通用模板 Start -->
		<jsp:include page="../../index/user_top.jsp" />
		<!-- 顶部通用模板 END -->
		<div class="xinwen">
			<div class="xinwen_con">
				<div class="xinwen_list">
					<h2>
						<img src="<%=__PUBLIC__%>/images/edit.png" width="32" height="32" />
						新闻资讯
					</h2>
					<ul>
						<c:forEach items="${article_list_Other}" var="article">
							<li>
								<a
									href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${article.article_id}">${article.name}</a>
								<!--  <img src="<%=__PUBLIC__%>/images/xw_list.png" />-->
									
									<c:if test="${not empty article.pathAddress}">
										<img src="${article.pathAddress}" />
									</c:if>
									<c:if test="${empty article.pathAddress}">
										
									</c:if>
								<p >
									${article.content}
								</p>
								<span>发布时间：${article.add_time}</span>
								<!--  
								<span>发布时间：<fmt:formatDate value="${article.add_time}"
										pattern="yyyy-MM-dd HH:mm:ss" />
								</span>-->
							</li>
						</c:forEach>
					</ul>
					<h6>${pageDiv}</h6>
				</div>
				
				<div class="xinwen_con_right">
					<c:forEach var="article_list" items="${article_list_map}">
						<h2>
							${article_list.key}
						</h2>
						<ul>
							<c:forEach items="${article_list.value}" var="article">
								<li>
									<a
										href="<%=__ROOT_PATH__%>/index/news/news_detail.html?id=${article.m.article_id}">${article.m.name}</a>
								</li>
							</c:forEach>
						</ul>
					</c:forEach>
				</div>
				
			</div>
		</div>
		<!-- 底部通用模板 Start -->
		<jsp:include page="../../index/user_foot.jsp" />
		<!-- 底部通用模板 END -->
	</body>
</html>

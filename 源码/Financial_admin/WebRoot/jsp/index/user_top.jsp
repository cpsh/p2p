<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="org.jsoup.Connection.Request"%>
<%
	String path = request.getContextPath();
    String pagePath =  request.getRequestURI();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ ="https://tfoll-cdn.oss.aliyuncs.com"; 
	//String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
	String url=request.getRequestURI();
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://com.tfoll.web/lable/menu_show_tag"  prefix="tfoll-menu"%>

<!-- 头部 Start -->
<!-- 返回顶部 Start -->
<a id="gotop" href="#top" ></a>
<!-- 返回顶部结束 -->
<!-- 注册登录 Start -->
<div id="loginDiv1"></div>
<div id="loginDiv2" style="z-index: 10005;">
	<table width="620" border="0" cellpadding="3" cellspacing="1"
		class="login_div_style">
		<tr id="m_tr">
			<td class="dl">
				<p id="sever1" onclick="setTab('sever',1,2)" class="hover">
					登录
				</p>
				<p id="sever2" onclick="setTab('sever',2,2)">
					注册
				</p>
			</td>
			<td align="right" style="padding-right: 8px">
				<input type="button" value="ｘ" onClick="closeme()"
					style="background: none; border: 0; cursor: pointer;">
			</td>
		</tr>
		<tr>
			<td colspan="2" width="100%" bgcolor="#FFFFFF" height="250"
				style="padding: 15px 0 0 15px;">
				
				    <!-- 用户登陆 Start -->
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						id="con_sever_1">
						<tr>
							<td width="25%" height="40" align="right">
								邮箱：
							</td>
							<td width="75%" height="40">
								<input name="username" type="text" class="dl_kk" onkeyup="checkUserEmail('1','#F00');" id="m_u1" />
							</td>
						</tr>
						<tr>
							<td height="40" align="right">
								密码：
							</td>
							<td height="40">
								<input name="password" type="password" class="dl_kk"
									onkeyup="checkUserPass('1','#F00');" id="m_p1" onclick="checkUserEmail('1','#F00');" />
							</td>
						</tr>
						<tr>
							<td width="25%" height="40" align="right">
							</td>
							<td width="75%" height="40">
								<span id="checkText1" class="err_message"></span>
							</td>
						</tr>
						<tr>
							<td height="40">
								&nbsp;
							</td>
							<td height="40">
								<button onclick="doLogin('1')" class="dl_an" id="doLoginTFollTop">立即登录</button>
								&nbsp;&nbsp;
								<a href="<%=__ROOT_PATH__%>/index/show_4_update_index.html"
									style="color: #06C">忘记密码</a>
							</td>
							<td height="40">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td height="25">
								&nbsp;
							</td>
							<td height="25">
								还没有账号？
								<a href="Javascript:setTab('sever',2,2)"   class="hongse">立即注册</a>
							</td>
							<td height="25">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td height="40">
								&nbsp;
							</td>
							<td height="40">
								&nbsp;
							</td>
							<td height="40">
								&nbsp;
							</td>
						</tr>
					</table>
					<!-- 用户登陆 END -->
					
					<!-- 用户注册 Start -->
					<form method="post" onsubmit="return doRegisterUser();">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							id="con_sever_2" style="display: none">
							<tr>
								<td height="30" colspan="3" class="hongse">
									我们会在您注册成功之后发送一份确认邮件至您的邮箱，你可以选择确认绑定，用于方便找回密码。
								</td>
							</tr>
							<tr>
								<td width="25%" height="40" align="right">
									* 电子邮箱：
								</td>
								<td width="75%" height="40">
									<input name="username" id="username" type="text" class="dl_kk" onkeyup="checkInputUserEmail(false);" onclick="clearError()"/>
										<span id="r_msg_e" class="err_message"></span>
								</td>
							</tr>
							<tr>
								<td height="40" align="right">
									* 登录密码：
								</td>
								<td height="40">
									<input type="password" name="password" id="password" class="dl_kk"
										onkeyup="checkInputUserPass();" onclick="checkInputUserEmail(true);"/>
										<span id="r_msg_p" class="err_message"></span>
								</td>
							</tr>
							<tr>
								<td height="40" align="right">
									* 确认密码：
								</td>
								<td height="40">
									<input name="" type="password" class="dl_kk" name="password2"
										id="password2" onkeyup ="checkInputUserPass2();" onclick="checkInputUserPass();" />
										<span id="r_msg_p2" class="err_message"></span>
								</td>
							</tr>
							<tr>
								<td height="40">
									&nbsp;
								</td>
								<td height="40">
									<input type="button" class="dl_an" value="确认注册" id="reg_btn" 
										onclick="return doRegisterUser();" />
									<span id="msg"></span></span>&nbsp;&nbsp;已经注册？
									<a href="Javascript:setTab('sever',1,2);" style="color: #00a2c9">快速登录</a>
								</td>
								<td height="40">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td height="40">
									&nbsp;
								</td>
								<td height="40">
									<p>
										<input name="复选框组1" type="checkbox" id="复选框组1_0" value="复选框"
											style="float: left" checked="checked" />
									</p>
									&nbsp;&nbsp;已阅读并同意？
									<a href="#" style="color: #00a2c9">TFOLL网络服务条款</a>
								</td>
								<td height="40">
									&nbsp;
								</td>
							</tr>
							<tr>
							</tr> 							
						</table>
					</form>
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
							id="show_success" style="display: none">
							<!-- 用户注册成功 Start -->
					<div style="width:550px; height:250px;margin:0 auto;display:none;" id="show_success">
                    <h1 style=" height:33px; float:left"><img src="<%=__PUBLIC__%>/images/register_success.jpg" style="float:left; padding-top:50px;" /></h1>
                    <h2 style="width:450px; float:left; padding:30px 0 0 20px; height:30px;"><p style=" padding-bottom:15px;font-size:20px; font-family:'微软雅黑'; color:#4ea000;" id="show_success_message"></p><a href="<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/btc_buy_index.html" style="padding:5px 7px 5px 7px; font-size:14px;background:#f3ffe8; border:1px solid #d9ecc8; text-decoration:none; color:#4ea000">个人中心 >></a></h2>
                    </div>
                    <!-- 用户注册成功 END -->
					</table>
					<!-- 用户注册 END -->	
			</td>
		</tr>
	</table>
</div>
<!-- 注册登录 END --><!--
<link rel="stylesheet" type="text/css" href="<%=__PUBLIC__%>/css/default_blue.css"/>
<script language="javascript" src="<%=__PUBLIC__%>/js/jquery.Sonline.js"></script>
<script type="text/javascript" src="<%=__PUBLIC__%>/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
$(function(){
	$("body").Sonline({
		Position:"right";
		Top:250;
		Effect:false; 
		DefaultsOpen:false; 
		Qqlist:"812121242|客服01"; 
		
	});
})	
</script>-->

<!-- 顶部跟随 Banner Start -->
<div class="head_top">
	<div class="head_top2" style="width: 1000px; clear: both;">
	    <!-- 最新成交价格 Start -->
	    <div class="tfoll_now_price">
		<h1>
			最新成交价&nbsp;&nbsp;比特币BTC : ￥
			<input type="text"  class="head_top_lanse" value="${btc_price }" style="width: 60px; text-align: center" id="btc_max_price" onclick="javascript:location.href='<%=__ROOT_PATH__%>/user/trade_info/btc_trade/btc_info_buy_index.html';" readonly>
			&nbsp;&nbsp;莱特币LTC : ￥
			<input type="text" class="head_top_lanse"  value="${ltc_price}" style="width: 60px; text-align: center" id="ltc_max_price" onclick="javascript:location.href='<%=__ROOT_PATH__%>/user/trade_info/ltc_trade/ltc_info_buy_index.html';"  readonly>
			&nbsp;&nbsp;凯特币CTC : ￥
			<input type="text"  class="head_top_lanse" value="${ctc_price }" style="width: 60px; text-align: center" id="ctc_max_price" onclick="javascript:location.href='<%=__ROOT_PATH__%>/user/trade_info/ctc_trade/ctc_info_buy_index.html';"  readonly>
			&nbsp;&nbsp;
		</h1>
		</div>
		<!-- 最新成交价格 END -->
		
		<!-- 用户账户Panel Start -->
		<div id="site-nav" style="display:none">
			<ul class="quick-menu">
				<li class="services menu-item last">
						<div class="menu"">
							<a class="menu-hd" href="#" target="_top">总资产：<span id="user_money">0</span><b></b>
							</a>
							<div class="menu-bd"
								style="width: 644px; height: 110px; padding: 15px;">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="17%" rowspan="4">
											交易账户
										</td>
										<td width="27%" height="30">
											交易账户净资产:
											<span class="lanse" id="userCanUseMoney">0.00</span>
										</td>
										<td height="30" colspan="3">
											交易账户总资产:
											<span class="lanse" id="userAllMoney">0.00</span>
										</td>
									</tr>
									<tr>
										<td height="30">
											可 用 CNY:
											<span class="lvse" id="has_CNY">0.00</span>
										</td>
										<td width="22%" height="30">
											BTC:
											<span class="lvse" id="has_BTC">0.00</span>
										</td>
										<td width="18%" height="30">
											LTC:
											<span class="lvse" id="has_LTC">0.00</span>
										</td>
										<td width="16%" height="30">
											CTC:
											<span class="lvse" id="has_CTC">0.00</span>
										</td>
									</tr>
									<tr>
										<td height="30">
											冻 结 CNY:
											<span class="huise" id="use_CNY">0.00</span>
										</td>
										<td height="30">
											BTC:
											<span class="huise" id="use_BTC">0.00</span>
										</td>
										<td height="30">
											LTC:
											<span class="huise" id="use_LTC">0.00</span>
										</td>
										<td height="30">
											CTC:
											<span class="huise" id="use_CTC">0.00</span>
										</td>
									</tr>				
								</table>
							</div>
						</div>
				</li>
			</ul>
		</div>
		<!-- 用户账号Panel END -->
		
		<!-- 用户登陆按钮 Start -->
		
		
		<div class="user_login_panel">
		<h2 id="userNoLoginDiv">
			<p>
				<input type="button" value="登录" onclick="openme('0')"
					class="head_top2_an" />
			</p>
			<p>
				<input type="button" value="注册" onclick="openme('1')"
					class="head_top2_an" />
			</p>
		</h2>
		<h3 id="userLoginDiv" style="display: none;">
			用户名：
			<c:if test="${sessionScope.user!=null}">${sessionScope.user.m.alias}</c:if>
			<a href="javascript:doLogout();">退出</a>
		</h3>
		</div>
		<!-- 用户登陆按钮 END -->
		
	</div>
</div>
<!-- 顶部跟随 Banner END -->

<!-- 导航 Start -->
<div class="head_nav" name="top">
	<h1>
		<img src="<%=__PUBLIC__%>/images/logo_03.jpg" alt="logo"  />
	</h1>
	<ul>
		<li>
			<a id="user-nav"
				href="<%=__ROOT_PATH__%>/user/trade_manage/btc_trade/btc_buy_index.html" 
				<tfoll-menu:if need_url="/jsp/user/user/user_info_show.jsp" url="<%=url %>">class="hover"</tfoll-menu:if>
				>用户中心</a>
		</li>
		<li>
			<a id="money-nav"
				href="<%=__ROOT_PATH__%>/user/acount/financial_record/income/financial_record_income_index.html" 
				<tfoll-menu:if need_url="/jsp/user/acount/coins/coins_add_list.jsp#/jsp/user/acount/coins/get_btc_coins_list_index.jsp#/jsp/user/acount/coins/get_ltc_coins_list_index.jsp#/jsp/user/acount/coins/get_ctc_coins_list_index.jsp#/jsp/user/acount/rmb_code/rmb_code_add_money.jsp#/jsp/user/acount/rmb_code/agent_rmb_code_add_money.jsp#/jsp/user/user/user_bank_info_add_index.jsp" url="<%=url%>">class="hover"</tfoll-menu:if>
				>财务中心</a>
		</li>
		<!--
		<li>
			<a id="money-nav" href="#">融资融币</a>
		</li>
		-->
		<li>
			<a id="news-nav" href="<%=__ROOT_PATH__%>/index/news/index.html"
			<tfoll-menu:if need_url="/jsp/index/news/news_info.jsp#/jsp/index/news/news_list.jsp" url="<%=url %>">class="hover"</tfoll-menu:if>
			>新闻资讯</a>
		</li>
		<li>
			<a id="trade-nav"
				href="<%=__ROOT_PATH__%>/user/trade_info/btc_trade/btc_info_buy_index.html"
				<tfoll-menu:if need_url="/jsp/user/trade_info/jiaoyi_lt_index.jsp#/jsp/user/trade_info/jiaoyi_ct_index.jsp#/jsp/user/index/index.jsp#/jsp/user/trade_manage/ltc_trade/ltc_jy.jsp#/jsp/user/trade_manage/ctc_trade/ctc_jy.jsp#/jsp/user/trade_manage/btc_trade/btc_jy2.jsp#/jsp/user/trade_manage/ltc_trade/ltc_jy2.jsp#/jsp/user/trade_manage/ctc_trade/ctc_jy2.jsp#/jsp/user/trade_manage/entrust.jsp" url="<%=url%>">class="hover"</tfoll-menu:if>
				>交易市场</a>
		</li>
		<li>
			<a id="index-nav" href="<%=__ROOT_PATH__%>/index/index.html" 
			<tfoll-menu:if need_url="/jsp/index/indexPage.jsp#123456#/jsp/index/indexPage.jsp" url="<%=url%>">class="hover"</tfoll-menu:if>
			>首页</a>
		</li>
	</ul>
</div>
<div style="height:10px; background:url(<%=__PUBLIC__%>/images/head_bj_bt_02.jpg)"></div>
<link rel="icon" href="<%=__ROOT_PATH__%>/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<%=__ROOT_PATH__%>/favicon.ico" type="image/x-icon"  />
<!-- 导航 END -->
<!-- 头部 END -->


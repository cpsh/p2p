<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String __ROOT_PATH__ = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
	String __PUBLIC__ = __ROOT_PATH__ + "/__PUBLIC__";//资源文件
%>

<%
	request.setCharacterEncoding("utf-8");
	response.setCharacterEncoding("utf-8");
%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>合同模板管理</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
	<style type="text/css">
	body{
		width:100%;
		height:100%;
		margin:0 auto;
		background-color:#cccccc;
	}
</style>
	
  </head>
 <body>
 </body>
  <script type="text/javascript" src="<%=__PUBLIC__ %>/js/loadpdfjs.js"></script>
	<script type="text/javascript">
	      window.onload = function (){
	        var mPDFHandler = new PDFHandler({ url: "<%=__PUBLIC__ %>/PDF/Java.pdf" }).embed();
	      };
	</script>
</html>

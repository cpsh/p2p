package com.tfoll.trade.config;

/**
 * 路由配置
 */
public class RouteSet {
	/**
	 * BaseViewPath是整个框架视图文件夹存放的文件夹
	 */
	public static String baseViewPath = "/view";

	public static String getBaseViewPath() {
		return baseViewPath;
	}

	/**
	 * 制定整个框架视图文件夹路径baseViewPath:like /XX
	 */
	public static void setBaseViewPath(String baseViewPath) {

		if (baseViewPath != null && !"".equals(baseViewPath)) {
			if (!RouteSet.baseViewPath.startsWith("/")) {
				throw new IllegalArgumentException("baseViewPath必须以/开头");
			}
			if (RouteSet.baseViewPath.endsWith("/")) {
				throw new IllegalArgumentException("baseViewPath不能以/结尾");
			}
			RouteSet.baseViewPath = baseViewPath;
		}

	}

}

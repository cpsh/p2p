package com.tfoll.trade.config;

/**
 * 开发者常量设置.注意框架配置常量一开始设定正确,开发者无需修改.在整个框架中统一使用静态变量来提高效率
 */
public class Constants {
	/**
	 * 编码为utf-8不能修改,这样便于统一开发的编码
	 */
	public static final String characterEncoding = "utf-8";
	/**
	 * 设置开发模式,对在开发过程中出现的项目运行信息进行调试
	 */
	public static boolean devMode = true;
	/**
	 * <pre>
	 * Action-URL扩展名处理:这里只是一个url限制,用来避免不是Action的访问,框架内部的Action跳转无需actionExtension.采用jspx的目的是为了保证整个系统url很普通
	 * </pre>
	 */
	public static String actionExtension = "";
	public static boolean isUseActionExtension = false;// 使用JSTL必须支持
	/**
	 * 框架COS上传组件临时上传的目录<br/>
	 * /E:/pzhu_dof/JFly/WebRoot/temp_upload_folder
	 */
	public static final String uploadTempFolderPath = Constants.class.getClassLoader().getResource("").getPath().replace("WEB-INF/classes/", "temp_upload_folder");

}

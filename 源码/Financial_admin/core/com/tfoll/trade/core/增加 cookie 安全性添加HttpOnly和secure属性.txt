[导读]当设置为true时，表示创建的 Cookie 会被以安全的形式向服务器传输，也就是只能在 HTTPS 连接中被浏览器传递到服务器端进行会话验证，如果是 HTTP 连接则不会传递该信息，所以不会被窃取到Cookie 的具体内容。
一、属性说明：
1 secure属性
当设置为true时，表示创建的 Cookie 会被以安全的形式向服务器传输，也就是只能在 HTTPS 连接中被浏览器传递到服务器端进行会话验证，如果是 HTTP 连接则不会传递该信息，所以不会被窃取到Cookie 的具体内容。
2 HttpOnly属性
如果在Cookie中设置了"HttpOnly"属性，那么通过程序(JS脚本、Applet等)将无法读取到Cookie信息，这样能有效的防止XSS攻击。
 
对于以上两个属性，
首先，secure属性是防止信息在传递的过程中被监听捕获后信息泄漏，HttpOnly属性的目的是防止程序获取cookie后进行攻击。
其次，GlassFish2.x支持的是servlet2.5，而servlet2.5不支持Session Cookie的"HttpOnly"属性。不过使用Filter做一定的处理可以简单的实现HttpOnly属性。GlashFish3.0(支持servlet3.0)默认开启Session Cookie的HttpOnly属性。
也就是说两个属性，并不能解决cookie在本机出现的信息泄漏的问题(FireFox的插件FireBug能直接看到cookie的相关信息)。
 
 
二、实例
项目架构环境：jsp+servlet+applet
 
1 添加HttpOnly和secure属性
根据之前的说明，GlassFish2不支持Session Cookie的HttpOnly属性，以及secure属性也需要自己进行设置，所以最后的处理方法是：在工程各添加一个Filter，对请求的入口页面(或者是请求后跳转到的第一个客户可见的页面，一般是登陆页面)，重新设置客户端的session属性。(response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + ";Path=/ccrl;secure;HttpOnly"); 可以看出，这句话的前提是这里只能使用Session  Cookie这唯一一个Cookie，不能使用其他Cookie在浏览器和服务器之间交互，否则会清除其他Cookie信息，如果一定要支持其他的Cookie，可以在Header下功夫)
 
2 修改程序中不兼容的代码(ccrl113)
（1）现象：在Session Cookie被设置为HttpOnly属性后，因为程序再也取不到客户端Session Cookie的内容，导致Applet发送URLConnection请求到服务器时，无法从浏览器中读取到sessionID，致使一些依赖于session中内容的URLConnection无法返回正确的结果。
         解决：在启动Applet时先将SessionID信息传入到applet中，然后在URLConnection发送请求时，重新设置Session Cookie信息。urlCon.setRequestProperty("Cookie", "JSESSIONID=" + ssid + ";Path=/ccrl113;secure;HttpOnly");
 
（2）现象：在Dynamic Analysis启动时，在jsp页面中存在使用URLConnection访问servlet的情况，可是在HTTPS的情况下，不允许jsp使用URLConnection访问servlet(从现象推论)。
         解决：将servlet中的内容重构抽取成工具类或是实体类供jsp页面使用。因为jsp页面和servlet都是服务器端，所以完全可以避免jsp页面通过URLConnection访问servlet。
 
可以使用专门的工具(fiddler2，burp)对安全性进行测试。
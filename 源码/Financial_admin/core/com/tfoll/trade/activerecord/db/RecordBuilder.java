package com.tfoll.trade.activerecord.db;

import com.tfoll.trade.activerecord.model.ModelBuilder;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecordBuilder {

	public static final List<Record> build(ResultSet rs) throws SQLException {
		List<Record> list = new ArrayList<Record>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		// 数据库字段顺序<以此为标准>和数组下标要保持一致
		String[] columnLabels = new String[columnCount + 1];
		int[] types = new int[columnCount + 1];
		buildColumnLabelsAndTypes(rsmd, columnLabels, types);
		while (rs.next()) {
			Record r = new Record();
			Map<String, Object> columnMap = r.getColumnMap();
			for (int i = 1; i <= columnCount; i++) {
				Object value;
				// 根据字段的类型的Enum类型取值
				if (types[i] < Types.BLOB) {
					value = rs.getObject(i);
				} else if (types[i] == Types.CLOB) {
					value = ModelBuilder.handleClob(rs.getClob(i));
				} else if (types[i] == Types.NCLOB) {
					value = ModelBuilder.handleClob(rs.getNClob(i));
				} else if (types[i] == Types.BLOB) {
					value = ModelBuilder.handleBlob(rs.getBlob(i));
				} else {
					value = rs.getObject(i);
				}
				// 向Record.columnMap里面设值
				columnMap.put(columnLabels[i], value);
			}
			list.add(r);
		}
		return list;
	}

	private static final void buildColumnLabelsAndTypes(ResultSetMetaData rsmd, String[] labelNames, int[] types) throws SQLException {
		int length = labelNames.length;
		if (length == 0) {
			throw new IllegalArgumentException("表字段个数为0");
		}
		for (int i = 1; i <= length - 1; i++) {
			labelNames[i] = rsmd.getColumnLabel(i);
			types[i] = rsmd.getColumnType(i);
		}
	}
}

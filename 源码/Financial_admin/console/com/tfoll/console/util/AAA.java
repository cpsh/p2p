/*package com.tfoll.console.util;

import java.math.BigDecimal;
import java.util.List;

import com.tfoll.console.util.ConsoleInit;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.db.Record;
import com.tfoll.web.model.FinanceAcountM;
import com.tfoll.web.model.FinanceBorrowM;
import com.tfoll.web.model.OrderBtcM;
import com.tfoll.web.model.OrderCtcM;
import com.tfoll.web.model.OrderLtcM;
import com.tfoll.web.model.TransactionRecordsBtcM;
import com.tfoll.web.model.TransactionRecordsCtcM;
import com.tfoll.web.model.TransactionRecordsLtcM;
import com.tfoll.web.model.UserRechargeCnyM;
import com.tfoll.web.model.UserWithDrawalsCnyM;
import com.tfoll.web.model.VirtualCurrencyRechargeM;
import com.tfoll.web.model.VirtualCurrencyWithdrawM;
import com.tfoll.web.util.Utils;

public class AAA {
	public static void main(String[] args) throws Exception {
		ConsoleInit.initTables();

 *//**
 * 充币操作
 */
/*
 *//**
 * 提币操作
 * 
 * virtual_currency_recharge 虚拟币充值表
 * virtual_currency_withdraw 虚拟币提现表
 */
/*
 String user_identity = "330482198705023915";//吴国峰
 //充值的RMB = 

 //查询 虚拟币 充值
 List<VirtualCurrencyRechargeM> chongzhiDummyList = VirtualCurrencyRechargeM.dao.find("SELECT * from virtual_currency_recharge where user_identity=?", new Object[] { user_identity });
 BigDecimal in_btc = new BigDecimal(0);//充值的BTC
 BigDecimal in_ltc = new BigDecimal(0);//充值的LTC
 BigDecimal in_ctc = new BigDecimal(0);//充值的CTC

 List<VirtualCurrencyWithdrawM> tixianDummyList = VirtualCurrencyWithdrawM.dao.find("select * from virtual_currency_withdraw t where t.user_identity = ?",new Object[] { user_identity });
 BigDecimal out_btc = new BigDecimal(0);//提现的BTC
 BigDecimal out_ltc = new BigDecimal(0);//提现的LTC
 BigDecimal out_ctc = new BigDecimal(0);//提现的CTC

 BigDecimal in_cny = new BigDecimal(0);//充值的CNY
 BigDecimal out_cny = new BigDecimal(0);//提现的CNY
 //如果 ，充值不为空
 if (Utils.isHasData(chongzhiDummyList)) {

 for (VirtualCurrencyRechargeM m : chongzhiDummyList) {
 int dummy_type = m.getInt("dummy_type");
 // `type` int(1) NOT NULL DEFAULT '0' COMMENT
 // '虚拟币充值或提现。0为充值，1为提现',
 int type = m.getInt("type");
 BigDecimal quantity = m.getBigDecimal("quantity");
 if (type == 0) {

 if (dummy_type == 1) {
 in_btc = in_btc.add(quantity);

 } else if (dummy_type == 2) {
 in_ltc = in_ltc.add(quantity);
 } else if (dummy_type == 3) {
 in_ctc = in_ctc.add(quantity);
 }

 } 
 }

 }
 //如果，提现不为空
 if(Utils.isHasData(tixianDummyList)){
 for(VirtualCurrencyWithdrawM m : tixianDummyList){
 int is_finish=m.getInt("is_finish");
 int dummy_type = m.getInt("dummy_type");
 BigDecimal quantity = m.getBigDecimal("quantity");
 if (is_finish==1) {
 if (dummy_type == 1) {
 out_btc = out_btc.subtract(quantity);

 } else if (dummy_type == 2) {
 out_ltc = out_ltc.subtract(quantity);
 } else if (dummy_type == 3) {
 out_ctc = out_ctc.subtract(quantity);
 }
 }
 }

 }

 List<UserRechargeCnyM> userRechargeCnyList = UserRechargeCnyM.dao.find("SELECT * from  user_recharge_cny where user_identity=?", new Object[] { user_identity });

 if (Utils.isHasData(userRechargeCnyList)) {
 for (UserRechargeCnyM m : userRechargeCnyList) {
 BigDecimal money = m.getBigDecimal("money");
 in_cny = in_cny.add(money);

 }

 }

 List<UserWithDrawalsCnyM> userWithDrawalsCnyList = UserWithDrawalsCnyM.dao.find("SELECT * from  user_withdrawals_cny where user_identity=?", new Object[] { user_identity });
 if (Utils.isHasData(userWithDrawalsCnyList)) {
 for (UserWithDrawalsCnyM m : userWithDrawalsCnyList) {
 int is_payment = m.getInt("is_payment");
 BigDecimal money = m.getBigDecimal("money");
 if (is_payment == 1) {
 out_cny = out_cny.subtract(money);

 }

 }

 }

 System.out.println(user_identity);
 System.out.println("充值：CNY in_cny  " + in_cny);
 System.out.println("充值：BTC in_btc  " + in_btc);
 System.out.println("充值：LTC in_ltc  " + in_ltc);
 System.out.println("充值：CTC in_ctc  " + in_ctc);

 System.out.println("提现：CNY out_cny  " + out_cny);
 System.out.println("提现：BTC out_btc  " + out_btc);
 System.out.println("提现：LTC out_ltc  " + out_ltc);
 System.out.println("提现：CTC out_ctc  " + out_ctc);

 BigDecimal xxxx_cny_out = new BigDecimal(0);
 BigDecimal xxxx_btc_out = new BigDecimal(0);
 BigDecimal xxxx_ltc_out = new BigDecimal(0);
 BigDecimal xxxx_ctc_out = new BigDecimal(0);

 BigDecimal xxxx_cny_in = new BigDecimal(0);//
 BigDecimal xxxx_btc_in = new BigDecimal(0);
 BigDecimal xxxx_ltc_in = new BigDecimal(0);
 BigDecimal xxxx_ctc_in = new BigDecimal(0);

 //查询BTC订单sql
 String SQL_BTC = "select * from order_btc t where t.user_identity = ?";
 //查询LTC订单sql
 String SQL_LTC = "select * from order_ltc t where t.user_identity = ?";
 //查询CTC订单sql
 String SQL_CTC = "select * from order_ctc t where t.user_identity = ?";
 *//**************************************************
 * BTC
 * ***************************************************/
/*
 *//**
 * 该用户买BTC用去CNY
 */
/*
 BigDecimal buy_btc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户买入BTC数量
 */
/*
 BigDecimal buy_btc_acount = new BigDecimal(0);

 *//**
 *  该用户卖BTC收入CNY
 */
/*
 BigDecimal sell_btc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户卖出BTC数量
 */
/*
 BigDecimal sell_btc_acount = new BigDecimal(0);
 //查询BTC订单
 List<OrderBtcM> orderBtcMList = OrderBtcM.dao.find(SQL_BTC, new Object[]{user_identity});
 if(Utils.isHasData(orderBtcMList)){
 for(OrderBtcM ob:orderBtcMList){
 long id = ob.getLong("id");
 //BTC买成交记录
 List<TransactionRecordsBtcM> transaction_BTC_buy_RecordList = TransactionRecordsBtcM.dao.find("select  * from transaction_records_btc t WHERE t.buy_id = ?", new Object[]{id});
 //				bargain_sum
 for(TransactionRecordsBtcM btcM:transaction_BTC_buy_RecordList){
 BigDecimal bargain_sum = btcM.getBigDecimal("bargain_sum");//每笔BTC买单的成交总额
 BigDecimal quantity = btcM.getBigDecimal("quantity");//每笔BTC买单的数量
 buy_btc_used_total_CNY = buy_btc_used_total_CNY.add(bargain_sum);//依次相加CNY
 buy_btc_acount = buy_btc_acount.add(quantity);//买入BTC依次相加

 }
 //BTC卖成交记录
 List<TransactionRecordsBtcM> transaction_BTC_sell_RecordList = TransactionRecordsBtcM.dao.find("select  * from transaction_records_btc t WHERE t.sell_id = ?", new Object[]{id});
 for(TransactionRecordsBtcM btcM:transaction_BTC_sell_RecordList){
 BigDecimal bargain_sum = btcM.getBigDecimal("bargain_sum");//每笔BTC卖单的成交总额
 BigDecimal quantity = btcM.getBigDecimal("quantity");//每笔BTC卖单的数量
 sell_btc_used_total_CNY = sell_btc_used_total_CNY.add(bargain_sum);//依次相加CNY
 sell_btc_acount = sell_btc_acount.add(quantity);//买入BTC依次相加

 }
 }
 }
 //BTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱(卖BTC的钱 - 买BTC的钱)
 BigDecimal  btc_recharege_CNY = sell_btc_used_total_CNY.subtract(buy_btc_used_total_CNY);
 //BTC交易数量，如果大于0，表示BTC数量增加，如果小于0，表示BTC数量减少，（买入BTC数量 - 卖出BTC数量）
 BigDecimal  btc_recharege_ACOUNT = buy_btc_acount.subtract(sell_btc_acount);
 System.out.println("-----------BTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱--------------  "+btc_recharege_CNY);
 System.out.println("--------BTC交易数量，如果大于0，表示BTC数量增加，如果小于0，表示BTC数量减少----------  "+btc_recharege_ACOUNT);
 *//**************************************************
 * LTC
 * ***************************************************/
/*
 *//**
 * 该用户买LTC用去CNY
 */
/*
 BigDecimal buy_ltc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户买入LTC数量
 */
/*
 BigDecimal buy_ltc_acount = new BigDecimal(0);

 *//**
 *  该用户卖LTC收入CNY
 */
/*
 BigDecimal sell_ltc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户卖出LTC数量
 */
/*
 BigDecimal sell_ltc_acount = new BigDecimal(0);

 //查询LTC订单
 List<OrderLtcM> orderLtcMList = OrderLtcM.dao.find(SQL_LTC, new Object[]{user_identity});
 if(Utils.isHasData(orderLtcMList)){
 for(OrderLtcM ob:orderLtcMList){
 long id = ob.getLong("id");
 //LTC买成交记录
 List<TransactionRecordsLtcM> transaction_LTC_buy_RecordList = TransactionRecordsLtcM.dao.find("select  * from transaction_records_ltc t WHERE t.buy_id = ?", new Object[]{id});
 for(TransactionRecordsLtcM ltcM:transaction_LTC_buy_RecordList){
 BigDecimal bargain_sum = ltcM.getBigDecimal("bargain_sum");//每笔LTC买单的成交总额
 BigDecimal quantity = ltcM.getBigDecimal("quantity");//每笔LTC买单的数量
 buy_ltc_used_total_CNY = buy_ltc_used_total_CNY.add(bargain_sum);//依次相加CNY
 buy_ltc_acount = buy_ltc_acount.add(quantity);//买入LTC依次相加

 }
 //LTC卖成交记录
 List<TransactionRecordsLtcM> transaction_LTC_sell_RecordList = TransactionRecordsLtcM.dao.find("select  * from transaction_records_ltc t WHERE t.sell_id = ?", new Object[]{id});
 for(TransactionRecordsLtcM ltcM:transaction_LTC_sell_RecordList){
 BigDecimal bargain_sum = ltcM.getBigDecimal("bargain_sum");//每笔LTC卖单的成交总额
 BigDecimal quantity = ltcM.getBigDecimal("quantity");//每笔LTC卖单的数量
 sell_ltc_used_total_CNY = sell_ltc_used_total_CNY.add(bargain_sum);//依次相加CNY
 sell_ltc_acount = sell_ltc_acount.add(quantity);//买入LTC依次相加

 }
 }
 }

 //LTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱(卖LTC的钱 - 买LTC的钱)
 BigDecimal  ltc_recharege_CNY = sell_ltc_used_total_CNY.subtract(buy_ltc_used_total_CNY);
 //LTC交易数量，如果大于0，表示LTC数量增加，如果小于0，表示LTC数量减少，（买入LTC数量 - 卖出LTC数量）
 BigDecimal  ltc_recharege_ACOUNT = buy_ltc_acount.subtract(sell_ltc_acount);
 System.out.println("-----------LTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱--------------  "+ltc_recharege_CNY);
 System.out.println("--------LTC交易数量，如果大于0，表示LTC数量增加，如果小于0，表示LTC数量减少----------  "+ltc_recharege_ACOUNT);

 *//**************************************************
 * CTC
 * ***************************************************/
/*
 *//**
 * 该用户买CTC用去CNY
 */
/*
 BigDecimal buy_ctc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户买入CTC数量
 */
/*
 BigDecimal buy_ctc_acount = new BigDecimal(0);

 *//**
 *  该用户卖CTC收入CNY
 */
/*
 BigDecimal sell_ctc_used_total_CNY = new BigDecimal(0);
 *//**
 *  该用户卖出CTC数量
 */
/*
 BigDecimal sell_ctc_acount = new BigDecimal(0);

 //查询CTC订单
 List<OrderCtcM> orderCtcMList = OrderCtcM.dao.find(SQL_CTC, new Object[]{user_identity});

 if(Utils.isHasData(orderCtcMList)){
 for(OrderCtcM ob:orderCtcMList){
 long id = ob.getLong("id");
 //LTC买成交记录
 List<TransactionRecordsCtcM> transaction_CTC_buy_RecordList = TransactionRecordsCtcM.dao.find("select  * from transaction_records_ctc t WHERE t.buy_id = ?", new Object[]{id});
 for(TransactionRecordsCtcM ctcM:transaction_CTC_buy_RecordList){
 BigDecimal bargain_sum = ctcM.getBigDecimal("bargain_sum");//每笔CTC买单的成交总额
 BigDecimal quantity = ctcM.getBigDecimal("quantity");//每笔CTC买单的数量
 buy_ctc_used_total_CNY = buy_ctc_used_total_CNY.add(bargain_sum);//依次相加CNY
 buy_ctc_acount = buy_ctc_acount.add(quantity);//买入CTC依次相加

 }
 //LTC卖成交记录
 List<TransactionRecordsCtcM> transaction_CTC_sell_RecordList = TransactionRecordsCtcM.dao.find("select  * from transaction_records_ctc t WHERE t.sell_id = ?", new Object[]{id});
 for(TransactionRecordsCtcM ctcM:transaction_CTC_sell_RecordList){
 BigDecimal bargain_sum = ctcM.getBigDecimal("bargain_sum");//每笔LTC卖单的成交总额
 BigDecimal quantity = ctcM.getBigDecimal("quantity");//每笔LTC卖单的数量
 sell_ctc_used_total_CNY = sell_ctc_used_total_CNY.add(bargain_sum);//依次相加CNY
 sell_ctc_acount = sell_ctc_acount.add(quantity);//买入LTC依次相加

 }
 }
 }
 //CTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱(卖LTC的钱 - 买LTC的钱)
 BigDecimal  ctc_recharege_CNY = sell_ctc_used_total_CNY.subtract(buy_ctc_used_total_CNY);
 //CTC交易数量，如果大于0，表示LTC数量增加，如果小于0，表示LTC数量减少，（买入LTC数量 - 卖出LTC数量）
 BigDecimal  ctc_recharege_ACOUNT = buy_ctc_acount.subtract(sell_ctc_acount);
 System.out.println("-----------CTC交易盈亏的金额，如果大于0，表示赚钱，如果小于0表示亏钱--------------  "+ctc_recharege_CNY);
 System.out.println("--------CTC交易数量，如果大于0，表示CTC数量增加，如果小于0，表示LTC数量减少----------  "+ctc_recharege_ACOUNT);


 //		buy_btc_used_total_CNY

 *//**************************************
 * 				借贷账户				  *
 *************************************/
/*
 String query_finaceAcount_info = "select  * FROM finance_acount t where t.user_identity = ? ";
 FinanceAcountM finaceAcount = FinanceAcountM.dao.findFirst(query_finaceAcount_info, new Object[]{user_identity});

 `transfer_cny` decimal(30,8) DEFAULT '0.00000000' COMMENT '放款账户RMB余额',
 `transfer_btc` decimal(30,8) DEFAULT '0.00000000' COMMENT '放款账户BTC余额',
 `transfer_ltc` decimal(30,8) DEFAULT '0.00000000' COMMENT '放款账户LTC余额',
 `transfer_ctc` decimal(30,8) DEFAULT '0.00000000' COMMENT '放款账户CTC余额',
 `lend_cny` decimal(30,8) DEFAULT '0.00000000' COMMENT '借出人民额',
 `lend_btc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借出BTC数量',
 `lend_ltc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借出LTC数量',
 `lend_ctc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借出CTC数量',
 `borrow_cny` decimal(30,8) DEFAULT '0.00000000' COMMENT '借入人民币数',
 `borrow_btc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借入BTC数量',
 `borrow_ltc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借入LTC数量',
 `borrow_ctc` decimal(30,8) DEFAULT '0.00000000' COMMENT '借入CTC数量',
 System.out.println("------放款账户RMB余额-----  "+finaceAcount.getBigDecimal("transfer_cny"));
 System.out.println("------放款账户BTC余额-----  "+finaceAcount.getBigDecimal("transfer_btc"));
 System.out.println("------放款账户LTC余额-----  "+finaceAcount.getBigDecimal("transfer_ltc"));
 System.out.println("------放款账户CTC余额-----  "+finaceAcount.getBigDecimal("transfer_ctc"));

 System.out.println("------借出人民额-----  "+finaceAcount.getBigDecimal("lend_cny"));
 System.out.println("------借出BTC数量-----  "+finaceAcount.getBigDecimal("lend_btc"));
 System.out.println("------借出LTC数量-----  "+finaceAcount.getBigDecimal("lend_ltc"));
 System.out.println("------借出CTC数量-----  "+finaceAcount.getBigDecimal("lend_ctc"));

 System.out.println("------借入人民额-----  "+finaceAcount.getBigDecimal("borrow_cny"));
 System.out.println("------借入BTC数量-----  "+finaceAcount.getBigDecimal("borrow_btc"));
 System.out.println("------借入LTC数量-----  "+finaceAcount.getBigDecimal("borrow_ltc"));
 System.out.println("------借入CTC数量-----  "+finaceAcount.getBigDecimal("borrow_ctc"));


 *//*************************
 * 	查询预约借款	 未还的款		**
 *************************/
/*
 * 
 * String query_finance_borrow =
 * "SELECT t1.user_identity AS user_identity, sum(t1.amount) AS borrw_money FROM finance_borrow t1 LEFT JOIN finance_borrow_log t2 ON t1.user_identity = t2.user_identity WHERE t1.user_identity = ? AND t2.back = 0 GROUP BY t1.user_identity"
 * ; Record finaceBorrowList = Db.findFirst(databaseKey, sql, params);
 * //(query_finance_borrow, new Object[]{user_identity});
 * 
 * if(Utils.isHasData(finaceBorrowList)){ for(FinanceBorrowM
 * finaceBorrowM:finaceBorrowList){
 * 
 * } }
 * 
 * 
 * } }
 */
package com.tfoll.web.timer.activity;

import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.core.Controller;
import com.tfoll.web.model.FixBidUserHoldM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model._____Activity_1_Recommend_UrlM;
import com.tfoll.web.model._____Activity___Fix_Bid_WaitingM;
import com.tfoll.web.model._____Activity___Fix_Bid_Waiting_CopyM;
import com.tfoll.web.model._____Activity___Reward_RecordM;
import com.tfoll.web.util.Utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class Timer______activity___fix_bid_waiting_deal extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer______activity___fix_bid_waiting_deal.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer______activity___fix_bid_waiting_deal.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer______activity___fix_bid_waiting_deal.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer______activity___fix_bid_waiting_deal.class.getName() + "开始");
				{// 真正的业务控制-不用while就可以实现循环检测功能
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer______activity___fix_bid_waiting_deal.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer______activity___fix_bid_waiting_deal.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		List<_____Activity___Fix_Bid_WaitingM> _____activity___fix_bid_waiting_list = _____Activity___Fix_Bid_WaitingM.dao.find("select * from _____activity___fix_bid_waiting");
		if (Utils.isHasData(_____activity___fix_bid_waiting_list)) {
			for (_____Activity___Fix_Bid_WaitingM _____activity___fix_bid_waiting : _____activity___fix_bid_waiting_list) {
				try {
					doSubTaskFor(_____activity___fix_bid_waiting);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * 第一个活动的时间改为：2014年12月12日到2015年12月12日 第二个活动的时间改为：2014年12月12日到2015年3月12日
	 */

	public static void doSubTaskFor(final _____Activity___Fix_Bid_WaitingM _____activity___fix_bid_waiting) throws Exception {
		final long hold_id = _____activity___fix_bid_waiting.getLong("hold_id");
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				FixBidUserHoldM fix_bid_user_hold = FixBidUserHoldM.dao.findById(hold_id);
				int bid_money = fix_bid_user_hold.getInt("bid_money");

				int user_id = _____activity___fix_bid_waiting.getInt("user_id");
				UserM user = UserM.dao.findFirst("select recommend_id from user_info where id = ?", user_id);
				int recommend_id = user.getInt("recommend_id");
				Date date = new Date();
				Date $2014_12_12 = Model.Date.parse("2014-12-12");
				Date $2015_12_13 = Model.Date.parse("2015-12-13");
				Date $2015_3_13 = Model.Date.parse("2015-3-13");

				if (date.getTime() >= $2014_12_12.getTime() && date.getTime() < $2015_12_13.getTime()) {
					if (recommend_id != 0 && bid_money >= 100) {
						// 推荐
						_____Activity_1_Recommend_UrlM _____activity_1_recommend_url = _____Activity_1_Recommend_UrlM.dao.findFirst("select user_id,user_id_map from _____activity_1_recommend_url where user_id = ? for update", recommend_id);
						String user_id_map = _____activity_1_recommend_url.getString("user_id_map");
						if (!Utils.isNotNullAndNotEmptyString(user_id_map)) {
							throw new NullPointerException("user_id_map is null with user_id = " + recommend_id);
						}

						Type type = new TypeToken<Map<String, String>>() {
						}.getType();
						Map<String, String> user_id_map_map = Controller.gson.fromJson(user_id_map, type);
						if (!user_id_map_map.containsKey(user_id + "")) {
							throw new NullPointerException("user_id_map_map not has key:" + user_id);
						}
						String value = user_id_map_map.get(user_id + "");
						if ("1".equals(value)) {
							// 不用管
						} else {

							int count = 0;
							// 需要添加理财记录，计算以前有几个推荐人进行理财了
							for (String var : user_id_map_map.values()) {
								if ("1".equals(var)) {
									count++;
								}
							}
							// 修改状态
							user_id_map_map.put(user_id + "", "1");
							BigDecimal reward_recommend = new BigDecimal("20");
							if ("1".equals("0")) {
								if (count < 5) {
									reward_recommend = new BigDecimal("30");
								} else if (count >= 5 && count < 10) {
									reward_recommend = new BigDecimal("40");
								} else if (count >= 10) {
									reward_recommend = new BigDecimal("50");
								} else {
									throw new RuntimeException("can not get here,hold_id:" + hold_id);
								}
							}
							/**
							 * 修改推荐表里面的推荐状态
							 */
							boolean is_ok_update______activity_1_recommend_url = _____activity_1_recommend_url.set("user_id_map", Controller.gson.toJson(user_id_map_map)).update();
							if (!is_ok_update______activity_1_recommend_url) {
								return false;
							}

							// 向推荐人打钱
							UserNowMoneyM user_now_money_recommend = UserNowMoneyM.get_user_current_money_for_update(recommend_id);
							BigDecimal cny_can_used_recommend = user_now_money_recommend.getBigDecimal("cny_can_used");
							BigDecimal cny_freeze_recommend = user_now_money_recommend.getBigDecimal("cny_freeze");
							boolean is_ok_uptate_user_now_money_recommend = user_now_money_recommend.set("cny_can_used", cny_can_used_recommend.add(reward_recommend)).update();
							if (!is_ok_uptate_user_now_money_recommend) {
								return false;
							}
							boolean is_ok_add_sys_expenses_records = SysExpensesRecordsM.add_sys_expenses_records(recommend_id, UserMoneyChangeRecordsM.Type_203, reward_recommend, "推荐人奖励,持有表ID:" + hold_id);
							boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(recommend_id, UserMoneyChangeRecordsM.Type_203, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_recommend, cny_can_used_recommend.add(cny_freeze_recommend).add(reward_recommend), "推荐人奖励,持有表ID:" + hold_id);
							boolean is_ok_add_user_transaction_records_by_type = UserTransactionRecordsM.add_user_transaction_records_by_type(recommend_id, UserTransactionRecordsM.Type_203, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_recommend, cny_can_used_recommend.add(cny_freeze_recommend).add(reward_recommend));
							if (!is_ok_add_sys_expenses_records) {
								return false;
							}
							if (!is_ok_add_user_money_change_records_by_type) {
								return false;
							}
							if (!is_ok_add_user_transaction_records_by_type) {
								return false;
							}
							/**
							 * 添加推荐人记录
							 */
							boolean is_ok_add______activity___reward_record = _____Activity___Reward_RecordM.add______activity___reward_record(user_id, 1, 3, hold_id, new BigDecimal(bid_money + ""), reward_recommend, recommend_id);
							if (!is_ok_add______activity___reward_record) {
								return false;
							}

						}

					}
				}
				// 理财人
				if (date.getTime() >= $2014_12_12.getTime() && date.getTime() < $2015_3_13.getTime()) {

					UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
					BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
					BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
					BigDecimal reward_financial = new BigDecimal(bid_money + "").multiply(new BigDecimal("0.01"));
					boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.add(reward_financial)).update();
					if (!is_ok_update_user_now_money) {
						return false;
					}
					boolean is_ok_add_sys_expenses_records = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_204, reward_financial, "理财奖励,持有表ID:" + hold_id);

					boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_204, cny_can_used.add(cny_freeze), new BigDecimal("0"), reward_financial, cny_can_used.add(cny_freeze).add(reward_financial), "理财奖励,持有表ID:" + hold_id);
					boolean is_ok_add_user_transaction_records_by_type = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id, UserTransactionRecordsM.Type_204, cny_can_used.add(cny_freeze), new BigDecimal("0"), reward_financial, cny_can_used.add(cny_freeze).add(reward_financial));
					if (!is_ok_add_sys_expenses_records) {
						return false;
					}
					if (!is_ok_add_user_money_change_records_by_type) {
						return false;
					}
					if (!is_ok_add_user_transaction_records_by_type) {
						return false;
					}
					/**
					 *给推荐人理财金的1%
					 */

					if (recommend_id != 0 && bid_money >= 100) {
						UserNowMoneyM user_now_money_recommend = UserNowMoneyM.get_user_current_money_for_update(recommend_id);
						BigDecimal cny_can_used_recommend = user_now_money_recommend.getBigDecimal("cny_can_used");
						BigDecimal cny_freeze_recommend = user_now_money_recommend.getBigDecimal("cny_freeze");

						boolean is_ok_update_user_now_money_recommend = user_now_money_recommend.set("cny_can_used", cny_can_used_recommend.add(reward_financial)).update();
						if (!is_ok_update_user_now_money_recommend) {
							return false;
						}

						boolean is_ok_add_sys_expenses_records_recommend = SysExpensesRecordsM.add_sys_expenses_records(recommend_id, UserMoneyChangeRecordsM.Type_205, reward_financial, "推荐理财奖励,持有表ID:" + hold_id);

						boolean is_ok_add_user_money_change_records_by_type_recommend = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(recommend_id, UserMoneyChangeRecordsM.Type_205, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_financial, cny_can_used_recommend.add(cny_freeze_recommend).add(reward_financial), "推荐理财奖励,持有表ID:" + hold_id);
						boolean is_ok_add_user_transaction_records_by_type_recommend = UserTransactionRecordsM.add_user_transaction_records_by_type(recommend_id, UserTransactionRecordsM.Type_205, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_financial, cny_can_used_recommend.add(cny_freeze_recommend).add(reward_financial));
						if (!is_ok_add_sys_expenses_records_recommend) {
							return false;
						}
						if (!is_ok_add_user_money_change_records_by_type_recommend) {
							return false;
						}
						if (!is_ok_add_user_transaction_records_by_type_recommend) {
							return false;
						}
					}

					/**
					 * 添加活动交易记录
					 */
					boolean is_ok_add______activity___reward_record = _____Activity___Reward_RecordM.add______activity___reward_record(user_id, 2, 3, hold_id, new BigDecimal(bid_money + ""), reward_financial, 0);
					if (!is_ok_add______activity___reward_record) {
						return false;
					}
					/**
					 * 被推荐的人理财 推荐人添加交易记录
					 */

					boolean is_ok_add______activity___reward_record_recommend = _____Activity___Reward_RecordM.add______activity___reward_record(recommend_id, 2, 3, hold_id, new BigDecimal(bid_money + ""), reward_financial, 0);
					if (!is_ok_add______activity___reward_record_recommend) {
						return false;
					}

				}
				boolean is_ok_add_____activity___fix_bid_waiting_copy = _____Activity___Fix_Bid_Waiting_CopyM.add_____activity___fix_bid_waiting_copy(_____activity___fix_bid_waiting);
				if (!is_ok_add_____activity___fix_bid_waiting_copy) {
					return false;
				}
				boolean is_ok_delete_fix_bid_waitting = _____activity___fix_bid_waiting.delete();
				if (!is_ok_delete_fix_bid_waitting) {
					return false;
				}
				return true;
			}
		});
		if (!is_ok) {
			logger.error("活动一二处理失败，hold_id:" + hold_id);
		}

		// 指定时间处理

	}

}

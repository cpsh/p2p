package com.tfoll.web.timer.activity;

import com.google.gson.reflect.TypeToken;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.trade.core.Controller;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.model._____Activity_1_Recommend_UrlM;
import com.tfoll.web.model._____Activity___Reward_RecordM;
import com.tfoll.web.model._____Activity___Standard_Order_Waiting_Of_BorrowerM;
import com.tfoll.web.model._____Activity___Standard_Order_Waiting_Of_Borrower_CopyM;
import com.tfoll.web.util.Utils;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class Timer______activity___standard_order_waiting_of_borrower_deal extends QuartzJobBean {

	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(_____Activity___Standard_Order_Waiting_Of_BorrowerM.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + _____Activity___Standard_Order_Waiting_Of_BorrowerM.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + _____Activity___Standard_Order_Waiting_Of_BorrowerM.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + _____Activity___Standard_Order_Waiting_Of_BorrowerM.class.getName() + "开始");
				{// 真正的业务控制-不用while就可以实现循环检测功能
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + _____Activity___Standard_Order_Waiting_Of_BorrowerM.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + _____Activity___Standard_Order_Waiting_Of_BorrowerM.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	/*
	 * 第一个活动的时间改为：2014年12月12日到2015年12月12日 第二个活动的时间改为：2014年12月12日到2015年3月12日
	 */
	public static void doTask() throws Exception {
		List<_____Activity___Standard_Order_Waiting_Of_BorrowerM> _____activity___standard_order_waiting_of_borrower_list = _____Activity___Standard_Order_Waiting_Of_BorrowerM.dao.find("select * from _____activity___standard_order_waiting_of_borrower");
		if (Utils.isHasData(_____activity___standard_order_waiting_of_borrower_list)) {
			for (_____Activity___Standard_Order_Waiting_Of_BorrowerM _____activity___standard_order_waiting_of_borrower : _____activity___standard_order_waiting_of_borrower_list) {
				try {
					doSubTaskFor(_____activity___standard_order_waiting_of_borrower);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}

	public static void doSubTaskFor(final _____Activity___Standard_Order_Waiting_Of_BorrowerM _____activity___standard_order_waiting_of_borrower) {
		final int user_id = _____activity___standard_order_waiting_of_borrower.getInt("user_id");
		final long gather_money_order_id = _____activity___standard_order_waiting_of_borrower.getLong("gather_money_order_id");
		final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findById(gather_money_order_id);
		final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
		// 理财
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				UserM user = UserM.dao.findFirst("select recommend_id from user_info where id = ?", user_id);
				int recommend_id = user.getInt("recommend_id");
				Date date = new Date();
				Date $2014_12_12 = Model.Date.parse("2014-12-12");
				Date $2015_12_13 = Model.Date.parse("2015-12-13");
				if (date.getTime() >= $2014_12_12.getTime() && date.getTime() < $2015_12_13.getTime()) {
					if (recommend_id != 0 && borrow_all_money.compareTo(new BigDecimal("100")) >= 0) {
						// 推荐
						_____Activity_1_Recommend_UrlM _____activity_1_recommend_url = _____Activity_1_Recommend_UrlM.dao.findFirst("select user_id,user_id_map from _____activity_1_recommend_url where user_id = ? for update", recommend_id);
						String user_id_map = _____activity_1_recommend_url.getString("user_id_map");
						if (!Utils.isNotNullAndNotEmptyString(user_id_map)) {
							throw new NullPointerException("user_id_map is null with user_id = " + recommend_id);
						}

						Type type = new TypeToken<Map<String, String>>() {
						}.getType();
						Map<String, String> user_id_map_map = Controller.gson.fromJson(user_id_map, type);
						if (!user_id_map_map.containsKey(user_id + "")) {
							throw new NullPointerException("user_id_map_map not has key:" + user_id);
						}
						String value = user_id_map_map.get(user_id + "");
						if ("1".equals(value)) {
							// 不用管
						} else {

							int count = 0;
							// 需要添加理财记录，计算以前有几个推荐人进行理财了
							for (String var : user_id_map_map.values()) {
								if ("1".equals(var)) {
									count++;
								}
							}
							// 修改状态
							user_id_map_map.put(user_id + "", "1");
							BigDecimal reward_recommend = new BigDecimal("20");
							if ("1".equals("0")) {
								if (count < 5) {
									reward_recommend = new BigDecimal("30");
								} else if (count >= 5 && count < 10) {
									reward_recommend = new BigDecimal("40");
								} else if (count >= 10) {
									reward_recommend = new BigDecimal("50");
								} else {
									throw new RuntimeException("can not get here,gather_money_order_id:" + gather_money_order_id);
								}
							}
							/**
							 * 修改推荐表里面的推荐状态
							 */
							boolean is_ok_update______activity_1_recommend_url = _____activity_1_recommend_url.set("user_id_map", Controller.gson.toJson(user_id_map_map)).update();
							if (!is_ok_update______activity_1_recommend_url) {
								return false;
							}
							// 向推荐人打钱
							UserNowMoneyM user_now_money_recommend = UserNowMoneyM.get_user_current_money_for_update(recommend_id);
							BigDecimal cny_can_used_recommend = user_now_money_recommend.getBigDecimal("cny_can_used");
							BigDecimal cny_freeze_recommend = user_now_money_recommend.getBigDecimal("cny_freeze");
							boolean is_ok_uptate_user_now_money_recommend = user_now_money_recommend.set("cny_can_used", cny_can_used_recommend.add(reward_recommend)).update();
							if (!is_ok_uptate_user_now_money_recommend) {
								return false;
							}
							boolean is_ok_add_sys_expenses_records = SysExpensesRecordsM.add_sys_expenses_records(recommend_id, UserMoneyChangeRecordsM.Type_203, reward_recommend, "推荐人奖励,凑集单ID:" + gather_money_order_id);
							boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(recommend_id, UserMoneyChangeRecordsM.Type_203, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_recommend, cny_can_used_recommend.add(cny_freeze_recommend).add(cny_freeze_recommend), "推荐人奖励,凑集单ID:" + gather_money_order_id);
							boolean is_ok_add_user_transaction_records_by_type = UserTransactionRecordsM.add_user_transaction_records_by_type(recommend_id, UserTransactionRecordsM.Type_203, cny_can_used_recommend.add(cny_freeze_recommend), new BigDecimal("0"), reward_recommend, cny_can_used_recommend.add(cny_freeze_recommend).add(reward_recommend));
							if (!is_ok_add_sys_expenses_records) {
								return false;
							}
							if (!is_ok_add_user_money_change_records_by_type) {
								return false;
							}
							if (!is_ok_add_user_transaction_records_by_type) {
								return false;
							}
							/**
							 * 添加推荐人记录
							 */
							boolean is_ok_add______activity___reward_record = _____Activity___Reward_RecordM.add______activity___reward_record(user_id, 1, 1, gather_money_order_id, borrow_all_money, reward_recommend, recommend_id);
							if (!is_ok_add______activity___reward_record) {
								return false;
							}
							boolean is_ok_add______activity___standard_order_waiting_of_borrower_copy = _____Activity___Standard_Order_Waiting_Of_Borrower_CopyM.add______activity___standard_order_waiting_of_borrower_copy(_____activity___standard_order_waiting_of_borrower);
							if (!is_ok_add______activity___standard_order_waiting_of_borrower_copy) {
								return false;
							}

						}
					}
				}
				boolean is_ok_delete______activity___standard_order_waiting_of_borrower = _____activity___standard_order_waiting_of_borrower.delete();
				if (!is_ok_delete______activity___standard_order_waiting_of_borrower) {
					return false;
				}
				return true;
			}

		});
		if (!is_ok) {
			logger.error("活动一二处理失败，hold_id:" + gather_money_order_id);
		}
	}

}

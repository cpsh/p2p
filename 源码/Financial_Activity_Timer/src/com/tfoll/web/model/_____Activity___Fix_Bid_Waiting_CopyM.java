package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.util.Map;

@TableBind(tableName = "_____activity___fix_bid_waiting_copy", primaryKey = "id")
public class _____Activity___Fix_Bid_Waiting_CopyM extends Model<_____Activity___Fix_Bid_Waiting_CopyM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Fix_Bid_Waiting_CopyM dao = new _____Activity___Fix_Bid_Waiting_CopyM();

	public static boolean add_____activity___fix_bid_waiting_copy(_____Activity___Fix_Bid_WaitingM _____activity___fix_bid_waiting) {
		_____Activity___Fix_Bid_Waiting_CopyM _____activity___fix_bid_waiting_copy = new _____Activity___Fix_Bid_Waiting_CopyM();
		/**
		 * 里面的表结构必须一致，也就是必须是同一条的数据才行
		 */
		Map<String, Object> map = _____activity___fix_bid_waiting.getM();
		map.remove("id");
		_____activity___fix_bid_waiting_copy.setMap(map);
		return _____activity___fix_bid_waiting_copy.save();
	}
}

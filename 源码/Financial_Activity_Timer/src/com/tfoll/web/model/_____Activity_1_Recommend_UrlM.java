package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "_____activity_1_recommend_url", primaryKey = "user_id")
public class _____Activity_1_Recommend_UrlM extends Model<_____Activity_1_Recommend_UrlM> implements Serializable {

	private static final long serialVersionUID = 1262241552051672915L;
	public static _____Activity_1_Recommend_UrlM dao = new _____Activity_1_Recommend_UrlM();
}

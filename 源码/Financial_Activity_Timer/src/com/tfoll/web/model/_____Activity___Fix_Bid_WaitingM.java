package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "_____activity___fix_bid_waiting", primaryKey = "id")
public class _____Activity___Fix_Bid_WaitingM extends Model<_____Activity___Fix_Bid_WaitingM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Fix_Bid_WaitingM dao = new _____Activity___Fix_Bid_WaitingM();
}

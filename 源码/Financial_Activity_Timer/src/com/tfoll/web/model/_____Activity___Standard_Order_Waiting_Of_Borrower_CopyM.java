package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.util.Map;

@TableBind(tableName = "_____activity___standard_order_waiting_of_borrower_copy", primaryKey = "id")
public class _____Activity___Standard_Order_Waiting_Of_Borrower_CopyM extends Model<_____Activity___Standard_Order_Waiting_Of_Borrower_CopyM> implements Serializable {

	private static final long serialVersionUID = -7124140993536632669L;
	public static _____Activity___Standard_Order_Waiting_Of_Borrower_CopyM dao = new _____Activity___Standard_Order_Waiting_Of_Borrower_CopyM();

	public static boolean add______activity___standard_order_waiting_of_borrower_copy(_____Activity___Standard_Order_Waiting_Of_BorrowerM _____activity___standard_order_waiting_of_borrower) {
		_____Activity___Standard_Order_Waiting_Of_Borrower_CopyM _____activity___standard_order_waiting_of_borrower_copy = new _____Activity___Standard_Order_Waiting_Of_Borrower_CopyM();
		/**
		 * 里面的表结构必须一致，也就是必须是同一条的数据才行
		 */
		Map<String, Object> map = _____activity___standard_order_waiting_of_borrower.getM();
		map.remove("id");
		_____activity___standard_order_waiting_of_borrower_copy.setMap(map);
		return _____activity___standard_order_waiting_of_borrower_copy.save();
	}
}

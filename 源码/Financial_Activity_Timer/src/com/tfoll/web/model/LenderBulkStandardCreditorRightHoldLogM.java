package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_creditor_right_hold_log", primaryKey = "id")
public class LenderBulkStandardCreditorRightHoldLogM extends Model<LenderBulkStandardCreditorRightHoldLogM> {
	public static LenderBulkStandardCreditorRightHoldLogM dao = new LenderBulkStandardCreditorRightHoldLogM();
}

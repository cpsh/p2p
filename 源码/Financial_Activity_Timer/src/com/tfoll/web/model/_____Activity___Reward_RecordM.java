package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableBind(tableName = "_____activity___reward_record", primaryKey = "user_id")
public class _____Activity___Reward_RecordM extends Model<_____Activity___Reward_RecordM> implements Serializable {

	private static final long serialVersionUID = 1262241552051672915L;
	public static final _____Activity___Reward_RecordM dao = new _____Activity___Reward_RecordM();

	/**
	 * <pre>
	 * CREATE TABLE `_____activity___reward_record` (
	 *   `id` bigint(13) NOT NULL AUTO_INCREMENT,
	 *   `user_id` int(11) DEFAULT NULL COMMENT '用户id',
	 *   `type` int(1) DEFAULT NULL COMMENT '交易类型 1为成功推荐 2指定期间内理财交易',
	 *   `order_type` int(1) DEFAULT '0' COMMENT '订单ID:散标凑集单ID-1，散标理单ID-2，联福宝持有ID-3',
	 *   `order_id` bigint(13) DEFAULT '0' COMMENT '订单ID:散标凑集单ID，散标理单ID，联福宝持有ID',
	 *   `amount` decimal(10,0) DEFAULT NULL COMMENT '交易资金',
	 *   `reward_amount` decimal(20,2) DEFAULT NULL COMMENT '奖励资金',
	 *   `friend_id` int(11) DEFAULT '0' COMMENT '好友id-只是针对推荐 ',
	 *   `add_time` timestamp NULL DEFAULT NULL,
	 *   `add_time_long` bigint(13) DEFAULT NULL,
	 *   PRIMARY KEY (`id`)
	 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	 * </pre>
	 */
	public static boolean add______activity___reward_record(int user_id, int type, int order_type, long order_id, BigDecimal amount, BigDecimal reward_amount, int friend_id) {
		_____Activity___Reward_RecordM _____activity___reward_record = new _____Activity___Reward_RecordM();
		return _____activity___reward_record.//
				set("user_id", user_id).//
				set("type", type).//
				set("order_type", order_type).//
				set("order_id", order_id).set("amount", amount).//
				set("reward_amount", reward_amount).//
				set("friend_id", friend_id).//
				set("add_time_long", (new Date().getTime())).//
				save();

	}
}

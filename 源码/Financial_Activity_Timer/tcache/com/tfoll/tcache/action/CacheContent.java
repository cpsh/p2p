package com.tfoll.tcache.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletResponse;

/**
 * Holds the servlet response in a byte array so that it can be held in the
 * cache (and, since this class is serializable, optionally persisted to disk).
 * 
 */
public class CacheContent {
	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(long activeTime) {
		this.activeTime = activeTime;
	}

	private static final long Second = 1000L;
	private byte[] content = null;
	/**
	 * 配置更新时间和活跃时间
	 */
	private long createTime = 0L;
	private long activeTime = 60 * Second;
	private long overTime;

	private transient ByteArrayOutputStream baos = new ByteArrayOutputStream(10000);

	/**
	 * baos
	 */
	public OutputStream getOutputStream() {
		return baos;
	}

	public int getSize() {
		return (content != null) ? content.length : (-1);
	}

	/**
	 * Called once the response has been written in its entirety. This method
	 * commits the response output stream by converting the output stream into a
	 * byte array.
	 */
	public void toByteArray() {
		if (baos != null) {
			content = baos.toByteArray();// 获取数据
			baos = null;
		}
	}

	public void writeTo(ServletResponse servletResponse) throws IOException {
		OutputStream os = new BufferedOutputStream(servletResponse.getOutputStream());
		servletResponse.setContentLength(content.length);
		os.write(content);
		os.flush();
	}

	public void setOverTime() {
		this.overTime = createTime + activeTime;
	}

	public long getOverTime() {
		return overTime;
	}

}

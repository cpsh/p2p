package com.tfoll.trade.config;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

/**
 * 用户项目开发配置
 * 
 * <pre>
 * Config order: action组合[configRoute(), configInterceptor(), configHandler()],configPlugin()
 * </pre>
 */
public abstract class UserConfig {
	public static Logger logging = Logger.getLogger(UserConfig.class);
	/**
	 * 告诉整个系统或者定时器框架初始化完成
	 */
	public static AtomicBoolean System_Is_Start_Ok = new AtomicBoolean(false);

	/**
	 * 数据库相关的配置:数据库插件启动后才有效<br/>
	 * 
	 * <pre>
	 * 关于ActiveRecord组件配置如下
	 * ActiveRecordManager activeRecord = new ActiveRecordManager(XXPlugin.dataSource);
	 * activeRecord.addMapping(&quot;user&quot;, User.class);
	 * activeRecord.buildAllTableInfo();
	 * </pre>
	 */
	public abstract void configActiveRecord();

	/**
	 * 配置基本的过滤器
	 */
	public abstract void configInterceptor(InterceptorList me);

	/**
	 * 配置插件
	 */
	public abstract void configPlugin(PluginList me);

	/**
	 * 配置路由信息
	 */
	public abstract void configRoute();

	public abstract void afterBuildOK();

	/**
	 * 拼判断是不是Windows系统
	 */
	public boolean isWindows() {
		boolean flag = false;
		if (System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1) {
			flag = true;
		}
		return flag;
	}

}

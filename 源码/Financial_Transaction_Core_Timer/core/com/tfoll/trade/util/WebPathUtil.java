package com.tfoll.trade.util;

import java.io.File;
import java.net.MalformedURLException;

import javax.servlet.http.HttpServletRequest;

public class WebPathUtil {
	/**
	 * 在linux上面运行正常
	 */
	@SuppressWarnings("deprecation")
	public static String getWebPath(HttpServletRequest request) {
		String webRootPath = request.getRealPath("/") + "/";
		return webRootPath;
	}

	// 下面均是windows操作系统项目路径信息.
	/**
	 * <pre>
	 * eg:F:\jfly\JFly\WebRoot\WEB-INF\classes\
	 * </pre>
	 * 
	 * @see #getSrcOrClassesUrlPath()
	 */
	public static String getSrcOrClassesPath() {
		String urlPath = WebPathUtil.class.getClassLoader().getResource("").getPath();
		return (new File(urlPath)).getPath() + "/";
	}

	/**
	 * <pre>
	 * eg:/F:/jfly/JFly/WebRoot/WEB-INF/classes/
	 * </pre>
	 * 
	 * @see #getSrcOrClassesPath()
	 */
	public static String getSrcOrClassesUrlPath() {
		String urlPath = WebPathUtil.class.getClassLoader().getResource("").getPath();
		return urlPath;
	}

	/**
	 * <pre>
	 * eg:F:\jfly\JFly\WebRoot\
	 * </pre>
	 * 
	 * @see #getWebRootUrlPath()
	 */
	public static String getWebRootPath() {
		String urlPath = WebPathUtil.class.getClassLoader().getResource("").getPath();
		return ((new File(urlPath)).getPath() + "/").replace("WEB-INF/classes/", "");
	}

	/**
	 * <pre>
	 * eg:/F:/jfly/JFly/WebRoot/
	 * </pre>
	 * 
	 * @see #getWebRootPath()
	 */
	public static String getWebRootUrlPath() {
		String urlPath = (WebPathUtil.class.getClassLoader().getResource("").getPath()).replace("WEB-INF/classes/", "");
		return urlPath;
	}

	public static void main(String[] args) throws MalformedURLException {
		System.out.println(getSrcOrClassesUrlPath());

	}
}

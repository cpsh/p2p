package com.tfoll.trade.render;

import com.tfoll.trade.aop.ActionExecutor;
import com.tfoll.trade.core.Action;
import com.tfoll.trade.core.ActionContext;
import com.tfoll.trade.core.ActionMapping;
import com.tfoll.trade.core.Controller;

public class ActionRender extends Render {

	private String actionUrl;

	public ActionRender(String actionUrl) {
		if (actionUrl == null || "".equals(actionUrl.trim())) {
			throw new NullPointerException("actionUrl为空");
		}
		this.actionUrl = actionUrl.trim();
	}

	public String getActionUrl() {
		return actionUrl;
	}

	@Override
	public void render() {
		// 再次调用Action进行业务处理
		Action action = ActionMapping.getAction(actionUrl);
		// 缓存控制器
		Controller controller = action.getController();

		// ActionContext.setActionContext(request, response);
		(new ActionExecutor(action, controller)).invoke();
		Render render = ActionContext.getRender();
		// 下面这些渲染器是通过测试的
		// 核心处理：Action的渲染
		if (render instanceof ActionRender) {
			// 继续进行处理
			String actionUrl = ((ActionRender) render).getActionUrl();
			if (actionUrl == null || "".equals(actionUrl)) {
				throw new IllegalArgumentException("将要跳转的Action的actionUrl为空");
			}
			render.render();
			return;

		}
		if (render instanceof TextRender) {
			render.render();
			return;
		}
		if (render instanceof FileRender) {
			render.render();
			return;
		}
		// 页面跳转和重定向

		if (render instanceof RootRender) {
			render.render();
			return;
		}

		if (render instanceof HttpRender) {
			render.render();
			return;
		}
		throw new RuntimeException("不支持的渲染方式");

	}
}

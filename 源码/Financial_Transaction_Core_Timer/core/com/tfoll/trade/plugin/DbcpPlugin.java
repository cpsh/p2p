package com.tfoll.trade.plugin;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.log4j.Logger;

public class DbcpPlugin implements IPlugin {
	public DbcpPlugin() {
	}

	private static Logger logger = Logger.getLogger(DbcpPlugin.class);
	private static DataSource dataSource;

	public DbcpPlugin(Properties properties) throws Exception {
		setDataSource(BasicDataSourceFactory.createDataSource(properties));
		logger.debug("加载数据库连接OK");
	}

	public boolean start() {
		return true;
	}

	public boolean stop() {
		return true;
	}

	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		DbcpPlugin.dataSource = dataSource;
	}
}

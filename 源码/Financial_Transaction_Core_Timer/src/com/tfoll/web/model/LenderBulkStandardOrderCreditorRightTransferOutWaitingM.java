package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "lender_bulk_standard_order_creditor_right_transfer_out_waiting", primaryKey = "id")
public class LenderBulkStandardOrderCreditorRightTransferOutWaitingM extends Model<LenderBulkStandardOrderCreditorRightTransferOutWaitingM> {
	public static final LenderBulkStandardOrderCreditorRightTransferOutWaitingM dao = new LenderBulkStandardOrderCreditorRightTransferOutWaitingM();
}

package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

@TableBind(tableName = "user_message_request_error", primaryKey = "id")
public class UserMessageRequestErrorM extends Model<UserMessageRequestErrorM> {
	public static UserMessageRequestErrorM dao = new UserMessageRequestErrorM();
}

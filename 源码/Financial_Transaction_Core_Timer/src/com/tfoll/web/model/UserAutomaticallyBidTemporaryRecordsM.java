package com.tfoll.web.model;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;

import java.util.Date;

//@TableBind(tableName = "user_automatically_bid_temporary_records", primaryKey = "id")
public class UserAutomaticallyBidTemporaryRecordsM extends Model<UserAutomaticallyBidTemporaryRecordsM> {
	public static UserAutomaticallyBidTemporaryRecordsM dao = new UserAutomaticallyBidTemporaryRecordsM();

	public static boolean is_hash_bid(int user_id, long gather_money_order_id) {
		return Db.queryLong("SELECT COUNT(1) from user_automatically_bid_temporary_records WHERE user_id=? and gather_money_order_id=?", new Object[] { user_id, gather_money_order_id }) > 0;
	}

	/**
	 * 删除8天以前的数据
	 */
	public static void delete_8_day_temporary_records() {
		Date now = new Date();
		long $8day_ago = now.getTime() - 8 * Model.Day;
		Db.update("DELETE from user_automatically_bid_temporary_records WHERE add_time_long<=?", new Object[] { $8day_ago });
	}

	/**
	 * 如果存在临时记录则返回为假,为真则可以进行自动投标
	 */
	public static boolean is_can_auto_bid(int user_id, long gather_money_order_id) {
		if ("1".equals("2")) {
			if (is_hash_bid(user_id, gather_money_order_id)) {
				return false;
			} else {
				UserAutomaticallyBidTemporaryRecordsM user_automatically_bid_temporary_records = new UserAutomaticallyBidTemporaryRecordsM();
				return user_automatically_bid_temporary_records.set("user_id", user_id).set("gather_money_order_id", gather_money_order_id).set("add_time_long", System.currentTimeMillis()).save();
			}
		}
		return true;

	}

}

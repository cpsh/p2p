package com.tfoll.web.model;

import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.model.TableBind;

import java.io.Serializable;

@TableBind(tableName = "fix_bid_user_newer_bid", primaryKey = "id")
public class FixBidUserNewerBidM extends Model<FixBidUserNewerBidM> implements Serializable {

	/**
	 * 新手标
	 */
	private static final long serialVersionUID = -4121098540066401857L;
	public static FixBidUserNewerBidM dao = new FixBidUserNewerBidM();
}

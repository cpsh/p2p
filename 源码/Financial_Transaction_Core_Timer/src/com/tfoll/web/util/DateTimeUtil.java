package com.tfoll.web.util;

import com.tfoll.trade.activerecord.model.Model;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {

	/**
	 * 需要判断输入的日期不能为空且不能为空字符串
	 */
	public static Date my_get_morning_time(String $date) {
		if ($date == null) {
			throw new RuntimeException("date is null");
		}
		if ("".equals($date.trim())) {
			throw new RuntimeException("date is empty");
		}
		try {
			return Model.Time.parse($date.trim() + " 00:00:00");
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 需要判断输入的日期不能为空且不能为空字符串
	 */
	public static Date my_get_night_time(String $date) {
		if ($date == null) {
			throw new RuntimeException("date is null");
		}
		if ("".equals($date.trim())) {
			throw new RuntimeException("date is empty");
		}
		try {
			return Model.Time.parse($date.trim() + " 23:59:59");
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 获取当天的时间格式
	 */

	public static String getMorningStringOfNoDay() {
		String $day = Model.Date.format(new Date());

		return ($day + " 00:00:00");

	}

	public static String getNowStringOfNoDay() {
		return Model.Time.format(new Date());

	}

	public static String getNightStringOfNoDay() {
		String $day = Model.Date.format(new Date());

		return ($day + " 23:59:59");

	}

	public static Date get_morning_time(String $time) {
		if ($time == null) {
			throw new RuntimeException("$time is null");
		}
		if ("".equals($time.trim())) {
			throw new RuntimeException("$time is empty");
		}
		try {
			return Model.Time.parse($time);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 需要判断输入的日期不能为空且不能为空字符串
	 */
	public static Date get_night_time(String $time) {
		if ($time == null) {
			throw new RuntimeException("$time is null");
		}
		if ("".equals($time.trim())) {
			throw new RuntimeException("$time is empty");
		}
		try {
			return Model.Time.parse($time);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param smdate
	 *            较小的时间
	 * @param bdate
	 *            较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(String smdate, String bdate) throws ParseException {
		Date sm_date = Model.Date.parse(smdate);
		Date b_date = Model.Date.parse(bdate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(sm_date);
		long time1 = cal.getTimeInMillis();
		cal.setTime(b_date);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	public static void main(String[] args) throws ParseException {
		System.out.println(daysBetween("2012-09-08 10:10:10", "2012-09-08 11:00:00"));
	}
}

package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.domain.FinancialRecipientInformation;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardOverdueRepaymentRecordM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldRepaymentRecordM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutM;
import com.tfoll.web.model.LenderBulkStandardOrderCreditorRightTransferOutWaitingM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.SendBackTheSuccessfulInformation;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 针对没有还款的单子进行自动还款
 */
public class Timer_auto_pay extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_auto_pay.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_auto_pay.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_auto_pay.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_auto_pay.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer_auto_pay.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_auto_pay.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {

		String now_day = Model.Date.format(new Date());
		List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT id from borrower_bulk_standard_repayment_plan WHERE automatic_repayment_date=? and  is_repay=0", new Object[] { now_day });
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
			try {
				doSubTaskFor(repayment_plan_id);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}

	}

	/**
	 * 处理每个还款计划
	 */
	public static void doSubTaskFor(final long repayment_plan_id) {
		final List<Long> has_pay_repayment_plan_id_list = new ArrayList<Long>();
		final Map<Long, List<FinancialRecipientInformation>> financial_recipient_information_list_map = new HashMap<Long, List<FinancialRecipientInformation>>();
		/**
		 * 会出现一个失败转移的问题
		 */
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan = BorrowerBulkStandardRepaymentPlanM.dao.findFirst("SELECT * from borrower_bulk_standard_repayment_plan where id=? ", new Object[] { repayment_plan_id });
				if (borrower_bulk_standard_repayment_plan == null) {
					return false;
				}
				long gather_money_order_id = borrower_bulk_standard_repayment_plan.getLong("gather_money_order_id");
				int borrower_user_id = borrower_bulk_standard_repayment_plan.getInt("borrower_user_id");
				//
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT * FROM borrower_bulk_standard_gather_money_order gmo WHERE id =? ", new Object[] { gather_money_order_id });// 至少含有几个还款计划
				if (borrower_bulk_standard_gather_money_order == null) {
					return false;
				}
				String borrow_title = borrower_bulk_standard_gather_money_order.getString("borrow_title");
				int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");
				int pay_for_object = borrower_bulk_standard_gather_money_order.getInt("pay_for_object");
				/**
				 * 如果该还款计划逾期了同样需要进行自动还款处理-资金不够则不能进行自动还款
				 */
				final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
				final int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");// 需要关注份额之比

				/**
				 * <pre>
				 * CREATE TABLE `borrower_bulk_standard_repayment_plan` (
				 * `repay_actual_time` timestamp NULL DEFAULT NULL COMMENT '实际还款时间',
				 * `repay_actual_time_long` bigint(20) DEFAULT NULL COMMENT '实际还款时间毫秒数',
				 * 
				 * `should_repayment_total` decimal(10,2) DEFAULT '0.00' COMMENT '应该要还的本金和利息',
				 * `should_repayment_principle` decimal(10,2) DEFAULT '0.00' COMMENT '要还的本金',
				 * `should_repayment_interest` decimal(10,2) DEFAULT '0.00' COMMENT '要还的利息（逾期后该数值每天都会改变，要写存储过程？）',
				 * 
				 * 
				 * `normal_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正常管理费',
				 * `over_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '逾期管理费',
				 * `over_punish_interest` decimal(10,2) DEFAULT '0.00' COMMENT '逾期罚息',
				 * `actual_repayment` decimal(10,2) DEFAULT '0.00' COMMENT '实际还款金额',
				 * 
				 * `is_repay` int(1) DEFAULT '0' COMMENT '是否已经还款 0.没有还款 1.已经还款',
				 * 
				 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
				 * PRIMARY KEY (`id`),
				 * KEY `automatic_repayment_date` (`automatic_repayment_date`)
				 * ) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COMMENT='借款人的还款计划表';
				 * </pre>
				 */
				/**
				 * 如果用户属于没有严重逾期的情况-那么这部分的资金是给理财人的,要不然是给系统的
				 */
				//
				/**
				 * 是否支付0/1
				 */
				int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");// 是否支付-决定逾期管理费和逾期罚息是否需要从数据库计算还是从数据库记录中取值
				if (is_repay != 0) {
					return true;
				}
				int total_periods = borrower_bulk_standard_repayment_plan.getInt("total_periods");
				int current_period = borrower_bulk_standard_repayment_plan.getInt("current_period");

				// 实际还款时间
				Date repay_actual_time = new Date();
				long repay_actual_time_long = repay_actual_time.getTime();
				borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
				borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

				// 还款日期
				/**
				 * <pre>
				 * Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate(&quot;repay_end_time&quot;);
				 * </pre>
				 */
				/**
				 * <pre>
				 * String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
				 * 
				 * <pre>
				 */
				/**
				 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息- 系统收取管理费用
				 */
				// 要还的本息
				// 应该要还的本金和利息
				BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
				// 要还的本金
				BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
				// 要还的利息
				BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
				// 正常管理费
				BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
				// 管理费[包含了逾期管理费]
				BigDecimal over_manage_fee = new BigDecimal("0");
				// 罚息
				BigDecimal over_punish_interest = new BigDecimal("0");
				/**
				 * 上面是单项的数据
				 */
				// 应还总额=月还本息+管理费 +逾期费用
				BigDecimal total_total = should_repayment_total.add(normal_manage_fee);// 理财那边的还款和借款这边的还款是不一样的

				/**
				 * 针对各种借款还款阶段做出响应的操作:判断正常还款,自动还款,是否严重逾期,逾期 <br/>
				 * 
				 * <pre>
				 * 正常还款和自动还款是没有罚息的
				 * 逾期是含有罚息的,那么这部分的资金是打给理财用户的
				 * 严重逾期那么这部分的资金是还给系统的-而不是理财的用户-理财用户是我们系统指定的一个帐号id= 4id=[1-4]都被系统征用
				 * </pre>
				 */

				/**
				 * <pre>
				 * CREATE TABLE `borrower_bulk_standard_repayment_plan` (
				 * `repay_actual_time` timestamp NULL DEFAULT NULL COMMENT '实际还款时间',
				 * `repay_actual_time_long` bigint(20) DEFAULT NULL COMMENT '实际还款时间毫秒数',
				 * `should_repayment_total` decimal(10,2) DEFAULT '0.00' COMMENT '应该要还的本金和利息',
				 * `should_repayment_principle` decimal(10,2) DEFAULT '0.00' COMMENT '要还的本金',
				 * `should_repayment_interest` decimal(10,2) DEFAULT '0.00' COMMENT '要还的利息（逾期后该数值每天都会改变，要写存储过程？）',
				 * `normal_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正常管理费',
				 * `over_manage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '逾期管理费',
				 * `over_punish_interest` decimal(10,2) DEFAULT '0.00' COMMENT '逾期罚息',
				 * `actual_repayment` decimal(10,2) DEFAULT '0.00' COMMENT '实际还款金额',
				 * `is_repay` int(1) DEFAULT '0' COMMENT '是否已经还款 0.没有还款 1.已经还款',
				 * `repayment_period` int(1) DEFAULT '0' COMMENT '记录什么时候还钱的-还款期间:0未还款-->1.处于还款日期之前[手动还款] ,2还款日当天系统自动还款3普通逾期 4.严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}] ',
				 * PRIMARY KEY (`id`),
				 * KEY `automatic_repayment_date` (`automatic_repayment_date`)
				 * ) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COMMENT='借款人的还款计划表';
				 * </pre>
				 */
				// 记录上面的费用
				borrower_bulk_standard_repayment_plan.set("normal_manage_fee", normal_manage_fee);// 正常管理费-属于系统的收入
				borrower_bulk_standard_repayment_plan.set("over_manage_fee", over_manage_fee);// 逾期管理费-属于系统的收入
				borrower_bulk_standard_repayment_plan.set("over_punish_interest", over_punish_interest);// 罚息利息-如果是严重逾期则给系统，如果是一般逾期则给理财的用户,还款日之前-包括还款日不需要给理财用户

				// 实际还款额=两个管理费用+一个罚息
				BigDecimal actual_repayment = should_repayment_total.add(normal_manage_fee);
				borrower_bulk_standard_repayment_plan.set("actual_repayment", actual_repayment);
				//
				borrower_bulk_standard_repayment_plan.set("is_repay", 1);
				/**
				 * <pre>
				 * 记录什么时候还钱的-还款期间:
				 * -----------------没有逾期的三个阶段------------------
				 * 0未还款
				 * 1处于还款日期之前[手动还款]
				 * 2还款日当天系统自动还款
				 * -----------------没有逾期的二个阶段------------------
				 * 3普通逾期
				 * 4严重逾期[{还款开始时间-还款结束时间(}},逾期开始时间-逾期结束时间(},严重逾期开始时间{(...}]
				 * </pre>
				 */
				int repayment_period = 2;
				borrower_bulk_standard_repayment_plan.set("repayment_period", repayment_period);
				boolean is_ok_update_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.update();
				if (!is_ok_update_borrower_bulk_standard_repayment_plan) {
					return false;
				}
				/**
				 * 1修改借款人的资金信息
				 */
				int user_id_of_borrower = borrower_bulk_standard_gather_money_order.getInt("user_id");
				UserM user = UserM.getUserM(user_id_of_borrower);
				String borrower_nickname = user.getString("nickname");
				UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_of_borrower);
				BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used"); // 还款之前的金额
				BigDecimal cny_freeze_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_freeze");

				boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower.subtract(total_total)).update();
				if (!is_ok_update_user_now_money_of_borrower) {
					return false;
				}
				/**
				 * 需要区分那些是打款给理财人或者系统的
				 * 
				 * <pre>
				 * sys_expenses_records no
				 * sys_income_records yes
				 * 
				 * user_money_change_records yes 
				 * user_transaction_records yes
				 * </pre>
				 */
				/**
				 * 没有逾期管理费和没有逾期罚息
				 */
				// 系统收入的部分
				// 正常管理费
				boolean is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, normal_manage_fee, "正常管理费");
				if (!is_ok_add_sys_income_records) {
					return false;
				}
				/**
				 * 
				 * <pre>
				 * 向借款人打钱由两部分组成
				 * public static final int Type_31 = 31;// 打入借入者借款总额-收入
				 * @SysIncome
				 * public static final int Type_32 = 32;// 收取借入者借款服务费-支出
				 * // 向借款人收取本息由...部分组成.这个是正常还款的记录
				 * ------------------------------------------------------------------------
				 * public static final int Type_33 = 33;// 偿还本息-支出
				 * @SysIncome
				 * public static final int Type_34 = 34;// 返还服务费..特别收取-支出
				 * @SysIncome
				 * public static final int Type_35 = 35;// 借款管理费-支出-[系统]
				 * @SysIncome
				 * public static final int Type_36 = 36;// 借款超期管理费-支出-[系统]
				 * @SysIncome
				 * public static final int Type_37 = 37;// 普通罚息-1-30天内-支出[理财人]
				 * @SysIncome
				 * public static final int Type_38 = 38;// 严重超期罚息-大于30天-支出-[系统]
				 * 可能还有其他key的需要添加系统的收入
				 * </pre>
				 */
				// 用户资金变化
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息#还款计划id"
						+ repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee),
						"偿还一般管理费#还款计划id" + repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				// 一般罚息和严重罚息-no
				// 用户交易记录
				/**
				 * <pre>
				 * public static final int Type_31 = 31;// 借款服务费
				 * public static final int Type_32 = 32;// 偿还本息 
				 * public static final int Type_33 = 33;// 借款管理费
				 * public static final int Type_34 = 34;// 返还服务费 
				 * public static final int Type_35 = 35;//回收本息 
				 * public static final int Type_36 = 36;// 提前回收 
				 * public static final int Type_37 = 37;// 提前还款 
				 * public static final int Type_38 = 38;// 逾期管理费
				 * public static final int Type_39 = 39;// 逾期罚息
				 * </pre>
				 */
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_32, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee), "借款管理费");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}

				// 判断是不是最后的一期
				final boolean is_last_period = total_periods == current_period;
				if (is_last_period) {
					/**
					 * 如果还款计划里面最后一期之前的所有都还款成功,那么则可以表示整个凑集单都成功还款
					 */
					boolean has_not_pay_all = Db.queryLong("SELECT COUNT(1) from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id=? and is_repay=0 and id!=?", new Object[] { gather_money_order_id, repayment_plan_id }) >= 0;
					if (has_not_pay_all) {
						{
							/**
							 * 更新借款人的借款单信息和用户信息
							 */
							boolean is_ok_update_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("payment_state", 3).set("payment_finish_time", new Date()).update();
							if (!is_ok_update_borrower_bulk_standard_gather_money_order) {
								return false;
							}

							/**
							 * is_not_pay还款完成的时候需要修改is_not_pay =0,borrow_type=0
							 */
							boolean is_update_user_info_is_not_pay_ok = Db.update("UPDATE user_info SET is_not_pay =0,borrow_type=0 WHERE id=?", new Object[] { borrower_user_id }) >= 0;
							if (!is_update_user_info_is_not_pay_ok) {
								return false;
							}
							// 如果当前没有处于严重逾期状态,则需要把该凑集单的所有的持有和正在转让的单子撤掉
							// 见下面...非逾期状态-债权持有处理之后

						}
					}

				}
				/**
				 * 根绝pay_for_object是否还给系统来决定是否还给理财用户或者系统
				 */
				if (pay_for_object == 1) {
					// 还给系统-如果系统进行对这个资金进行处理了-那么这部分的钱应该入账了且可以被公司使用
					boolean is_ok_add_borrower_bulk_standard_overdue_repayment_record = BorrowerBulkStandardOverdueRepaymentRecordM.add_borrower_bulk_standard_overdue_repayment_record(//
							gather_money_order_id,//
							repayment_plan_id, //
							total_periods, //
							current_period,//
							should_repayment_total, //
							should_repayment_principle, //
							should_repayment_interest,//
							normal_manage_fee,//
							over_manage_fee, //
							over_punish_interest,//
							total_total);//
					if (!is_ok_add_borrower_bulk_standard_overdue_repayment_record) {
						return false;
					} else {
						return true;
					}

				} else {
					List<FinancialRecipientInformation> financial_recipient_information_list = new ArrayList<FinancialRecipientInformation>();
					/**
					 * 本息是所有的理财用户都需要进行平均分配的
					 */
					BigDecimal each_principal_interest = should_repayment_total.divide(new BigDecimal(borrow_all_share + ""), 4, BigDecimal.ROUND_DOWN); // 平均每份所对应的本息
					each_principal_interest = each_principal_interest.setScale(8, BigDecimal.ROUND_DOWN);
					/**
					 * 
					 * 定义当前需要还的每份的本金
					 */
					BigDecimal each_principal = should_repayment_principle.divide(new BigDecimal(borrow_all_share + ""), 4, BigDecimal.ROUND_DOWN); // 平均每份所对应的本金
					each_principal = each_principal.setScale(8, BigDecimal.ROUND_DOWN);
					/**
					 * 定义当前需要还的每份的利息
					 */
					BigDecimal each_interest = should_repayment_interest.divide(new BigDecimal(borrow_all_share + ""), 4, BigDecimal.ROUND_DOWN); // 平均每份所对应的利息
					each_interest = each_interest.setScale(8, BigDecimal.ROUND_DOWN);

					// 到债权持有表中查找每个理财人对应的持有份数, 来增加他们资金表中的可用余额
					String sql_find_holders = "select id,user_id,hold_money,hold_share,transfer_share,is_transfer_all from lender_bulk_standard_creditor_right_hold where gather_money_order_id =? and hold_share!=0 ";
					List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find(sql_find_holders, new Object[] { gather_money_order_id });
					if (!Utils.isHasData(lender_bulk_standard_creditor_right_hold_list)) {
						return false;
					}
					// 还给理财人
					/**
					 * 还款的时候债权的价值以及对应的价格和手续费会相应的按照折价的系数修改-
					 * 同时系统需要一种机制告诉系统所有需要通过债权转让获取的债权持有的理财人暂时不能操作
					 */
					for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {
						long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");// id
						int financial_user_id = lender_bulk_standard_creditor_right_hold.getInt("user_id"); // 理财人id
						BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
						int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 原始购买的份额
						int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 转出的份额
						if (transfer_share != 0) {// 正在转让过程中需要对外面的债权转让实现冻结
							/**
							 * 自动还款的时候债权的价值改变
							 */
							List<LenderBulkStandardOrderCreditorRightTransferOutM> lender_bulk_standard_order_creditor_right_transfer_out_list = LenderBulkStandardOrderCreditorRightTransferOutM.dao.find("SELECT id from lender_bulk_standard_order_creditor_right_transfer_out WHERE creditor_right_hold_id=? and state=1", new Object[] { creditor_right_hold_id });
							if (Utils.isHasData(lender_bulk_standard_order_creditor_right_transfer_out_list)) {
								for (LenderBulkStandardOrderCreditorRightTransferOutM lender_bulk_standard_order_creditor_right_transfer_out : lender_bulk_standard_order_creditor_right_transfer_out_list) {
									LenderBulkStandardOrderCreditorRightTransferOutWaitingM lender_bulk_standard_order_creditor_right_transfer_out_waiting = new LenderBulkStandardOrderCreditorRightTransferOutWaitingM();
									boolean is_ok_add_lender_bulk_standard_order_creditor_right_transfer_out_waiting = lender_bulk_standard_order_creditor_right_transfer_out_waiting.//
											set("creditor_right_transfer_out_id", lender_bulk_standard_order_creditor_right_transfer_out.getLong("id")).//
											set("deal_level", 2).// 要求系统马上进行债权转让程序进行处理
											save();
									if (!is_ok_add_lender_bulk_standard_order_creditor_right_transfer_out_waiting) {
										return false;
									}

								}

							}

						}
						// 应该收到的钱由两部分构成
						BigDecimal principal = each_principal.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本金
						BigDecimal interest = each_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的利息
						BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本息
						//						
						BigDecimal punish_interest = new BigDecimal("0");// 理财人具体收到的罚息
						// 应该回收的金额:principal_interest+punish_interest
						BigDecimal recieve_money = principal_interest;

						UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(financial_user_id);
						BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
						BigDecimal cny_freeze_of_lender = user_now_money_of_lender.getBigDecimal("cny_freeze");

						/**
						 * 理财人回款
						 */
						boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender.add(recieve_money)).update();
						if (!is_ok_update_user_now_money_of_lender) {
							return false;
						}
						/**
						 * 需要记录资金信息
						 * 
						 * <pre>
						 * sys_expenses_records no
						 * sys_income_records no
						 * 
						 * user_money_change_records yes 
						 * user_transaction_records yes
						 * </pre>
						 */
						// user_money_change_records
						is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_74, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息#还款计划id" + repayment_plan_id);
						if (!is_ok_add_user_money_change_records_by_type) {

						}

						// user_transaction_records
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(financial_user_id, UserTransactionRecordsM.Type_32, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息");
						if (!is_ok_add_user_transaction_records) {
							return false;
						}

						/**
						 * 添加回款的财务记录
						 */
						boolean is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record = LenderBulkStandardCreditorRightHoldRepaymentRecordM.add_lender_bulk_standard_creditor_right_hold_repayment_record(//
								financial_user_id,//
								user_id_of_borrower,//
								gather_money_order_id,//
								borrow_type, //
								borrower_nickname,//
								repayment_plan_id, //
								total_periods, //
								current_period,//
								creditor_right_hold_id,//
								hold_money,//
								hold_share, //
								principal,//
								interest,//
								principal_interest,//
								punish_interest, //
								recieve_money,//
								new Date());//
						if (!is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record) {
							return false;
						}
						// 记录进financial_recipient_information_list
						FinancialRecipientInformation financial_recipient_information = new FinancialRecipientInformation(//
								financial_user_id,//
								gather_money_order_id,//
								user_id_of_borrower,//
								repayment_plan_id, //
								total_periods, //
								current_period, //
								creditor_right_hold_id, //
								hold_money,//
								hold_share, //
								principal_interest, //
								punish_interest,//
								new Date(), //
								new Date().getTime(),//
								borrow_title);//
						financial_recipient_information_list.add(financial_recipient_information);
					}// for
					// 

					if (!is_last_period) {

						/**
						 * 自动还款-成功还掉最后一期-可能以前逾期导致即使还了最后一期所有的还款计划都没有还完-
						 * 则需要其他程序进行处理
						 */
						/**
						 * 针对这个单子-如果该期的还款计划是最后的一期那么我们需要检查以前的所有的的还款计划是否还款成功
						 */
						long has_not_pay_all = Db.queryLong("SELECT COUNT(1) from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id=?  and id!=?  and is_repay=0", new Object[] { gather_money_order_id, repayment_plan_id });
						if (has_not_pay_all == 0) {// 以前都还清了的
							{
								boolean is_ok_update_lender_bulk_standard_creditor_right_hold = Db.update("UPDATE lender_bulk_standard_creditor_right_hold SET transfer_money=0,transfer_share=0,is_need_handle=0 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
								if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
									return false;
								}
								boolean is_ok_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
								if (!is_ok_lender_bulk_standard_order_creditor_right_transfer_out) {
									return false;
								}
							}
						}

					}
					{// 外面进行理财人回款成功消息提示
						financial_recipient_information_list_map.put(repayment_plan_id, financial_recipient_information_list);
						has_pay_repayment_plan_id_list.add(repayment_plan_id);// 保证有序性
					}
					return true;

					// }

				}

			}
		});
		if (!is_ok) {
			logger.error("自动还款失败repayment_plan_id=" + repayment_plan_id);
		} else {
			if (Utils.isHasData(has_pay_repayment_plan_id_list)) {
				SendBackTheSuccessfulInformation.send_back_the_successful_information(has_pay_repayment_plan_id_list, financial_recipient_information_list_map);
			}
		}

	}

}
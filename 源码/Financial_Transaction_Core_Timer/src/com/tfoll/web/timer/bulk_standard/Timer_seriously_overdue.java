package com.tfoll.web.timer.bulk_standard;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.domain.FinancialRecipientInformation;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentBySystemM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldLogM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldRepaymentRecordM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.SendBackTheSuccessfulInformation;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 严重逾期处理.
 * 
 * <pre>
 * 首先我们分析还款业务
 * 还款间隔是最低是28天闰年  最多31天.
 * 为了保证[严重逾期处理]和[系统自动还款或者手动还款]冲突[冲突就是三者在同一时刻进行处理,会造成用户资金乱扣],所以制定严重逾期处理位于上个自动还款日32天
 * </pre>
 */
public class Timer_seriously_overdue extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_seriously_overdue.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_seriously_overdue.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_seriously_overdue.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_seriously_overdue.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();

					}
				}
				logger.info("定时任务:" + Timer_seriously_overdue.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_seriously_overdue.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		/**
		 * 把用户表当着一个队列进行依次处理-一个用户最多只能同时向系统借款一次
		 */
		List<UserM> user_list = UserM.dao.find("SELECT id,nickname from user_info");
		for (UserM user : user_list) {
			try {
				doSubTaskFor(user);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();

			}
		}
	}

	static Gson gson = new Gson();

	public static void doSubTaskFor(final UserM user) throws Exception {
		final int user_id = user.getInt("id");
		final String nickname = user.getString("nickname");
		/**
		 * 根据用户ID查找最近正在还款且没有进行严重逾期处理的凑集单
		 */
		final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("select * from borrower_bulk_standard_gather_money_order where user_id = ? and payment_state=2 and pay_for_object=0 order by id desc limit 1", user_id);
		if (borrower_bulk_standard_gather_money_order == null) {
			return;
		}
		final int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");// 需要关注份额之比
		final long gather_money_order_id = borrower_bulk_standard_gather_money_order.getLong("id");
		final int borrow_type = borrower_bulk_standard_gather_money_order.getInt("borrow_type");
		final String borrow_title = borrower_bulk_standard_gather_money_order.getString("borrow_title");
		//
		int borrow_duration = borrower_bulk_standard_gather_money_order.getInt("borrow_duration");
		@SuppressWarnings("unused")
		int borrow_duration_day = borrower_bulk_standard_gather_money_order.getInt("borrow_duration_day");
		final int total_periods = borrow_duration > 0 ? borrow_duration : 1;
		/**
		 * 查询出还款计划
		 */
		final List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT * from borrower_bulk_standard_repayment_plan where gather_money_order_id = ? order by id asc", new Object[] { gather_money_order_id });

		Date now = new Date();
		long now_long = now.getTime();
		/**
		 * 严重逾期32天表示距离上个自动还款日大于31天整的间隔
		 */
		long day_31 = Model.Day * (32 - 1);
		/**
		 * 定义是否严重逾期了
		 */
		boolean is_seriously_overdue = false;
		long repayment_plan_id = 0;
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
			if (is_repay == 1) {
				continue;
			} else {
				long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
				// 按照正常情况应该是now_long>=day_31+repay_end_time_long
				// 还款截止时间后的32天的早上：day_31+repay_end_time_long
				if ((now_long > day_31 + repay_end_time_long)) {
					is_seriously_overdue = true;
					repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
					break;
				} else {
					// ...check later
				}
			}
		}
		if (!is_seriously_overdue) {
			return;
		}
		/**
		 * 计算出是否含有当月以及当月的还款的计划ID是多少
		 */
		boolean is_in_the_normal_repayment_plan = false;
		long repayment_plan_id_of_this_month = 0;
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {

			repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
			long repay_start_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_start_time_long");
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if ((repay_start_time_long <= now_long && now_long <= repay_end_time_long)) {
				is_in_the_normal_repayment_plan = true;
				repayment_plan_id_of_this_month = repayment_plan_id;
				break;
			}
		}
		BigDecimal all_repayment = new BigDecimal("0");
		// 全部本息
		BigDecimal all_principle_interest = new BigDecimal("0");
		// 全部本金
		BigDecimal all_principle = new BigDecimal("0");
		// 全部利息
		BigDecimal all_interest = new BigDecimal("0");
		// 全部罚息
		BigDecimal all_over_punish_interest = new BigDecimal("0");
		// 默认为key升序排列-严重逾期处理的时候需要记录当时未还款的还款计划ID
		final TreeMap<String, String> repayment_periods_map = new TreeMap<String, String>(new Comparator<String>() {

			public int compare(String o1, String o2) {
				if (null == o1 || null == o2) {
					return 0;
				}
				Integer i1 = Integer.parseInt(o1);
				Integer i2 = Integer.parseInt(o2);
				if (i1 < i2) {
					return -1;
				} else {
					return 1;
				}

			}
		});
		if (is_in_the_normal_repayment_plan) {
			for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
				int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
				if (is_repay == 1) {
					continue;
				} else {
					repayment_periods_map.put(borrower_bulk_standard_repayment_plan.getInt("current_period") + "", borrower_bulk_standard_repayment_plan.getLong("id") + "");
					Date repay_actual_time = new Date();
					long repay_actual_time_long = repay_actual_time.getTime();
					borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
					borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

					// 还款日期
					Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
					String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
					// repayment_period==3普通逾期 repayment_period==4严重逾期
					int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);
					/**
					 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
					 * 系统收取管理费用
					 */
					// 要还的本息
					// 应该要还的本金和利息
					BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
					// 要还的本金
					BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
					// 要还的利息
					BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
					/**
					 * 不必关注管理费
					 */
					// 罚息
					BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

					repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
					if (repayment_plan_id < repayment_plan_id_of_this_month) {
						// repayment_period==3普通逾期 repayment_period==4严重逾期
						if (repayment_period != 3 && repayment_period != 4) {
							logger.error("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
							throw new RuntimeException("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
						} else if (repayment_period == 3) {
						} else if (repayment_period == 4) {
							/**
							 * 罚息只能是31天
							 */
							over_punish_interest = CalculationFormula.get_punish_principal_and_interest_of_only_31(should_repayment_total);
						} else {
							throw new RuntimeException("...程序不可能执行到这的");
						}

					} else if (repayment_plan_id == repayment_plan_id_of_this_month) {
						// 当月
						over_punish_interest = new BigDecimal("0");
					} else {// >
						// 当月之后没有利息
						should_repayment_interest = new BigDecimal("0");// 利息为0
						over_punish_interest = new BigDecimal("0");

					}

					{
						//
						all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
						all_principle = all_principle.add(should_repayment_principle);
						all_interest = all_interest.add(should_repayment_interest);
						all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);
						//
						all_repayment = all_repayment.add(should_repayment_principle).add(should_repayment_interest).add(over_punish_interest);
					}

					if (Constants.devMode) {
						System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + repayment_plan_id + "   repayment_period:" + repayment_period);
						System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
						System.out.println("要还的本金" + should_repayment_principle);
						System.out.println("要还的利息" + should_repayment_interest);
						/**
						 * 不必关注管理费
						 */
						System.out.println("31天内的罚息" + over_punish_interest);//
					}

				}

			}

		} else {
			/**
			 * 如果不含有当期，则可能是借款日是28号，刚刚现在还款计划的最后的一期，同时当前是某个月的3号。[即最后一期还款截止时间之后]
			 * 那么则所有的还款计划如果没有还则都需要安装本金和罚息来算.
			 */

			for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
				int is_repay = borrower_bulk_standard_repayment_plan.getInt("is_repay");
				if (is_repay == 1) {
					continue;
				} else {
					repayment_periods_map.put(borrower_bulk_standard_repayment_plan.getInt("current_period") + "", borrower_bulk_standard_repayment_plan.getLong("id") + "");
					Date repay_actual_time = new Date();
					long repay_actual_time_long = repay_actual_time.getTime();
					borrower_bulk_standard_repayment_plan.set("repay_actual_time", repay_actual_time);
					borrower_bulk_standard_repayment_plan.set("repay_actual_time_long", repay_actual_time_long);

					// 还款日期
					Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
					String automatic_repayment_date = borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date");
					// repayment_period==3普通逾期 repayment_period==4严重逾期
					int repayment_period = CalculationFormula.get_repayment_period_with_now(repay_end_time, automatic_repayment_date);
					/**
					 * 算出该借款用户在这个还款计划需要还给理财人的资金-针对每个用户按照比例进行还款:包括正常的本息和罚息-
					 * 系统收取管理费用
					 */
					// 要还的本息
					// 应该要还的本金和利息
					BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
					// 要还的本金
					BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
					// 要还的利息
					BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
					/**
					 * 不必关注管理费
					 */
					// 罚息
					BigDecimal over_punish_interest = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);

					// repayment_period==3普通逾期 repayment_period==4严重逾期

					if (repayment_period != 3 && repayment_period != 4) {
						logger.error("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
						throw new RuntimeException("repayment_plan_id < repayment_plan_id_of_this_month--repayment_period!=3&&repayment_period!=4");
					} else if (repayment_period == 3) {

					} else if (repayment_period == 4) {
						/**
						 * max只能是31天
						 */
						over_punish_interest = CalculationFormula.get_punish_principal_and_interest_of_only_31(should_repayment_total);
					} else {
						throw new RuntimeException("...程序不可能执行到这的");
					}

					{
						//
						all_principle_interest = all_principle_interest.add(should_repayment_principle).add(should_repayment_interest);
						all_principle = all_principle.add(should_repayment_principle);
						all_interest = all_interest.add(should_repayment_interest);
						all_over_punish_interest = all_over_punish_interest.add(over_punish_interest);
						//
						all_repayment = all_repayment.add(should_repayment_principle).add(should_repayment_interest).add(over_punish_interest);
					}
					if (Constants.devMode) {
						System.out.println("借款人每个还款计划详细信息    repayment_plan_id:" + repayment_plan_id + "   repayment_period:" + repayment_period);
						System.out.println("要还的本息" + (should_repayment_principle).add(should_repayment_interest));
						System.out.println("要还的本金" + should_repayment_principle);
						System.out.println("要还的利息" + should_repayment_interest);
						/**
						 * 不必关注管理费
						 */
						System.out.println("31天内的罚息" + over_punish_interest);//
					}

				}

			}

		}

		// 全部本息
		final BigDecimal all_principle_interest_of_final = all_principle_interest;
		// 全部本金
		final BigDecimal all_principle_of_final = all_principle;
		// 全部利息
		final BigDecimal all_interest_of_final = all_interest;
		// 全部罚息
		final BigDecimal all_over_punish_interest_of_final = all_over_punish_interest;
		// all
		final BigDecimal all_repayment_of_final = all_repayment;

		if (Constants.devMode) {
			System.out.println("全部本息" + all_principle_interest_of_final);
			System.out.println("全部本金" + all_principle_of_final);
			System.out.println("全部利息" + all_interest_of_final);
			System.out.println("全部罚息" + all_over_punish_interest_of_final);
			System.out.println(" all" + all_repayment_of_final);
		}

		/**
		 * 本息是所有的理财用户都需要进行平均分配的
		 */
		final BigDecimal each_principal_interest = all_principle_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本息
		/**
		 * 
		 * 定义当前需要还的每份的本金
		 */
		final BigDecimal each_principal = all_principle.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本金
		/**
		 * 定义当前需要还的每份的利息
		 */
		final BigDecimal each_interest = all_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的利息
		/**
		 * 定义借款人给理财人的还款罚息
		 */
		final long repayment_plan_id_ = repayment_plan_id;
		final BigDecimal each_punish_interest = all_over_punish_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);
		final List<FinancialRecipientInformation> financial_recipient_information_list = new ArrayList<FinancialRecipientInformation>();
		boolean ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				// 到债权持有表中查找每个理财人对应的持有份数, 来增加他们资金表中的可用余额
				String sql_find_holders = "select id,user_id,hold_money,hold_share,transfer_share,is_transfer_all from lender_bulk_standard_creditor_right_hold where gather_money_order_id =? and hold_share!=0 ";
				List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find(sql_find_holders, new Object[] { gather_money_order_id });
				if (lender_bulk_standard_creditor_right_hold_list == null) {
					return false;
				}
				// 还给理财人
				/**
				 * 还款的时候债权的价值以及对应的价格和手续费会相应的按照折价的系数修改-
				 * 同时系统需要一种机制告诉系统所有需要通过债权转让获取的债权持有的理财人暂时不能操作
				 */
				for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {
					long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");// id
					int financial_user_id = lender_bulk_standard_creditor_right_hold.getInt("user_id"); // 理财人id
					BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
					int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share"); // 原始购买的份额
					int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");// 转出的份额
					if (transfer_share != 0) {
						/**
						 * 严重逾期处理的时候需要让债权失效 如果处于正在转让的需要撤掉
						 */
						boolean is_ok_update_lender_bulk_standard_creditor_right_hold = lender_bulk_standard_creditor_right_hold.set("transfer_money", 0).set("transfer_share", 0).update();
						if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
							return false;
						}
						boolean is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE creditor_right_hold_id=? and state=1", new Object[] { creditor_right_hold_id }) >= 0;
						if (!is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out) {
							return false;
						}

					}
					/**
					 * 
					 * 定义当前需要还的每份的本金
					 */
					// 应该收到的钱由两部分构成
					BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本息

					BigDecimal principal = each_principal.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本金

					BigDecimal interest = each_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的利息

					//						
					BigDecimal punish_interest = each_punish_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的罚息

					// 应该回收的金额:principal_interest+punish_interest
					BigDecimal recieve_money = principal_interest.add(punish_interest);

					if (Constants.devMode) {
						System.out.println("financial_user_id:" + financial_user_id);
						System.out.println("理财人具体收到的本息" + principal_interest);
						System.out.println("理财人具体收到的本金" + principal);
						System.out.println("理财人具体收到的利息" + interest);
						System.out.println("理财人具体收到的罚息" + punish_interest);
						System.out.println("recieve_money=" + recieve_money);
					}

					UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(financial_user_id);
					BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
					BigDecimal cny_freeze_of_lender = user_now_money_of_lender.getBigDecimal("cny_freeze");

					/**
					 * 理财人回款
					 */
					boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender.add(recieve_money)).update();
					if (!is_ok_update_user_now_money_of_lender) {
						return false;
					}
					/**
					 * 需要记录资金信息
					 * 
					 * <pre>
					 * sys_expenses_records no
					 * sys_income_records no
					 * 
					 * user_money_change_records yes 
					 * user_transaction_records yes
					 * </pre>
					 */
					// user_money_change_records
					boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_74, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息#还款计划id" + repayment_plan_id_);
					if (!is_ok_add_user_money_change_records_by_type) {
						return false;
					}
					is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(financial_user_id, UserMoneyChangeRecordsM.Type_76, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息#还款计划id"
							+ repayment_plan_id_);
					if (!is_ok_add_user_money_change_records_by_type) {
						return false;
					}

					// user_transaction_records
					boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(financial_user_id, UserTransactionRecordsM.Type_32, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息");
					if (!is_ok_add_user_transaction_records) {
						return false;
					}
					is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(financial_user_id, UserTransactionRecordsM.Type_39, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息");
					if (!is_ok_add_user_transaction_records) {
						return false;
					}

					/**
					 * 添加回款的财务记录
					 */
					boolean is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record = LenderBulkStandardCreditorRightHoldRepaymentRecordM.add_lender_bulk_standard_creditor_right_hold_repayment_record_to_system(financial_user_id,//
							user_id, //
							gather_money_order_id, //
							borrow_type,//
							nickname, //
							0,// repayment_plan_id
							borrower_bulk_standard_repayment_plan_list.size(),//
							0,// current_period
							creditor_right_hold_id,//
							hold_money,//
							hold_share,//
							principal,//
							interest,//
							principal_interest,//
							punish_interest,//
							new Date());//
					if (!is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record) {
						return false;
					}
					// 
					/**
					 *还款对象pay_for_object 0是理财人1是系统，pay_for_object=1告知系统以后是还给系统
					 */
					boolean is_ok_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("pay_for_object", 1).update();
					if (!is_ok_borrower_bulk_standard_gather_money_order) {
						return false;
					}
					// 记录进financial_recipient_information_list
					FinancialRecipientInformation financial_recipient_information = new FinancialRecipientInformation(//
							financial_user_id,//
							gather_money_order_id,//
							user_id,//
							0, // repayment_plan_id
							total_periods, //
							total_periods, // 当前第几期
							creditor_right_hold_id, //
							hold_money,//
							hold_share, //
							principal_interest, //
							punish_interest,//
							new Date(), //
							new Date().getTime(),//
							borrow_title);//
					financial_recipient_information_list.add(financial_recipient_information);

				}// for
				/**
				 * 让所有理财人的持有失效-系统不用再给相关的理财人进行本息归还
				 */
				boolean is_ok_set_all_is_not_need_handle = LenderBulkStandardCreditorRightHoldLogM.set_all_is_not_need_handle(gather_money_order_id);
				if (!is_ok_set_all_is_not_need_handle) {
					return false;
				}
				// 不能改变还款计划is_repay状态，因为借款人没有还
				// 记录到系统支出-还给理财人的-不是系统-所以不需要系统收支
				boolean add_borrower_bulk_standard_repayment_by_system_ok = BorrowerBulkStandardRepaymentBySystemM.add_borrower_bulk_standard_repayment_by_system(//
						gather_money_order_id,//
						user_id,//
						borrower_bulk_standard_repayment_plan_list.size(), //
						all_principle_interest_of_final, //
						all_principle_of_final,//
						all_interest_of_final,//
						all_over_punish_interest_of_final, //
						all_repayment_of_final, //
						gson.toJson(repayment_periods_map));//
				if (!add_borrower_bulk_standard_repayment_by_system_ok) {
					return false;
				}
				return true;
			}
		});
		if (!ok) {
			logger.error("严重逾期处理失败,筹集单id=" + gather_money_order_id);
		} else {
			if (Utils.isHasData(financial_recipient_information_list)) {
				SendBackTheSuccessfulInformation.send_back_the_successful_information_of_all_with_overdue(financial_recipient_information_list);
			}
		}
	}
}

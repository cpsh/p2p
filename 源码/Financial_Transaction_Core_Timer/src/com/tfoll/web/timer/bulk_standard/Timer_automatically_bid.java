package com.tfoll.web.timer.bulk_standard;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.LenderBulkStandardOrderM;
import com.tfoll.web.model.UserAutomaticallyBidM;
import com.tfoll.web.model.UserAutomaticallyBidTemporaryRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *自动投标
 */
public class Timer_automatically_bid extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_automatically_bid.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.debug("系统框架还没有启动好,定时任务:" + Timer_automatically_bid.class.getName() + "暂时还不能执行");
			return;
		}
		if (isRunning.get()) {
			logger.debug("还有其他的线程正在执行定时任务:" + Timer_automatically_bid.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.debug("定时任务:" + Timer_automatically_bid.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}

				}
				logger.debug("定时任务:" + Timer_automatically_bid.class.getName() + "结束");
			} catch (Exception e) {
				logger.debug("定时任务:" + Timer_automatically_bid.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	/**
	 * 具体业务处理
	 */
	public void doTask() throws Exception {
		// 检测是否有符合条件筹集单子
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order();
		if (borrower_bulk_standard_gather_money_order == null) {
			// 外面的单子不是时时刻刻，同时需要解决我们系统手动投标的时候数据库死锁竞争的问题
			TimeUnit.SECONDS.sleep(36);
			/**
			 * 删掉数据库里面8天之前的数据
			 */
			UserAutomaticallyBidTemporaryRecordsM.delete_8_day_temporary_records();
		} else {
			List<UserAutomaticallyBidM> user_automatically_bid_list = UserAutomaticallyBidM.get_user_automatically_bid_list_of_only_user_id();
			if (Utils.isHasData(user_automatically_bid_list)) {

				for (UserAutomaticallyBidM user_automatically_bid : user_automatically_bid_list) {
					doSubTaskFor(user_automatically_bid);
				}
			}
		}

	}

	public void doSubTaskFor(UserAutomaticallyBidM user_automatically_bid) throws Exception {
		int user_id = user_automatically_bid.get("user_id");
		user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);
		int disjunctor = user_automatically_bid.getInt("disjunctor");
		if (disjunctor == 1) {
			doSubTaskForAutoBid(user_automatically_bid);
		}
	}

	public static void doSubTaskForAutoBid(UserAutomaticallyBidM user_automatically_bid) throws Exception {
		int user_id = user_automatically_bid.get("user_id");
		user_automatically_bid = UserAutomaticallyBidM.dao.findById(user_id);
		if (user_automatically_bid == null) {
			return;
		}
		int disjunctor = user_automatically_bid.getInt("disjunctor");
		if (disjunctor == 0) {
			return;
		}

		BigDecimal bid_amount = user_automatically_bid.getBigDecimal("bid_amount");// 投标金额
		BigDecimal annulized_rate_min = user_automatically_bid.getBigDecimal("annulized_rate_min");// 年化利率(最小)
		BigDecimal annulized_rate_max = user_automatically_bid.getBigDecimal("annulized_rate_max");// 年化利率(最大)
		int borrow_duration_min = user_automatically_bid.getInt("borrow_duration_min");// 最小借款期限（月）
		int borrow_duration_max = user_automatically_bid.getInt("borrow_duration_max");// 最大借款期限（月）

		int borrow_duration_day_min = user_automatically_bid.getInt("borrow_duration_day_min");// 最小借款期限（天）
		int borrow_duration_day_max = user_automatically_bid.getInt("borrow_duration_day_max");// 最大借款期限（天）
		int credit_rate_min_int = user_automatically_bid.getInt("credit_rate_min_int");// 最小信用等级int
		int credit_rate_max_int = user_automatically_bid.getInt("credit_rate_max_int");// 最大信用等级int
		BigDecimal reserved_amount = user_automatically_bid.getBigDecimal("reserved_amount"); // 保留金额

		BigDecimal cny_can_used_now = Db.queryBigDecimal("select cny_can_used from user_now_money where user_id = ?", user_id);// 用户可用金额
		BigDecimal $0 = new BigDecimal("0");

		// 再次检测 符合条件凑集单子
		BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order(//
				annulized_rate_min,//
				annulized_rate_max, //
				borrow_duration_min,//
				borrow_duration_max,//
				borrow_duration_day_min, //
				borrow_duration_day_max, //
				credit_rate_min_int,//
				credit_rate_max_int);//

		if (borrower_bulk_standard_gather_money_order != null) {
			int user_id_of_borrower = borrower_bulk_standard_gather_money_order.getInt("user_id");
			if (user_id_of_borrower == user_id) {
				return;// 绝对不能投自己
			}
			/**
			 * 无论处理是否成功 都需要向一张表里面记录已经自动向该表里面投标了(目前不用)
			 */
			if (UserAutomaticallyBidTemporaryRecordsM.is_can_auto_bid(user_id, borrower_bulk_standard_gather_money_order.getLong("id"))) {

				long gather_money_order_id = borrower_bulk_standard_gather_money_order.get("id", 0);
				BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");// 筹集单借款总金额
				BigDecimal have_gather_money = borrower_bulk_standard_gather_money_order.getBigDecimal("have_gather_money");// 筹集单已经筹集到金额
				BigDecimal annulized_rate = borrower_bulk_standard_gather_money_order.getBigDecimal("annulized_rate");// 筹集单年化利率
				int borrow_duration = borrower_bulk_standard_gather_money_order.getInt("borrow_duration");// 借款期限（月）
				int borrow_duration_day = borrower_bulk_standard_gather_money_order.getInt("borrow_duration_day");// 借款期限（天）
				int credit_rating_int = borrower_bulk_standard_gather_money_order.getInt("credit_rating_int");// 信用等级int型

				// 投标金额 要小于 标总额度20% 只投20% 的且为50倍金额
				BigDecimal rate = bid_amount.divide(borrow_all_money, 4, BigDecimal.ROUND_DOWN);
				BigDecimal borrow_all_money_20 = borrow_all_money.multiply(new BigDecimal("0.2"));// 总投标金额的95%
				int remainder_20 = borrow_all_money_20.intValue() % 50;
				if (rate.compareTo(new BigDecimal("0.2")) == 1) {
					bid_amount = borrow_all_money.multiply(new BigDecimal("0.2")).subtract(new BigDecimal(remainder_20));
				}

				// 若投标后，超过95%，只投不超过95%且为50倍数金额。
				BigDecimal borrow_all_money_95 = borrow_all_money.multiply(new BigDecimal("0.95"));// 总投标金额的95%
				int remainder_95 = borrow_all_money_95.intValue() % 50;
				if (bid_amount.add(have_gather_money).compareTo(borrow_all_money_95) == 1) {
					bid_amount = borrow_all_money_95.subtract(have_gather_money).subtract(new BigDecimal(remainder_95 + ""));
				}

				/**
				 * 1.用户资金 — 保留金额 — 投标金额 >= 0; 2.年化利率范围; 3.借款期限; 4. 信用等级范围
				 */
				if (//
				(cny_can_used_now.subtract(bid_amount).subtract(reserved_amount).compareTo($0) > -1) //
						&& //
						(annulized_rate.compareTo(annulized_rate_min) != -1 && annulized_rate.compareTo(annulized_rate_max) != 1)//
						&& (//
						(borrow_duration != 0 && (borrow_duration >= borrow_duration_min && borrow_duration <= borrow_duration_max)) // 凑集单是月,borrow_duration_min-可以是0
						|| //
						(borrow_duration == 0 && (borrow_duration_min == 0 && (borrow_duration_day >= borrow_duration_day_min) && (borrow_duration_day <= borrow_duration_day_max || borrow_duration_day_max == 0)))// 凑集单是天的,min为天，max为月
						) //
						&& (credit_rate_min_int >= credit_rating_int && credit_rating_int >= credit_rate_max_int)) {

					bid(user_id, gather_money_order_id, bid_amount);
				}
			}
		}

	}

	/**
	 * 投标
	 * 
	 * @param lend_user_id
	 * @param gather_money_order_id
	 * @param invest_money
	 */
	public static void bid(final int lend_user_id, final long gather_money_order_id, final BigDecimal invest_money) {
		if (lend_user_id == 0) {
			return;
		}
		if (gather_money_order_id == 0) {
			return;
		}
		if (invest_money == null) {
			return;
		}
		int invest_money_int = invest_money.intValue();// 投标金额
		if (invest_money_int == 0) {
			return;
		}
		if (invest_money_int % 50 != 0) {
			return;
		}
		final int invest_share = invest_money_int / 50; // 投资的份额
		/**
		 * 判断资金够不
		 */
		String sql = "select cny_can_used from user_now_money where user_id = ? ";
		BigDecimal money_can_use = Db.queryBigDecimal(sql, new Object[] { lend_user_id });
		int result = money_can_use.compareTo(invest_money);
		if (result < 0) {
			// 你的余额不足，请重新填写投资金额
			return;
		}
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				// 处理筹款单
				// 线程安全获取筹款单信息，加上等待超时功能
				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.get_borrower_bulk_standard_gather_money_order_short_wait(gather_money_order_id);
				if (borrower_bulk_standard_gather_money_order == null) {
					return false;
				}
				int borrow_user_id = borrower_bulk_standard_gather_money_order.getInt("user_id");// 筹款人id

				@SuppressWarnings("unused")
				BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
				int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");

				BigDecimal have_gather_money = borrower_bulk_standard_gather_money_order.getBigDecimal("have_gather_money");// 已筹集的金额
				int have_gather_share = borrower_bulk_standard_gather_money_order.getInt("have_gather_share");// 已筹集的份额

				BigDecimal remain_money = borrower_bulk_standard_gather_money_order.getBigDecimal("remain_money");// 剩余金额
				int remain_share = borrower_bulk_standard_gather_money_order.getInt("remain_share");// 剩余份额

				int gather_progress = ((have_gather_share + invest_share) * 100) / borrow_all_share;// 筹集进度-先*100防止计算精度失败
				int gather_progress_level = 0;// 筹集进度分类
				if (gather_progress < 50) {
					gather_progress_level = 1;
				} else if ((50 <= gather_progress) && gather_progress < 80) {
					gather_progress_level = 2;
				} else if ((80 <= gather_progress) && gather_progress < 100) {
					gather_progress_level = 3;
				} else if (gather_progress == 100) {
					gather_progress_level = 4;
				}

				int join_people_num = borrower_bulk_standard_gather_money_order.getInt("join_people_num");

				/**
				 *优先级 凑集时间>>凑集状态>>剩下的份额进行
				 */

				Date deadline = borrower_bulk_standard_gather_money_order.getDate("deadline");// 筹款截止时间
				Date now = new Date();
				if (now.compareTo(deadline) > 0) { // 该筹款单已经过期，不能投资
					return false;
				}
				int gather_state = borrower_bulk_standard_gather_money_order.get("gather_state");
				if (gather_state != 1) {
					return false;
				}
				if (remain_share < invest_share) { // 剩余的份额不足，不能投资
					return false;
				}

				borrower_bulk_standard_gather_money_order.set("have_gather_money", have_gather_money.add(invest_money));// 增加已筹集金额
				borrower_bulk_standard_gather_money_order.set("have_gather_share", have_gather_share + invest_share);// 增加已筹集金额

				borrower_bulk_standard_gather_money_order.set("remain_money", remain_money.subtract(invest_money));// 减少剩余金额
				borrower_bulk_standard_gather_money_order.set("remain_share", remain_share - invest_share); // 减少剩余份额

				borrower_bulk_standard_gather_money_order.set("gather_progress", gather_progress); // 筹集进度
				borrower_bulk_standard_gather_money_order.set("gather_progress_level", gather_progress_level);
				borrower_bulk_standard_gather_money_order.set("join_people_num", join_people_num + 1); // 加入人次加1

				if (remain_share == invest_share) { // 如果投资金额正好=剩余金额,就是筹集成功

					Date add_time = borrower_bulk_standard_gather_money_order.getDate("add_time"); // 筹款开始时间
					int full_scale_use_time = (int) ((now.getTime() - add_time.getTime()) / 1000); // 满标用时（秒）
					borrower_bulk_standard_gather_money_order.set("full_scale_use_time", full_scale_use_time);
					borrower_bulk_standard_gather_money_order.set("finish_time", now);

				}
				boolean is_ok_update_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.update();

				// 添加 理财人 投资订单表
				LenderBulkStandardOrderM lender_bulk_standard_order = new LenderBulkStandardOrderM();
				lender_bulk_standard_order.set("gather_money_order_id", gather_money_order_id);
				lender_bulk_standard_order.set("lend_user_id", lend_user_id); // 理财人id
				lender_bulk_standard_order.set("borrow_user_id", borrow_user_id);// 借款人id
				lender_bulk_standard_order.set("invest_money", invest_money);
				lender_bulk_standard_order.set("invest_share", invest_share);
				lender_bulk_standard_order.set("bid_time", new Date());
				lender_bulk_standard_order.set("bid_time_long", System.currentTimeMillis());
				lender_bulk_standard_order.set("state", 1);

				boolean is_ok_save_lender_bulk_standard_order = lender_bulk_standard_order.save();

				// 处理 理财人 资金表
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(lend_user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

				BigDecimal cny_can_used_later = cny_can_used.subtract(invest_money);
				BigDecimal cny_freeze_later = cny_freeze.add(invest_money);

				user_now_money.set("cny_can_used", cny_can_used_later);
				user_now_money.set("cny_freeze", cny_freeze_later);

				boolean is_ok_update_user_now_money = user_now_money.update();

				if (is_ok_update_borrower_bulk_standard_gather_money_order && is_ok_save_lender_bulk_standard_order && is_ok_update_user_now_money) {
					return true;
				} else {
					return false;
				}
			}
		});

		if (is_ok) {
			return;
		} else {
			return;
		}

	}
}
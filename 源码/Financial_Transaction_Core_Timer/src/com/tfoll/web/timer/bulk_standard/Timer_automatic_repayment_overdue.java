package com.tfoll.web.timer.bulk_standard;

import com.google.gson.Gson;
import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.domain.FinancialRecipientInformation;
import com.tfoll.web.model.BorrowerBulkStandardGatherMoneyOrderM;
import com.tfoll.web.model.BorrowerBulkStandardOverdueRepaymentRecordM;
import com.tfoll.web.model.BorrowerBulkStandardRepaymentPlanM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldM;
import com.tfoll.web.model.LenderBulkStandardCreditorRightHoldRepaymentRecordM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.CalculationFormula;
import com.tfoll.web.util.SendBackTheSuccessfulInformation;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 普通逾期处理
 * 
 */
public class Timer_automatic_repayment_overdue extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_automatic_repayment_overdue.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_automatic_repayment_overdue.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_automatic_repayment_overdue.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_automatic_repayment_overdue.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				logger.info("定时任务:" + Timer_automatic_repayment_overdue.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_automatic_repayment_overdue.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		List<UserM> user_list = UserM.dao.find("SELECT id,nickname from user_info");
		for (UserM user : user_list) {
			try {
				doSubTaskFor(user);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}

	}

	static Gson gson = new Gson();

	/**
	 * 普通逾期处理
	 * 
	 */
	public static void doSubTaskFor(final UserM borrower_user) throws Exception {
		final int user_id = borrower_user.getInt("id");
		final BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("select * from borrower_bulk_standard_gather_money_order where user_id = ? and payment_state=2 order by id desc limit 1", user_id);
		if (borrower_bulk_standard_gather_money_order == null) {
			return;
		}
		final long gather_money_order_id = borrower_bulk_standard_gather_money_order.getLong("id");
		final List<BorrowerBulkStandardRepaymentPlanM> borrower_bulk_standard_repayment_plan_list = BorrowerBulkStandardRepaymentPlanM.dao.find("SELECT * from borrower_bulk_standard_repayment_plan where is_repay = 0 and gather_money_order_id=? order by id asc", new Object[] { gather_money_order_id });
		for (BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan : borrower_bulk_standard_repayment_plan_list) {
			long repay_end_time_long = borrower_bulk_standard_repayment_plan.getLong("repay_end_time_long");
			if (System.currentTimeMillis() > repay_end_time_long) {
				try {
					automatic_repayment_for_overdue(borrower_user, gather_money_order_id, borrower_bulk_standard_repayment_plan);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}

			}
		}
	}

	public static void automatic_repayment_for_overdue(final UserM borrower_user, final long gather_money_order_id, final BorrowerBulkStandardRepaymentPlanM borrower_bulk_standard_repayment_plan) {
		final int user_id_of_borrower = borrower_user.getInt("id");
		/**
		 * 需要记录那些成功还款的还款计划-因为我们需要发送成功的还款通知消息-这个是不能写在核心的代码里面-怕影响核心流程的正常的执行
		 */
		/**
		 * 系统是2014-12-23 17:08:20开始逾期,该时间是系统正式上线后一个月的时间
		 */
		final List<Long> has_pay_repayment_plan_id_list = new ArrayList<Long>();
		final Map<Long, List<FinancialRecipientInformation>> financial_recipient_information_list_map = new HashMap<Long, List<FinancialRecipientInformation>>();

		boolean ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				BorrowerBulkStandardGatherMoneyOrderM borrower_bulk_standard_gather_money_order = BorrowerBulkStandardGatherMoneyOrderM.dao.findFirst("SELECT * from borrower_bulk_standard_gather_money_order where user_id = ? order by id desc", new Object[] { user_id_of_borrower });// 至少含有几个还款计划
				if (borrower_bulk_standard_gather_money_order == null) {
					return false;
				}
				final long gather_money_order_id = borrower_bulk_standard_gather_money_order.getLong("id");
				final BigDecimal borrow_all_money = borrower_bulk_standard_gather_money_order.getBigDecimal("borrow_all_money");
				int pay_for_object = borrower_bulk_standard_gather_money_order.getInt("pay_for_object");

				final long repayment_plan_id = borrower_bulk_standard_repayment_plan.getLong("id");
				int total_periods = borrower_bulk_standard_repayment_plan.getInt("total_periods");
				int current_period = borrower_bulk_standard_repayment_plan.getInt("current_period");
				Date repay_end_time = borrower_bulk_standard_repayment_plan.getDate("repay_end_time");
				// 本息
				final BigDecimal should_repayment_total = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_total");
				// 本金
				final BigDecimal should_repayment_principle = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_principle");
				// 利息
				final BigDecimal should_repayment_interest = borrower_bulk_standard_repayment_plan.getBigDecimal("should_repayment_interest");
				// 正常管理费
				final BigDecimal normal_manage_fee = borrow_all_money.multiply(new BigDecimal("0.003"));
				// 罚息
				final BigDecimal over_punish_interest = CalculationFormula.get_punish_manage_fee(should_repayment_total, repay_end_time);
				// 逾期管理费
				final BigDecimal over_manage_fee = CalculationFormula.get_punish_principal_and_interest(should_repayment_total, repay_end_time);
				final BigDecimal actual_repayment = should_repayment_total.add(normal_manage_fee).add(over_manage_fee).add(over_punish_interest);

				int borrow_all_share = borrower_bulk_standard_gather_money_order.getInt("borrow_all_share");

				//
				UserNowMoneyM user_now_money_of_borrower = UserNowMoneyM.get_user_current_money_for_update(user_id_of_borrower);
				BigDecimal cny_can_used_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze_of_borrower = user_now_money_of_borrower.getBigDecimal("cny_freeze");
				if (cny_can_used_of_borrower.compareTo(actual_repayment) < 0) {
					return false;
				}
				// 更新还款计划
				boolean update_borrower_bulk_standard_repayment_plan = borrower_bulk_standard_repayment_plan.set("is_repay", 1).set("over_manage_fee", over_manage_fee).set("over_punish_interest", over_punish_interest).set("actual_repayment", actual_repayment).update();
				if (!update_borrower_bulk_standard_repayment_plan) {
					return false;
				}
				// 更新借款人资金
				boolean is_ok_update_user_now_money_of_borrower = user_now_money_of_borrower.set("cny_can_used", cny_can_used_of_borrower.subtract(actual_repayment)).update();
				if (!is_ok_update_user_now_money_of_borrower) {
					return false;
				}

				/**
				 * 需要区分那些是打款给理财人或者系统的
				 * 
				 * <pre>
				 * sys_expenses_records no
				 * sys_income_records yes
				 * 
				 * user_money_change_records yes 
				 * user_transaction_records yes
				 * </pre>
				 */
				boolean is_ok_add_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, normal_manage_fee, "正常管理费");
				if (!is_ok_add_add_sys_income_records) {
					return false;
				}
				is_ok_add_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, over_manage_fee, "逾期管理费");
				if (!is_ok_add_add_sys_income_records) {
					return false;
				}
				// 用户资金变化
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_33, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息#还款计划id"
						+ repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_35, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee), "偿还管理费#还款计划id"
						+ repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_36, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(
						over_manage_fee), "逾期管理费#还款计划id" + repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id_of_borrower, UserMoneyChangeRecordsM.Type_37, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_punish_interest, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(
						over_manage_fee).subtract(over_punish_interest), "罚息#还款计划id" + repayment_plan_id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				// user_transaction_records
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_32, cny_can_used_of_borrower.add(cny_freeze_of_borrower), should_repayment_total, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total), "偿还本息");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), normal_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee), "正常管理费");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_manage_fee, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(over_manage_fee),
						"逾期管理费");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id_of_borrower, UserTransactionRecordsM.Type_39, cny_can_used_of_borrower.add(cny_freeze_of_borrower), over_punish_interest, new BigDecimal(0), cny_can_used_of_borrower.add(cny_freeze_of_borrower).subtract(should_repayment_total).subtract(normal_manage_fee).subtract(
						over_manage_fee).subtract(over_punish_interest), "逾期的罚息");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}

				// 判断是不是最后的一期
				final boolean is_last_period = total_periods == current_period;
				if (is_last_period) {
					/**
					 * 如果还款计划里面最后一期之前的所有都还款成功,那么则可以表示整个凑集单都成功还款
					 */
					boolean has_not_pay_all = Db.queryLong("SELECT COUNT(1) from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id=? and is_repay=0 and id!=?", new Object[] { gather_money_order_id, repayment_plan_id }) >= 0;
					if (has_not_pay_all) {
						{
							/**
							 * 更新借款人的借款单信息和用户信息
							 */

							// 只要是最后的一期-凑集单的还款的状态会变成还款成功
							boolean is_ok_update_borrower_bulk_standard_gather_money_order = borrower_bulk_standard_gather_money_order.set("payment_state", 3).set("payment_finish_time", new Date()).update();
							if (!is_ok_update_borrower_bulk_standard_gather_money_order) {
								return false;
							}

							// is_not_pay还款完成的时候需要修改is_not_pay =0,borrow_type=0
							boolean is_update_user_info_is_not_pay_ok = Db.update("UPDATE user_info SET is_not_pay =0,borrow_type=0 WHERE id=?", new Object[] { user_id_of_borrower }) >= 0;
							if (!is_update_user_info_is_not_pay_ok) {
								return false;
							}
							// 如果当前没有处于严重逾期状态,则需要把该凑集单的所有的持有和正在转让的单子撤掉
							// 见下面...非逾期状态-债权持有处理之后

						}
					}

				}

				if (pay_for_object == 1) {

					// 还给系统-如果系统进行对这个资金进行处理了-那么这部分的钱应该入账了且可以被公司使用
					boolean is_ok_add_borrower_bulk_standard_overdue_repayment_record = BorrowerBulkStandardOverdueRepaymentRecordM.add_borrower_bulk_standard_overdue_repayment_record(//
							gather_money_order_id,//
							repayment_plan_id, //
							total_periods, //
							current_period,//
							should_repayment_total, //
							should_repayment_principle, //
							should_repayment_interest,//
							normal_manage_fee,//
							over_manage_fee, //
							over_punish_interest,//
							actual_repayment);//
					if (!is_ok_add_borrower_bulk_standard_overdue_repayment_record) {
						return false;
					} else {
						return true;
					}

				} else {
					int repayment_period = CalculationFormula.get_repayment_period_with_now(borrower_bulk_standard_repayment_plan.getDate("repay_end_time"), borrower_bulk_standard_repayment_plan.getString("automatic_repayment_date"));// 得到当前的还款期间
					if (!(repayment_period == 1 || repayment_period == 2 || repayment_period == 3 || repayment_period == 4)) {
						return false;
					}
					// 处于严重逾期 要交给严重逾期处理
					if (repayment_period == 4) {
						return false;
					}
					if (repayment_period != 3) {
						return false;
					}

					final List<FinancialRecipientInformation> financial_recipient_information_list = new ArrayList<FinancialRecipientInformation>();

					List<LenderBulkStandardCreditorRightHoldM> lender_bulk_standard_creditor_right_hold_list = LenderBulkStandardCreditorRightHoldM.dao.find("select * from lender_bulk_standard_creditor_right_hold where gather_money_order_id = ? and hold_share>0", new Object[] { gather_money_order_id });
					final List<Integer> lender_user_id_list = new ArrayList<Integer>();
					for (LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold : lender_bulk_standard_creditor_right_hold_list) {
						int lender_id = lender_bulk_standard_creditor_right_hold.getInt("user_id");
						lender_user_id_list.add(lender_id);
					}
					for (int lender_id : lender_user_id_list) {
						LenderBulkStandardCreditorRightHoldM lender_bulk_standard_creditor_right_hold = LenderBulkStandardCreditorRightHoldM.dao.findFirst("select * from lender_bulk_standard_creditor_right_hold where gather_money_order_id = ? and user_id=?", new Object[] { gather_money_order_id, lender_id });
						long creditor_right_hold_id = lender_bulk_standard_creditor_right_hold.getLong("id");
						int transfer_share = lender_bulk_standard_creditor_right_hold.getInt("transfer_share");
						int hold_share = lender_bulk_standard_creditor_right_hold.getInt("hold_share");
						BigDecimal hold_money = lender_bulk_standard_creditor_right_hold.getBigDecimal("hold_money");
						if (transfer_share != 0) {
							/**
							 *只要出现逾期-系统将该人的所有的正在债权转让的所有的转出单都撤销掉
							 */
							boolean is_ok_update_lender_bulk_standard_creditor_right_hold = lender_bulk_standard_creditor_right_hold.set("transfer_money", 0).set("transfer_share", 0).update();
							if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
								return false;
							}
							boolean is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE creditor_right_hold_id=? and state=1", new Object[] { creditor_right_hold_id }) >= 0;
							if (!is_ok_update_lender_bulk_standard_order_creditor_right_transfer_out) {
								return false;
							}
						}
						/**
						 * 本息是所有的理财用户都需要进行平均分配的
						 */
						BigDecimal each_principal_interest = should_repayment_total.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本息
						/**
						 * 
						 * 定义当前需要还的每份的本金
						 */
						BigDecimal each_principal = should_repayment_principle.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的本金
						/**
						 * 定义当前需要还的每份的利息
						 */
						BigDecimal each_interest = should_repayment_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN); // 平均每份所对应的利息
						/**
						 * 定义借款人给理财人的还款罚息
						 */
						BigDecimal each_punish_interest = over_punish_interest.divide(new BigDecimal(borrow_all_share + ""), 8, BigDecimal.ROUND_DOWN);

						// 应该收到的钱由两部分构成
						BigDecimal principal = each_principal.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本金
						BigDecimal interest = each_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的利息
						BigDecimal principal_interest = each_principal_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的本息
						//						
						BigDecimal punish_interest = each_punish_interest.multiply(new BigDecimal(hold_share + ""));// 理财人具体收到的罚息
						// 应该回收的金额:principal_interest+punish_interest
						BigDecimal recieve_money = principal_interest.add(punish_interest);

						UserNowMoneyM user_now_money_of_lender = UserNowMoneyM.get_user_current_money_for_update(lender_id);
						BigDecimal cny_can_used_of_lender = user_now_money_of_lender.getBigDecimal("cny_can_used");
						BigDecimal cny_freeze_of_lender = user_now_money_of_lender.getBigDecimal("cny_freeze");
						// 更新理财人资金
						boolean is_ok_update_user_now_money_of_lender = user_now_money_of_lender.set("cny_can_used", cny_can_used_of_lender.add(recieve_money)).update();
						if (!is_ok_update_user_now_money_of_lender) {
							return false;
						}

						/**
						 * 
						 * 财务记录
						 * 
						 * <pre>
						 * sys_expenses_records no
						 * sys_income_records no
						 * 
						 * user_money_change_records yes 
						 * user_transaction_records yes
						 * </pre>
						 */
						// 理财人的user_money_change_records记录
						is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(lender_id, UserMoneyChangeRecordsM.Type_74, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息#还款计划id" + repayment_plan_id);
						if (!is_ok_add_user_money_change_records_by_type) {
							return false;
						}
						if (over_manage_fee.compareTo(new BigDecimal("0")) > 0) {
							is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(lender_id, UserMoneyChangeRecordsM.Type_76, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息#还款计划id"
									+ repayment_plan_id);
							if (!is_ok_add_user_money_change_records_by_type) {
								return false;
							}
						}
						// user_transaction_records
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(lender_id, UserTransactionRecordsM.Type_32, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), principal_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest), "回收本息");
						if (!is_ok_add_user_transaction_records) {
							return false;
						}
						is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(lender_id, UserTransactionRecordsM.Type_39, cny_can_used_of_lender.add(cny_freeze_of_lender), new BigDecimal(0), punish_interest, cny_can_used_of_lender.add(cny_freeze_of_lender).add(principal_interest).add(punish_interest), "均摊借款人逾期的罚息");
						if (!is_ok_add_user_transaction_records) {
							return false;
						}

						/**
						 * 添加回款的财务记录
						 */
						boolean is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record = LenderBulkStandardCreditorRightHoldRepaymentRecordM.add_lender_bulk_standard_creditor_right_hold_repayment_record(lender_id, //
								user_id_of_borrower, gather_money_order_id, //
								borrower_bulk_standard_gather_money_order.getInt("borrow_type"),//
								borrower_user.getString("nickname"), //

								repayment_plan_id,// repayment_plan_id
								total_periods,//
								current_period,// current_period

								lender_bulk_standard_creditor_right_hold.getLong("id"),//
								hold_money,//
								hold_share,//

								principal_interest,//
								interest,//
								principal,//
								recieve_money,//
								each_principal_interest.add(each_punish_interest), new Date());//
						if (!is_ok_add_lender_bulk_standard_creditor_right_hold_repayment_record) {
							return false;
						}
						// 记录进financial_recipient_information_list
						FinancialRecipientInformation financial_recipient_information = new FinancialRecipientInformation(//
								lender_id,//
								gather_money_order_id,//
								user_id_of_borrower,//
								repayment_plan_id, //
								total_periods, //
								current_period, //
								lender_bulk_standard_creditor_right_hold.getLong("id"), //
								hold_money,//
								hold_share, //
								each_principal_interest, //
								each_punish_interest,//
								new Date(), //
								new Date().getTime(),//
								borrower_bulk_standard_gather_money_order.getString("borrow_title"));//

						financial_recipient_information_list.add(financial_recipient_information);
					}// for
					if (is_last_period) {
						/**
						 * 针对这个单子-如果该期的还款计划是最后的一期那么我们需要检查以前的所有的的还款计划是否还款成功,
						 * 如果成功则让该借款人的所有持有失效和债权转让单撤销
						 */
						long has_not_pay_all = Db.queryLong("SELECT COUNT(1) from borrower_bulk_standard_repayment_plan WHERE gather_money_order_id=?  and id!=?  and is_repay=0", new Object[] { gather_money_order_id, repayment_plan_id });
						if (has_not_pay_all == 0) {// 以前都还清了的
							{
								boolean is_ok_update_lender_bulk_standard_creditor_right_hold = Db.update("UPDATE lender_bulk_standard_creditor_right_hold SET transfer_money=0,transfer_share=0,is_need_handle=0 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
								if (!is_ok_update_lender_bulk_standard_creditor_right_hold) {
									return false;
								}
								boolean is_ok_lender_bulk_standard_order_creditor_right_transfer_out = Db.update("UPDATE lender_bulk_standard_order_creditor_right_transfer_out set state=3 WHERE gather_money_order_id=?", new Object[] { gather_money_order_id }) >= 0;
								if (!is_ok_lender_bulk_standard_order_creditor_right_transfer_out) {
									return false;
								}
							}
						}

					}

					financial_recipient_information_list_map.put(borrower_bulk_standard_repayment_plan.getLong("id"), financial_recipient_information_list);
					has_pay_repayment_plan_id_list.add(borrower_bulk_standard_repayment_plan.getLong("id"));// 保证有序性

				}

				return true;
			}

		});
		if (ok) {
			if (Utils.isHasData(has_pay_repayment_plan_id_list)) {
				SendBackTheSuccessfulInformation.send_back_the_successful_information(has_pay_repayment_plan_id_list, financial_recipient_information_list_map);
			}
		}
	}

}

package com.tfoll.web.timer.fix_bid;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.FixBidUserNewerBidM;
import com.tfoll.web.model.FixBidUserNewerBidWaitingM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 散标新手表利息自动还款
 */
public class Timer_fix_bid_newer_bid_return_interest extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_fix_bid_newer_bid_return_interest.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_fix_bid_newer_bid_return_interest.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_fix_bid_newer_bid_return_interest.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_fix_bid_newer_bid_return_interest.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();

					}
				}
				logger.info("定时任务:" + Timer_fix_bid_newer_bid_return_interest.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_fix_bid_newer_bid_return_interest.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		String now_day = Model.Date.format(new Date());
		/**
		 * 处理用户投新手标到期收利息的情况-要求联富宝和散标新手标的时候必须把他们的业务主键放入等待表里面，等待后端定时器进行自动处理
		 */
		List<FixBidUserNewerBidWaitingM> fix_bid_user_newer_bid_waiting_list = FixBidUserNewerBidWaitingM.dao.find("SELECT id,user_newer_bid_id from fix_bid_user_newer_bid_waiting WHERE recieve_date=?", new Object[] { now_day });
		if (Utils.isHasData(fix_bid_user_newer_bid_waiting_list)) {
			for (FixBidUserNewerBidWaitingM fix_bid_user_newer_bid_waiting : fix_bid_user_newer_bid_waiting_list) {
				try {
					doSubTaskFor(fix_bid_user_newer_bid_waiting);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();

				}
			}
		}

	}

	/**
	 * 根据等待表ID进行利息返还
	 */
	// id,user_newer_bid_id
	private static void doSubTaskFor(final FixBidUserNewerBidWaitingM fix_bid_user_newer_bid_waiting) {
		final long user_newer_bid_id = fix_bid_user_newer_bid_waiting.getLong("user_newer_bid_id");

		final FixBidUserNewerBidM fix_bid_user_newer_bid = FixBidUserNewerBidM.dao.findFirst("SELECT id,user_id,bid_money,annual_rate,days,earnings,is_end,is_return_interest from  fix_bid_user_newer_bid WHERE id=?", new Object[] { user_newer_bid_id });
		if (fix_bid_user_newer_bid == null) {
			fix_bid_user_newer_bid_waiting.delete();
			return;
		}
		final int user_id = fix_bid_user_newer_bid.getInt("user_id");
		int is_return_interest = fix_bid_user_newer_bid.getInt("is_return_interest");
		if (is_return_interest == 1) {
			// 如果还款了就从等待表删除
			fix_bid_user_newer_bid_waiting.delete();
			return;
		}
		/**
		 * 利息转移
		 */
		Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {

				boolean is_ok_delete_fix_bid_user_newer_bid_waiting = fix_bid_user_newer_bid_waiting.delete();
				if (!is_ok_delete_fix_bid_user_newer_bid_waiting) {
					return false;
				}
				/**
				 * 需要对这部分的资金进行计算earnings
				 */
				int bid_money = fix_bid_user_newer_bid.getInt("bid_money");
				BigDecimal annual_rate = fix_bid_user_newer_bid.getBigDecimal("annual_rate");
				int days = fix_bid_user_newer_bid.getInt("days");

				BigDecimal earnings = new BigDecimal(bid_money + "").multiply(annual_rate).multiply(new BigDecimal(days + "")).divide(new BigDecimal("366"), 8, BigDecimal.ROUND_DOWN);// 366为标准
				boolean is_ok_update_fix_bid_user_newer_bid = fix_bid_user_newer_bid.//
						set("is_end", 1).//
						set("is_return_interest", 1).//
						set("earnings", earnings).//
						update();
				if (!is_ok_update_fix_bid_user_newer_bid) {
					return false;
				}
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);
				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.add(earnings)).update();
				if (!is_ok_update_user_now_money) {
					return false;
				}
				/**
				 * 添加财务记录
				 * 
				 * <pre>
				 * sys_expenses_records-yes
				 * sys_income_records-no
				 * user_money_change_records-yes
				 * user_transaction_records-yes
				 * </pre>
				 */
				boolean is_ok_add_sys_expenses_records = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_202, earnings, "支出联富宝新手标利息,新手标ID" + user_newer_bid_id);
				if (!is_ok_add_sys_expenses_records) {
					return false;
				}
				boolean is_ok_add_user_money_change_records = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_202, cny_can_used.add(cny_freeze), new BigDecimal("0"), earnings, cny_can_used.add(cny_freeze).add(earnings), "支出联富宝新手标利息#新手标ID" + user_newer_bid_id);
				if (!is_ok_add_user_money_change_records) {
					return false;
				}
				boolean is_ok_add_user_transaction_records_by_type = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id, UserTransactionRecordsM.Type_202, cny_can_used.add(cny_freeze), new BigDecimal("0"), earnings, cny_can_used.add(cny_freeze).add(earnings), "支出联富宝新手标利息");
				if (!is_ok_add_user_transaction_records_by_type) {
					return false;
				}
				return true;
			}
		});

	}

}
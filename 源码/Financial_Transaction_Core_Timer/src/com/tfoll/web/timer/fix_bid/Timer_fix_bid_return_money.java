package com.tfoll.web.timer.fix_bid;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.model.Model;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.FixBidUserExitM;
import com.tfoll.web.model.FixBidUserHoldM;
import com.tfoll.web.model.SysExpensesRecordsM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.model.UserNowMoneyM;
import com.tfoll.web.model.UserTransactionRecordsM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 针对联富包自动还款
 */
public class Timer_fix_bid_return_money extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_fix_bid_return_money.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_fix_bid_return_money.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_fix_bid_return_money.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_fix_bid_return_money.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();

					}
				}
				logger.info("定时任务:" + Timer_fix_bid_return_money.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_fix_bid_return_money.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}
	}

	public static void doTask() throws Exception {
		long now = (new Date()).getTime();
		long half_hour_ago = (long) (now - Model.Hour * 0.5);
		long half_hour_after = (long) (now + Model.Hour * 0.5);
		/**
		 * 位于截止时间前半个小时或者后半个小时
		 */
		List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find("SELECT * from fix_bid_system_order WHERE repayment_time_long>=? and repayment_time_long<=? and is_pay_back=0", new Object[] { half_hour_ago, half_hour_after });
		if (Utils.isHasData(fix_bid_system_order_list)) {// 一般查询出来有三条记录
			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				try {
					doSubTaskFor(fix_bid_system_order);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * 针对联富宝当时该还款的持有进行还款
	 */
	private static void doSubTaskFor(FixBidSystemOrderM fix_bid_system_order) {
		long fix_bid_sys_id = fix_bid_system_order.getLong("id");
		List<FixBidUserHoldM> fix_bid_user_hold_list = FixBidUserHoldM.dao.find("SELECT id,bid_name,bid_type,fix_bid_sys_id,user_id,nick_name,real_name,annual_earning,bid_money,earned_incom,state,exit_time, exit_time_long, lock_time, lock_time_long from fix_bid_user_hold WHERE fix_bid_sys_id=? and state=1", new Object[] { fix_bid_sys_id });

		if (!Utils.isHasData(fix_bid_user_hold_list)) {// 可能持有的都自动或者提前还完了
			boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.set("is_pay_back", 1).update();
			if (!is_ok_update_fix_bid_system_order) {
				logger.error("更新fix_bid_system_order-is_pay_back失败with fix_bid_sys_id:" + fix_bid_sys_id);
			}
			return;

		} else {
			for (FixBidUserHoldM fix_bid_user_hold : fix_bid_user_hold_list) {
				doSubTaskForFixBidUserHold(fix_bid_user_hold);
			}
			// 针对它所有的单子进行扫描-如果所有的都还完了,那么把is_pay_back变为1
			boolean is_pay_all = Db.queryLong("SELECT COUNT(*) from fix_bid_user_hold WHERE fix_bid_sys_id=? and state=1", new Object[] { fix_bid_sys_id }) == 0;
			if (is_pay_all) {
				boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.set("is_pay_back", 1).update();
				if (!is_ok_update_fix_bid_system_order) {
					logger.error("更新fix_bid_system_order-is_pay_back失败with fix_bid_sys_id:" + fix_bid_sys_id);
				}
			}
		}

	}

	private static void doSubTaskForFixBidUserHold(final FixBidUserHoldM fix_bid_user_hold) {
		boolean is_ok = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				Date now = new Date();
				long id = fix_bid_user_hold.getLong("id");
				String bid_name = fix_bid_user_hold.getString("bid_name");
				int bid_type = fix_bid_user_hold.getInt("bid_type");
				long fix_bid_sys_id = fix_bid_user_hold.getLong("fix_bid_sys_id");
				int user_id = fix_bid_user_hold.getInt("user_id");

				//
				String nick_name = fix_bid_user_hold.getString("nick_name");
				String real_name = fix_bid_user_hold.getString("real_name");
				//
				BigDecimal annual_earning = fix_bid_user_hold.getBigDecimal("annual_earning");
				BigDecimal bid_money = new BigDecimal(fix_bid_user_hold.getInt("bid_money") + "");
				//
				Date exit_time = fix_bid_user_hold.getDate("exit_time");
				long exit_time_long = fix_bid_user_hold.getLong("exit_time_long");

				Date lock_time = fix_bid_user_hold.getDate("lock_time");
				long lock_time_long = fix_bid_user_hold.getLong("lock_time_long");

				// earned_incom
				// state
				int month_count = 0;
				if (bid_type == 1) {
					month_count = 3;
				} else if (bid_type == 2) {
					month_count = 6;
				} else if (bid_type == 3) {
					month_count = 12;
				} else {
					return false;
				}
				// id,bid_type,fix_bid_sys_id,user_id,annual_earning,earned_incom,state
				BigDecimal earned_incom = bid_money.multiply(annual_earning).multiply(new BigDecimal(month_count + "")).divide(new BigDecimal(12 + ""), 8, BigDecimal.ROUND_DOWN);
				/**
				 * 更新持有的状态
				 */
				boolean is_ok_update_fix_bid_user_hold = fix_bid_user_hold.set("state", 2).set("earned_incom", earned_incom).update();
				if (!is_ok_update_fix_bid_user_hold) {
					return false;
				}
				// 保存用户退出记录表
				FixBidUserExitM fix_bid_user_exit = new FixBidUserExitM();
				boolean is_ok_add_fix_bid_user_exit = fix_bid_user_exit//
						.set("bid_name", bid_name)//
						.set("bid_type", bid_type)//
						.set("fix_bid_user_hold_id", id)//
						.set("fix_bid_sys_id", fix_bid_sys_id)//
						.set("user_id", user_id)//
						.set("nick_name", nick_name)//
						.set("real_name", real_name)//
						.set("annual_earning", annual_earning)//
						.set("bid_money", bid_money)//
						.set("earned_incom", earned_incom)// 自动退出时候的收益
						.set("exit_fee", 0)//
						.set("state", 1)// 设置自动退出
						.set("exit_time", exit_time)//
						.set("exit_time_long", exit_time_long)//
						.set("lock_time", lock_time)//
						.set("lock_time_long", lock_time_long)//
						.set("user_exit_time", now)//
						.set("user_exit_time_long", now.getTime())//
						.save();
				if (!is_ok_add_fix_bid_user_exit) {
					return false;
				}

				/**
				 * 更新资金信息
				 */
				UserNowMoneyM user_now_money = UserNowMoneyM.get_user_current_money_for_update(user_id);

				BigDecimal cny_can_used = user_now_money.getBigDecimal("cny_can_used");
				BigDecimal cny_freeze = user_now_money.getBigDecimal("cny_freeze");

				BigDecimal all_money = bid_money.add(earned_incom);// 本金和利息
				boolean is_ok_update_user_now_money = user_now_money.set("cny_can_used", cny_can_used.add(all_money)).update();
				if (!is_ok_update_user_now_money) {
					return false;
				}
				/**
				 * 更新联富宝记录all_pay_back_money
				 */
				FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.get_month_fix_bid_by_id(fix_bid_sys_id);
				BigDecimal all_pay_back_money = fix_bid_system_order.getBigDecimal("all_pay_back_money");
				if (all_pay_back_money == null) {
					all_pay_back_money = new BigDecimal("0");
				}
				boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.set("all_pay_back_money", all_pay_back_money.add(all_money)).update();
				if (!is_ok_update_fix_bid_system_order) {
					return false;
				}
				/**
				 * <pre>
				 * sys_expenses_records-yes
				 * sys_income_records-no
				 * user_money_change_records-yes
				 * user_transaction_records-no
				 * </pre>
				 */
				boolean is_ok_add_sys_expenses_records = SysExpensesRecordsM.add_sys_expenses_records(user_id, UserMoneyChangeRecordsM.Type_29, earned_incom, "自动支出联富宝利息,持有表ID:" + id);
				if (!is_ok_add_sys_expenses_records) {
					return false;
				}
				// 添加本金和利息
				boolean is_ok_add_user_money_change_records = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_28, cny_can_used.add(cny_freeze), new BigDecimal("0"), bid_money, cny_can_used.add(cny_freeze).add(bid_money), "自动支出联富宝利息#持有表ID:" + id);
				if (!is_ok_add_user_money_change_records) {
					return false;
				}
				is_ok_add_user_money_change_records = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_29, cny_can_used.add(cny_freeze), new BigDecimal("0"), earned_incom, cny_can_used.add(cny_freeze).add(bid_money).add(earned_incom), "自动支出联富宝利息#持有表ID:" + id);
				if (!is_ok_add_user_money_change_records) {
					return false;
				}
				// 21
				boolean is_ok_add_user_transaction_records = UserTransactionRecordsM.add_user_transaction_records_by_type(user_id, UserMoneyChangeRecordsM.Type_21, cny_can_used.add(cny_freeze), new BigDecimal("0"), bid_money.add(earned_incom), cny_can_used.add(cny_freeze).add(bid_money).add(earned_incom), "支出联富宝利息");
				if (!is_ok_add_user_transaction_records) {
					return false;
				}
				return true;
			}
		});
		if (!is_ok) {
			long id = fix_bid_user_hold.getLong("id");
			logger.error("联富宝持有ID" + id + "联富宝资金利息返还失败");
		}

	}
}
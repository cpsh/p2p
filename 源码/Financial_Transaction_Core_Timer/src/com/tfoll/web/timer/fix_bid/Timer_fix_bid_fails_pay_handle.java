package com.tfoll.web.timer.fix_bid;

import com.tfoll.trade.activerecord.db.Db;
import com.tfoll.trade.activerecord.transaction.IAtomic;
import com.tfoll.trade.config.Constants;
import com.tfoll.trade.config.UserConfig;
import com.tfoll.web.model.FixBidSystemOrderM;
import com.tfoll.web.model.FixBidUserOrderM;
import com.tfoll.web.model.SysIncomeRecordsM;
import com.tfoll.web.model.UserMoneyChangeRecordsM;
import com.tfoll.web.util.Utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 针对联富包预定未支付处理
 */
public class Timer_fix_bid_fails_pay_handle extends QuartzJobBean {
	private static AtomicBoolean isRunning = new AtomicBoolean(false);
	private static Logger logger = Logger.getLogger(Timer_fix_bid_fails_pay_handle.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		if (!UserConfig.System_Is_Start_Ok.get()) {
			logger.info("系统框架还没有启动好,定时任务:" + Timer_fix_bid_fails_pay_handle.class.getName() + "暂时还不能执行");
			return;
		}

		if (isRunning.get()) {
			logger.info("还有其他的线程正在执行定时任务:" + Timer_fix_bid_fails_pay_handle.class.getName());
			return;
		} else {
			try {
				isRunning.set(true);
				logger.info("定时任务:" + Timer_fix_bid_fails_pay_handle.class.getName() + "开始");
				{// 真正的业务控制
					try {
						doTask();
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();

					}
				}
				logger.info("定时任务:" + Timer_fix_bid_fails_pay_handle.class.getName() + "结束");
			} catch (Exception e) {
				logger.info("定时任务:" + Timer_fix_bid_fails_pay_handle.class.getName() + "抛出异常:" + e.getCause().getMessage());
				if (Constants.devMode) {
					e.printStackTrace();
				}
			} finally {
				isRunning.set(false);
			}
		}

	}

	public static void doTask() throws Exception {
		long now = System.currentTimeMillis();
		/**
		 * 查询出联富宝支付截止时间到联富宝公开投标之间
		 */
		List<FixBidSystemOrderM> fix_bid_system_order_list = FixBidSystemOrderM.dao.find("SELECT id from fix_bid_system_order WHERE pay_end_time_long<=? and invite_start_time_long>=?", new Object[] { now, now });
		if (Utils.isHasData(fix_bid_system_order_list)) {// 一般查询出来有三条记录
			for (FixBidSystemOrderM fix_bid_system_order : fix_bid_system_order_list) {
				try {
					doSubTaskFor(fix_bid_system_order);
				} catch (Exception e) {
					logger.error(e.getMessage());
					e.printStackTrace();

				}
			}

		}

	}

	/**
	 * <pre>
	 * fix_bid_system_order表里面的is_deduct_if_not_pay=1通知[联富宝资金自动转移到系统指定的三个理财账户  com.tfoll.web.timer.fix_bid.Timer_fix_bid_transfer_funds_to_user_account 进行资金划扣]
	 * </pre>
	 */
	private static void doSubTaskFor(FixBidSystemOrderM fix_bid_system_order) {
		long fix_bid_sys_id = fix_bid_system_order.getLong("id");
		List<FixBidUserOrderM> fix_bid_user_order_list = FixBidUserOrderM.dao.find("SELECT id,user_id,already_pay_bond,bid_money,bid_share,is_pay from fix_bid_user_order WHERE fix_bid_sys_id=? and is_pay=0", new Object[] { fix_bid_sys_id });
		if (!Utils.isHasData(fix_bid_user_order_list)) {
			fix_bid_system_order.set("is_deduct_if_not_pay", 1).update();
		} else {
			for (FixBidUserOrderM fix_bid_user_order : fix_bid_user_order_list) {
				/**
				 * 针对联富宝的预定但进行资金的划扣
				 */
				doSubTaskForFixBidUserOrder(fix_bid_user_order, fix_bid_sys_id);
			}
			/**
			 * <pre>
			 * 判断所有的预订单都进行了资金划扣,如果OK则把is_deduct_if_not_pay字段变为1-这样通知联富宝资金转移程序进行成功投资的资金进行转移
			 * </pre>
			 */
			boolean is_ok = true;
			for (FixBidUserOrderM fix_bid_user_order : fix_bid_user_order_list) {
				int is_pay = fix_bid_user_order.getInt("is_pay");
				// 表示之前的程序没有支付或者支付资金划扣
				if (is_pay == 0) {
					is_ok = false;
					break;
				}

			}
			if (is_ok) {
				fix_bid_system_order.set("is_deduct_if_not_pay", 1).update();
			} else {
				// 可能会出BUG-永远不会出现is_deduct_if_not_pay=1的情况
				logger.error("联富宝预定资金划扣失败-需要运维人员进行处理");

			}
		}

	}

	/**
	 * 针对联富宝的预定但进行资金的划扣
	 */
	private static void doSubTaskForFixBidUserOrder(final FixBidUserOrderM fix_bid_user_order, final long fix_bid_sys_id) {
		int is_pay = fix_bid_user_order.getInt("is_pay");
		/**
		 * 如果is_pay不为0表示已经处理过了,可能支付成功可能未支付但系统已经处理过了
		 */
		if (is_pay != 0) {
			return;
		}
		final long id = fix_bid_user_order.getLong("id");
		boolean is_ok_fix_bid_fails_pay_handle = Db.tx(new IAtomic() {

			public boolean transactionProcessing() throws Exception {
				// id,user_id,already_pay_bond,is_pay
				int user_id = fix_bid_user_order.getInt("user_id");
				// bid_money,bid_share-all int
				int bid_money = fix_bid_user_order.getInt("bid_money");
				// int bid_share=fix_bid_user_order.getInt("bid_share");
				int already_pay_bond = fix_bid_user_order.getInt("already_pay_bond");
				/**
				 * 更新is_pay为预定为支付状态
				 */
				boolean is_ok_update_fix_bid_user_order = fix_bid_user_order.set("is_pay", 2).update();
				if (!is_ok_update_fix_bid_user_order) {
					return false;
				}
				/**
				 * 修改联富宝资金信息sold_money
				 */
				FixBidSystemOrderM fix_bid_system_order = FixBidSystemOrderM.dao.findFirst("SELECT id,sold_money,not_pay_after_deadline from fix_bid_system_order WHERE id=? for UPDATE", new Object[] { fix_bid_sys_id });
				int sold_money = fix_bid_system_order.getInt("sold_money");
				sold_money = sold_money - bid_money;
				/**
				 * 修改联富宝没有支付的资金
				 */
				BigDecimal not_pay_after_deadline = fix_bid_system_order.getBigDecimal("not_pay_after_deadline");
				if (not_pay_after_deadline == null) {
					not_pay_after_deadline = new BigDecimal("0");
				}
				not_pay_after_deadline = not_pay_after_deadline.add(new BigDecimal(already_pay_bond + ""));

				boolean is_ok_update_fix_bid_system_order = fix_bid_system_order.set("sold_money", sold_money).set("not_pay_after_deadline", not_pay_after_deadline).update();
				if (!is_ok_update_fix_bid_system_order) {
					return false;
				}
				// 添加财务记录
				/**
				 * <pre>
				 * sys_expenses_records no
				 * sys_income_records-no
				 * user_money_change_records yes
				 * user_transaction_records no
				 * </pre>
				 */
				boolean is_ok_add_sys_income_records = SysIncomeRecordsM.add_sys_income_records(user_id, UserMoneyChangeRecordsM.Type_22, new BigDecimal(already_pay_bond + ""), "联富宝预定未支付#预订表ID:" + id);
				if (!is_ok_add_sys_income_records) {
					return false;
				}
				BigDecimal cny_can_used_and_cny_freeze = Db.queryBigDecimal("SELECT (cny_can_used+cny_freeze) as cny_can_used_cny_freeze from user_now_money WHERE user_id=?", new Object[] { user_id });
				boolean is_ok_add_user_money_change_records_by_type = UserMoneyChangeRecordsM.add_user_money_change_records_by_type(user_id, UserMoneyChangeRecordsM.Type_22, cny_can_used_and_cny_freeze, new BigDecimal(already_pay_bond + ""), new BigDecimal(0 + ""), cny_can_used_and_cny_freeze, "联富宝预定未支付#预订表ID:" + id);
				if (!is_ok_add_user_money_change_records_by_type) {
					return false;
				}
				return true;
			}
		});

		if (!is_ok_fix_bid_fails_pay_handle) {
			logger.error("联福宝预约定金转移失败  order-id=" + id);
		}

	}

}